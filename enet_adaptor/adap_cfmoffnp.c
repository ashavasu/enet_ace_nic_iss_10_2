#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_brgnp.h"
#include "adap_service_config.h"
#include "mea_drv_common.h"
#include "adap_enetConvert.h"
#include "adap_ecfmminp.h"
#include "EnetHal_L2_Api.h"

#define WITHOUT_USING_FORWARDER

static MEA_Bool cfmOffload=MEA_FALSE;

MEA_Bool adap_getCfmOffloadState(void)
{
	return cfmOffload;
}

void adap_setCfmOffloadState(MEA_Bool state)
{
	cfmOffload = state;
}

static MEA_Status adap_FsMiHwSetInterfaceCfmOamTagged (MEA_Port_t enetPort,int cfm_oam_qtag,int cfm_oam_tag,int cfm_oam_untag)
{
 	MEA_IngressPort_Entry_dbt entry;


	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        fprintf(stdout,"adap_FsMiHwSetInterfaceCfmOamTagged: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	entry.parser_info.cfm_oam_qtag      =        cfm_oam_qtag;
	entry.parser_info.cfm_oam_tag       =        cfm_oam_tag;
	entry.parser_info.cfm_oam_untag      =       cfm_oam_untag;


	if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		fprintf(stdout,"adap_FsMiHwSetInterfaceCfmOamTagged: MEA_API_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}



    return (MEA_OK);
}

static MEA_Status adap_FsMiHwPacketGenCreateiNFO(tAdap_PacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond)
{
	MEA_PacketGen_Entry_dbt entry;
	MEA_Uint32	idx=0;
	MEA_Uint32  oamInfo=0;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =1; //120 bytes

	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->DA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //DA
	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->SA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //SA

	if(pPacketGen->num_of_vlans)
	{
		adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pPacketGen->num_of_vlans > 1)
		{
			adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pPacketGen->meg_level & 0x000007) << 5);
	oamInfo |=  (pPacketGen->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->flags & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->tlvOffset & 0xFF;


	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->initSeqNum,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //seq number

	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->mepId,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //mepid

	for(idx=0;idx<pPacketGen->megLen;idx++)
	{
		entry.header.data[entry.header.length++]= pPacketGen->megData[idx];
	}

	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCf,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCf information

	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->RxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //RxFCb information

	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCb information

	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCb information

	entry.header.length++;

	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

static ADAP_Int32 adap_FsCreateHwPacketGenerator(tAdap_ECFMCcmInst	*ccmDb,int ccmEntry)
{
	tEnetHal_MacAddr macAddr = {.ether_addr_octet = {0x00,0x01,0x02,0x01,0x02,0x00}};
	MEA_EHP_Info_dbt tEhp_info;
#ifndef WITHOUT_USING_FORWARDER	// create action and forwarder
	int num_of_entries=1;
#endif
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 VpnIndex=0;
	tAdap_PacketGenHeader tPacketGen;
	MEA_Uint32 packetsPerSecond=0;
	tRmtSrvInfo srvInfo;
	ADAP_Uint16 genVlan=0;
	MEA_Uint32  MD_level;
	MEA_Port_t  enetPort;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
	adap_memset(&srvInfo,0,sizeof(tRmtSrvInfo));

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		adap_FsMiHwSetInterfaceCfmOamTagged ((ADAP_Int32)ccmDb->EcfmDbParams.u4PortArray[ccmEntry],1,1,1);
	}
	else
	{
		adap_FsMiHwSetInterfaceCfmOamTagged (ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex,1,1,1);
	}

	// if the port configure as  VLAN_PROVIDER_CORE_BRIDGE_MODE then add VLAN and swap the exist one
	if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) )
	{
		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x88a8;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG; //MEA_EHP_FLOW_TYPE_QTAG;
		tEhp_info.ehp_data.eth_info.val.vlan.pri	  = ccmDb->EcfmDbParams.EcfmHwMepParams.u1CcmPriority & 0x7;
	}
	else
	{
		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		tEhp_info.ehp_data.eth_info.val.vlan.pri	  = ccmDb->EcfmDbParams.EcfmHwMepParams.u1CcmPriority & 0x7;

	}

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	genVlan = adap_allocateInternalVlanId();
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		ccmDb->EcfmDbParams.tSubEntry[ccmEntry].internalVlan = genVlan;
	}
	else
	{
		ccmDb->EcfmDbParams.internalVlan = genVlan;
	}


	srvInfo.EnetPolilcingProfile = adap_GetDefaultPolicerId(1); // need to create policer for port 120
	srvInfo.b_lmEnable=MEA_FALSE;
#ifdef WITHOUT_USING_FORWARDER
	srvInfo.keyType = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN; //MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN; //MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
#else
	srvInfo.keyType = MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN; //MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
#endif
	srvInfo.lmId=0;
	srvInfo.me_level=0;
	srvInfo.me_valid=MEA_FALSE;
	srvInfo.src_port=120;
	srvInfo.vlanId=genVlan;

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
#ifdef WITHOUT_USING_FORWARDER
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[ccmEntry], &enetPort);
			MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
			MEA_SET_OUTPORT(&tOutPorts, enetPort);
		}
		else
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex, &enetPort);
			MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
			MEA_SET_OUTPORT(&tOutPorts, enetPort);
		}

		if(adap_FsMiHwCreateCfmServiceWithOutPut(&ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.Service_id,
											&srvInfo,&tOutPorts,&tEhp_info) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create service\n");
			return ENET_FAILURE;
		}
#else
		if(adap_FsMiHwCreateCfmService(&ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.Service_id,&srvInfo) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create service\n");
			return ENET_FAILURE;
		}
#endif
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsCreateHwPacketGenerator: create upMep service:%d\n",
								ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.Service_id);
	}
	else
	{
#ifdef WITHOUT_USING_FORWARDER		// send the traffic directly to output port without creating forwarder

		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[ccmEntry], &enetPort);
			MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
			MEA_SET_OUTPORT(&tOutPorts, enetPort);
		}
		else
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex, &enetPort);
			MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
			MEA_SET_OUTPORT(&tOutPorts, enetPort);
		}
		if(adap_FsMiHwCreateCfmServiceWithOutPut(&ccmDb->EcfmDbParams.enetPacketGenDb.Service_id,
											&srvInfo,&tOutPorts,&tEhp_info) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create service\n");
			return ENET_FAILURE;
		}
#else // create service without output port
		if(adap_FsMiHwCreateCfmService(&ccmDb->EcfmDbParams.enetPacketGenDb.Service_id,&srvInfo) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create service\n");
			return ENET_FAILURE;
		}
#endif

	}
	VpnIndex = srvInfo.vpn;

	ccmDb->isDestMulticast=MEA_FALSE;
	MD_level = ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel;
	if(adap_is_mac_multicast(&ccmDb->EcfmDbParams.EcfmHwMepParams.DstMacAddr))
	{
		MD_level |= 0x08;
		ccmDb->isDestMulticast=MEA_TRUE;
		// multicast packet
	}


#ifndef WITHOUT_USING_FORWARDER	// create action and forwarder
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.actionid,&tEhp_info,num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create action failed\n");
			return enet_FAILURE;
		}

	}
	else
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.enetPacketGenDb.actionid,&tEhp_info,num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create action failed\n");
			return ENET_FAILURE;
		}
	}

	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsCreateHwPacketGenerator:actionId:%d\n",ccmDb->EcfmDbParams.enetPacketGenDb.actionid);

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[ccmEntry], &enetPort);
		MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
		MEA_SET_OUTPORT(&tOutPorts, enetPort);
	}
	else
	{
		MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex, &enetPort);
		MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
		MEA_SET_OUTPORT(&tOutPorts, enetPort);
	}

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.actionid,
								VpnIndex,
								&tOutPorts,
								MD_level,
								D_CCM_OPCODE_VALUE,
								genVlan,
								120) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create forwarder failed\n");
				return ENET_FAILURE;
			}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action_id:%d vpnId:%d entry:%d\n",ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.actionid,VpnIndex,ccmEntry);
		ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.VpnIndex = VpnIndex;
		ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenProfileId = getDplPacketGenProfile(93);
	}
	else
	{
		if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.enetPacketGenDb.actionid,
								VpnIndex,
								&tOutPorts,
								MD_level,
								D_CCM_OPCODE_VALUE,
								genVlan,
								120) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create forwarder failed\n");
				return ENET_FAILURE;
			}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action_id:%d vpnId:%d\n",ccmDb->EcfmDbParams.enetPacketGenDb.actionid,VpnIndex);
		ccmDb->EcfmDbParams.enetPacketGenDb.VpnIndex = VpnIndex;
		ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId = getDplPacketGenProfile(93);
	}
#else
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.VpnIndex = VpnIndex;
		ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenProfileId = adap_getPacketGenProfile(93);
	}
	else
	{
		ccmDb->EcfmDbParams.enetPacketGenDb.VpnIndex = VpnIndex;
		ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId = adap_getPacketGenProfile(93);
	}
#endif
	if(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId != UNDEFINED_VALUE16)
	{
		//allocDplPacketGenProfile(93,ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsCreateHwPacketGenerator: profile:%d allready exist\n",ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);

	}
	else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction != ADAP_ECFM_NP_MP_DIR_UP)
		{
			if(adap_FsMiHwPacketGenProfInfoCreate(93,&ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId,3) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create packet gen profile failed\n");
				return ENET_FAILURE;
			}
			adap_allocPacketGenProfile(93,ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);
		}
		else
		{
			if(adap_FsMiHwPacketGenProfInfoCreate(93,&ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenProfileId,3) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: create packet gen profile failed\n");
				return ENET_FAILURE;
			}
			adap_allocPacketGenProfile(93,ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenProfileId);
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: not enough room to create new packet gen profile\n");
		return ENET_FAILURE;
	}


	if(adap_FsMiHwGetInterfaceCfmOamUCMac(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex,&macAddr) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to get mac address from interface:%d\n",ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex);
		return ENET_FAILURE;
	}

	if((adap_is_mac_zero(&macAddr) == ADAP_TRUE) && (ccmDb->tPduFromIss.valid == MEA_TRUE))
	{
		adap_mac_from_arr(&macAddr, ccmDb->tPduFromIss.SA.b);
		adap_FsMiHwSetInterfaceCfmOamUCMac(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex,&macAddr);
	}

// MEA pac gen set create %d
	adap_mac_to_arr(&ccmDb->EcfmDbParams.EcfmHwMepParams.DstMacAddr, tPacketGen.DA.b);
	adap_mac_to_arr(&macAddr, tPacketGen.SA.b);
	tPacketGen.num_of_vlans=1;
//	tPacketGen.outerVlan=0x81000000 | ccmDb->EcfmHwMaParams.u4VlanIdIsid | ( (ccmDb->EcfmDbParams.EcfmHwMepParams.u1CcmPriority & 0x7) << 13);
	tPacketGen.outerVlan = 0x81000000 | genVlan | ( (ccmDb->EcfmDbParams.EcfmHwMepParams.u1CcmPriority & 0x7) << 13);
	tPacketGen.innerVlan=0x81000000;
	tPacketGen.cfmEtherType=0x8902;
	tPacketGen.meg_level=ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel; //3 bits
	tPacketGen.version=0; //5 bits
	tPacketGen.opCode=ADAP_D_CCM_OPCODE_VALUE; // 8 bits

	if(ccmDb->EcfmHwMaParams.u4CcmInterval == 1000000)
	{
		packetsPerSecond=1;
		tPacketGen.flags=ADAP_D_CCM_1SEC_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 10000)
	{
		packetsPerSecond=100;
		tPacketGen.flags=ADAP_D_CCM_10MS_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 10000000)
	{
		packetsPerSecond=1;
		tPacketGen.flags=ADAP_D_CCM_10SEC_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 100000)
	{
		packetsPerSecond=10;
		tPacketGen.flags=ADAP_D_CCM_100MS_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 60000000)
	{
		packetsPerSecond=1;
		tPacketGen.flags=ADAP_D_CCM_1MIN_PERIOD;
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 600000000)
	{
		packetsPerSecond=1;
		tPacketGen.flags=ADAP_D_CCM_10MIN_PERIOD;
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 3000)
	{
		packetsPerSecond=333;
		tPacketGen.flags=ADAP_D_CCM_3_3MS_PERIOD; // 8 bits
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: unknown interval:%d\n",ccmDb->EcfmHwMaParams.u4CcmInterval);
		return ENET_FAILURE;
	}
	ccmDb->EcfmDbParams.enetPacketGenDb.ccm_interval = ccmDb->EcfmHwMaParams.u4CcmInterval;
	tPacketGen.tlvOffset=70; // 8 bits
	tPacketGen.initSeqNum=0;
	tPacketGen.mepId=ccmDb->EcfmDbParams.EcfmHwMepParams.u2MepId;
	tPacketGen.megLen=48;
	adap_memcpy(&tPacketGen.megData[0],ccmDb->EcfmHwMaParams.au1MAID,48);
	tPacketGen.TxFCf=0;
	tPacketGen.RxFCb=0;
	tPacketGen.TxFCb=0;

	if(adap_isPacketBigGenTableFull() == MEA_TRUE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: not enough room to create new packet gen id\n");
		return ENET_FAILURE;
	}


	ccmDb->EcfmDbParams.rdiState=MEA_FALSE;
	ccmDb->EcfmDbParams.tSubEntry[ccmEntry].rdiState=MEA_FALSE;
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwPacketGenCreateiNFO(&tPacketGen,ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenProfileId,
						&ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenId,packetsPerSecond) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create packet gen id\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		if(adap_FsMiHwPacketGenCreateiNFO(&tPacketGen,ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId,&ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId,packetsPerSecond) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to create packet gen id\n");
			return ENET_FAILURE;
		}
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		adap_AllocPacketBigGenTableFull(ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenId);
	}
	else
	{
		adap_AllocPacketBigGenTableFull(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId);
	}

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.tSubEntry[ccmEntry].enetPacketGenDb.packetGenId,MEA_TRUE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to activate packet gen\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId,MEA_TRUE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to activate packet gen\n");
			return ENET_FAILURE;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwPacketGenActive activate\n");

	return ENET_SUCCESS;
}

ADAP_Int32 adap_FsDeleteHwPacketGenerator(tAdap_ECFMCcmInst *ccmDb,int entry_num)
{
	MEA_Bool bValid=MEA_FALSE;
	ADAP_Uint8 MD_level=0;

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.Service_id != UNDEFINED_VALUE16)
		{
			if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.Service_id) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the service %d entry:%d\n",
													ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.Service_id,
													entry_num);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create the new service %d\n",ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.Service_id);
			ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
		}
	}
	if(ccmDb->EcfmDbParams.enetPacketGenDb.Service_id != UNDEFINED_VALUE16)
	{
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, ccmDb->EcfmDbParams.enetPacketGenDb.Service_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the service %d\n", ccmDb->EcfmDbParams.enetPacketGenDb.Service_id);
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create the new service %d\n",ccmDb->EcfmDbParams.enetPacketGenDb.Service_id);
		ccmDb->EcfmDbParams.enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenId != UNDEFINED_VALUE16)
	{

		if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenId,MEA_FALSE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to activate packet gen\n");
			return ENET_FAILURE;
		}

		adap_usSleepFunc(20*1000);

		if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the packetGen %d\n", ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId);
		}
		adap_freePacketBigGenTableFull(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenId);
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId != UNDEFINED_VALUE16)
	{
		if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId,MEA_FALSE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreateHwPacketGenerator: failed to activate packet gen\n");
			return ENET_FAILURE;
		}

		if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the packetGen %d\n", ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId);
		}
		adap_freePacketBigGenTableFull(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId);
		ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: profileId=%d\n",
										ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId);

		adap_freePacketGenProfile(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId,&bValid);
		if(bValid == MEA_TRUE)
		{
			if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the packetGen profile:%d\n",
									ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: profileId=%d deleted\n",
							ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId);
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;

	}
	if(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId != UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: profileId=%d\n",ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);
		adap_freePacketGenProfile(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId,&bValid);
		if(bValid == MEA_TRUE)
		{
			if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: Unable to delete the packetGen profile:%d\n", ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: profileId=%d deleted\n",ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId);
		}
		ccmDb->EcfmDbParams.enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;
	}

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		MD_level = ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: delete multicast icast forwarder \n");
		if(ccmDb->isDestMulticast==MEA_TRUE)
		{
			MD_level |= 0x08;
		}
#ifndef WITHOUT_USING_FORWARDER	// not need to delete forwarder
		if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmDbParams.tSubEntry[entry_num].internalVlan,
									ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.VpnIndex,
									MD_level,
									ADAP_D_CCM_OPCODE_VALUE,
									120,
									&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"adap_FsDeleteHwPacketGenerator:id:%d internalVlan:%d vpn:%d\n",
										entry_num,
										ccmDb->EcfmDbParams.tSubEntry[entry_num].internalVlan,
										ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.VpnIndex);
		}
#endif
		adap_freeInternalVlanId(ccmDb->EcfmDbParams.tSubEntry[entry_num].internalVlan);
		ccmDb->EcfmDbParams.tSubEntry[entry_num].internalVlan = UNDEFINED_VALUE16;

	}
	if(ccmDb->EcfmDbParams.internalVlan != UNDEFINED_VALUE16)
	{
		MD_level = ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeleteHwPacketGenerator: delete multicast icast forwarder \n");
		if(ccmDb->isDestMulticast==MEA_TRUE)
		{
			MD_level |= 0x08;
		}
#ifndef WITHOUT_USING_FORWARDER	// not need to delete forwarder
		if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmDbParams.internalVlan,
									ccmDb->EcfmDbParams.enetPacketGenDb.VpnIndex,
									MD_level,
									ADAP_D_CCM_OPCODE_VALUE,
									120,
									&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"adap_FsDeleteHwPacketGenerator:second time\n");
		}
#endif
		adap_freeInternalVlanId(ccmDb->EcfmDbParams.internalVlan);
		ccmDb->EcfmDbParams.internalVlan = UNDEFINED_VALUE16;
	}
	// free the vpnIndex
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.actionid != UNDEFINED_VALUE16)
		{
			if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.actionid) != MEA_OK )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			}
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.actionid = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.enetPacketGenDb.actionid != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.enetPacketGenDb.actionid) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeleteHwPacketGenerator ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
		}
		ccmDb->EcfmDbParams.enetPacketGenDb.actionid = UNDEFINED_VALUE16;
	}
	adap_FreeGenVpnId(ccmDb->EcfmDbParams.enetPacketGenDb.VpnIndex);
	ccmDb->EcfmDbParams.enetPacketGenDb.VpnIndex=0;
	adap_FreeGenVpnId(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.VpnIndex);
	ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketGenDb.VpnIndex=0;

	return ENET_SUCCESS;

}

ADAP_Uint32 adap_FsDeletePacketAnalyzer(tAdap_ECFMCcmInst *ccmDb,int entry_num)
{
	ADAP_Uint8 md_level=0;
	MEA_Action_t get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	ADAP_Uint32 num_of_try=0;
	MEA_Port_t	enetPort;

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetCcmAppDb.ccmState = ADAP_E_WAIT_ON_PACKET_STATE;
	}
	else
	{
		ccmDb->EcfmDbParams.enetCcmAppDb.ccmState = ADAP_E_WAIT_ON_PACKET_STATE;
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
			adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.u4PortArray[entry_num],
									D_TAGGED_SERVICE_TAG,
									MEA_FALSE,
									md_level);

			adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.u4PortArray[entry_num],
									D_PROVIDER_SERVICE_TAG,
									MEA_FALSE,
									md_level);
	}
	else
	{
		adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex,
								D_TAGGED_SERVICE_TAG,
								MEA_FALSE,
								md_level);

		adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex,
								D_PROVIDER_SERVICE_TAG,
								MEA_FALSE,
								md_level);
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeletePacketAnalyzer unicast ccm\n");
	md_level = ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel;
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsDeletePacketAnalyzer multicast ccm\n");
	if(ccmDb->isDestMulticast == MEA_TRUE)
	{
		md_level |= 0x08;
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
		if( adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
									md_level,
									ADAP_D_CCM_OPCODE_VALUE,
									enetPort,
									&get_paction_id)!= MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: FsMiHwDeleteRxForwarder FAIL !!! vlanId:%d\n",
			ccmDb->EcfmHwMaParams.u4VlanIdIsid);
		}
	}
	else
	{
		MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
		if( adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
									md_level,
									ADAP_D_CCM_OPCODE_VALUE,
									enetPort,
									&get_paction_id)!= MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: adap_FsMiHwDeleteRxForwarder FAIL !!! ===\n");
		}
	}

	if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
	}
	//delete forwarder
	if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io != UNDEFINED_VALUE16)
	{
		if( MEA_API_Delete_CCM_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer: failed to delete MEA_API_Delete_CCM_Entry\r\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana != UNDEFINED_VALUE16)
	{
		// delete forwarder before delete action
		for(num_of_try=0;num_of_try<3;num_of_try++)
		{
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
			if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
										ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
										md_level,
										ADAP_D_CCM_OPCODE_VALUE,
										enetPort,
										&get_paction_id)!= MEA_OK)

			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteRxForwarder ERROR: failed by delete adap_FsMiHwDeleteRxForwarder num of try:%d\n",num_of_try);
			}
			else
			{
				break;
			}
		}
		adap_usSleepFunc(20*1000);
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu != UNDEFINED_VALUE16)
	{
		for(num_of_try=0;num_of_try<3;num_of_try++)
		{
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
			if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
										ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
										md_level,
										ADAP_D_CCM_OPCODE_VALUE,
										enetPort,
										&get_paction_id)!= MEA_OK)

			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteRxForwarder ERROR: failed by delete adap_FsMiHwDeleteRxForwarder num of try:%d\n",num_of_try);
			}
			else
			{
				break;
			}
		}
		adap_usSleepFunc(20*1000);
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
	}
	if(ccmDb->EcfmDbParams.enetPacketAnaDb.id_io != UNDEFINED_VALUE16)
	{
		//delete forwarder
		if( MEA_API_Delete_CCM_Entry(MEA_UNIT_0,ccmDb->EcfmDbParams.enetPacketAnaDb.id_io) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer: failed to delete MEA_API_Delete_CCM_Entry\r\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
	}
	// delete analyzer
	return ENET_SUCCESS;
}

static ADAP_Uint16 adap_getIndexByRxFilter(ADAP_Uint16 u2TxFilterId,ADAP_Uint16 *pIdxArray,ADAP_Uint16 MaxNumOfIndex)
{
	ADAP_Int32 idx;
	ADAP_Int16 numOfIndex = 0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if(tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid == MEA_TRUE)
		{
			if(tAdap_EcfmCcmDb[idx].EcfmDbParams.EcfmHwCcTxParams_valid == MEA_TRUE)
			{
				if(tAdap_EcfmCcmDb[idx].EcfmDbParams.EcfmHwCcRxParams.u4IfIndex == u2TxFilterId )
				{
					if(numOfIndex < MaxNumOfIndex)
					{
						pIdxArray[numOfIndex] = idx;
						numOfIndex++;
					}
				}
			}

		}
	}
	return numOfIndex;
}

static MEA_Status adap_FsCcmGetPmCounterFromAction(MEA_Action_t action_id,MEA_Counters_PM_dbt *pPM_Entry)
{
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt	Action_editing;

    adap_memset(&Action_policer  , 0 , sizeof(MEA_Policer_Entry_dbt ));
    adap_memset(&Action_editing  , 0 , sizeof(MEA_EgressHeaderProc_Array_Entry_dbt ));
	adap_memset(&Action_outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
    adap_memset(&Action_data  , 0 , sizeof(MEA_Action_Entry_Data_dbt ));

	if(ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
						action_id,
						&Action_data,
						&Action_outPorts,
						&Action_policer,
						&Action_editing)!= MEA_OK )
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsCcmGetPmCounterFromAction ERROR: ENET_ADAPTOR_Get_Action FAIL !!! ===\n");
        return MEA_ERROR;
	}
	if(Action_data.pm_id_valid == MEA_TRUE)
	{
		if(MEA_API_Collect_Counters_PM(MEA_UNIT_0,Action_data.pm_id,NULL) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to collect statistics for PmID: %d!!! \n", Action_data.pm_id);
			return MEA_ERROR;
		}
		adap_memset(pPM_Entry,0,sizeof(MEA_Counters_PM_dbt));
		if (MEA_API_Get_Counters_PM(MEA_UNIT_0, Action_data.pm_id, pPM_Entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get statistics for PmID: %d!!! \n", Action_data.pm_id);
			return MEA_ERROR;
		}
		return MEA_OK;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"action not include pm counter!!!\n");
	return MEA_ERROR;
}

ADAP_Uint32 adap_FsGetRxCcmPacketsFromIndex(ADAP_Uint16 idx)
{
	MEA_Counters_PM_dbt tPM_Entry;
	ADAP_Uint32 packets=0;
	if(tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid == MEA_TRUE)
	{
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu == UNDEFINED_VALUE16)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action send to cpu not exist\r\n");
		}
		else if(adap_FsCcmGetPmCounterFromAction(tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,&tPM_Entry) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsGetRxCcmPacketsFromIndex: green forward traffic:%lu",tPM_Entry.green_fwd_pkts.s.lsw);
			packets += tPM_Entry.green_fwd_pkts.s.lsw;
		}
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana == UNDEFINED_VALUE16)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action send to analyzer not exist\r\n");
		}
		else if(adap_FsCcmGetPmCounterFromAction(tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,&tPM_Entry) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsGetRxCcmPacketsFromIndex: green forward traffic:%lu",tPM_Entry.green_fwd_pkts.s.lsw);
			packets += tPM_Entry.green_fwd_pkts.s.lsw;
		}
	}
	return packets;
}

static ADAP_Int32 adap_getIndexByMaStrCcmDb(char *au1MAID)
{
	ADAP_Int32 idx;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if( (tAdap_EcfmCcmDb[idx].valid == MEA_TRUE) &&
			(tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid == MEA_TRUE) &&
			(strncmp((char *)tAdap_EcfmCcmDb[idx].EcfmHwMaParams.au1MAID,au1MAID,48) == 0) )
		{
			return idx;
		}
	}
	return UNDEFINED_VALUE16;
}

static ADAP_Int32 adap_setMaStrCcmDb(tEnetHal_EcfmHwMaParams *pMaParam)
{
	ADAP_Int32 idx;
	ADAP_Int32 idx1;
	ADAP_Int32 rand_number;


	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if(tAdap_EcfmCcmDb[idx].valid == MEA_FALSE)
		{
			adap_memset(&tAdap_EcfmCcmDb[idx],0,sizeof(tAdap_ECFMCcmInst));
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.actionid = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.internalVlan = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].valid = MEA_TRUE;
			adap_memcpy(&tAdap_EcfmCcmDb[idx].EcfmHwMaParams,pMaParam,sizeof(tEnetHal_EcfmHwMaParams));
			tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid = MEA_TRUE;
			for(idx1=0;idx1<ADAP_D_MAX_OF_CCM_SUB_ENTRIES;idx1++)
			{
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.actionid = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].internalVlan = UNDEFINED_VALUE16;
			}
			rand_number = rand();
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"dpl_allocateFreeCcmDb: random:%d",rand_number);
			tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[0] = (ADAP_Uint8)((rand_number & 0xFF000000) >> 24);
			tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[1] = (ADAP_Uint8)((rand_number & 0x00FF0000) >> 16);
			tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[2] = (ADAP_Uint8)((rand_number & 0x0000FF00) >> 8);
			tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[3] = (ADAP_Uint8)((rand_number & 0x000000FF) );
			return idx;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_allocateFreeCcmDb: not enough room to get free index");
	return UNDEFINED_VALUE16;
}

static void adap_parseCcmIssPdu(ADAP_Uint16 u2PduLength,ADAP_Uint8 *pu1Pdu,tAdap_PacketGenHeader *pPacketGen)
{
	ADAP_Uint16 idx,idx1;
	ADAP_Uint16 ethertype=0;
	char printBuffer[50];

	pPacketGen->valid = MEA_FALSE;

	if( (pu1Pdu == NULL) || (u2PduLength < 70) )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"data pdu is NULL or len:%d less then 70\r\n",u2PduLength);
		return;
	}
//#if 0
	idx1=0;
	for(idx=0;idx<u2PduLength;idx++)
	{
		if((idx%16) == 0)
		{
			if(idx1 != 0)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\r\n",printBuffer);
			}
			adap_memset(&printBuffer[0],0,50);
			idx1=0;
		}
		idx1 += sprintf(&printBuffer[idx1],"%x ",pu1Pdu[idx]);
	}
	if(idx1 != 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\r\n",printBuffer);
	}

//#endif
	idx=0;
	adap_memcpy(&pPacketGen->DA.b,&pu1Pdu[idx],ETH_ALEN);
	idx+=ETH_ALEN;
	adap_memcpy(&pPacketGen->SA.b,&pu1Pdu[idx],ETH_ALEN);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Mac address %x:%x:%x:%x:%x:%x\r\n",
										pPacketGen->SA.b[0],
										pPacketGen->SA.b[1],
										pPacketGen->SA.b[2],
										pPacketGen->SA.b[3],
										pPacketGen->SA.b[4],
										pPacketGen->SA.b[5]);
	idx+=ETH_ALEN;
	adap_memcpy(&pPacketGen->outerVlan,&pu1Pdu[idx],4);
	idx+=4;
	adap_memcpy(&ethertype,&pu1Pdu[idx],2);
	if(ethertype == 0x8100)
	{
		adap_memcpy(&pPacketGen->innerVlan,&pu1Pdu[idx],4);
		idx+=4;
	}
	adap_memcpy(&pPacketGen->cfmEtherType,&pu1Pdu[idx],2);
	idx+=2;

	pPacketGen->meg_level = (pu1Pdu[idx] & 0xE0) >> 5;
	pPacketGen->version = pu1Pdu[idx] & 0x1F;
	idx++;

	pPacketGen->opCode = pu1Pdu[idx];
	idx++;

	pPacketGen->flags = pu1Pdu[idx];
	idx++;
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ISS-PDU: flags:%d opCode:%d lvl:%d \r\n",pPacketGen->flags,pPacketGen->opCode,pPacketGen->meg_level);
	pPacketGen->tlvOffset = pu1Pdu[idx];
	idx++;


	idx+=8; // sequence id

	adap_memcpy(&pPacketGen->megData,&pu1Pdu[idx],48);
	pPacketGen->megLen = strlen((char *)pPacketGen->megData);
	pPacketGen->valid=MEA_TRUE;
}

static tAdap_ECFMCcmInst *adap_getDatabaseCcmDbFromIndex(ADAP_Uint32 idx)
{
	if(idx >= ADAP_D_MAX_DATABASE_CCM_CONFIGURATION)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_getDatabaseCcmDbFromIndex: index not of range:%d",idx);
		return NULL;
	}
	return &tAdap_EcfmCcmDb[idx];
}

static ADAP_Int32 adap_FsTxCcmHandler(ADAP_Uint8 u1Type,tEnetHal_EcfmHwParams * pEcfmHwInfo)
{
	ADAP_Uint32 idx;
	tAdap_ECFMCcmInst	*ccmDb;
	MEA_Bool param_change=MEA_FALSE;
	ADAP_Int32 issPortList;

	idx = adap_getIndexByMaStrCcmDb((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID);
	if(idx== UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: ma:%s not found\r\n",pEcfmHwInfo->EcfmHwMaParams.au1MAID);
		return ENET_FAILURE;
	}
	ccmDb = adap_getDatabaseCcmDbFromIndex(idx);
	if(ccmDb == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: ccm db not found\r\n");
		return ENET_FAILURE;
	}

	pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0] = ccmDb->EcfmDbParams.au1HwHandler[0];
	pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1] = ccmDb->EcfmDbParams.au1HwHandler[1];
	pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2] = ccmDb->EcfmDbParams.au1HwHandler[2];
	pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3] = ccmDb->EcfmDbParams.au1HwHandler[3];

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ECFM_NP_START_CCM_TX handler :%d %d %d %d\n",
								pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0],
								pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1],
								pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2],
								pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3]);

	adap_parseCcmIssPdu(pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2PduLength,pEcfmHwInfo->unParam.EcfmHwCcTxParams.pu1Pdu,&ccmDb->tPduFromIss);

	if( ( (ccmDb->u1LastTxType == ADAP_ECFM_NP_START_CCM_TX) && (u1Type == ADAP_ECFM_NP_START_CCM_TX) ) ||
		( (ccmDb->u1LastTxType == ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST) && (u1Type == ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST) ) )
	{
		return ENET_SUCCESS;
	}
	ccmDb->u1LastTxType = u1Type;
	ccmDb->EcfmDbParams.countTimeBeforeStart=0;

	//adap_parseCcmIssPdu(pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2PduLength,pEcfmHwInfo->unParam.EcfmHwCcTxParams.pu1Pdu,&ccmDb->tPduFromIss);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsTxCcmHandler serviceId:%d\n",ccmDb->EcfmDbParams.enetPacketGenDb.Service_id);
	if(u1Type == ADAP_ECFM_NP_STOP_CCM_TX)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsTxCcmHandler: stop CCM Tx\n");
		if(ccmDb->EcfmDbParams.enetPacketGenDb.active == MEA_TRUE)
		{
			if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId,MEA_FALSE) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to inActive the CCM tx traffic\r\n");
				return ENET_FAILURE;
			}
			ccmDb->EcfmDbParams.enetPacketGenDb.active = MEA_FALSE;
			ccmDb->EcfmDbParams.u1EcfmOffStatus = pEcfmHwInfo->u1EcfmOffStatus;
		}
		if(ccmDb->EcfmDbParams.EcfmHwCcTxParams_valid == MEA_TRUE)
		{
			if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
			{
				for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
				{
					if(adap_FsDeleteHwPacketGenerator(ccmDb,issPortList) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to delete packet gen\r\n");
						return ENET_FAILURE;
					}
				}
			}
			else
			{
				if(adap_FsDeleteHwPacketGenerator(ccmDb,0) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to delete packet gen\r\n");
					return ENET_FAILURE;
				}
			}
		}
		ccmDb->EcfmDbParams.EcfmHwCcTxParams_valid = MEA_FALSE;

	}
	else if( (u1Type == ADAP_ECFM_NP_START_CCM_TX) || (u1Type == ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST) )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsTxCcmHandler: start CCM Tx\n");
		ccmDb->EcfmDbParams.u1EcfmOffStatus = pEcfmHwInfo->u1EcfmOffStatus;

		if(ccmDb->EcfmDbParams.EcfmHwCcTxParams_valid == MEA_FALSE)
		{
			adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwCcTxParams,&pEcfmHwInfo->unParam.EcfmHwCcTxParams,sizeof(tEnetHal_EcfmHwCcTxParams));
			ccmDb->EcfmDbParams.EcfmHwCcTxParams_valid = MEA_TRUE;
		}
		else
		{
			if(ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex != pEcfmHwInfo->unParam.EcfmHwCcTxParams.u4IfIndex)
			{
				// tx IfIndex is changed
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex = pEcfmHwInfo->unParam.EcfmHwCcTxParams.u4IfIndex;
			}
		}
		if(ccmDb->EcfmDbParams.EcfmHwMepParams_valid == MEA_FALSE)
		{
			adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwMepParams,&pEcfmHwInfo->EcfmHwMepParams,sizeof(tEnetHal_EcfmHwMepParams));
			ccmDb->EcfmDbParams.EcfmHwMepParams_valid = MEA_TRUE;
		}
		else
		{

			if(adap_memcmp(&ccmDb->EcfmDbParams.EcfmHwMepParams.DstMacAddr,&pEcfmHwInfo->EcfmHwMepParams.DstMacAddr,sizeof(tEnetHal_MacAddr)) != 0)
			{
				//dest mac address is changed
				adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwMepParams.DstMacAddr,&pEcfmHwInfo->EcfmHwMepParams.DstMacAddr,sizeof(tEnetHal_MacAddr));
				param_change=MEA_TRUE;
			}
			if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel != pEcfmHwInfo->EcfmHwMepParams.u1MdLevel)
			{
				// md level is change
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwMepParams.u1MdLevel = pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
			}
			if(ccmDb->EcfmDbParams.EcfmHwMepParams.u2MepId != pEcfmHwInfo->EcfmHwMepParams.u2MepId)
			{
				// mep id change
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwMepParams.u2MepId = pEcfmHwInfo->EcfmHwMepParams.u2MepId;
			}
			if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction != pEcfmHwInfo->EcfmHwMepParams.u1Direction)
			{
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction = pEcfmHwInfo->EcfmHwMepParams.u1Direction;
			}
		}
		if(ccmDb->EcfmDbParams.EcfmHwRMepParams_valid == MEA_FALSE)
		{
			adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwRMepParams,&pEcfmHwInfo->EcfmHwRMepParams,sizeof(tEnetHal_EcfmHwRMepParams));
			ccmDb->EcfmDbParams.EcfmHwRMepParams_valid = MEA_TRUE;
		}
		else
		{
			if(ccmDb->EcfmDbParams.EcfmHwRMepParams.u1MdLevel != pEcfmHwInfo->EcfmHwRMepParams.u1MdLevel)
			{
				// mdLvl change
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwRMepParams.u1MdLevel = pEcfmHwInfo->EcfmHwRMepParams.u1MdLevel;
			}
			if(ccmDb->EcfmDbParams.EcfmHwRMepParams.u2MepId != pEcfmHwInfo->EcfmHwRMepParams.u2MepId)
			{
				// meId change
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwRMepParams.u2MepId = pEcfmHwInfo->EcfmHwRMepParams.u2MepId;
			}
			if(ccmDb->EcfmDbParams.EcfmHwRMepParams.u2RMepId != pEcfmHwInfo->EcfmHwRMepParams.u2RMepId)
			{
				// remote mepId
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwRMepParams.u2RMepId = pEcfmHwInfo->EcfmHwRMepParams.u2RMepId;
			}
			if(ccmDb->EcfmDbParams.EcfmHwRMepParams.u1Direction !=  pEcfmHwInfo->EcfmHwRMepParams.u1Direction)
			{
				// Up/Down Mep change
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwRMepParams.u1Direction =  pEcfmHwInfo->EcfmHwRMepParams.u1Direction;
			}
		}
		if(ccmDb->EcfmHwMaParams_valid == MEA_TRUE)
		{
			if(ccmDb->EcfmDbParams.enetPacketGenDb.ccm_interval != pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval)
			{
				ccmDb->EcfmDbParams.enetPacketGenDb.ccm_interval = pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval;
				ccmDb->EcfmHwMaParams.u4CcmInterval = pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval;
				param_change=MEA_TRUE;
			}
			if(ccmDb->EcfmHwMaParams.u4VlanIdIsid != pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid)
			{
				ccmDb->EcfmHwMaParams.u4VlanIdIsid = pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
				param_change=MEA_TRUE;
			}
		}
		if( (ccmDb->EcfmDbParams.EcfmHwCcTxParams_valid == MEA_TRUE) &&
			(ccmDb->EcfmDbParams.EcfmHwMepParams_valid == MEA_TRUE) &&
			(ccmDb->EcfmDbParams.EcfmHwRMepParams_valid == MEA_TRUE) )
		{
			if(param_change == MEA_TRUE)
			{
				if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
				{
					for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
					{
						//delete
						if(adap_FsDeleteHwPacketGenerator(ccmDb,issPortList) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to delete packet gen\r\n");
							return ENET_FAILURE;
						}
					}
				}
				else
				{
					if(adap_FsDeleteHwPacketGenerator(ccmDb,0) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to delete packet gen\r\n");
						return ENET_FAILURE;
					}
				}

				ccmDb->EcfmDbParams.packetGenParams_active = MEA_FALSE;
			}
			if( (ccmDb->EcfmDbParams.enetPacketGenDb.active == MEA_FALSE) &&
				(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId != UNDEFINED_VALUE16) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsTxCcmHandler: CCM tx traffic restart\r\n");
				if(adap_FsMiHwPacketGenActive(ccmDb->EcfmDbParams.enetPacketGenDb.packetGenId,MEA_TRUE) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed to Activate the CCM tx traffic\r\n");
					return ENET_FAILURE;
				}
				ccmDb->EcfmDbParams.enetPacketGenDb.active = MEA_TRUE;
			}
			else if(ccmDb->EcfmDbParams.packetGenParams_active == MEA_FALSE)
			{
				ccmDb->EcfmDbParams.packetGenParams_active = MEA_TRUE;
				//create packet generator
				if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
				{
					for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
					{
						if(adap_FsCreateHwPacketGenerator(ccmDb,issPortList) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed adap_FsCreateHwPacketGenerator\r\n");
							return ENET_FAILURE;
						}
					}
				}
				else
				{
					if(adap_FsCreateHwPacketGenerator(ccmDb,0) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: failed adap_FsCreateHwPacketGenerator\r\n");
						return ENET_FAILURE;
					}
				}
			}
		}
	}
	return ENET_SUCCESS;

}

static const char *adap_getCCmStringFromType(ADAP_Uint8 u1Type)
{
	switch(u1Type)
	{
	case ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST: return "ECFM_NP_START_CCM_TX_ON_PORTLIST";
	case ADAP_ECFM_NP_START_CCM_TX: return "ECFM_NP_START_CCM_TX";
	case ADAP_ECFM_NP_STOP_CCM_TX: return "ECFM_NP_STOP_CCM_TX";
	case ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST: return "ECFM_NP_START_CCM_RX_ON_PORTLIST";
	case ADAP_ECFM_NP_STOP_CCM_RX: return "ECFM_NP_STOP_CCM_RX";
	case ADAP_ECFM_NP_START_CCM_RX: return "ECFM_NP_START_CCM_RX";
	case ADAP_ECFM_NP_MA_CREATION: return "ECFM_NP_MA_CREATION";
	case ADAP_ECFM_NP_MA_DELETION: return "ECFM_NP_MA_DELETION";
	case ADAP_ECFM_NP_OFFLOAD_STATE: return "ECFM_NP_OFFLOAD_STATE";
	default:	return "unknown type";
	}

}

ADAP_Uint32 adap_delete_rmtSide_Configuration(tAdap_RmtSide *pConfig)
{
	ADAP_Uint32 ret=ENET_SUCCESS;
	ADAP_Uint32 num_of_try;
	MEA_Action_t get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t enetPort;


	for(num_of_try=0;num_of_try<3;num_of_try++)
	{
		MeaAdapGetPhyPortFromLogPort(pConfig->srcPort,&enetPort);
		if(adap_FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->VpnIndex,pConfig->megLevel,pConfig->opcode,
				enetPort,&get_paction_id ) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_delete_rmtSide_Configuration ERROR: failed by delete FsMiHwDeleteRxForwarder num of try:%d\n",num_of_try);
		}
		else
		{
			break;
		}
	}
	adap_usSleepFunc(20*1000);
	if(pConfig->action_id != UNDEFINED_VALUE16)
	{
		if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0,pConfig->action_id) != MEA_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_delete_rmtSide_Configuration ERROR: action id %d not valid realActionId:%d\n", pConfig->action_id,get_paction_id);
			ret = ENET_FAILURE;
		}
		else if(ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,pConfig->action_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_delete_rmtSide_Configuration ERROR: failed by delete action id %d real actionId:%d\n",
										pConfig->action_id,get_paction_id);
			ret = ENET_FAILURE;
		}
	}

	pConfig->action_id = UNDEFINED_VALUE16;

	return ret;
}

static MEA_Status adap_FsMiHwGetRxForwarder(ADAP_Uint16 vlanId,ADAP_Uint16 VpnIndex, ADAP_Uint8 u1MdLevel,MEA_Uint32 op_code,MEA_Uint32 Src_port)
{
	MEA_SE_Entry_key_dbt key;
	MEA_SE_Entry_dbt     entry;

	adap_memset(&key,0,sizeof(MEA_SE_Entry_key_dbt));
	key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
	key.oam_vpn.label_1 = vlanId;
	key.oam_vpn.VPN = VpnIndex;
	key.oam_vpn.MD_level = u1MdLevel;
	key.oam_vpn.op_code = op_code; //D_CCM_OPCODE_VALUE;
	key.oam_vpn.packet_is_untag_mpls = 0;
	key.oam_vpn.Src_port = Src_port;

	adap_memcpy(&entry.key,&key,sizeof(MEA_SE_Entry_key_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteRxForwarder ===>entry not found vlanId:%d VpnIndex:%d u1MdLevel:%d op_code:%d srcPort:%d!!!\n",
								vlanId,VpnIndex,u1MdLevel,op_code,Src_port);

		return MEA_ERROR;
	}
	return MEA_OK;
}

ADAP_Uint32 adap_create_rmtSide_Configuration(tAdap_RmtSide *pConfig,MEA_Bool send_to_cpu)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	tRmtSrvInfo srvInfo;
	MEA_Ingress_TS_type_t 	type=MEA_INGGRESS_TS_TYPE_NONE;
	MEA_Action_t 			get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t				enetPort;


	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info[0],0,sizeof(MEA_EHP_Info_dbt)*2);
	adap_memset(&srvInfo,0,sizeof(tRmtSrvInfo));

	srvInfo.EnetPolilcingProfile = pConfig->policerId; // if Index
	srvInfo.b_lmEnable=MEA_FALSE;
	srvInfo.lmId=0;
	srvInfo.keyType=MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
	srvInfo.me_valid=MEA_FALSE;
	srvInfo.me_level=0;
	srvInfo.vlanId = pConfig->vlanId;
	srvInfo.vpn = pConfig->VpnIndex;
	srvInfo.src_port = pConfig->srcPort;

	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

	if(send_to_cpu == MEA_TRUE)
	{
		tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	}

	if(pConfig->lmEnable == ADAP_TRUE)
	{
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.LmId = pConfig->lmId;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_dasa=1;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_SWAP_OAM;

		if(pConfig->opcode == ADAP_D_TST_OPCODE_VALUE)
		{
			tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_TRANSPARENT;
		}
		tEhp_info[num_of_entries].ehp_data.martini_info.DA.b[5]=0x01;
		tEhp_info[num_of_entries].ehp_data.martini_info.SA.b[5]=0x01;

	}
	if(send_to_cpu == MEA_TRUE)
	{
		MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,127);
		MEA_SET_OUTPORT(&tOutPorts, 127);

	}
	else
	{
		MeaAdapGetPhyPortFromLogPort(pConfig->srcPort, &enetPort);
		MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,enetPort);
		MEA_SET_OUTPORT(&tOutPorts, enetPort);
	}



	num_of_entries++;

	// create action
	// MEA action set create -f 1033 -pm 1 103 -ed 1 0 -h 81000001 0 0 3 -lmid 1 1 0 3
	if(pConfig->opcode ==ADAP_D_DMM_OPCODE_VALUE)
	{
		type=MEA_INGGRESS_TS_TYPE_ENABLE;
	}

	MeaAdapGetPhyPortFromLogPort(pConfig->srcPort, &enetPort);
	// check if stillfound in the forwarder
	if(adap_FsMiHwGetRxForwarder(pConfig->vlanId,pConfig->VpnIndex,pConfig->megLevel,pConfig->opcode,enetPort) == MEA_OK)
	{
		adap_FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->VpnIndex,pConfig->megLevel,pConfig->opcode,enetPort,&get_paction_id);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_create_rmtSide_Configuration old entry still found in the forwarder opcode:%d try to delete with actionId:%d\n",
								pConfig->opcode,
								get_paction_id);
	}

	pConfig->action_id = MEA_PLAT_GENERATE_NEW_ID;
	if(adap_FsMiHwCreateCfmAction(&pConfig->action_id,
								&tEhp_info[0],
								num_of_entries,
								&tOutPorts,
								type) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_create_rmtSide_Configuration failed FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"call to adap_FsMiHwCreateRxForwarder with opcode:%d\n",pConfig->opcode);
	if(adap_FsMiHwCreateRxForwarder(pConfig->action_id,
								pConfig->VpnIndex,
								&tOutPorts,
								pConfig->megLevel,
								pConfig->opcode,//D_DMM_OPCODE_VALUE,
								pConfig->vlanId,
								enetPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_create_rmtSide_Configuration failed FsMiHwCreateRxForwarder\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action_id:%d vpnId:%d\n",pConfig->action_id,pConfig->VpnIndex);


	pConfig->bConfigured=MEA_TRUE;

	return MEA_OK;
}

static ADAP_Uint32 adap_create_apsFilter_Configuration(tAdap_RmtSide *pConfig)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Port_t			enetPort;


	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	MeaAdapGetPhyPortFromLogPort(pConfig->srcPort, &enetPort);

	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = enetPort;

	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,127);
	MEA_SET_OUTPORT(&tOutPorts, 127);

	num_of_entries++;

	if(adap_FsMiHwCreateCfmAction(&pConfig->action_id,
								&tEhp_info[0],
								num_of_entries,
								&tOutPorts,
								MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_create_apsFilter_Configuration failed adap_FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}

	if(adap_FsMiHwCreateRxForwarder(pConfig->action_id,
								pConfig->VpnIndex,
								&tOutPorts,
								pConfig->megLevel,
								pConfig->opcode,//D_DMM_OPCODE_VALUE,
								pConfig->vlanId,
								enetPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_create_apsFilter_Configuration failed adap_FsMiHwCreateRxForwarder\n");
		return MEA_ERROR;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"action_id:%d vpnId:%d\n",pConfig->action_id,pConfig->VpnIndex);
	pConfig->bConfigured=MEA_TRUE;

	return MEA_OK;
}

static ADAP_Uint32 adap_FsRemoteSidePacketSwap(tEnetHal_EcfmHwParams *pEcfmHwInfo,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Type)
{
	tAdap_InterfaceRemoteSide *pRemoteSide=NULL;
	ADAP_Uint16 VpnIndex=0;
	MEA_Bool send_to_cpu;

	pRemoteSide = adap_getIntRemoteSide(u4IfIndex);


	if(pRemoteSide == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: get null pointer\n");
		return ENET_FAILURE;
	}
	if(pRemoteSide->tLbrDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tLbrDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tLbrDatabase.bConfigured = MEA_FALSE;
	}
	if(pRemoteSide->tLbmDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tLbmDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tLbmDatabase.bConfigured = MEA_FALSE;
	}
	if(pRemoteSide->tTstDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tTstDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tTstDatabase.bConfigured = ADAP_FALSE;
	}

	if(pRemoteSide->tDmrDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tDmrDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tDmrDatabase.bConfigured = ADAP_FALSE;
	}
	if(pRemoteSide->tLmrDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tLmrDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		adap_freeLmId(pRemoteSide->tLmrDatabase.lmId);
		pRemoteSide->tLmrDatabase.bConfigured = ADAP_FALSE;
	}
	if(pRemoteSide->tApsDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tApsDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tApsDatabase.bConfigured = ADAP_FALSE;
	}
#ifdef RPS_USE_FORWARDER_KEY
	if(pRemoteSide->tRpsDatabase.bConfigured == ADAP_TRUE)
	{
		delete_rmtErpsSide_Configuration(&pRemoteSide->tRpsDatabase);
		pRemoteSide->tRpsDatabase.bConfigured = ADAP_FALSE;
	}
#endif
	if(pRemoteSide->t1DmDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->t1DmDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->t1DmDatabase.bConfigured = ADAP_FALSE;
	}
	if(pRemoteSide->t1LtmDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->t1LtmDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->t1LtmDatabase.bConfigured = ADAP_FALSE;
	}
	if(pRemoteSide->t1LtrDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->t1LtrDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->t1LtrDatabase.bConfigured = ADAP_FALSE;
	}
	if(pRemoteSide->tAisDatabase.bConfigured == ADAP_TRUE)
	{
		if(adap_delete_rmtSide_Configuration(&pRemoteSide->tAisDatabase) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete remote configuration\n");
		}
		pRemoteSide->tAisDatabase.bConfigured = ADAP_FALSE;
	}

	if(u1Type == ADAP_ECFM_NP_STOP_CCM_RX)
	{
		return ENET_SUCCESS;
	}

	VpnIndex = adap_GetVpnByVlan (pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid);
	if (VpnIndex == ADAP_END_OF_TABLE)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap FAIL: adap_GetVpnByVlan failed for VlanId:%d\n", pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid);
	   return ENET_FAILURE;
	}
	pRemoteSide->tLbrDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tLbrDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tLbrDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tLbrDatabase.srcPort = u4IfIndex;
	pRemoteSide->tLbrDatabase.lmEnable=MEA_FALSE;
	pRemoteSide->tLbrDatabase.lmId=0;
	pRemoteSide->tLbrDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tLbrDatabase.opcode=ADAP_D_LBM_OPCODE_VALUE;
	send_to_cpu=MEA_TRUE;
#ifdef LBM_DONE_BY_ENET
	pRemoteSide->tLbrDatabase.lmEnable=MEA_TRUE;
	send_to_cpu=MEA_FALSE;
#endif

	if(adap_create_rmtSide_Configuration(&pRemoteSide->tLbrDatabase,send_to_cpu) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}
#ifndef LBM_DONE_BY_ENET
	pRemoteSide->tLbmDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tLbmDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tLbmDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tLbmDatabase.srcPort = u4IfIndex;
	pRemoteSide->tLbmDatabase.lmEnable=MEA_FALSE;
	pRemoteSide->tLbmDatabase.lmId=0;
	pRemoteSide->tLbmDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tLbmDatabase.opcode=ADAP_D_LBR_OPCODE_VALUE;
	send_to_cpu=MEA_TRUE;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->tLbmDatabase,send_to_cpu) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}
#endif
	pRemoteSide->tTstDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tTstDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tTstDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tTstDatabase.srcPort = u4IfIndex;
	pRemoteSide->tTstDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->tTstDatabase.lmId=0;
	pRemoteSide->tTstDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	if(adap_is_mac_multicast(&pEcfmHwInfo->EcfmHwMepParams.DstMacAddr))
	{
		pRemoteSide->tTstDatabase.megLevel |= 0x08;
	}
	pRemoteSide->tTstDatabase.opcode=ADAP_D_TST_OPCODE_VALUE;
	send_to_cpu=MEA_FALSE;

	if(adap_create_rmtSide_Configuration(&pRemoteSide->tTstDatabase,send_to_cpu) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}


	pRemoteSide->tDmrDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tDmrDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tDmrDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tDmrDatabase.srcPort = u4IfIndex;
	pRemoteSide->tDmrDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->tDmrDatabase.lmId=adap_allocateLmId(pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid,u4IfIndex);
	pRemoteSide->tDmrDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tDmrDatabase.opcode=ADAP_D_DMM_OPCODE_VALUE;
	//zxzxcvzxcvz;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->tDmrDatabase,MEA_FALSE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}

	pRemoteSide->tApsDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tApsDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tApsDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tApsDatabase.srcPort = u4IfIndex;
	pRemoteSide->tApsDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->tApsDatabase.lmId=0;
	pRemoteSide->tApsDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tApsDatabase.opcode=ADAP_D_APS_OPCODE_VALUE;
	if(adap_create_apsFilter_Configuration(&pRemoteSide->tApsDatabase) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_apsFilter_Configuration\n");
		return ENET_FAILURE;
	}
#ifdef RPS_USE_FORWARDER_KEY
	pRemoteSide->tRpsDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tRpsDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tRpsDatabase.policerId=DPLGetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tRpsDatabase.srcPort = u4IfIndex;
	pRemoteSide->tRpsDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->tRpsDatabase.lmId=0;
	pRemoteSide->tRpsDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tRpsDatabase.opcode=D_RAPS_OPCODE_VALUE;
	if(create_apsErpsFilter_Configuration(&pRemoteSide->tRpsDatabase) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_apsFilter_Configuration\n");
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"create D_RAPS_OPCODE_VALUE forwarder\n");
#else
	adap_FsMiVlanHwLacpLxcpApsUpdate (ADAP_TRUE,u4IfIndex);
#endif
	pRemoteSide->t1DmDatabase.VpnIndex=VpnIndex;
	pRemoteSide->t1DmDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->t1DmDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->t1DmDatabase.srcPort = u4IfIndex;
	pRemoteSide->t1DmDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->t1DmDatabase.lmId=0;
	pRemoteSide->t1DmDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->t1DmDatabase.opcode=ADAP_D_1DM_OPCODE_VALUE;
	if(adap_create_apsFilter_Configuration(&pRemoteSide->t1DmDatabase) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_apsFilter_Configuration\n");
		return ENET_FAILURE;
	}


	pRemoteSide->tLmrDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tLmrDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tLmrDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tLmrDatabase.srcPort = u4IfIndex;
	pRemoteSide->tLmrDatabase.lmEnable=MEA_TRUE;
	pRemoteSide->tLmrDatabase.lmId=adap_allocateLmId(pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid,u4IfIndex);
	pRemoteSide->tLmrDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->tLmrDatabase.opcode=ADAP_D_LMM_OPCODE_VALUE;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->tLmrDatabase,MEA_FALSE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}

	pRemoteSide->t1LtmDatabase.VpnIndex=VpnIndex;
	pRemoteSide->t1LtmDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->t1LtmDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->t1LtmDatabase.srcPort = u4IfIndex;
	pRemoteSide->t1LtmDatabase.lmEnable=MEA_FALSE;
	pRemoteSide->t1LtmDatabase.lmId=0;
	pRemoteSide->t1LtmDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel + 0x08;
	pRemoteSide->t1LtmDatabase.opcode=ADAP_D_LTM_OPCODE_VALUE;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->t1LtmDatabase,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}

	pRemoteSide->t1LtrDatabase.VpnIndex=VpnIndex;
	pRemoteSide->t1LtrDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->t1LtrDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->t1LtrDatabase.srcPort = u4IfIndex;
	pRemoteSide->t1LtrDatabase.lmEnable=MEA_FALSE;
	pRemoteSide->t1LtrDatabase.lmId=0;
	pRemoteSide->t1LtrDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel;
	pRemoteSide->t1LtrDatabase.opcode=ADAP_D_LTR_OPCODE_VALUE;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->t1LtrDatabase,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}
	pRemoteSide->tAisDatabase.VpnIndex=VpnIndex;
	pRemoteSide->tAisDatabase.vlanId=pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
	pRemoteSide->tAisDatabase.policerId=adap_GetDefaultPolicerId(u4IfIndex);
	pRemoteSide->tAisDatabase.srcPort = u4IfIndex;
	pRemoteSide->tAisDatabase.lmEnable=MEA_FALSE;
	pRemoteSide->tAisDatabase.lmId=0;
	pRemoteSide->tAisDatabase.megLevel=pEcfmHwInfo->EcfmHwMepParams.u1MdLevel + 0x08;
	pRemoteSide->tAisDatabase.opcode=ADAP_D_AIS_OPCODE_VALUE;
	if(adap_create_rmtSide_Configuration(&pRemoteSide->tAisDatabase,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRemoteSidePacketSwap ERROR: adap_create_rmtSide_Configuration\n");
		return ENET_FAILURE;
	}

	if(adap_MiVlanSetServiceLmId (pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid,
									u4IfIndex,
									D_TAGGED_SERVICE_TAG,
									MEA_TRUE,
									pRemoteSide->tLmrDatabase.lmId,MEA_FALSE) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call DPL_MiVlanSetServiceLmId\n");
		return MEA_ERROR;
	}

	if(adap_MiVlanSetPartnerServiceLmId (pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid,
								u4IfIndex,
								D_TAGGED_SERVICE_TAG,MEA_TRUE,
								pRemoteSide->tLmrDatabase.lmId)	 != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call DPL_MiVlanSetServiceLmId\n");
		return MEA_ERROR;
	}

	return ENET_SUCCESS;
}

static ADAP_Uint32 adap_FsCreatePacketAnalyzer(tAdap_ECFMCcmInst *ccmDb,int entry_num)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 VpnIndex=0;
	int expected_period=0;
	MEA_Uint32  MD_level=0;
	ADAP_Uint32 InternalVlanId=0;
	MEA_Port_t	enetPort;

    VpnIndex = adap_GetVpnByVlan (ccmDb->EcfmHwMaParams.u4VlanIdIsid);

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsCreatePacketAnalyzer vpnId:%d\n",VpnIndex);

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info[0],0,sizeof(MEA_EHP_Info_dbt)*2);

	if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) )
	{
		tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;

		InternalVlanId = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
	}
	else
	{
		InternalVlanId = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
		tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;

	}

	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;


	if(ccmDb->EcfmHwMaParams.u4CcmInterval == 1000000)
	{
		expected_period=ADAP_D_CCM_1SEC_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 10000)
	{
		expected_period=ADAP_D_CCM_10MS_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 10000000)
	{
		expected_period=ADAP_D_CCM_10SEC_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 100000)
	{
		expected_period=ADAP_D_CCM_100MS_PERIOD; // 8 bits
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 60000000)
	{
		expected_period=ADAP_D_CCM_1MIN_PERIOD;
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 600000000)
	{
		expected_period=ADAP_D_CCM_10MIN_PERIOD;
	}
	else if(ccmDb->EcfmHwMaParams.u4CcmInterval == 3000)
	{
		expected_period=ADAP_D_CCM_3_3MS_PERIOD; // 8 bits
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: unknown interval:%d\r\n",ccmDb->EcfmHwMaParams.u4CcmInterval);
		return ENET_FAILURE;
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.ccm_interval = ccmDb->EcfmHwMaParams.u4CcmInterval;
		// create analyzer
		// MEA analyzer CCM set create 1 -mep_id 2222 -time_event 1 -clear_segid 1 0 -period_id 1
		if(adap_FsMiHwCreateCcmAnalyzer(&ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u2RMepId,
								expected_period,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u4StartSeqNo,
								(char *)ccmDb->EcfmHwMaParams.au1MAID) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create analyzer profile\r\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		ccmDb->EcfmDbParams.enetPacketAnaDb.ccm_interval = ccmDb->EcfmHwMaParams.u4CcmInterval;
		// create analyzer
		// MEA analyzer CCM set create 1 -mep_id 2222 -time_event 1 -clear_segid 1 0 -period_id 1
		if(adap_FsMiHwCreateCcmAnalyzer(&ccmDb->EcfmDbParams.enetPacketAnaDb.id_io,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u2RMepId,
								expected_period,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u4StartSeqNo,
								(char *)ccmDb->EcfmHwMaParams.au1MAID) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create analyzer profile\r\n");
			return ENET_FAILURE;
		}
	}
	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,ADAP_D_ANALYZER_ETHERNITY_PORT);
	MEA_SET_OUTPORT(&tOutPorts, ADAP_D_ANALYZER_ETHERNITY_PORT);
	MEA_SET_OUTPORT(&tOutPorts, 127);

#if 0
	if( (DPLGetBridgeMode() == VLAN_PROVIDER_CORE_BRIDGE_MODE) || (DPLGetBridgeMode() == VLAN_PROVIDER_EDGE_BRIDGE_MODE) )
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ECFM_NP_MP_DIR_UP)
		{
			tEhp_info[num_of_entries].ehp_data.atm_info.val.double_vlan_tag.vid      = ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io;
		}
		else
		{
			tEhp_info[num_of_entries].ehp_data.atm_info.val.double_vlan_tag.vid      = ccmDb->EcfmDbParams.enetPacketAnaDb.id_io;

		}
		num_of_entries++;
		tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
		MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,127);
		num_of_entries++;
	}
	else
#endif
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.id_io;

			num_of_entries++;
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
			tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = enetPort;

		}
		else
		{
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmDbParams.enetPacketAnaDb.id_io;

			num_of_entries++;
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex, &enetPort);
			tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
			tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = enetPort;
		}
		tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,127);
		num_of_entries++;


	}

	// create action
	// MEA action set create -f 1033 -pm 1 103 -ed 1 0 -h 81000001 0 0 3 -lmid 1 1 0 3
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu,&tEhp_info[0],num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create action\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,&tEhp_info[0],num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create action\n");
			return ENET_FAILURE;
		}
	}
	MD_level = ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u1MdLevel;
	if(adap_is_mac_multicast(&ccmDb->EcfmDbParams.EcfmHwMepParams.DstMacAddr))
	{
		MD_level |= 0x08;
	}
	if(ccmDb->EcfmHwMaParams.u4CcmInterval >= 1000000)
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu,
										VpnIndex,
										&tOutPorts,MD_level,
										ADAP_D_CCM_OPCODE_VALUE,
										InternalVlanId,
										enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder: failed to create forwarder\r\n");
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateRxForwarder port_id:%d\r\n",
									ccmDb->EcfmDbParams.u4PortArray[entry_num]);
		}
		else
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex, &enetPort);
			// MEA forwarder add 4 2 102 2 1 0 3 1 0 1 120 -action 1 1033
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,
										VpnIndex,
										&tOutPorts,MD_level,
										ADAP_D_CCM_OPCODE_VALUE,
										InternalVlanId,
										enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsCreatePacketAnalyzer: failed to create forwarder\r\n");
				return ENET_FAILURE;
			}
		}
	}
	num_of_entries=1;
	MEA_CLEAR_OUTPORT(&tEhp_info[0].output_info,127);
	MEA_CLEAR_OUTPORT(&tOutPorts,127);
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana,&tEhp_info[0],num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create action\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.ifIndex	= ccmDb->EcfmDbParams.EcfmHwCcRxParams.PortList.pu4PortArray[entry_num];
	}
	else
	{
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,&tEhp_info[0],num_of_entries,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsCreatePacketAnalyzer: failed to create action\n");
			return ENET_FAILURE;
		}
		ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex	= ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex;
		//ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu=MEA_TRUE;
	}
	if(ccmDb->EcfmHwMaParams.u4CcmInterval >= 1000000)
	{
		ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu=PACKET_ANALYZER_INIT;
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu=PACKET_ANALYZER_INIT;
	}
	else
	{
		ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu=PACKET_ANALYZER_INIT;
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu=PACKET_ANALYZER_INIT;

		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana,
										VpnIndex,
										&tOutPorts,MD_level,
										ADAP_D_CCM_OPCODE_VALUE,
										InternalVlanId,
										enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder: failed to create forwarder\r\n");
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsMiHwCreateRxForwarder port_id:%d\r\n",
									ccmDb->EcfmDbParams.u4PortArray[entry_num]);
		}
		else
		{
			MeaAdapGetPhyPortFromLogPort(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex, &enetPort);
			// MEA forwarder add 4 2 102 2 1 0 3 1 0 1 120 -action 1 1033
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,
										VpnIndex,
										&tOutPorts,MD_level,
										ADAP_D_CCM_OPCODE_VALUE,
										InternalVlanId,
										enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsCreatePacketAnalyzer: failed to create forwarder\r\n");
				return ENET_FAILURE;
			}
		}
	}
	ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpuTime=0;
	ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpuTime=0;
	ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex = VpnIndex;
	ccmDb->EcfmDbParams.u1MdLevel = MD_level; //ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo->u1MdLevel;
	ccmDb->EcfmDbParams.enetCcmAppDb.sendLossCCm=0;
	ccmDb->EcfmDbParams.enetCcmAppDb.sendLossCCmThreshould=ADAP_E_COUNT_TIME_BEFORE_START_CCM;


	ccmDb->EcfmDbParams.tSubEntry[entry_num].enetCcmAppDb.sendLossCCm=0;
	ccmDb->EcfmDbParams.tSubEntry[entry_num].enetCcmAppDb.sendLossCCmThreshould=ADAP_E_COUNT_TIME_BEFORE_START_CCM;

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
			adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.u4PortArray[entry_num],
									D_TAGGED_SERVICE_TAG,
									MEA_TRUE,
									MD_level);

			adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
									ccmDb->EcfmDbParams.u4PortArray[entry_num],
									D_PROVIDER_SERVICE_TAG,
									MEA_TRUE,
									MD_level);
	}
	else
	{
		adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex,
								D_TAGGED_SERVICE_TAG,
								MEA_TRUE,
								MD_level);

		adap_MiVlanSetServiceMdLevel (ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex,
								D_PROVIDER_SERVICE_TAG,
								MEA_TRUE,
								MD_level);
	}
	ccmDb->EcfmDbParams.packetAnaParams_active = MEA_TRUE;
	return ENET_SUCCESS;

}

static ADAP_Uint32 adap_FsRxCcmHandler(ADAP_Uint8 u1Type,tEnetHal_EcfmHwParams * pEcfmHwInfo,MEA_Bool *pConfigured)
{
	ADAP_Uint32 idx;
	tAdap_ECFMCcmInst	*ccmDb;
	MEA_Bool param_change=MEA_FALSE;
	ADAP_Int32 issPortList;

	(*pConfigured) = MEA_FALSE;

	idx = adap_getIndexByMaStrCcmDb((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID);
	if(idx== UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsTxCcmHandler: ma:%s not found\r\n",pEcfmHwInfo->EcfmHwMaParams.au1MAID);
		return ENET_FAILURE;
	}
	ccmDb = adap_getDatabaseCcmDbFromIndex(idx);
	if(ccmDb == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsRxCcmHandler: ccm db not found\r\n");
		return ENET_FAILURE;
	}

	if( ( (ccmDb->u1LastRxType == ADAP_ECFM_NP_START_CCM_RX) && (u1Type == ADAP_ECFM_NP_START_CCM_RX) ) ||
		( (ccmDb->u1LastRxType == ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST) && (u1Type == ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST) ) )
	{
		// allready configured
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"rx allready started:%d\n",ccmDb->EcfmDbParams.EcfmHwMepParams.u2MepId);
		ccmDb->EcfmDbParams.EcfmHwCcRxParams.u2RxFilterId = pEcfmHwInfo->unParam.EcfmHwCcRxParams.u2RxFilterId;
		if(pEcfmHwInfo->EcfmHwRMepParams.u2RMepId != 0)
		{
			ccmDb->EcfmDbParams.EcfmHwRMepParams.u2RMepId = pEcfmHwInfo->EcfmHwRMepParams.u2RMepId;
		}
		ccmDb->EcfmDbParams.packetAnaParams_active = MEA_TRUE;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"==================> the type was:%d and now is:%d\n",ccmDb->u1LastRxType,u1Type);
	ccmDb->u1LastRxType = u1Type;

	ccmDb->EcfmDbParams.countTimeBeforeStart=0;
	if(pEcfmHwInfo->EcfmHwRMepParams.u2RMepId != 0)
	{
		ccmDb->EcfmDbParams.EcfmHwRMepParams.u2RMepId = pEcfmHwInfo->EcfmHwRMepParams.u2RMepId;
	}
	if(u1Type == ADAP_ECFM_NP_STOP_CCM_RX)
	{
		if(ccmDb->EcfmDbParams.packetAnaParams_active == MEA_TRUE)
		{
				//delete packet analyzer
			(*pConfigured) = MEA_TRUE;
			if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
			{
				for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
				{
					adap_FsDeletePacketAnalyzer(ccmDb,issPortList);
				}
			}
			else
			{
				adap_FsDeletePacketAnalyzer(ccmDb,0);
			}
			ccmDb->EcfmDbParams.packetAnaParams_active = MEA_FALSE;
		}
	}
	else if( (u1Type == ADAP_ECFM_NP_START_CCM_RX) || (u1Type == ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST) )
	{
		if(ccmDb->EcfmDbParams.EcfmHwCcRxParams_valid == MEA_FALSE)
		{
			adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwCcRxParams,&pEcfmHwInfo->unParam.EcfmHwCcRxParams,sizeof(tEnetHal_EcfmHwCcRxParams));
			if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
			{
				ccmDb->EcfmDbParams.i4Length=pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.i4Length;
				for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.i4Length;issPortList++)
				{
					ccmDb->EcfmDbParams.u4PortArray[issPortList] = pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.pu4PortArray[issPortList];
				}
			}
		}
		else
		{
			ccmDb->EcfmDbParams.EcfmHwCcRxParams.u2RxFilterId = pEcfmHwInfo->unParam.EcfmHwCcRxParams.u2RxFilterId;
			if(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex != pEcfmHwInfo->unParam.EcfmHwCcRxParams.u4IfIndex)
			{
				param_change=MEA_TRUE;
				ccmDb->EcfmDbParams.EcfmHwCcRxParams.u4IfIndex = pEcfmHwInfo->unParam.EcfmHwCcRxParams.u4IfIndex;
			}
		}
		if(ccmDb->EcfmHwMaParams_valid == MEA_TRUE)
		{
			if(ccmDb->EcfmDbParams.enetPacketAnaDb.ccm_interval != pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval)
			{
				param_change=MEA_TRUE;
				ccmDb->EcfmHwMaParams.u4CcmInterval = pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval;
				ccmDb->EcfmDbParams.enetPacketAnaDb.ccm_interval = pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval;
			}
			if(ccmDb->EcfmHwMaParams.u4VlanIdIsid != pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid)
			{
				ccmDb->EcfmHwMaParams.u4VlanIdIsid = pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid;
				param_change=MEA_TRUE;
			}
		}
		if(ccmDb->EcfmDbParams.packetAnaParams_active == MEA_TRUE)
		{
			if(param_change == MEA_TRUE)
			{
				//delete packet analyzer
				(*pConfigured) = MEA_TRUE;
				if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
				{
					for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
					{
						adap_FsDeletePacketAnalyzer(ccmDb,issPortList);
					}
				}
				else
				{
					adap_FsDeletePacketAnalyzer(ccmDb,0);
				}
				ccmDb->EcfmDbParams.packetAnaParams_active = MEA_FALSE;
			}
		}

		if(ccmDb->EcfmDbParams.packetAnaParams_active == MEA_FALSE)
		{
			adap_memcpy(&ccmDb->EcfmDbParams.EcfmHwCcRxParams,&pEcfmHwInfo->unParam.EcfmHwCcRxParams,sizeof(tEnetHal_EcfmHwCcRxParams));
			ccmDb->EcfmDbParams.packetAnaParams_active = MEA_TRUE;

			if(ccmDb->EcfmDbParams.EcfmHwCcRxParams.pEcfmCcRxInfo != NULL)
			{
				(*pConfigured) = MEA_TRUE;
				if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
				{
					for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
					{
						adap_FsCreatePacketAnalyzer(ccmDb,issPortList);
					}
				}
				else
				{
					adap_FsCreatePacketAnalyzer(ccmDb,0);
				}
			}
		}

	}
	return ENET_SUCCESS;
}

static ADAP_Int32 adap_send_loss_of_sync(tEnetHal_EcfmHwParams * pEcfmHwInfo)
{
	tAdap_ECFMCcmInst	*ccmDb;
	ADAP_Int32 				issPortList;
	ADAP_Uint32 			idx;

	idx = adap_getIndexByMaStrCcmDb((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID);
	if(idx== UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_send_loss_of_sync: ma:%s not found\r\n",pEcfmHwInfo->EcfmHwMaParams.au1MAID);
		return ENET_FAILURE;
	}
	ccmDb = adap_getDatabaseCcmDbFromIndex(idx);
	if(ccmDb == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_send_loss_of_sync: ccm db not found\r\n");
		return ENET_FAILURE;
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		for(issPortList=0;issPortList < ccmDb->EcfmDbParams.i4Length;issPortList++)
		{
			adap_FsCallBackEventLossOfCCM(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u2RxFilterId,
										ccmDb->EcfmDbParams.u4PortArray[issPortList]);
			ccmDb->EcfmDbParams.tSubEntry[issPortList].enetCcmAppDb.callback_loss_called = MEA_TRUE;
		}
	}
	else
	{
		adap_FsCallBackEventLossOfCCM(ccmDb->EcfmDbParams.EcfmHwCcRxParams.u2RxFilterId,ccmDb->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex);
		ccmDb->EcfmDbParams.enetCcmAppDb.callback_loss_called = MEA_TRUE;
	}
	ccmDb->EcfmDbParams.packetAnaParams_active = MEA_FALSE;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_freeMaStrCcmDb(char *au1MAID)
{
	ADAP_Int32 idx;


	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if( (tAdap_EcfmCcmDb[idx].valid == MEA_TRUE) &&
			(tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid == MEA_TRUE) &&
			(strncmp((char *)tAdap_EcfmCcmDb[idx].EcfmHwMaParams.au1MAID,au1MAID,48) == 0) )
		{
			tAdap_EcfmCcmDb[idx].valid = MEA_FALSE;
			return idx;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_freeMaStrCcmDb: ma string:%s not found",au1MAID);
	return UNDEFINED_VALUE16;
}

static ADAP_Int32 adap_getIndexByHandler(ADAP_Uint8 *pAu1HwHandler)
{
	ADAP_Int32 idx;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if( (tAdap_EcfmCcmDb[idx].valid == MEA_TRUE) &&
			(tAdap_EcfmCcmDb[idx].EcfmHwMaParams_valid == MEA_TRUE) &&
			(tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[0] == pAu1HwHandler[0] ) &&
			(tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[1] == pAu1HwHandler[1] ) &&
			(tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[2] == pAu1HwHandler[2] ) &&
			(tAdap_EcfmCcmDb[idx].EcfmDbParams.au1HwHandler[3] == pAu1HwHandler[3] ) )
		{
			return idx;
		}
	}

	return UNDEFINED_VALUE16;
}

static ADAP_Uint32 adap_delete_ccm_configuration(tEnetHal_EcfmHwParams * pEcfmHwInfo)
{
	ADAP_Uint32 idx=0;
	tAdap_ECFMCcmInst *pCcmInst=NULL;
	ADAP_Int32 issPortList=0;

	idx = adap_getIndexByHandler(&pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0]);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"idx:%d\n",idx);
	if(idx == UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_delete_ccm_configuration entry not found\n");
		return ENET_FAILURE;
	}
	pCcmInst = adap_getDatabaseCcmDbFromIndex(idx);
	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"pCcmInst:%s\n",pCcmInst==NULL?"exist":"not exist");
	if(pCcmInst == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_delete_ccm_configuration entry is NULL\n");
		return ENET_FAILURE;
	}

	if(pCcmInst->EcfmDbParams.EcfmHwCcTxParams_valid == MEA_TRUE)
	{
		if(pCcmInst->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			for(issPortList=0;issPortList < pCcmInst->EcfmDbParams.i4Length;issPortList++)
			{
				if(adap_FsDeleteHwPacketGenerator(pCcmInst,issPortList) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete packet gen\r\n");
					return ENET_FAILURE;
				}
			}
		}
		else
		{
			if(adap_FsDeleteHwPacketGenerator(pCcmInst,0) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete packet gen\r\n");
				return ENET_FAILURE;
			}
		}
	}


	if(pCcmInst->EcfmDbParams.packetAnaParams_active == MEA_TRUE)
	{
			//delete packet analyzer
		if(pCcmInst->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
		{
			for(issPortList=0;issPortList < pCcmInst->EcfmDbParams.i4Length;issPortList++)
			{
				adap_FsDeletePacketAnalyzer(pCcmInst,issPortList);
			}
		}
		else
		{
			adap_FsDeletePacketAnalyzer(pCcmInst,0);
		}
		pCcmInst->EcfmDbParams.packetAnaParams_active = MEA_FALSE;
	}

	if(pCcmInst->EcfmDbParams.EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
	{
		for(issPortList=0;issPortList < pCcmInst->EcfmDbParams.i4Length;issPortList++)
		{
			adap_FsRemoteSidePacketSwap(pEcfmHwInfo,
									pCcmInst->EcfmDbParams.u4PortArray[issPortList],
									ADAP_ECFM_NP_STOP_CCM_RX);
		}
	}
	else
	{
		adap_FsRemoteSidePacketSwap(pEcfmHwInfo,pCcmInst->EcfmDbParams.EcfmHwCcTxParams.u4IfIndex,ADAP_ECFM_NP_STOP_CCM_RX);
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"end of delete CCM configuration:%s\r\n",pCcmInst->EcfmHwMaParams.au1MAID);
	adap_freeMaStrCcmDb((char *)pCcmInst->EcfmHwMaParams.au1MAID);

	return ENET_SUCCESS;
}





/*****************************************************************************
 * EnetHal_FsEcfmHwGetCcmRxStatistics
 *
 * DESCRIPTION:  This routine gets CCM PDU reception statistics from the
 *               offloaded module
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2RxFilterId - Handle to get Reception statistices from
 *                              offloaded module
 *               pEcfmCcOffMepTxStats - CCM transmission information in
 *                                      offloaded module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : ENET_SUCCESS - On successful set (or)
 *                      ENET_FAILURE - Error during setting
 *****************************************************************************/
ADAP_Int32 EnetHal_FsEcfmHwGetCcmRxStatistics (ADAP_Uint32 u4ContextId,
												ADAP_Uint16 u2RxFilterId,
												tEnetHal_EcfmCcOffMepRxStats * pEcfmCcOffMepRxStats)
{
    ADAP_Uint16 IdxArray[ADAP_D_MAX_DATABASE_CCM_CONFIGURATION];
    ADAP_Uint16 numOfEntries=0;
    ADAP_Uint16 idx;

	numOfEntries = adap_getIndexByRxFilter(u2RxFilterId,&IdxArray[0],ADAP_D_MAX_DATABASE_CCM_CONFIGURATION);

	pEcfmCcOffMepRxStats->u4CcmRx=0;
	for(idx=0;idx<numOfEntries;idx++)
	{
		pEcfmCcOffMepRxStats->u4CcmRx += adap_FsGetRxCcmPacketsFromIndex(IdxArray[idx]);
	}

    return ENET_SUCCESS;

}

/****************************************************************************
 * Function Name      : EnetHal_FsMiEcfmHwCallNpApi
 *
 * Description        : This generic api will call NPAPIs depending on the Type
 *                      passed as an argument.
 *
 * Input(s)           : u1Type - Type of Call to NPAPI
 *                      pEcfmHwInfo - Structure pointer to the
 *                                    structure tEnetHal_EcfmHwParams
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ENET_SUCCESS / ENET_FAILURE
 *****************************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmHwCallNpApi (ADAP_Uint8 u1Type, tEnetHal_EcfmHwParams * pEcfmHwInfo)
{
	ADAP_Uint32 ret = ENET_SUCCESS;
	ADAP_Int32 issPortList;
	MEA_Bool bRxConfigure=MEA_FALSE;
	MEA_LxCp_t              LxCp_Id;

	if(pEcfmHwInfo != NULL)
	{

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMaParams u4VlanIdIsid:%d\n",pEcfmHwInfo->EcfmHwMaParams.u4VlanIdIsid);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMaParams u4CcmInterval:%d\n",pEcfmHwInfo->EcfmHwMaParams.u4CcmInterval);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMaParams au1MAID:%s\n",pEcfmHwInfo->EcfmHwMaParams.au1MAID);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMepParams DstMacAddr:%s\n",
				adap_mac_to_string(&pEcfmHwInfo->EcfmHwMepParams.DstMacAddr));

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMepParams u2MepId:%d\n",pEcfmHwInfo->EcfmHwMepParams.u2MepId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMepParams u1MdLevel:%d\n",pEcfmHwInfo->EcfmHwMepParams.u1MdLevel);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMepParams u1Direction:%d\n",pEcfmHwInfo->EcfmHwMepParams.u1Direction);
		// ECFM_NP_MP_DIR_UP
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMepParams u1CcmPriority:%d\n",pEcfmHwInfo->EcfmHwMepParams.u1CcmPriority);


		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwRMepParams u2MepId:%d\n",pEcfmHwInfo->EcfmHwRMepParams.u2MepId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwRMepParams u2RMepId:%d\n",pEcfmHwInfo->EcfmHwRMepParams.u2RMepId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwRMepParams u1MdLevel:%d\n",pEcfmHwInfo->EcfmHwRMepParams.u1MdLevel);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwRMepParams u1Direction:%d\n",pEcfmHwInfo->EcfmHwRMepParams.u1Direction);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwRMepParams u1State:%d\n",pEcfmHwInfo->EcfmHwRMepParams.u1State);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmHwInfo u1EcfmOffStatus:%d\n",pEcfmHwInfo->u1EcfmOffStatus);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"pEcfmHwInfo type:%s u2MepId:%d\n",adap_getCCmStringFromType(u1Type),pEcfmHwInfo->EcfmHwMepParams.u2MepId);



		//adap_memcpy(&maHandle,&pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0],4);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMaParams u2TxFilterId:%d:\n",pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2TxFilterId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwMaParams u2RxFilterId:%d:\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.u2RxFilterId);

		switch(u1Type)
		{
			case ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Tx Up Mep tagged len:%d\n",
						pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.i4Length);
				for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.i4Length;issPortList++)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Tx Up Mep tagged IssPort:%d\n",
					pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.pu4PortArray[issPortList]);
				}
				for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcTxParams.UntagPortList.i4Length;issPortList++)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Tx Up Mep untagged IssPort:%d\n",
					pEcfmHwInfo->unParam.EcfmHwCcTxParams.UntagPortList.pu4PortArray[issPortList]);
				}
			case ADAP_ECFM_NP_START_CCM_TX:
//				pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0] = 0x01;
//				pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1] = 0x02;
//				pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2] = 0x03;
//				pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3] = 0x04;
//
//				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ECFM_NP_START_CCM_TX handler :%d %d %d %d\n",
//											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0],
//											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1],
//											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2],
// 											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3]);

			case ADAP_ECFM_NP_STOP_CCM_TX:
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u4IfIndex:%d\n",pEcfmHwInfo->unParam.EcfmHwCcTxParams.u4IfIndex);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u2PduLength:%d\n",pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2PduLength);
				break;

			case ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST :
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Rx Up Mep tagged len:%d\n",
						pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.i4Length);
				for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.i4Length;issPortList++)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Rx Up Mep tagged IssPort:%d\n",
					pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.pu4PortArray[issPortList]);
				}
				for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcRxParams.UntagPortList.i4Length;issPortList++)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Rx Up Mep untagged IssPort:%d\n",
					pEcfmHwInfo->unParam.EcfmHwCcRxParams.UntagPortList.pu4PortArray[issPortList]);
				}
			case ADAP_ECFM_NP_STOP_CCM_RX:
			case ADAP_ECFM_NP_START_CCM_RX:

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u4IfIndex:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.u4IfIndex);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u2RxFilterId:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.u2RxFilterId);

				if(pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo != NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u4StartSeqNo:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->u4StartSeqNo);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u4CcmRxTimeout:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->u4CcmRxTimeout);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u4VlanIdIsid:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->u4VlanIdIsid);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u2RMepId:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->u2RMepId);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams u1MdLevel:%d\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->u1MdLevel);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams au1MAID:%s\n",pEcfmHwInfo->unParam.EcfmHwCcRxParams.pEcfmCcRxInfo->au1MAID);
				}
				break;
			case ADAP_ECFM_NP_MA_CREATION :
				//pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2TxFilterId = rand();
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams ma creation :%d\r\n",pEcfmHwInfo->unParam.EcfmHwCcTxParams.u2TxFilterId);
				break;
			case ADAP_ECFM_NP_MA_DELETION:
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams ma deletion\r\n");
				break;
			case ADAP_ECFM_NP_OFFLOAD_STATE :
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EcfmHwCcTxParams offload\r\n");

				break;
			default:
				break;
		}

	}


	adap_signal_condMutexLock_ccm();
	// disable all ports
	//hw_set_ingress_ports(0);

	//pEcfmHwInfo->unParam.EcfmHwCcRxParams.u2RxFilterId
	// don't do return at the middle of function
	switch(u1Type)
	{
			case ADAP_ECFM_NP_STOP_CCM_TX:
				// inactive the generator
				break;
			case ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST:
			case ADAP_ECFM_NP_START_CCM_TX:
				if(strlen((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID) == 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: ma is empty\r\n");
					ret = ENET_FAILURE;
				}
				if(adap_getIndexByMaStrCcmDb((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID) == UNDEFINED_VALUE16)
				{
					// create one
					if(adap_setMaStrCcmDb(&pEcfmHwInfo->EcfmHwMaParams) == UNDEFINED_VALUE16)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: failed to allocate space\r\n");
						ret = ENET_FAILURE;
					}
				}
				if(adap_FsTxCcmHandler(u1Type,pEcfmHwInfo) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: failed to handle CCM tx r\n");
					ret = ENET_FAILURE;
				}
				break;
			case ADAP_ECFM_NP_STOP_CCM_RX:
				// send loss of sync
				adap_send_loss_of_sync(pEcfmHwInfo);
				break;
			case ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST:
			case ADAP_ECFM_NP_START_CCM_RX:
				if(strlen((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID) == 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: ma is empty\r\n");
					ret = ENET_FAILURE;
				}
				if(adap_getIndexByMaStrCcmDb((char *)pEcfmHwInfo->EcfmHwMaParams.au1MAID) == UNDEFINED_VALUE16)
				{
					// create one
					if(adap_setMaStrCcmDb(&pEcfmHwInfo->EcfmHwMaParams) == UNDEFINED_VALUE16)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: failed to allocate space\r\n");
						ret = ENET_FAILURE;
					}
				}
				bRxConfigure = MEA_FALSE;
				if(adap_FsRxCcmHandler(u1Type,pEcfmHwInfo,&bRxConfigure) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: failed to handle CCM rx r\n");
					ret = ENET_FAILURE;
				}
				// configure rge forwarder
				if(bRxConfigure == MEA_TRUE)
				{
					if(pEcfmHwInfo->EcfmHwMepParams.u1Direction == ADAP_ECFM_NP_MP_DIR_UP)
					{
						for(issPortList=0;issPortList < pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.i4Length;issPortList++)
						{
							adap_FsRemoteSidePacketSwap(pEcfmHwInfo,
													pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.pu4PortArray[issPortList],
													u1Type);
						}
					}
					else
					{
						adap_FsRemoteSidePacketSwap(pEcfmHwInfo,pEcfmHwInfo->unParam.EcfmHwCcRxParams.u4IfIndex,u1Type);
					}
				}

				break;
			case ADAP_ECFM_NP_MA_CREATION:
				for(issPortList=1; issPortList<MeaAdapGetNumOfPhyPorts(); issPortList++)
				{
					if(getLxcpId(issPortList,&LxCp_Id) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ECFM_NP_OFFLOAD_STATE FAIL: LxCp Id NOT FOUND for ISS port %d\n", issPortList);
						return ENET_FAILURE;
					}


						// move all port to non offload
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"mode LXCP:%d cfm_oam to send to cpu\n",LxCp_Id);
					if(adap_getCfmOffloadState() == MEA_FALSE)
					{
						adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_TRUE,LxCp_Id);
					}
					else
					{
						adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_FALSE,LxCp_Id);
					}
				}
				break;
			case ADAP_ECFM_NP_MA_DELETION:
				if( (pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0] == 0) &&
					(pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1] == 0) &&
					(pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2] == 0) &&
					(pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3] == 0) )
				{
					break;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"handler :%d %d %d %d\n",
											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[0],
											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[1],
											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[2],
											pEcfmHwInfo->EcfmHwMaParams.au1HwHandler[3]);

				adap_delete_ccm_configuration(pEcfmHwInfo);

				break;
			case ADAP_ECFM_NP_OFFLOAD_STATE:
				if(pEcfmHwInfo->u1EcfmOffStatus ==1)
				{
					adap_setCfmOffloadState(MEA_TRUE);
				}
				else
				{
					adap_setCfmOffloadState(MEA_FALSE);
				}
				for(issPortList=1; issPortList<MeaAdapGetNumOfPhyPorts(); issPortList++)
				{
					if(getLxcpId(issPortList,&LxCp_Id) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ECFM_NP_OFFLOAD_STATE FAIL: LxCp Id NOT FOUND\n");
						return ENET_FAILURE;
					}

					if(pEcfmHwInfo->u1EcfmOffStatus ==1)
					{
						// move all ports to offload
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"mode LXCP:%d cfm_oam to transparent\n",LxCp_Id);
						adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_FALSE,LxCp_Id);
					}
					else
					{
						// move all port to non offload
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"mode LXCP:%d cfm_oam to send to cpu\n",LxCp_Id);
						adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_TRUE,LxCp_Id);
					}
				}
				break;
			default:
				break;
	}
	//hw_set_ingress_ports(1);
	adap_signal_condMutexUnLock_ccm();
	//FsCreatePacketGenApplication();
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiEcfmHwCallNpApi: return failedr\n");
	}
    return ret;

}

