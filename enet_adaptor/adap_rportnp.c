/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
 * adap_rportnp.c
 *
 *  Created on: 15 ???? 2016
 *      Author: GilM
 */
#include  <string.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_rport.h"
#include "adap_ipnp.h"
#include "EnetHal_L2_Api.h"
#include "EnetHal_L3_Api.h"

ADAP_Uint32  EnetHal_FsNpIpv4L3IpInterface (eEnetHal_L3Action L3Action, tEnetHal_FsNpL3IfInfo * pFsNpL3IfInfo)
{

	ADAP_UNUSED_PARAM (L3Action);
	ADAP_UNUSED_PARAM (pFsNpL3IfInfo);
	tEnetHal_FsNpL3IfInfo *pRouterPort=NULL;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Int32 ret = ENET_FAILURE;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint8 u1Port = 0;
	char  ifname[20];

	if (pFsNpL3IfInfo->IfType == IF_PPP)
	{
		u1Port = pFsNpL3IfInfo->u1Port;
	}
	else
	{
		u1Port = (ADAP_Uint8) pFsNpL3IfInfo->u4IfIndex;
	}

	pRouterPort = getRouterPortInfo(u1Port);
	if(pRouterPort != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"L3Action:%d u4IpAddr:%x\n",L3Action,pFsNpL3IfInfo->u4IpAddr);

		switch(L3Action)
		{
		case ENETHAL_L3_HW_CREATE_IF_INFO:
			if(pRouterPort->valid==MEA_FALSE)
			{
				pRouterPort->valid=MEA_TRUE;
				if (EnetHal_FsMiVlanHwCreateFdbId 
					(pFsNpL3IfInfo->u4VrId, pRouterPort->u2PortVlanId) == ENET_SUCCESS)
				{
					if (EnetHal_FsMiVlanHwAssociateVlanFdb (pFsNpL3IfInfo->u4VrId, 
							pRouterPort->u2PortVlanId,
							pRouterPort->u2PortVlanId) == ENET_SUCCESS)
					{
						Adap_bridgeDomain_semLock();
						pBridgeDomain = Adap_getBridgeDomainByVlan
							(pRouterPort->u2PortVlanId, pFsNpL3IfInfo->u4VrId);
						if(pBridgeDomain != NULL)
						{
							adap_memset(&pBridgeDomain->ifIndex,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
							adap_memset(&pBridgeDomain->ifType,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
							pBridgeDomain->ifIndex[0] = u1Port;
							pBridgeDomain->ifType[0] = VLAN_UNTAGGED_MEMBER_PORT;
							pBridgeDomain->num_of_ports=1;
							AdapSetAccepFrameType(pBridgeDomain->ifIndex[0],
								ADAP_VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES);
							setPortToPvid (pBridgeDomain->ifIndex[0],pRouterPort->u2PortVlanId);
							if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[0],
								pBridgeDomain,VLAN_UNTAGGED_MEMBER_PORT) == MEA_OK)
							{
								ret = ENET_SUCCESS;
							}
						}
						Adap_bridgeDomain_semUnlock();
					}
				}
			}
			else
			{
				if (pFsNpL3IfInfo->IfType == IF_PPP)
				{
					/* Logical PPP interface can be bound on same router port */
					adap_memset(&ifname, 0, 20);
					sprintf (ifname, "vlan%d", pRouterPort->u2PortVlanId);
					if (EnetHal_FsNpIpv4CreatePPPInterface
							(pFsNpL3IfInfo->u4VrId,(ADAP_Uint8 *)ifname,pFsNpL3IfInfo->u4IfIndex,
							 pFsNpL3IfInfo->u4IpAddr, pFsNpL3IfInfo->u4IpSubnet, 
							 pRouterPort->u2PortVlanId,u1Port) != ENET_SUCCESS)
					{
						return ENET_FAILURE;
					}
					return ENET_SUCCESS;
				}
			}

			if (ret == ENET_SUCCESS)
			{
				adap_memset(&ifname, 0, 20);
				sprintf (ifname, "vlan%d", pRouterPort->u2PortVlanId);
				if (EnetHal_FsNpIpv4CreatePPPInterface 
					(pFsNpL3IfInfo->u4VrId,(ADAP_Uint8 *)ifname,pFsNpL3IfInfo->u4IfIndex,
					 pFsNpL3IfInfo->u4IpAddr, pFsNpL3IfInfo->u4IpSubnet, 
					 pRouterPort->u2PortVlanId,u1Port) != ENET_SUCCESS)
				{
					return ENET_FAILURE;
				}
				ret = getLxcpId(u1Port,&lxcp_Id);
				if(ret == ENET_SUCCESS)
				{
					ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,
							MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,
							MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,
							MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
				}
				ret = adap_ReconfigureHwVlan (pRouterPort->u2PortVlanId,0);
			}
			break;
		case ENETHAL_L3_HW_MODIFY_IF_INFO:
			if(pRouterPort->valid==MEA_TRUE)
			{
				if( (pFsNpL3IfInfo->u4IpAddr != pRouterPort->u4IpAddr) || (pFsNpL3IfInfo->u4IpSubnet != pRouterPort->u4IpSubnet) )
				{
					adap_memset(&ifname, 0, 20);
					sprintf (ifname, "vlan%d", pRouterPort->u2PortVlanId);
					if (EnetHal_FsNpIpv4ModifyPPPInterface 
							(pFsNpL3IfInfo->u4VrId,(ADAP_Uint8 *)ifname,pFsNpL3IfInfo->u4IfIndex,
							 pFsNpL3IfInfo->u4IpAddr, pFsNpL3IfInfo->u4IpSubnet, 
							 pRouterPort->u2PortVlanId,u1Port) != ENET_SUCCESS)
					{
						return ENET_FAILURE;
					}
				}
			}
			break;
		case ENETHAL_L3_HW_DELETE_IF_INFO:
			if(pRouterPort->valid==MEA_TRUE)
			{
				pRouterPort->valid=MEA_FALSE;
                                adap_memset(&ifname, 0, 20);
                                sprintf (ifname, "vlan%d", pRouterPort->u2PortVlanId);

				if (EnetHal_FsNpIpv4DeleteIpInterface 
                                        (pFsNpL3IfInfo->u4VrId,(ADAP_Uint8 *)ifname,pFsNpL3IfInfo->u4IfIndex,
                                         pRouterPort->u2PortVlanId) == ENET_SUCCESS)
				{
				    if (pFsNpL3IfInfo->IfType != IF_PPP)
				    {
					MeaDrvDelVlanEntryEnet (pRouterPort->u2PortVlanId);
					EnetHal_FsMiVlanHwDeleteFdbId (pFsNpL3IfInfo->u4VrId,
								pRouterPort->u2PortVlanId);
				}
				}
				else
				{
					return ENET_FAILURE;
				}
			}
			break;
		default:
			break;
		}
	}
    return ENET_SUCCESS;
}


ADAP_Uint32 EnetHal_FsNpL3Ipv4ArpAdd (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4IpAddr, 
                                      tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,
                                      ADAP_Int32 i1State, ADAP_Uint32 *pu4TblFull)
{
	tEnetHal_FsNpL3IfInfo *pRouterPort=NULL;
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpDestV4Routing *pIpv4Routing=NULL;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint32         logPort = 0;
	MEA_Port_t          PhyPort;
	MEA_Action_t        Action_id=0;
	MEA_EHP_Info_dbt tEhp_info;
	tEnetHal_MacAddr srcMac;
	tEnetHal_MacAddr daMac;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
	adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
	adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IfIndex:%d\n",u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%x\n",u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pMacAddr:%s\n",adap_mac_to_string(pMacAddr));
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," i1State:%d\n",i1State);

	pRouterPort = getRouterPortInfo(u4IfIndex);
	if(pRouterPort == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"router interface table for ifIndex:%d not exist\n",u4IfIndex);
		return ENET_FAILURE;
	}

	/* update Adaptor ARP table */
	if(AdapGetEntryIpTable(&pIpv4Routing,u4IpAddr) != ENET_SUCCESS)
	{
		ret = AdapAllocateEntryIpTable(&pIpv4Routing,u4IpAddr);
		if( (ret != ENET_SUCCESS) || (pIpv4Routing == NULL) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to allocate routeEntry for IfIndex:%d\r\n",u4IfIndex);
			return ENET_FAILURE;
		}
		else
		{
			adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));
		}
	}
	else
	{
		adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));
	}

	/* install forwarder entry */
	if ((i1State == NP_ARP_DYNAMIC) || (i1State == NP_ARP_STATIC))
	{
		logPort = u4IfIndex;
		MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
		MEA_SET_OUTPORT(&tOutPorts,PhyPort);

		adap_memcpy(&daMac,pMacAddr,sizeof(tEnetHal_MacAddr));
		MeaDrvGetFirstGlobalMacAddress(&srcMac);

		MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);

		tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;

		/* create action for the route */
		MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,&daMac, MEA_EGRESS_HEADER_PROC_CMD_EXTRACT, 0, 0);

		if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
					&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
		}
		// create entry in forwarder
		if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
					(u4IpAddr & 0xFF000000) >> 24,
					(u4IpAddr & 0x00FF0000) >> 16,
					(u4IpAddr & 0x0000FF00) >> 8,
					(u4IpAddr & 0x000000FF) );
		}
	}

	ADAP_UNUSED_PARAM (*pu4TblFull);
	ADAP_UNUSED_PARAM (pu1IfName);
	return ret;
}


ADAP_Uint32 EnetHal_FsNpL3Ipv4ArpModify (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4IpAddr, 
                                      tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName, ADAP_Int32 i1State)
{
	tEnetHal_FsNpL3IfInfo *pRouterPort=NULL;
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpDestV4Routing *pIpv4Routing=NULL;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint32         logPort = 0;
	MEA_Port_t          PhyPort;
	MEA_Action_t        Action_id=0;
	MEA_EHP_Info_dbt tEhp_info;
	tEnetHal_MacAddr srcMac;
	tEnetHal_MacAddr daMac;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
	adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
	adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IfIndex:%d\n",u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%x\n",u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pMacAddr:%s\n",adap_mac_to_string(pMacAddr));
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," i1State:%d\n",i1State);

	pRouterPort = getRouterPortInfo(u4IfIndex);
	if(pRouterPort == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"router interface table for ifIndex:%d not exist\n",u4IfIndex);
		return ENET_FAILURE;
	}

	/* update Adaptor ARP table */
	if(AdapGetEntryIpTable(&pIpv4Routing,u4IpAddr) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to get arpEntry for IfIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}
	else
	{
		adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));
	}

	/* install forwarder entry */
	if ((i1State == NP_ARP_DYNAMIC) || (i1State == NP_ARP_STATIC))
	{
		logPort = u4IfIndex;
		MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
		MEA_SET_OUTPORT(&tOutPorts,PhyPort);

		adap_memcpy(&daMac,pMacAddr,sizeof(tEnetHal_MacAddr));
		MeaDrvGetFirstGlobalMacAddress(&srcMac);

		MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);

		tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;

		/* create action for the route */
		MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,&daMac, MEA_EGRESS_HEADER_PROC_CMD_EXTRACT, 0, 0);

		if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
					&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
		}
		// create entry in forwarder
		if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
					(u4IpAddr & 0xFF000000) >> 24,
					(u4IpAddr & 0x00FF0000) >> 16,
					(u4IpAddr & 0x0000FF00) >> 8,
					(u4IpAddr & 0x000000FF) );
		}
	}

	ADAP_UNUSED_PARAM (pu1IfName);
	return ret;
}

