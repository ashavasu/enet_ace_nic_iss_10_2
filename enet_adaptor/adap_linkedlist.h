/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ADAP_LINKEDLIST_H__
#define __ADAP_LINKEDLIST_H__

/** 
 *  @file Adap_UtilsHashMap.h
 *  @brief Generic Hash map of key-value pairs implemented with separate chaining using linked lists.
 *	
 *  @details  The hash map (sometimes called dictionary or associative array)
 *  is a set of distinct keys (or indexes) mapped (or associated) to values.
 *  size of allocated table will be the nearest prime number greater than requested capacity
 *  Lists used for chaining will be allocated lazely.
 *
 *  @author Yotam Ramon & Roee Ben Halevi
 * 
 *  @bug No known bugs.
 */

#include "adap_types.h"
#include "EnetHal_status.h"

typedef struct adap_linkedlist adap_linkedlist;

typedef struct adap_linkedlist_element {  /* TODO - need to hide the struct content in some way */
	struct adap_linkedlist_element* next;
	struct adap_linkedlist_element* prev;
} adap_linkedlist_element;

typedef struct {
	ADAP_Uint32 capacity;
	ADAP_Bool thread_safe;
	int (*compare)(adap_linkedlist_element* element1, adap_linkedlist_element* element2);
} adap_linkedlist_params;

/** 
 * @brief Create a new linked list with given parameters.
 * @return
 */
EnetHal_Status_t adap_linkedlist_create(adap_linkedlist** list, adap_linkedlist_params *params);


/**
 * @brief Destroy list set
 * @return
 */
EnetHal_Status_t adap_linkedlist_destroy(adap_linkedlist* list);



EnetHal_Status_t adap_linkedlist_add_first(adap_linkedlist* list, adap_linkedlist_element* element);

EnetHal_Status_t adap_linkedlist_add_last(adap_linkedlist* list, adap_linkedlist_element* element);

EnetHal_Status_t adap_linkedlist_remove_first(adap_linkedlist* list, adap_linkedlist_element** element);

EnetHal_Status_t adap_linkedlist_remove_last(adap_linkedlist* list, adap_linkedlist_element** element);

EnetHal_Status_t adap_linkedlist_remove(adap_linkedlist* list, adap_linkedlist_element* element);

ADAP_Uint32 adap_linkedlist_size(adap_linkedlist* list);

EnetHal_Status_t adap_linkedlist_iter(adap_linkedlist *list, adap_linkedlist_element** element);


#endif /* __ADAP_LINKEDLIST_H__ */





