#ifndef _ADAP_LPM_NP_H
#define _ADAP_LPM_NP_H

#include "adap_lpminc.h"
#include "adap_lpmrb.h"
#include "adap_lpmtrie.h"
#include "adap_types.h"

typedef struct {
    tRedBlkNodeEmbd   RbNode;
    UINT4         DstIp;
    UINT4         u4VrId;
    UINT4         u4IfIndex;
    UINT2         u2vlanId;
    tMacAddr      MacAddr;
} tArpEntry;

typedef struct {
    tRedBlkNodeEmbd   RbNode;
    tIp6Address   DstIp6;
    UINT4         u4VrId;
    UINT4         u4IfIndex;
    UINT2         u2vlanId;
    tMacAddr      MacAddr;
}tNDEntry;

typedef struct NextHopInfo
{
        UINT4     u4NextHopGt; /* Ip addr of the NextHop */
        UINT4     u4IfIndex;   /* Index through which NextHop
                                  is reachable */
        UINT4     u4ContextId; /* VRF ID */
        UINT2     u2VlanId;
        UINT2     u2Reserved;
} tNextHopInfo;

typedef struct {
    	tIp6Address Ip6Prefix;
    	tIp6Address Ip6NextHop;
        UINT4 u4IpDestAddr;
        UINT4 u4SubNetMask;
        UINT4 u4NextHopGt;
        UINT4 u4IfIndex;
        UINT4 u4VrId;
        UINT2 u2VlanId;
	UINT1 u1PrefixLen;
	UINT1 u1AddrType;
} tRtNextHopEntry;

typedef struct {
        tRedBlkNodeEmbd   RbNode;
        tMacAddr      DestMacAddr;
        UINT4         u4DestAddr;
        UINT4         u4NextHopGt;
        UINT4         u4IfIndex;
        UINT4         u4VrId;
        UINT2         u2VlanId;
        UINT2         u2Port;
} tFPRouteEntry;

typedef struct
{
   tIp6Address  Ip6NextHop;
   UINT4        u4IfIndex;
   UINT4        u4ContextId; 
   UINT2        u2VlanId;
   UINT2        u2Reserved;
}tIpv6NextHop;


typedef struct {
    tIp6Address Ip6Prefix;
    tIp6Address Ip6NextHop;
    UINT4       u4IfIndex;
    UINT4       u4VrId;
    UINT2       u2VlanId;
    UINT1       u1PrefixLen;
    UINT1       u1Reserved;
} tIp6RouteEntry;


extern tRedBlkTree  gpL3ArpTbl;
extern tRedBlkTree  gpL3NDTbl;
extern tRedBlkTree  gpFPShadowRouteTbl;
extern void    *gpLpmRouteTbl;        /* L3 Route Tbl */
extern void    *gpLpmIp6RouteTbl;        /* L3 Route Tbl */
extern int	gLpmAdapStatus;
extern int	gLpmAdapHWStatus;

INT4    LpmL3ArpTblCmp (tRedBlkElem *e1, tRedBlkElem *e2);
INT4    LpmL3NDTblCmp (tRedBlkElem *e1, tRedBlkElem *e2);
INT4    LpmFPShadowRouteTblCmp (tRedBlkElem *e1, tRedBlkElem *e2);


INT4  LpmTrieDbAddRtEntry 
                 (VOID *pInputParams, VOID *pOutputParams,
                   tNextHopInfo **ppAppSpecInfo, tNextHopInfo * 
                   pNewAppSpecInfo);

INT4 LpmTrieDbDeleteRtEntry (VOID *pInputParams,
                                    tNextHopInfo **ppAppSpecInfo,
                                    VOID *pOutputParams,
                                    VOID * NextHop,
                                    tKey Key);

INT4 LpmTrieDbLookUpEntry (tInputParams * pInputParams,
                                  tOutputParams *pOutputParams,
                                  VOID *pAppSpecInfo,
                                  UINT2 u2KeySize, tKey key);

INT4
LpmTrieDbBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                    VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key);
UINT1       *
LpmTrieDbDelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                        tOutputParams * pOutputParams);
VOID
LpmTrieDbCbDelete (VOID *pInput);

INT4
LpmTrieDbDelete (VOID *pInputParams, VOID **ppAppSpecPtr, tKey Key);
/*Ipv6 TrieDb*/

INT4  LpmTrieDbAddIp6RtEntry (VOID *pInputParams,  
                                      VOID *pOutputParams,
                                      tIpv6NextHop **ppAppSpecInfo, 
                                      tIpv6NextHop *pNewAppSpecInfo);

INT4 LpmTrieDbDeleteIp6RtEntry (VOID *pInputParams,
                                       tIpv6NextHop **ppAppSpecInfo,
                                       VOID *pOutputParams,
                                       VOID * NextHop,
                                       tKey Key);

INT4 LpmTrieDbLookUpIp6Entry (tInputParams * pInputParams,
                                     tOutputParams *pOutputParams,
                                     VOID *pAppSpecInfo,
                                     UINT2 u2KeySize, 
                                     tKey key);

INT4 LpmTrieDbIp6BestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                            VOID *pOutputParams, VOID *pAppSpecInfo, 
                            tKey Key);

UINT1       *
LpmTrieDbIp6DelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                           tOutputParams * pOutputParams);
VOID
LpmTrieDbIp6CbDelete (VOID *pInput);

UINT4
Enet_Lpm_Arp_Add (UINT4 u4VrId, UINT4 u4IpAddr, UINT4 u4IfIdx,
                    UINT2 u2VlanId, UINT1 *pMacAddr);

UINT4
Enet_Lpm_Arp_Delete (UINT4 u4VrId, UINT4 u4IpAddr);

tArpEntry        *
Enet_Lpm_Arp_Get (UINT4 u4VrId, UINT4 u4IpAddr);

UINT4
Enet_Lpm_ND_Add (UINT4 u4VrId, UINT1 *pIp6Addr, UINT4 u4IfIdx,
                    UINT2 u2VlanId, UINT1 *pMacAddr);

UINT4
Enet_Lpm_ND_Delete (UINT4 u4VrId, UINT1 *pIp6Addr);

tNDEntry        *
Enet_Lpm_ND_Get (UINT4 u4VrId, UINT1 *pIp6Addr);

UINT4
Enet_Lpm_Ipv6Route_Add (tRtNextHopEntry  *pRouteEntry);

UINT4
Enet_Lpm_Ipv6Route_Delete (tRtNextHopEntry  *pRouteEntry);


UINT4
Enet_Lpm_Route_Add (tRtNextHopEntry  *pRouteEntry);

UINT4
Enet_Lpm_Route_Delete (tRtNextHopEntry  *pRouteEntry);

UINT4
Enet_Lpm_Route_Get (tRtNextHopEntry  *pRouteInput, tRtNextHopEntry **pRtNextHop);

UINT4
Enet_Lpm_Add_FP_Entry (UINT4 u4VrId, UINT4 u4IpDestAddr,
                       UINT4 u4NextHop, UINT4 u4IfIndex,
                       UINT2 u2VlanId, UINT2 u2VpnId, UINT1 *pDestMacAddr);

UINT4
Enet_Lpm_Delete_FP_Entry (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask);

INT4
Enet_Lpm_Handle_FPGA_Arp_Delete (UINT4 u4VrId, UINT4 u4IpAddr);

INT4
EnetAdapProcessLpmPacket (UINT4 u4IfIndex, void *pBuf, UINT4 len);

VOID
DumpLPMRouteTable (VOID);

VOID
DumpArpTbl (VOID);

VOID
DumpFPShadowRouteTbl (VOID);

void UtilIpv6CopyAddrBits (tIp6Address * pPref, tIp6Address * pAddr, INT4 i4NumBits);

#endif
