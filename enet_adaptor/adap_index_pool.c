/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#include <pthread.h>
#include "adap_index_pool.h"
#include "adap_utils.h"

struct adap_index_pool {
	ADAP_Bool *indexes;
	ADAP_Bool thread_safe;
    pthread_mutex_t mutex;
    ADAP_Uint32 capacity;
    ADAP_Uint32 size;
};

EnetHal_Status_t adap_index_pool_init(adap_index_pool** pool, struct adap_index_pool_params *params)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	adap_index_pool* t_pool = NULL;
	ADAP_Uint32 i;
	int err;

    if ((params == NULL) || (pool == NULL)) {
    	rc = ENETHAL_STATUS_NULL_PARAM;
    	goto bail;
    }

    if (params->capacity == 0) {
    	rc = ENETHAL_STATUS_INVALID_PARAM;
    	goto bail;
    }
    
    t_pool = (adap_index_pool*)adap_malloc(sizeof(adap_index_pool));
    if (t_pool == NULL) {
    	rc = ENETHAL_STATUS_MEM_ALLOC;
    	goto bail;
    }

    t_pool->indexes = (ADAP_Bool *)adap_malloc(sizeof(ADAP_Bool) * params->capacity);
    if (t_pool->indexes == NULL) {
    	rc = ENETHAL_STATUS_MEM_ALLOC;
    	goto bail;
    }

    for(i = 0; i < params->capacity; i++) {
    	t_pool->indexes[i] = ADAP_FALSE;
    }

    if (params->thread_safe == ADAP_TRUE) {
		err = pthread_mutex_init(&(t_pool->mutex), NULL);
		if (err != 0) {
			rc =  ENETHAL_STATUS_MUTEX_ERROR;
			goto bail;
		}
    }

    t_pool->thread_safe = params->thread_safe;
    t_pool->capacity = params->capacity;
    t_pool->size = 0;

    *pool = t_pool;
    t_pool = NULL;

bail:
	if (t_pool != NULL) {
		adap_safe_free(t_pool->indexes);
		adap_safe_free(t_pool);
	}
    return rc;
}

EnetHal_Status_t adap_index_pool_deinit(adap_index_pool* pool)
{
	int err;

    if (pool == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (pool->thread_safe == ADAP_TRUE) {
		err = pthread_mutex_destroy(&pool->mutex);
		if (err != 0) {
			return ENETHAL_STATUS_MUTEX_ERROR;
		}
    }

    adap_safe_free(pool->indexes);
    adap_safe_free(pool);

  	return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_index_pool_alloc(adap_index_pool* pool, ADAP_Uint32* index)
{
	int err;

    if ((pool == NULL) || (index == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (pool->thread_safe == ADAP_TRUE) {
		err = pthread_mutex_lock(&pool->mutex);
		if (err != 0) {
			return ENETHAL_STATUS_MUTEX_ERROR;
		}
    }

    if (pool->size == pool->capacity) {
    	return ENETHAL_STATUS_LIMIT_REACHED;
    }

    for(*index = 0; pool->indexes[*index] == ADAP_TRUE; (*index)++);

	pool->indexes[*index] = ADAP_TRUE;
	pool->size++;

	if (pool->thread_safe == ADAP_TRUE) {
		err = pthread_mutex_unlock(&pool->mutex);
		if (err != 0) {
			return ENETHAL_STATUS_MUTEX_ERROR;
		}
	}

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_index_pool_free(adap_index_pool* pool, ADAP_Uint32 index)
{
	int err;

    if (pool == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (pool->thread_safe == ADAP_TRUE) {
    	err = pthread_mutex_lock(&pool->mutex);
    	if (err != 0) {
    		return ENETHAL_STATUS_MUTEX_ERROR;
    	}
    }

    if (pool->indexes[index] == ADAP_TRUE) {
    	pool->indexes[index] = ADAP_FALSE;
    	pool->size--;
    }

    if (pool->thread_safe == ADAP_TRUE) {
    	err = pthread_mutex_unlock(&pool->mutex);
    	if (err != 0) {
    		return ENETHAL_STATUS_MUTEX_ERROR;
    	}
    }

    return ENETHAL_STATUS_SUCCESS;
}

ADAP_Uint32 adap_index_pool_size(adap_index_pool* pool)
{
    if (pool == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    return pool->size;
}
