/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef ENETHAL_OF_API_H
#define ENETHAL_OF_API_H

typedef enum
{
    ENETHAL_OFC_IP_REASSEMBLE = 0,
    ENETHAL_OFC_PKT_BUFFERING = 1,
    ENETHAL_OFC_STP_STATUS = 2
}eEnetHal_OfcCfgParam;

typedef struct
{
	ADAP_Uint32     ofcGroupIndex;
    ADAP_Int32      ofcGroupType;
    ADAP_Uint32     ofcGroupDurationSec;
    //tTMO_SLL *pBucketList;
} tEnetHal_OfcHwGroupInfo;

typedef struct
{
	ADAP_Uint32 u4FsofcMeterIndex;
	ADAP_Uint32 u4FsofcMeterFlowCount;
	ADAP_Uint32 u4FsofcMeterDurationSec;
	ADAP_Uint16 u2Flags;
	ADAP_2Uint32 u8FsofcMeterPacketInCount;
	ADAP_2Uint32 u8FsofcMeterByteInCount;
       //tTMO_SLL             BandList; /* tOfcMeterBand */
} tEnetHal_OfcHwMeterInfo;

typedef enum
{
    ENETHAL_OFC_NP_PORT_ADD,
    ENETHAL_OFC_NP_PORT_DELETE,
    ENETHAL_OFC_NP_PORT_DOWN,
    ENETHAL_OFC_NP_PORT_UP,
    ENETHAL_OFC_NP_FLOW_ADD,
    ENETHAL_OFC_NP_FLOW_DELETE,
    ENETHAL_OFC_NP_FLOW_MODIFY,
    ENETHAL_OFC_NP_GROUP_ADD,
    ENETHAL_OFC_NP_GROUP_DELETE,
    ENETHAL_OFC_NP_GROUP_MODIFY,
    ENETHAL_OFC_NP_METER_ADD,
    ENETHAL_OFC_NP_METER_DELETE,
    ENETHAL_OFC_NP_METER_MODIFY,
    ENETHAL_OFC_NP_CONFIG_PARAM,
    ENETHAL_OFC_NP_GET_FLOW_STATS,
    ENETHAL_OFC_NP_GET_GROUP_STATS,
    ENETHAL_OFC_NP_GET_METER_STATS,
    ENETHAL_OFC_NP_VLAN_ADD,
    ENETHAL_OFC_NP_VLAN_DELETE,
    ENETHAL_OFC_NP_VLAN_SET,
    ENETHAL_OFC_NP_VLAN_RESET,
    ENETHAL_OFC_NP_INIT
}eEnetHal_OfcCmd;

typedef struct
{
	ADAP_Uint32 u4InIfIndex;
	ADAP_Uint8 au1Ip6Src[16];
	ADAP_Uint8 au1Ip6Dst[16];
	tEnetHal_MacAddr au1SrcMacAddr;
	tEnetHal_MacAddr au1DstMacAddr;
    ADAP_Uint32 u4Ip4Src;
    ADAP_Uint32 u4Ip4Dst;
    ADAP_Uint32 u4InPort;
    ADAP_Uint16 u2EthType;
    ADAP_Uint16 u2TpSrc;
    ADAP_Uint16 u2TpDst;
    ADAP_Uint8 u1IpProto;
    ADAP_Uint8 u1IpTos;
    ADAP_Uint8 u1IpDscp;
}tEnetHal_OfcHwExactFlowMatch;

typedef struct
{
	ADAP_Uint32                u4FlowIndex;
	ADAP_Uint32                u2Priority;
	ADAP_Uint8                u1TableId;
	ADAP_Uint32                u4Action;
	ADAP_Uint32                u4ActionParam;
    //tPortList            RedirectPortList;
	tEnetHal_OfcHwExactFlowMatch ExactKey;
    ADAP_Uint32                u4NumActs;
    ADAP_Uint8                OfcActs[20];
    /* Newly added to support 1.3.1 */
    ADAP_2Uint32             u8Cookie;
    ADAP_2Uint32             u8CookieMask;
    ADAP_Uint16                u2IdleTimeout;
    ADAP_Uint16                u2HardTimeout;
    //tTMO_SLL            *pMatchList;
    //tTMO_SLL            *pInstrList;
}tEnetHal_OfcHwFlowInfo;

typedef struct
{
	ADAP_Uint32    RefCount;
	ADAP_2Uint32   PktCount;
	ADAP_2Uint32   ByteCount;
	ADAP_Uint32    DurSec;
}tEnetHal_OfcGroupStats;

typedef struct
{
	ADAP_2Uint32           PktCount;          /* Number of packets matched. */
	ADAP_2Uint32           ByteCount;         /* Number of bytes matched. */
}tEnetHal_OfcHwMeterStats;

ADAP_Int32 EnetHal_FsOfcHwOpenflowInitAclFilters (ADAP_Uint32 u4Entries);
ADAP_Int32 EnetHal_FsOfcHwOpenflowInitMeterEntries (ADAP_Uint32 u4Entries);
ADAP_Int32 EnetHal_FsOfcHwOpenflowClientCfgParamsUpdate (eEnetHal_OfcCfgParam OfcCfgParam,ADAP_Uint32 u4CfgParamValue);
ADAP_Int32 EnetHal_FsOfcHwUpdateGroupEntry (tEnetHal_OfcHwGroupInfo * pOfcGroupEntry, eEnetHal_OfcCmd OfcCmd);
ADAP_Int32 EnetHal_FsOfcHwUpdateMeterEntry (tEnetHal_OfcHwMeterInfo * pOfcMeterEntry, eEnetHal_OfcCmd OfcCmd);
ADAP_Int32 EnetHal_FsOfcHwGetGroupStats (tEnetHal_OfcHwGroupInfo * pOfcGroupEntry,tEnetHal_OfcGroupStats * pOfcGroupStats);
ADAP_Int32 EnetHal_FsOfcHwGetMeterStats (tEnetHal_OfcHwMeterInfo * pOfcMeterEntry,tEnetHal_OfcHwMeterStats * pOfcMeterStats);
ADAP_Int32 EnetHal_FsOfcHwAddVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint32 *pu1UntagPorts);
ADAP_Int32 EnetHal_FsOfcHwDelVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId);
ADAP_Int32 EnetHal_FsOfcHwSetVlanMemberPorts (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint8 *pu1UntagPorts);
ADAP_Int32 EnetHal_FsOfcHwResetVlanMemberPorts (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint8 *pu1UntagPorts);
void EnetHal_FsOfcHwInitFilter (ADAP_Uint32 u4Index, tEnetHal_OfcHwFlowInfo * pFlow);
ADAP_Int32 EnetHal_FsOfcHwOpenflowPortUpdate (ADAP_Uint32 u4IfIndex, eEnetHal_OfcCmd OfcCmd);

#endif
