/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ADAP_IPSEC_H__
#define __ADAP_IPSEC_H__

#include "adap_types.h"
#include "EnetHal_L2_Api.h" /* TODO - should be in types */
#include "EnetHal_status.h"

/*****************************************************************************/
/* IPsec modes (according to RFC 4301)                                       */
/*****************************************************************************/
typedef enum {
	ADAP_IPSEC_MODE_TUNNEL,
	ADAP_IPSEC_MODE_TRANSPORT,
} adap_ipsec_mode;

/*****************************************************************************/
/* IPsec protocols                                                           */
/*****************************************************************************/
typedef enum {
	/* AH protocol according to RFC 4302 */
	ADAP_IPSEC_PROTO_AH,

	/* AH protocol according to RFC 4303 */
	ADAP_IPSEC_PROTO_ESP,
} adap_ipsec_proto;

/*****************************************************************************/
/* Policy actions                                                            */
/*****************************************************************************/
typedef enum {
	/* Forward packet without IPsec protection */
	ADAP_IPSEC_POLICY_ACTION_BYPASS,

	/* Discard the packet */
	ADAP_IPSEC_POLICY_ACTION_DISCARD,

	/* IPsec protection is needed */
	ADAP_IPSEC_POLICY_ACTION_PROTECT,
} adap_ipsec_policy_action;


/*****************************************************************************/
/* Policy selector mode                                                      */
/*****************************************************************************/
typedef enum {
	/* Any value for the selector is allowed */
	ADAP_IPSEC_POLICY_SELECTOR_MODE_ANY,

	/* Opaque values for the selector are allowed */
	ADAP_IPSEC_POLICY_SELECTOR_MODE_OPAQUE,

	/* The specified value (or range of values) is allowed */
	ADAP_IPSEC_POLICY_SELECTOR_MODE_VALUE,
} adap_ipsec_policy_selector_mode;


/*****************************************************************************/
/* Policy encapsulation mode                                                 */
/*****************************************************************************/
typedef enum {
	/* No encapsulation */
	ADAP_IPSEC_POLICY_ENCAP_IPSEC_ONLY,

	/* NVGRE encapsulation */
	ADAP_IPSEC_POLICY_ENCAP_IPSEC_NVGRE,

	/* VxLAN encapsulation */
	ADAP_IPSEC_POLICY_ENCAP_IPSEC_VXLAN,
} adap_ipsec_policy_encapsulation;

/*****************************************************************************/
/* Policy traffic direction relative to IPsec boundary.                      */
/* IN = traffic from untrusted network to trusted network                    */
/* OUT = traffic from trusted network to untrusted network                   */
/*****************************************************************************/
typedef enum {
	/* Incoming traffic */
	ADAP_IPSEC_POLICY_DIR_IN,

	/* Outgoing traffic */
	ADAP_IPSEC_POLICY_DIR_OUT,
} adap_ipsec_policy_direction;


/*****************************************************************************/
/* Authentication algorithms                                                 */
/*****************************************************************************/
typedef enum {
    ADAP_AUTH_ALG_MD5_96,
    ADAP_AUTH_ALG_SHA1_96,
	ADAP_AUTH_ALG_SHA256_128,
	ADAP_AUTH_ALG_SHA384_192,
	ADAP_AUTH_ALG_SHA512_256,
	ADAP_AUTH_ALG_AES_XCBC_MAC_96,
	ADAP_AUTH_ALG_AES_CMAC_96,
	ADAP_AUTH_ALG_AES_128_GMAC_128,
	ADAP_AUTH_ALG_AES_192_GMAC_128,
	ADAP_AUTH_ALG_AES_256_GMAC_128,
	ADAP_AUTH_ALG_NONE,
	ADAP_AUTH_ALG_LAST,
} adap_auth_algorithm;

/*****************************************************************************/
/* Encryption algorithms                                                     */
/*****************************************************************************/
typedef enum {
	ADAP_ENCR_ALG_3DES_CBC,
	ADAP_ENCR_ALG_AES_128_CBC,
	ADAP_ENCR_ALG_AES_192_CBC,
	ADAP_ENCR_ALG_AES_256_CBC,
	ADAP_ENCR_ALG_AES_128_CTR,
	ADAP_ENCR_ALG_AES_192_CTR,
	ADAP_ENCR_ALG_AES_256_CTR,
	ADAP_ENCR_ALG_AES_128_GCM_64,
	ADAP_ENCR_ALG_AES_128_GCM_96,
	ADAP_ENCR_ALG_AES_128_GCM_128,
	ADAP_ENCR_ALG_AES_192_GCM_64,
	ADAP_ENCR_ALG_AES_192_GCM_96,
	ADAP_ENCR_ALG_AES_192_GCM_128,
	ADAP_ENCR_ALG_AES_256_GCM_64,
	ADAP_ENCR_ALG_AES_256_GCM_96,
	ADAP_ENCR_ALG_AES_256_GCM_128,
	ADAP_ENCR_ALG_AES_128_CCM_64,
	ADAP_ENCR_ALG_AES_128_CCM_96,
	ADAP_ENCR_ALG_AES_128_CCM_128,
	ADAP_ENCR_ALG_NONE,
	ADAP_ENCR_ALG_LAST,
} adap_encr_algorithm;



/*****************************************************************************/
/* adap_ipsec_sa_selectors (defined in RFC 4301)                             */
/*                                                                           */
/* Security Association (SA) selectors that uniquely identify the SA.        */
/*****************************************************************************/
typedef struct {
	/* Security Parameter Index (defined in RFC 4301) */
	ADAP_Uint32 spi;

	/* Tunnel IPv4 destination & source address. */
	ADAP_Uint32 tunnel_dip;
	ADAP_Uint32 tunnel_sip;

	/* Next header of the IP header (can be ESP or AH) */
	adap_ipsec_proto  next_header;
} adap_ipsec_sa_selectors;


/*****************************************************************************/
/* adap_ipsec_sa_nvgre_params                                                */
/*                                                                           */
/* Parameters for NVGRE over IPSEC encapsulation.                            */
/*****************************************************************************/
typedef struct {
	/* GRE destination IP */
	ADAP_Uint32 gre_dip;

	/* Virtual subnet ID (24bit) */
	ADAP_Uint32 virtual_subnet_id;

	/* Flow ID */
	ADAP_Uint8 flow_id;

	/* Outer destination MAC address */
	tEnetHal_MacAddr outer_dmac;
} adap_ipsec_sa_nvgre_params;


/*****************************************************************************/
/* adap_ipsec_sa_ipsec_params                                                */
/*                                                                           */
/* Parameters for IPSEC only encapsulation.                                  */
/*****************************************************************************/
typedef struct {
	/* Sestination MAC address */
	tEnetHal_MacAddr dmac;

	/* Source MAC address */
	tEnetHal_MacAddr smac;
} adap_ipsec_sa_ipsec_params;


/*****************************************************************************/
/* adap_ipsec_sa_vxlan_params                                                */
/*                                                                           */
/* Parameters for VxLAN over IPSEC encapsulation.                            */
/*****************************************************************************/
typedef struct {
	/* Destination IP */
	ADAP_Uint32 dip;

	/* VxLAN network identifier (VNI)  */
	ADAP_Uint32 network_id;

	/* Source & destination ports */
	ADAP_Uint16 sport;
	ADAP_Uint16 dport;

	/* TTL */
	ADAP_Uint8 ttl;

	/* ??? (3 bits) */
	ADAP_Uint8 sgw;

	/* destination MAC address */
	tEnetHal_MacAddr outer_dmac;
} adap_ipsec_sa_vxlan_params;


/*****************************************************************************/
/* adap_ipsec_sa                                                             */
/*                                                                           */
/* Security Association (SA) parameters                                      */
/*****************************************************************************/
typedef struct {
	/* SA selectors */
	adap_ipsec_sa_selectors selectors;

	/* IPsec mode */
	adap_ipsec_mode mode;

	/* Is extended sequence number allowed */
	adap_ipsec_proto protocol;

	/* Authentication & Encryption algorithms */
	adap_auth_algorithm auth_alg;
	adap_encr_algorithm encr_alg;

	/* Authentication key */
#define ADAP_AUTH_KEY_MAX_LENGTH 32
	ADAP_Uint8 auth_key[ADAP_AUTH_KEY_MAX_LENGTH];

	/* Encryption key */
#define ADAP_ENCR_KEY_MAX_LENGTH 32
	ADAP_Uint8 encr_key[ADAP_ENCR_KEY_MAX_LENGTH];

	/* Nonce value (if needed) */
	ADAP_Uint32 nonce;

	/* Is sequence number overflow allowed */
	ADAP_Bool seqnum_overflow_en;

	/* Is extended sequence number allowed */
	ADAP_Bool extended_seqnum;

	/* Anti-replay window size (defined in RFC 4301) */
	ADAP_Uint16 anti_replay_window_size;

	/* Is TFC padding enabled */
	ADAP_Bool tfc_en;

	/* Forced output port */
	ADAP_Uint8 output_port;

	/* Encapsulation type */
	adap_ipsec_policy_encapsulation encap;

	/* Encapsultion parameters according to <encap> value  */
	union {
		/* <encap> is VXLAN */
		adap_ipsec_sa_vxlan_params vxlan;

		/* <encap> is NVGRE */
		adap_ipsec_sa_nvgre_params nvgre;

		/* <encap> is IPSEC */
		adap_ipsec_sa_ipsec_params ipsec;
	};
} adap_ipsec_sa;



/*****************************************************************************/
/* ipsec_policy_port_selectors                                               */
/*                                                                           */
/* IPsec policy selectors for ports protocols (TCP, UDP, SCTP, ...)          */
/*****************************************************************************/
typedef struct {
	/* Mode of source port selector */
	adap_ipsec_policy_selector_mode sport_mode;

	/* Range of source ports (valid in case sport_mode=VALUE) */
	ADAP_Uint16 sport_start;
	ADAP_Uint16 sport_end;

	/* Mode of destination port selector */
	adap_ipsec_policy_selector_mode dport_mode;

	/* Range of destination ports (valid in case dport_mode=VALUE) */
	ADAP_Uint16 dport_start;
	ADAP_Uint16 dport_end;
} adap_ipsec_policy_port_selectors;


/*****************************************************************************/
/* ipsec_policy_icmp_selectors                                               */
/*                                                                           */
/* IPsec policy selectors for ICMP protocol                                  */
/*****************************************************************************/
typedef struct {
	/* Mode of type selector (can be ANY of VALUE) */
	adap_ipsec_policy_selector_mode type_mode;

	/* ICMP type (valid in case type_mode=VALUE) */
	ADAP_Uint8 type;

	/* Mode of code selector (can be ANY of VALUE) */
	adap_ipsec_policy_selector_mode code_mode;

	/* Range of ICMP codes (valid in case code_mode=VALUE) */
	ADAP_Uint8 code_start;
	ADAP_Uint8 code_end;
} adap_ipsec_policy_icmp_selectors;



/*****************************************************************************/
/* adap_ipsec_policy_selectors (defined in RFC 4301)                         */
/*****************************************************************************/
typedef struct {
	/* Source IPv4 address & mask (0-32) */
	ADAP_Uint32 sip;
	ADAP_Uint32 sip_mask;

	/* Destination IPv4 address & mask (0-32) */
	ADAP_Uint32 dip;
	ADAP_Uint32 dip_mask;

	/* Mode of next_proto selector (can be ANY or VALUE) */
	adap_ipsec_policy_selector_mode next_proto_mode;

	/* Next header of the IP header (valid in case type_mode=VALUE) */
	ADAP_Uint8 next_proto;

	/* Selectors according to <next_proto> value  */
	union {
		 /* <next_proto> is TCP, UDP or SCTP */
		adap_ipsec_policy_port_selectors  port_selectors;

		/* <next_proto> is ICMP */
		adap_ipsec_policy_icmp_selectors  icmp_selectors;
	};
} adap_ipsec_policy_selectors;


/*****************************************************************************/
/* adap_ipsec_policy                                                         */
/*                                                                           */
/* IPsec policy parameters                                                   */
/*****************************************************************************/
typedef struct {
	/* Direction of the traffic with respect to the IPsec boundary */
	adap_ipsec_policy_direction dir;

	/* Ingress ifindex */
	ADAP_Uint32 ifindex;

	/* VLAN ID (if tagged) */
	ADAP_Bool tagged;
	ADAP_Uint32 vlan_id;

	/* IPsec policy selectors */
	adap_ipsec_policy_selectors selectors;

	/* Priority of policy in the range 0-31 (May not be unique) */
	ADAP_Uint8 priority;

	/* Action to be taken */
	adap_ipsec_policy_action action;
} adap_ipsec_policy;





typedef ADAP_Uint32 adap_ipsec_policy_id;
typedef ADAP_Uint32 adap_ipsec_sa_id;



typedef struct {
	tEnetHal_MacAddr nvgre_smac;
	ADAP_Uint16 nvgre_vlan;
	ADAP_Uint8 nvgre_ttl;
	ADAP_Uint32 nvgre_sip;

	ADAP_Uint32 my_ip;
	ADAP_Uint8 ipsec_ttl;
} adap_ipsec_params;



/**
 * @brief Initialize IPsec module.
 *
 * @note  If module is already initialized the operation returns ADAP_IPSEC_RC_SUCCESS without effect.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_GENERIC_ERROR if an error occurred.
 */
EnetHal_Status_t EnetHal_ipsec_init(adap_ipsec_params *params);


/**
 * @brief De-initialize IPsec module.
 *
 * @note  If module is already un-initialized the operation returns ADAP_IPSEC_RC_SUCCESS without effect.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_GENERIC_ERROR if an error occurred.
 */
EnetHal_Status_t EnetHal_ipsec_deinit(void);


/**
 * @brief Create a new IPsec policy.
 *
 * @param[in]  policy     - policy parameters (see description for adap_ipsec_policy).
 * @param[out] policy_id  - ID that uniquely identifies the policy.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_INVALID_PARAM if one of the arguments is not valid (see description for adap_ipsec_policy).
 *         ENETHAL_STATUS_NULL_PARAM if unexpected NULL encountered.
 *         ENETHAL_STATUS_NOT_SUPPPORTED if combination of parameters in <policy> is not supported (see description for adap_ipsec_policy).
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_create_policy(adap_ipsec_policy *policy,
		                                     adap_ipsec_policy_id *policy_id);


/**
 * @brief Retrieve IPsec policy.
 *
 * @param[in]  policy_id  - ID that uniquely identifies the policy
 * @param[out] policy     - policy parameters (see description for adap_ipsec_policy).
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <policy_id> doesn't exist.
 *         ENETHAL_STATUS_NULL_PARAM if unexpected NULL encountered.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_get_policy(adap_ipsec_policy_id policy_id,
		                                  adap_ipsec_policy *policy);


/**
 * @brief Destroy an IPsec policy.
 *
 * @note Any SA that may be attached to the policy will be detached.
 *
 * @param[in] policy_id   - ID that uniquely identifies the policy.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <policy_id> doesn't exist.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_destroy_policy(adap_ipsec_policy_id policy_id);


/**
 * @brief Create a new IPsec Security Association (SA).
 *
 * @param[in]  sa     - SA parameters (see description for adap_ipsec_sa).
 * @param[out] sa_id  - ID that uniquely identifies the SA.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_INVALID_PARAM if one of the arguments is not valid (see description for adap_ipsec_sa).
 *         ENETHAL_STATUS_NULL_PARAM if unexpected NULL encountered.
 *         ENETHAL_STATUS_NOT_SUPPPORTED if combination of parameters in <policy> is not supported (see description for adap_ipsec_sa).
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_create_sa(adap_ipsec_sa *sa,
		                                 adap_ipsec_sa_id *sa_id);


/**
 * @brief Retrieve the Security Association (SA).
 *
 * @param[in]  sa_id   - ID that uniquely identifies the SA.
 * @param[out] sa      - SA parameters (see description for adap_ipsec_sa).
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <sa_id> doesn't exist.
 *         ENETHAL_STATUS_NULL_PARAM if unexpected NULL encountered.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_get_sa(adap_ipsec_sa_id sa_id,
		                              adap_ipsec_sa *sa);


/**
 * @brief Destroy a Security Association (SA).
 *
 * @note The SA will be detached from all policies.
 *
 * @param[in] sa_id   - ID that uniquely identifies the SA.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <sa_id> doesn't exist.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_destroy_sa(adap_ipsec_sa_id sa_id);


/**
 * @brief Attach a Security Association (SA) to an IPsec policy.
 *
 * @param[in]  policy_id  - ID that uniquely identifies the policy.
 * @param[in]  sa_id      - ID that uniquely identifies the SA.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <policy_id> or <sa_id> don't exist.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_attach_sa(adap_ipsec_policy_id policy_id,
		                                 adap_ipsec_sa_id sa_id);


/**
 * @brief Detach a Security Association (SA) from an IPsec policy.
 *
 * @note If no SA is attached to the policy the operation returns ADAP_IPSEC_RC_SUCCESS without effect.
 *
 * @param[in]  policy_id   - ID that uniquely identifies the policy.
 *
 * @return ENETHAL_STATUS_SUCCESS if operation succeeded.
 *         ENETHAL_STATUS_NOT_FOUND if <policy_id> doesn't exist.
 *         ENETHAL_STATUS_NOT_INITIALIZED - IPsec module is not initialized.
 */
EnetHal_Status_t EnetHal_ipsec_detach_sa(adap_ipsec_policy_id policy_id);


#endif /*__ADAP_IPSEC_H__ */
