/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
MEA_Status MeaCreateCluster(char *name,MEA_Port_t 	enetPort,ENET_QueueId_t  *queueId);
MEA_Status MeaSetCluster_Shaper(ENET_QueueId_t  queueId,MEA_Bool enable,ADAP_Uint32 cir,ADAP_Uint32 cbs);
MEA_Status MeaSetCluster_PriQShaper(ENET_QueueId_t  queueId,ADAP_Uint32 PriQIdx,MEA_Bool enable, ADAP_Uint32 cir,ADAP_Uint32 cbs);
MEA_Status MeaDeleteCluster(ENET_QueueId_t  queueId);

