	/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"

#include "adap_lag_config.h"
#include "adap_service_config.h"
#include "mea_drv_common.h"
#include "adap_enetConvert.h"
#include "adap_vlanminp.h"
#include "adap_lanp.h"
#include "adap_stp.h"
#include "EnetHal_L2_Api.h"

ADAP_Int32  EnetHal_FsLaHwCreateAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 *pu2HwAggId)
{
	ADAP_Uint16 virtualPort;

	Adap_SetLagGlobalLag(1);

	if(adap_getLagInstanceFromAggrId(u2AggIndex) != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwCreateAggGroup: aggrIndex:%d already exist\r\n",u2AggIndex);
		return ENET_SUCCESS;
	}
	if(adap_CreateLagInstace(u2AggIndex) == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwCreateAggGroup: aggrIndex:%d failed to create instance\r\n",u2AggIndex);
		return ENET_FAILURE;
	}
	*pu2HwAggId = adap_getLagIndexFromAggrId(u2AggIndex);

	if(*pu2HwAggId == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwCreateAggGroup: aggrIndex:%d failed to get index\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

	virtualPort = adap_getFreeVirtualPort();

	if(virtualPort == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwCreateAggGroup: error get free virtual port\r\n");
		return ENET_FAILURE;
	}
	adap_SetLagVirtualPort(u2AggIndex,virtualPort);

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwAddLinkToAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber, ADAP_Uint16 *pu2HwAggId)
{
	/* To prevent redundant Agg port conf */
	if(adap_isLagPortFromAggrId(u2AggIndex,u2PortNumber) == ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"EnetHal_FsLaHwAddLinkToAggGroup: aggrIndex:%d port:%d already configured\r\n",u2AggIndex,u2PortNumber);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit\r\n");
		*pu2HwAggId = adap_getLagIndexFromAggrId(u2AggIndex);
		if(*pu2HwAggId == ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddLinkToAggGroup: aggrIndex:%d failed to get index\r\n",u2AggIndex);
			return ENET_FAILURE;
		}
		return ENET_SUCCESS;
	}
	if(EnetHal_FsLaHwAddPortToConfAggGroup (u2AggIndex,u2PortNumber) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddLinkToAggGroup: aggrIndex:%d failed to add link to port\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

	*pu2HwAggId = adap_getLagIndexFromAggrId(u2AggIndex);

	if(*pu2HwAggId == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddLinkToAggGroup: aggrIndex:%d failed to get index\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwSetSelectionPolicy (ADAP_Uint16 u2AggIndex, ADAP_Uint8 u1SelectionPolicy)
{
	MEA_Uint32  enet_selection=0;
	MEA_Port_t 	enetPort;
	MEA_Uint32		issPort=0;

	if(adap_getLagInstanceFromAggrId(u2AggIndex) == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwSetSelectionPolicy: aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwSetSelectionPolicy: new iss selection:0x%x\r\n",u1SelectionPolicy);

	 if(adap_setLagPolicyFromAggrId(u2AggIndex,u1SelectionPolicy) == ENET_FAILURE)
	 {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwSetSelectionPolicy: aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_FAILURE;
	 }

	 switch(u1SelectionPolicy)
	 {
		 case ENET_LA_SA:
		 case ENET_LA_DA:
		 case ENET_LA_SA_DA:
		 case ENET_LA_VLAN_ID:
		 case ENET_LA_SA_VID:
		 case ENET_LA_DA_VID:
		 case ENET_LA_SA_DA_VID:
			 enet_selection |= MEA_PARSER_DIST_SET_L2;
			 break;
		 case ENET_LA_SA_IP:
		 case ENET_LA_DA_IP:
		 case ENET_LA_SA_DA_IP:
		 case ENET_LA_ISID:
		 case ENET_LA_DA_IP6:
		 case ENET_LA_SA_IP6:
		 case ENET_LA_L3_PROTOCOL:
			 enet_selection |= MEA_PARSER_DIST_SET_L3;
			 break;
		 case ENET_LA_DA_L4_PORT:
		 case ENET_LA_SA_L4_PORT:
			 enet_selection |= MEA_PARSER_DIST_SET_L4;
			 break;
		 case ENET_LA_MPLS_VC_LABEL:
		 case ENET_LA_MPLS_TUNNEL_LABEL:
		 case ENET_LA_MPLS_VC_TUNNEL:
			 enet_selection |= MEA_PARSER_DIST_SET_MPLS;
			 break;
		 default:
			 printf("Unsupported policy:%d\n", u1SelectionPolicy);
			 return ENET_FAILURE;
	 }

	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enet selection:%d\n",enet_selection);

	for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
	{
		if(MeaAdapGetPhyPortFromLogPort(issPort, &enetPort) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",issPort, enetPort);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", issPort);
			return ENET_FAILURE;
	    }

		Adap_SetLagSetLagSelection (enetPort,enet_selection);
	}

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwSetSelectionPolicyBitList (ADAP_Uint16 u2AggIndex, ADAP_Uint32 u4SelectionPolicyBitList)
{
	MEA_Uint32  enet_selection=0;
	MEA_Port_t 	enetPort;
	MEA_Uint32	issPort=0;

	if(adap_getLagInstanceFromAggrId(u2AggIndex) == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwSetSelectionPolicyBitList: aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwSetSelectionPolicyBitList: new iss selection:0x%x\r\n",u4SelectionPolicyBitList);

	if(adap_setLagPolicyFromAggrId(u2AggIndex,u4SelectionPolicyBitList) == ENET_FAILURE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwSetSelectionPolicyBitList: aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_FAILURE;
	}

    if( (u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_MAC) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_DST_MAC) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_DST_MAC) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_VLAN_ID) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_MAC_SRC_VID) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_MAC_DST_VID) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_SERVICE_INSTANCE) )
    {
    	enet_selection |= MEA_PARSER_DIST_SET_L2;
    }
    if( (u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_IP) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_DST_IP) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_DST_IP) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_DST_IP6) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_IP6) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_L3_PROTOCOL) )
    {
    	enet_selection |= MEA_PARSER_DIST_SET_L3;
    }
    if( (u4SelectionPolicyBitList & ENET_LA_SELECT_DST_L4_PORT) || (u4SelectionPolicyBitList & ENET_LA_SELECT_SRC_L4_PORT) )
    {
    	enet_selection |= MEA_PARSER_DIST_SET_L4;
    }

    if( (u4SelectionPolicyBitList & ENET_LA_SELECT_MPLS_VC_LABEL) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_MPLS_TUNNEL_LABEL) ||
    	(u4SelectionPolicyBitList & ENET_LA_SELECT_MPLS_VC_TUNNEL) )
    {
    	enet_selection |= MEA_PARSER_DIST_SET_MPLS;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enet selection:%d\n",enet_selection);

	for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
	{
		if(MeaAdapGetPhyPortFromLogPort(issPort, &enetPort) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",issPort, enetPort);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", issPort);
			return ENET_FAILURE;
	    }

		Adap_SetLagSetLagSelection (enetPort,enet_selection);
	}

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwInit (void)
{
	Adap_SetLagGlobalLag(1);
	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwDeInit (void)
{
	Adap_SetLagGlobalLag(0);
	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwEnableCollection (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber)
{
	if(adap_getLagInstanceFromAggrId(u2AggIndex) == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwEnableCollection: adap_getLagInstanceFromAggrId aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_SUCCESS;
	}

	if(adap_EnableLagPortFromAggrId(u2AggIndex,u2PortNumber) == ENET_FAILURE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwEnableCollection: adap_EnableLagPortFromAggrId aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_SUCCESS;
	}

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwSetPortChannelStatus (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2Inst, ADAP_Uint8 u1StpState)
{
	if (adap_IsPortChannel(u2AggIndex) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"No need to set state of Agg Index %d, This is not a LAG group \n", u2AggIndex);
		return ENET_SUCCESS;
	}

	eAdap_StpMode mode;
	ADAP_Uint8 u1CurrPortState;
	if(adap_GetSpanningTreeMode(u2AggIndex,&mode,&u1CurrPortState) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwSetPortChannelStatus: ERROR  getting stp mode :%d \n", u2AggIndex);
		return ENET_FAILURE;
	}
	if(mode == ENET_SPANNING_TREE_RSTP_MODE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"\n RSTP call to EnetHal_MiRstpNpSetPortState:%d\n",u1StpState);

		if (EnetHal_MiRstpNpSetPortState (0,u2AggIndex,u1StpState) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set RSTP state: %d\n",u1StpState);
			return ENET_FAILURE;
		}
	}
	else if(mode == ENET_SPANNING_TREE_MSTP_MODE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"\n MSTP call to EnetHal_MiMstpNpSetInstancePortState:%d\n",u1StpState);
		if(u2Inst < ADAP_MAX_NUM_OF_MSTP_INSTANCES)
		{
			if (EnetHal_MiMstpNpSetInstancePortState (0,u2AggIndex,u2Inst,u1StpState) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set MSTP state: %d\n",u1StpState);
				return ENET_FAILURE;
			}
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MSTP instance is out of scope: %d\n", u2Inst);
			return ENET_FAILURE;
		}
	}

	return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_FsLaHwAddPortToConfAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber)
{
	MEA_Port_t 	EnetPortId;
	ADAP_Uint16		virtualPort=0;
	ADAP_Uint16		lagId=0;
	MEA_LxCp_t   LxCp_Id;
	ADAP_Uint16 		u2HwAggId;
	tEnetHal_VlanId VlanId;
	ADAP_Uint16 u2LagMasterPort, masterPortDefServId;
	MEA_Port_t enetPort;

	//MeaAdapSetEgressPort(127,0,1);

	if(MeaAdapGetPhyPortFromLogPort(u2PortNumber, &EnetPortId) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u2PortNumber, EnetPortId);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u2PortNumber);
		return ENET_FAILURE;
    }

	if(adap_getLagInstanceFromAggrId(u2AggIndex) == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d not exist create one\r\n",u2AggIndex);
		if(EnetHal_FsLaHwCreateAggGroup (u2AggIndex,&u2HwAggId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d failed to create aggr\r\n",u2AggIndex);
			return ENET_FAILURE;
		}
	}

	if(adap_isLagPortFromAggrId(u2AggIndex,u2PortNumber) == ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d port:%d already configured\r\n",u2AggIndex,u2PortNumber);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit\r\n");
		return ENET_SUCCESS;
	}

	 if(adap_setLagPortFromAggrId(u2AggIndex,u2PortNumber) == ENET_FAILURE)
	 {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d port:%d already configuredr\n",u2AggIndex,u2PortNumber);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit\r\n");
		return ENET_SUCCESS;
	 }

	virtualPort = adap_GetLagVirtualPort(u2AggIndex);

	if(virtualPort == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddPortToConfAggGroup: virtualport undefined:%d\r\n",u2AggIndex);
		return ENET_FAILURE;
	}
	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwAddPortToConfAggGroup: virtualport:%d\r\n",virtualPort);

	Adap_SetIngressMasterLag (EnetPortId,virtualPort);
	adap_PortSetVirtualPort(u2PortNumber,virtualPort);

	lagId = adap_GetEnetLagId(u2AggIndex);
	adap_PortSetEnetLagId(u2PortNumber,lagId);

	if(lagId == ADAP_END_OF_TABLE)
	{
		adap_SetLagMasterPort(u2AggIndex,u2PortNumber);
		adap_SetMasterPort(u2PortNumber,u2PortNumber);

		// Remove port from all Vlans including Vlan 0 (for priority services)
		for(VlanId=0;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
		{
			if( (adap_VlanCheckUntaggedPort(VlanId,u2PortNumber) == ADAP_TRUE) || (adap_VlanCheckTaggedPort(VlanId,u2PortNumber) == ADAP_TRUE) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d vlanId:%d\r\n",u2AggIndex,VlanId);
				EnetHal_FsMiVlanHwResetVlanMemberPort(0, VlanId, u2PortNumber);
			}
		}

		meaDrvDeleteDefaultService(u2PortNumber);

		// create master port
		if(Adap_CreateLagEntry(EnetPortId,&lagId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwAddPortToConfAggGroup: failed to create lag:%d\r\n",u2AggIndex);
			return ENET_FAILURE;
		}
		adap_SetEnetLagId(u2AggIndex,lagId);
		adap_PortSetEnetLagId(u2PortNumber,lagId);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwAddPortToConfAggGroup lagId:%d u2AggIndex:%d masterPort:%d virtualPort:%d created\r\n",lagId,u2AggIndex,EnetPortId,virtualPort);

		meaDrvCreateDefaultLagService(u2AggIndex);
	}
	else
	{
		u2LagMasterPort = adap_GetLagMasterPort(u2AggIndex);
		adap_SetMasterPort(u2PortNumber,u2LagMasterPort);

		// Remove port from all Vlans including Vlan 0 (for priority services)
		for(VlanId=0;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
		{
			if( (adap_VlanCheckUntaggedPort(VlanId,u2PortNumber) == ADAP_TRUE) || (adap_VlanCheckTaggedPort(VlanId,u2PortNumber) == ADAP_TRUE) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwAddPortToConfAggGroup: aggrIndex:%d vlanId:%d\r\n",u2AggIndex,VlanId);
				EnetHal_FsMiVlanHwResetVlanMemberPort(0, VlanId, u2PortNumber);
			}
		}

		meaDrvDeleteDefaultService(u2PortNumber);

		Adap_AddLagEntry(EnetPortId,lagId);

		printf("EnetHal_FsLaHwAddPortToConfAggGroup lagId:%d u2AggIndex:%d masterPort:%d virtualPort:%d added\r\n",lagId,u2AggIndex,EnetPortId,virtualPort);

		if (u2LagMasterPort != ADAP_END_OF_TABLE)
		{
			masterPortDefServId = adap_GetDefaultSid(u2AggIndex);
			if(MeaAdapGetPhyPortFromLogPort(u2PortNumber, &enetPort) == ENET_SUCCESS)
			{
				printf("logPort:%d, enetPortId:%d\n",u2PortNumber, enetPort);
			}
			else
			{
				printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u2PortNumber);
				return ENET_FAILURE;
		    }
			MeaDrvHwSetDefaultService (enetPort, masterPortDefServId, MEA_DEF_SID_ACTION_DEF_SID);
			adap_SetDefaultSid(u2PortNumber, masterPortDefServId);
		}
	}
	if(getLxcpId(u2PortNumber,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
		return ENET_FAILURE;
	}
	MeaDrvVlanHwLacpLxcpUpdate (MEA_TRUE,LxCp_Id);

	if(adap_NumOfLagPortFromAggrId(u2AggIndex) > 1)
	{
		//DPL_MiVlanHwDelPvidVlanEntry (0,u2PortNumber);
		if(setPortToPvid(u2PortNumber,0) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to setPortToPvid: port%d\r\n",u2PortNumber);
			return ENET_FAILURE;
		}
	}

	MeaAdapSetEgressPort(127,1,1);

	return ENET_SUCCESS;

}

ADAP_Int32  EnetHal_FsLaHwRemovePortFromConfAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber)
{
	MEA_Port_t 		EnetPortId;
	ADAP_Uint16		virtualPort=0;
	ADAP_Uint16		lagId=0;
	ADAP_Uint32		newIssMasterPort=0;
	ENET_QueueId_t  newMasterclusterId = MEA_PLAT_GENERATE_NEW_ID;
	MEA_Uint32		issPort=0;

	// this function remove the port from HW

	if(MeaAdapGetPhyPortFromLogPort(u2PortNumber, &EnetPortId) == ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"logPort:%d, enetPortId:%d\n",u2PortNumber, EnetPortId);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to convert u4IfIndex:%d to the ENET portId\n", u2PortNumber);
		return ENET_FAILURE;
    }

	if(adap_getLagInstanceFromAggrId(u2AggIndex) == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwRemovePortFromConfAggGroup: aggrIndex:%d not exist\r\n",u2AggIndex);
		return ENET_SUCCESS;
	}

	if(adap_isLagPortFromAggrId(u2AggIndex,u2PortNumber) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwRemovePortFromConfAggGroup: aggrIndex:%d port:%d not found\r\n",u2AggIndex,u2PortNumber);
		return ENET_SUCCESS;
	}

	virtualPort = adap_GetLagVirtualPort(u2AggIndex);

	if(virtualPort == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsLaHwRemovePortFromConfAggGroup: virtualport undefined:%d\r\n",u2AggIndex);
		return ENET_FAILURE;
	}
	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsLaHwRemovePortFromConfAggGroup: virtualport:%d\r\n",virtualPort);

	lagId = adap_GetEnetLagId(u2AggIndex);


	if(lagId == ADAP_END_OF_TABLE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MiFsLaHwRemovePortFromConfAggGroup: virtualport undefined:%d\r\n",u2AggIndex);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsLaHwRemovePortFromConfAggGroup: lagId:%d u2AggIndex:%d\r\n",lagId,u2AggIndex);

	if (Adap_clearIngressMasterLag (EnetPortId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Adap_clearIngressMasterLag: failed for enetPortId:%d\r\n", EnetPortId);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"before aggr:%d number of ports in aggr:%d\n",u2AggIndex,adap_NumOfLagPortFromAggrId(u2AggIndex));

	if(adap_NumOfLagPortFromAggrId(u2AggIndex) > 0)
	{
		if (Adap_RemoveLagEntry(EnetPortId,lagId,&newMasterclusterId) != MEA_OK)
		{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Adap_RemoveLagEntry: failed for enetPortId:%d, lagId:%d\r\n", EnetPortId, lagId);
				return ENET_FAILURE;
		}

		adap_SetMasterPort(u2PortNumber,ADAP_END_OF_TABLE);

		if((adap_GetLagMasterPort(u2AggIndex) == u2PortNumber) && (newMasterclusterId != MEA_PLAT_GENERATE_NEW_ID))
		{
			if(MeaAdapGetLogPortFromPhyPort(&newIssMasterPort, newMasterclusterId) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MiFsLaHwRemovePortFromConfAggGroup: invalid newMasterclusterId:%d\r\n",newMasterclusterId);
				return ENET_FAILURE;
			}

//			Update ports table for all ports except master
//			in this scope u2PortNumber is master
			for(issPort = 1; issPort < MeaAdapGetNumOfPhyPorts(); issPort++)
			{
				if ((adap_isLagPortFromAggrId(u2AggIndex, issPort) == ENET_SUCCESS) && (issPort != u2PortNumber) )
				{
					adap_SetMasterPort(issPort, newIssMasterPort);
				}
			}

			adap_PortSetEnetLagId(newIssMasterPort,lagId);

			if (adap_SetLagMasterPort(u2AggIndex,newIssMasterPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetLagMasterPort: failed for aggrIndex:%d, masterPort:%d\r\n", u2AggIndex, newIssMasterPort);
				return ENET_FAILURE;
			}

			adap_PortSetVirtualPort(u2PortNumber,virtualPort);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MiFsLaHwRemovePortFromConfAggGroup: affg:%d new master port:%d\n",u2AggIndex,newIssMasterPort);
		}

		if (adap_RemoveLagPortFromAggrId(u2AggIndex,u2PortNumber) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_RemoveLagPortFromAggrId: failed for aggrIndex:%d, ifIndex:%d\r\n", u2AggIndex, u2PortNumber);
			return ENET_FAILURE;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"after aggr:%d number of ports in aggr:%d\n",u2AggIndex,adap_NumOfLagPortFromAggrId(u2AggIndex));

		// if this is the last port then it need to remove the aggr
		if(newMasterclusterId == MEA_PLAT_GENERATE_NEW_ID)
		{
			adap_PortSetVirtualPort(u2PortNumber,ADAP_END_OF_TABLE);
			adap_PortSetEnetLagId(u2PortNumber,ADAP_END_OF_TABLE);

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MiFsLaHwRemovePortFromConfAggGroup: aggr:%d last port in LAG delete lag\r\n",u2AggIndex);

			if (adap_setFreeVirtualPort(virtualPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_setFreeVirtualPort: failed for virtualPort:%d\r\n", virtualPort);
				return ENET_FAILURE;
			}

			if(adap_DeleteLagInstace(u2AggIndex) != 	ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_DeleteLagInstace: aggrIndex:%d failed to delete\r\n",u2AggIndex);
				return ENET_FAILURE;
			}
			if (meaDrvDeleteDefaultService(u2AggIndex) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "meaDrvDeleteDefaultService: failed for aggrIndex:%d\r\n", u2AggIndex);
				return ENET_FAILURE;
			}
		}

		if (MeaDrvCreateDefaultService(u2PortNumber) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "MeaDrvCreateDefaultService: failed for ifIndex:%d\r\n", u2PortNumber);
			return ENET_FAILURE;
		}

		if (adap_ReconfigureVlanPriority(u2PortNumber) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "adap_ReconfigureVlanPriority: failed for ifIndex:%d\r\n", u2PortNumber);
			return ENET_FAILURE;
		}

	}

	return ENET_SUCCESS;
}
