/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "adap_types.h"
#include "adap_utils.h"
#include <stdio.h>

char* adap_mac_to_string(tEnetHal_MacAddr *mac_addr)
{
    static char str[sizeof("**:**:**:**:**:**") + 1];

    if(mac_addr == NULL) {
    	return "";
    }

    snprintf(str, sizeof(str), "%02x:%02x:%02x:%02x:%02x:%02x",
    		mac_addr->ether_addr_octet[0], mac_addr->ether_addr_octet[1], mac_addr->ether_addr_octet[2],
			mac_addr->ether_addr_octet[3], mac_addr->ether_addr_octet[4], mac_addr->ether_addr_octet[5]);

    return str;
}


ADAP_Bool adap_is_mac_zero(tEnetHal_MacAddr *mac_addr)
{
	tEnetHal_MacAddr zero_mac = {.ether_addr_octet = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

	if(adap_memcmp(mac_addr, &zero_mac, sizeof(tEnetHal_MacAddr)) != 0)	{
		return ADAP_FALSE;
	}

	return ADAP_TRUE;
}

ADAP_Bool adap_is_mac_equal(tEnetHal_MacAddr *mac_addr, ADAP_Uint8 *arr)
{
	if(adap_memcmp(mac_addr->ether_addr_octet, arr, sizeof(tEnetHal_MacAddr)) != 0)	{
		return ADAP_FALSE;
	}

	return ADAP_TRUE;
}

ADAP_Bool adap_is_mac_multicast(tEnetHal_MacAddr *mac_addr)
{

	if ((mac_addr->ether_addr_octet[0] & 0x01) != 0) {
		return ADAP_TRUE;
	}
	return ADAP_FALSE;
}

void adap_mac_from_arr(tEnetHal_MacAddr *mac_addr, ADAP_Uint8 *arr)
{
	adap_memcpy(mac_addr->ether_addr_octet, arr, ETH_ALEN);
}

void adap_mac_to_arr(tEnetHal_MacAddr *mac_addr, ADAP_Uint8 *arr)
{
	adap_memcpy(arr, mac_addr->ether_addr_octet, ETH_ALEN);
}

void adap_apply_mask_to_mac(tEnetHal_MacAddr *mac_addr, ADAP_Uint64 macAddrMask, tEnetHal_MacAddr *result)
{
    int i;
    ADAP_Uint64 mask = macAddrMask;
    ADAP_Uint8 octet;
    for (i = ETH_ALEN-1; i >= 0 ; i--)
    {
        octet = mask & 0x00000000000000FF;
        result->ether_addr_octet[i] = mac_addr->ether_addr_octet[i] & octet;
        mask = mask >> 8;
    }
}


char* adap_ipv6_to_string(tEnetHal_IPv6Addr *ipv6_addr)
{
    static char str[sizeof("****:****:****:****:****:****:****:****") + 1];

    if (ipv6_addr == NULL) {
    	return "";
    }

    snprintf(str, sizeof(str), "%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x",
    		ipv6_addr->s6_addr[0], ipv6_addr->s6_addr[1], ipv6_addr->s6_addr[2],
			ipv6_addr->s6_addr[3], ipv6_addr->s6_addr[4], ipv6_addr->s6_addr[5],
			ipv6_addr->s6_addr[6], ipv6_addr->s6_addr[7], ipv6_addr->s6_addr[8],
			ipv6_addr->s6_addr[9], ipv6_addr->s6_addr[10], ipv6_addr->s6_addr[11],
			ipv6_addr->s6_addr[12], ipv6_addr->s6_addr[13], ipv6_addr->s6_addr[14],
			ipv6_addr->s6_addr[15]);

    return str;
}

ADAP_Bool adap_is_ipv6_zero(tEnetHal_IPv6Addr *ipv6_addr)
{
	tEnetHal_IPv6Addr zero_ipv6 = {.s6_addr = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

	if (adap_memcmp(ipv6_addr, &zero_ipv6, sizeof(tEnetHal_IPv6Addr)) != 0)	{
		return ADAP_FALSE;
	}

	return ADAP_TRUE;
}

