/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#include "adap_srv_rm.h"

#include "adap_index_pool.h"
#include "adap_hashmap.h"
#include "adap_utils.h"
#include "adap_logger.h"
#include "mea_api.h"


static ADAP_Uint8 max_entries_per_qos_table;
static adap_hashmap * qos_table_info_map = NULL;

static ADAP_Uint8 max_entries_per_action_table;
static adap_index_pool * action_table_pool = NULL;
static adap_hashmap * action_table_info_map = NULL;

static ADAP_Bool g_initialized = ADAP_FALSE;

struct adap_srv_rm_action_table_hash_element {
	adap_hashmap_element map_item;
	adap_index_pool * action_ids_pool;
};

struct adap_srv_rm_qos_table_hash_element {
	adap_hashmap_element map_item;
	adap_index_pool * entry_ids_pool;
};


EnetHal_Status_t adap_srv_rm_init(struct adap_srv_rm_params *params)
{
	struct adap_index_pool_params pool_params;
	struct adap_hashmap_params hash_params;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		if (params == NULL) {
			rc = ENETHAL_STATUS_NULL_PARAM;
			goto bail;
		}

		pool_params.thread_safe = ADAP_FALSE;
		pool_params.capacity = params->max_action_tables;
		rc = adap_index_pool_init(&action_table_pool, &pool_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		hash_params.capacity = params->max_action_tables;
		hash_params.hash = &identity_func;
		hash_params.flush = NULL;
		hash_params.num_of_buckets = 16; /* TODO - what should that be ? */
		hash_params.thread_safe = ADAP_FALSE;
		rc = adap_hashmap_init(&action_table_info_map, &hash_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		max_entries_per_action_table = params->max_entries_per_action_table;



		hash_params.capacity = params->max_qos_tables;
		hash_params.hash = &identity_func;
		hash_params.flush = NULL;
		hash_params.num_of_buckets = 16; /* TODO - what should that be ? */
		hash_params.thread_safe = ADAP_FALSE;
		rc = adap_hashmap_init(&qos_table_info_map, &hash_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		max_entries_per_qos_table = params->max_entries_per_qos_table;

		g_initialized = ADAP_TRUE;
	}

bail:
	/* TODO - rollback */
	return rc;
}

EnetHal_Status_t adap_srv_rm_deinit(void)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_TRUE) {
		rc = adap_index_pool_deinit(action_table_pool);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = adap_hashmap_deinit(action_table_info_map);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = adap_hashmap_deinit(qos_table_info_map);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		g_initialized = ADAP_FALSE;
	}

bail:
	return rc;
}


EnetHal_Status_t adap_srv_rm_alloc_qos_table(MEA_TFT_t *qos_table_id)
{
	struct adap_index_pool_params pool_params;
	struct adap_srv_rm_qos_table_hash_element *hash_element = NULL;
	MEA_TFT_entry_data_dbt tft_entry_data_dbt;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Status mea_status;

	ADAP_Bool profile_allocated = ADAP_FALSE;
	ADAP_Bool pool_created = ADAP_FALSE;
	ADAP_Bool hash_item_inserted = ADAP_FALSE;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (qos_table_id == NULL) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	tft_entry_data_dbt.en_rule_type = ADAP_TRUE;
	tft_entry_data_dbt.rule_type = 0x7;
	mea_status = MEA_API_TFT_Create_QoS_Profile(MEA_UNIT_0,
			                                    qos_table_id,
												&tft_entry_data_dbt);
	if (mea_status != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed on TFT profile create.\n");
		rc = ENETHAL_STATUS_MEA_ERROR;
		goto bail;
	}
	profile_allocated = ADAP_TRUE;

	hash_element = (struct adap_srv_rm_qos_table_hash_element*)adap_malloc(sizeof(struct adap_srv_rm_qos_table_hash_element));
	if (hash_element == NULL) {
		rc = ENETHAL_STATUS_MEM_ALLOC;
		goto bail;
	}

	pool_params.thread_safe = ADAP_FALSE;
	pool_params.capacity = max_entries_per_qos_table;
	rc = adap_index_pool_init(&hash_element->entry_ids_pool, &pool_params);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}
	pool_created = ADAP_TRUE;

	rc = adap_hashmap_insert(qos_table_info_map, *qos_table_id, &hash_element->map_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}
	hash_item_inserted = ADAP_TRUE;

	profile_allocated = ADAP_FALSE;
	pool_created = ADAP_FALSE;
	hash_item_inserted = ADAP_FALSE;
	hash_element = NULL;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," QoS table ID %d allocated.\n", *qos_table_id);

bail:
	if (hash_item_inserted) {
		adap_hashmap_remove(qos_table_info_map, *qos_table_id, NULL);
	}
	if (pool_created && (hash_element != NULL)) {
		adap_index_pool_deinit(hash_element->entry_ids_pool);
	}
	if (profile_allocated) {
		/* TODO - rollback */
	}
	adap_safe_free(hash_element);
	return rc;
}

EnetHal_Status_t adap_srv_rm_free_qos_table(MEA_TFT_t qos_table_id)
{
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_qos_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Status mea_status;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	rc = adap_hashmap_remove(action_table_info_map, qos_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_qos_table_hash_element, map_item);

	rc = adap_index_pool_deinit(hash_element->entry_ids_pool);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	mea_status = MEA_API_TFT_Delete_QoS_Profile(MEA_UNIT_0,
  			                                    qos_table_id);
	if (mea_status != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed on TFT profile create.\n");
		rc = ENETHAL_STATUS_MEA_ERROR;
		goto bail;
	}

	free(hash_element);

bail:
	return rc;
}

EnetHal_Status_t adap_srv_rm_alloc_qos_entry(ADAP_Uint32 qos_table_id, ADAP_Uint8 *qos_entry_id)
{
	ADAP_Uint32 idx;
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_qos_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (qos_entry_id == NULL) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	rc = adap_hashmap_find(action_table_info_map, qos_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Can't allocate QoS entry (QoS table ID %d not allocated).\n", qos_table_id);
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_qos_table_hash_element, map_item);

	rc = adap_index_pool_alloc(hash_element->entry_ids_pool, &idx);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	*qos_entry_id = idx;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Action ID %d allocated in group %d.\n", idx, qos_table_id);

bail:
	return rc;
}

EnetHal_Status_t adap_srv_rm_free_qos_entry(ADAP_Uint32 qos_table_id, ADAP_Uint8 qos_entry_id)
{
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_qos_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	rc = adap_hashmap_find(action_table_info_map, qos_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_qos_table_hash_element, map_item);

	rc = adap_index_pool_free(hash_element->entry_ids_pool, qos_entry_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

bail:
	return rc;

}


EnetHal_Status_t adap_srv_rm_alloc_action_table(ADAP_Uint32 *action_table_id)
{
	ADAP_Uint32 idx;
	struct adap_index_pool_params pool_params;
	struct adap_srv_rm_action_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	ADAP_Bool index_allocated = ADAP_FALSE;
	ADAP_Bool pool_created = ADAP_FALSE;
	ADAP_Bool hash_item_inserted = ADAP_FALSE;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (action_table_id == NULL) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	rc = adap_index_pool_alloc(action_table_pool, &idx);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}
	index_allocated = ADAP_TRUE;

	hash_element = (struct adap_srv_rm_action_table_hash_element*)adap_malloc(sizeof(struct adap_srv_rm_action_table_hash_element));
	if (hash_element == NULL) {
		rc = ENETHAL_STATUS_MEM_ALLOC;
		goto bail;
	}

	pool_params.thread_safe = ADAP_FALSE;
	pool_params.capacity = max_entries_per_action_table;
	rc = adap_index_pool_init(&hash_element->action_ids_pool, &pool_params);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}
	pool_created = ADAP_TRUE;

	rc = adap_hashmap_insert(action_table_info_map, idx, &hash_element->map_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}
	hash_item_inserted = ADAP_TRUE;

	*action_table_id = idx;

	index_allocated = ADAP_FALSE;
	pool_created = ADAP_FALSE;
	hash_item_inserted = ADAP_FALSE;
	hash_element = NULL;

bail:
	if (hash_item_inserted) {
		adap_hashmap_remove(action_table_info_map, idx, NULL);
	}
	if (pool_created && (hash_element != NULL)) {
		adap_index_pool_deinit(hash_element->action_ids_pool);
	}
	if (index_allocated) {
		adap_index_pool_free(action_table_pool, idx);
	}
	adap_safe_free(hash_element);
	return rc;
}

EnetHal_Status_t adap_srv_rm_free_action_table(ADAP_Uint32 action_table_id)
{
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_action_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	rc = adap_hashmap_remove(action_table_info_map, action_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_action_table_hash_element, map_item);

	rc = adap_index_pool_deinit(hash_element->action_ids_pool);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	rc = adap_index_pool_free(action_table_pool, action_table_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	free(hash_element);

bail:
	return rc;
}

EnetHal_Status_t adap_srv_rm_alloc_action(ADAP_Uint32 action_table_id, ADAP_Uint8 *action_id)
{
	ADAP_Uint32 idx;
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_action_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (action_id == NULL) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	rc = adap_hashmap_find(action_table_info_map, action_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Can't allocate action ID (group ID %d not allocated).\n", action_table_id);
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_action_table_hash_element, map_item);

	rc = adap_index_pool_alloc(hash_element->action_ids_pool, &idx);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	*action_id = idx;

bail:
	return rc;
}

EnetHal_Status_t adap_srv_rm_free_action(ADAP_Uint32 action_table_id, ADAP_Uint8 action_id)
{
	adap_hashmap_element *hashmap_item = NULL;
	struct adap_srv_rm_action_table_hash_element *hash_element = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (g_initialized == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	rc = adap_hashmap_find(action_table_info_map, action_table_id, &hashmap_item);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	hash_element = container_of(hashmap_item, struct adap_srv_rm_action_table_hash_element, map_item);

	rc = adap_index_pool_free(hash_element->action_ids_pool, action_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

bail:
	return rc;
}
