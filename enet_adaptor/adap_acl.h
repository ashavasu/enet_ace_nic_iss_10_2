/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_ACL_H
#define ADAP_ACL_H

// layer 3 filter
#define ENET_L3_SRC_IPV4_FIELD  0x01
#define ENET_L3_DST_IPV4_FIELD  0x02
#define ENET_L3_SRC_PORT_FIELD  0x04
#define ENET_L3_DST_PORT_FIELD  0x08
#define ENET_L3_PROTOCOL_FIELD  0x10
#define ENET_L3_SRC_IPV6_FIELD  0x20
#define ENET_L3_DST_IPV6_FIELD  0x40
#define ENET_L3_SRC_IPV4_MASK_FIELD  0x80
#define ENET_L3_DST_IPV4_MASK_FIELD  0x100

// layer 2 filter
#define ENET_L2FLTR_SRC_MAC_ADD_FIELD   0x1
#define ENET_L2FLTR_DST_MAC_ADD_FIELD   0x2
#define ENET_L2FLTR_CVLAN_FIELD         0x4
#define ENET_L2FLTR_SVLAN_FIELD         0x8
#define ENET_L2FLTR_OUTER_ETHERTYPE_FIELD     0x10
#define ENET_L2FLTR_INNER_ETHERTYPE_FIELD     0x20
#define ENET_L2FLTR_SERVICE_EPRIORITY_FIELD     0x40
#define ENET_L2FLTR_CUSTOMER_EPRIORITY_FIELD     0x80
#define ENET_L2FLTR_PROTOCOL_TYPE_FIELD       0x100

#define ENET_L2FLTR_DOUBLE_FIELD          (ENET_L2FLTR_CVLAN_FIELD | ENET_L2FLTR_SVLAN_FIELD)

MEA_Status MeaDrvCreateLxcp(MEA_LxCp_t *pNewLxcp);
MEA_Status MeaDrvSetLxcpParamaters(MEA_LxCP_Protocol_te opcode,MEA_LxCP_Protocol_Action_te action,MEA_LxCp_t lxcp_Id,MEA_Action_t *pAction_id,MEA_OutPorts_Entry_dbt *pAction_outPorts);
MEA_Status meaDrvSetHpmPriority(MEA_TFT_t  tft_profile_Id,ADAP_Uint32 ipAddr,ADAP_Uint32 mask,ADAP_Uint32 entryId);
MEA_Status meaDrvClearHpmPriority(MEA_TFT_t  tft_profile_Id,ADAP_Uint32 entryId);
MEA_Status meaDrvSetIpv6HpmPriority(MEA_TFT_t  tft_profile_Id,tEnetHal_IPv6Addr *ipv6Addr,ADAP_Uint32 mask,ADAP_Uint32 entryId);
MEA_Status MeaDrvSetGlobalMacAddress(tEnetHal_MacAddr *macAddr,MEA_Bool addNewMac);
MEA_Status MeaDrvSetGlobalIpAddress(ADAP_Uint32 ipaddress,MEA_Bool addNewIp);
MEA_Status adap_IpRolesDbEntryValid(tIpRoleData *pRoleData,ADAP_Uint32 *entrymask);
MEA_Status MeaDrvGetFirstGlobalIpAddress(ADAP_Uint32 *ipaddress);
MEA_Status adap_SetAclFilterMaskParameters(tAdapAclFilterType *pAclType,MEA_ACL_mask_dbt *pAclMask,MEA_Filter_Key_dbt *pFilterKey,ADAP_Uint32 *pMaxNumOfAcl);
MEA_Status adap_SetAclFilterConfiguration(tAclFilterParams *pAclParams,ADAP_Uint32 acl_index,ADAP_Uint32 acl_profile);
ADAP_Int32 adap_HwProcessFilterHierarchy(ADAP_Int32 i4Value, eAclFilterTypes fType,
        tEnetHal_L2FilterEntry *pL2FilterEntry, tEnetHal_L3FilterEntry *pL3FilterEntry, tPolicyDb *pPolicy);
ADAP_Uint32 ConfigureArpRedirection(ADAP_Uint16 vlanId, ADAP_Uint16 portMap[MAX_NUM_OF_SUPPORTED_PORTS],
      ADAP_Bool to_cpu);
ADAP_Bool ArpRedirectionRulePresent(ADAP_Uint16 vlanId);
#endif
