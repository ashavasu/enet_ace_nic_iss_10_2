/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_ECFMMINP_H
#define ADAP_ECFMMINP_H

#include "EnetHal_L2_Api.h"


#define ADAP_D_ENET_CCM_TASK_PERIOD				50000
#define ADAP_D_ENET_CCM_CHECK_PERIOD			500000
#define ADAP_D_ENET_LINK_STATUS_PERIOD			200000
#define ADAP_D_ENET_LM_COLLECT_PERIOD			1000000

#define ADAP_D_ENET_LBM_LBR_OFFLOADING			0x01
#define ADAP_D_ENET_DMM_DMR_OFFLOADING			0x02
#define ADAP_D_ENET_1DM_OFFLOADING				0x04
#define ADAP_D_ENET_LMM_LMR_OFFLOADING			0x08
#define ADAP_D_ENET_TST_OFFLOADING				0x10
#define ADAP_D_ENET_CCM_OFFLOADING				0x20


#define ADAP_D_MAX_DATABASE_CCM_CONFIGURATION	10
#define ADAP_D_MAX_DATABASE_LBM_CONFIGURATION	10
#define ADAP_D_MAX_DATABASE_DMM_CONFIGURATION	10
#define ADAP_D_MAX_DATABASE_LMM_CONFIGURATION	10
#define ADAP_D_MAX_DATABASE_LMID_CONFIGURATION	16

#define ADAP_D_MAX_OF_CCM_SUB_ENTRIES			4
#define ADAP_D_MAX_CCM_INTERNAL_VLANS			100
#define ADAP_D_THRESHOULD_START_TIME			10 // * 100 mili

#define ADAP_ECFM_HW_HANDLER_SIZE				(ADAP_ECFM_HW_MEP_HANDLER_SIZE / 2)

#define ADAP_ECFM_NP_START_CCM_TX_ON_PORTLIST 1
#define ADAP_ECFM_NP_START_CCM_TX             2
#define ADAP_ECFM_NP_STOP_CCM_TX              3
#define ADAP_ECFM_NP_START_CCM_RX_ON_PORTLIST 4
#define ADAP_ECFM_NP_STOP_CCM_RX              5
#define ADAP_ECFM_NP_START_CCM_RX             6
#define ADAP_ECFM_NP_MA_CREATION              7
#define ADAP_ECFM_NP_MA_DELETION              8
#define ADAP_ECFM_NP_OFFLOAD_STATE            9
#define ADAP_ECFM_NP_MEP_CREATE               10
#define ADAP_ECFM_NP_MEP_DELETE               11

#define ADAP_ECFM_NP_MP_DIR_DOWN              	1  /* MP Direction */
#define ADAP_ECFM_NP_MP_DIR_UP                	2  /* MP Direction */

#define ADAP_ECFM_MAX_CHASSISID_LEN				255
#define ADAP_ECFM_MAX_PORTID_LEN				255

#define ADAP_ECFM_CALL_BACK                  	28
#define ADAP_MAX_IFNAME_SIZE            		36

#define	ADAP_D_ANALYZER_ETHERNITY_PORT			120

#define	ADAP_D_MAX_NUM_OF_GEN_PROFILES			8
#define	ADAP_D_MAX_NUM_OF_GENERATORS			16
#define	ADAP_D_MAX_NUM_OF_ANALYZERS				16

#define ADAP_D_CCM_1SEC_PERIOD					0x04

#define	ADAP_D_CCM_OPCODE_VALUE					1
#define	ADAP_D_LBR_OPCODE_VALUE					2
#define	ADAP_D_LBM_OPCODE_VALUE					3
#define	ADAP_D_LTR_OPCODE_VALUE					4
#define	ADAP_D_LTM_OPCODE_VALUE					5
#define	ADAP_D_AIS_OPCODE_VALUE					33
#define	ADAP_D_TST_OPCODE_VALUE					37
#define	ADAP_D_APS_OPCODE_VALUE					39
#define ADAP_D_LMR_OPCODE_VALUE					42
#define	ADAP_D_LMM_OPCODE_VALUE					43
#define	ADAP_D_1DM_OPCODE_VALUE					45
#define	ADAP_D_DMR_OPCODE_VALUE					46
#define	ADAP_D_DMM_OPCODE_VALUE					47

#define ADAP_ECFM_SUCCESS						0
#define ADAP_ECFM_FAILURE						1

#define ADAP_MAX_NUM_OF_VPN_GENERATORS			32
#define ADAP_START_VPN_GENERATORS				480

#define ADAP_FLAG_EVENT_TIMER    		1
#define ADAP_FLAG_EVENT_CCM    			2

#define ADAP_MAX_NUM_OF_VPNS					512

#define ADAP_E_CCM_UNEXPECTED_MEP_ID_EVENT		0x00000001
#define ADAP_E_CCM_UNEXPECTED_MEG_ID_EVENT		0x00000002
#define ADAP_E_CCM_UNEXPECTED_PERIOD_EVENT		0x00000004
#define ADAP_E_CCM_SEQUENCE_NUMBER_INCREMENT	0x00000008
#define ADAP_E_CCM_REORDER_EVENT				0x00000010
#define ADAP_E_CCM_RDI_BIT_SET_EVENT			0x00000020

#define	ADAP_D_CCM_3_3MS_PERIOD					0x01
#define	ADAP_D_CCM_10MS_PERIOD					0x02
#define	ADAP_D_CCM_100MS_PERIOD					0x03
#define	ADAP_D_CCM_10SEC_PERIOD					0x05
#define	ADAP_D_CCM_1MIN_PERIOD					0x06
#define	ADAP_D_CCM_10MIN_PERIOD					0x07

#define ADAP_E_COUNT_TIME_BEFORE_START_CCM		3 //// * 100 mili

#define ADAP_E_MAX_TIME_LOSSCCM_BEFORE_IDEN_LOSS		30

#define ADAP_D_MAX_OF_CCM_SUB_ENTRIES			4


#define ADAP_E_CCM_DEFECTS		( ADAP_E_CCM_UNEXPECTED_MEP_ID_EVENT | ADAP_E_CCM_UNEXPECTED_MEG_ID_EVENT | ADAP_E_CCM_UNEXPECTED_PERIOD_EVENT | ADAP_E_CCM_REORDER_EVENT | ADAP_E_CCM_RDI_BIT_SET_EVENT)

#define ADAP_ERPS_PORT_STATE_UNBLOCKING    ENET_AST_PORT_STATE_FORWARDING
#define ADAP_ERPS_PORT_STATE_BLOCKING      ENET_AST_PORT_STATE_DISCARDING

#define ADAP_ECFM_NP_MP_DIR_DOWN              1  /* MP Direction */
#define ADAP_ECFM_NP_MP_DIR_UP                2   /* MP Direction */

typedef ADAP_Uint32		tAdap_EcfmMsgType;

typedef enum
{
	ADAP_E_WAIT_ON_PACKET_STATE,
	ADAP_E_GOOD_PACKETS_STATE,
	ADAP_E_BAD_PACKETS_STATE,
}eAdap_CcmStateMachine;

typedef enum
{
	ADAP_E_APS_WORKING_PATH,
	ADAP_E_APS_PROTECTED_PATH,
	ADAP_E_APS_INGRESS_PATH
}eAdap_ApsServicePath;

typedef struct
{
	ADAP_Bool		bConfigured;
	MEA_Service_t 	Service_id;
	ADAP_Uint16 	VpnIndex;
	ADAP_Uint16		vlanId;
	ADAP_Uint16		policerId;
	ADAP_Uint16		srcPort;
	ADAP_Bool		lmEnable;
	ADAP_Bool		lmId;
	MEA_Action_t 	action_id;
	ADAP_Uint16		megLevel;
	ADAP_Uint16		opcode;
	MEA_Action_t    lmm_transmit_action;
	MEA_Action_t	received_action;

}tAdap_RmtSide;

typedef struct
{
	tAdap_RmtSide		tLbmDatabase;
	tAdap_RmtSide		tLbrDatabase;
	tAdap_RmtSide		tDmrDatabase;
	tAdap_RmtSide 		tLmrDatabase;
	tAdap_RmtSide 		tApsDatabase;
	tAdap_RmtSide 		tRpsDatabase;
	tAdap_RmtSide 		t1DmDatabase;
	tAdap_RmtSide 		t1LtmDatabase;
	tAdap_RmtSide 		t1LtrDatabase;
	tAdap_RmtSide 		tAisDatabase;
	tAdap_RmtSide 		tTstDatabase;
}tAdap_InterfaceRemoteSide;

typedef struct
{
	MEA_Uint32 	vpn;
	MEA_Uint16 	vlanId;
	MEA_Uint16 	EnetPolilcingProfile;
	MEA_Bool 	b_lmEnable;
	MEA_Uint16 	lmId;
	MEA_Uint16 	src_port;
	MEA_Uint32 	keyType;
	MEA_Uint32 	me_valid;
	MEA_Uint32 	me_level;
}tRmtSrvInfo;


typedef struct
{
	MEA_Service_t 	serviceId;
	tRmtSrvInfo		info;
	ADAP_Uint32		ref_count;
	MEA_Bool		valid;
}tAdap_PacketGenService;

typedef struct
{
	MEA_Action_t 	actionId;
	MEA_Uint16		vlanId;
	MEA_Uint16		outputPort;
	ADAP_Uint16 	VpnIndex;
	ADAP_Uint32		ref_count;
	MEA_Bool		valid;
}tAdap_PacketGenAction;

typedef struct
{
	ADAP_Uint16 internalVlan;
	ADAP_Uint16 VpnIndex;
	ADAP_Uint16 analyzerVpnIndex;
	MEA_PacketGen_profile_info_t  packetGenProfileId;
	MEA_PacketGen_t PacketGeId;
	MEA_Service_t 	serviceId;
	MEA_Action_t 	action_id;
	MEA_Analyzer_t 	analyzerId;
	MEA_Action_t 	analyzer_action_id;
	MEA_Action_t 	tx_action_id;
	MEA_Action_t 	rx_action_id;
	MEA_CcmId_t  	id_io;
}tEnetLbmPacketGen,tEnetDmmPacketGen,tEnetLmmPacketGen;

typedef struct
{
	MEA_Bool					valid;
	tEnetLbmPacketGen 			packetGen;
	tEnetHal_EcfmMepInfoParams 	tEcfmMepInfoParams;
}tAdap_LbmPacketGen;

typedef struct
{
	tEnetHal_MacAddr 		destMac;
	tEnetHal_MacAddr		srcMac;
	ADAP_Uint32 			meLevel;
	ADAP_Uint32 			version;
	ADAP_Uint32 			opCode;
	ADAP_Uint32 			flags;
	ADAP_Uint32 			tlvOffset;
	ADAP_Uint32 			u4IfIndex;
	tEnetHal_VlanTagInfo 	VlanTag;
	ADAP_Uint32				u1Direction;
	ADAP_Uint32 			lmId;
}tAdap_EcfmMepLmmInfoParams;

typedef struct
{
	MEA_Bool						valid;
	tEnetLmmPacketGen 				packetGen;
	tAdap_EcfmMepLmmInfoParams 	tEcfmMepInfoParams;
}tAdap_LmmPacketGen;

typedef struct
{
	tEnetHal_MacAddr 	destMac;
	tEnetHal_MacAddr	srcMac;
	ADAP_Uint32 		meLevel;
	ADAP_Uint32 		version;
	ADAP_Uint32 		opCode;
	ADAP_Uint32 		flags;
	ADAP_Uint32 		tlvOffset;
	ADAP_Uint32 		u4IfIndex;
	tEnetHal_VlanTag 	VlanTag;
	ADAP_Uint32 		u1Direction;
	ADAP_Uint32 		lmId;
}tAdap_EcfmMepDmmInfoParams;

typedef struct
{
	MEA_Bool						valid;
	tEnetDmmPacketGen 				packetGen;
	tAdap_EcfmMepDmmInfoParams 	tEcfmMepInfoParams;
}tAdap_DmmPacketGen,t1Adap_DmPacketGen;

typedef struct
{
	MEA_Bool			valid;
	ADAP_Uint32			lmId;
	ADAP_Uint16			vlanId;
	ADAP_Uint32			u4IfIndex;
	ADAP_Uint32			numOfOwners;
}tAdap_LmIdConfig;

typedef struct
{
	MEA_Bool			valid;
	ADAP_Uint16			Vlan;
}tAdap_InternalVlan;

typedef struct
{
	ADAP_Uint32 					packetRx;
	ADAP_Uint32 					packetTx;
	MEA_Counters_CCM_Defect_dbt 	ccmDefect;
	eAdap_CcmStateMachine		ccmState;
	ADAP_Uint32						timeCheckCcmTraffic;
	ADAP_Uint32						sendLossCCm;
	ADAP_Uint32						sendLossCCmThreshould;
	ADAP_Uint32						Good3CCmPackets;
	ADAP_Uint32						priodicSendPacketToCpu;
	ADAP_Bool						callback_loss_called;
}tAdap_CcmApplicationDb;

typedef struct
{
	MEA_Service_t 					Service_id;
	MEA_PacketGen_t 				packetGenId;
	MEA_PacketGen_profile_info_t  	packetGenProfileId;
	MEA_Action_t 					actionid;
	MEA_Bool						active;
	ADAP_Uint16 					VpnIndex;
	ADAP_Uint32						ccm_interval;
}tAdap_PacketGen;

typedef enum
{
	PACKET_ANALYZER_INIT,
	PACKET_ANALYZER_SEND_TO_CPU,
	PACKET_ANALYZER_SEND_TO_ANA,
}eAnaTraffic;

typedef struct
{
	MEA_Action_t 				actionid_send_to_cpu;
	MEA_Action_t 				actionid_send_to_ana;
	MEA_CcmId_t  				id_io;
	MEA_Counter_t         		green_fwd_pkts;
	MEA_Counters_CCM_Defect_dbt DefectEntry;
	ADAP_Uint16 				VpnIndex;
	ADAP_Uint32					ccm_interval;
	eAnaTraffic					sendPacketToCpu;
	MEA_Bool					sendPacketToCpuTime;
	ADAP_Uint32					ifIndex;
}tAdap_PacketAna;

typedef struct
{
	MEA_Bool	valid;
	MEA_MacAddr	DA;
	MEA_MacAddr	SA;
	MEA_Uint32  num_of_vlans;
	MEA_Uint32  outerVlan;
	MEA_Uint32  innerVlan;
	MEA_Uint16	cfmEtherType;
	MEA_Uint32	meg_level; //3 bits
	MEA_Uint32	version; //5 bits
	MEA_Uint32	opCode; // 8 bits
	MEA_Uint32	flags; // 8 bits
	MEA_Uint32	tlvOffset; // 8 bits
	MEA_Uint32	initSeqNum;
	MEA_Uint16	mepId;
	MEA_Uint32	megLen;
	MEA_Uint8	megData[48];
	MEA_Uint32	TxFCf;
	MEA_Uint32	RxFCb;
	MEA_Uint32	TxFCb;

	MEA_Uint32 type;
	MEA_Uint16 Length;
	MEA_Uint32 Pattern_Type;

}tAdap_PacketGenHeader;

typedef struct
{
	ADAP_Uint16  					Id_io;
	ADAP_Uint32						header_length;
	ADAP_Uint16						ref_count;
}tAdap_PacketGenPrfile;

typedef struct
{
	ADAP_Uint16  					Id_io;
	tAdap_PacketGenPrfile		profile;
}tAdap_PacketGenData;

typedef struct
{
	MEA_Bool  						bFreeAnalyzer;
}tAdap_PacketAnalyzerData;

typedef struct
{
	tEnetHal_VlanId serviceVlanId;
	tEnetHal_VlanId workingVlanId;
	tEnetHal_VlanId protectionVlanId;
	ADAP_Uint32 u4WorkingIfIndex;
	ADAP_Uint32 u4ProtectionIfIndex;
	ADAP_Uint32 u4IngressIfIndex;
	ADAP_Uint8 u1Bidirectional;
	ADAP_Uint8 u1ArchType;

	// in case of MPLS than this is the phy port
	ADAP_Uint32 u4PgWorkingPhyPort;
	ADAP_Uint32 u4PgProtectionPhyPort;
	MEA_Bool	is_mpls_session;
}tAdap_ProtectVlanId;

typedef struct
{
	pthread_cond_t cond_var;
	pthread_mutex_t cond_mtx;
	ADAP_Uint32 event_flags;
}tAdap_CondVarCcmMachine;

typedef struct
{
	ADAP_Uint16					internalVlan;
	tAdap_PacketAna				enetPacketAnaDb;
	tAdap_CcmApplicationDb		enetCcmAppDb;
	tAdap_PacketGen				enetPacketGenDb;
	ADAP_Uint32					countTimeBeforeStart;
	ADAP_Bool                	sendToCpu;
	ADAP_Bool 					rdiState;
}tAdap_CcmEnetUpMepDb;

typedef struct
{
	ADAP_Uint16					internalVlan;
	ADAP_Uint8					EcfmHwMepParams_valid;
	tEnetHal_EcfmHwMepParams    EcfmHwMepParams;
	ADAP_Uint8					EcfmHwRMepParams_valid;
	tEnetHal_EcfmHwRMepParams  	EcfmHwRMepParams;
	ADAP_Uint8					EcfmHwCcTxParams_valid;
	tEnetHal_EcfmHwCcTxParams   EcfmHwCcTxParams;
	ADAP_Uint8					EcfmHwCcRxParams_valid;
	tEnetHal_EcfmHwCcRxParams   EcfmHwCcRxParams;
	ADAP_Uint8					u1MdLevel;
	ADAP_Uint8					packetGenParams_active;
	tAdap_PacketGen				enetPacketGenDb;
	ADAP_Uint8					packetAnaParams_active;
	tAdap_PacketAna				enetPacketAnaDb;
	tAdap_CcmApplicationDb		enetCcmAppDb;
	ADAP_Uint8                  u1EcfmOffStatus;
	ADAP_Bool                	sendToCpu;
	ADAP_Bool 					rdiState;
	ADAP_Uint32					countTimeBeforeStart;
	ADAP_Uint8              	au1HwHandler [ENETHAL_ECFM_HW_MA_HANDLER_SIZE];

	ADAP_Int32					i4Length;
	ADAP_Uint32					u4PortArray[MAX_NUM_OF_SUPPORTED_PORTS];

	// for up mep when there is more than one port
	ADAP_Uint32					num_of_subEntry;
	tAdap_CcmEnetUpMepDb		tSubEntry[ADAP_D_MAX_OF_CCM_SUB_ENTRIES];
}tAdap_ECFMCcmDb;

typedef struct
{
	ADAP_Uint8					EcfmHwMaParams_valid;
	tEnetHal_EcfmHwMaParams     EcfmHwMaParams;
	ADAP_Uint8					valid;
	tAdap_ECFMCcmDb			EcfmDbParams;
	ADAP_Uint8					isDestMulticast;

	ADAP_Uint8                  lastu1EcfmOffStatus;
	ADAP_Uint8                  lastu1EcfmType;

	tAdap_PacketGenHeader	tPduFromIss;

	ADAP_Uint8 					u1LastTxType;
	ADAP_Uint8 					u1LastRxType;

}tAdap_ECFMCcmInst;

typedef struct
{
	ADAP_Uint32 u4CcmTx;
	ADAP_Uint32 u4LastSeqNum;
	ADAP_Uint32 u4CcmTxFailure;
}tAdap_EcfmCcOffMepTxStats;

typedef struct
{
	ADAP_Uint32 vpnCurrFlushTime[ADAP_MAX_NUM_OF_VPNS];
	ADAP_Uint32 vpnLastFlushTime[ADAP_MAX_NUM_OF_VPNS];
}tAdap_FdbFlushTimes;

typedef struct
{
	ADAP_Uint32			ringId;
	ADAP_Uint8			u1Port2ProtectionType;
	ADAP_Uint8			u1Port1ProtectionType;
	ADAP_Uint16			u2VlanGroupId;
	ADAP_Uint8       	u1Port1Action;
	ADAP_Uint32			u4Port1IfIndex;
	ADAP_Uint8       	u1Port2Action;
	ADAP_Uint32			u4Port2IfIndex;
	ADAP_Uint16			vlanId;
}tAdap_ErpsVInstanceDb;

/* NETIPV4 STRUCTURES
 * This NetIP structures is used to while acessing the IP Interface related
 * Information .Whenever this record is referred,u4IfIndex refers to
 * Ip Interface Number */
typedef struct
{
	ADAP_Uint32    u4IfIndex; /* IP Port number/Index number */
	ADAP_Uint32    u4CfaIfIndex; /* Cfa IfIndex */
	ADAP_Uint32    u4ContextId; /* Vrf context id */
	ADAP_Uint32    u4Mtu; /* Maximum Transmittable Units */
	ADAP_Uint32    u4Addr; /* IP Address of the Interface */
	ADAP_Uint32    u4NetMask; /*  Address Mask */
	ADAP_Uint32    u4BcastAddr; /* BroadCast Address */
	ADAP_Uint32    u4IfSpeed; /* Speed of the Interface */
	ADAP_Uint32    u4IfHighSpeed; /* High Speed of the Interface */
	ADAP_Uint32    u4RoutingProtocol; /* Routing Protocol Configured */
	ADAP_Uint32    u4Admin; /* Admin Status */
	ADAP_Uint32    u4Oper; /* Operational Status */
	ADAP_Uint32    u4IfType; /* Interface Type */
	ADAP_Uint32    u4DhcpReleaseStatusFlag; /* Flag is set for release */
	ADAP_Uint16    u2VlanId; /* Vlan Id for the interface idx */
	ADAP_Uint8    u1CfaIfType; /* CFA interface type. */
	ADAP_Uint8    u1Pad;                  /* reserved bytes for padding */
	ADAP_Uint8    au1IfName[ADAP_MAX_IFNAME_SIZE]; /* Interface Name */
	ADAP_Uint8    u1EncapType;
	ADAP_Uint8    u1ProxyArpAdminStatus;     /* Proxy ARP Enable/Disable Flag      */
	ADAP_Uint8    u1LocalProxyArpStatus;    /* Local Proxy ARP Enable/Disable Flag */
	ADAP_Uint8    au1Align[1];                  /* reserved bytes for padding */
}tAdap_NetIpv4IfInfo;

/********************** MPLS TP Identifiers ***************************/
/* Tunnel Identifier structure */
typedef struct {
	ADAP_Uint32           u4TunnelId; /* Tunnel identifier. This identifies the
                                 * tunnel associated with the ME
                                 */
	ADAP_Uint32           u4TunnelInst; /* Instance of the tunnel identifier.*/

	ADAP_Uint32           u4SrcLer;     /* Identifier of the source LER */

	ADAP_Uint32           u4DstLer;     /* Identifier of the destination LER */
}tAdap_EcfmMplsLspParams;

/* MPLS Path identification union (Union of LSP & PW) */
typedef union {
    tAdap_EcfmMplsLspParams   MplsLspParams; /* LSP Tunnel path identifier */

    ADAP_Uint32                u4PswId;       /* pwID in pwTable (VC-ID) */
}unAdap_EcfmMplsPathParams;

/** MPLS Path Identification parameters *** */
typedef struct {
     unAdap_EcfmMplsPathParams  MplsPathParams; /* Union containing the MPLS LSP or
                                            * pseudowire parameters
                                            * */
     ADAP_Uint8                 u1MplsPathType; /* Determines the type of the path
                                            * either LSP or PW
                                            * */
     ADAP_Uint8                 au1Pad[3];      /* Array used for padding */
}tAdap_EcfmMplsParams;

typedef struct {
	ADAP_Uint32                    u4ContextId;     /* Virtual Context Identifier */
	tAdap_EcfmMplsParams        EcfmMplsParams;  /* Contains the information
                                           * regarding the path can be
                                           * either LSP or PWMpls.
                                           */
    void                 		  *pSlotInfo;      /* Pointer to IfIndex/SlotInfo
                                           * to identify the slot.
                                           */
    ADAP_Uint32                    u4Period;        /* AIS Period */
    ADAP_Uint32                    u4AisInterval;   /* Interval for the periodic
                                           * transmission of AIS
                                           */
    ADAP_Uint8                    u1MdLevel;       /* Level in which the AIS PDU
                                           * needs to be transmitted
                                           * or received.
                                           */
    ADAP_Uint8                    u1PhbValue;      /* Value of the Per hop behavior */
    ADAP_Bool                     b1AisCondition;  /* TRUE(1)  - indicates AIS Cond Entry
                                           * FALSE(0) - indicates AIS Cond Exit
                                           */
    ADAP_Uint8                    au1Pad[1];       /* Added for Padding */
}tAdap_EcfmMplstpAisOffParams;

typedef struct {
	ADAP_Uint32  u4IfIndex;
	ADAP_Uint16  u2_SubReferenceNum;
	ADAP_Uint8  u1_InterfaceType;
	ADAP_Uint8  u1_InterfaceNum;
}tAdap_CRU_INTERFACE;

typedef struct {
	ADAP_Uint8           u1_SourceModuleId;
	ADAP_Uint8           u1_DestinModuleId;
	ADAP_Uint16           u2Reserved;            /* added for packing */
    tAdap_CRU_INTERFACE  InterfaceId;
    ADAP_Uint32           u4Reserved1;
    ADAP_Uint32           u4Reserved2;
    ADAP_Uint32           u4Reserved3;

}tAdap_MODULE_DATA;

typedef struct _tEnetHalCruBufHdr
{
	ADAP_Uint32               u4BufSize;
	ADAP_Uint32               u4_ValidByteCount;
	ADAP_Uint32               u4_FreeByteCount;
	ADAP_Uint8              *pu1_FirstByte;
	ADAP_Uint8              *pu1_FirstValidByte;
    struct _tEnetHalCruBufHdr *pNext;
    tAdap_MODULE_DATA        ModuleData;
    struct _tEnetHalCruBufHdr *pFirstValidDataDesc;
} tAdap_CruBufHdr;

/* IPv6 Address definition */
typedef struct
{
    tEnetHal_IPv6Addr  Ip6Addr;                        /* Interface IPv6 Address */
    ADAP_Uint32     u4PrefixLength;                 /* Address Mask */
    ADAP_Uint32     u4Type;                         /* Address Type */
}tAdap_NetIpv6AddrInfo;

typedef struct
{
    tAdap_NetIpv6AddrInfo       Ipv6AddrInfo;    /* IP6 Address Information */
    ADAP_Uint32                    u4Index;         /* Interface Index */
    ADAP_Uint32                    u4Mask;          /* Address Status Mask */
#define ADAP_NETIPV6_ADDRESS_ADD            0x1
#define ADAP_NETIPV6_ADDRESS_DELETE         0x2
}tAdap_NetIpv6AddrChange;

typedef tAdap_CruBufHdr tAdap_CRU_BUF_CHAIN_HEADER;
typedef tAdap_CRU_BUF_CHAIN_HEADER     tAdap_EcfmBufChainHeader;

typedef struct
{
   union
   {
#ifdef MBSM_WANTED
      struct MbsmIndication {
          tMbsmProtoMsg *pMbsmProtoMsg;
      }MbsmCardUpdate;
#endif
#ifdef L2RED_WANTED
      // tEcfmRedRmFrame   RmFrame;
#endif
       struct
       {
    	   ADAP_Uint32                u4VidIsid;      /* VLAN ID of the MEP*/
           /* In MPLSTP-OAM, MEPs is not added to the ECFM_CC_PORT_MEP_TABLE
            * as the MPLSTP MEPs are not associated to port.
            * Inorder to the get the information about the MPLSTP MEPs from
            * ECFM_CC_MEP_TABLE, the MEP related information is added here.
            */
    	   ADAP_Uint32                u4MdIndex;      /* MdIndex of the MEP */
    	   ADAP_Uint32                u4MaIndex;      /* MaIndex of the MEP */
    	   ADAP_Uint16                u2MepId;        /* MEP ID */
    	   ADAP_Uint8                 u1Direction;    /* Direction of the MEP*/
    	   ADAP_Uint8                 u1MdLevel;      /* MD Level of the MEP*/
    	   ADAP_Uint8                 u1SelectorType; /* Selector Type */
    	   ADAP_Uint8                 au1Pad[3];
       }Mep;
       /* structure for chassis information update*/
       struct
       {
    	   ADAP_Uint16 u2IdLen;
    	   ADAP_Uint8  u1SubType;
    	   ADAP_Uint8  au1Id[ADAP_ECFM_MAX_CHASSISID_LEN];
    	   ADAP_Uint8  au1Pad[2];
       }ChassisId;
       /* structure for port information update*/
       struct
       {
    	   ADAP_Uint16 u2IdLen;
    	   ADAP_Uint8 u1SubType;
    	   ADAP_Uint8 au1Id[ADAP_ECFM_MAX_PORTID_LEN];
    	   ADAP_Uint8 au1Pad[2];
       }PortId;
       /* structure for IPv4 interface change updated*/
       struct
       {
           tAdap_NetIpv4IfInfo  	NetIpIfInfo;
           ADAP_Uint32     			u4BitMap;
       }Ip4ManAddr;
       struct
       {
    	   ADAP_Uint8           au1HwHandler [ENETHAL_ECFM_HW_MEP_HANDLER_SIZE];
#ifdef NPAPI_WANTED
           tEcfmHwEvents   EcfmHwEvents;
#endif
       }HwCalBackParam;
       /* structure for IPv6 interface change updated*/
       tAdap_NetIpv6AddrChange Ip6ManAddr;
       struct
       {
    	   ADAP_Uint32            u4VlanIdIsid;
    	   ADAP_Uint16            u2CcmOffloadHandle;
    	   ADAP_Uint16            u2MstInst;
    	   ADAP_Uint16            u2EtherTypeValue; /* VLAN Ether Type Value */
    	   ADAP_Uint8             u1EtherType;      /* VLAN Ingress or Egress Ether
                                               * Type
                                               */
    	   ADAP_Uint8            u1PortState;
       }Indications;
       tAdap_EcfmBufChainHeader   *pEcfmPdu;    /* ECFMPDU received from CFA*/

       tAdap_EcfmMplsParams       EcfmMplsParams; /* Structure Containing MPLS
                                              * specific parameters
                                              */
       tAdap_EcfmMplstpAisOffParams MplstpAisOffParams; /* MPLSTP AIS Offad
                                                    * details
                                                    */
       ADAP_Uint8                 u1PortOperState;
       ADAP_Uint8                 u1IntfType;
   }uMsg;
   ADAP_Uint32            u4IfIndex;     /* Interface Index of the port*/
   ADAP_Uint32            u4ContextId;   /* Context Identifier*/
   tAdap_EcfmMsgType     MsgType;       /* Message type specifying Creation /
                                    * deletion / updation or PDU received.
                                    */
   tAdap_EcfmMsgType     MsgSubType;    /* Message type specifying sub message
                                    */
   ADAP_Uint16            u2PortNum;
   ADAP_Uint8             u1MdLevel;
   ADAP_Uint8             u1MplsTnlPwOperState;  /* Specifies the port state
                                            * Up/Down of the port. Port
                                            * operational state indication
                                            * is provided by the CFA.
                                            */
}tAdap_EcfmCcMsg;

extern tAdap_ECFMCcmInst tAdap_EcfmCcmDb[ADAP_D_MAX_DATABASE_CCM_CONFIGURATION];

ADAP_Int32 adap_FsDeleteHwPacketGenerator(tAdap_ECFMCcmInst	*ccmDb,int entry_num);
void adap_usSleepFunc(ADAP_Uint32 usec);
ADAP_Uint32 adap_FreeGenVpnId(ADAP_Uint16 vpnId);
void adap_freeInternalVlanId(ADAP_Uint16 VlanId);
ADAP_Uint16 adap_allocateInternalVlanId(void);
MEA_Status adap_FsMiHwPacketGenActive(MEA_PacketGen_t  Id_io,MEA_Bool active);
MEA_Status adap_FsMiHwDeleteRxForwarder(ADAP_Uint16 vlanId,ADAP_Uint16 VpnIndex, ADAP_Uint8 u1MdLevel,MEA_Uint32 op_code,MEA_Uint32 Src_port,MEA_Action_t *paction_id);
MEA_Status adap_FsMiHwSetRxForwarder(tAdap_ECFMCcmInst *ccmDb,eAnaTraffic packetToCpu,int entry_num,MEA_Bool bSilence);
MEA_Status adap_FsMiHwDeleteTxForwarder(tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex);
MEA_Status adap_FsMiHwCreateRxForwarder(MEA_Action_t action_id,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Uint32  MD_level,MEA_Uint32 op_code,MEA_Uint16 vlanId,MEA_Uint32 Src_port);
MEA_Status adap_FsMiHwCreateCcmAnalyzer(MEA_CcmId_t  *id_io,int mepId,int period,int seqNumber,char *megData);
MEA_Status adap_FsMiHwCreateCfmAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,int num_of_entries,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Ingress_TS_type_t type);
MEA_Status adap_FsMiHwSetGenericAnalyzer(MEA_Analyzer_t id,int seqNumber);
ADAP_Int32 adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_Bool/*BOOL1*/ add,MEA_LxCp_t   LxCp_Id);
ADAP_Int32 adap_FsMiVlanHwLacpLxcpApsUpdate (ADAP_Bool/*BOOL1*/ add,ADAP_Uint32 ifIndex);
ADAP_Uint32 adap_freePacketSmallGenTable(ADAP_Uint16 Id_io);
ADAP_Uint32 adap_AllocPacketSmallGenTable(ADAP_Uint16 Id_io);
MEA_Bool adap_isPacketBigGenTableFull(void);
ADAP_Uint32 adap_freePacketBigGenTableFull(ADAP_Uint16 Id_io);
ADAP_Uint32 adap_AllocPacketBigGenTableFull(ADAP_Uint16 Id_io);
ADAP_Uint32 adap_freePacketGenProfile(ADAP_Uint32 Id_io,MEA_Bool *freeProfile);
ADAP_Uint32 adap_allocPacketGenProfile(ADAP_Uint32 header_length,ADAP_Uint32 Id_io);
ADAP_Uint32 adap_getDplPacketGenProfile(ADAP_Uint32 header_length);
ADAP_Uint32 adap_getPacketGenProfile(ADAP_Uint32 header_length);
MEA_Status adap_FsMiHwPacketGenProfInfoCreate(int header_length,MEA_PacketGen_profile_info_t  *pId_io,MEA_Uint32 stream_type);
MEA_Status adap_FsMiHwGetInterfaceCfmOamUCMac (ADAP_Int32 i4Port,tEnetHal_MacAddr *macAddr);
MEA_Status adap_FsMiHwSetInterfaceCfmOamUCMac (ADAP_Int32 i4Port,tEnetHal_MacAddr *macAddr);
MEA_Status adap_FsMiHwCreateCfmService(MEA_Service_t *pService_id,tRmtSrvInfo *info);
MEA_Status adap_FsMiHwPacketGenDmmCreateiNFO(tAdap_PacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond,MEA_Bool bDmmPacket);
ADAP_Uint32 adap_FsDeletePacketAnalyzer(tAdap_ECFMCcmInst *ccmDb,int entry_num);
MEA_Bool adap_isPacketGenProfileTableFull(void);
ADAP_Int32 adap_MiVlanSetServiceMdLevel (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,ADAP_Uint16 level);
ADAP_Int32 adap_MiVlanSetServiceLmId (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,MEA_Uint32 LmId,MEA_Bool bPartner);
ADAP_Int32 adap_MiVlanSetPartnerServiceLmId (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,MEA_Uint32 LmId);
ADAP_Uint32 adap_CompareCurrFdbFlush(ADAP_Uint32 ifIndex,ADAP_Uint32 vpnId);
void adap_copyLastToCurrFdbFlush(ADAP_Uint32 ifIndex);
tAdap_InterfaceRemoteSide *adap_getIntRemoteSide(ADAP_Uint32 u4IfIndex);
ADAP_Uint32 adap_delete_rmtSide_Configuration(tAdap_RmtSide *pConfig);
ADAP_Uint32 adap_allocateLmId(ADAP_Uint16 vlanId,ADAP_Uint32 u4IfIndex);
void adap_freeLmId(ADAP_Uint32 lmId);
void adap_signal_condtimer_clearFlag(ADAP_Uint32 flag);
void adap_signal_condMutexLock_ccm(void);
void adap_signal_condMutexUnLock_ccm(void);
void adap_signal_condWait_ccm(void);
MEA_Status adap_read_event_group(MEA_Uint32 *pBitMaskEvent);
void adap_update_iss_with_link_status(void);
void adap_FsCallBackEventLossOfCCM(ADAP_Uint16 u2RMepId,ADAP_Uint32 u4IfIndex);
ADAP_Int8 adap_FsBrgFlushPortVpn (ADAP_Uint32 u4IfIndex, ADAP_Uint16 VpnId);
MEA_Status adap_FsMiHwCreateCfmServiceWithOutPut(MEA_Service_t *pService_id,tRmtSrvInfo *srcInfo,MEA_OutPorts_Entry_dbt 	*pOutPorts,MEA_EHP_Info_dbt *pInfo);
MEA_Status adap_FsMiHwSetPortShaper(MEA_Port_t	enetPort,MEA_EirCir_t CIR,MEA_EbsCbs_t CBS);

#endif
