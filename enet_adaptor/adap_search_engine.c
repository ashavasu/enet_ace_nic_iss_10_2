/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "adap_qosnp.h"

MEA_Status MeaDrvHwSetDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts, MEA_Action_t  *Action_id)
{
	MEA_SE_Entry_dbt                      	entry;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"VpnIndex:%d macAddr:%x:%x:%x:%x:%x:%x\r\n", VpnIndex, macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_memcpy(&entry.key.mac_plus_vpn.MAC.b[0],macAddr,6);
	entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.data.static_flag  = MEA_TRUE;//MEA_TRUE;

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetDestMacForwarder FAIL: ENET_ADAPTOR_Get_SE_Entry!!!\n");
		return MEA_ERROR;
	}

    if(entry.data.actionId_valid == MEA_TRUE)
    {
    	(*Action_id) = entry.data.actionId;
    }

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   if(ENET_ADAPTOR_Set_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetDestMacForwarder FAIL: ENET_ADAPTOR_Set_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit MeaDrvHwSetDestMacForwarder Action_id:%d\r\n", *Action_id);
	return MEA_OK;
}



MEA_Status MeaDrvHwGetDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex, MEA_Action_t   *Action_id)
{
	MEA_SE_Entry_dbt                      entry;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"VpnIndex:%d macAddr:%x:%x:%x:%x:%x:%x\r\n", VpnIndex, macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);

	if(macAddr == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"macAddr incorrect\r\n");
		return MEA_ERROR;
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_memcpy(&entry.key.mac_plus_vpn.MAC.b[0],macAddr,6);
	entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.data.static_flag  = MEA_TRUE;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		return MEA_ERROR;
	}

    if(entry.data.actionId_valid == MEA_TRUE)
    {
    	(*Action_id) = entry.data.actionId;
    }

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit MeaDrvHwGetDestMacForwarder Action_id:%d\r\n", *Action_id);
	return MEA_OK;
}

MEA_Status MeaDrvHwDeleteDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_Action_t   *Action_id)
{
	MEA_SE_Entry_dbt                      entry;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"VpnIndex:%d macAddr:%s\r\n", VpnIndex, adap_mac_to_string(macAddr));

	if(macAddr == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"macAddr incorrect\r\n");
		return MEA_OK;
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(macAddr, entry.key.mac_plus_vpn.MAC.b);
	entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.data.static_flag  = MEA_TRUE;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetDestMacForwarder FAIL: ENET_ADAPTOR_Get_SE_Entry!!!\n");
		return MEA_OK;
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_memcpy(&entry.key.mac_plus_vpn.MAC.b[0],macAddr,6);
	entry.key.mac_plus_vpn.VPN = VpnIndex;

   if(ENET_ADAPTOR_Delete_SE_Entry (MEA_UNIT_0, &entry.key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwDeleteDestIpForwarder FAIL: MEA_API_Delete_SE_Entry!!!\n");
		return MEA_ERROR;
	}
   (*Action_id) = MEA_PLAT_GENERATE_NEW_ID;

    if(entry.data.actionId_valid == MEA_TRUE)
    {
    	(*Action_id) = entry.data.actionId;
    }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit FsMiHwDeleteDestIpForwarder Action_id:%d\r\n", *Action_id);
	return MEA_OK;
}

MEA_Status MeaDrvHwCreateDestMacForwarder(MEA_Action_t action_id,MEA_Bool action_valid,tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      	entry;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"action_id:%d action_valid:%d VpnIndex:%d macAddr:%s\r\n",
			action_id, action_valid, VpnIndex, adap_mac_to_string(macAddr));

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(macAddr, entry.key.mac_plus_vpn.MAC.b);
	entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.data.static_flag  = MEA_TRUE;

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_SE_Entry - entry exists\n");
		return MEA_OK;
	}
	entry.data.actionId_valid  = action_valid;
	entry.data.actionId = action_id;
	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwCreateDestMacForwarder FAIL: MEA_API_Create_SE_Entry!!!\n");
		return MEA_ERROR;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"entry for actionId:%d\n", entry.data.actionId);
	return MEA_OK;
}


MEA_Status MeaDrvHwCreatePPPoEForwarder(MEA_Action_t action_id,tEnetHal_MacAddr *macAddr,ADAP_Uint32 sessionId,ADAP_Uint32 VpnIndex, MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      	entry;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"action_id:%d sessionId:%d macAddr:%s\r\n", action_id, sessionId, adap_mac_to_string(macAddr));

	adap_memset(&entry, 0, sizeof(entry));
#ifdef PPP_MAC
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
	adap_mac_to_arr(macAddr, entry.key.mac_plus_pppoe_session_id.MAC.b);
	entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = sessionId;
#else
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
	entry.key.PPPOE_plus_vpn.PPPoE_session_id = sessionId;
	entry.key.PPPOE_plus_vpn.VPN = VpnIndex;
#endif

	entry.data.static_flag  = MEA_TRUE;

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_SE_Entry - entry exists\n");
		return MEA_OK;
	}
	entry.data.actionId_valid  = MEA_TRUE;
	entry.data.actionId = action_id;
	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   	if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwCreatePPPoEForwarder FAIL: MEA_API_Create_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}



MEA_Status MeaDrvHwDeletePPPoEForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint32 sessionId ,ADAP_Uint32 VpnIndex)
{
	MEA_SE_Entry_dbt                      	entry;
#ifdef PPP_MAC
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"DeletePPPoE sessionId:%d macAddr:%x:%x:%x:%x:%x:%x\r\n", sessionId, 
				macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
#else
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"DeletePPPoE sessionId:%d  VPNId:\r\n", sessionId,VpnIndex);
#endif

	adap_memset(&entry, 0, sizeof(entry));
#ifdef PPP_MAC
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
	adap_memcpy(&entry.key.mac_plus_pppoe_session_id.MAC.b[0],macAddr,6);
	entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = sessionId;
	entry.data.static_flag  = MEA_TRUE;
#else
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
	entry.key.PPPOE_plus_vpn.PPPoE_session_id = sessionId;
	entry.key.PPPOE_plus_vpn.VPN = VpnIndex;
#endif

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_SE_Entry - entry does not exists\n");
		return MEA_ERROR;
	}

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK)
	{
		return MEA_ERROR;
	}

	return MEA_OK;

}


MEA_Status MeaDrvHwDeleteDestIpForwarder(ADAP_Uint32 ipAddr,ADAP_Uint32 ipclass,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_dbt                      entry;

	if(ipAddr == 0)
	{
		return MEA_OK;
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
	if(ipclass == D_IP_MASK_24)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_24;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFFFF00);
	}
	else if(ipclass == D_IP_MASK_16)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_16;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFF0000);
	}
	else if(ipclass == D_IP_MASK_08)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_8;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFF000000);
	}

	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;


	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsMiHwDeleteDestIpForwarder key not found for ipAdd:%d.%d.%d.%d\n",
																( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24),
																( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000) >> 16),
																( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00) >> 8),
																( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF)) );

		return MEA_OK;
	}

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
	if(ipclass == D_IP_MASK_24)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_24;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFFFF00);
	}
	else if(ipclass == D_IP_MASK_16)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_16;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFF0000);
	}
	else if(ipclass == D_IP_MASK_08)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_8;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFF000000);
	}

	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;


   if(ENET_ADAPTOR_Delete_SE_Entry (MEA_UNIT_0, &entry.key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwDeleteDestIpForwarder FAIL: MEA_API_Delete_SE_Entry!!!\n");
		return MEA_ERROR;
	}


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsMiHwDeleteDestIpForwarder deleted ipAdd:%d.%d.%d.%d\n",
												( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24),
												( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000) >> 16),
												( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00) >> 8),
												( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF)) );

	return MEA_OK;
}

MEA_Status MeaDrvHwDeleteDestIpSaDaForwarder(ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_dbt                      	entry;


	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4=srcIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4=destIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = VpnIndex;


	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		return MEA_OK;
	}

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4=srcIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4=destIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = VpnIndex;

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK)
	{
		return MEA_ERROR;
	}

	return MEA_OK;

}

MEA_Status MeaDrvHwGlobalsForwarderMCIpSubNet(MEA_Uint32  dstIp,MEA_Uint32  sourceIp)
{
    MEA_Globals_Entry_dbt entry;


    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed!!!\n");
		return MEA_ERROR;
    }


    entry.Globals_Device.MC_ServerIp.DestIp   = dstIp;
    entry.Globals_Device.MC_ServerIp.sourceIp = sourceIp;


    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Globals_Entry failed!!!\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status MeaDrvHwGetDestIpSaDaForwarder(ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_dbt                      	entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4=srcIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4=destIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = VpnIndex;


	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetDestIpSaDaForwarder FAIL: MEA_API_Get_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}



MEA_Status MeaDrvHwCreateDestIpSaDaForwarder(MEA_Action_t action_id,MEA_Bool action_valid,ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      	entry;
	//MEA_Action_t							action=MEA_PLAT_GENERATE_NEW_ID;


	adap_memset(&entry, 0, sizeof(entry));


	// for MSB 8 bits
	MeaDrvHwGlobalsForwarderMCIpSubNet(destIpAddr,srcIpAddr);

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4=srcIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4=destIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;



	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before FsMiHwCreateForwarder 0x%x\n",entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4);


	if(pOutPorts != NULL)
	{
		adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));
	}

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsMiHwCreateForwarder key allready exist %d.%d.%d.%d  %d.%d.%d.%d\n",
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 & 0xFF000000) >> 24,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 & 0x00FF0000) >> 16,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 & 0x0000FF00) >> 8,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 & 0x000000FF ),
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0xFF000000) >> 24,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0x00FF0000) >> 16,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0x0000FF00) >> 8,
									  (entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0x000000FF ) );

		return MEA_OK;
	}
	entry.data.actionId_valid  = action_valid;
	entry.data.actionId = action_id;

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4=srcIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4=destIpAddr;
	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;



	entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = VpnIndex;

	if(pOutPorts != NULL)
	{
		adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));
	}

   if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwCreateForwarder FAIL: MEA_API_Create_SE_Entry!!!\n");
		return MEA_ERROR;
	}

	return MEA_OK;
}

MEA_Status MeaDrvHwCreateDestIpForwarder(MEA_Action_t action_id,MEA_Bool action_valid,ADAP_Uint32 ipAddr,ADAP_Uint32 IpClass,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      	entry;
	MEA_Action_t							action=MEA_PLAT_GENERATE_NEW_ID;
	ADAP_Uint32 Mask = 0xffffffff;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;

	if(IpClass == D_IP_MASK_24)
	{
		Mask = 0xFFFFFF00;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_24;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 = (MEA_Uint32)(ipAddr & 0xFFFFFF00);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before FsMiHwCreateForwarder 24\n");

	}
	else if(IpClass == D_IP_MASK_16)
	{
		Mask = 0xFFFF0000;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_16;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4= (MEA_Uint32)(ipAddr & 0xFFFF0000);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before FsMiHwCreateForwarder 16\n");
	}
	else if(IpClass == D_IP_MASK_08)
	{
		Mask = 0xFF000000;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_8;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4= (MEA_Uint32)(ipAddr & 0xFF000000);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before FsMiHwCreateForwarder 8\n");
	}


	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;//MEA_TRUE;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before FsMiHwCreateForwarder 0x%x\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4);


	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsMiHwCreateForwarder key allready exist %d.%d.%d.%d\n",
									( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000 >> 16),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00 >> 8),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF ) );
		return MEA_OK;
	}
	entry.data.actionId_valid  = action_valid;
	entry.data.actionId = action_id;

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;

	if(IpClass == D_IP_MASK_24)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_24;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFFFF00);
	}
	else if(IpClass == D_IP_MASK_16)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_16;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFFFF0000);
	}
	else if(IpClass == D_IP_MASK_08)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_8;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=(ipAddr & 0xFF000000);
	}

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(action_valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip addr %d.%d.%d.%d  mask:%d send to cpu\n",
									( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000 >> 16),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00 >> 8),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF ),
									  entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip addr %d.%d.%d.%d send to output port\n",
									( (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000 >> 16),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00 >> 8),
									  (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF ) );
	}
	// if the packet send to null group, send the packet to CPU in order to be learned by slow path
	if(action_valid == MEA_FALSE)
	{
		action = AdapGetRouteActionToCpu();
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"got actionId:%d\n",action);
		if(action == MEA_PLAT_GENERATE_NEW_ID)
		{
			if(MeaDrvCreateActionToCpu(&action) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create action\n");
				return MEA_ERROR;
			}
			else
			{
				AdapSetRouteActionToCpu(action);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"actionId:%d created\n",action);
		}
		entry.data.actionId_valid  = MEA_TRUE;
		//check if the action exist
		entry.data.actionId = action;

		adap_memset(&entry.data.OutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		MEA_SET_OUTPORT(&entry.data.OutPorts, 127);

	}
	else
	{
		if(MEA_IS_SET_OUTPORT(&entry.data.OutPorts,127) != 0)
		{
			MEA_CLEAR_OUTPORT(&entry.data.OutPorts,127);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"output port %d actionId:%d\n",127,entry.data.actionId);
		}
	}

   if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwCreateForwarder FAIL: MEA_API_Create_SE_Entry!!!\n");
		return MEA_ERROR;
	}

	MeaDrvUpdatePolicerForIp(ipAddr, Mask);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"succeeded add to add ip to forwarder:%d.%d.%d.%d actionId:%d\n",
										(entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0xFF000000) >> 24,
										(entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x00FF0000) >> 16,
										(entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000FF00) >> 8,
										(entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x000000FF) ,
										entry.data.actionId);

	return MEA_OK;
}

MEA_Status MeaDrvHwCreateNatForwarder(MEA_Action_t action_id,ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_Port_t enetPort,MEA_SE_Forwarding_key_type_te type)
{
	MEA_SE_Entry_dbt                      	entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = type;
	if(type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=l4Port;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;
	}
	else
	{
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4=ipAddr;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port=l4Port;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = VpnIndex;
	}
	entry.data.actionId_valid  = MEA_TRUE;
	entry.data.actionId = action_id;

    entry.data.aging = 3;
    entry.data.static_flag     = MEA_TRUE;
    entry.data.sa_discard      = MEA_FALSE;

	MEA_SET_OUTPORT(&entry.data.OutPorts, enetPort);

	if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwCreateForwarder FAIL: MEA_API_Create_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"============ create entry in the forwarder ============= \r\n");
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"type:%d\r\n",entry.key.type);
	if(entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAipAddr:%x \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAl4Port:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAmask_type:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAVpnIndex:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAipAddr:%x \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAl4Port:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAmask_type:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAVpnIndex:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN);

	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"succeeded add to add tcp/udp actionId:%d\n",entry.data.actionId);
	return MEA_OK;
}



MEA_Status MeaDrvHwGetNatForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_SE_Forwarding_key_type_te type)
{
	MEA_SE_Entry_dbt                      	entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = type;
	if(type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=l4Port;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;
	}
	else
	{
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4=ipAddr;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port=l4Port;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = VpnIndex;
	}
	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetNatForwarder FAIL: MEA_API_Get_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MEA_API_Get_SE_Entry get search engine succeeded\n");
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"type:%d \r\n",entry.key.type);
	if(entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAipAddr:%x \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAl4Port:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAmask_type:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAVpnIndex:%d \r\n",entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAipAddr:%x \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAl4Port:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAmask_type:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SAVpnIndex:%d \r\n",entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN);

	}
	return MEA_OK;
}

MEA_Status MeaDrvHwDeleteNatForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_SE_Forwarding_key_type_te type)
{
	MEA_SE_Entry_dbt                      	entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = type;
	if(type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
	{
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=l4Port;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;
	}
	else
	{
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4=ipAddr;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port=l4Port;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
		entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = VpnIndex;
	}
	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvHwGetNatForwarder MEA_API_Get_SE_Entry not found!!!\n");
		return MEA_OK;
	}

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetNatForwarder FAIL: ENET_ADAPTOR_Delete_SE_Entry!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaDrvHwGetAllDestIpForwarder(MEA_SE_Entry_key_dbt  *pEntryKey,ADAP_Uint32 *maxNumOfEntries)
{
	MEA_SE_Entry_dbt                      entry;
	MEA_Bool                       		  found;
	ADAP_Uint32							  num_of_entries=(*maxNumOfEntries);

	(*maxNumOfEntries)=0;

	if( ENET_ADAPTOR_GetFirst_SE_Entry(MEA_UNIT_0,&entry,&found) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to get first entry\r\n");
		return MEA_OK;
	}

	while( (found == MEA_TRUE) && ( (*maxNumOfEntries) < num_of_entries) )
	{
		if(entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
		{
			adap_memcpy(&pEntryKey[(*maxNumOfEntries)],&entry.key,sizeof(MEA_SE_Entry_key_dbt));
			(*maxNumOfEntries)++;
		}


		if( ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,&entry,&found) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to get first entry\r\n");
			return MEA_OK;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsMiHwGetAllDestIpForwarder num-of-entries\r\n",(*maxNumOfEntries));
	return MEA_OK;
}

MEA_Bool MeaDrvIsIpAddressInIpForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_dbt                      entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=ipAddr;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;

	entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = VpnIndex;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		// if there is action
		if(entry.data.actionId_valid  == MEA_FALSE)
		{
			return MEA_FALSE;
		}

		// if the action is not send to cpu
		if( (entry.data.actionId != AdapGetRouteActionToCpu()) || (entry.data.actionId != 0) )
		{
			return MEA_TRUE;
		}
	}

	return MEA_FALSE;
}

static const MEA_Uint32 IPv6_MASK_64[4] = { 0xffffffff, 0xffffffff, 0, 0 };
static const MEA_Uint32 IPv6_MASK_63[4] = { 0xffffffff, 0xfffffffe, 0, 0 };
static const MEA_Uint32 IPv6_MASK_62[4] = { 0xffffffff, 0xfffffffc, 0, 0 };
static const MEA_Uint32 IPv6_MASK_61[4] = { 0xffffffff, 0xfffffff8, 0, 0 };
static const MEA_Uint32 IPv6_MASK_60[4] = { 0xffffffff, 0xfffffff0, 0, 0 };
static const MEA_Uint32 IPv6_MASK_59[4] = { 0xffffffff, 0xffffffe0, 0, 0 };
static const MEA_Uint32 IPv6_MASK_LAST[4] = { 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff };
const MEA_Uint32 *IPv6_MASKS[MEA_TYPE_TEID_IPV6_MASK_LAST + 1] = 
	{ IPv6_MASK_64, IPv6_MASK_63, IPv6_MASK_62, IPv6_MASK_61, IPv6_MASK_60, IPv6_MASK_59, IPv6_MASK_LAST };

MEA_Status MeaDrvHwDeleteDestIpv6Forwarder(tIPv6Addr ipv6Addr, MEA_IPCS_IPV6_MASK_Type_t mask_type, 
	ADAP_Uint16 VpnIndex)
{
        MEA_SE_Entry_dbt entry;

        adap_memset(&entry, 0, sizeof(entry));
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = 0;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = VpnIndex;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type = mask_type;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = 
		(ipv6Addr[12] << 24) | (ipv6Addr[13] << 16) | (ipv6Addr[14] << 8) | ipv6Addr[15] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = 
		(ipv6Addr[8] << 24) | (ipv6Addr[9] << 16) | (ipv6Addr[10] << 8) | ipv6Addr[11] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = 
		(ipv6Addr[4] << 24) | (ipv6Addr[5] << 16) | (ipv6Addr[6] << 8) | ipv6Addr[7] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = 
		(ipv6Addr[0] << 24) | (ipv6Addr[1] << 16) | (ipv6Addr[2] << 8) | ipv6Addr[3] << 0;

        if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0, &entry) != MEA_OK)
        {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Forwarder entry does not exist\n");
                return MEA_OK;
        }

        if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0, &entry.key) != MEA_OK)
        {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Delete_SE_Entry failed!!!\n");
                return MEA_ERROR;
        }

	return MEA_OK;
}

MEA_Status MeaDrvHwCreateDestIpv6Forwarder(MEA_Action_t action_id, MEA_Bool toCPU, tIPv6Addr ipv6Addr, 
	MEA_IPCS_IPV6_MASK_Type_t mask_type, ADAP_Uint16 VpnIndex, MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt entry;

	adap_memset(&entry, 0, sizeof(entry));
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = VpnIndex;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type = mask_type;

	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = 
		(ipv6Addr[12] << 24) | (ipv6Addr[13] << 16) | (ipv6Addr[14] << 8) | ipv6Addr[15] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = 
		(ipv6Addr[8] << 24) | (ipv6Addr[9] << 16) | (ipv6Addr[10] << 8) | ipv6Addr[11] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = 
		(ipv6Addr[4] << 24) | (ipv6Addr[5] << 16) | (ipv6Addr[6] << 8) | ipv6Addr[7] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = 
		(ipv6Addr[0] << 24) | (ipv6Addr[1] << 16) | (ipv6Addr[2] << 8) | ipv6Addr[3] << 0;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0, &entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Forwarder entry already exists\n");
		return MEA_OK;
	}

	adap_memset(&entry, 0, sizeof(entry));
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = VpnIndex;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type = mask_type;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = 
		(ipv6Addr[12] << 24) | (ipv6Addr[13] << 16) | (ipv6Addr[14] << 8) | ipv6Addr[15] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = 
		(ipv6Addr[8] << 24) | (ipv6Addr[9] << 16) | (ipv6Addr[10] << 8) | ipv6Addr[11] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = 
		(ipv6Addr[4] << 24) | (ipv6Addr[5] << 16) | (ipv6Addr[6] << 8) | ipv6Addr[7] << 0;
	entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = 
		(ipv6Addr[0] << 24) | (ipv6Addr[1] << 16) | (ipv6Addr[2] << 8) | ipv6Addr[3] << 0;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	
	if(toCPU == MEA_TRUE)
	{
		MEA_Action_t action = AdapGetRouteActionToCpu();
		if(action == MEA_PLAT_GENERATE_NEW_ID)
		{
			if(MeaDrvCreateActionToCpu(&action) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\n");
				return MEA_ERROR;
			}
			else
			{
				AdapSetRouteActionToCpu(action);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"actionId:%d created\n",action);
		}
		entry.data.actionId_valid = MEA_TRUE;
		entry.data.actionId = action;

		adap_memset(&entry.data.OutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		MEA_SET_OUTPORT(&entry.data.OutPorts, 127);

	}
	else
	{
		entry.data.actionId_valid = MEA_TRUE;
		entry.data.actionId = action_id;
		adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));
		if(MEA_IS_SET_OUTPORT(&entry.data.OutPorts,127) != 0)
		{
			MEA_CLEAR_OUTPORT(&entry.data.OutPorts,127);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"output port %d actionId:%d\n",127,entry.data.actionId);
		}
	}

	if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Create_SE_Entry failed!!!\n");
		return MEA_ERROR;
	}

	return MEA_OK;
}
