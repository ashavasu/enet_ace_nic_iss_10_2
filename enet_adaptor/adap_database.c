/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <stdlib.h>
#include <pthread.h>


#include "adap_types.h"
#include "mea_api.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_acl.h"
#include "adap_linklist.h"
#include "adap_enetConvert.h"
#include "adap_lpm.h"
#include "adap_search_engine.h"
#include "enet_queue_drv.h"
#include "mea_port_drv.h"
#include "adap_taskManager.h"

#define INTERFACE_NAME_PORT_104			"ifenet104"
#define INTERFACE_NAME_PORT_105			"ifenet105"
#define INTERFACE_NAME_PORT_106			"ifenet106"
#define INTERFACE_NAME_PORT_107			"ifenet107"

static ADAP_Uint32 Adap_SVlanMapListCompare (void *data1,void *data2);
static ADAP_Uint32 Adap_PepPvidListCompare(void *data1,void *data2);

static int process_is_killed=0;

static MEA_Bool		process_terminate=MEA_FALSE;

static tAdap_AdapDatabase tAdapDatabase;

extern ENET_Bool    ENET_InitDone;
#ifdef ENET_WITH_THIRD_PARTY
static char* thirdParty_ver="10.1";
#endif


static char* adap_ver="01.33b";




tEnetHal_MacAddr            adapStpAddress = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x00 }};
tEnetHal_MacAddr            adapMmrpAddress = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 }};
tEnetHal_MacAddr            adapGmrpAddress = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 }};
tEnetHal_MacAddr            adapGvrpAddress = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 }};
tEnetHal_MacAddr            adapMvrpAddress  = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 }};
tEnetHal_MacAddr            adapBcastAddress = {.ether_addr_octet = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }};
tEnetHal_MacAddr            adapLldpAddress  = {.ether_addr_octet = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0e }};
tEnetHal_MacAddr            adapVlanProviderStpAddr  = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd0 }};
tEnetHal_MacAddr            adapVlanProviderGvrpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd1}};
tEnetHal_MacAddr            adapVlanProviderGmrpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd2 }};
tEnetHal_MacAddr            adapVlanProviderMvrpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd5}};
tEnetHal_MacAddr            adapVlanProviderMmrpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd6}};
tEnetHal_MacAddr            adapVlanProviderDot1xAddr= {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd3 }};
tEnetHal_MacAddr            adapVlanProviderLacpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd4 }};
tEnetHal_MacAddr            adapVlanProviderElmiAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd7}};
tEnetHal_MacAddr            adapVlanProviderLldpAddr = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd8}};

tEnetHal_MacAddr 			adapOspfMacMulticastAllRoutes = {.ether_addr_octet = {0x01,0x00,0x5E,0x00,0x00,0x05}};
tEnetHal_MacAddr 			adapOspfMacMulticastAllDrs 	  = {.ether_addr_octet = {0x01,0x00,0x5E,0x00,0x00,0x06}};
tEnetHal_MacAddr 			adapRipMacMulticastAllRoutes  = {.ether_addr_octet = {0x01,0x00,0x5E,0x00,0x00,0x09}};


struct ipv4_next_route_entry {
	adap_rbtree_element  RbNode;
	tIpV4NextRoute       entry;
};

struct dest_ipv4_route_entry {
	adap_rbtree_element  RbNode;
	tIpDestV4Routing     entry;
};


#ifdef OPENFLOW_ZYNC_MODE  //MEA_OS_EPC_ZYNQ7045
MEA_Port_t	 ADAP_port_list[] =
{
/*0*/		ADAP_CPU_PORT_ID,
/*1*/		105,
/*2*/		104,
/*3*/		101,
/*4*/		102,
/*5*/		103,
/*6*/		108,
/*7*/		109,
/*8*/		110
};
#endif


#ifdef NIC_PLATFORM
MEA_Port_t	 ADAP_port_list[] =
{
/*0*/		ADAP_CPU_PORT_ID,
/*1*/		104,
/*2*/		105,
/*3*/		106,
/*4*/		107
//  /*5*/           127 /*for counter CPU*/  //need for openFlow
 
};
#endif

#ifdef BL_PLATFORM
MEA_Port_t   ADAP_port_list[] =
{
/*0*/       ADAP_CPU_PORT_ID,
/*1*/       100,
/*2*/       101,
/*3*/       102,
/*4*/       103,
/*5*/       104,
/*6*/       105,
/*7*/       118,
/*8*/       119,
//  /*5*/           127 /*for counter CPU*/  //need for openFlow

};
#endif


#ifdef ENET_WITH_THIRD_PARTY
static void print_NpapiVersion(char *buff,MEA_Uint32 len)
{
	if(buff == NULL){
		printf("buff == NULL\n");
	}

	if( len < MEA_OS_strlen(thirdParty_ver)){
		printf("len < %d\n",(int)MEA_OS_strlen(thirdParty_ver));
	}
	MEA_OS_sprintf(buff,"%s ",thirdParty_ver);

}
#endif

static void print_AdapVersion(char *buff,MEA_Uint32 len)
{
	if(buff == NULL){
		printf("buff == NULL\n");
	}

	if( len < MEA_OS_strlen(adap_ver)){
		printf("len < %d\n",(int)MEA_OS_strlen(adap_ver));
	}
	MEA_OS_sprintf(buff,"%s ", adap_ver);

}

#if 0
void iss_debug_function(ADAP_Uint32 val,ADAP_Uint32   flag)
{
    ADAP_UNUSED_PARAM (val);
    ADAP_UNUSED_PARAM (flag);
}
#endif
/*********************************************/
/* this function init the Enet adaptor       */
/*********************************************/
void EnetAdaptorInit(ADAP_Int32 software_key)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 idx1=0;
    ADAP_Uint16  PriQIdx = 0, WRedIndex = 0, clusterIdx = 0;

	adap_memset(&tAdapDatabase,0,sizeof(tAdapDatabase));
	//create_service_management();
	DplOpenSyslogApplication(ENET_ADAP_ALERT | ENET_ADAP_CRIT | ENET_ADAP_ERR | ENET_ADAP_WARNING | ENET_ADAP_INFO | ENET_ADAP_DEBUG | ENET_ADAP_NOTICE);

#ifdef ENET_WITH_THIRD_PARTY
	MEA_SetNpapiCallBackFunction(print_NpapiVersion);
#endif
	MEA_SetAdapCallBackFunction(print_AdapVersion);

    adap_task_manager_init();

	tAdapDatabase.hw_init_done = ADAP_FALSE;

	if( (software_key & SW_ENET_NIC_BOARD) == SW_ENET_NIC_BOARD)
	{
		fprintf(stdout,"number of physical ports:%d\r\n",(int)(sizeof(ADAP_port_list)/sizeof(MEA_Port_t)));
		//printf("number of physical ports:%d\r\n",(int)(sizeof(ADAP_port_list)/sizeof(MEA_Port_t)));
		for(idx=0;idx<sizeof(ADAP_port_list)/sizeof(MEA_Port_t);idx++)
		{
			tAdapDatabase.logical_ports[tAdapDatabase.number_of_physical_ports]=idx;
			tAdapDatabase.physical_ports[tAdapDatabase.number_of_physical_ports]=ADAP_port_list[idx];

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"#############################################################################\r\n");
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"logical port:%d physical port:%d\r\n",
										tAdapDatabase.logical_ports[tAdapDatabase.number_of_physical_ports],
										tAdapDatabase.physical_ports[tAdapDatabase.number_of_physical_ports]);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"#############################################################################\r\n");

			fprintf(stdout,"logical port:%d physical port:%d\r\n",
							tAdapDatabase.logical_ports[tAdapDatabase.number_of_physical_ports],
							tAdapDatabase.physical_ports[tAdapDatabase.number_of_physical_ports]);
			tAdapDatabase.number_of_physical_ports++;

			tAdapDatabase.aPolicer_id[idx] = MEA_PLAT_GENERATE_NEW_ID;
			tAdapDatabase.tm_Id[idx] = 0;

			tAdapDatabase.interfaceType[idx] = 0;

			tAdapDatabase.adapCluserDb[idx].enetPort = ADAP_port_list[idx];
		}
		sleep(1);
		printf("#############################################################################\r\n");
		for(idx=0;idx<tAdapDatabase.number_of_physical_ports;idx++)
		{
			
		printf("logical port:%d physical port:%d\r\n",
										tAdapDatabase.logical_ports[idx],
										tAdapDatabase.physical_ports[idx]);
		

		
		}
		printf("#############################################################################\r\n");
		
		
		strncpy(tAdapDatabase.adapLinuxInf[0].interface_name,INTERFACE_NAME_PORT_104,strlen(INTERFACE_NAME_PORT_104));
		tAdapDatabase.adapLinuxInf[0].valid = ADAP_TRUE;
		tAdapDatabase.adapLinuxInf[0].valid_ip=ADAP_FALSE;
		tAdapDatabase.adapLinuxInf[0].ip_address=0;
		strncpy(tAdapDatabase.adapLinuxInf[1].interface_name,INTERFACE_NAME_PORT_105,strlen(INTERFACE_NAME_PORT_105));
		tAdapDatabase.adapLinuxInf[1].valid = ADAP_TRUE;
		tAdapDatabase.adapLinuxInf[1].valid_ip=ADAP_FALSE;
		tAdapDatabase.adapLinuxInf[1].ip_address=0;
		strncpy(tAdapDatabase.adapLinuxInf[2].interface_name,INTERFACE_NAME_PORT_106,strlen(INTERFACE_NAME_PORT_106));
		tAdapDatabase.adapLinuxInf[2].valid = ADAP_TRUE;
		tAdapDatabase.adapLinuxInf[2].valid_ip=ADAP_FALSE;
		tAdapDatabase.adapLinuxInf[2].ip_address=0;
		strncpy(tAdapDatabase.adapLinuxInf[3].interface_name,INTERFACE_NAME_PORT_107,strlen(INTERFACE_NAME_PORT_107));
		tAdapDatabase.adapLinuxInf[3].valid = ADAP_TRUE;
		tAdapDatabase.adapLinuxInf[3].valid_ip=ADAP_FALSE;
		tAdapDatabase.adapLinuxInf[3].ip_address=0;

		tAdapDatabase.adapLinuxInf[4].valid = ADAP_TRUE;
		tAdapDatabase.adapLinuxInf[4].valid_ip=ADAP_FALSE;
		tAdapDatabase.adapLinuxInf[4].ip_address=0;
	}
	else
	{
		fprintf(stdout,"number of physical ports:%d\r\n",(int)(sizeof(ADAP_port_list)/sizeof(MEA_Port_t)));
	//set port interfaces
	for(idx=0;idx<sizeof(ADAP_port_list)/sizeof(MEA_Port_t);idx++)
	{
		tAdapDatabase.logical_ports[tAdapDatabase.number_of_physical_ports]=idx;
		tAdapDatabase.physical_ports[tAdapDatabase.number_of_physical_ports]=ADAP_port_list[idx];

		fprintf(stdout,"logical port:%d physical port:%d\r\n",
						tAdapDatabase.logical_ports[tAdapDatabase.number_of_physical_ports],
						tAdapDatabase.physical_ports[tAdapDatabase.number_of_physical_ports]);
		tAdapDatabase.number_of_physical_ports++;

			tAdapDatabase.aPolicer_id[idx] = MEA_PLAT_GENERATE_NEW_ID;
			tAdapDatabase.tm_Id[idx] = 0;

			tAdapDatabase.interfaceType[idx] = 0;

			tAdapDatabase.adapCluserDb[idx].enetPort = ADAP_port_list[idx];
		}
	}
	// init the service database
	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		tAdapDatabase.tAllocateServiceId[idx].serviceId = (MEA_Service_t)(START_OF_SERVICE_ID+idx);
		tAdapDatabase.tAllocateServiceId[idx].actionId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tAllocateServiceId[idx].pmId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tAllocateServiceId[idx].valid = ADAP_FALSE;
		tAdapDatabase.tAllocateServiceId[idx].enet_IncPriority = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tAllocateServiceId[idx].enet_priority_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP;
		tAdapDatabase.tAllocateServiceId[idx].vpnId = MEA_PLAT_GENERATE_NEW_ID;
		for(idx1=0;idx1<MEA_ACL_HIERARC_NUM_OF_FLOW;idx1++)
		{
			tAdapDatabase.tAllocateServiceId[idx].FilterId[idx1] = MEA_PLAT_GENERATE_NEW_ID;
		}
	}

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.tportState[idx] = ADAP_INIT_STATE;
		tAdapDatabase.acceptableType[idx] = ADAP_VLAN_ADMIT_ALL_FRAMES;
		tAdapDatabase.defPriority[idx] = 0;
	}
	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.tLxcpId[idx] = MEA_PLAT_GENERATE_NEW_ID;
	}

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.tLxcpErpsId[idx] = MEA_PLAT_GENERATE_NEW_ID;
	}

	tAdapDatabase.BridgeMode = ADAP_VLAN_CUSTOMER_BRIDGE_MODE;

	tAdapDatabase.McastMode = ADAP_MCAST_MAC_FORWARDING_MODE;

	tAdapDatabase.NATRouterStatus = ADAP_FALSE;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.providerBridgePortMode[idx] = ADAP_VLAN_TRUNK_PORT;
		tAdapDatabase.providerCoreBridgePortMode[idx] = ADAP_PROVIDER_NETWORK_PORT;

	}
	for(idx=0;idx<ADAP_VLAN_MAX_NUMBER_OF_ACCEPTABLE_FRAMES_TYPE;idx++)
	{
		tAdapDatabase.AccepFrameTypeMode[idx].valid = ADAP_TRUE;
	}
	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		tAdapDatabase.MeterTable[idx].valid=ADAP_FALSE;
		tAdapDatabase.MeterTable[idx].policer_prof_id = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.MeterTable[idx].tm_Id=0;
	}
    
	for(idx=0;idx<ADAP_NUMBER_OF_MULTICAST_MAC;idx++)
	{
		tAdapDatabase.multicastMac[idx].valid=ADAP_FALSE;
	}

	for (idx = 0; idx < ADAP_MAX_NUMBER_OF_MAC_LEARNING; idx++)
	{
		tAdapDatabase.macLearning[idx].valid=MEA_FALSE;
	}

	ADAP_initLinkList(&tAdapDatabase.tSVlanMapLinkList,Adap_SVlanMapListCompare);

	ADAP_initLinkList(&tAdapDatabase.tPepPvidLinkList,Adap_PepPvidListCompare);

	Adap_init_accesslist();

	LpmInitialize();

	tAdapDatabase.tGlobals.aging_Time = 300;
	tAdapDatabase.tGlobals.temp_aging_Time = 300;
	tAdapDatabase.tGlobals.recover_aging_Time = MEA_FALSE;
	tAdapDatabase.tGlobals.L2TunnelingEnable = ADAP_FALSE;
	tAdapDatabase.tGlobals.default_flowMarkingID = MEA_PLAT_GENERATE_NEW_ID;
	tAdapDatabase.tGlobals.default_cosMappingID = MEA_PLAT_GENERATE_NEW_ID;
	tAdapDatabase.tGlobals.mirroringStatus = MEA_FALSE;


	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.tShaperDb[idx].admin=ADAP_TRUE;
		tAdapDatabase.tShaperDb[idx].cir = 1000000;
		tAdapDatabase.tShaperDb[idx].cbs=64;
		tAdapDatabase.tShaperDb[idx].consempetion=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
		tAdapDatabase.tShaperDb[idx].overhead=0;
		tAdapDatabase.tShaperDb[idx].portid=idx;
		tAdapDatabase.tShaperDb[idx].resolution=ENET_SHAPER_RESOLUTION_TYPE_BIT;
	}
	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		tAdapDatabase.aggrEntry[idx].valid=ADAP_FALSE;
	}

	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		tAdapDatabase.lagVirtualPort[idx].lagVirtualPort = START_OF_LAG_VIRTUAL_PORTS + idx;
		tAdapDatabase.lagVirtualPort[idx].free_virtual_port=1;
	}

	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		tAdapDatabase.adapNaptRule[idx].valid = ADAP_FALSE;
	}

	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		tAdapDatabase.adapNaptAddressTable[idx].valid = ADAP_FALSE;
	}
	ADAP_initLinkList(&tAdapDatabase.tSVlanMapLinkList,Adap_SVlanMapListCompare);

	ADAP_initLinkList(&tAdapDatabase.tPepPvidLinkList,Adap_PepPvidListCompare);

	for(idx=0;idx<D_MAX_VXLAN_FLOODING;idx++)
	{
		tAdapDatabase.vxlanFloodArr[idx].valid=ADAP_FALSE;
		tAdapDatabase.vxlanFloodArr[idx].UE_pdn_Id=idx+1;
	}

/**************************************************************
      ACE_VIRTUAL_FUNC

*****************************************************************/

	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		tAdapDatabase.aceVirtualDb[idx].valid=ADAP_FALSE;
		tAdapDatabase.aceVirtualDb[idx].phy_to_cpu_service_id = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.aceVirtualDb[idx].cpu_to_phy_service_id = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.aceVirtualDb[idx].tClusterId = ENET_PLAT_GENERATE_NEW_ID;
		for(idx1=0;idx1<MEA_ACL_HIERARC_NUM_OF_FLOW;idx1++)
		{
			tAdapDatabase.aceVirtualDb[idx].phy_to_cpu_FilterId[idx1] = MEA_PLAT_GENERATE_NEW_ID;
			tAdapDatabase.aceVirtualDb[idx].cpu_to_phy_FilterId[idx1] = MEA_PLAT_GENERATE_NEW_ID;
		}
		tAdapDatabase.aceVirtualDb[idx].acl_profile_virt.acl_profile = 0;
		tAdapDatabase.aceVirtualDb[idx].acl_profile_virt.valid= MEA_FALSE;
	}
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		tAdapDatabase.aceCtrlAdmin[idx].valid=ADAP_FALSE;
		tAdapDatabase.aceCtrlAdmin[idx].phy_to_cpu_service_id = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.aceCtrlAdmin[idx].cpu_to_phy_service_id = MEA_PLAT_GENERATE_NEW_ID;
		for(idx1=0;idx1<MEA_ACL_HIERARC_NUM_OF_FLOW;idx1++)
		{
			tAdapDatabase.aceCtrlAdmin[idx].phy_to_cpu_FilterId[idx1] = MEA_PLAT_GENERATE_NEW_ID;
			tAdapDatabase.aceCtrlAdmin[idx].cpu_to_phy_FilterId[idx1] = MEA_PLAT_GENERATE_NEW_ID;
		}
		tAdapDatabase.aceCtrlAdmin[idx].acl_profile_ctrl_phy_to_cpu.valid= MEA_FALSE;
		tAdapDatabase.aceCtrlAdmin[idx].acl_profile_ctrl_phy_to_cpu.acl_profile=0;

		tAdapDatabase.aceCtrlAdmin[idx].acl_profile_ctrl_cpu_to_phy.valid= MEA_FALSE;
		tAdapDatabase.aceCtrlAdmin[idx].acl_profile_ctrl_cpu_to_phy.acl_profile=0;



	}

	for(idx=0;idx<NUMBER_OF_VTEP_VNI;idx++)
	{
		tAdapDatabase.openVswitchDb.tVtepService[idx].valid = ADAP_FALSE;
	}

	Adap_init_accesslist();


//	printf(" MAX_NUM_OF_SUPPORTED_PORTS PortsTable  %d \r\n",MAX_NUM_OF_SUPPORTED_PORTS);

	for(idx = 0; idx < MAX_NUM_OF_SUPPORTED_PORTS; idx ++)
	{
		tAdapDatabase.PortsTable[idx].iss_PortNum = idx;
		tAdapDatabase.PortsTable[idx].MasterLagPort = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].lagVirtualPort = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].lagId = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].defSid	= ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].priorityCosProfileId = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].priorityMapProfileId = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].flowMarkingProfileId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.PortsTable[idx].flowPolicerProfileId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.PortsTable[idx].portTunnelType = ADAP_VLAN_NO_TUNNEL_PORT;
		tAdapDatabase.PortsTable[idx].StagForCustomerVlan = ADAP_END_OF_TABLE;
		for(idx1=0;idx1<ENETHAL_VLAN_MAX_PROT_ID;idx1++)
		{
			tAdapDatabase.PortsTable[idx].tunnelOption[idx1] = 0;
		}
		tAdapDatabase.PortsTable[idx].stpMode = ENET_SPANNING_TREE_MSTP_MODE;
		tAdapDatabase.PortsTable[idx].stpState = 0;
		for(idx1=0;idx1<ADAP_NUM_OF_SUPPORTED_VLANS;idx1++)
		{
			tAdapDatabase.PortsTable[idx].cepPortState[idx1] = ENET_AST_PORT_STATE_DISABLED;
		}
		tAdapDatabase.PortsTable[idx].VlanSVlanMap.CVlanId = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].VlanSVlanMap.SVlanId = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].VlanSVlanMap.u1CepUntag = 0;
		tAdapDatabase.PortsTable[idx].VlanSVlanMap.u1PepUntag = 0;
		tAdapDatabase.PortsTable[idx].CustomerVlan = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].tunnelProToCusAction_id = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].tunnelCusToProAction_id = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].defMeter.policer_prof_id = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.PortsTable[idx].defMeter.tm_Id = MEA_PLAT_GENERATE_NEW_ID;
		//for(idx=0;idx<=MAX_NUMBER_OF_SERVICES;idx++)
			//tAdapDatabase.PortsTable[idx].defMeter.PmId[idx] = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.PortsTable[idx].defMeter.mappingEditingIdx = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.PortsTable[idx].meterIndex = ADAP_END_OF_TABLE;
		tAdapDatabase.PortsTable[idx].lacpTrafficBlockStatus = ADAP_FALSE;
	}

	for(idx = 0; idx < ADAP_MAX_NUMBER_OF_VLAN_EDITING_MAPPING; idx ++)
	{
		tAdapDatabase.editMapping[idx].valid 	= MEA_FALSE;
		tAdapDatabase.editMapping[idx].id		=  MEA_PLAT_GENERATE_NEW_ID;
	}

	for (idx = 0; idx < ADAP_NUM_OF_SUPPORTED_VLANS; idx++)
	{
		tAdapDatabase.MstpVlanTable[idx].isValid = ADAP_FALSE;
		tAdapDatabase.MstpVlanTable[idx].MSTPInstId = ADAP_END_OF_TABLE;
	}

	for (idx = 0; idx < ADAP_MAX_NUM_OF_MSTP_INSTANCES; idx++)
	{
		tAdapDatabase.MstpInstTable[idx].MSTPInst_valid = 0;
		for(idx1=0;idx1<MAX_NUM_OF_SUPPORTED_PORTS;idx1++)
		{
			tAdapDatabase.MstpInstTable[idx].port_state[idx1] = ENET_AST_PORT_STATE_DISCARDING;
		}
	}

	for (idx = 0; idx < MAX_NUM_OF_FLOW_MARKING_PROFILES; idx++)
	{
		tAdapDatabase.FlowMarkingUseTable[idx].UseCount = 0;
		tAdapDatabase.FlowMarkingUseTable[idx].portNum = ADAP_END_OF_TABLE;
		tAdapDatabase.FlowMarkingUseTable[idx].isFree = ADAP_TRUE;
	}

    for (idx = 0; idx < MAX_NUM_OF_SUPPORTED_PORTS; idx++)
    {
    	tAdapDatabase.pnacAutorized[idx].u1AuthMode=2;
    }

    for (idx = 0; idx < MAX_NUM_OF_SUPPORTED_PORTS; idx++)
    {
    	tAdapDatabase.svlanRelay[idx].u2LocalSVlan = ADAP_END_OF_TABLE;
    	tAdapDatabase.svlanRelay[idx].u2RelaySVlan = ADAP_END_OF_TABLE;
    	tAdapDatabase.svlanRelay[idx].ServiceId[D_UNTAGGED_SERVICE_TAG] = ADAP_END_OF_TABLE;
    	tAdapDatabase.svlanRelay[idx].ServiceId[D_TAGGED_SERVICE_TAG] = ADAP_END_OF_TABLE;
    	tAdapDatabase.svlanRelay[idx].ServiceId[D_PROVIDER_SERVICE_TAG] = ADAP_END_OF_TABLE;
        tAdapDatabase.pcpConfig[idx].u2PcpSelection = 1; //VLAN_8P0D_SEL_ROW
        tAdapDatabase.pcpConfig[idx].u1UseDei=0;
        tAdapDatabase.pcpConfig[idx].ProfileId=MEA_PLAT_GENERATE_NEW_ID;
        tAdapDatabase.pcpConfig[idx].mappingMarkingProfile=MEA_PLAT_GENERATE_NEW_ID;
        tAdapDatabase.pcpConfig[idx].cosProfile=MEA_PLAT_GENERATE_NEW_ID;
        tAdapDatabase.pcpConfig[idx].valid=MEA_FALSE;
        tAdapDatabase.pcpConfig[idx].policer_id=MEA_PLAT_GENERATE_NEW_ID;
    }



	for(idx=0;idx<ADAP_MAX_NUMBER_OF_VLAN_PRIORITY;idx++)
	{
		tAdapDatabase.ingressPcpInfo[idx].pcpInfo[idx].valid=MEA_FALSE;
	}

	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		tAdapDatabase.ClassifierTable[idx].valid=MEA_FALSE;
		adap_memset(&tAdapDatabase.ClassifierTable[idx].issClassifier, 0, sizeof(tEnetHal_QoSClassMapEntry));
		adap_memset(&tAdapDatabase.ClassifierTable[idx].issFilter, 0, sizeof(tFilterDb));
		tAdapDatabase.ClassifierTable[idx].PolicyIdx = ADAP_END_OF_TABLE;
		tAdapDatabase.ClassifierTable[idx].VlanId = ADAP_END_OF_TABLE;
		tAdapDatabase.ClassifierTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
	}

	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		tAdapDatabase.PolicyTable[idx].valid=MEA_FALSE;
		adap_memset(&tAdapDatabase.PolicyTable[idx].issPolicy, 0, sizeof(tEnetHal_QoSPolicyMapEntry));
		adap_memset(&tAdapDatabase.PolicyTable[idx].issInProActEntry, 0, sizeof(tEnetHal_QoSInProfileActionEntry));
		adap_memset(&tAdapDatabase.PolicyTable[idx].issOutProActEntry, 0, sizeof(tEnetHal_QoSOutProfileActionEntry));
		tAdapDatabase.PolicyTable[idx].ClassifierIdx = ADAP_END_OF_TABLE;
		tAdapDatabase.PolicyTable[idx].MeterIdx = ADAP_END_OF_TABLE;
		tAdapDatabase.PolicyTable[idx].QueueIdx = ADAP_END_OF_TABLE;
		tAdapDatabase.PolicyTable[idx].issInProActEntryValid = ADAP_FALSE;
		tAdapDatabase.PolicyTable[idx].issOutProActEntryValid = ADAP_FALSE;
	}

	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		tAdapDatabase.SchedulerTable[idx].valid=ADAP_FALSE;
		tAdapDatabase.SchedulerTable[idx].QueueIdx	= ADAP_END_OF_TABLE;
		tAdapDatabase.SchedulerTable[idx].level	= MAX_SCHEDULER_LEVEL;
		tAdapDatabase.SchedulerTable[idx].ClusterId = ADAP_END_OF_TABLE;
		tAdapDatabase.SchedulerTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
		for(PriQIdx = 0; PriQIdx < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; PriQIdx++)
			tAdapDatabase.SchedulerTable[idx].ChildsSchedIdx[PriQIdx] = ADAP_END_OF_TABLE;
		adap_memset(&tAdapDatabase.SchedulerTable[idx].issScheduler, 0, sizeof(tEnetHal_QoSSchedulerEntry));
		adap_memset(&tAdapDatabase.SchedulerTable[idx].issShaper, 0, sizeof(tEnetHal_QoSShapeCfgEntry));
		tAdapDatabase.SchedulerTable[idx].weight = 0;
	}

	for(idx = 1; idx < MAX_NUM_OF_QUEUES; idx ++)
	{
		tAdapDatabase.QueueTable[idx].PriQueueIdx	= ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].ClusterId	= ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].SchedulerIdx	= ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].PolicyIdx	= ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].ClassIdx = ADAP_END_OF_TABLE;
		tAdapDatabase.QueueTable[idx].WredProfileIdx = ADAP_END_OF_TABLE;
		adap_memset(&tAdapDatabase.QueueTable[idx].issQueue, 0, sizeof(tEnetHal_QoSQEntry));
        adap_memset(&tAdapDatabase.QueueTable[idx].issQueueType, 0, sizeof(tEnetHal_QoSQtypeEntry));
        for(WRedIndex = 0; WRedIndex < 3; WRedIndex ++)
            adap_memset(&tAdapDatabase.QueueTable[idx].issWredEntry[WRedIndex], 0, sizeof(tEnetHal_QoSREDCfgEntry));
	}

    for(idx = 0; idx <MAX_NUM_OF_WRED_PROFILES; idx ++)
    {
        tAdapDatabase.WredProfTable[idx].WredProfileId = idx;
        tAdapDatabase.WredProfTable[idx].UseCount = 0;
    }

//	printf(" MAX_NUM_OF_SUPPORTED_PORTS ClustersTable  %d \r\n",MAX_NUM_OF_SUPPORTED_PORTS);
		for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
		{
			tAdapDatabase.ClustersTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
			for(clusterIdx=0;clusterIdx<NUM_OF_CLUSTERS_PER_PORT;clusterIdx++)
			{
				tAdapDatabase.ClustersTable[idx].clusterInfo[clusterIdx].valid = ADAP_FALSE;
				tAdapDatabase.ClustersTable[idx].clusterInfo[clusterIdx].clusterId = ADAP_END_OF_TABLE;
				tAdapDatabase.ClustersTable[idx].clusterInfo[clusterIdx].numOfDefinedQueues = 0;
			}
		}

		while(ENET_InitDone == MEA_FALSE)
		{
		    printf("wait on init done\r\n");
			sleep(2);
		}

		if(adap_CreateClusters() != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_CreateClusters: ENET_Create_Queue FAIL !\n");
		}
		else if (MEA_device_environment_info.MEA_DEFAULT_LAG_SUPPORTED_enable == MEA_TRUE) {
		{
			if(adap_CreateDefaultLag() != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_CreateDefaultLag: ENET_Create_Queue FAIL !\n");
			}
		}
    }

	printf(" EnetAdaptorInit Exit \r\n");
}
/*********************************************/
/* this function destroys the Enet Adaptor */
/*********************************************/
void EnetAdaptorDestroy()
{
	adap_task_manager_deinit();
}

/*********************************************/
/* this function init the Enet adaptor VLANs */
/*********************************************/
ADAP_Uint32 initVlanDatabase(void)
{
	init_vpn_database();
	Adap_init_bridgeDomain();

	tAdapDatabase.defaultVlanId=1;

	return ENET_SUCCESS;
}


static
int compareIPv4RouteRBTree(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct ipv4_next_route_entry *route1 = container_of(e1, struct ipv4_next_route_entry, RbNode);
	struct ipv4_next_route_entry *route2 = container_of(e2, struct ipv4_next_route_entry, RbNode);

	if (route1->entry.u4IpDestAddr < route2->entry.u4IpDestAddr) {
		return -1;
	} else if (route1->entry.u4IpDestAddr > route2->entry.u4IpDestAddr) {
		return 1;
	} else {
		/* if IP is same compare subnet mask */
		if (route1->entry.u4IpSubNetMask < route2->entry.u4IpSubNetMask) {
			return -1;
		} else if (route1->entry.u4IpSubNetMask > route2->entry.u4IpSubNetMask) {
			return 1;
		} else {
			/* If subnet mask also same compare Next hop Ip */
			if (route1->entry.u4NextHopGt < route2->entry.u4NextHopGt) {
				return -1;
			} else if (route1->entry.u4NextHopGt > route2->entry.u4NextHopGt) {
				return 1;
			}
		}
	}

	return 0;
}

static
void swapIPv4RouteRBTree(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct ipv4_next_route_entry *route1 = container_of(e1, struct ipv4_next_route_entry, RbNode);
	struct ipv4_next_route_entry *route2 = container_of(e2, struct ipv4_next_route_entry, RbNode);
	struct ipv4_next_route_entry temp;

	adap_memcpy(&temp, route1, sizeof(struct ipv4_next_route_entry));
	adap_memcpy(&route1->entry, &route2->entry, sizeof(tIpV4NextRoute));
	adap_memcpy(&route2->entry, &temp.entry, sizeof(tIpV4NextRoute));
}

static
int compareIPv4NextHopRBTree(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct dest_ipv4_route_entry *route1 = container_of(e1, struct dest_ipv4_route_entry, RbNode);
	struct dest_ipv4_route_entry *route2 = container_of(e2, struct dest_ipv4_route_entry, RbNode);

	if (route1->entry.ipAddr < route2->entry.ipAddr) {
		return -1;
	} else if (route1->entry.ipAddr > route2->entry.ipAddr) {
		return 1;
	}

	return 0;
}

static
void swapIPv4NextHopRBTree(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct dest_ipv4_route_entry *route1 = container_of(e1, struct dest_ipv4_route_entry, RbNode);
	struct dest_ipv4_route_entry *route2 = container_of(e2, struct dest_ipv4_route_entry, RbNode);
	struct dest_ipv4_route_entry temp;

	adap_memcpy(&temp, route1, sizeof(struct dest_ipv4_route_entry));
	adap_memcpy(&route1->entry, &route2->entry, sizeof(tIpDestV4Routing));
	adap_memcpy(&route2->entry, &temp.entry, sizeof(tIpDestV4Routing));
}

void AdapInitLimiterProfDb();
void AdapInitArpFilterParams();

static void init_route_database(void)
{
	EnetHal_Status_t status;
	ADAP_Uint32 idx=0;
	ADAP_Uint32 idx1=0;
	ADAP_Uint32 uipdn_id=1;
	struct adap_rbtree_params rbtree_params;

	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		tAdapDatabase.adp_IpInterface[idx].valid=ADAP_FALSE;

		tAdapDatabase.adp_IpInterface[idx].MyIpAddr.valid=ADAP_FALSE;
	}

	for(idx=1;idx<=MeaAdapGetNumOfPhyPorts();idx++)
	{
		tAdapDatabase.tL3RouterPort[idx].valid=MEA_FALSE;
		tAdapDatabase.tL3RouterPort[idx].u4IfIndex=idx;
		tAdapDatabase.tL3RouterPort[idx].u2PortVlanId=(4080 + idx);
	}
	
	rbtree_params.capacity = 0; /* Unlimited */
	rbtree_params.compare = compareIPv4NextHopRBTree;
	rbtree_params.swap = swapIPv4NextHopRBTree;
	status = adap_rbtree_init(&tAdapDatabase.adp_nextHopIpPool, &rbtree_params);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, "IPv4 Next Hop RedBlkTree creation failed\n");
	}

	rbtree_params.capacity = 0; /* Unlimited */
	rbtree_params.compare = compareIPv4RouteRBTree;
	rbtree_params.swap = swapIPv4RouteRBTree;
	status = adap_rbtree_init(&tAdapDatabase.adp_nextRoute, &rbtree_params);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, "IPv4 Routing table RedBlkTree creation failed\n");
	}

	idx=0;
	for(idx=0;idx<MAX_HPM_TABLES;idx++)
	{
		tAdapDatabase.tIpRolesDb[idx].valid=ADAP_FALSE;
		tAdapDatabase.tIpRolesDb[idx].tft_profile_Id=MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tIpRolesDb[idx].UEPDN_Id=uipdn_id++;

		for(idx1=0;idx1<MAX_HPM_ENTRY_ROLES;idx1++)
		{
			tAdapDatabase.tIpRolesDb[idx].roleArray[idx1].valid=ADAP_FALSE;
			tAdapDatabase.tIpRolesDb[idx].roleArray[idx1].Action_id=MEA_PLAT_GENERATE_NEW_ID;
			tAdapDatabase.tIpRolesDb[idx].roleArray[idx1].entryId=idx1;

		}
	}
	for(idx=0;idx<MAX_PARENT_CONTROL_ENTRIES;idx++)
	{
		tAdapDatabase.tParentControl[idx].valid=ADAP_FALSE;
		tAdapDatabase.tParentControl[idx].Action_id = MEA_PLAT_GENERATE_NEW_ID;
	}

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		tAdapDatabase.aclHierarchy[idx].valid = ADAP_FALSE;
		tAdapDatabase.aclHierarchy[idx].table_id=0;
		tAdapDatabase.aclHierarchy[idx].serviceId=0;
		tAdapDatabase.aclHierarchy[idx].acl_profile_Hier.acl_profile = 0;
		tAdapDatabase.aclHierarchy[idx].acl_profile_Hier.valid       = MEA_FALSE;
		for(idx1=0;idx1<MEA_ACL_HIERARC_NUM_OF_FLOW;idx1++)
		{
			tAdapDatabase.aclHierarchy[idx].priority[idx1] = 0;
			tAdapDatabase.aclHierarchy[idx].priority_valid[idx1] = ADAP_FALSE;
		}
	}

	for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
	{
		tAdapDatabase.adapAclProfile[idx].valid = ADAP_FALSE;
		tAdapDatabase.adapAclProfile[idx].acl_profile=0;
		adap_memset(tAdapDatabase.adapAclProfile[idx].servicePrevFilt, 0 ,sizeof (tAdapDatabase.adapAclProfile[idx].servicePrevFilt));

	}
	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
    {
        tAdapDatabase.adapAclProfileNum[idx].valid = ADAP_FALSE;
        tAdapDatabase.adapAclProfileNum[idx].acl_profile=idx;
    }

	for (idx = 0; idx < MAX_NUMBER_OF_WHITE_LIST; idx++)
	{
		tAdapDatabase.tFilterList[idx].valid=MEA_FALSE;
		tAdapDatabase.tFilterList[idx].Service_id=MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tFilterList[idx].FilterId=MEA_PLAT_GENERATE_NEW_ID;
	}

   AdapInitArpFilterParams();

	idx = 0;

	if(idx <MAX_NUM_OF_INTERNAL_PORTS)
	{
		tAdapDatabase.internalPorts[idx].inPort = INTERNAL_PORT1_NUMBER;
		tAdapDatabase.internalPorts[idx].serviceId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.internalPorts[idx].mask = D_IP_MASK_24;
		idx++;
	}
	if(idx <MAX_NUM_OF_INTERNAL_PORTS)
	{
		tAdapDatabase.internalPorts[idx].inPort = INTERNAL_PORT2_NUMBER;
		tAdapDatabase.internalPorts[idx].serviceId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.internalPorts[idx].mask = D_IP_MASK_16;
		idx++;
	}

	if(idx <MAX_NUM_OF_INTERNAL_PORTS)
	{
		tAdapDatabase.internalPorts[idx].inPort = INTERNAL_PORT3_NUMBER;
		tAdapDatabase.internalPorts[idx].serviceId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.internalPorts[idx].mask = D_IP_MASK_08;
		idx++;
	}

	if(idx <MAX_NUM_OF_INTERNAL_PORTS)
	{
		tAdapDatabase.internalPorts[idx].inPort = INTERNAL_PORT4_NUMBER;
		tAdapDatabase.internalPorts[idx].serviceId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.internalPorts[idx].mask = 0;
		idx++;
	}

	tAdapDatabase.RouteActionSendToCpu=MEA_PLAT_GENERATE_NEW_ID;

	for(idx=0;idx<MAX_NUM_OF_FAILED_PPP_ARP;idx++)
	{
            tAdapDatabase.FailedPPPArp[idx].valid=ADAP_FALSE;
	}

    for(idx=0;idx<MEA_MAX_ACT_MTU_ID;idx++)
    {
        tAdapDatabase.mtuFlowProfDb[idx].valid=ADAP_FALSE;
        tAdapDatabase.mtuFlowProfDb[idx].mtuProfId = idx;
    }

    /* Init Mac limiter database */
    AdapInitLimiterProfDb();

#ifdef	MEA_NAT_DEMO
	for(idx=0;idx<MAX_NUMBER_ICMP_ECHO_SESSIONS;idx++)
	{
		tAdapDatabase.icmpTableDb[idx].valid=ADAP_FALSE;
		tAdapDatabase.icmpTableDb[idx].newEchoId = 1000+idx;
	}

	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		tAdapDatabase.tcpUdpTable[idx].valid=ADAP_FALSE;
		tAdapDatabase.tcpUdpTable[idx].newSourcePort = 40000+idx;
		tAdapDatabase.tcpUdpTable[idx].HostToNetAtionId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tcpUdpTable[idx].NetToHostAtionId = MEA_PLAT_GENERATE_NEW_ID;
		tAdapDatabase.tcpUdpTable[idx].handle_by_cpu = ADAP_TRUE;
	}
#endif
}


ADAP_Uint32 adapIpInit(void)
{
	init_route_database();
	return ENET_SUCCESS;
}


ADAP_Uint32 MeaAdapGetNumOfPhyPorts(void)
{
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaAdapGetNumOfPhyPorts - %d\n",tAdapDatabase.number_of_physical_ports);
	return tAdapDatabase.number_of_physical_ports;
}

ADAP_Uint32 MeaAdapGetPhyPortFromLogPort(ADAP_Uint32 logPort,MEA_Port_t *pPhyPort)
{
	ADAP_Uint32 idx=0;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaAdapGetPhyPortFromLogPort - logPort:%d\n",logPort);
	for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		if(tAdapDatabase.logical_ports[idx] == logPort)
		{
			(*pPhyPort) = tAdapDatabase.physical_ports[idx];
			return ENET_SUCCESS;
		}
	}
	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"logical port is out of scope ifIndex:%d\r\n",logPort);
	return ENET_FAILURE;
}

void MeaAdapPrintEnetPorts(void)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		printf("logical port:%d phy port:%d\r\n",tAdapDatabase.physical_ports[idx],idx);
	}
}

ADAP_Uint32 MeaAdapIsInternalPort(MEA_Port_t PhyPort)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUM_OF_INTERNAL_PORTS;idx++)
	{
		if(tAdapDatabase.internalPorts[idx].inPort == PhyPort)
		{
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 MeaAdapGetInternalPortMask(MEA_Port_t PhyPort,ADAP_Uint32 *pMask)
{
	ADAP_Uint32 idx=0;

	(*pMask) = 0;
	for(idx=0;idx<MAX_NUM_OF_INTERNAL_PORTS;idx++)
	{
		if(tAdapDatabase.internalPorts[idx].inPort == PhyPort)
		{
			(*pMask) = tAdapDatabase.internalPorts[idx].mask;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}


ADAP_Uint32 MeaAdapGetLogPortFromPhyPort(ADAP_Uint32 *pLogPort,MEA_Port_t PhyPort)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		if(tAdapDatabase.physical_ports[idx] == PhyPort)
		{
			(*pLogPort) = tAdapDatabase.logical_ports[idx];
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"physical port is out of scope phyIndex:%d\r\n",PhyPort);
	return ENET_FAILURE;
}

ADAP_Bool adap_isProcessTerminated(void)
{
	return process_terminate;
}

void adap_setProcessTerminated(void)
{
	process_terminate=MEA_TRUE;
}

ADAP_Uint32 getCfaFromIfindexInterface(ADAP_Uint32 *cfaPort,ADAP_Uint32 logPort)
{
	ADAP_Uint32 start_of_cfa_ifIndex;


	start_of_cfa_ifIndex = (MeaAdapGetNumOfPhyPorts()-1)*2; // phy port index , then lag ports and then cfa ports

	(*cfaPort) = start_of_cfa_ifIndex+logPort;

	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"cfa port:%d\n",(*cfaPort));
	return ENET_SUCCESS;
}

ADAP_Uint32 getPhyFromCfaInterface(ADAP_Uint32 cfaPort,ADAP_Uint32 *pLogPort)
{
	ADAP_Uint32 start_of_cfa_ifIndex;
	ADAP_Uint32 end_of_cfa_ifIndex;


	start_of_cfa_ifIndex = (MeaAdapGetNumOfPhyPorts()-1)*2; // phy port index , then lag ports and then cfa ports
	end_of_cfa_ifIndex = (MeaAdapGetNumOfPhyPorts()-1)*3;

	if( (cfaPort >  start_of_cfa_ifIndex) && (cfaPort <= end_of_cfa_ifIndex) )
	{
		(*pLogPort) =  cfaPort - start_of_cfa_ifIndex;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"cfa port:%d is out of scoper\n",cfaPort);
	return ENET_FAILURE;
}

ADAP_Uint32 MeaAdapGetIntType(ADAP_Uint8 *pIntType,ADAP_Uint32 index)
{
	if(index < MeaAdapGetNumOfPhyPorts() )
	{
		(*pIntType) = tAdapDatabase.interfaceType[index];
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"index port is out of scope index:%d\r\n",index);
	return ENET_FAILURE;
}

ADAP_Uint32 MeaAdapSetIntType(ADAP_Uint8 IntType,ADAP_Uint32 index)
{
	if(index < MeaAdapGetNumOfPhyPorts() )
	{
		tAdapDatabase.interfaceType[index] = IntType;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"index port is out of scope index:%d\r\n",index);
	return ENET_FAILURE;
}

ADAP_Uint32 MeaAdapGetLogPortFromIndex(ADAP_Uint32 *pLogPort,ADAP_Uint32 index)
{
	if(index < MeaAdapGetNumOfPhyPorts() )
	{
		(*pLogPort) = tAdapDatabase.logical_ports[index];
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"index port is out of scope index:%d\r\n",index);
	return ENET_FAILURE;
}

tServiceDb * MeaAdapAllocateServiceId(void)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if(tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.tAllocateServiceId[idx].valid = ADAP_TRUE;
			return &tAdapDatabase.tAllocateServiceId[idx];
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate new service\r\n");
	return NULL;
}

tServiceDb * MeaAdapGetServiceByServiceId(MEA_Service_t serviceId)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if ((tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
           (tAdapDatabase.tAllocateServiceId[idx].serviceId == serviceId))
        {
            return &tAdapDatabase.tAllocateServiceId[idx];
        }
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get service by id = %d\r\n", serviceId);
    return NULL;
}

ADAP_Uint32 MeaAdapFreeServiceId(MEA_Service_t serviceId)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) && (tAdapDatabase.tAllocateServiceId[idx].serviceId == serviceId) )
		{
			tAdapDatabase.tAllocateServiceId[idx].valid = ADAP_FALSE;
			tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type=0;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free serviceId:%d\r\n",serviceId);
	return ENET_FAILURE;
}

tServiceDb * MeaAdapGetFirstServicePort(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) && (tAdapDatabase.tAllocateServiceId[idx].ifIndex == u4IfIndex) )
		{
			return &tAdapDatabase.tAllocateServiceId[idx];
		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetNextServicePort(ADAP_Uint32 u4IfIndex,MEA_Service_t ServId)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint8 found=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if(tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE)
		{
			if(found == 0)
			{
				if( (tAdapDatabase.tAllocateServiceId[idx].ifIndex == u4IfIndex) &&
						(tAdapDatabase.tAllocateServiceId[idx].serviceId == ServId) )
				{
					found = 1;
				}
			}
			// hotfix
			// this function requires refactoring
			else if(tAdapDatabase.tAllocateServiceId[idx].serviceId == ADAP_END_OF_TABLE)
			{
				return NULL;
			}
			else if(tAdapDatabase.tAllocateServiceId[idx].ifIndex == u4IfIndex)
			{
				return &tAdapDatabase.tAllocateServiceId[idx];
			}
		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetFirstServiceVlan(ADAP_Uint32 vlanId)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE)
             && (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId) )
        {
            return &tAdapDatabase.tAllocateServiceId[idx];
        }
    }
    return NULL;
}

tServiceDb * MeaAdapGetNextServiceVlan(ADAP_Uint32 vlanId,MEA_Service_t ServId)
{
    ADAP_Uint32 idx=0;
    ADAP_Uint8 found=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if(tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE)
        {
            if(found == 0)
            {
                if( (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId) &&
                        (tAdapDatabase.tAllocateServiceId[idx].serviceId == ServId) )
                {
                    found = 1;
                }
            }
            // hotfix
            // this function requires refactoring
            else if(tAdapDatabase.tAllocateServiceId[idx].serviceId == ADAP_END_OF_TABLE)
            {
                return NULL;
            }
            else if(tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId)
            {
                return &tAdapDatabase.tAllocateServiceId[idx];
            }
        }
    }
    return NULL;
}

ADAP_Uint32 MeaAdapSetDefaultTmId(MEA_TmId_t tmId,ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		tAdapDatabase.aTm_id[index] = tmId;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

MEA_TmId_t  MeaAdapGetDefaultTmId(ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		return tAdapDatabase.aTm_id[index];
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

tServiceDb * MeaAdapGetServiceIdByEnetPort(ADAP_Uint32 L2_protocol_type,ADAP_Uint16 net_tag,ADAP_Uint16 inner_netTag_from_value,
																					ADAP_Uint32 sub_protocol_type,MEA_Port_t inPort)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == L2_protocol_type) &&
				(tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == sub_protocol_type) &&
				(tAdapDatabase.tAllocateServiceId[idx].innerVlan == inner_netTag_from_value) &&
				(tAdapDatabase.tAllocateServiceId[idx].outerVlan == net_tag) &&
				(tAdapDatabase.tAllocateServiceId[idx].inPort == inPort) )
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetFirstL3ServiceIdByEnetPort(MEA_Port_t inPort)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3) &&
			(tAdapDatabase.tAllocateServiceId[idx].inPort == inPort) )
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetNextL3ServiceIdByEnetPort(MEA_Port_t inPort,MEA_Service_t ServId)
{
    ADAP_Uint32 idx=0;
    ADAP_Uint8 found=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if(tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE)
        {
            if(found == 0)
            {
                if( (tAdapDatabase.tAllocateServiceId[idx].inPort == inPort) &&
                        (tAdapDatabase.tAllocateServiceId[idx].serviceId == ServId) &&
                        (tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3))
                {
                    found = 1;
                }
            }
            else if(tAdapDatabase.tAllocateServiceId[idx].serviceId == ADAP_END_OF_TABLE)
            {
                return NULL;
            }
            else if((tAdapDatabase.tAllocateServiceId[idx].inPort == inPort) &&
                    (tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3))
            {
                return &tAdapDatabase.tAllocateServiceId[idx];
            }
        }
    }
    return NULL;
}

tServiceDb * MeaAdapGetServiceIdByEditType(ADAP_Uint32 L2_protocol_type,ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == L2_protocol_type) &&
				(tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) )
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetServiceIdByVlanPortPri(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 L2_protocol_type,ADAP_Uint16 priority)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
			(tAdapDatabase.tAllocateServiceId[idx].enet_IncPriority == priority) &&
			(tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == L2_protocol_type) &&
			(tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId))
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	return NULL;
}

tServiceDb * MeaAdapGetServiceIdByTrafTypePortPri(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 traffic_type,
        ADAP_Uint16 priority, MEA_IngressPort_Ip_pri_type_t priorityType)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
            (tAdapDatabase.tAllocateServiceId[idx].enet_priority_type == priorityType) &&
            (tAdapDatabase.tAllocateServiceId[idx].enet_IncPriority == priority) &&
            (((traffic_type == D_TAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Ctag )) ||
            ((traffic_type == D_UNTAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Untagged))) )
        {
            return &tAdapDatabase.tAllocateServiceId[idx];

        }
    }
    return NULL;
}

tServiceDb * MeaAdapGetServiceIdByTrafTypePortPriSubType(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 traffic_type,
        ADAP_Uint16 priority, MEA_IngressPort_Ip_pri_type_t priorityType, ADAP_Uint32 subProtocolType)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
            (tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == subProtocolType) &&
            (tAdapDatabase.tAllocateServiceId[idx].enet_priority_type == priorityType) &&
            (tAdapDatabase.tAllocateServiceId[idx].enet_IncPriority == priority) &&
            (((traffic_type == D_TAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Ctag )) ||
            ((traffic_type == D_UNTAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Untagged))) )
        {
            return &tAdapDatabase.tAllocateServiceId[idx];

        }
    }
    return NULL;
}

tServiceDb * MeaAdapGetServiceIdByVlanIfIndex(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 L2_protocol_type)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
			(tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == L2_protocol_type) &&
			((L2_protocol_type == MEA_PARSING_L2_KEY_Untagged) || (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId)))
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	return NULL;
}


tServiceDb * MeaAdapGetServiceIdByPortTrafType(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 traffic_type)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
            (((traffic_type == D_TAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Ctag )) ||
            ((traffic_type == D_UNTAGGED_SERVICE_TAG) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == MEA_PARSING_L2_KEY_Untagged))) )
        {
            return &tAdapDatabase.tAllocateServiceId[idx];

        }
    }
    return NULL;
}

tServiceDb * MeaAdapGetServiceIdByPortTrafTypeSubProtocol(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 traffic_type,
        ADAP_Uint32 subProtocolType)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
    {
        if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.tAllocateServiceId[idx].ifIndex == ifIndex) &&
            (tAdapDatabase.tAllocateServiceId[idx].sub_protocol_type == subProtocolType) &&
            (tAdapDatabase.tAllocateServiceId[idx].L2_protocol_type == traffic_type) &&
            ((traffic_type == D_UNTAGGED_SERVICE_TAG) ||
             (tAdapDatabase.tAllocateServiceId[idx].outerVlan == vlanId)))
        {
            return &tAdapDatabase.tAllocateServiceId[idx];

        }
    }

    return NULL;
}

tServiceDb * MeaAdapGetServiceId(MEA_Service_t serviceId)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) && (tAdapDatabase.tAllocateServiceId[idx].serviceId == serviceId) )
		{
			return &tAdapDatabase.tAllocateServiceId[idx];

		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get database for serviceId:%d\r\n",serviceId);
	return NULL;
}

tServiceDb * MeaAdapGetSimpleServiceIdByPort(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged)
{
	ADAP_Uint16 Index;

	for(Index=0;Index<MAX_NUMBER_OF_SERVICES;Index++)
	{
		if( (tAdapDatabase.tAllocateServiceId[Index].valid == ADAP_TRUE) &&
		    (tAdapDatabase.tAllocateServiceId[Index].ifIndex == PortId) &&
			(tAdapDatabase.tAllocateServiceId[Index].outerVlan == VlanId) &&
			(tAdapDatabase.tAllocateServiceId[Index].L2_protocol_type == u1IsTagged) &&
			(tAdapDatabase.tAllocateServiceId[Index].enet_priority_type  == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP) &&
			(tAdapDatabase.tAllocateServiceId[Index].enet_IncPriority == MEA_PLAT_GENERATE_NEW_ID) )
			{
				return &tAdapDatabase.tAllocateServiceId[Index];
			}
	}
	return NULL;
}

tServiceDb * MeaAdapGetSimpleServiceIdByPortSubType(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged,
        ADAP_Uint32 subProtocolType)
{
	ADAP_Uint16 Index;

    for(Index=0;Index<MAX_NUMBER_OF_SERVICES;Index++)
    {
        if( (tAdapDatabase.tAllocateServiceId[Index].valid == ADAP_TRUE) &&
            (tAdapDatabase.tAllocateServiceId[Index].ifIndex == PortId) &&
            (tAdapDatabase.tAllocateServiceId[Index].sub_protocol_type == subProtocolType) &&
            (tAdapDatabase.tAllocateServiceId[Index].outerVlan == VlanId) &&
            (tAdapDatabase.tAllocateServiceId[Index].L2_protocol_type == u1IsTagged) &&
            (tAdapDatabase.tAllocateServiceId[Index].enet_priority_type  == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP) &&
            (tAdapDatabase.tAllocateServiceId[Index].enet_IncPriority == MEA_PLAT_GENERATE_NEW_ID) )
            {
                return &tAdapDatabase.tAllocateServiceId[Index];
            }
    }
    return NULL;
}

tServiceDb * MeaAdapGetServiceIdByPortPriority(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged,
										ADAP_Uint8  priority, MEA_IngressPort_Ip_pri_type_t priority_type)
{
	ADAP_Uint16 Index;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapGetServiceIdByPortPriority: vlanId:%d portId:%d Tagged:%d priority:%d type:%d\n",
								VlanId,
								PortId,
								u1IsTagged,
								priority,
								priority_type);

	for(Index=0;Index<MAX_NUMBER_OF_SERVICES;Index++)
	{
		if( (tAdapDatabase.tAllocateServiceId[Index].valid == ADAP_TRUE) &&
			(tAdapDatabase.tAllocateServiceId[Index].ifIndex == PortId) &&
			(tAdapDatabase.tAllocateServiceId[Index].L2_protocol_type == u1IsTagged) &&
			(tAdapDatabase.tAllocateServiceId[Index].enet_IncPriority == priority) &&
            (tAdapDatabase.tAllocateServiceId[Index].enet_priority_type == priority_type))
		{
			return &tAdapDatabase.tAllocateServiceId[Index];
		}
	}
	return NULL;
}

ADAP_Uint32 MeaAdapAddPriorityToService (MEA_Service_t Service, ADAP_Uint8 priority, MEA_IngressPort_Ip_pri_type_t priority_type)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_SERVICES;idx++)
	{
		if( (tAdapDatabase.tAllocateServiceId[idx].valid == ADAP_TRUE) && (tAdapDatabase.tAllocateServiceId[idx].serviceId == Service) )
		{
			tAdapDatabase.tAllocateServiceId[idx].enet_IncPriority = priority;
			tAdapDatabase.tAllocateServiceId[idx].enet_priority_type = priority_type;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapAddPriorityToService ServiceId:%d priority:%d priority_type:%d\n", Service,priority,priority_type);
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLAddPriorityToService ServiceId:%d not found\n", Service);
	return ENET_FAILURE;
}

MEA_Policer_prof_t MeaAdapGetDefaultPolicer(ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		return tAdapDatabase.aPolicer_id[index];
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

ADAP_Uint32 MeaAdapSetDefaultPolicer(MEA_Policer_prof_t policerId,ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		tAdapDatabase.aPolicer_id[index] = policerId;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}
MEA_TmId_t MeaAdapGetDefaultTm(ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		return tAdapDatabase.tm_Id[index];
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

ADAP_Uint32 MeaAdapSetDefaultTm(MEA_TmId_t tmId,ADAP_Uint32 index)
{
	if(tAdapDatabase.number_of_physical_ports > index)
	{
		tAdapDatabase.tm_Id[index] = tmId;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

void init_vpn_database(void)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_NUMBER_OF_BRIDGE_DOMAIN;idx++)
	{
		tAdapDatabase.vpnDbPool[idx].valid=MEA_FALSE;
		tAdapDatabase.vpnDbPool[idx].vpn=idx+2;
	}
}

ADAP_Uint32 allocate_vpn_database(ADAP_Uint16 *pVpn)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_NUMBER_OF_BRIDGE_DOMAIN;idx++)
	{
		if(tAdapDatabase.vpnDbPool[idx].valid==MEA_FALSE)
		{
			(*pVpn) = tAdapDatabase.vpnDbPool[idx].vpn;
			tAdapDatabase.vpnDbPool[idx].valid=MEA_TRUE;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate_vpn_database\r\n");
	return ENET_FAILURE;
}

ADAP_Uint32 free_vpn_database(ADAP_Uint16 vpn)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_NUMBER_OF_BRIDGE_DOMAIN;idx++)
	{
		if( (tAdapDatabase.vpnDbPool[idx].valid==MEA_TRUE) && ( tAdapDatabase.vpnDbPool[idx].vpn == vpn) )
		{
			tAdapDatabase.vpnDbPool[idx].valid=MEA_FALSE;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free_vpn_database:%d\r\n",vpn);
	return ENET_FAILURE;
}

static ADAP_Uint32 Adap_bridgeDomainCompare(void *data1,void *data2)
{
	tBridgeDomain *pDomain1=NULL;
	tBridgeDomain *pDomain2=NULL;

	pDomain1 = (tBridgeDomain *)data1;
	pDomain2 = (tBridgeDomain *)data2;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Adap_bridgeDomainCompare bridge1:%d bridge2:%d\r\n",pDomain1->bridgeId,pDomain2->bridgeId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Adap_bridgeDomainCompare swFdb1:%d swFdb2:%d\r\n",pDomain1->swFdb,pDomain2->swFdb);

	if( (pDomain1->bridgeId == pDomain2->bridgeId) && (pDomain1->swFdb == pDomain2->swFdb) )
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 Adap_PepPvidListCompare(void *data1,void *data2)
{
	tPepPvid *pPepPvid1=NULL;
	tPepPvid *pPepPvid2=NULL;

	pPepPvid1 = (tPepPvid *)data1;
	pPepPvid2 = (tPepPvid *)data2;

	if( (pPepPvid1->SVlanId == pPepPvid2->SVlanId) &&
		(pPepPvid1->Pvid == pPepPvid2->Pvid) &&
		(pPepPvid1->u4IfIndex == pPepPvid2->u4IfIndex) )
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 Adap_SVlanMapListCompare (void *data1,void *data2)
{
	tEnetHal_VlanSVlanMap *pVlanSVlanMap1=NULL;
	tEnetHal_VlanSVlanMap *pVlanSVlanMap2=NULL;

	pVlanSVlanMap1 = (tEnetHal_VlanSVlanMap *)data1;
	pVlanSVlanMap2 = (tEnetHal_VlanSVlanMap *)data2;

	if( (pVlanSVlanMap1->SVlanId == pVlanSVlanMap2->SVlanId) &&
		(pVlanSVlanMap1->CVlanId == pVlanSVlanMap2->CVlanId) &&
		(pVlanSVlanMap1->u2Port == pVlanSVlanMap2->u2Port) )
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}


ADAP_Uint32 Adap_addPepPvidListToLinkList(tPepPvid *pPepPvid)
{
	if(ADAP_pushLinkList(&tAdapDatabase.tPepPvidLinkList,(void *)pPepPvid) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to add S-VLAN map to linklist\r\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

tPepPvid *Adap_freePepPvidListLinkList(tPepPvid *pPepPvid)
{
	tPepPvid *pLocalPepPvid;

	ADAP_popLinkList(&tAdapDatabase.tPepPvidLinkList,(void *)&pLocalPepPvid,(void **)&pPepPvid);

	if(pLocalPepPvid == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free accesss list from linklist\r\n");
		return NULL;
	}
	return pLocalPepPvid;
}

tPepPvid * Adap_getPepPvidListLinkList(tPepPvid *pPepPvid)
{
	tPepPvid *pLocalPepPvid=NULL;


	ADAP_getDataLinkList(&tAdapDatabase.tPepPvidLinkList,(void *)&pPepPvid,(void **)&pLocalPepPvid);

	if(pLocalPepPvid == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free bridge from linklist\r\n");
		return NULL;
	}
	return pLocalPepPvid;
}

ADAP_Uint32 Adap_IsPepPvidFoundLinkList(ADAP_Uint16 Svlan, ADAP_Uint16 Cvlan,ADAP_Uint32 ifIndex)
{
	tPepPvid PepPvid;

	PepPvid.Pvid=Cvlan;
	PepPvid.SVlanId=Svlan;
	PepPvid.u4IfIndex=ifIndex;

	if(Adap_getPepPvidListLinkList(&PepPvid) != NULL)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 Adap_addSVlanMapListToLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	if(ADAP_pushLinkList(&tAdapDatabase.tSVlanMapLinkList,(void *)pVlanSVlanMap) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to add S-VLAN map to linklist\r\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

tEnetHal_VlanSVlanMap *Adap_freeSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	tEnetHal_VlanSVlanMap *pLocalVlanSVlanMap;

	ADAP_popLinkList(&tAdapDatabase.tSVlanMapLinkList,(void *)&pLocalVlanSVlanMap,(void **)&pVlanSVlanMap);

	if(pLocalVlanSVlanMap == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free accesss list from linklist\r\n");
		return NULL;
	}
	return pLocalVlanSVlanMap;
}

tEnetHal_VlanSVlanMap * Adap_getSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	tEnetHal_VlanSVlanMap *pLocalVlanSVlanMap=NULL;


	ADAP_getDataLinkList(&tAdapDatabase.tSVlanMapLinkList,(void *)&pVlanSVlanMap,(void **)&pLocalVlanSVlanMap);

	if(pLocalVlanSVlanMap == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free bridge from linklist\r\n");
		return NULL;
	}
	return pLocalVlanSVlanMap;
}

ADAP_Uint32 Adap_getFirstSVlanMapListLinkList(tEnetHal_VlanSVlanMap **pVlanSVlanMap)
{
	(*pVlanSVlanMap) = NULL;
	(*pVlanSVlanMap) = ADAP_getFirstDataLinkList(&tAdapDatabase.tSVlanMapLinkList);
	if((*pVlanSVlanMap) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_getNextSVlanMapListLinkList(tEnetHal_VlanSVlanMap **pVlanSVlanMap)
{
	(*pVlanSVlanMap) = ADAP_getNextDataLinkList(&tAdapDatabase.tSVlanMapLinkList,(void *)(*pVlanSVlanMap));

	if((*pVlanSVlanMap) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_getAllSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMapArr,ADAP_Uint32 *maxNumOfEntries,ADAP_Uint16 Svlan,ADAP_Uint32 ifIndex)
{
	tEnetHal_VlanSVlanMap *pLocalVlanSVlanMap=NULL;
	ADAP_Uint32 numOfEntries=*maxNumOfEntries;
	ADAP_Uint32 ret=ENET_SUCCESS;

	*maxNumOfEntries=0;

	ret = Adap_getFirstSVlanMapListLinkList(&pLocalVlanSVlanMap);
	while(ret == ENET_SUCCESS)
	{
		if( (pLocalVlanSVlanMap->SVlanId == Svlan) && (pLocalVlanSVlanMap->u2Port == ifIndex) )
		{
			if( (*maxNumOfEntries) < numOfEntries)
			{
				adap_memcpy(&pVlanSVlanMapArr[*maxNumOfEntries],pLocalVlanSVlanMap,sizeof(tEnetHal_VlanSVlanMap) );
				(*maxNumOfEntries)++;
			}
		}
		ret = Adap_getNextSVlanMapListLinkList(&pLocalVlanSVlanMap);
	}
	return ENET_SUCCESS;
}

static ADAP_Uint32 Adap_serviceCompare(void *data1,void *data2)
{
	tServiceDb *pService1=NULL;
	tServiceDb *pService2=NULL;

	pService1 = (tServiceDb *)data1;
	pService2 = (tServiceDb *)data2;

	if( (pService1->ifIndex == pService2->ifIndex) && (pService1->L2_protocol_type == pService2->L2_protocol_type) )
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 Adap_AccessListCompare(void *data1,void *data2)
{
    ADAP_Int32 filterNoId1, filterNoId2;
    tAdap_FilterEntry *entry1 = (tAdap_FilterEntry *)data1;
    tAdap_FilterEntry *entry2 = (tAdap_FilterEntry *)data2;
    if (entry1->fType == FILTER_TYPE_L2)
        filterNoId1 = entry1->x.l2FilterEntry.filterNoId;
    else
        filterNoId1 = entry1->x.l3FilterEntry.filterNoId;

    if (entry2->fType == FILTER_TYPE_L2)
        filterNoId2 = entry2->x.l2FilterEntry.filterNoId;
    else
        filterNoId2 = entry2->x.l3FilterEntry.filterNoId;

	if ((entry1->fType == entry2->fType) && (filterNoId1 == filterNoId2))
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;

}

static ADAP_Uint32 Adap_AccessListGreater(void *data1,void *data2)
{
    ADAP_Int32 filterPri1, filterPri2;
    tAdap_FilterEntry *entry1 = (tAdap_FilterEntry *)data1;
    tAdap_FilterEntry *entry2 = (tAdap_FilterEntry *)data2;
    if (entry1->fType == FILTER_TYPE_L2)
        filterPri1 = entry1->x.l2FilterEntry.filterPriority;
    else
        filterPri1 = entry1->x.l3FilterEntry.filterPriority;

    if (entry2->fType == FILTER_TYPE_L2)
        filterPri2 = entry2->x.l2FilterEntry.filterPriority;
    else
        filterPri2 = entry2->x.l3FilterEntry.filterPriority;
    // Do not check ftype in compare
    if (filterPri1 < filterPri2)
    {
        return 1;
    }
    return 0;

}

void Adap_init_accesslist(void)
{
	ADAP_initLinkList(&tAdapDatabase.tAclLinkList,Adap_AccessListCompare);
}

ADAP_Uint32 Adap_L3AccessListToFilterEntry(tEnetHal_L3FilterEntry *pL3FilterEntry,
        tPolicyDb *pPolicy, tAdap_FilterEntry *out)
{
    adap_memset(out, 0, sizeof(tAdap_FilterEntry));
    out->fType = FILTER_TYPE_L3;
    adap_memcpy(&out->x.l3FilterEntry, pL3FilterEntry, sizeof(tEnetHal_L3FilterEntry));
    if (pPolicy)
    {
        adap_memcpy(&out->policy, pPolicy, sizeof(out->policy));
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 Adap_L2AccessListToFilterEntry(tEnetHal_L2FilterEntry *pL2FilterEntry,
        tPolicyDb *pPolicy, tAdap_FilterEntry *out)
{
    adap_memset(out, 0, sizeof(tAdap_FilterEntry));
    out->fType = FILTER_TYPE_L2;
    adap_memcpy(&out->x.l2FilterEntry, pL2FilterEntry, sizeof(tEnetHal_L2FilterEntry));
    if (pPolicy)
    {
        adap_memcpy(&out->policy, pPolicy, sizeof(out->policy));
    }

    return ENET_SUCCESS;
}


ADAP_Uint32 Adap_pushFilterEntryToLinkListSorted(tAdap_FilterEntry *pFilterEntry)
{
	tAdap_FilterEntry *entry = adap_malloc(sizeof(tAdap_FilterEntry));
    adap_memcpy(entry, pFilterEntry, sizeof(tAdap_FilterEntry));

    if(ADAP_pushLinkListBeforeCompKey(&tAdapDatabase.tAclLinkList,(void *)entry, Adap_AccessListGreater) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to add accesslist to linklist\r\n");
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

ADAP_Uint32 Adap_popFilterEntryLinkList(tAdap_FilterEntry *pFilter, tAdap_FilterEntry *out)
{
	tAdap_FilterEntry *entryOut;
	tAdap_FilterEntry entry;

    adap_memcpy(&entry, pFilter, sizeof(tAdap_FilterEntry));

	ADAP_popLinkList(&tAdapDatabase.tAclLinkList,(void *)&entry,(void **)&entryOut);

	if(entryOut == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free access list from linklist\r\n");
		return ENET_FAILURE;
	}

	adap_memcpy(out, entryOut, sizeof(*out));

	free(entryOut);

	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_UpdateFilterEntryInLinkList(tAdap_FilterEntry *pFilterEntry)
{
	tAdap_FilterEntry *entryOut;
	tAdap_FilterEntry entry;
    adap_memcpy(&entry, pFilterEntry, sizeof(tAdap_FilterEntry));

    ADAP_getDataLinkList(&tAdapDatabase.tAclLinkList,(void *)&entry,(void **)&entryOut);

    if(entryOut == NULL)
    {
        //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to update access list from linklist\r\n");
        return ENET_FAILURE;
    }

    adap_memcpy(entryOut, pFilterEntry, sizeof(*pFilterEntry));

    return ENET_SUCCESS;

}

tAdap_FilterEntry *Adap_GetFilterEntryFromLinkList(ADAP_Uint32 filterNoId, eAclFilterTypes fType)
{
    tAdap_FilterEntry *entryOut;
    tAdap_FilterEntry entry;
    adap_memset(&entry, 0, sizeof(tAdap_FilterEntry));
    if (fType == FILTER_TYPE_L2)
        entry.x.l2FilterEntry.filterNoId = filterNoId;
    else
        entry.x.l3FilterEntry.filterNoId = filterNoId;

    entry.fType = fType;

    ADAP_getDataLinkList(&tAdapDatabase.tAclLinkList,(void *)&entry,(void **)&entryOut);

    return entryOut;

}


tAdap_FilterEntry *Adap_GetFirstAccessListLinkList()
{
    return (tAdap_FilterEntry *)ADAP_getFirstDataLinkList(&tAdapDatabase.tAclLinkList);
}

tAdap_FilterEntry *Adap_GetNextAccessListLinkList(tAdap_FilterEntry *entry)
{
    return (tAdap_FilterEntry *)ADAP_getNextDataLinkList(&tAdapDatabase.tAclLinkList, entry);
}

void Adap_init_bridgeDomain(void)
{
	ADAP_Uint32 idx;

	ADAP_initLinkList(&tAdapDatabase.tBridgeDomainDb,Adap_bridgeDomainCompare);
	pthread_mutex_init(&tAdapDatabase.bridge_mutex,NULL);
	pthread_mutex_init(&tAdapDatabase.nat_mutex,NULL);

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		tAdapDatabase.tPvidDb[idx].valid=ADAP_TRUE;
		tAdapDatabase.tPvidDb[idx].ifIndex = idx;
		tAdapDatabase.tPvidDb[idx].pvId = 1;
	}
}

void Adap_bridgeDomain_semLock(void)
{
	pthread_mutex_lock(&tAdapDatabase.bridge_mutex);
}
void Adap_bridgeDomain_semUnlock(void)
{
	pthread_mutex_unlock(&tAdapDatabase.bridge_mutex);
}

void Adap_natDomain_semLock(void)
{
	pthread_mutex_lock(&tAdapDatabase.nat_mutex);
}
void Adap_natDomain_semUnlock(void)
{
	pthread_mutex_unlock(&tAdapDatabase.nat_mutex);
}

ADAP_Uint32 Adap_addServiceToLinkList(tServiceDb *pServiceInfo,tBridgeDomain *pBridgeDomain)
{
	if(ADAP_pushLinkList(&pBridgeDomain->tServiceLinkList,(void *)pServiceInfo) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to add bridge to linklist\r\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_getFirstServiceFromLinkList(tServiceDb **pServiceInfo,tBridgeDomain *pBridgeDomain)
{
	(*pServiceInfo) = NULL;
	(*pServiceInfo) = ADAP_getFirstDataLinkList(&pBridgeDomain->tServiceLinkList);
	if((*pServiceInfo) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

tServiceDb *Adap_freeServiceFromLinkList(tBridgeDomain *pBridgeDomain,ADAP_Uint32 u4IfIndex,ADAP_Uint32 L2_protocol_type,ADAP_Uint16 vlanId)
{
	tServiceDb tServiceInfo;
	tServiceDb *pServiceInfo;

	tServiceInfo.L2_protocol_type=L2_protocol_type;
	tServiceInfo.ifIndex=u4IfIndex;
	tServiceInfo.outerVlan=vlanId;

	ADAP_popLinkList(&pBridgeDomain->tServiceLinkList,(void *)&tServiceInfo,(void **)&pServiceInfo);

	if(pServiceInfo == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Service not found for u4IfIndex:%d vlanId:%d L2_protocol_type:%d \r\n", u4IfIndex, vlanId, L2_protocol_type);
		return NULL;
	}
	return pServiceInfo;
}

ADAP_Uint32 Adap_getNextServiceFromLinkList(tServiceDb **pServiceInfo,tBridgeDomain *pBridgeDomain)
{
	if(pBridgeDomain == NULL)
	{
		return ENET_FAILURE;
	}
	(*pServiceInfo) = ADAP_getNextDataLinkList(&pBridgeDomain->tServiceLinkList,(void *)(*pServiceInfo));

	if((*pServiceInfo) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_getFirstBridgeFromLinkList(tBridgeDomain **pBridgeDomain)
{
	(*pBridgeDomain) = NULL;
	(*pBridgeDomain) = ADAP_getFirstDataLinkList(&tAdapDatabase.tBridgeDomainDb);
	if((*pBridgeDomain) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_getNextBridgeFromLinkList(tBridgeDomain **pBridgeDomain)
{
	if(*pBridgeDomain == NULL)
	{
		return ENET_FAILURE;
	}
	(*pBridgeDomain) = ADAP_getNextDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)(*pBridgeDomain));

	if((*pBridgeDomain) == NULL)
	{
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_deleteAllServiceFromLinkList(tBridgeDomain *pBridgeDomain,func_linklosk_delete func)
{
	ADAP_deleteAllDataLinkList(&pBridgeDomain->tServiceLinkList,func);

	return ENET_SUCCESS;
}

ADAP_Uint32 Adap_allocateBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId)
{
	//first check if exist
	tBridgeDomain tDomain;
	tBridgeDomain *pNewDomain=NULL;
	ADAP_Uint32		idx=0;

	tDomain.bridgeId=bridgeId;
	tDomain.swFdb=swFdb;

	ADAP_getDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)&tDomain,(void **)&pNewDomain);

	// not exist
	if(pNewDomain == NULL)
	{
		pNewDomain = adap_malloc(sizeof(tBridgeDomain));
		if(pNewDomain == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
			return ENET_FAILURE;
		}
		adap_memset(pNewDomain, 0, sizeof(tBridgeDomain));
		if(allocate_vpn_database(&pNewDomain->vpn) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to vpn id\r\n");
			adap_safe_free(pNewDomain);
			return ENET_FAILURE;
		}
		pNewDomain->bridgeId=bridgeId;
		pNewDomain->swFdb=swFdb;
		pNewDomain->num_of_ports=0;

		for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
		{
			pNewDomain->learningMode[idx] = ADAP_VLAN_LEARNING_ENABLED;
		}

		if(ADAP_pushLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)pNewDomain) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to add bridge to linklist\r\n");
			free_vpn_database(pNewDomain->vpn);
			adap_safe_free(pNewDomain);
			return ENET_FAILURE;
		}
		ADAP_initLinkList(&pNewDomain->tServiceLinkList,Adap_serviceCompare);

	}
	return ENET_SUCCESS;
}

tBridgeDomain * Adap_getBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId)
{
	tBridgeDomain tDomain;
	tBridgeDomain *pNewDomain=NULL;

	tDomain.bridgeId=bridgeId;
	tDomain.swFdb=swFdb;

	ADAP_getDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)&tDomain,(void **)&pNewDomain);

	if(pNewDomain == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge from linklist swFdb:%d bridgeId:%d\r\n",swFdb,bridgeId);
		return NULL;
	}
	return pNewDomain;
}

ADAP_Uint32 Adap_freeBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId)
{
	tBridgeDomain tDomain;
	tBridgeDomain *pNewDomain=NULL;

	tDomain.bridgeId=bridgeId;
	tDomain.swFdb=swFdb;

	ADAP_popLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)&tDomain,(void **)&pNewDomain);

	if(pNewDomain == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free bridge from linklist\r\n");
		return ENET_FAILURE;
	}

	if(pNewDomain != NULL)
	{
		free_vpn_database(pNewDomain->vpn);
		adap_safe_free(pNewDomain);
	}
	return ENET_SUCCESS;
}

tBridgeDomain * Adap_getBridgeDomainByVlan(tEnetHal_VlanId VlanId,ADAP_Uint32 bridgeId)
{
	tBridgeDomain *pNewDomain=NULL;

	pNewDomain = (tBridgeDomain *)ADAP_getFirstDataLinkList(&tAdapDatabase.tBridgeDomainDb);

	while(pNewDomain != NULL)
	{
		if( (pNewDomain->bridgeId == bridgeId) &&(pNewDomain->vlanId == VlanId) )
		{
			return pNewDomain;
		}
		pNewDomain = (tBridgeDomain *)ADAP_getNextDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)pNewDomain);
	}
	return NULL;
}

tBridgeDomain * Adap_getBridgeDomainByFdb(ADAP_Uint32 u4Fid)
{
	tBridgeDomain *pNewDomain=NULL;

	pNewDomain = (tBridgeDomain *)ADAP_getFirstDataLinkList(&tAdapDatabase.tBridgeDomainDb);

	while(pNewDomain != NULL)
	{
		if(pNewDomain->swFdb == u4Fid)
		{
			return pNewDomain;
		}
		pNewDomain = (tBridgeDomain *)ADAP_getNextDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)pNewDomain);
	}
	return NULL;
}

tBridgeDomain * Adap_getBridgeDomainByVpn(ADAP_Uint16 u2Vpn)
{
	tBridgeDomain *pNewDomain=NULL;

	pNewDomain = (tBridgeDomain *)ADAP_getFirstDataLinkList(&tAdapDatabase.tBridgeDomainDb);

	while(pNewDomain != NULL)
	{
		if(pNewDomain->vpn == u2Vpn)
		{
			return pNewDomain;
		}
		pNewDomain = (tBridgeDomain *)ADAP_getNextDataLinkList(&tAdapDatabase.tBridgeDomainDb,(void *)pNewDomain);
	}
	return NULL;
}

ADAP_Uint16 adap_GetVpnByVlan(tEnetHal_VlanId VlanId)
{
	tBridgeDomain *pNewDomain=NULL;

	pNewDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pNewDomain!= NULL)
	{
		return pNewDomain->vpn;
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Bool adap_VlanCheckPortsInDomain(tBridgeDomain *pBridgeDomain, ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint32			idx=0;
	ADAP_Bool			ret = ADAP_FALSE;

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( ((pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) && (pBridgeDomain->ifIndex[idx] == u2PortNumber)) ||
				((pBridgeDomain->ifType[idx] == VLAN_TAGGED_MEMBER_PORT) && (pBridgeDomain->ifIndex[idx] == u2PortNumber)) )
			{
				ret = ADAP_TRUE;
				break;
			}
		}
	}
	return ret;
}

ADAP_Bool adap_VlanCheckUntaggedPort(tEnetHal_VlanId VlanId,ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint32			idx=0;
	ADAP_Bool			ret = ADAP_FALSE;

	tBridgeDomain *pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId, 0);

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( (pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) && (pBridgeDomain->ifIndex[idx] == u2PortNumber) )
			{
				ret = ADAP_TRUE;
				break;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();
	return ret;
}

ADAP_Bool adap_VlanCheckTaggedPort(tEnetHal_VlanId VlanId,ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint32			idx=0;
	ADAP_Bool			ret = ADAP_FALSE;

	tBridgeDomain *pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId, 0);

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( (pBridgeDomain->ifType[idx] == VLAN_TAGGED_MEMBER_PORT) && (pBridgeDomain->ifIndex[idx] == u2PortNumber) )
			{
				ret = ADAP_TRUE;
				break;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();
	return ret;
}

ADAP_Uint16 getPortToPvid(ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if( (tAdapDatabase.tPvidDb[idx].valid == ADAP_TRUE) && (tAdapDatabase.tPvidDb[idx].ifIndex == ifIndex) )
		{
			return tAdapDatabase.tPvidDb[idx].pvId;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 setPortToPvid(ADAP_Uint32 ifIndex,ADAP_Uint16 pvid)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if( (tAdapDatabase.tPvidDb[idx].valid == ADAP_TRUE) && (tAdapDatabase.tPvidDb[idx].ifIndex == ifIndex) )
		{
			tAdapDatabase.tPvidDb[idx].pvId = pvid;
			return ENET_SUCCESS;
		}
	}

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if(tAdapDatabase.tPvidDb[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.tPvidDb[idx].ifIndex = ifIndex;
			tAdapDatabase.tPvidDb[idx].valid = ADAP_TRUE;
			tAdapDatabase.tPvidDb[idx].pvId = pvid;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"not enough space for pvid\r\n");
	return ENET_FAILURE;
}

ADAP_Uint32 getPortSpape(ADAP_Uint32 ifIndex,eAdap_PortStatus *pState)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		(*pState) = tAdapDatabase.tportState[ifIndex];
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

ADAP_Uint32 setPortSpape(ADAP_Uint32 ifIndex,eAdap_PortStatus newState)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.tportState[ifIndex] = newState;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

tEnetHal_FsNpL3IfInfo *getRouterPortInfo(ADAP_Uint32 ifIndex)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.tL3RouterPort[ifIndex];
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return NULL;
}

ADAP_Uint32 getLxcpId(ADAP_Uint32 ifIndex,MEA_LxCp_t *pNewLxcp)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		(*pNewLxcp) = tAdapDatabase.tLxcpId[ifIndex];
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

ADAP_Uint32 setLxcpId(ADAP_Uint32 ifIndex,MEA_LxCp_t lxcp)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.tLxcpId[ifIndex] = lxcp;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

ADAP_Uint32 getLxcpErpsId(ADAP_Uint32 ifIndex,MEA_LxCp_t *pNewLxcpErps)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		(*pNewLxcpErps) = tAdapDatabase.tLxcpErpsId[ifIndex];
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

ADAP_Uint32 setLxcpErpsId(ADAP_Uint32 ifIndex,MEA_LxCp_t lxcpErps)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.tLxcpErpsId[ifIndex] = lxcpErps;
		return ENET_SUCCESS;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex is out of scope:%d\r\n",ifIndex);
	return ENET_FAILURE;
}

static ADAP_Uint32 Adap_RoutintingCompare(void *data1,void *data2)
{
	tIpDestV4Routing *pRoute1 = (tIpDestV4Routing *)data1;
	tIpDestV4Routing *pRoute2 = (tIpDestV4Routing *)data2;

	if(pRoute1->ipAddr == pRoute2->ipAddr)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 AdapCompareIpv6Neigbor(void *data1,void *data2)
{
	tIpv6Neighbor *neighbor1 = (tIpv6Neighbor *) data1;
	tIpv6Neighbor *neighbor2 = (tIpv6Neighbor *) data2;

	if (adap_memcmp(neighbor1->ipv6Addr, neighbor2->ipv6Addr, sizeof(tIPv6Addr)) == 0)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 AdapCompareIpv6Route(void *data1,void *data2)
{
	tIpv6Route *Ipv6Route1 = (tIpv6Route *) data1;
	tIpv6Route *Ipv6Route2 = (tIpv6Route *) data2;

	if (adap_memcmp(Ipv6Route1->destIpv6Addr, Ipv6Route2->destIpv6Addr, sizeof(tIPv6Addr)) == 0)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 AdapCompareIpv6NeigborCache(void *data1,void *data2)
{
	tIpv6NeighborCache *neighbor1 = (tIpv6NeighborCache *) data1;
	tIpv6NeighborCache *neighbor2 = (tIpv6NeighborCache *) data2;

	if (adap_memcmp(neighbor1->neighbor.ipv6Addr, neighbor2->neighbor.ipv6Addr, 
		sizeof(tIPv6Addr)) == 0)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adapIpv6Init(void)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		tAdapDatabase.adp_Ipv6Interface[idx].valid = ADAP_FALSE;
	}
	ADAP_initLinkList(&tAdapDatabase.Ipv6NeighborCacheList, AdapCompareIpv6NeigborCache);
	ADAP_initLinkList(&tAdapDatabase.Ipv6RoutingTable, AdapCompareIpv6Route);
	return ENET_SUCCESS;
}

tIpv6InterfaceTable *AdapAllocateIpv6Interface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if( (tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex == IfIndex) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
		{
			return &tAdapDatabase.adp_Ipv6Interface[idx];
		}
	}
	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.adp_Ipv6Interface[idx].valid = ADAP_TRUE;
			tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex = IfIndex;
			ADAP_initLinkList(&tAdapDatabase.adp_Ipv6Interface[idx].Ipv6NeigborLinkList, AdapCompareIpv6Neigbor);
			return &tAdapDatabase.adp_Ipv6Interface[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 AdapDeleteIpv6Interface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if( (tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex == IfIndex) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
		{
			tAdapDatabase.adp_Ipv6Interface[idx].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tIpv6InterfaceTable *AdapGetIpv6Interface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if( (tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex == IfIndex) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
			return &tAdapDatabase.adp_Ipv6Interface[idx];
	}
	return NULL;
}

tIpv6InterfaceTable *AdapGetIpv6InterfaceByVlanId(ADAP_Uint16 vlanId)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if( (tAdapDatabase.adp_Ipv6Interface[idx].u2VlanId == vlanId) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
			return &tAdapDatabase.adp_Ipv6Interface[idx];
	}
	return NULL;
}

tIpv6InterfaceTable *AdapGetFirstIpv6Interface(void)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE)
			return &tAdapDatabase.adp_Ipv6Interface[idx];
	}
	return NULL;
}

tIpv6InterfaceTable *AdapGetNextIpv6Interface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;
	ADAP_Uint32 found = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if(found == 1)
		{
			//get the next one
			if(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE)
			{
				return &tAdapDatabase.adp_Ipv6Interface[idx];
			}
		}
		else if( (tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex == IfIndex) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
		{
			found = 1;
		}
	}
	return NULL;
}

ADAP_Uint32 AdapIsVlanIpv6Interface(ADAP_Uint16 vlanId)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if( (tAdapDatabase.adp_Ipv6Interface[idx].u2VlanId == vlanId) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) )
			return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapSetIpv6InterfaceType (ADAP_Uint32 IfIndex, ADAP_Uint32 IfType)
{
	tIpv6InterfaceTable *pInterfaceTable = NULL;

	pInterfaceTable = AdapGetIpv6Interface(IfIndex);
	if (pInterfaceTable != NULL)
	{
		pInterfaceTable->IfType = IfType;
		return ENET_SUCCESS;
	}

	return ENET_FAILURE;
}

ADAP_Uint32 AdapIsPppIpv6Interface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if((tAdapDatabase.adp_Ipv6Interface[idx].ifIndex == IfIndex) && 
			(tAdapDatabase.adp_Ipv6Interface[idx].IfType == IF_PPP))
		{
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapIpv6L3InterfaceGetInnerVlan(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx = 0;

	for(idx = 0; idx < MAX_NUM_OF_L3_INTERFACES; idx++)
	{
		if((tAdapDatabase.adp_Ipv6Interface[idx].u4CfaIfIndex == IfIndex) && 
			(tAdapDatabase.adp_Ipv6Interface[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adp_Ipv6Interface[idx].u2InnerVlanId != 0))
		{
			return tAdapDatabase.adp_Ipv6Interface[idx].u2InnerVlanId;
		}
	}
	return ADAP_FALSE;
}

tIpv6Route *AdapGetIpv6Route(tIPv6Addr ipv6Addr)
{
	tIpv6Route input;
	tIpv6Route *Ipv6Route = NULL;

	adap_memcpy(input.destIpv6Addr, ipv6Addr, sizeof(tIPv6Addr));
	ADAP_getDataLinkList(&tAdapDatabase.Ipv6RoutingTable, (void *) &input, (void **) &Ipv6Route);
	if (Ipv6Route != NULL)
	{
		return Ipv6Route;
	}

	return NULL;
}

ADAP_Uint32 AdapAddIpv6Route(tIpv6Route *Ipv6Route)
{
	return ADAP_pushLinkList(&tAdapDatabase.Ipv6RoutingTable, (void *) Ipv6Route);
}

ADAP_Uint32 AdapDeleteIpv6Route(tIpv6Route *Ipv6Route)
{
	tIpv6Route *Ipv6RouteRet = NULL;
	ADAP_popLinkList(&tAdapDatabase.Ipv6RoutingTable, (void *) Ipv6Route, (void **) &Ipv6RouteRet);
	adap_safe_free(Ipv6RouteRet);

	return ENET_SUCCESS;
}

tIpv6NeighborCache *AdapFindIpv6Neighbor(tIPv6Addr ipv6Addr)
{
	tIpv6NeighborCache input;
	tIpv6NeighborCache *neighborCache = NULL;

	adap_memcpy(input.neighbor.ipv6Addr, ipv6Addr, sizeof(tIPv6Addr));
	ADAP_getDataLinkList(&tAdapDatabase.Ipv6NeighborCacheList, (void *) &input, (void **) &neighborCache);
	if (neighborCache != NULL)
	{
		return neighborCache;
	}

	return NULL;
}

tIpv6NeighborCache *AdapFindNextIpv6Neighbor(tIPv6Addr ipv6Addr)
{
	tIpv6NeighborCache input;
	tIpv6NeighborCache *neighborCache = NULL;

	adap_memcpy(input.neighbor.ipv6Addr, ipv6Addr, sizeof(tIPv6Addr));
	ADAP_getDataLinkList(&tAdapDatabase.Ipv6NeighborCacheList, (void *) &input, (void **) &neighborCache);
	if (neighborCache != NULL)
	{
		return (tIpv6NeighborCache *) ADAP_getNextDataLinkList(&tAdapDatabase.Ipv6NeighborCacheList, 
			(void *) neighborCache);
	}

	return NULL;
}

tIpv6Neighbor *AdapGetIpv6NeighborEntry(ADAP_Uint32 ifIndex, tIPv6Addr ipv6Addr)
{
	tIpv6Neighbor input;
	tIpv6Neighbor *neighbor = NULL;
	tIpv6InterfaceTable *ipv6Interface = AdapGetIpv6Interface(ifIndex);

	if (ipv6Interface == NULL)
		return NULL;

	adap_memcpy(input.ipv6Addr, ipv6Addr, sizeof(tIPv6Addr));
	ADAP_getDataLinkList(&ipv6Interface->Ipv6NeigborLinkList, (void *) &input, (void **) &neighbor);

	return neighbor;
}

ADAP_Uint32 AdapAddIpv6NeighborEntry(ADAP_Uint32 ifIndex, tIpv6Neighbor *neighbor)
{
	tIpv6InterfaceTable *ipv6Interface = AdapGetIpv6Interface(ifIndex);
	tIpv6NeighborCache neighborCache;

	if (neighbor == NULL || ipv6Interface == NULL)
		return ENET_FAILURE;

	if(ADAP_pushLinkList(&ipv6Interface->Ipv6NeigborLinkList, (void *) neighbor) != ENET_SUCCESS)
	{
		return ENET_FAILURE;
	}

	neighborCache.neighbor = *neighbor;
	neighborCache.ifIndex = ifIndex;
	return ADAP_pushLinkList(&tAdapDatabase.Ipv6NeighborCacheList, (void *) &neighborCache);
}

tIpv6Neighbor *AdapRemoveIpv6NeigborEntry(ADAP_Uint32 ifIndex, tIPv6Addr ipv6Addr)
{
	tIpv6Neighbor input;
	tIpv6Neighbor *neighbor = NULL;
	tIpv6NeighborCache neighborCacheInput;
	tIpv6NeighborCache *neighborCache;
	tIpv6InterfaceTable *ipv6Interface = AdapGetIpv6Interface(ifIndex);

	if (ipv6Interface == NULL)
		return NULL;

	adap_memcpy(input.ipv6Addr, ipv6Addr, sizeof(tIPv6Addr));
	ADAP_popLinkList(&ipv6Interface->Ipv6NeigborLinkList, (void *)&input, (void **) &neighbor);

	neighborCacheInput.neighbor = input;
	ADAP_popLinkList(&tAdapDatabase.Ipv6NeighborCacheList, (void *)&neighborCacheInput, (void **) &neighborCache);
	adap_safe_free(neighborCache);

	return neighbor;
}

tIpInterfaceTable *AdapAllocateIpInterface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if( (tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex == IfIndex) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
		{
			//ADAP_initLinkList(&tAdapDatabase.adp_RouterTable[idx].tNextHopIpLinkList,Adap_RoutintingCompare);
			return &tAdapDatabase.adp_IpInterface[idx];
		}
	}
	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if(tAdapDatabase.adp_IpInterface[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.adp_IpInterface[idx].valid=ADAP_TRUE;
			tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex=IfIndex;
			ADAP_initLinkList(&tAdapDatabase.adp_IpInterface[idx].tNextHopIpLinkList,Adap_RoutintingCompare);
			return &tAdapDatabase.adp_IpInterface[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 AdapDeleteIpInterface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if( (tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex == IfIndex) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
		{
			//ADAP_initLinkList(&tAdapDatabase.adp_RouterTable[idx].tNextHopIpLinkList,Adap_RoutintingCompare);
			tAdapDatabase.adp_IpInterface[idx].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tIpInterfaceTable *AdapGetIpInterface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if( (tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex == IfIndex) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
			return &tAdapDatabase.adp_IpInterface[idx];
	}
	return NULL;
}

tIpInterfaceTable *AdapGetIpInterfaceByVlanId(ADAP_Uint16 vlanId)
{
	ADAP_Uint32 idx;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if( (tAdapDatabase.adp_IpInterface[idx].u2VlanId == vlanId) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
			return &tAdapDatabase.adp_IpInterface[idx];
	}
	return NULL;
}

tIpInterfaceTable *AdapGetFirstIpInterface(void)
{
	ADAP_Uint32 idx;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if(tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE)
			return &tAdapDatabase.adp_IpInterface[idx];
	}
	return NULL;
}

tIpInterfaceTable *AdapGetNextIpInterface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;
	ADAP_Uint32 found=0;


	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if(found == 1)
		{
			//get the next one
			if(tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE)
			{
				return &tAdapDatabase.adp_IpInterface[idx];
			}
		}
		else if( (tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex == IfIndex) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
		{
			found=1;
		}
	}
	return NULL;
}


ADAP_Uint32 AdapIsVlanIpInterface(ADAP_Uint16 vlanId)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if( (tAdapDatabase.adp_IpInterface[idx].u2VlanId == vlanId) && (tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) )
			return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapSetInterfaceType (ADAP_Uint32 IfIndex, ADAP_Uint32 IfType)
{
	tIpInterfaceTable *pInterfaceTable=NULL;

    	pInterfaceTable = AdapGetIpInterface(IfIndex);
	if (pInterfaceTable != NULL)
	{
		pInterfaceTable->IfType = IfType;
		return ENET_SUCCESS;
	}
	
	return ENET_FAILURE;
}

ADAP_Uint32 AdapIsPppInterface(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if((tAdapDatabase.adp_IpInterface[idx].ifIndex == IfIndex) && 
			(tAdapDatabase.adp_IpInterface[idx].IfType == IF_PPP))
		{
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapL3InterfaceGetInnerVlan(ADAP_Uint32 IfIndex)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if((tAdapDatabase.adp_IpInterface[idx].u4CfaIfIndex == IfIndex) && 
			(tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adp_IpInterface[idx].u2InnerVlanId != 0))
		{
			return tAdapDatabase.adp_IpInterface[idx].u2InnerVlanId;
		}
	}
	return ADAP_FALSE;
}

ADAP_Uint32 AdapChkDoubleTaggedInt ()
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_L3_INTERFACES;idx++)
	{
		if ((tAdapDatabase.adp_IpInterface[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adp_IpInterface[idx].u2InnerVlanId != 0))
		{
			return ADAP_TRUE;
		}
	}
	return ADAP_FALSE;
}


//#define DEBUG_NAT		1

ADAP_Uint32 AdapGetEntryIpTable(tIpDestV4Routing **pAddNextHop,ADAP_Uint32 ipAddr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct dest_ipv4_route_entry input;
	struct dest_ipv4_route_entry *ipv4NextHop;

	*pAddNextHop = NULL;

#ifdef DEBUG_NAT
	static tIpDestV4Routing tempNextHop;
	tempNextHop.macAddress[0]=0x00;
	tempNextHop.macAddress[1]=0x00;
	tempNextHop.macAddress[2]=(ADAP_Uint8)((ipAddr & 0xFF000000) >> 24);
	tempNextHop.macAddress[3]=(ADAP_Uint8)((ipAddr & 0x00FF0000) >> 16);
	tempNextHop.macAddress[4]=(ADAP_Uint8)((ipAddr & 0x0000FF00) >> 8);
	tempNextHop.macAddress[5]=(ADAP_Uint8)((ipAddr & 0x000000FF) );
	(*pAddNextHop) = &tempNextHop;
	return ENET_SUCCESS;
#endif

	input.entry.ipAddr = ipAddr;
    status = adap_rbtree_find(tAdapDatabase.adp_nextHopIpPool, &input.RbNode, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
    	ipv4NextHop = container_of(rbtree_elem, struct dest_ipv4_route_entry, RbNode);
    	*pAddNextHop = &ipv4NextHop->entry;
    	return ENET_SUCCESS;
    } else {
    	return ENET_FAILURE;
    }
}

ADAP_Uint32 AdapAllocateEntryIpTable(tIpDestV4Routing **pAddNextHop,ADAP_Uint32 ipAddr)
{
	EnetHal_Status_t status;
	struct dest_ipv4_route_entry *ipv4NextHop;

	*pAddNextHop = NULL;

	if (AdapGetEntryIpTable(pAddNextHop, ipAddr) == ENET_SUCCESS) {
		return ENET_SUCCESS;
	}

	ipv4NextHop = (struct dest_ipv4_route_entry *)adap_malloc(sizeof(struct dest_ipv4_route_entry));
	if (ipv4NextHop == NULL) {
		return ENET_FAILURE;
	}

	ipv4NextHop->entry.ipAddr = ipAddr;
	status = adap_rbtree_insert(tAdapDatabase.adp_nextHopIpPool, &ipv4NextHop->RbNode);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return ENET_FAILURE;
	}

	*pAddNextHop = &ipv4NextHop->entry;
	return ENET_SUCCESS;
}

ADAP_Uint32 AdapFreeEntryIpTable(tIpDestV4Routing *pAddNextHop)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct dest_ipv4_route_entry input;
	struct dest_ipv4_route_entry *ipv4NextHop;

	input.entry.ipAddr = pAddNextHop->ipAddr;
	status = adap_rbtree_remove(tAdapDatabase.adp_nextHopIpPool, &input.RbNode, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return ENET_FAILURE;
	}

	ipv4NextHop = container_of(rbtree_elem, struct dest_ipv4_route_entry, RbNode);
	adap_safe_free(ipv4NextHop);

	return ENET_SUCCESS;
}

tIpDestV4Routing *AdapGetEntryHopIpTable(tIpInterfaceTable *pRouteTable,ADAP_Uint32 ipAddr)
{
	tIpDestV4Routing temp;
	tIpDestV4Routing *GetHopIp=NULL;

	temp.ipAddr=ipAddr;

	ADAP_getDataLinkList(&pRouteTable->tNextHopIpLinkList,(void *)&temp,(void **)&GetHopIp);

	return GetHopIp;
}

ADAP_Uint32 AdapAddEntryHopIpTable(tIpInterfaceTable *pRouteTable,tIpDestV4Routing *pAddNextHop)
{
	ADAP_Uint32 ret=ENET_SUCCESS;


	ret = ADAP_pushLinkList(&pRouteTable->tNextHopIpLinkList,(void **)pAddNextHop);

	return ret;
}

tIpDestV4Routing *AdapDelEntryHopIpTable(tIpInterfaceTable *pRouteTable,ADAP_Uint32 ipAddr)
{
	tIpDestV4Routing temp;
	tIpDestV4Routing *GetHopIp=NULL;

	temp.ipAddr=ipAddr;

	ADAP_popLinkList(&pRouteTable->tNextHopIpLinkList,(void *)&temp,(void **)&GetHopIp);

	return GetHopIp;
}

ADAP_Uint32 AdapgetRouteInternalServiceId(MEA_Service_t  *pServiceId,MEA_Port_t portId)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NUM_OF_INTERNAL_PORTS;idx++)
	{
		if(portId == tAdapDatabase.internalPorts[idx].inPort)
		{
			(*pServiceId) = tAdapDatabase.internalPorts[idx].serviceId;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapSetRouteInternalServiceId(MEA_Service_t  ServiceId,MEA_Port_t portId)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NUM_OF_INTERNAL_PORTS;idx++)
	{
		if(portId == tAdapDatabase.internalPorts[idx].inPort)
		{
			tAdapDatabase.internalPorts[idx].serviceId = ServiceId;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

MEA_Action_t AdapGetRouteActionToCpu(void)
{
	return tAdapDatabase.RouteActionSendToCpu;
}

void AdapSetRouteActionToCpu(MEA_Action_t action)
{
	tAdapDatabase.RouteActionSendToCpu=action;
}

void AdapSetBridgeMode(ADAP_Uint32 u4BridgeMode)
{
	tAdapDatabase.BridgeMode = u4BridgeMode;
}

ADAP_Uint32 AdapGetBridgeMode(void)
{
	return tAdapDatabase.BridgeMode;
}

void AdapSetMcastMode(ADAP_Uint32 u4McastMode)
{
	tAdapDatabase.McastMode = u4McastMode;
}

ADAP_Uint32 AdapGetMcastMode(void)
{
	return tAdapDatabase.McastMode;
}

ADAP_Uint32 AdapSetProviderBridgeMode(ADAP_Uint32  mode,ADAP_Uint32 ifIndex)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		tAdapDatabase.providerBridgePortMode[ifIndex] = mode;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapGetProviderBridgeMode(ADAP_Uint32  *pMode,ADAP_Uint32 ifIndex)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		(*pMode) = tAdapDatabase.providerBridgePortMode[ifIndex];
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapSetProviderCoreBridgeMode(ADAP_Uint32  mode,ADAP_Uint32 ifIndex)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		tAdapDatabase.providerCoreBridgePortMode[ifIndex] = mode;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapGetProviderCoreBridgeMode(ADAP_Uint32  *pMode,ADAP_Uint32 ifIndex)
{
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		(*pMode) = tAdapDatabase.providerCoreBridgePortMode[ifIndex];
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapGetUserPriority(ADAP_Uint32 u4IfIndex,ADAP_Uint32 *defPriority)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		(*defPriority) = tAdapDatabase.defPriority[u4IfIndex];
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}
ADAP_Uint32 AdapSetUserPriority(ADAP_Uint32 u4IfIndex,ADAP_Uint8 defPriority)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		tAdapDatabase.defPriority[u4IfIndex] = defPriority;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}


ADAP_Uint32 AdapGetAccepFrameType(ADAP_Uint32 u4IfIndex,ADAP_Uint8 *pAcceptableType)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		(*pAcceptableType) = tAdapDatabase.acceptableType[u4IfIndex];
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}
ADAP_Uint32 AdapSetAccepFrameType(ADAP_Uint32 u4IfIndex,ADAP_Uint8 acceptableType)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS )
	{
		tAdapDatabase.acceptableType[u4IfIndex] = acceptableType;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapSetAccepFrameTypeMode( ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 u1AccepFrameType)
{
	ADAP_Uint32 index=0;

	for(index=0;index<ADAP_VLAN_MAX_NUMBER_OF_ACCEPTABLE_FRAMES_TYPE;index++)
	{
		if( (tAdapDatabase.AccepFrameTypeMode[index].valid == ADAP_TRUE) &&
			(tAdapDatabase.AccepFrameTypeMode[index].u4IfIndex == u4IfIndex) &&
			(tAdapDatabase.AccepFrameTypeMode[index].SVlanId == SVlanId) )
		{
			tAdapDatabase.AccepFrameTypeMode[index].u1AccepFrameType = u1AccepFrameType;
			return ENET_SUCCESS;
		}
	}
	for(index=0;index<ADAP_VLAN_MAX_NUMBER_OF_ACCEPTABLE_FRAMES_TYPE;index++)
	{
		if(tAdapDatabase.AccepFrameTypeMode[index].valid == ADAP_FALSE)
		{
			tAdapDatabase.AccepFrameTypeMode[index].u1AccepFrameType = u1AccepFrameType;
			tAdapDatabase.AccepFrameTypeMode[index].SVlanId = SVlanId;
			tAdapDatabase.AccepFrameTypeMode[index].u4IfIndex = u4IfIndex;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}



ADAP_Uint32 AdapGetAccepFrameTypeMode(ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 *pu1AccepFrameType)
{
	ADAP_Uint32 index=0;

	for(index=0;index<ADAP_VLAN_MAX_NUMBER_OF_ACCEPTABLE_FRAMES_TYPE;index++)
	{
		if( (tAdapDatabase.AccepFrameTypeMode[index].valid == ADAP_TRUE) &&
			(tAdapDatabase.AccepFrameTypeMode[index].u4IfIndex == u4IfIndex) &&
			(tAdapDatabase.AccepFrameTypeMode[index].SVlanId == SVlanId) )
		{
			(*pu1AccepFrameType) = tAdapDatabase.AccepFrameTypeMode[index].u1AccepFrameType;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}


/*****************************/
ADAP_Uint32 AdapGetIpNextRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry input;
    struct ipv4_next_route_entry *ipv4Routing;

    *pAddNextHop = NULL;

    input.entry.u4IpDestAddr = pMatchNextHop->u4IpDestAddr;
    input.entry.u4IpSubNetMask = pMatchNextHop->u4IpSubNetMask;
    input.entry.u4NextHopGt = pMatchNextHop->u4NextHopGt;

    status = adap_rbtree_find(tAdapDatabase.adp_nextRoute, &input.RbNode, &rbtree_elem);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	return ENET_FAILURE;
    }

    ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
    *pAddNextHop = &ipv4Routing->entry;

    return ENET_SUCCESS;
}

ADAP_Uint32 AdapGetIpRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry *ipv4Routing;

	*pAddNextHop = NULL;

	status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
		if ((ipv4Routing->entry.u4IpDestAddr == pMatchNextHop->u4IpDestAddr) &&
				(ipv4Routing->entry.u4IpSubNetMask == pMatchNextHop->u4IpSubNetMask)) {
			*pAddNextHop = &ipv4Routing->entry;
			return ENET_SUCCESS;
		}

		status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, rbtree_elem, &rbtree_elem);
	}

	return ENET_FAILURE;
}

ADAP_Uint32 AdapGetDefaultGatewayRouteTable(tIpV4NextRoute **pAddNextHop)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry *ipv4Routing;

	*pAddNextHop = NULL;

	status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
		if (ipv4Routing->entry.u4IpSubNetMask == 0)	{
			*pAddNextHop = &ipv4Routing->entry;
			return ENET_SUCCESS;
		}

		status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, rbtree_elem, &rbtree_elem);
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default gateway not found\n");
	return ENET_FAILURE;
}

ADAP_Uint32 AdapAllocIpNextRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop)
{
	EnetHal_Status_t status;
	struct ipv4_next_route_entry *ipv4Routing;

	*pAddNextHop = NULL;

	if (AdapGetIpNextRouteTable(pAddNextHop, pMatchNextHop) == ENET_SUCCESS) {
		return ENET_SUCCESS;
	}

	ipv4Routing = (struct ipv4_next_route_entry *)adap_malloc(sizeof(struct ipv4_next_route_entry));
	if (ipv4Routing == NULL) {
		return ENET_FAILURE;
	}

	ipv4Routing->entry.valid = ADAP_TRUE;
	ipv4Routing->entry.u4IpDestAddr = pMatchNextHop->u4IpDestAddr;
	ipv4Routing->entry.u4IpSubNetMask = pMatchNextHop->u4IpSubNetMask;
	ipv4Routing->entry.u2VlanId = pMatchNextHop->u2VlanId;
	ipv4Routing->entry.u4IfIndex = pMatchNextHop->u4IfIndex;
	ipv4Routing->entry.u4NextHopGt = pMatchNextHop->u4NextHopGt;
	status = adap_rbtree_insert(tAdapDatabase.adp_nextRoute, &ipv4Routing->RbNode);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return ENET_FAILURE;
	}

	*pAddNextHop = &ipv4Routing->entry;
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IPv4Routing entry created\n");

	return ENET_SUCCESS;
}
ADAP_Uint32 AdapFreeIpNextRouteTable(tIpV4NextRoute *pMatchNextHop)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry input;
	struct ipv4_next_route_entry *ipv4Routing;

	input.entry.u4IpDestAddr = pMatchNextHop->u4IpDestAddr;
	input.entry.u4IpSubNetMask = pMatchNextHop->u4IpSubNetMask;
	input.entry.u4NextHopGt = pMatchNextHop->u4NextHopGt;
	status = adap_rbtree_remove(tAdapDatabase.adp_nextRoute, &input.RbNode, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return ENET_FAILURE;
	}

	ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
	adap_safe_free(ipv4Routing);

	return ENET_SUCCESS;
}

tIpV4NextRoute *AdapGetFirstIpNextRouteTable(void)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry *ipv4Routing;

	status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, NULL, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return NULL;
	}

	ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
	return &ipv4Routing->entry;
}

tIpV4NextRoute *AdapGetNextIpNextRouteTable(tIpV4NextRoute *pEntry)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct ipv4_next_route_entry input;
	struct ipv4_next_route_entry *ipv4Routing;
	
	input.entry.u4IpDestAddr = pEntry->u4IpDestAddr;
	input.entry.u4IpSubNetMask = pEntry->u4IpSubNetMask;
	input.entry.u4NextHopGt = pEntry->u4NextHopGt;
	status = adap_rbtree_iter(tAdapDatabase.adp_nextRoute, &input.RbNode, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return NULL;
	}

	ipv4Routing = container_of(rbtree_elem, struct ipv4_next_route_entry, RbNode);
	return &ipv4Routing->entry;
}
tEnetHal_VlanId getDefaultVlanId(void)
{
	return tAdapDatabase.defaultVlanId;
}
void setDefaultVlanId(tEnetHal_VlanId vlanId)
{
	tAdapDatabase.defaultVlanId = vlanId;
}

ADAP_Bool adap_CheckVlanForPort(ADAP_Uint32 u4IfIndex, tEnetHal_VlanId VlanId, ADAP_Uint16 *vpn)
{
	ADAP_Uint32			idx=0;
	tBridgeDomain 		*pBridgeDomain=NULL;
	ADAP_Bool			ret = ADAP_FALSE;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( (pBridgeDomain->ifIndex[idx] == u4IfIndex) && (pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) )
			{
				*vpn = pBridgeDomain->vpn;
				ret = ADAP_TRUE;
				break;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();
	return ret;
}

tIpRoleData *adap_getIpRolesDb(ADAP_Uint8 	tableId)
{
	ADAP_Uint32 index;
	for(index=0;index<MAX_HPM_TABLES;index++)
	{
		if( (tAdapDatabase.tIpRolesDb[index].valid == ADAP_TRUE) &&
			(tAdapDatabase.tIpRolesDb[index].tableId == tableId) )
		{
			return &tAdapDatabase.tIpRolesDb[index];
		}
	}
	return NULL;
}

tIpRoleData *adap_allocateIpRolesDb(ADAP_Uint8 	tableId)
{
	ADAP_Uint32 index;
	for(index=0;index<MAX_HPM_TABLES;index++)
	{
		if( (tAdapDatabase.tIpRolesDb[index].valid == ADAP_TRUE) &&
			(tAdapDatabase.tIpRolesDb[index].tableId == tableId) )
		{
			return &tAdapDatabase.tIpRolesDb[index];
		}
	}
	for(index=0;index<MAX_HPM_TABLES;index++)
	{
		if(tAdapDatabase.tIpRolesDb[index].valid == ADAP_FALSE)
		{
			tAdapDatabase.tIpRolesDb[index].tableId = tableId;
			tAdapDatabase.tIpRolesDb[index].valid = ADAP_TRUE;
			return &tAdapDatabase.tIpRolesDb[index];
		}
	}
	return NULL;
}

ADAP_Bool adap_isIpRolesDb(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				(pRoleData->roleArray[index].daIpAddr == ip) &&
				(pRoleData->roleArray[index].daMaskAddr == mask))
		{
			return ADAP_TRUE;
		}
	}
	return ADAP_FALSE;
}

ADAP_Bool adap_isIpV6RolesDb(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				adap_memcmp(&pRoleData->roleArray[index].daIpv6Addr,ipv6,sizeof(tEnetHal_IPv6Addr))==0 &&
				adap_memcmp(&pRoleData->roleArray[index].dav6MaskAddr,mask,sizeof(tEnetHal_IPv6Addr)) == 0)
		{
			return ADAP_TRUE;
		}
	}
	return ADAP_FALSE;
}

ADAP_Uint32 adap_freeIpv6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				adap_memcmp(&pRoleData->roleArray[index].daIpv6Addr,ipv6,sizeof(tEnetHal_IPv6Addr))==0 &&
				adap_memcmp(&pRoleData->roleArray[index].dav6MaskAddr,mask,sizeof(tEnetHal_IPv6Addr)) == 0)
		{
			pRoleData->roleArray[index].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_freeIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask)
{
	ADAP_Uint32 index;
	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				(pRoleData->roleArray[index].daIpAddr == ip) &&
				(pRoleData->roleArray[index].daMaskAddr == mask) )
		{
			pRoleData->roleArray[index].valid = ADAP_FALSE;
			pRoleData->roleArray[index].hwConfig = ADAP_FALSE;
			pRoleData->roleArray[index].daIpAddr=0;
			pRoleData->roleArray[index].daMaskAddr=0;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tIpRole *adap_getFirstIpRoleId(tIpRoleData *pRoleData)
{
	ADAP_Uint32 index;
	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if(pRoleData->roleArray[index].valid == ADAP_TRUE)
		{
			return &pRoleData->roleArray[index];
		}
	}
	return NULL;
}

tIpRole *adap_getNextIpRoleId(tIpRoleData *pRoleData,tIpRole *pCurrRole)
{
	ADAP_Uint32 index;
	ADAP_Uint32 found=0;
	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if(found == 0)
		{
			if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
					(pRoleData->roleArray[index].daIpAddr == pCurrRole->daIpAddr) &&
					(pRoleData->roleArray[index].daMaskAddr == pCurrRole->daMaskAddr) )
			{
				found=1;
			}
		}
		else
		{
			if(pRoleData->roleArray[index].valid == ADAP_TRUE)
			{
				return &pRoleData->roleArray[index];
			}
		}
	}
	return NULL;
}

tIpRole *adap_getIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask)
{
	ADAP_Uint32 index;
	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				(pRoleData->roleArray[index].daIpAddr == ip) &&
				(pRoleData->roleArray[index].daMaskAddr == mask) )
		{
			return &pRoleData->roleArray[index];
		}
	}
	return NULL;
}
tIpRole *adap_getIpv6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_HPM_ENTRY_ROLES;index++)
	{
		if( (pRoleData->roleArray[index].valid == ADAP_TRUE) &&
				adap_memcmp(&pRoleData->roleArray[index].daIpv6Addr,ipv6,sizeof(tEnetHal_IPv6Addr))==0 &&
				adap_memcmp(&pRoleData->roleArray[index].dav6MaskAddr,mask,sizeof(tEnetHal_IPv6Addr)) == 0)
		{
			return &pRoleData->roleArray[index];
		}
	}
	return NULL;
}

tIpRole *adap_allocateIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask,mea_action_type_te type)
{
	ADAP_Uint32 index;

	if(type == MEA_ACTION_TYPE_HPM)
	{
		for(index=0;index<MAX_NUM_OF_TFP_ENTRIES;index++)
		{
			if(pRoleData->roleArray[index].valid == ADAP_FALSE)
			{
				pRoleData->roleArray[index].daIpAddr = ip;
				pRoleData->roleArray[index].daMaskAddr = mask;
				pRoleData->roleArray[index].type=type;
				pRoleData->roleArray[index].valid = ADAP_TRUE;
				return &pRoleData->roleArray[index];

			}
		}
		return NULL;
	}
	else
	{
		for(index=MAX_NUM_OF_TFP_ENTRIES;index<MAX_HPM_ENTRY_ROLES;index++)
		{
			if(pRoleData->roleArray[index].valid == ADAP_FALSE)
			{
				pRoleData->roleArray[index].daIpAddr = ip;
				pRoleData->roleArray[index].daMaskAddr = mask;
				pRoleData->roleArray[index].type=type;
				pRoleData->roleArray[index].valid = ADAP_TRUE;
				return &pRoleData->roleArray[index];

			}
		}
	}
	return NULL;
}

tIpRole *adap_allocateIpV6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask,mea_action_type_te type)
{
	ADAP_Uint32 index;

	if(type == MEA_ACTION_TYPE_HPM)
	{
		for(index=0;index<MAX_NUM_OF_TFP_ENTRIES;index++)
		{
			if(pRoleData->roleArray[index].valid == ADAP_FALSE)
			{
				adap_memcpy(&pRoleData->roleArray[index].daIpv6Addr,ipv6,sizeof(tEnetHal_IPv6Addr));
				adap_memcpy(&pRoleData->roleArray[index].dav6MaskAddr,mask,sizeof(tEnetHal_IPv6Addr));
				pRoleData->roleArray[index].type=type;
				return &pRoleData->roleArray[index];

			}
		}
		return NULL;
	}
	else
	{
		for(index=MAX_NUM_OF_TFP_ENTRIES;index<MAX_HPM_ENTRY_ROLES;index++)
		{
			if(pRoleData->roleArray[index].valid == ADAP_FALSE)
			{
				adap_memcpy(&pRoleData->roleArray[index].daIpv6Addr,ipv6,sizeof(tEnetHal_IPv6Addr));
				adap_memcpy(&pRoleData->roleArray[index].dav6MaskAddr,mask,sizeof(tEnetHal_IPv6Addr));
				pRoleData->roleArray[index].type=type;
				return &pRoleData->roleArray[index];

			}
		}
	}
	return NULL;
}
tParentControl *adap_getParentConrol(ADAP_Uint32 saIp,ADAP_Uint32 daIp)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_PARENT_CONTROL_ENTRIES;index++)
	{
		if( (tAdapDatabase.tParentControl[index].valid == ADAP_TRUE) &&
				(tAdapDatabase.tParentControl[index].saIpAddr == saIp) &&
				(tAdapDatabase.tParentControl[index].daIpAddr == daIp) )
		{
			return &tAdapDatabase.tParentControl[index];
		}
	}
	return NULL;
}
tParentControl *adap_allocateParentConrol(ADAP_Uint32 saIp,ADAP_Uint32 daIp)
{
	ADAP_Uint32 index;

	for(index=0;index<MAX_PARENT_CONTROL_ENTRIES;index++)
	{
		if( (tAdapDatabase.tParentControl[index].valid == ADAP_TRUE) &&
				(tAdapDatabase.tParentControl[index].saIpAddr == saIp) &&
				(tAdapDatabase.tParentControl[index].daIpAddr == daIp) )
		{
			return &tAdapDatabase.tParentControl[index];
		}
	}
	for(index=0;index<MAX_PARENT_CONTROL_ENTRIES;index++)
	{
		if(tAdapDatabase.tParentControl[index].valid == ADAP_FALSE)
		{
			tAdapDatabase.tParentControl[index].valid = ADAP_TRUE;
			tAdapDatabase.tParentControl[index].saIpAddr = saIp;
			tAdapDatabase.tParentControl[index].daIpAddr = daIp;
			tAdapDatabase.tParentControl[index].Action_id = MEA_PLAT_GENERATE_NEW_ID;
			return &tAdapDatabase.tParentControl[index];
		}
	}
	return NULL;
}

ADAP_Uint32 ADAP_setPortShaper(tDatabaseShaperParams *shaperDb)
{
	if(shaperDb->portid < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		adap_memcpy(&tAdapDatabase.tShaperDb[shaperDb->portid],shaperDb,sizeof(tDatabaseShaperParams));
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

tDatabaseShaperParams *ADAP_getPortShaper(ADAP_Uint32 portId)
{
	if(portId < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.tShaperDb[portId];
	}
	return NULL;
}

ADAP_Bool adap_is_part_lag_port(ADAP_Uint32 logPort)
{
	ADAP_Uint32 index;

	for(index=0;index<tAdapDatabase.number_of_physical_ports;index++)
	{
		if( (tAdapDatabase.valid_onf_ports[index] == ADAP_TRUE) && (tAdapDatabase.logical_onf_ports[index] == logPort) )
		{
			return tAdapDatabase.port_part_of_lag[index];
		}
	}
	return ADAP_FALSE;
}

ADAP_Uint32 adap_add_lag_port(ADAP_Uint32 phyPort)
{
	ADAP_Uint32 index;

	for(index=0;index<tAdapDatabase.number_of_physical_ports;index++)
	{
		if( (tAdapDatabase.valid_onf_ports[index] == ADAP_TRUE) && (tAdapDatabase.physical_onf_ports[index] == phyPort) )
		{
			tAdapDatabase.port_part_of_lag[index] = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_add_onf_port(ADAP_Uint32 logPort,ADAP_Uint32 phyPort)
{
	ADAP_Uint32 index;

	for(index=0;index<tAdapDatabase.number_of_physical_ports;index++)
	{
		if( (tAdapDatabase.valid_onf_ports[index] == ADAP_TRUE) && (tAdapDatabase.logical_onf_ports[index] == logPort) )
		{
			return ENET_SUCCESS;
		}
	}
	//MeaAdapSetIngressPort (ADAP_CPU_PORT_ID,ADAP_TRUE,ADAP_FALSE);
	for(index=0;index<MAX_NUM_OF_SUPPORTED_PORTS;index++)
	{
		if(tAdapDatabase.valid_onf_ports[index] == ADAP_FALSE)
		{
			tAdapDatabase.logical_onf_ports[index] = logPort;
			tAdapDatabase.physical_onf_ports[index] = phyPort;
			//tAdapDatabase.number_of_physical_ports++;
			tAdapDatabase.valid_onf_ports[index] = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_get_onf_phy_port(ADAP_Uint32 logPort,MEA_Port_t *pPhyPort)
{
	ADAP_Uint32 index;
	for(index=0;index<tAdapDatabase.number_of_physical_ports;index++)
	{
		if( (tAdapDatabase.valid_onf_ports[index] == ADAP_TRUE) && (tAdapDatabase.logical_onf_ports[index] == logPort) )
		{
			(*pPhyPort) = tAdapDatabase.physical_onf_ports[index];
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}
ADAP_Uint32 adap_get_onf_log_port(ADAP_Uint32 index,ADAP_Uint32 *plogPort)
{
	if(tAdapDatabase.valid_onf_ports[index] == ADAP_TRUE)
	{
		(*plogPort) = tAdapDatabase.logical_onf_ports[index];
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

ADAP_Uint32 MeaAdapGetNumOfOnfPhyPorts(void)
{
	return tAdapDatabase.number_of_physical_ports;
}

ADAP_Uint32 adap_InsertEntryToFilterList(MEA_Filter_t 	FilterId,MEA_Service_t   Service_id, tEnetHal_MacAddr *macAddr)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUMBER_OF_WHITE_LIST;idx++)
	{
		if(tAdapDatabase.tFilterList[idx].valid == MEA_FALSE)
		{
			tAdapDatabase.tFilterList[idx].valid = MEA_TRUE;
			tAdapDatabase.tFilterList[idx].Service_id = Service_id;
			tAdapDatabase.tFilterList[idx].FilterId = FilterId;
			adap_memcpy(&tAdapDatabase.tFilterList[idx].macAddr,macAddr,sizeof(tEnetHal_MacAddr));
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

MEA_Filter_t adap_DeleteEntryFromFilterListMacAddr(MEA_Service_t   Service_id, tEnetHal_MacAddr *pMacAddr)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUMBER_OF_WHITE_LIST;idx++)
	{
		if( (tAdapDatabase.tFilterList[idx].valid == MEA_FALSE) && (tAdapDatabase.tFilterList[idx].Service_id == Service_id) &&
			(adap_memcmp(&tAdapDatabase.tFilterList[idx].macAddr,pMacAddr,sizeof(tEnetHal_MacAddr)) ==0 ) )
		{
			tAdapDatabase.tFilterList[idx].valid = MEA_FALSE;
			return tAdapDatabase.tFilterList[idx].FilterId;
		}
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

MEA_Filter_t adap_DeleteEntryFromFilterList(MEA_Service_t   Service_id)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUMBER_OF_WHITE_LIST;idx++)
	{
		if( (tAdapDatabase.tFilterList[idx].valid == MEA_FALSE) && (tAdapDatabase.tFilterList[idx].Service_id == Service_id))
		{
			tAdapDatabase.tFilterList[idx].valid = MEA_FALSE;
			return tAdapDatabase.tFilterList[idx].FilterId;
		}
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

tMeterDb *MeaAdapGetMeterParams(ADAP_Uint16 meter_id)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( (tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.MeterTable[idx].issMeter.i4QoSMeterId == meter_id) )
		{
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapGetMeterParams Index: %d for meter_id:%d!!\n", idx, meter_id);
			return &tAdapDatabase.MeterTable[idx];
		}
	}
	return NULL;
}

tMeterDb *MeaAdapGetMeterByIndex(ADAP_Uint16 meterIdx)
{
	if(meterIdx >=MAX_NUMBER_OF_METERING )
		return NULL;

	return &tAdapDatabase.MeterTable[meterIdx];
}

ADAP_Uint32 adap_GetMeterIndexFromPolicer(ADAP_Uint16 PolicerIndex)
{
	if(PolicerIndex == ADAP_END_OF_TABLE)
	{
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PolicyTable[PolicerIndex].MeterIdx;
}


void MeaAdapDeleteServiceFromPmDb(MEA_Service_t	 service)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 idx1=0;

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE )
		{
			for(idx1=0;idx1<MAX_NUMBER_OF_PM_PER_METER;idx1++)
			{
				if(tAdapDatabase.MeterTable[idx].pmDb[idx1].service == service)
				{
					tAdapDatabase.MeterTable[idx].pmDb[idx1].valid=ADAP_FALSE;
				}
			}
		}
	}
}
void MeaAdapDeleteActionFromPmDb(MEA_Action_t	action)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 idx1=0;

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE )
		{
			for(idx1=0;idx1<MAX_NUMBER_OF_PM_PER_METER;idx1++)
			{
				if(tAdapDatabase.MeterTable[idx].pmDb[idx1].action == action)
				{
					tAdapDatabase.MeterTable[idx].pmDb[idx1].valid=ADAP_FALSE;
				}
			}
		}
	}
}

tMeterDb *MeaAdapGetMeterParamsByPolicerId(MEA_Policer_prof_t policer_prof_id)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( (tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.MeterTable[idx].policer_prof_id == policer_prof_id) )
		{
			return &tAdapDatabase.MeterTable[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 MeaAdapSetPmIdByMeter(tMeterDb *pMeter, MEA_PmId_t pmId, MEA_Service_t Service_Id)
{
	ADAP_Uint32 idx=0;

	if(pMeter == NULL)
	{
		return ENET_FAILURE;
	}

	for(idx=0;idx<MAX_NUMBER_OF_PM_PER_METER;idx++)
	{
		if((pMeter->pmDb[idx].valid == ADAP_TRUE) && (pMeter->pmDb[idx].service == Service_Id))
		{
			pMeter->pmDb[idx].pmId = pmId;
			break;
		}
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 MeaAdapFreeMeterParams(ADAP_Uint16 meter_id)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( (tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.MeterTable[idx].issMeter.i4QoSMeterId == meter_id) )
		{
			tAdapDatabase.MeterTable[idx].valid = ADAP_FALSE;
			adap_memset(&tAdapDatabase.MeterTable[idx].issMeter, 0x00, sizeof(tEnetHal_QoSMeterEntry));
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_UpdateMeterPolicerClassifier(ADAP_Uint16 MeterIndex, ADAP_Uint16 PolicerIndex, ADAP_Uint16 ClassifierIndex, ADAP_Uint8 ToMap)
{
	ADAP_Uint16  		QueueIdx;
	tQueueDb     		*pQueue = NULL;

	if(ToMap == 1)
    {
        if(PolicerIndex != ADAP_END_OF_TABLE)
        {
            if(ClassifierIndex != ADAP_END_OF_TABLE)
            {
            	tAdapDatabase.PolicyTable[PolicerIndex].ClassifierIdx = ClassifierIndex;
            	tAdapDatabase.ClassifierTable[ClassifierIndex].PolicyIdx = PolicerIndex;

                //Try to find queue according to the classifier and associate it with policer
                for(QueueIdx=0; QueueIdx<MAX_NUM_OF_QUEUES; QueueIdx++)
                {
                    pQueue = adap_getQueueEntryByIndex(QueueIdx);
                    if (pQueue == NULL || pQueue->ClassIdx == ADAP_END_OF_TABLE)
                         continue;

                    if(pQueue->ClassIdx == ClassifierIndex)
                    {
                    	tAdapDatabase.PolicyTable[PolicerIndex].QueueIdx = QueueIdx;
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLUpdateMeterPolicerClassifier: PolicerIndex:%d, QueueIdx:%d\n", PolicerIndex, QueueIdx);
                    	break;
                    }
                }
            }

            if(MeterIndex != ADAP_END_OF_TABLE)
            {
            	tAdapDatabase.PolicyTable[PolicerIndex].MeterIdx = MeterIndex;
                tAdapDatabase.MeterTable[MeterIndex].PolicyIdx = PolicerIndex;
            }
        }
    }
    else
    {
        if(MeterIndex != ADAP_END_OF_TABLE)
        {
        	tAdapDatabase.MeterTable[MeterIndex].PolicyIdx = ADAP_END_OF_TABLE;
        }

        if(PolicerIndex != ADAP_END_OF_TABLE)
        {
        	tAdapDatabase.PolicyTable[PolicerIndex].ClassifierIdx = ADAP_END_OF_TABLE;
        	tAdapDatabase.PolicyTable[PolicerIndex].MeterIdx = ADAP_END_OF_TABLE;
            tAdapDatabase.PolicyTable[PolicerIndex].QueueIdx = ADAP_END_OF_TABLE;
        }

        if(ClassifierIndex != ADAP_END_OF_TABLE)
        {
        	tAdapDatabase.ClassifierTable[ClassifierIndex].PolicyIdx = ADAP_END_OF_TABLE;
        }
    }

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_MapClassToQueue(ADAP_Int32 i4IfIndex, ADAP_Uint16 ClassifierIndex, ADAP_Uint32 u4QId, ADAP_Uint8 u1Flag)
{
	ADAP_Uint16  PolicerIndex, QueueIndex;

    PolicerIndex = tAdapDatabase.ClassifierTable[ClassifierIndex].PolicyIdx;
    QueueIndex = adap_getQueueIndexById(i4IfIndex, u4QId);
    
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_MapClassToQueue: ClassifierIndex:%d, u4QId:%d, QueueIndex:%d, PolicerIndex:%d\n", ClassifierIndex, u4QId, QueueIndex, PolicerIndex);

    if(QueueIndex != ADAP_END_OF_TABLE)
    {
        if(u1Flag == 1) /* Map */
        {
        	if(PolicerIndex != ADAP_END_OF_TABLE)
        		tAdapDatabase.PolicyTable[PolicerIndex].QueueIdx = QueueIndex;
        	tAdapDatabase.QueueTable[QueueIndex].ClassIdx = ClassifierIndex;
        }
        else  /* UnMap */
        {
        	if(PolicerIndex != ADAP_END_OF_TABLE)
        		tAdapDatabase.PolicyTable[PolicerIndex].QueueIdx = ADAP_END_OF_TABLE;
        	tAdapDatabase.QueueTable[QueueIndex].ClassIdx = ADAP_END_OF_TABLE;
        }
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPLMapClassToQueue ERROR: Unable to get Queue Index forQid: %d!!\n", u4QId);
        return ENET_FAILURE;
    }

	return ENET_SUCCESS;
}

ADAP_Uint16 MeaAdapGetMeterIdLinkedToIP (ADAP_Uint32 ipv4Addr)
{
        ADAP_Uint32 index = 0;
	ADAP_Uint32 index_2 = 0;
	ADAP_Uint32 filterIp = 0;
	ADAP_Uint32 filterMask = 0;
	ADAP_Uint16 PolicyIdx = 0;

        for(index = 1; index < MAX_NUM_OF_CLASSIFIERS; index++)
        {
                if( (tAdapDatabase.ClassifierTable[index].valid != ADAP_TRUE))
		{
			continue;
		}
		for (index_2 = 0; index_2 < ADAP_MAX_NUM_ACL_FILTERS; index_2++)
		{
			if (tAdapDatabase.ClassifierTable[index].issFilter.l3Valid[index_2] != MEA_TRUE)
			{
				continue;
			}
			filterIp = tAdapDatabase.ClassifierTable[index].issFilter.L3Filter[index_2].FilterDstIpAddr;
			filterMask = tAdapDatabase.ClassifierTable[index].issFilter.L3Filter[index_2].FilterDstIpAddrMask;
			PolicyIdx = tAdapDatabase.ClassifierTable[index].PolicyIdx;

			if (((filterIp & filterMask) == (ipv4Addr & filterMask)) && 
				(PolicyIdx != ADAP_END_OF_TABLE))
			{
				return tAdapDatabase.PolicyTable[PolicyIdx].MeterIdx;
			} 
		}
	}

	return ADAP_END_OF_TABLE;
}
tMulticastMac *adap_getMulticastMacEntry(tEnetHal_MacAddr *macAddress)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_NUMBER_OF_MULTICAST_MAC;idx++)
	{
		if( (tAdapDatabase.multicastMac[idx].valid==ADAP_TRUE) && (adap_memcmp(&tAdapDatabase.multicastMac[idx].macAddress,macAddress,sizeof(tEnetHal_MacAddr)) == 0) )
		{
			return &tAdapDatabase.multicastMac[idx];
		}
	}
	return NULL;
}

tMulticastMac *adap_createMulticastMacEntry(tEnetHal_MacAddr *macAddress)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_NUMBER_OF_MULTICAST_MAC;idx++)
	{
		if( (tAdapDatabase.multicastMac[idx].valid==ADAP_TRUE) && (adap_memcmp(&tAdapDatabase.multicastMac[idx].macAddress,macAddress,sizeof(tEnetHal_MacAddr)) == 0) )
		{
			return &tAdapDatabase.multicastMac[idx];
		}
	}
	for(idx=0;idx<ADAP_NUMBER_OF_MULTICAST_MAC;idx++)
	{
		if(tAdapDatabase.multicastMac[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.multicastMac[idx].valid=MEA_TRUE;
			adap_memcpy(&tAdapDatabase.multicastMac[idx].macAddress,macAddress,sizeof(tEnetHal_MacAddr));
			return &tAdapDatabase.multicastMac[idx];
		}
	}
	return NULL;
}

void adap_freeMulticastMacEntry(tEnetHal_MacAddr *macAddress)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_NUMBER_OF_MULTICAST_MAC;idx++)
	{
		if( (tAdapDatabase.multicastMac[idx].valid==ADAP_TRUE) && (adap_memcmp(&tAdapDatabase.multicastMac[idx].macAddress,macAddress,sizeof(tEnetHal_MacAddr)) == 0) )
		{
			tAdapDatabase.multicastMac[idx].valid=MEA_FALSE;
			tAdapDatabase.multicastMac[idx].portbitmap=0;
			tAdapDatabase.multicastMac[idx].vlanId=0;
			adap_memset(&tAdapDatabase.multicastMac[idx].macAddress,0,sizeof(tEnetHal_MacAddr));
		}
	}
}

ADAP_Uint32 adap_insertEntryToMacLearning(tMacLearningTable *newEntry)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_MAC_LEARNING;idx++)
	{
		if( (tAdapDatabase.macLearning[idx].valid == MEA_TRUE) &&
			(adap_memcmp(&tAdapDatabase.macLearning[idx].MacAddr,&newEntry->MacAddr,sizeof(tEnetHal_MacAddr)) == 0) &&
			(tAdapDatabase.macLearning[idx].u4FdbId == newEntry->u4FdbId) )
		{
			return ENET_SUCCESS;
		}
	}

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_MAC_LEARNING;idx++)
	{
		if(tAdapDatabase.macLearning[idx].valid == MEA_FALSE)
		{
			adap_memcpy(&tAdapDatabase.macLearning[idx],newEntry,sizeof(tMacLearningTable));
			tAdapDatabase.macLearning[idx].valid = MEA_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_getEntryFromMacLearning(tMacLearningTable *newEntry,ADAP_Uint32 fdbId,tEnetHal_MacAddr *pMacAddr)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_MAC_LEARNING;idx++)
	{
		if( (tAdapDatabase.macLearning[idx].valid == MEA_TRUE) &&
			(adap_memcmp(&tAdapDatabase.macLearning[idx].MacAddr,pMacAddr,sizeof(tEnetHal_MacAddr)) == 0) &&
			(tAdapDatabase.macLearning[idx].u4FdbId == fdbId) )
		{
			adap_memcpy(newEntry,&tAdapDatabase.macLearning[idx],sizeof(tMacLearningTable));

			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_getEntryFromMacLearningFromIndex(tMacLearningTable *newEntry,ADAP_Uint32 index)
{
	if(index >= ADAP_MAX_NUMBER_OF_MAC_LEARNING)
	{
		return ENET_FAILURE;
	}

	if(tAdapDatabase.macLearning[index].valid == MEA_FALSE)
	{
		return ENET_FAILURE;
	}

	adap_memcpy(newEntry,&tAdapDatabase.macLearning[index],sizeof(tMacLearningTable));

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_IsMacLearningTableFull(void)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_MAC_LEARNING;idx++)
	{
		if(tAdapDatabase.macLearning[idx].valid == MEA_FALSE)
		{
			return ENET_FAILURE;
		}
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 adap_resetMacLearningTable(void)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_MAC_LEARNING;idx++)
	{
		tAdapDatabase.macLearning[idx].valid = MEA_FALSE;
	}
	return ENET_SUCCESS;
}

tMeterDb *MeaAdapAllocateMeterParams(ADAP_Uint16 meter_id)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 idx1=0;
	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( (tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.MeterTable[idx].issMeter.i4QoSMeterId == meter_id) )
		{
            //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapAllocateMeterParams existing Index: %d for meter_id:%d!!\n", idx, meter_id);
			return &tAdapDatabase.MeterTable[idx];
		}
	}

	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if(tAdapDatabase.MeterTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.MeterTable[idx].valid=ADAP_TRUE;
			tAdapDatabase.MeterTable[idx].issMeter.i4QoSMeterId = meter_id;
			adap_memset(&tAdapDatabase.MeterTable[idx].pmDb[0],0,sizeof(tPmPerMeter)*MAX_NUMBER_OF_PM_PER_METER);
			for(idx1=0;idx1<MAX_NUMBER_OF_PM_PER_METER;idx1++)
			{
				tAdapDatabase.MeterTable[idx].pmDb[idx1].valid=ADAP_FALSE;
				tAdapDatabase.MeterTable[idx].pmDb[idx1].service=MEA_PLAT_GENERATE_NEW_ID;
				tAdapDatabase.MeterTable[idx].pmDb[idx1].action=MEA_PLAT_GENERATE_NEW_ID;
			}
            //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapAllocateMeterParams new Index: %d for meter_id:%d!!\n", idx, meter_id);
			return &tAdapDatabase.MeterTable[idx];

		}
	}
	return NULL;
}

ADAP_Uint16 adap_getMeterIndex(ADAP_Int32 MeterId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUMBER_OF_METERING;idx++)
	{
		if( (tAdapDatabase.MeterTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.MeterTable[idx].issMeter.i4QoSMeterId==MeterId) )
		{
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_getMeterIndex Index: %d for meter_id:%d!!\n", idx, MeterId);
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_updateMeterEntry(ADAP_Uint32 idx, tEnetHal_QoSMeterEntry * pMeterEntry)
{
	if(idx>=MAX_NUMBER_OF_METERING)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_updateMeterEntry - Meter index is out of range idx:%d!\n", idx);
        return ENET_FAILURE;
	}
	adap_memcpy(&tAdapDatabase.MeterTable[idx].issMeter, pMeterEntry, sizeof(tEnetHal_QoSMeterEntry));
	return ENET_SUCCESS;
}

void enet_hw_init_done(void)
{
	tAdapDatabase.hw_init_done=ADAP_TRUE;
}
ADAP_Bool is_enet_init_done(void)
{
	return tAdapDatabase.hw_init_done;
}

tEnetHal_MacAddr *AdapGetProvStpMacAddress(void)
{
	return &adapVlanProviderStpAddr;
}

tEnetHal_MacAddr *AdapGetProvGvrpMacAddress(void)
{
	return &adapVlanProviderGvrpAddr;
}

tEnetHal_MacAddr *AdapGetProvGmrpMacAddress(void)
{
	return &adapVlanProviderGmrpAddr;
}
tEnetHal_MacAddr *AdapGetProvMvrpMacAddress(void)
{
	return &adapVlanProviderMvrpAddr;
}
tEnetHal_MacAddr *AdapGetProvMmrpMacAddress(void)
{
	return &adapVlanProviderMmrpAddr;
}
tEnetHal_MacAddr *AdapGetProvDot1xMacAddress(void)
{
	return &adapVlanProviderDot1xAddr;
}
tEnetHal_MacAddr *AdapGetProvLacpMacAddress(void)
{
	return &adapVlanProviderLacpAddr;
}
tEnetHal_MacAddr *AdapGetProvEmliMacAddress(void)
{
	return &adapVlanProviderElmiAddr;
}
tEnetHal_MacAddr *AdapGetProvLldpMacAddress(void)
{
	return &adapVlanProviderLldpAddr;
}
tEnetHal_MacAddr *AdapGetStpMacAddress(void)
{
	return &adapStpAddress;
}

tEnetHal_MacAddr *AdapGetMmrpMacAddress(void)
{
	return &adapMmrpAddress;
}
tEnetHal_MacAddr *AdapGetGmrpMacAddress(void)
{
	return &adapGmrpAddress;
}

tEnetHal_MacAddr *AdapGetGvrpMacAddress(void)
{
	return &adapGvrpAddress;
}

tEnetHal_MacAddr *AdapGetMvrpMacAddress(void)
{
	return &adapMvrpAddress;
}
tEnetHal_MacAddr *AdapGetBcastMacAddress(void)
{
	return &adapBcastAddress;
}
tEnetHal_MacAddr *AdapGetLldpMacAddress(void)
{
	return &adapLldpAddress;
}
tEnetHal_MacAddr *AdapGetOspfMacMulticastAllRoutes(void)
{
	return &adapOspfMacMulticastAllRoutes;
}
tEnetHal_MacAddr *AdapGetOspfMacMulticastAllDrs(void)
{
	return &adapOspfMacMulticastAllDrs;
}
tEnetHal_MacAddr *AdapGetRipMacMulticastAllRoutes(void)
{
	return &adapRipMacMulticastAllRoutes;
}

ADAP_Uint32 AdapAllocAclProfile(void)
{
    return AdapAllocAclProfileFilterIdFtype(0, FILTER_TYPE_NONE, 0, 0, 0);
}

static
ADAP_Uint32 InternalAllocProfileNum(ADAP_Uint32 inPort, eAclFilterTypes ftype, ADAP_Uint32 vlanId, ADAP_Uint32 priField)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
    {
        if ((tAdapDatabase.adapAclProfileNum[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.adapAclProfileNum[idx].ftype == ftype) &&
            (tAdapDatabase.adapAclProfileNum[idx].inPort == inPort) &&
            (tAdapDatabase.adapAclProfileNum[idx].vlanid == vlanId) &&
            (tAdapDatabase.adapAclProfileNum[idx].priField == priField))
        {
            printf("ACL Profile num is %d\n", tAdapDatabase.adapAclProfileNum[idx].acl_profile);
            tAdapDatabase.adapAclProfileNum[idx].refCount++;
            return tAdapDatabase.adapAclProfileNum[idx].acl_profile;
        }
    }

    for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
    {
        if (tAdapDatabase.adapAclProfileNum[idx].valid == ADAP_FALSE)
        {
            tAdapDatabase.adapAclProfileNum[idx].ftype = ftype;
            tAdapDatabase.adapAclProfileNum[idx].inPort = inPort;
            tAdapDatabase.adapAclProfileNum[idx].vlanid = vlanId;
            tAdapDatabase.adapAclProfileNum[idx].priField = priField;
            tAdapDatabase.adapAclProfileNum[idx].refCount = 1;
            tAdapDatabase.adapAclProfileNum[idx].valid = ADAP_TRUE;
            return tAdapDatabase.adapAclProfileNum[idx].acl_profile;
        }
    }

    return 0xFFFFFFF;
}

static
ADAP_Uint32 InternalFreeProfileNum(ADAP_Uint32 acl_profile)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
    {
        if ((tAdapDatabase.adapAclProfileNum[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.adapAclProfileNum[idx].acl_profile == acl_profile))
        {
            tAdapDatabase.adapAclProfileNum[idx].refCount--;
            if (tAdapDatabase.adapAclProfileNum[idx].refCount == 0)
                tAdapDatabase.adapAclProfileNum[idx].valid = ADAP_FALSE;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

ADAP_Uint32 AdapAllocAclProfileFilterIdFtype(ADAP_Uint32 filterId, eAclFilterTypes ftype,
                    ADAP_Uint32 inPort, ADAP_Uint32 vlanId, ADAP_Uint32 priField)
{
	ADAP_Uint32	prof_id=0;
	ADAP_Uint32	idx=0;
	for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
	{
		if(tAdapDatabase.adapAclProfile[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.adapAclProfile[idx].valid = ADAP_TRUE;
			tAdapDatabase.adapAclProfile[idx].filter_id = filterId;
			tAdapDatabase.adapAclProfile[idx].ftype = ftype;
			tAdapDatabase.adapAclProfile[idx].inPort = inPort;
			tAdapDatabase.adapAclProfile[idx].refCount=1;
			tAdapDatabase.adapAclProfile[idx].vlanId = vlanId;
			tAdapDatabase.adapAclProfile[idx].priField = priField;
			prof_id = InternalAllocProfileNum(inPort, ftype, vlanId, priField);
			if (prof_id != 0xFFFFFFFF)
			{
                tAdapDatabase.adapAclProfile[idx].acl_profile = prof_id;
            } else
            {
                printf("ERROR: can't get acl_profile\n");
                return 0xFFFFFFFF;
            }
			return tAdapDatabase.adapAclProfile[idx].acl_profile;
		}
	}
	return 0xFFFFFFFF;
}

ADAP_Uint32 IncRefCountAclProfile(ADAP_Uint32 acl_profile)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
	{
		if( (tAdapDatabase.adapAclProfile[idx].valid == ADAP_TRUE) && (tAdapDatabase.adapAclProfile[idx].acl_profile == acl_profile) )
		{
			tAdapDatabase.adapAclProfile[idx].refCount++;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 AdapGetAclProfileFtype(ADAP_Uint32 filterId, eAclFilterTypes ftype, ADAP_Uint32 InPort)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
    {
        if ((tAdapDatabase.adapAclProfile[idx].filter_id == filterId)
           && (tAdapDatabase.adapAclProfile[idx].valid == ADAP_TRUE)
           && (tAdapDatabase.adapAclProfile[idx].inPort == InPort)
           && (tAdapDatabase.adapAclProfile[idx].ftype == ftype))
        {
            return tAdapDatabase.adapAclProfile[idx].acl_profile;
        }
    }
    return 0xFFFFFFFF;
}

ADAP_Uint32 AdapGetAclProfileInPort(ADAP_Uint32 inPort, eAclFilterTypes ftype, ADAP_Uint32 vlanId, ADAP_Uint32 priField)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
    {
        if ((tAdapDatabase.adapAclProfile[idx].inPort == inPort)
           && (tAdapDatabase.adapAclProfile[idx].valid == ADAP_TRUE)
           && (tAdapDatabase.adapAclProfile[idx].ftype == ftype)
           && (tAdapDatabase.adapAclProfile[idx].vlanId == vlanId)
           && (tAdapDatabase.adapAclProfile[idx].priField == priField))
        {
            return tAdapDatabase.adapAclProfile[idx].acl_profile;
        }
    }
    return 0xFFFFFFFF;
}

tAdapAclProfile *AdapGetAclProfilePtr(ADAP_Uint32 acl_profile, ADAP_Uint32 filterId)
{
    ADAP_Uint32 idx=0;
    for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
    {
        if ((tAdapDatabase.adapAclProfile[idx].acl_profile == acl_profile)
           && (tAdapDatabase.adapAclProfile[idx].filter_id == filterId)
           && (tAdapDatabase.adapAclProfile[idx].valid == ADAP_TRUE))
        {
            return &tAdapDatabase.adapAclProfile[idx];
        }
    }
    return NULL;
}

ADAP_Uint32 AdapfreeAclProfile(ADAP_Uint32 acl_profile,ADAP_Uint32 filterNoId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_ACL_FILTER_NUM;idx++)
	{
		if( (tAdapDatabase.adapAclProfile[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapAclProfile[idx].filter_id == filterNoId) &&
			(tAdapDatabase.adapAclProfile[idx].acl_profile == acl_profile) )
		{
			tAdapDatabase.adapAclProfile[idx].refCount--;
			if(tAdapDatabase.adapAclProfile[idx].refCount == 0)
			{
				tAdapDatabase.adapAclProfile[idx].valid = ADAP_FALSE;
			}
			return InternalFreeProfileNum(acl_profile);
		}
	}

	return ENET_FAILURE;
}
ADAP_Uint32 AdapFreeAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if( (tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.aclHierarchy[idx].table_id == table_id) &&
				(tAdapDatabase.aclHierarchy[idx].serviceId == serviceId) )
		{
			tAdapDatabase.aclHierarchy[idx].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tAclHierarchy *AdapGetFirstAclHierarchy(void)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if(tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE)
		{
			return &tAdapDatabase.aclHierarchy[idx];
		}
	}
	return NULL;
}

void AdapDeleteAclHierarchyTableId(ADAP_Uint8 table_id)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if( (tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.aclHierarchy[idx].table_id == table_id) )
		{
			tAdapDatabase.aclHierarchy[idx].valid = ADAP_FALSE;
		}
	}
}

tAclHierarchy *AdapGetNextAclHierarchy(tAclHierarchy *AclHi)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 found=0;

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
		{
			if(found == 0)
			{
				if( (tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE) &&
						(tAdapDatabase.aclHierarchy[idx].table_id == AclHi->table_id) &&
						(tAdapDatabase.aclHierarchy[idx].serviceId == AclHi->serviceId) )
				{
					found = 1;
				}
			}
			else if(tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE)
			{
				return &tAdapDatabase.aclHierarchy[idx];
			}

		}
	}
	return NULL;
}

tAclHierarchy *AdapGetAclHierarchyByType(ADAP_Uint32 ifIndex,ADAP_Uint32 L2_protocol_type,ADAP_Uint16 vlanId)
{
	ADAP_Uint32 idx=0;
#if 0
	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if(tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE)
		{
			printf("AdapGetAclHierarchyByType ifIndex:%d\r\n",tAdapDatabase.aclHierarchy[idx].ifIndex);
			printf("AdapGetAclHierarchyByType L2_protocol_type:%d\r\n",tAdapDatabase.aclHierarchy[idx].L2_protocol_type);
			printf("AdapGetAclHierarchyByType outerVlan:%d\r\n",tAdapDatabase.aclHierarchy[idx].outerVlan);
		}
	}
#endif
	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if( (tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.aclHierarchy[idx].ifIndex == ifIndex) &&
				(tAdapDatabase.aclHierarchy[idx].L2_protocol_type == L2_protocol_type) &&
				(tAdapDatabase.aclHierarchy[idx].outerVlan == vlanId))
		{
			return &tAdapDatabase.aclHierarchy[idx];
		}
	}
	return NULL;
}

tAclHierarchy *AdapGetAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if( (tAdapDatabase.aclHierarchy[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.aclHierarchy[idx].table_id == table_id) &&
				(tAdapDatabase.aclHierarchy[idx].serviceId == serviceId) )
		{
			return &tAdapDatabase.aclHierarchy[idx];
		}
	}
	return NULL;
}

tAclHierarchy *AdapAllocateAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_ACL_PROF_NUM;idx++)
	{
		if(tAdapDatabase.aclHierarchy[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.aclHierarchy[idx].valid=ADAP_TRUE;
			tAdapDatabase.aclHierarchy[idx].table_id = table_id;
			tAdapDatabase.aclHierarchy[idx].serviceId = serviceId;
			return &tAdapDatabase.aclHierarchy[idx];
		}
	}
	return NULL;
}

tDbaseHwAggEntry *AdapGetAggrDbByHdLagId(ADAP_Uint16 hwlagId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if( (tAdapDatabase.aggrEntry[idx].valid==ADAP_TRUE) &&
			(tAdapDatabase.aggrEntry[idx].hwlagId == hwlagId) )
		{
			return &tAdapDatabase.aggrEntry[idx];
		}
	}
	return NULL;
}

tDbaseHwAggEntry *AdapGetAggrDbByVirtualPort(ADAP_Uint32 virtualPort)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if( (tAdapDatabase.aggrEntry[idx].valid==ADAP_TRUE) &&
			(tAdapDatabase.aggrEntry[idx].lagVirtualPort == virtualPort) )
		{
			return &tAdapDatabase.aggrEntry[idx];
		}
	}
	return NULL;
}



tDbaseHwAggEntry *AdapAllocateAggrDbByHdLagId(ADAP_Uint16 hwlagId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if( (tAdapDatabase.aggrEntry[idx].valid==ADAP_TRUE) &&
			(tAdapDatabase.aggrEntry[idx].hwlagId == hwlagId) )
		{
			return &tAdapDatabase.aggrEntry[idx];
		}
	}
	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if(tAdapDatabase.aggrEntry[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.aggrEntry[idx].valid=ADAP_TRUE;
			tAdapDatabase.aggrEntry[idx].hwlagId = hwlagId;
			return &tAdapDatabase.aggrEntry[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 adapGetBoardSpecific(void)
{
	return tAdapDatabase.globalBitmap;
}

void adapSetBoardSpecific(ADAP_Uint32 bitmap)
{
	tAdapDatabase.globalBitmap = bitmap;
}

ADAP_Uint32 adapDeleteCluster(MEA_Port_t enetPort,ENET_QueueId_t clusterId)
{
	ADAP_Uint32 index;
	ADAP_Uint32 index1;
	for(index=0;index<MAX_NUM_OF_SUPPORTED_PORTS;index++)
	{
		if(tAdapDatabase.adapCluserDb[index].enetPort == enetPort)
		{
			for(index1=0;index1<MAX_NUM_OF_CLUSTERS_PER_PORT;index1++)
			{
				if( (tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid == ADAP_TRUE) && 
				    (tAdapDatabase.adapCluserDb[index].clustrDb[index1].clusterId == clusterId) )
				{
					tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid = ADAP_FALSE;
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adapCreateCluster(MEA_Port_t enetPort,ENET_QueueId_t clusterId)
{
	ADAP_Uint32 index;
	ADAP_Uint32 index1;
	for(index=0;index<MAX_NUM_OF_SUPPORTED_PORTS;index++)
	{
		if(tAdapDatabase.adapCluserDb[index].enetPort == enetPort)
		{
			for(index1=0;index1<MAX_NUM_OF_CLUSTERS_PER_PORT;index1++)
			{
				if(tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid == ADAP_FALSE)
				{
					tAdapDatabase.adapCluserDb[index].clustrDb[index1].clusterId = clusterId;
					tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid = ADAP_TRUE;
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adapGetFirstCluster(MEA_Port_t enetPort ,ENET_QueueId_t *pClusterId)
{
	ADAP_Uint32 index;
	ADAP_Uint32 index1;
	for(index=0;index<MAX_NUM_OF_SUPPORTED_PORTS;index++)
	{
		if(tAdapDatabase.adapCluserDb[index].enetPort == enetPort)
		{
			for(index1=0;index1<MAX_NUM_OF_CLUSTERS_PER_PORT;index1++)
			{
				if(tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid == ADAP_TRUE)
				{
					(*pClusterId) = tAdapDatabase.adapCluserDb[index].clustrDb[index1].clusterId;
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adapGetNextCluster(MEA_Port_t enetPort ,ENET_QueueId_t prevClusterId, ENET_QueueId_t *pClusterId)
{
    ADAP_Uint32 index;
    ADAP_Uint32 index1;
    ADAP_Uint16 found = 0;
    for(index=0;index<MAX_NUM_OF_SUPPORTED_PORTS;index++)
    {
        if(tAdapDatabase.adapCluserDb[index].enetPort == enetPort)
        {
            for(index1=0;index1<MAX_NUM_OF_CLUSTERS_PER_PORT;index1++)
            {
                if(tAdapDatabase.adapCluserDb[index].clustrDb[index1].valid == ADAP_TRUE)
                {
                    if (tAdapDatabase.adapCluserDb[index].clustrDb[index1].clusterId == prevClusterId)
                    {
                        found = 1;
                        continue;
                    }

                    if (found == 1)
                    {
                        (*pClusterId) = tAdapDatabase.adapCluserDb[index].clustrDb[index1].clusterId;
                        return ENET_SUCCESS;
                    }
                }
            }
        }
    }
    return ENET_FAILURE;
}


ADAP_Uint32 adapFreeNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if( (tAdapDatabase.adapNaptRule[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapNaptRule[idx].sourceIpAddr == sourceIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].destIpAddr == destIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].sourcePort == sourcePort) &&
			(tAdapDatabase.adapNaptRule[idx].destPort == destPort) &&
			(tAdapDatabase.adapNaptRule[idx].protocol == protocol) )
		{
			tAdapDatabase.adapNaptRule[idx].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tNaptRule *adapGetNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if( (tAdapDatabase.adapNaptRule[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapNaptRule[idx].sourceIpAddr == sourceIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].destIpAddr == destIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].sourcePort == sourcePort) &&
			(tAdapDatabase.adapNaptRule[idx].destPort == destPort) &&
			(tAdapDatabase.adapNaptRule[idx].protocol == protocol) )
		{
			return &tAdapDatabase.adapNaptRule[idx];
		}
	}
	return NULL;
}
tNaptRule *adapAllocNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if( (tAdapDatabase.adapNaptRule[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapNaptRule[idx].sourceIpAddr == sourceIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].destIpAddr == destIpAddr) &&
			(tAdapDatabase.adapNaptRule[idx].sourcePort == sourcePort) &&
			(tAdapDatabase.adapNaptRule[idx].destPort == destPort) &&
			(tAdapDatabase.adapNaptRule[idx].protocol == protocol) &&
			(tAdapDatabase.adapNaptRule[idx].ifIndex == ifIndex) )
		{
			return &tAdapDatabase.adapNaptRule[idx];
		}
	}
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if(tAdapDatabase.adapNaptRule[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.adapNaptRule[idx].valid = ADAP_TRUE;
			tAdapDatabase.adapNaptRule[idx].sourceIpAddr = sourceIpAddr;
			tAdapDatabase.adapNaptRule[idx].destIpAddr = destIpAddr;
			tAdapDatabase.adapNaptRule[idx].sourcePort = sourcePort;
			tAdapDatabase.adapNaptRule[idx].destPort = destPort;
			tAdapDatabase.adapNaptRule[idx].protocol = protocol;
			tAdapDatabase.adapNaptRule[idx].ifIndex = ifIndex;
			tAdapDatabase.adapNaptRule[idx].AtionId = MEA_PLAT_GENERATE_NEW_ID;
			return &tAdapDatabase.adapNaptRule[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 AdapAddStaticNatEntry (tNaptAddressTable *pNatEntry)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if(tAdapDatabase.adapNaptAddressTable[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.adapNaptAddressTable[idx].valid = ADAP_TRUE;
			tAdapDatabase.adapNaptAddressTable[idx].localIpAddr = pNatEntry->localIpAddr;
			tAdapDatabase.adapNaptAddressTable[idx].globalIpAddr = pNatEntry->globalIpAddr;
			tAdapDatabase.adapNaptAddressTable[idx].localPort = pNatEntry->localPort;
			tAdapDatabase.adapNaptAddressTable[idx].globalPort = pNatEntry->globalPort;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tNaptAddressTable *
AdapGetNatEntryFromHosttoNetwork (ADAP_Uint32 localIpAddr, ADAP_Uint16 localPort)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if( (tAdapDatabase.adapNaptAddressTable[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapNaptAddressTable[idx].localIpAddr == localIpAddr))
		{
			return &tAdapDatabase.adapNaptAddressTable[idx];
		}
	}
	return NULL;
}

tNaptAddressTable *
AdapGetNatEntryFromNetworktoHost (ADAP_Uint32 globalIpAddr, ADAP_Uint16 globalPort)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NAT_ENTRIES_PER_TABLE;idx++)
	{
		if( (tAdapDatabase.adapNaptAddressTable[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapNaptAddressTable[idx].globalIpAddr == globalIpAddr))
		{
			return &tAdapDatabase.adapNaptAddressTable[idx];
		}
	}
	return NULL;
}


tLinuxIpInterface *adap_get_interface_by_index(ADAP_Int32 index)
{

	if(tAdapDatabase.adapLinuxInf[index].valid == ADAP_TRUE)
	{
		return &tAdapDatabase.adapLinuxInf[index];
	}
	return NULL;

}

ADAP_Uint32 adap_getIndex_interface_by_name(ADAP_Int8 *name)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if( (tAdapDatabase.adapLinuxInf[idx].valid == ADAP_TRUE) &&
				strncmp(tAdapDatabase.adapLinuxInf[idx].interface_name,name,MAX_SIZEOFOF_INTERFACE_NAME) == 0)
		{
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

tLinuxIpInterface *adap_get_interface_by_name(ADAP_Int8 *name)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if( (tAdapDatabase.adapLinuxInf[idx].valid == ADAP_TRUE) &&
				strncmp(tAdapDatabase.adapLinuxInf[idx].interface_name,name,MAX_SIZEOFOF_INTERFACE_NAME) == 0)
		{
			return &tAdapDatabase.adapLinuxInf[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 adap_set_interfaceip(ADAP_Int8 *name,ADAP_Int8  	*host_ip)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		if( (tAdapDatabase.adapLinuxInf[idx].valid == ADAP_TRUE) &&
				strncmp(tAdapDatabase.adapLinuxInf[idx].interface_name,name,MAX_SIZEOFOF_INTERFACE_NAME) == 0)
		{
			strncpy(tAdapDatabase.adapLinuxInf[idx].host_ip,host_ip,MAX_SIZEOFOF_INTERFACE_IP);
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tLinuxArpCache *adap_getfirst_newArpEntry(MEA_Port_t portId)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NUM_OF_ARP_CACHE;idx++)
	{
		if( (tAdapDatabase.linuxArpCache[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.linuxArpCache[idx].newEntry == ADAP_TRUE) &&
			(tAdapDatabase.linuxArpCache[idx].portId == portId) )
			{
				return &tAdapDatabase.linuxArpCache[idx];
			}
	}
	return NULL;
}

tLinuxArpCache *adap_get_ArpEntry(ADAP_Uint32 index)
{
	if(index >= MAX_NUM_OF_ARP_CACHE)
	{
		return NULL;
	}
	return &tAdapDatabase.linuxArpCache[index];
}

ADAP_Uint32 adap_getfirst_ArpEntry(ADAP_Uint32 *index)
{
	ADAP_Uint32 idx;
	for(idx=0;idx<MAX_NUM_OF_ARP_CACHE;idx++)
	{
		if(tAdapDatabase.linuxArpCache[idx].valid == ADAP_TRUE)
		{
			(*index) = idx;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_getNext_ArpEntry(ADAP_Uint32 *index)
{
	ADAP_Uint32 idx=(*index)+1;

	if(idx >= MAX_NUM_OF_ARP_CACHE)
	{
		return ENET_FAILURE;
	}

	for(;idx<MAX_NUM_OF_ARP_CACHE;idx++)
	{
		if(tAdapDatabase.linuxArpCache[idx].valid == ADAP_TRUE)
		{
			(*index) = idx;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}


ADAP_Uint32 adap_create_arpEntry(tLinuxArpCache *arpCache)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUM_OF_ARP_CACHE;idx++)
	{
		if( (tAdapDatabase.linuxArpCache[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.linuxArpCache[idx].ip_address == arpCache->ip_address) &&
			(adap_memcmp(&tAdapDatabase.linuxArpCache[idx].macAddress,&arpCache->macAddress,sizeof(tEnetHal_MacAddr)) == 0) )
			{
				return ENET_SUCCESS;
			}
	}
	for(idx=0;idx<MAX_NUM_OF_ARP_CACHE;idx++)
	{
		if(tAdapDatabase.linuxArpCache[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.linuxArpCache[idx].ip_address = arpCache->ip_address;
			adap_memcpy(&tAdapDatabase.linuxArpCache[idx].macAddress,&arpCache->macAddress,sizeof(tEnetHal_MacAddr));
			tAdapDatabase.linuxArpCache[idx].portId=arpCache->portId;
			tAdapDatabase.linuxArpCache[idx].valid =ADAP_TRUE;
			tAdapDatabase.linuxArpCache[idx].newEntry=ADAP_TRUE;
			printf("add new entry ip address:0x%x\r\n",arpCache->ip_address);
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tTunnelService *adap_getnext_tunnelServiceByPortId(tTunnelService *currService)
{
	ADAP_Uint32 idx;
	ADAP_Bool found=ADAP_FALSE;

	for(idx=0;idx<MAX_NUMBER_OF_TUNNEL_SESSION;idx++)
	{
		if(found==ADAP_FALSE)
		{
			if( (tAdapDatabase.tunnelService[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.tunnelService[idx].Service_id == currService->Service_id) )
				{
					found = ADAP_TRUE;
				}
		}
		else
		{
			if( (tAdapDatabase.tunnelService[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.tunnelService[idx].Service_id == currService->Service_id) )
				{
					return &tAdapDatabase.tunnelService[idx];
				}
		}
	}
	return NULL;
}

ADAP_Uint32 adap_del_AceVirtualFunc(unsigned char 	*AceName)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if( (tAdapDatabase.aceVirtualDb[idx].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.aceVirtualDb[idx].tVirtualFunc.AceName,(char *)AceName,MAX_ACE_NAME_LEN) == 0) )
			{
				tAdapDatabase.aceVirtualDb[idx].valid = ADAP_FALSE;
				return ENET_SUCCESS;
			}
	}
	return ENET_FAILURE;
}

tAceVirtualFunc *adap_get_AceVirtualFunc(unsigned char 	*AceName)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if( (tAdapDatabase.aceVirtualDb[idx].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.aceVirtualDb[idx].tVirtualFunc.AceName,(char *)AceName,MAX_ACE_NAME_LEN) == 0) )
			{
				return &tAdapDatabase.aceVirtualDb[idx];
			}
	}
	return NULL;
}

ADAP_Uint32 adap_set_AceVirtualFunc(tComAceVirtualFunc *aceFunc)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if(tAdapDatabase.aceVirtualDb[idx].valid == ADAP_FALSE)
		{
			adap_memcpy(&tAdapDatabase.aceVirtualDb[idx].tVirtualFunc,aceFunc,sizeof(tAceVirtualFunc));
			tAdapDatabase.aceVirtualDb[idx].valid = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_del_AceAdminCtrl(unsigned int 	accesslist_priorityid)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if( (tAdapDatabase.aceCtrlAdmin[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.aceCtrlAdmin[idx].tCtrlAdmin.accesslist_priorityid == accesslist_priorityid) )
			{
				tAdapDatabase.aceCtrlAdmin[idx].valid = ADAP_FALSE;
				return ENET_SUCCESS;
			}
	}
	return ENET_FAILURE;
}
tAceCtrlAdmin *adap_get_AceAdminCtrl(unsigned int 	accesslist_priorityid)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if( (tAdapDatabase.aceCtrlAdmin[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.aceCtrlAdmin[idx].tCtrlAdmin.accesslist_priorityid == accesslist_priorityid) )
			{
				return &tAdapDatabase.aceCtrlAdmin[idx];
			}
	}
	return NULL;
}

ADAP_Uint32 adap_set_AceAdminCtrl(tComAceVirtualCtrlAdmin *aceFunc)
{
	unsigned int idx=0;
	for(idx=0;idx<MAX_NUM_OF_ACE_VIRTUAL_FUNC;idx++)
	{
		if(tAdapDatabase.aceCtrlAdmin[idx].valid == ADAP_FALSE)
		{
			adap_memcpy(&tAdapDatabase.aceCtrlAdmin[idx].tCtrlAdmin,aceFunc,sizeof(tComAceVirtualCtrlAdmin));
			tAdapDatabase.aceCtrlAdmin[idx].valid = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tTunnelService *adap_getfirst_tunnelServiceByPortId(MEA_Port_t  portId)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUMBER_OF_TUNNEL_SESSION;idx++)
	{
		if( (tAdapDatabase.tunnelService[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tunnelService[idx].portId == portId) )
			{
				return &tAdapDatabase.tunnelService[idx];
			}
	}
	return NULL;
}

ADAP_Uint32 adap_create_tunnelService(tTunnelService *tunnel)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<MAX_NUMBER_OF_TUNNEL_SESSION;idx++)
	{
		if( (tAdapDatabase.tunnelService[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tunnelService[idx].tunnel_id == tunnel->tunnel_id) &&
			(tAdapDatabase.tunnelService[idx].portId == tunnel->portId) &&
			(tAdapDatabase.tunnelService[idx].vlanId == tunnel->vlanId) &&
			(tAdapDatabase.tunnelService[idx].Service_id == tunnel->Service_id) )
			{
				return ENET_SUCCESS;
			}
	}
	for(idx=0;idx<MAX_NUMBER_OF_TUNNEL_SESSION;idx++)
	{
		if(tAdapDatabase.tunnelService[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.tunnelService[idx].tunnel_id = tunnel->tunnel_id;
			tAdapDatabase.tunnelService[idx].portId = tunnel->portId;
			tAdapDatabase.tunnelService[idx].vlanId = tunnel->vlanId;
			tAdapDatabase.tunnelService[idx].Service_id = tunnel->Service_id;
			tAdapDatabase.tunnelService[idx].valid = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

void process_killed(void)
{
	process_is_killed=1;
}

int is_process_killed(void)
{
	return process_is_killed;
}

tBridgeIntName *get_bridgeportByPortNumber(ADAP_Uint16 port_number,ADAP_Uint8 *bridge_name)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_PORTS_BRIDGES;i++)
	{
		if( (tAdapDatabase.openVswitchDb.bridgePortDb[i].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.bridge_name,(char *)bridge_name,MAX_STRING_SIZE) == 0) &&
			(tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.port_number == port_number))
		{
			return &tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort;
		}
	}
	return NULL;
}

ADAP_Uint32 delete_bridgeport(tBridgeIntName *pBridgePort)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_PORTS_BRIDGES;i++)
	{
		if( (tAdapDatabase.openVswitchDb.bridgePortDb[i].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.bridge_name,(char *)pBridgePort->bridge_name,MAX_STRING_SIZE) == 0) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.interface_name,(char *)pBridgePort->interface_name,MAX_STRING_SIZE) == 0) &&
			(tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.port_number == pBridgePort->port_number))
		{
			tAdapDatabase.openVswitchDb.bridgePortDb[i].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;

}

tEnetVtepSerDb *ovsdb_alloc_entry_by_vni(void)
{
	ADAP_Uint32 i;
	for(i=0;i<NUMBER_OF_VTEP_VNI;i++)
	{
		if(tAdapDatabase.openVswitchDb.tVtepService[i].valid == ADAP_FALSE)
		{
			tAdapDatabase.openVswitchDb.tVtepService[i].valid=0;
			tAdapDatabase.openVswitchDb.tVtepService[i].refCount=0;
			return &tAdapDatabase.openVswitchDb.tVtepService[i];
		}
	}
	return NULL;
}

ADAP_Uint32 ovsdb_free_entry_by_vni(unsigned int vni)
{
	ADAP_Uint32 i;
	for(i=0;i<NUMBER_OF_VTEP_VNI;i++)
	{
		if( (tAdapDatabase.openVswitchDb.tVtepService[i].valid == ADAP_TRUE) &&
			(tAdapDatabase.openVswitchDb.tVtepService[i].vniId == vni) )
		{
			tAdapDatabase.openVswitchDb.tVtepService[i].refCount--;
			if(tAdapDatabase.openVswitchDb.tVtepService[i].refCount == 0)
			{
				tAdapDatabase.openVswitchDb.tVtepService[i].valid=ADAP_FALSE;
			}
		}
	}
	return ENET_SUCCESS;
}

tEnetVtepSerDb *ovsdb_get_entry_by_vni(unsigned int vni)
{
	ADAP_Uint32 i;
	for(i=0;i<NUMBER_OF_VTEP_VNI;i++)
	{
		if( (tAdapDatabase.openVswitchDb.tVtepService[i].valid == ADAP_TRUE) &&
			(tAdapDatabase.openVswitchDb.tVtepService[i].vniId == vni) )
		{
			return &tAdapDatabase.openVswitchDb.tVtepService[i];
		}
	}
	return NULL;
}

ADAP_Uint32 is_ovsdb_remote_mac_entry(tEnetOvsdbTunnel *vtep)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if( (tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 1) &&
				(tAdapDatabase.openVswitchDb.ovsdbVtep[i].vniId == vtep->vniId) &&
				(adap_memcmp(&tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_mac_dest,&vtep->vtep_mac_dest,sizeof(tEnetHal_MacAddr)) == 0) &&
				(adap_memcmp((char *)tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_ipv_dest,(char *)vtep->vtep_ipv_dest,4) == 0) )
		{
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tEnetOvsdbTunnel *ovsdb_getfirst_entry(void)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 1)
		{
			return &tAdapDatabase.openVswitchDb.ovsdbVtep[i];
		}
	}
	return NULL;
}

tEnetOvsdbTunnel *ovsdb_getnext_entry(tEnetOvsdbTunnel	*vtep)
{
	ADAP_Uint32 i;
	ADAP_Bool found=ADAP_FALSE;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(found==ADAP_FALSE)
		{
			if( (tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 1) &&
					(adap_memcmp(&tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_mac_dest,&vtep->vtep_mac_dest,sizeof(tEnetHal_MacAddr)) == 0) &&
					(adap_memcmp((char *)tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_ipv_dest,(char *)vtep->vtep_ipv_dest,4) == 0) )
			{
				found = ADAP_TRUE;
			}
		}
		else
		{
			if(tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 1)
			{
				return &tAdapDatabase.openVswitchDb.ovsdbVtep[i];
			}
		}
	}
	return NULL;
}

tEnetVtepGlobal *ovsdb_get_global_entry(void)
{
	return &tAdapDatabase.openVswitchDb.ovsdbGlobl;
}

ADAP_Uint32  ovsdb_sync_remote_mac_entry(tEnetOvsDbSync *pVtepSync)
{
	ADAP_Uint32 i=0;
	for(i=0;i<pVtepSync->num_of_entries;i++)
	{
		ovsdb_set_entry(&pVtepSync->tTunnel[i]);
	}
	return ENET_SUCCESS;
}
ADAP_Uint32  ovsdb_sync_local_mac_entry(tEnetOvsDbSync *pVtepSync)
{
	adap_memcpy(&tAdapDatabase.openVswitchDb.ovsdbLocalVtep,&pVtepSync->tTunnel[0],sizeof(tEnetOvsdbTunnel));
	return ENET_SUCCESS;
}

tEnetOvsdbTunnel  *ovsdb_get_local_mac_entry(void)
{
	return &tAdapDatabase.openVswitchDb.ovsdbLocalVtep;
}


ADAP_Uint32 ovsdb_set_global_entry(tEnetVtepGlobal	*pEntry)
{
	ADAP_Uint32 i;

	tAdapDatabase.openVswitchDb.ovsdbGlobl.num_of_entries = pEntry->num_of_entries;

	for(i=0;i<pEntry->num_of_entries;i++)
	{
		tAdapDatabase.openVswitchDb.ovsdbGlobl.vniId[i] = pEntry->vniId[i];
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 ovsdb_set_entry(tEnetOvsdbTunnel	*vtep)
{
	ADAP_Uint32 i;

	// first bytes cannot be zero
	if(vtep->vtep_ipv_dest[0] == 0)
	{
		return ENET_SUCCESS;
	}


	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if( (tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 1) &&
			(adap_memcmp(&tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_mac_dest,&vtep->vtep_mac_dest,sizeof(tEnetHal_MacAddr)) == 0) &&
			(adap_memcmp((char *)tAdapDatabase.openVswitchDb.ovsdbVtep[i].vtep_ipv_dest,(char *)vtep->vtep_ipv_dest,4) == 0) )
		{
			return ENET_SUCCESS;
		}
	}
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(tAdapDatabase.openVswitchDb.ovsdbVtep[i].action == 0)
		{
			adap_memcpy(&tAdapDatabase.openVswitchDb.ovsdbVtep[i],vtep,sizeof(tEnetOvsdbTunnel));
			tAdapDatabase.openVswitchDb.ovsdbVtep[i].action = 1;
			printf("num of entries:%d\r\n",vtep->num_of_entries);
			printf("vniId:%d\r\n",vtep->vniId[0]);
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 create_bridgeport(tBridgeIntName *pBridgePort)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_PORTS_BRIDGES;i++)
	{
		if( (tAdapDatabase.openVswitchDb.bridgePortDb[i].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.bridge_name,(char *)pBridgePort->bridge_name,MAX_STRING_SIZE) == 0) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.interface_name,(char *)pBridgePort->interface_name,MAX_STRING_SIZE) == 0) &&
			(tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort.port_number == pBridgePort->port_number))
		{
			return ENET_SUCCESS;
		}
	}
	for(i=0;i<MAX_NUM_OF_PORTS_BRIDGES;i++)
	{
		if(tAdapDatabase.openVswitchDb.bridgePortDb[i].valid == ADAP_FALSE)
		{
			adap_memcpy(&tAdapDatabase.openVswitchDb.bridgePortDb[i].bridgePort,pBridgePort,sizeof(tBridgeIntName));
			tAdapDatabase.openVswitchDb.bridgePortDb[i].valid = ADAP_TRUE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;

}

tEnetOfVxlanTunnel *get_vxLanTunnel_by_nameAndPort(ADAP_Uint8 *interface_name)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if( (tAdapDatabase.openVswitchDb.vxlanDb[i].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.vxlanDb[i].tunnel.vxlan_name,(char *)interface_name,MAX_STRING_SIZE) == 0) )
		{
			return &tAdapDatabase.openVswitchDb.vxlanDb[i].tunnel;
		}
	}
	return NULL;
}

ADAP_Uint32 getfirst_vxLanTunnel(void)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(tAdapDatabase.openVswitchDb.vxlanDb[i].valid == ADAP_TRUE)
		{
			return i;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 getnext_vxLanTunnel(ADAP_Uint32 i)
{
	i++;
	if(i >= MAX_NUM_OF_VXLAN_TUNNELS)
	{
		return ADAP_END_OF_TABLE;
	}
	for(;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(tAdapDatabase.openVswitchDb.vxlanDb[i].valid == ADAP_TRUE)
		{
			return i;
		}
	}
	return ADAP_END_OF_TABLE;
}

tEnetOfVxlanTunnel *get_vxLanTunnel(ADAP_Uint32 i)
{
	return &tAdapDatabase.openVswitchDb.vxlanDb[i].tunnel;
}

ADAP_Uint32 create_vxLanTunnel(tEnetOfVxlanTunnel *pBTunnel)
{
	ADAP_Uint32 i;
	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if( (tAdapDatabase.openVswitchDb.vxlanDb[i].valid == ADAP_TRUE) &&
			(strncmp((char *)tAdapDatabase.openVswitchDb.vxlanDb[i].tunnel.vxlan_name,(char *)pBTunnel->vxlan_name,MAX_STRING_SIZE) == 0) )
		{
			return ENET_SUCCESS;
		}
	}

	for(i=0;i<MAX_NUM_OF_VXLAN_TUNNELS;i++)
	{
		if(tAdapDatabase.openVswitchDb.vxlanDb[i].valid == ADAP_FALSE)
		{
			tAdapDatabase.openVswitchDb.vxlanDb[i].valid = ADAP_TRUE;
			adap_memcpy(&tAdapDatabase.openVswitchDb.vxlanDb[i].tunnel,pBTunnel,sizeof(tEnetOfVxlanTunnel));
			printf("source ipv6:%s\r\n", adap_ipv6_to_string(&pBTunnel->ipv6_src));
			printf("dest ipv6:%s\r\n", adap_ipv6_to_string(&pBTunnel->ipv6_dst));
			printf("odp_port:%d\r\n",pBTunnel->odp_port);
			printf("pkt_mark:%d\r\n",pBTunnel->pkt_mark);
			printf("in_key_flow:%d\r\n",pBTunnel->in_key_flow);
			printf("ip_src_flow:%d\r\n",pBTunnel->ip_src_flow);
			printf("ip_dst_flow:%d\r\n",pBTunnel->ip_dst_flow);
			printf("action:%d\r\n",pBTunnel->action);
			printf("vxlan_name:%s\r\n",pBTunnel->vxlan_name);
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}
ADAP_Uint32 adap_set_vxlan_snooping(tAdapVxlanSnoop *pnewEntry)
{
	ADAP_Uint32 i=0;
	for(i=0;i<MAX_NUMBER_OF_VXLAN_TUNNEL;i++)
	{
		if(	(tAdapDatabase.adapVxlanSnoop[i].valid == ADAP_TRUE) &&
			(tAdapDatabase.adapVxlanSnoop[i].src_ipv4_int == pnewEntry->src_ipv4_int) )
		{
			// later on need to check if external ip is different then VM moved
			return ENET_SUCCESS;
		}
	}
	for(i=0;i<MAX_NUMBER_OF_VXLAN_TUNNEL;i++)
	{
		if(tAdapDatabase.adapVxlanSnoop[i].valid == ADAP_FALSE)
		{
			adap_memcpy(&tAdapDatabase.adapVxlanSnoop[i],pnewEntry,sizeof(tAdapVxlanSnoop));
			tAdapDatabase.adapVxlanSnoop[i].valid = ADAP_TRUE;
			tAdapDatabase.adapVxlanSnoop[i].isEntrySetToHw = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tAdapVxlanSnoop *adap_get_vxlan_snooping_by_index(ADAP_Uint32 index)
{
	if(index >= MAX_NUMBER_OF_VXLAN_TUNNEL)
	{
		return NULL;
	}
	return &tAdapDatabase.adapVxlanSnoop[index];
}

ADAP_Uint32 adap_getfirstIndex_vxlan_snooping(void)
{
	ADAP_Uint32 i=0;
	for(i=0;i<MAX_NUMBER_OF_VXLAN_TUNNEL;i++)
	{
		if(tAdapDatabase.adapVxlanSnoop[i].valid == ADAP_TRUE)
		{
			return i;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_getNextIndex_vxlan_snooping(ADAP_Uint32 index)
{
	ADAP_Uint32 i=index+1;

	if(i >= MAX_NUMBER_OF_VXLAN_TUNNEL)
	{
		return ADAP_END_OF_TABLE;
	}
	for(;i<MAX_NUMBER_OF_VXLAN_TUNNEL;i++)
	{
		if(tAdapDatabase.adapVxlanSnoop[i].valid == ADAP_TRUE)
		{
			return i;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_is_vxlan_flooding_exist(tVxLanFlooding *pVxlan)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<D_MAX_VXLAN_FLOODING;idx++)
	{
		if( (tAdapDatabase.vxlanFloodArr[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.vxlanFloodArr[idx].tunnel_id == pVxlan->tunnel_id) &&
			(tAdapDatabase.vxlanFloodArr[idx].destIp == pVxlan->destIp) &&
			(adap_memcmp(&tAdapDatabase.vxlanFloodArr[idx].destMac,&pVxlan->destMac,sizeof(tEnetHal_MacAddr)) == 0) )
		{
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

tVxLanFlooding *adap_create_vxlan_flooding_exist(void)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<D_MAX_VXLAN_FLOODING;idx++)
	{
		if(tAdapDatabase.vxlanFloodArr[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.vxlanFloodArr[idx].valid=ADAP_TRUE;
			return &tAdapDatabase.vxlanFloodArr[idx];
		}
	}
	return NULL;
}


#ifdef	MEA_NAT_DEMO
/* this part is only for NAT demo */
tIcmpTable	*allocate_IcmpTable(tIcmpTable *pTable)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUMBER_ICMP_ECHO_SESSIONS;idx++)
	{
		if( (tAdapDatabase.icmpTableDb[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.icmpTableDb[idx].destIpAddr == pTable->destIpAddr) &&
				(tAdapDatabase.icmpTableDb[idx].sourceIpAddr == pTable->sourceIpAddr) &&
				(tAdapDatabase.icmpTableDb[idx].origEchoId == pTable->origEchoId) )
		{
			return &tAdapDatabase.icmpTableDb[idx];
		}
	}
	for(idx=0;idx<MAX_NUMBER_ICMP_ECHO_SESSIONS;idx++)
	{
		if(tAdapDatabase.icmpTableDb[idx].valid == ADAP_FALSE)
		{
			tAdapDatabase.icmpTableDb[idx].valid = ADAP_TRUE;
			tAdapDatabase.icmpTableDb[idx].destIpAddr = pTable->destIpAddr;
			tAdapDatabase.icmpTableDb[idx].sourceIpAddr = pTable->sourceIpAddr;
			tAdapDatabase.icmpTableDb[idx].origEchoId = pTable->origEchoId;
			return &tAdapDatabase.icmpTableDb[idx];
		}
	}
	return NULL;
}
tIcmpTable	*getIcmpTableFromEchoReply(ADAP_Uint32 sourceIp,ADAP_Uint16 EchoId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUMBER_ICMP_ECHO_SESSIONS;idx++)
	{
		if( (tAdapDatabase.icmpTableDb[idx].valid == ADAP_TRUE) &&
				(tAdapDatabase.icmpTableDb[idx].destIpAddr == sourceIp) &&
				(tAdapDatabase.icmpTableDb[idx].newEchoId == EchoId) )
		{
			return &tAdapDatabase.icmpTableDb[idx];
		}
	}
	return NULL;
}
tTcpUdpTable	*allocate_tcpUdpTable(tTcpUdpTable *pTable)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if( (tAdapDatabase.tcpUdpTable[idx].valid==ADAP_TRUE) &&
			(tAdapDatabase.tcpUdpTable[idx].destIpAddr==pTable->destIpAddr) &&
			(tAdapDatabase.tcpUdpTable[idx].sourceIpAddr==pTable->sourceIpAddr) &&
			(tAdapDatabase.tcpUdpTable[idx].sourcePort==pTable->sourcePort) &&
			(tAdapDatabase.tcpUdpTable[idx].destPort==pTable->destPort) &&
			(tAdapDatabase.tcpUdpTable[idx].protocol==pTable->protocol) )
		{
			return &tAdapDatabase.tcpUdpTable[idx];
		}
	}
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if(tAdapDatabase.tcpUdpTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.tcpUdpTable[idx].valid=ADAP_TRUE;
			tAdapDatabase.tcpUdpTable[idx].destIpAddr=pTable->destIpAddr;
			tAdapDatabase.tcpUdpTable[idx].sourceIpAddr=pTable->sourceIpAddr;
			tAdapDatabase.tcpUdpTable[idx].sourcePort=pTable->sourcePort;
			tAdapDatabase.tcpUdpTable[idx].destPort=pTable->destPort;
			tAdapDatabase.tcpUdpTable[idx].protocol=pTable->protocol;
			return &tAdapDatabase.tcpUdpTable[idx];
		}
	}
	return NULL;
}

tTcpUdpTable	*getTcpUdpTable(tTcpUdpTable *pTable)
{
	ADAP_Uint32 idx=0;

	// only for debug
#if 0
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if(tAdapDatabase.tcpUdpTable[idx].valid == ADAP_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"curr table destIp:%x destPort:%d newSrcPort:%d proto:%d\r\n",
					tAdapDatabase.tcpUdpTable[idx].destIpAddr,
					tAdapDatabase.tcpUdpTable[idx].destPort,
					tAdapDatabase.tcpUdpTable[idx].newSourcePort,
					tAdapDatabase.tcpUdpTable[idx].protocol);
		}
	}
#endif
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if( (tAdapDatabase.tcpUdpTable[idx].valid == ADAP_TRUE) &&
			(tAdapDatabase.tcpUdpTable[idx].destIpAddr == pTable->sourceIpAddr) &&
			(tAdapDatabase.tcpUdpTable[idx].destPort == pTable->sourcePort) &&
			(tAdapDatabase.tcpUdpTable[idx].newSourcePort == pTable->destPort) &&
			(tAdapDatabase.tcpUdpTable[idx].protocol == pTable->protocol) )
		{
			return &tAdapDatabase.tcpUdpTable[idx];
		}
	}
	return NULL;
}

tTcpUdpTable  *getFirstTcpUdpTable(void)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if(tAdapDatabase.tcpUdpTable[idx].valid == ADAP_TRUE)
		{
			return &tAdapDatabase.tcpUdpTable[idx];
		}
	}
	return NULL;
}

tTcpUdpTable  *getNextTcpUdpTable(tTcpUdpTable *pTable)
{
	ADAP_Uint32 idx=0;
	ADAP_Bool found=ADAP_FALSE;

	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		if(found == ADAP_TRUE )
		{
			if(tAdapDatabase.tcpUdpTable[idx].valid==ADAP_TRUE)
			{
				return &tAdapDatabase.tcpUdpTable[idx];
			}
		}
		else if( (tAdapDatabase.tcpUdpTable[idx].valid==ADAP_TRUE) &&
			(tAdapDatabase.tcpUdpTable[idx].destIpAddr==pTable->destIpAddr) &&
			(tAdapDatabase.tcpUdpTable[idx].sourceIpAddr==pTable->sourceIpAddr) &&
			(tAdapDatabase.tcpUdpTable[idx].sourcePort==pTable->sourcePort) &&
			(tAdapDatabase.tcpUdpTable[idx].destPort==pTable->destPort) &&
			(tAdapDatabase.tcpUdpTable[idx].protocol==pTable->protocol) )
		{
			found = ADAP_TRUE;
		}
	}
	return NULL;
}

void refreshTcpUdpTable(void)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{
		Adap_natDomain_semLock();
		if(tAdapDatabase.tcpUdpTable[idx].valid == ADAP_TRUE)
		{
			if(tAdapDatabase.tcpUdpTable[idx].flag_delete > 5)
			{
				// delete the entry
				if(tAdapDatabase.tcpUdpTable[idx].HostToNetAtionId != MEA_PLAT_GENERATE_NEW_ID)
				{
					if(MeaDrvHwDelNatForwarder(tAdapDatabase.tcpUdpTable[idx].sourceIpAddr,
											tAdapDatabase.tcpUdpTable[idx].sourcePort,
											VPN_HOST_TO_NETWORK,
											MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) == MEA_OK)
					{

						ENET_ADAPTOR_Delete_Action(MEA_UNIT_0,tAdapDatabase.tcpUdpTable[idx].HostToNetAtionId);
						tAdapDatabase.tcpUdpTable[idx].HostToNetAtionId = MEA_PLAT_GENERATE_NEW_ID;
						tAdapDatabase.tcpUdpTable[idx].HostToNetpmId=0;
					}
				}
				if(tAdapDatabase.tcpUdpTable[idx].NetToHostAtionId != MEA_PLAT_GENERATE_NEW_ID)
				{
					if(MeaDrvHwDelNatForwarder(tAdapDatabase.tcpUdpTable[idx].myIpAddr,
											tAdapDatabase.tcpUdpTable[idx].newSourcePort,
											START_L3_VPN,
											MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN) == MEA_OK)
					{

						ENET_ADAPTOR_Delete_Action(MEA_UNIT_0,tAdapDatabase.tcpUdpTable[idx].NetToHostAtionId);
						tAdapDatabase.tcpUdpTable[idx].NetToHostAtionId = MEA_PLAT_GENERATE_NEW_ID;
						tAdapDatabase.tcpUdpTable[idx].NetToHostpmId=0;
					}
				}
				if( (tAdapDatabase.tcpUdpTable[idx].HostToNetAtionId == MEA_PLAT_GENERATE_NEW_ID) &&
					(tAdapDatabase.tcpUdpTable[idx].NetToHostAtionId == MEA_PLAT_GENERATE_NEW_ID) )
				{
					tAdapDatabase.tcpUdpTable[idx].valid=ADAP_FALSE;
					printf("deleting idx:%d port:%d\r\n",idx,tAdapDatabase.tcpUdpTable[idx].newSourcePort);
					tAdapDatabase.tcpUdpTable[idx].handle_by_cpu = ADAP_TRUE;
					Adap_natDomain_semUnlock();
					break;

				}
			}
		}
		Adap_natDomain_semUnlock();
	}
}

void printTcpUdpTable(void)
{
	ADAP_Uint32 idx=0;
	char buffer[200];
	ADAP_Uint32 index=0;
	static ADAP_Bool toPrint = ADAP_FALSE;

	if(toPrint == ADAP_TRUE)
	{
		printf("\r\n");
		printf("userSrcIp      |userSrcPort    |netDestIp      |netDestPort    |newSrcPort     |hostToNetCount |netToHostCount |\r\n");
		printf("===============|===============|===============|===============|===============|===============|===============|\r\n");
	}
	for(idx=0;idx<MAX_NUMBER_OF_L4_CONNECTION;idx++)
	{

		if( (tAdapDatabase.tcpUdpTable[idx].valid == ADAP_TRUE) &&
				( (tAdapDatabase.tcpUdpTable[idx].destPort == 443) || (tAdapDatabase.tcpUdpTable[idx].destPort == 80)) )
		{
			toPrint = ADAP_TRUE;
			adap_memset(buffer,0,sizeof(buffer));
			index=0;
			index += sprintf(&buffer[index],"%3.3d.%3.3d.%3.3d.%3.3d|",
					( (tAdapDatabase.tcpUdpTable[idx].sourceIpAddr & 0xFF000000) >> 24),
					( (tAdapDatabase.tcpUdpTable[idx].sourceIpAddr & 0x00FF0000) >> 16),
					( (tAdapDatabase.tcpUdpTable[idx].sourceIpAddr & 0x0000FF00) >> 8),
					( (tAdapDatabase.tcpUdpTable[idx].sourceIpAddr & 0x000000FF) ) );

			index += sprintf(&buffer[index],"%5.5d          |",tAdapDatabase.tcpUdpTable[idx].sourcePort);


			index += sprintf(&buffer[index],"%3.3d.%3.3d.%3.3d.%3.3d|",
					( (tAdapDatabase.tcpUdpTable[idx].destIpAddr & 0xFF000000) >> 24),
					( (tAdapDatabase.tcpUdpTable[idx].destIpAddr & 0x00FF0000) >> 16),
					( (tAdapDatabase.tcpUdpTable[idx].destIpAddr & 0x0000FF00) >> 8),
					( (tAdapDatabase.tcpUdpTable[idx].destIpAddr & 0x000000FF) ) );

			index += sprintf(&buffer[index],"%5.5d          |",tAdapDatabase.tcpUdpTable[idx].destPort);

			index += sprintf(&buffer[index],"%5.5d          |",tAdapDatabase.tcpUdpTable[idx].newSourcePort);

			index += sprintf(&buffer[index],"%10.10d     |",tAdapDatabase.tcpUdpTable[idx].lastCountHostToNet);

			index += sprintf(&buffer[index],"%10.10d     |",tAdapDatabase.tcpUdpTable[idx].lastCountNetToHost);

			printf("%s\r\n",buffer);
		}
	}
}


#endif

/* micro seconds */
void enetMilisecondSleep(ADAP_Uint32 milisecond)
{
	struct timeval time;


	time.tv_sec = milisecond / 1000;
	time.tv_usec = 1000*(milisecond%1000);

	  select(1,NULL,NULL,NULL,&time);
}


uint64_t ntohll(uint64_t n)
{
    return htonl(1) == 1 ? n : ((uint64_t) ntohl(n) << 32) | ntohl(n >> 32);
}

/* QoS Part */
/* Classifier */
ADAP_Uint32 adap_createClassifierEntry(tEnetHal_QoSClassMapEntry *pClassMapTable)
{
	ADAP_Uint32 idx=0;

	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		if( (tAdapDatabase.ClassifierTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFClass==pClassMapTable->QoSMFClass) )
		{
			return idx;
		}
	}
	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		if(tAdapDatabase.ClassifierTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.ClassifierTable[idx].valid=MEA_TRUE;
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_freeClassifierEntry(ADAP_Int32 ClassId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		if( (tAdapDatabase.ClassifierTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFClass==ClassId) )
		{
			tAdapDatabase.ClassifierTable[idx].valid=MEA_FALSE;
			adap_memset(&tAdapDatabase.ClassifierTable[idx].issClassifier, 0, sizeof(tEnetHal_QoSClassMapEntry));
			adap_memset(&tAdapDatabase.ClassifierTable[idx].issFilter, 0, sizeof(tFilterDb));
			tAdapDatabase.ClassifierTable[idx].PolicyIdx = ADAP_END_OF_TABLE;
			tAdapDatabase.ClassifierTable[idx].VlanId = ADAP_END_OF_TABLE;
			tAdapDatabase.ClassifierTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
			return ENET_SUCCESS;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free classId:%d\r\n",ClassId);
	return ENET_FAILURE;

}

tClassifierDb *adap_getClassifierEntry(ADAP_Int32 ClassId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		if( (tAdapDatabase.ClassifierTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFClass==ClassId) )
		{
			return &tAdapDatabase.ClassifierTable[idx];
		}
	}
	return NULL;
}

tClassifierDb *adap_getClassifierEntryByIndex(ADAP_Uint16 ClassIdx)
{
	if(ClassIdx > MAX_NUM_OF_CLASSIFIERS)
		return NULL;
	else
		return &tAdapDatabase.ClassifierTable[ClassIdx];
}


ADAP_Uint16 adap_getClassifierIndex(ADAP_Int32 ClassId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_CLASSIFIERS;idx++)
	{
		if( (tAdapDatabase.ClassifierTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFClass==ClassId) )
		{
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_updateClassifierEntry(ADAP_Uint32 idx, tEnetHal_QoSClassMapEntry *pClassMapTable)
{
	ADAP_Uint16 		Index;
	ADAP_Uint16 		class_index = 0;
	MEA_Bool	filterFound=MEA_FALSE;

	if(idx>=MAX_NUM_OF_CLASSIFIERS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_updateClassifierEntry - Classifier index is out of rangeidx:%d!\n", idx);
        return ENET_FAILURE;
	}
	if( tAdapDatabase.ClassifierTable[idx].valid==ADAP_TRUE )
	{
		tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFClass = pClassMapTable->QoSMFClass;
		tAdapDatabase.ClassifierTable[idx].issClassifier.QoSMFCStatus = pClassMapTable->QoSMFCStatus;
		tAdapDatabase.ClassifierTable[idx].issClassifier.PreColor = pClassMapTable->PreColor;
		tAdapDatabase.ClassifierTable[idx].issClassifier.u2PPPSessId = pClassMapTable->u2PPPSessId;

		if(pClassMapTable->pPriorityMapPtr==NULL && pClassMapTable->pL2FilterPtr==NULL && pClassMapTable->pL3FilterPtr==NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," No Classifier specified!!! (pPriorityMapPtr=pL2FilterPtr=pL3FilterPtr=NULL)\n");
			return ENET_SUCCESS;
		}

		if(pClassMapTable->pPriorityMapPtr)
		{
			adap_memcpy(&tAdapDatabase.ClassifierTable[idx].issFilter.VlanPriorityMap, pClassMapTable->pPriorityMapPtr, sizeof(tEnetHal_PriorityMapEntry));
			tAdapDatabase.ClassifierTable[idx].issClassifier.pPriorityMapPtr = &tAdapDatabase.ClassifierTable[idx].issFilter.VlanPriorityMap;
		}

		if(pClassMapTable->pL2FilterPtr)
		{
			filterFound = MEA_FALSE;
			for(Index = 0; Index < ADAP_MAX_NUM_ACL_FILTERS; Index++)
			{
				if( (tAdapDatabase.ClassifierTable[idx].issFilter.l2Valid[Index] == MEA_TRUE) && (tAdapDatabase.ClassifierTable[idx].issFilter.L2Filter[Index].filterNoId == pClassMapTable->pL2FilterPtr->filterNoId) )
				{
					adap_memcpy(&tAdapDatabase.ClassifierTable[idx].issFilter.L2Filter[Index], pClassMapTable->pL2FilterPtr, sizeof(tEnetHal_L2FilterEntry));
					tAdapDatabase.ClassifierTable[idx].issClassifier.pL2FilterPtr = &tAdapDatabase.ClassifierTable[idx].issFilter.L2Filter[Index];
					filterFound = MEA_TRUE;
					break;
				}
			}
			if(filterFound == MEA_FALSE && pClassMapTable->pL2FilterPtr->filterNoId != 0)
			{
				Index = 0;
				while(tAdapDatabase.ClassifierTable[idx].issFilter.l2Valid[Index] == MEA_TRUE)
				{
					Index++;
					if(Index >= ADAP_MAX_NUM_ACL_FILTERS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Num of L2 filters is %d!\n", Index);
						return ENET_FAILURE;
					}
				}
				adap_memcpy(&tAdapDatabase.ClassifierTable[idx].issFilter.L2Filter[Index], pClassMapTable->pL2FilterPtr, sizeof(tEnetHal_L2FilterEntry));
				tAdapDatabase.ClassifierTable[idx].issClassifier.pL2FilterPtr = &tAdapDatabase.ClassifierTable[idx].issFilter.L2Filter[Index];
				tAdapDatabase.ClassifierTable[idx].issFilter.l2Valid[Index] = MEA_TRUE;

				for(class_index = 0; class_index < MAX_NUM_OF_CLASSIFIERS; class_index++)
				{	
					if ((tAdapDatabase.ClassifierTable[class_index].valid != 1) ||
						(class_index == idx))
					{
						continue;
					}			
					for(Index = 0; Index < ADAP_MAX_NUM_ACL_FILTERS; Index++)
					{
						if (tAdapDatabase.ClassifierTable[class_index].issFilter.L2Filter[Index].filterNoId == pClassMapTable->pL2FilterPtr->filterNoId)
						{
							tAdapDatabase.ClassifierTable[class_index].issFilter.l2Valid[Index] = MEA_FALSE;
							adap_memset(&tAdapDatabase.ClassifierTable[class_index].issFilter.L2Filter[Index], 
								0, sizeof(tEnetHal_L2FilterEntry)); 
						}
					}
				}
			}
		}

		if(pClassMapTable->pL3FilterPtr)
		{
			filterFound = MEA_FALSE;
			for(Index = 0; Index < ADAP_MAX_NUM_ACL_FILTERS; Index++)
			{
				if( (tAdapDatabase.ClassifierTable[idx].issFilter.l3Valid[Index] == MEA_TRUE) && (tAdapDatabase.ClassifierTable[idx].issFilter.L3Filter[Index].filterNoId == pClassMapTable->pL3FilterPtr->filterNoId) )
				{
					adap_memcpy(&tAdapDatabase.ClassifierTable[idx].issFilter.L3Filter[Index], pClassMapTable->pL3FilterPtr, sizeof(tEnetHal_L3FilterEntry));
					tAdapDatabase.ClassifierTable[idx].issClassifier.pL3FilterPtr = &tAdapDatabase.ClassifierTable[idx].issFilter.L3Filter[Index];
					filterFound = MEA_TRUE;
					break;
				}
			}
			if(filterFound == MEA_FALSE && pClassMapTable->pL3FilterPtr->filterNoId != 0)
			{
				Index = 0;
				while(tAdapDatabase.ClassifierTable[idx].issFilter.l3Valid[Index] == MEA_TRUE)
				{
					Index++;
					if(Index >= ADAP_MAX_NUM_ACL_FILTERS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Num of L3 filters is %d!\n", Index);
						return ENET_FAILURE;
					}
				}
				adap_memcpy(&tAdapDatabase.ClassifierTable[idx].issFilter.L3Filter[Index], pClassMapTable->pL3FilterPtr, sizeof(tEnetHal_L3FilterEntry));
				tAdapDatabase.ClassifierTable[idx].issClassifier.pL3FilterPtr = &tAdapDatabase.ClassifierTable[idx].issFilter.L3Filter[Index];
				tAdapDatabase.ClassifierTable[idx].issFilter.l3Valid[Index] = MEA_TRUE;
	
				for(class_index = 0; class_index < MAX_NUM_OF_CLASSIFIERS; class_index++)
				{	
					if ((tAdapDatabase.ClassifierTable[class_index].valid != 1) ||
						(class_index == idx))
					{
						continue;
					}			
					for(Index = 0; Index < ADAP_MAX_NUM_ACL_FILTERS; Index++)
					{
						if (tAdapDatabase.ClassifierTable[class_index].issFilter.L3Filter[Index].filterNoId == pClassMapTable->pL3FilterPtr->filterNoId)
						{
							tAdapDatabase.ClassifierTable[class_index].issFilter.l3Valid[Index] = MEA_FALSE;
							adap_memset(&tAdapDatabase.ClassifierTable[class_index].issFilter.L3Filter[Index], 
								0, sizeof(tEnetHal_L3FilterEntry)); 
						}
					}
				}
			}

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_updateClassifierEntry: The L3 classification specified for Classifier with Index: %d L3Index:%d\n", idx, Index);
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_updateClassifierEntry: Unable to find classifier for class: %d L3Index:%d\n", pClassMapTable->QoSMFClass);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_UnmapClassFromPolicer(ADAP_Uint16 ClassifierIndex)
{
	ADAP_Uint16 PolicerIndex;

    PolicerIndex = tAdapDatabase.ClassifierTable[ClassifierIndex].PolicyIdx;
    if(PolicerIndex == ADAP_END_OF_TABLE)
    	return ENET_FAILURE;

    tAdapDatabase.PolicyTable[PolicerIndex].ClassifierIdx = ADAP_END_OF_TABLE;
    return ENET_SUCCESS;
}

/* Policer */
ADAP_Uint32 adap_createPolicerEntry(tEnetHal_QoSPolicyMapEntry * pPlyMapEntry)
{
	ADAP_Uint32 idx=0;

	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		if( (tAdapDatabase.PolicyTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.PolicyTable[idx].issPolicy.QoSPolicyMapId==pPlyMapEntry->QoSPolicyMapId) )
		{
			return idx;
		}
	}
	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		if(tAdapDatabase.PolicyTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.PolicyTable[idx].valid=MEA_TRUE;
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_freePolicerEntry(ADAP_Int32 PolicyId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		if( (tAdapDatabase.PolicyTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.PolicyTable[idx].issPolicy.QoSPolicyMapId==PolicyId) )
		{
			tAdapDatabase.PolicyTable[idx].valid=MEA_FALSE;
			adap_memset(&tAdapDatabase.PolicyTable[idx].issPolicy, 0, sizeof(tEnetHal_QoSPolicyMapEntry));
			adap_memset(&tAdapDatabase.PolicyTable[idx].issInProActEntry, 0, sizeof(tEnetHal_QoSInProfileActionEntry));
			adap_memset(&tAdapDatabase.PolicyTable[idx].issOutProActEntry, 0, sizeof(tEnetHal_QoSOutProfileActionEntry));
			tAdapDatabase.PolicyTable[idx].ClassifierIdx = ADAP_END_OF_TABLE;
			tAdapDatabase.PolicyTable[idx].MeterIdx = ADAP_END_OF_TABLE;
			tAdapDatabase.PolicyTable[idx].QueueIdx = ADAP_END_OF_TABLE;
			tAdapDatabase.PolicyTable[idx].issInProActEntryValid = ADAP_FALSE;
			tAdapDatabase.PolicyTable[idx].issOutProActEntryValid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free PolicyId:%d\r\n",PolicyId);
	return ENET_FAILURE;

}

tPolicyDb *adap_getPolicerEntry(ADAP_Int32 PolicyId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		if( (tAdapDatabase.PolicyTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.PolicyTable[idx].issPolicy.QoSPolicyMapId==PolicyId) )
		{
			return &tAdapDatabase.PolicyTable[idx];
		}
	}
	return NULL;
}

tPolicyDb *adap_getPolicerEntryByIndex(ADAP_Uint16 PolicyIdx)
{
	if(PolicyIdx > MAX_NUM_OF_POLICERS)
		return NULL;
	else
		return &tAdapDatabase.PolicyTable[PolicyIdx];
}


ADAP_Uint16 adap_getPolicerIndex(ADAP_Int32 PolicyId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_POLICERS;idx++)
	{
		if( (tAdapDatabase.PolicyTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.PolicyTable[idx].issPolicy.QoSPolicyMapId==PolicyId) )
		{
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_updatePolicerEntry(ADAP_Uint32 idx,
									tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
									tEnetHal_QoSInProfileActionEntry * pInProActEntry,
									tEnetHal_QoSOutProfileActionEntry * pOutProActEntry)
{
	if(idx>=MAX_NUM_OF_POLICERS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_updatePolicerEntry - Policer index is out of rangeidx:%d!\n", idx);
        return ENET_FAILURE;
	}
	adap_memcpy(&tAdapDatabase.PolicyTable[idx].issPolicy, pPlyMapEntry, sizeof(tEnetHal_QoSPolicyMapEntry));
    if(pInProActEntry)
    {
         adap_memcpy(&tAdapDatabase.PolicyTable[idx].issInProActEntry, pInProActEntry, sizeof(tEnetHal_QoSInProfileActionEntry));
         tAdapDatabase.PolicyTable[idx].issInProActEntryValid = ADAP_TRUE;
    }
    else
    	tAdapDatabase.PolicyTable[idx].issInProActEntryValid = ADAP_FALSE;
	if(pOutProActEntry)
	{
		adap_memcpy(&tAdapDatabase.PolicyTable[idx].issOutProActEntry, pOutProActEntry, sizeof(tEnetHal_QoSOutProfileActionEntry));
		tAdapDatabase.PolicyTable[idx].issOutProActEntryValid = ADAP_TRUE;
	}
	else
		tAdapDatabase.PolicyTable[idx].issOutProActEntryValid = ADAP_FALSE;

	return ENET_SUCCESS;
}

/* Scheduler*/
tSchedulerDb *adap_createSchedulerEntry(tEnetHal_QoSSchedulerEntry *pSchedTable)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if( (tAdapDatabase.SchedulerTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId==pSchedTable->QosSchedulerId) &&
				(tAdapDatabase.SchedulerTable[idx].issScheduler.QosIfIndex==pSchedTable->QosIfIndex))
		{
			return &tAdapDatabase.SchedulerTable[idx];
		}
	}
	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if(tAdapDatabase.SchedulerTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.SchedulerTable[idx].valid=MEA_TRUE;
		    adap_memcpy(&tAdapDatabase.SchedulerTable[idx].issScheduler, pSchedTable, sizeof(tEnetHal_QoSSchedulerEntry));
		    adap_memcpy(&tAdapDatabase.SchedulerTable[idx].issShaper, pSchedTable->pShapePtr, sizeof(tEnetHal_QoSShapeCfgEntry));
		    tAdapDatabase.SchedulerTable[idx].level = pSchedTable->HL;
		    tAdapDatabase.SchedulerTable[idx].iss_PortNumber = pSchedTable->QosIfIndex;

			return &tAdapDatabase.SchedulerTable[idx];
		}
	}
	return NULL;
}

ADAP_Uint32 adap_freeSchedulerEntry(ADAP_Int32 ifIndex, ADAP_Int32 SchedId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if( (tAdapDatabase.SchedulerTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId==SchedId) &&
				(tAdapDatabase.SchedulerTable[idx].issScheduler.QosIfIndex==ifIndex))
		{
			tAdapDatabase.SchedulerTable[idx].valid=MEA_FALSE;
			adap_memset(&tAdapDatabase.SchedulerTable[idx].issScheduler, 0, sizeof(tEnetHal_QoSSchedulerEntry));
			adap_memset(&tAdapDatabase.SchedulerTable[idx].issShaper, 0, sizeof(tEnetHal_QoSShapeCfgEntry));
			tAdapDatabase.SchedulerTable[idx].level = ADAP_END_OF_TABLE;
			tAdapDatabase.SchedulerTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
			return ENET_SUCCESS;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free schedId:%d\r\n",SchedId);
	return ENET_FAILURE;

}

tSchedulerDb *adap_getSchedulerEntry(ADAP_Int32 ifIndex, ADAP_Int32 SchedId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if( (tAdapDatabase.SchedulerTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId==SchedId) &&
				tAdapDatabase.SchedulerTable[idx].iss_PortNumber == ifIndex)
		{
			return &tAdapDatabase.SchedulerTable[idx];
		}
	}
	return NULL;
}

tSchedulerDb *adap_getSchedulerEntryByIndex(ADAP_Uint16 SchedIdx)
{
	if(SchedIdx > MAX_NUM_OF_SCHEDULERS)
		return NULL;
	else
		return &tAdapDatabase.SchedulerTable[SchedIdx];
}


ADAP_Uint16 adap_getSchedulerIndex(ADAP_Int32 ifIndex, ADAP_Int32 SchedId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if( (tAdapDatabase.SchedulerTable[idx].valid==ADAP_TRUE) && (tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId==SchedId) &&
				tAdapDatabase.SchedulerTable[idx].iss_PortNumber == ifIndex)
		{
			return idx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_updateSchedulerEntry(tEnetHal_QoSSchedulerEntry *pSchedTable)
{
	ADAP_Uint32 idx=0;

	for(idx=1;idx<MAX_NUM_OF_SCHEDULERS;idx++)
	{
		if( (tAdapDatabase.SchedulerTable[idx].valid==ADAP_TRUE) && 
			(tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId==pSchedTable->QosSchedulerId) &&
			(tAdapDatabase.SchedulerTable[idx].iss_PortNumber == pSchedTable->QosIfIndex))
		{
		    adap_memcpy(&tAdapDatabase.SchedulerTable[idx].issScheduler, pSchedTable, sizeof(tEnetHal_QoSSchedulerEntry));
		    adap_memcpy(&tAdapDatabase.SchedulerTable[idx].issShaper, pSchedTable->pShapePtr, sizeof(tEnetHal_QoSShapeCfgEntry));
		    tAdapDatabase.SchedulerTable[idx].level = pSchedTable->HL;
		    tAdapDatabase.SchedulerTable[idx].iss_PortNumber = pSchedTable->QosIfIndex;
#if 0
			if((tAdapDatabase.SchedulerTable[idx].ClusterId != ADAP_END_OF_TABLE) && (tAdapDatabase.SchedulerTable[idx].level == H2Queue))
			{
				ADAP_Uint16 ClusterIndex;
				//Reset clusterId for this scheduler
				for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
				{
					if((tAdapDatabase.ClustersTable[tAdapDatabase.SchedulerTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].valid == ADAP_TRUE) &&
						tAdapDatabase.ClustersTable[tAdapDatabase.SchedulerTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].clusterId == tAdapDatabase.SchedulerTable[idx].ClusterId )
					{
						tAdapDatabase.ClustersTable[tAdapDatabase.SchedulerTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].valid = ADAP_FALSE;
						break;
					}
				}
				if(ClusterIndex == NUM_OF_CLUSTERS_PER_PORT)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SchedHierarchyMap - unable to remove clusterId from u4SchedId:%d!\n", tAdapDatabase.SchedulerTable[idx].issScheduler.QosSchedulerId);
					return ENET_FAILURE;
				}
			}
			tAdapDatabase.SchedulerTable[idx].ClusterId = ADAP_END_OF_TABLE;
#endif
		    tAdapDatabase.SchedulerTable[idx].level = pSchedTable->HL;
		    tAdapDatabase.SchedulerTable[idx].iss_PortNumber = pSchedTable->QosIfIndex;

			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Bool adap_compareSchedulerEntry(tSchedulerDb *pSchedDb, tEnetHal_QoSSchedulerEntry *pSchedEntry)
{
	if( (pSchedDb == NULL) || (pSchedEntry == NULL) )
		return ADAP_TRUE;

	//Check if we need to update scheduler DB
	if( (pSchedDb->issShaper.u4QosCIR == pSchedEntry->pShapePtr->u4QosCIR) &&
		(pSchedDb->issShaper.u4QosCBS == pSchedEntry->pShapePtr->u4QosCBS) &&
		(pSchedDb->issShaper.u4QosEIR == pSchedEntry->pShapePtr->u4QosEIR) &&
		(pSchedDb->issShaper.u4QosEBS == pSchedEntry->pShapePtr->u4QosEBS) &&
		(pSchedDb->issScheduler.QosSchedulerId == pSchedEntry->QosSchedulerId) &&
		(pSchedDb->issScheduler.HL == pSchedEntry->HL) &&
		(pSchedDb->issScheduler.QosSchedAlgo == pSchedEntry->QosSchedAlgo) )
			return ADAP_TRUE;

	return ADAP_FALSE;
}

ADAP_Uint32 adap_SchedHierarchyMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint32 u4NextSchedId, ADAP_Int16 i2HL,ADAP_Uint16 u2Sweight)
{
	ADAP_Uint16 ParentIndex;
	ADAP_Uint16 ThisIndex;    /* "Our" Scheduler (ID=u4SchedId)' Index*/
	ADAP_Uint16 ChildIndex;   /* "Child" Scheduler (Scheduler.ChildsSchedIdx[<some>]=u4SchedId)' Index*/
	ADAP_Uint16 ClusterIndex;   /* Cluster Index for this port*/
	ADAP_Uint8 isFound = 0;

    /* Update parent */

    for(ThisIndex = 1; ThisIndex < MAX_NUM_OF_SCHEDULERS; ThisIndex++)
    {
        if((tAdapDatabase.SchedulerTable[ThisIndex].iss_PortNumber == i4IfIndex) &&
            (tAdapDatabase.SchedulerTable[ThisIndex].issScheduler.QosSchedulerId == (ADAP_Int32)u4SchedId))
        {
        	tAdapDatabase.SchedulerTable[ThisIndex].level = i2HL;
        	tAdapDatabase.SchedulerTable[ThisIndex].weight = u2Sweight;

        	/* Assign Cluster to the scheduler */
        	if((i2HL == H2Queue) && (tAdapDatabase.SchedulerTable[ThisIndex].ClusterId == ADAP_END_OF_TABLE))
        	{
        		for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
        		{
        			if(tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].valid == ADAP_FALSE)
        			{
        				tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].valid = ADAP_TRUE;
        				tAdapDatabase.SchedulerTable[ThisIndex].ClusterId = tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].clusterId;
            	        		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_SchedHierarchyMap - clusterId:%d assigned to u4SchedId:%d SchedIndex:%d \n", 
						tAdapDatabase.SchedulerTable[ThisIndex].ClusterId, u4SchedId, ThisIndex);
        				break;
        			}
        		}
        		if(ClusterIndex == NUM_OF_CLUSTERS_PER_PORT)
        		{
        	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SchedHierarchyMap - unable to assign clusterId to u4SchedId:%d!\n", u4SchedId);
        	        return ENET_FAILURE;
        		}
        	}

            isFound = 1;

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SchedulerID:%d is mapped to Parent:%d with clusterId:%d!!!\n", u4SchedId, u4NextSchedId,
            		tAdapDatabase.SchedulerTable[ThisIndex].ClusterId);
            break;
        }
    }

    if(isFound == 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Scheduler ID:%d NOT FOUND on ISS interface:%d!\n", u4SchedId, i4IfIndex);
        return ENET_FAILURE;
    }

    /* Update all children and assign Cluster id to the H3 scheduler according to its parent cluster Id */
    if(u4NextSchedId == 0) //i2HL = 1, no need to update
    	return ENET_SUCCESS;

    isFound = 0;
    for(ParentIndex = 1; ParentIndex < MAX_NUM_OF_SCHEDULERS; ParentIndex++)
    {
        if((tAdapDatabase.SchedulerTable[ParentIndex].iss_PortNumber == i4IfIndex) &&
            (tAdapDatabase.SchedulerTable[ParentIndex].issScheduler.QosSchedulerId == (ADAP_Int32)u4NextSchedId))
        {
            for(ChildIndex = 0; ChildIndex < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; ChildIndex++)
            {
                if(tAdapDatabase.SchedulerTable[ParentIndex].ChildsSchedIdx[ChildIndex] == ADAP_END_OF_TABLE)
                {
                	tAdapDatabase.SchedulerTable[ParentIndex].ChildsSchedIdx[ChildIndex] = ThisIndex;
                    isFound = 1;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"In the parent Sched(ID:%d) Child#%d is updated with u4SchedId: %d\n",
							tAdapDatabase.SchedulerTable[ParentIndex].issScheduler.QosSchedulerId, ChildIndex, u4SchedId);
                    if(i2HL == H3PriQueue)
                    {
                    	tAdapDatabase.SchedulerTable[ThisIndex].ClusterId = tAdapDatabase.SchedulerTable[ParentIndex].ClusterId;
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4SchedId: %d is updated with clusterId:%d of its parent SchedId:%d\n",
                        		u4SchedId, tAdapDatabase.SchedulerTable[ParentIndex].ClusterId,
								tAdapDatabase.SchedulerTable[ParentIndex].issScheduler.QosSchedulerId);
                    }
                    break;
                }
            }
            if(ChildIndex >= ENET_PLAT_QUEUE_NUM_OF_PRI_Q)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"No Room in the children table - all 8 are already defined for the Parent Sched(ID:%d)!!!\n",
						tAdapDatabase.SchedulerTable[ParentIndex].issScheduler.QosSchedulerId);
                return ENET_FAILURE;
            }
        }
    }

    if(isFound == 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Parent Scheduler ID:%d NOT FOUND on ISS interface:%d!\n",
                u4NextSchedId,
                i4IfIndex);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 adap_SchedHierarchyUnMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint32 u4NextSchedId, ADAP_Uint16 i2HL)
{
	ADAP_Uint16 Index, ChildIndex;
	ADAP_Uint8 isFound = 0;
	ADAP_Uint16 ClusterIndex;   /* Cluster Index for this port*/

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_SchedHierarchyUnMap for Port: %d SchedID: %d NextSchedID: %d Level: %d!!!\n", i4IfIndex, u4SchedId, u4NextSchedId, i2HL);

	/* Update all children*/
	for(Index = 1; Index < MAX_NUM_OF_SCHEDULERS; Index++)
	{
		if((tAdapDatabase.SchedulerTable[Index].iss_PortNumber == i4IfIndex) &&
			(tAdapDatabase.SchedulerTable[Index].issScheduler.QosSchedulerId == (ADAP_Int32)u4NextSchedId))
		{
			for(ChildIndex = 0; ChildIndex < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; ChildIndex++)
			{
				if(tAdapDatabase.SchedulerTable[Index].ChildsSchedIdx[ChildIndex] == adap_getSchedulerIndex(i4IfIndex, u4SchedId))
				{
					tAdapDatabase.SchedulerTable[Index].ChildsSchedIdx[ChildIndex] = ADAP_END_OF_TABLE;
					isFound = 1;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Child %d For Parent SchedId: %d is unmapped!!!\n", ChildIndex, u4NextSchedId);
					break;
				}
			}
			if(isFound == 1)
				break;
		}
	}

	if(isFound == 0)
	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"For Parent Scheduler ID:%d children NOT FOUND on ISS interface:%d!\n", u4NextSchedId, i4IfIndex);
		return ENET_FAILURE;
	}
	else
	{
		isFound = 0;
		for(Index = 1; Index < MAX_NUM_OF_SCHEDULERS; Index++)
		{
			if((tAdapDatabase.SchedulerTable[Index].iss_PortNumber == i4IfIndex) &&
				(tAdapDatabase.SchedulerTable[Index].issScheduler.QosSchedulerId == (ADAP_Int32)u4SchedId))
			{
				tAdapDatabase.SchedulerTable[Index].level = MAX_SCHEDULER_LEVEL;
		       	/* Remove ClusterId from the scheduler */
				if(i2HL == H2Queue)
				{
					for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
					{
						if((tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].valid == ADAP_TRUE) &&
							tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].clusterId == tAdapDatabase.SchedulerTable[Index].ClusterId )
						{
							tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].valid = ADAP_FALSE;
							tAdapDatabase.SchedulerTable[Index].ClusterId = ADAP_END_OF_TABLE;
							break;
						}
					}
					if(ClusterIndex == NUM_OF_CLUSTERS_PER_PORT)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SchedHierarchyMap - unable to remove clusterId from u4SchedId:%d!\n", u4SchedId);
						return ENET_FAILURE;
					}
				}

				isFound = 1;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Parent for SchedId: %d is unmapped!!!\n", u4SchedId);
				break;
			}
		}
	}

	if(isFound == 0)
		return ENET_FAILURE;
	else
		return ENET_SUCCESS;
}

ENET_QueueId_t adap_GetAssignedClusterId(ADAP_Int32 i4IfIndex,tSchedulerDb * pSchedEntry, ADAP_Uint16 	SchedIndex)
{
	ENET_QueueId_t clusterId = ADAP_END_OF_TABLE;
	ADAP_Uint16 Index, ChildIndex;

	if(pSchedEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_GetAssignedClusterId - Invalid scheduler!!!\n");
		return clusterId;
	}

	switch(pSchedEntry->level)
	{
	case H1Port:
		clusterId = ADAP_END_OF_TABLE;
		break;
	case H2Queue:
		clusterId = pSchedEntry->ClusterId;
		break;
	case H3PriQueue:
		for(Index = 1; Index < MAX_NUM_OF_SCHEDULERS; Index++)
		{
			if(tAdapDatabase.SchedulerTable[Index].level != H2Queue)
				continue;

			if(tAdapDatabase.SchedulerTable[Index].iss_PortNumber != i4IfIndex)
				continue;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Look for ClusterId for i4IfIndex:%d SchedId: %d Parent SchedIdx:%d This SchedIdx:%d\n",
					i4IfIndex, pSchedEntry->issScheduler.QosSchedulerId, Index, SchedIndex);

			for(ChildIndex = 0; ChildIndex < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; ChildIndex++)
			{
				if(tAdapDatabase.SchedulerTable[Index].ChildsSchedIdx[ChildIndex] == SchedIndex)
				{
					clusterId = tAdapDatabase.SchedulerTable[Index].ClusterId;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ClusterId %d For i4IfIndex:%d SchedId: %d\n", 
							clusterId, i4IfIndex, pSchedEntry->issScheduler.QosSchedulerId);
					break;
				}
			}
		}
		break;
	default:
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetAssignedClusterId - Invalid scheduler level:%d for schedId:%d!!!\n", pSchedEntry->level, pSchedEntry->issScheduler.QosSchedulerId);
	}

	return clusterId;
}

tQueueDb *adap_createQueueEntry(ADAP_Int32 i4IfIndex, tEnetHal_QoSQEntry * pQEntry, tEnetHal_QoSQtypeEntry * pQTypeEntry, tEnetHal_QoSREDCfgEntry * papRDCfgEntry[])
{
	ADAP_Uint32 	idx=0;
	ADAP_Uint16 	wredIndex, SchedIndex;
	tSchedulerDb	*pSchedEntry = NULL;
	ADAP_Uint16 	ClusterIndex;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry for Port:%d\n", i4IfIndex);
	for(idx=1;idx<MAX_NUM_OF_QUEUES;idx++)
	{
		if( (tAdapDatabase.QueueTable[idx].valid==ADAP_TRUE) && 
		    (tAdapDatabase.QueueTable[idx].issQueue.i4QosQId==pQEntry->i4QosQId) &&
		    (tAdapDatabase.QueueTable[idx].iss_PortNumber==i4IfIndex))
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry-Queue found for ifIndex:%d  with QId:%d\n", i4IfIndex, pQEntry->i4QosQId);
			return &tAdapDatabase.QueueTable[idx];
		}
	}
	for(idx=1;idx<MAX_NUM_OF_QUEUES;idx++)
	{
		if(tAdapDatabase.QueueTable[idx].valid==ADAP_FALSE)
		{
			tAdapDatabase.QueueTable[idx].valid=MEA_TRUE;

		    adap_memcpy(&tAdapDatabase.QueueTable[idx].issQueue, pQEntry, sizeof(tEnetHal_QoSQEntry));
		    adap_memcpy(&tAdapDatabase.QueueTable[idx].issQueueType, pQTypeEntry, sizeof(tEnetHal_QoSQtypeEntry));
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry  - Wred part\n");
		    for(wredIndex = 0; wredIndex < 3; wredIndex++)
		    {
		        if(papRDCfgEntry[wredIndex] != NULL)
		        {

		            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry - wredIndex: %d MinThresh:%d Max Thresh:%d Prob:%d ECN_Enable:%d ECN_Thresh:%d \n",
		                    wredIndex,
		                    papRDCfgEntry[wredIndex]->u4MinAvgThresh,
		                    papRDCfgEntry[wredIndex]->u4MaxAvgThresh,
		                    papRDCfgEntry[wredIndex]->u1MaxProbability,
		                    papRDCfgEntry[wredIndex]->u1RDActionFlag,
		                    papRDCfgEntry[wredIndex]->u4ECNThresh);

		            if(papRDCfgEntry[wredIndex]->u1RDActionFlag !=0 && papRDCfgEntry[wredIndex]->u4ECNThresh<10)
		            {
		                papRDCfgEntry[wredIndex]->u4ECNThresh=10;
		                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry -  ECN enable and ECN threshold < 10 - update it to 10\n");
		            }
		            adap_memcpy(&tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex], papRDCfgEntry[wredIndex], sizeof(tEnetHal_QoSREDCfgEntry));
		        }
		        else
		        {
		            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry - NULL papRDCfgEntry for wredIndex: %d!!! AlgoType=%d %s\n",
		                    wredIndex,
		                    pQTypeEntry->u1DropAlgo,
		                    pQTypeEntry->u1DropAlgoEnableFlag==ADAP_QOS_Q_TEMP_DROP_ALGO_DISABLE?"disabled":"enabled");
		            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"papRDCfgEntry[%d] is filled with the following defaults: MinAvgThresh = 20; MaxAvgThresh = 80; MaxProbability = 50; \n", wredIndex);

		            tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex].u4MinAvgThresh = 20;
		            tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex].u4MaxAvgThresh = 80;
		            tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex].u1MaxProbability = 50;
		            tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex].u1RDActionFlag = 0;
		            tAdapDatabase.QueueTable[idx].issWredEntry[wredIndex].u4ECNThresh = 20;
		        }
			}
		    tAdapDatabase.QueueTable[idx].iss_PortNumber = i4IfIndex;

		    /* Link the Queue to the attached Scheduler */
		    SchedIndex = adap_getSchedulerIndex(i4IfIndex, pQEntry->i4QosSchedulerId);
		    if(SchedIndex == ADAP_END_OF_TABLE)
		    {
		        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid Scheduler Id:%d!!! \n", pQEntry->i4QosSchedulerId);
		        return NULL;
		    }
		    else
		    {
		        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"Scheduler index is: %d for ScheId: %d!!! \n", SchedIndex, pQEntry->i4QosSchedulerId);
		    }

		    tAdapDatabase.QueueTable[idx].SchedulerIdx = SchedIndex;
		    tAdapDatabase.SchedulerTable[SchedIndex].QueueIdx = idx;

		    pSchedEntry = adap_getSchedulerEntryByIndex(SchedIndex);
		    if((pSchedEntry == NULL) || (pSchedEntry->level != H3PriQueue))
		    {
		        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid u4SchedId %d is assigned to the QId:%d!\n", pQEntry->i4QosSchedulerId, pQEntry->i4QosQId);
		        return ENET_FAILURE;
		    }
		    else
		    {
		    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetQoSHwSchedulerUpdateParams - Scheduler found for u4SchedId: %d!!! \n", pQEntry->i4QosSchedulerId);

		    	tAdapDatabase.QueueTable[idx].ClusterId = adap_GetAssignedClusterId(i4IfIndex, pSchedEntry, SchedIndex);
	       		for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
	        	{
					if((tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].valid == ADAP_TRUE) &&
						( tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].clusterId == tAdapDatabase.QueueTable[idx].ClusterId))
					{
						tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].numOfDefinedQueues++;
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry - clusterId:%d numOfDefinedQueues:%d!\n", tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].clusterId, tAdapDatabase.ClustersTable[i4IfIndex].clusterInfo[ClusterIndex].numOfDefinedQueues);
						break;
					}
				}
	       			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry  - for port:%d Queue with QId:%d clusterID:%d\n",
	       				tAdapDatabase.QueueTable[idx].iss_PortNumber, pQEntry->i4QosQId, tAdapDatabase.QueueTable[idx].ClusterId);
				if(ClusterIndex == NUM_OF_CLUSTERS_PER_PORT)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_createQueueEntry - unable to find clusterId for queue:%d!\n", pQEntry->i4QosQId);
					return ENET_FAILURE;
				}
		    }

		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry  - new Queue with QId:%d is created in DB\n", pQEntry->i4QosQId);
			return &tAdapDatabase.QueueTable[idx];
		}
	}
	return NULL;
}

tQueueDb *adap_getQueueEntry(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId)
{
	ADAP_Uint32 idx=0;
	for(idx=1;idx<MAX_NUM_OF_QUEUES;idx++)
	{
		if( (tAdapDatabase.QueueTable[idx].valid==ADAP_TRUE) && 
			(tAdapDatabase.QueueTable[idx].issQueue.i4QosQId==(ADAP_Int32)QueueId) && 
			(tAdapDatabase.QueueTable[idx].iss_PortNumber==i4IfIndex))
		{
			return &tAdapDatabase.QueueTable[idx];
		}
	}
	return NULL;
}

tQueueDb *adap_getQueueEntryByIndex(ADAP_Uint16 QueueIdx)
{
	if(QueueIdx > MAX_NUM_OF_QUEUES)
		return NULL;
	else
		return &tAdapDatabase.QueueTable[QueueIdx];
}

ADAP_Uint16 adap_getQueueIndexById(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId)
{
	ADAP_Uint32 QueueIdx=0;
	for(QueueIdx=1;QueueIdx<MAX_NUM_OF_QUEUES;QueueIdx++)
	{
		if( (tAdapDatabase.QueueTable[QueueIdx].valid==ADAP_TRUE) && 
			(tAdapDatabase.QueueTable[QueueIdx].issQueue.i4QosQId==QueueId) &&
			(tAdapDatabase.QueueTable[QueueIdx].iss_PortNumber==i4IfIndex))
		{
			return QueueIdx;
		}
	}
	return ADAP_END_OF_TABLE;
}

ADAP_Uint32 adap_freeQueueEntry(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint16 	ClusterIndex;

	for(idx=1;idx<MAX_NUM_OF_QUEUES;idx++)
	{
		if( (tAdapDatabase.QueueTable[idx].valid==ADAP_TRUE) && 
			(tAdapDatabase.QueueTable[idx].issQueue.i4QosQId==(ADAP_Int32)QueueId) &&
			(tAdapDatabase.QueueTable[idx].iss_PortNumber==i4IfIndex))
		{
			tAdapDatabase.QueueTable[idx].valid=MEA_FALSE;

		    adap_memset(&tAdapDatabase.QueueTable[idx].issQueue, 0, sizeof(tEnetHal_QoSQEntry));
		    adap_memset(&tAdapDatabase.QueueTable[idx].issQueueType, 0, sizeof(tEnetHal_QoSQtypeEntry));

		    tAdapDatabase.SchedulerTable[tAdapDatabase.QueueTable[idx].SchedulerIdx].QueueIdx = ADAP_END_OF_TABLE;
		    tAdapDatabase.QueueTable[idx].SchedulerIdx = ADAP_END_OF_TABLE;
       		for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
        	{
				if((tAdapDatabase.ClustersTable[tAdapDatabase.QueueTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].valid == ADAP_TRUE) &&
					( tAdapDatabase.ClustersTable[tAdapDatabase.QueueTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].clusterId == tAdapDatabase.QueueTable[idx].ClusterId))
				{
					tAdapDatabase.ClustersTable[tAdapDatabase.QueueTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].numOfDefinedQueues--;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_freeQueueEntry - clusterId:%d numOfDefinedQueues:%d!\n",
							tAdapDatabase.ClustersTable[tAdapDatabase.QueueTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].clusterId, tAdapDatabase.ClustersTable[tAdapDatabase.QueueTable[idx].iss_PortNumber].clusterInfo[ClusterIndex].numOfDefinedQueues);
					break;
				}
			}
			if(ClusterIndex == NUM_OF_CLUSTERS_PER_PORT)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_freeQueueEntry - unable to find clusterId for queue:%d!\n", QueueId);
				return ENET_FAILURE;
			}
		    tAdapDatabase.QueueTable[idx].iss_PortNumber = ADAP_END_OF_TABLE;
		    tAdapDatabase.QueueTable[idx].ClusterId = ADAP_END_OF_TABLE;

			return ENET_SUCCESS;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to free QId:%d\r\n",QueueId);
	return ENET_FAILURE;

}

ADAP_Uint16 adap_getNumDefinedQueuesForCluster(ADAP_Uint16 iss_PortNumber, ENET_QueueId_t clusterId)
{
	ADAP_Uint16 	ClusterIndex;
	ADAP_Uint16		numOfDefinedQueues = ADAP_END_OF_TABLE;

	for(ClusterIndex = 0; ClusterIndex < NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
	{
		if((tAdapDatabase.ClustersTable[iss_PortNumber].clusterInfo[ClusterIndex].valid == ADAP_TRUE) &&
			( tAdapDatabase.ClustersTable[iss_PortNumber].clusterInfo[ClusterIndex].clusterId == clusterId))
		{
			numOfDefinedQueues = tAdapDatabase.ClustersTable[iss_PortNumber].clusterInfo[ClusterIndex].numOfDefinedQueues;
			break;
		}
	}
	return numOfDefinedQueues;
}

ADAP_Uint32 adap_CreateClusters(void)
{
	ADAP_Uint32 Index=0, ClusterIndex = 0, PriQIdx = 0;
	MEA_Port_t 	enetPort;
	ADAP_Uint32 ret = ENET_FAILURE;
	ENET_Queue_dbt queue;
	MEA_Globals_Entry_dbt globals_entry;
    MEA_PortsMapping_t  portMap;

	for(Index = 1; Index < MeaAdapGetNumOfPhyPorts(); Index ++)
	{
		enetPort = ADAP_END_OF_TABLE;
		if(Index <= tAdapDatabase.number_of_physical_ports)
			ret = MeaAdapGetPhyPortFromLogPort(Index, &enetPort);
		if(ret == ENET_FAILURE)
        {
            printf("MeaAdapGetPhyPortFromLogPort for port: %d is failed!", Index);
            return ENET_FAILURE;
        }

        tAdapDatabase.ClustersTable[Index].iss_PortNumber = Index;
#if 0
        /* Check if the first cluster for this port is already defined*/
		if(ENET_Get_Queue (ENET_UNIT_0, enetPort, &queue)==ENET_OK)
		{
			/* Add this cluster to the adaptor DB */
			tAdapDatabase.ClustersTable[Index].clusterInfo[0].clusterId = enetPort;
		}
#endif
        /* Create 4 clusters per port with 8 pri queues per cluster*/
        for(ClusterIndex = 0; ClusterIndex<NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
        {
            adap_memset(&queue,0,sizeof(queue));
            MEA_OS_strcpy(queue.name,ENET_PLAT_GENERATE_NEW_NAME);
            queue.port.type              = ENET_QUEUE_PORT_TYPE_PORT;
            queue.port.id.port           = enetPort;
            queue.adminOn                = MEA_TRUE;

            if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK)
            {
                printf("MEA_API_Get_Globals_Entry for port: %d Cluster: %d is failed!", enetPort, ClusterIndex);
                return ENET_FAILURE;
            }

            if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0, MEA_PORTS_TYPE_PHYISCAL_TYPE, enetPort,  &portMap)!=MEA_OK)
            {
                printf("MEA_Low_Get_Port_Mapping_By_key for port: %d Cluster: %d is failed!", enetPort, ClusterIndex);
                return ENET_FAILURE;
            }

            if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY)
            {
                queue.mode.type             = ENET_QUEUE_MODE_TYPE_RR;
                queue.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL;
            }
            else
            {
                queue.mode.type    = ENET_QUEUE_MODE_TYPE_WFQ;
                queue.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
            }

            queue.MTU                    = MEA_QUEUE_CLUSTER_MTU_DEF_VAL;
            queue.shaper_enable          = MEA_FALSE;
            queue.Shaper_compensation    = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
            queue.groupId                = 0;
            adap_memset(&queue.shaper_info,0,sizeof(queue.shaper_info));

            for(PriQIdx=0;PriQIdx<ADAP_MAX_NUMBER_OF_PRI_QUEUE;PriQIdx++)
            {
                queue.pri_queues[PriQIdx].max_q_size_Packets			= ENET_PRI_QUEUE_MQS_PACKET_DEF_128;
                queue.pri_queues[PriQIdx].max_q_size_Byte				= (ENET_PRI_QUEUE_MQS_PACKET_DEF_128*2048);
                queue.pri_queues[PriQIdx].mtu							= MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                if(MEA_GLOBAL_PRI_Q_MODE_DEF_VAL != MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)
                {
                    queue.pri_queues[PriQIdx].mode.type					= ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                    queue.pri_queues[PriQIdx].mode.value.strict_priority	= ENET_PQ_STRICT_DEF_VAL;
                }
                else
                {
                    queue.pri_queues[PriQIdx].mode.type					= ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    queue.pri_queues[PriQIdx].mode.value.strict_priority	= ENET_PQ_RR_DEF_VAL;
                }
                queue.pri_queues[PriQIdx].mc_MQS                      = ENET_QUEUE_MC_MQS_DEF_VAL;
            }

            tAdapDatabase.ClustersTable[Index].clusterInfo[ClusterIndex].clusterId = ADAP_END_OF_TABLE;

            if(ENET_Create_Queue (MEA_UNIT_0, &queue, &(tAdapDatabase.ClustersTable[Index].clusterInfo[ClusterIndex].clusterId)) != ENET_OK)
            {
            	printf("Cluster QueueCreate: ENET_Create_Queue FAIL for port: %d Cluster: %d!\n", enetPort, ClusterIndex);
				tAdapDatabase.ClustersTable[Index].clusterInfo[ClusterIndex].clusterId = ADAP_END_OF_TABLE;
                return (ENET_FAILURE);
            }
 //           printf("Cluster %d created for port:%d!\n", tAdapDatabase.ClustersTable[Index].clusterInfo[ClusterIndex].clusterId, Index);
        }
	}
 	return ENET_SUCCESS;
}

ADAP_Uint32 adap_CreateDefaultLag(void)
{
	ADAP_Uint32 Index=0, ClusterIndex = 0;
	ADAP_Uint16	lagId=MEA_PLAT_GENERATE_NEW_ID;
	MEA_LAG_dbt entry;
	MEA_Port_t 	enetPort;

	for(Index = 1; Index < MeaAdapGetNumOfPhyPorts(); Index ++)
	{
		lagId=MEA_PLAT_GENERATE_NEW_ID;
		adap_memset(&entry,0,sizeof(entry));

		enetPort = ADAP_END_OF_TABLE;
		if(Index <= tAdapDatabase.number_of_physical_ports)
		{
			if(MeaAdapGetPhyPortFromLogPort(Index, &enetPort) == ENET_FAILURE)
			{
				printf("MeaAdapGetPhyPortFromLogPort for port: %d is failed!", Index);
				return ENET_FAILURE;
			}
		}
		entry.cluster[entry.numof_clusters++] = enetPort;
        for(ClusterIndex = 1; ClusterIndex<NUM_OF_CLUSTERS_PER_PORT; ClusterIndex++)
        {
			entry.cluster[entry.numof_clusters++] = adap_GetPortQueueID(Index, ClusterIndex);
        }

		if(MEA_API_LAG_Create_Entry(MEA_UNIT_0,&entry,&lagId)!=MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwCreateLagEntry : failed to call MEA_API_LAG_Create_Entry \n");
			return MEA_ERROR;
		}
	}
 	return ENET_SUCCESS;
}

ENET_QueueId_t adap_GetPortQueueID(ADAP_Uint32 u4IfIndex, ADAP_Uint16 ClusterIdx)
{
	if((u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS) &&(ClusterIdx<NUM_OF_CLUSTERS_PER_PORT))
	{
		return tAdapDatabase.ClustersTable[u4IfIndex].clusterInfo[ClusterIdx].clusterId;
	}

	return ADAP_END_OF_TABLE;
}

ADAP_Int32 adap_SetAgingTime(ADAP_Uint32 AgingTime)
{
	tAdapDatabase.tGlobals.aging_Time = AgingTime;

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_GetAgingTime(void)
{
	return tAdapDatabase.tGlobals.aging_Time;
}

ADAP_Int32 adap_SetTempAgingTime(ADAP_Uint32 AgingTime)
{
	tAdapDatabase.tGlobals.temp_aging_Time = AgingTime;

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_GetTempAgingTime(void)
{
	return tAdapDatabase.tGlobals.temp_aging_Time;
}

ADAP_Int32 adap_SetRecoverAgingTime(ADAP_Bool recover_aging_Time)
{
	tAdapDatabase.tGlobals.recover_aging_Time = recover_aging_Time;

	return ENET_SUCCESS;
}

ADAP_Bool adap_GetRecoverAgingTime(void)
{
	return tAdapDatabase.tGlobals.recover_aging_Time;
}

/*  LAG part*/
ADAP_Uint16 adap_setLagPolicyFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint32 u4SelectionPolicy)
{
	ADAP_Uint16 Index=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			tAdapDatabase.aggrEntry[Index].selPolicy = u4SelectionPolicy;
			return ENET_SUCCESS;
		}
	}
	// index not found
	return ENET_FAILURE;
}

ADAP_Uint32 adap_getLagPolicyFromAggrId(ADAP_Uint16 u2AggIndex)
{
	ADAP_Uint16 Index=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			return tAdapDatabase.aggrEntry[Index].selPolicy;
		}
	}
	// index not found
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_clearLagPortFromAggrId(ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			for(idx=0;idx<=MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) != 0)
				{
					tAdapDatabase.aggrEntry[Index].portbitmask &= ~(1 << u2PortNumber);
					tAdapDatabase.aggrEntry[Index].numLagPorts--;
					return ENET_SUCCESS;
				}
			}
			// not enough room
			return ENET_FAILURE;
		}
	}
	// index not found
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_EnableLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			for(idx=0;idx<=MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) == 0)
				{
					tAdapDatabase.aggrEntry[Index].portbitmask |= (1 << u2PortNumber);
				}

				return ENET_SUCCESS;
			}
			// not enough room
			return ENET_FAILURE;
		}
	}
	// index not found
	return ENET_FAILURE;
}

ADAP_Uint16 adap_NumOfLagPortFromAggrId(ADAP_Uint16 u2AggIndex)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"number of ports:%d\r\n",tAdapDatabase.aggrEntry[Index].numLagPorts);
			return tAdapDatabase.aggrEntry[Index].numLagPorts;
		}
	}
	return 0;
}

ADAP_Uint16 adap_isLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			// check if the port already exist
			for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) != 0)
				{
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_isPortPartOfLag(ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if(tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE)
		{
			// check if the port already exist
			for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) != 0)
				{
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}


ADAP_Uint16 adap_RemoveLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			// check if the port already exist
			for(idx=0;idx<=MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) != 0)
				{
					tAdapDatabase.aggrEntry[Index].portbitmask &= ~(1 << u2PortNumber);
					tAdapDatabase.aggrEntry[Index].numLagPorts--;
					return ENET_SUCCESS;
				}
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint16 adap_setLagPortFromAggrId(ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint16 idx=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2AggIndex))
		{
			if(tAdapDatabase.aggrEntry[Index].numLagPorts >= MAX_NUM_PORTS_IN_LAG)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_setLagPortFromAggrId:portId%d allready exist\r\n",u2PortNumber);
				return ENET_FAILURE;
			}
			// check if the port already exist
			for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_setLagPortFromAggrId:portId%d allready exist\r\n",u2PortNumber);
					return ENET_SUCCESS;
				}
			}

			for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
			{
				if( (tAdapDatabase.aggrEntry[Index].portbitmask & (1 << u2PortNumber)) == 0)
				{
					tAdapDatabase.aggrEntry[Index].portbitmask |= (1 << u2PortNumber);
					tAdapDatabase.aggrEntry[Index].numLagPorts++;
					return ENET_SUCCESS;
				}
			}
			// not enough room
			return ENET_FAILURE;
		}
	}
	// index not found
	return ENET_FAILURE;
}

ADAP_Uint16 adap_getLagIndexFromAggrId(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"index:%d aggrIdx:%d request:%d\r\n",Index,tAdapDatabase.aggrEntry[Index].swAggIdx,u2HwAggIdx);
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			return Index;
		}
	}
	return ADAP_END_OF_TABLE;
}

tDbaseHwAggEntry *adap_getFirstLagInstanceFromIndex(void)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if(tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE)
		{
			return &tAdapDatabase.aggrEntry[Index];
		}
	}
	return NULL;
}

tDbaseHwAggEntry *adap_getNextLagInstanceFromIndex(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;
	ADAP_Uint8 found=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if(found==0)
		{
			if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
			{
				found=1;
			}
		}
		else
		{
			if(tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE)
			{
				return &tAdapDatabase.aggrEntry[Index];
			}
		}
	}
	return NULL;
}


tDbaseHwAggEntry *adap_getLagInstanceFromIndex(ADAP_Uint16 u2Idx)
{
	if(u2Idx < MAX_NUM_OF_AGGR_ENTRY)
	{
		return &tAdapDatabase.aggrEntry[u2Idx];
	}
	return NULL;
}


tDbaseHwAggEntry *adap_getLagInstanceFromAggrId(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			return &tAdapDatabase.aggrEntry[Index];
		}
	}
	return NULL;
}

ADAP_Uint16 adap_DeleteLagInstace(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			tAdapDatabase.aggrEntry[Index].valid = ADAP_FALSE;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}
ADAP_Uint16 adap_CreateLagInstace(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			// allready exist
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_CreateLagInstace: aggrIndex:%d already found\r\n",u2HwAggIdx);
			return ADAP_END_OF_TABLE;
		}
	}
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if(tAdapDatabase.aggrEntry[Index].valid == ADAP_FALSE)
		{
			tAdapDatabase.aggrEntry[Index].valid = ADAP_TRUE;
			tAdapDatabase.aggrEntry[Index].swAggIdx = u2HwAggIdx;
			tAdapDatabase.aggrEntry[Index].selPolicy = 0;
			tAdapDatabase.aggrEntry[Index].numLagPorts=0;
			tAdapDatabase.aggrEntry[Index].hwlagId = ADAP_END_OF_TABLE;
			tAdapDatabase.aggrEntry[Index].portbitmask  = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_CreateLagInstace: index:%d aggrIdx:%d\r\n",Index,tAdapDatabase.aggrEntry[Index].swAggIdx);
			return Index;
		}
	}
	// not enough space
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_CreateLagInstace: not enough spaces\r\n");
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_SetLagVirtualPort(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 virtualPort)
{
	ADAP_Uint16 Index=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			tAdapDatabase.aggrEntry[Index].lagVirtualPort = virtualPort;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetLagVirtualPort: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ENET_FAILURE;
}


ADAP_Uint16 adap_GetLagVirtualPort(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;
	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			return tAdapDatabase.aggrEntry[Index].lagVirtualPort;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetLagVirtualPort: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_SetLagMasterPort(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 masterPort)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			tAdapDatabase.aggrEntry[Index].masterPort = masterPort;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetLagMasterPort: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ENET_FAILURE;
}

ADAP_Uint16 adap_GetLagMasterPort(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			return tAdapDatabase.aggrEntry[Index].masterPort;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetLagMasterPort: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_GetLagAggrFromMasterPort(ADAP_Uint16 masterPort)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].masterPort == masterPort))
		{
			return tAdapDatabase.aggrEntry[Index].swAggIdx;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_GetLagAggrFromMasterPort: masterPort:%d not found for aggr\r\n",masterPort);
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_SetEnetLagId(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 id)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			tAdapDatabase.aggrEntry[Index].hwlagId = id;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPL_SetEnetLagId: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ENET_FAILURE;
}

ADAP_Uint16 adap_GetEnetLagId(ADAP_Uint16 u2HwAggIdx)
{
	ADAP_Uint16 Index=0;

	for(Index=0;Index<MAX_NUM_OF_AGGR_ENTRY;Index++)
	{
		if((tAdapDatabase.aggrEntry[Index].valid == ADAP_TRUE) && (tAdapDatabase.aggrEntry[Index].swAggIdx == u2HwAggIdx))
		{
			return tAdapDatabase.aggrEntry[Index].hwlagId;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetEnetLagId: aggrIndex:%d not found\r\n",u2HwAggIdx);
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_getFreeVirtualPort(void)
{
	ADAP_Uint16 idx;

	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if(tAdapDatabase.lagVirtualPort[idx].free_virtual_port == 1)
		{
			tAdapDatabase.lagVirtualPort[idx].free_virtual_port=0;
			return tAdapDatabase.lagVirtualPort[idx].lagVirtualPort;
		}

	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_getFreeVirtualPort: not enough spaces\r\n");
	return ADAP_END_OF_TABLE;
}
ADAP_Uint16 adap_setFreeVirtualPort(ADAP_Uint16 virtualPort)
{
	ADAP_Uint16 idx;

	for(idx=0;idx<MAX_NUM_OF_AGGR_ENTRY;idx++)
	{
		if( (tAdapDatabase.lagVirtualPort[idx].free_virtual_port == 0) &&
			(tAdapDatabase.lagVirtualPort[idx].lagVirtualPort == virtualPort) )
		{
			tAdapDatabase.lagVirtualPort[idx].free_virtual_port=1;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_setFreeVirtualPort: virtual port:%d not found\r\n",virtualPort);
	return ENET_FAILURE;
}

ADAP_Uint16 adap_IsPortChannel(ADAP_Uint16 issPort)
{
	if(MeaAdapGetNumOfPhyPorts() <= issPort)
	{
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

void adap_SetMasterPort(ADAP_Uint32 u4IfIndex,ADAP_Uint16 masterPort)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetMasterPort port:%d out of range\n", u4IfIndex);
		return ;
	}
	tAdapDatabase.PortsTable[u4IfIndex].MasterLagPort = masterPort;
}

ADAP_Uint16 adap_GetMasterPort(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetMasterPort port:%d out of range\n", u4IfIndex);
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[u4IfIndex].MasterLagPort;
}

void adap_PortSetVirtualPort(ADAP_Uint32 u4IfIndex,ADAP_Uint16 lagVirtualPort)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_PortSetVirtualPort port:%d out of range\n", u4IfIndex);
		return ;
	}
	tAdapDatabase.PortsTable[u4IfIndex].lagVirtualPort = lagVirtualPort;
}

ADAP_Uint16 adap_PortGetVirtualPort(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_PortGetVirtualPort port:%d out of range\n", u4IfIndex);
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[u4IfIndex].lagVirtualPort;
}

void adap_PortSetEnetLagId(ADAP_Uint32 u4IfIndex,ADAP_Uint16 lagId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_PortSetEnetLagId port:%d out of range\n", u4IfIndex);
		return ;
	}
	tAdapDatabase.PortsTable[u4IfIndex].lagId = lagId;
}

ADAP_Uint16 adap_PortGetEnetLagId(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_PortGetEnetLagId port:%d out of range\n", u4IfIndex);
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[u4IfIndex].lagId;
}

ADAP_Int32 adap_SetDefaultSid(ADAP_Uint32 PortId, MEA_Service_t sId)
{
	tAdapDatabase.PortsTable[PortId].defSid = sId;
	return ENET_SUCCESS;
}

MEA_Service_t adap_GetDefaultSid(ADAP_Uint32 PortId)
{
	return tAdapDatabase.PortsTable[PortId].defSid;
}

ADAP_Int32 adap_SetPortPriorityMap(ADAP_Uint32 u4IfIndex, ADAP_Uint16 priMapProfileId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetPortPriorityMap port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[u4IfIndex].priorityMapProfileId = priMapProfileId;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_GetPortPriorityMap(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *priMapProfileId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetPortPriorityMap port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}

	*priMapProfileId = tAdapDatabase.PortsTable[u4IfIndex].priorityMapProfileId;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_SetPortCosPriority(ADAP_Uint32 u4IfIndex, ADAP_Uint16 priCosProfileId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetPortCosPriority port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[u4IfIndex].priorityCosProfileId = priCosProfileId;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_GetPortCosPriority(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *priCosProfileId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetPortCosPriority port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}

	*priCosProfileId = tAdapDatabase.PortsTable[u4IfIndex].priorityCosProfileId;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_SetFlowMarkingProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 flowMarkingProfile)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetFlowMarkingProfile port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[u4IfIndex].flowMarkingProfileId = flowMarkingProfile;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_GetFlowMarkingProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *flowMarkingProfile)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetFlowMarkingProfile port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}

	*flowMarkingProfile = tAdapDatabase.PortsTable[u4IfIndex].flowMarkingProfileId;
	return ENET_SUCCESS;
}

ADAP_Uint32 adap_AllocateFlowMarkingProfile (ADAP_Uint16 *AllocatedProfileId, ADAP_Uint32 PortNum)
{
	ADAP_Uint16 ProfileId;

    for (ProfileId = 1; ProfileId < MAX_NUM_OF_FLOW_MARKING_PROFILES; ProfileId++)
    {

         if(tAdapDatabase.FlowMarkingUseTable[ProfileId].isFree == ADAP_TRUE)
        {
        	 tAdapDatabase.FlowMarkingUseTable[ProfileId].isFree = ADAP_FALSE;
        	 tAdapDatabase.FlowMarkingUseTable[ProfileId].portNum = (ADAP_Uint16)PortNum;
        	 tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount++;
             *AllocatedProfileId = ProfileId;

     		if(adap_SetFlowMarkingProfile(PortNum, ProfileId) == ENET_FAILURE)
     		{
     			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: failed to set flowMarkProfile:%d for port:%d\n", ProfileId, PortNum);
     			return ENET_FAILURE;
     		}

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_AllocateFlowMarkingProfile: New ProfileId:%d allocated\n", *AllocatedProfileId);
            return ENET_SUCCESS;
        }
	}

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_AllocateFlowMarkingProfile ERROR: The Flow Marking Profiles table is full!!!\n");
    return ENET_FAILURE;
}

ADAP_Uint32 adap_ReuseFlowMarkingProfile (ADAP_Uint16 ProfileId)
{
    /* Is the Flow Marking Profile already in use? */
    if(tAdapDatabase.FlowMarkingUseTable[ProfileId].isFree != ADAP_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPLReuseFlowMarkingProfile ERROR: The ProfileId: %d doesn't exist!\n", ProfileId);
        return MEA_ERROR;
    }

    /* Increment the use count */
    tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount++;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_ReuseFlowMarkingProfile: The Profile:%d is used %d times\n",
            ProfileId, tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount);

    return ENET_SUCCESS;
}

MEA_Status adap_FreeFlowMarkingProfile (ADAP_Uint16 ProfileId)
{
    /* Is the Flow Marking Profile free already? */
    if(tAdapDatabase.FlowMarkingUseTable[ProfileId].isFree != ADAP_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPLFreeFlowMarkingProfile ERROR: The ProfileId: %d doesn't exist!\n", ProfileId);
        return MEA_ERROR;
    }

    /* Decrement the use count */
    tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount--;

    /* Is the Flow Marking Profile can be released in HW
     * (Use Count == 0 )? */
    if(tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount == 0 )
    {
 		if(adap_SetFlowMarkingProfile(tAdapDatabase.FlowMarkingUseTable[ProfileId].portNum, MEA_PLAT_GENERATE_NEW_ID) == ENET_FAILURE)
 		{
 			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: failed to set flowMarkProfile:%d for port:%d\n", ADAP_END_OF_TABLE, tAdapDatabase.FlowMarkingUseTable[ProfileId].portNum);
 			return ENET_FAILURE;
 		}
 		tAdapDatabase.FlowMarkingUseTable[ProfileId].isFree = ADAP_TRUE;
 		tAdapDatabase.FlowMarkingUseTable[ProfileId].portNum = ADAP_END_OF_TABLE;

        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"DPLFreeFlowMarkingProfile: ProfileId:%d Released\n", ProfileId);

        return MEA_OK;
    }
    /* The resource is still busy */

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"DPLFreeFlowMarkingProfile: ProfileId:%d Still in use (Use count:%d)\n",
            ProfileId, tAdapDatabase.FlowMarkingUseTable[ProfileId].UseCount);

    return MEA_BUSY;
}

void adap_SetTunnelTranslation(ADAP_Bool Enable)
{
	tAdapDatabase.tGlobals.L2TunnelingEnable = Enable;
}

ADAP_Bool adap_GetTunnelTranslation(void)
{
	return tAdapDatabase.tGlobals.L2TunnelingEnable;
}

ADAP_Uint32 adap_SetVlanPortTunnelType(ADAP_Uint32 PortId, ADAP_Uint16 PortTunnelType)
{
	if(PortId < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.PortsTable[PortId].portTunnelType = PortTunnelType;
	}

	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetVlanPortTunnelType(ADAP_Uint32 PortId)
{
	if(PortId < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return tAdapDatabase.PortsTable[PortId].portTunnelType;
	}
	return 0;
}

ADAP_Uint32 adap_SetPBPortServVlanCustomerVlan(ADAP_Uint32 PortId,ADAP_Uint16 ServiceVlan)
{
	if(PortId >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[PortId].StagForCustomerVlan = ServiceVlan;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetPBPortServVlanCustomerVlan(ADAP_Uint32 PortId)
{
	if(PortId >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[PortId].StagForCustomerVlan;
}

void adap_SetTunnelOption(ADAP_Uint16 PortIndex,eEnetHal_VlanHwTunnelFilters filterOption,ADAP_Uint8 value)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPLSetTunnelOption : port out of range:%d\n",PortIndex);
		return ;
	}
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"DPLSetTunnelOption issPort:%d protocol:%d status:%d\n",
				PortIndex,
				filterOption,
				value);

    tAdapDatabase.PortsTable[PortIndex].tunnelOption[filterOption] = value;
}

ADAP_Uint8 adap_GetTunnelOption(ADAP_Uint16 PortIndex, eEnetHal_VlanHwTunnelFilters filterOption)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetTunnelOption : port out of range:%d\n",PortIndex);
		return 0;
	}

	return tAdapDatabase.PortsTable[PortIndex].tunnelOption[filterOption];
}

ADAP_Uint32 adap_SetSpanningTreeMode (ADAP_Int16 issPort, eAdap_StpMode mode,ADAP_Uint8 status)
{

	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetSpanningTreeMode ERROR: NO issPort(%d) found!!\n",issPort);
		return ENET_FAILURE;
	}


	tAdapDatabase.PortsTable[issPort].stpMode = mode;
	tAdapDatabase.PortsTable[issPort].stpState = status;
	return ENET_SUCCESS;
}

ADAP_Uint32 adap_GetSpanningTreeMode (ADAP_Int16 issPort, eAdap_StpMode *mode,ADAP_Uint8 *status)
{
	if( issPort < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		(*mode) = tAdapDatabase.PortsTable[issPort].stpMode;
		(*status) = tAdapDatabase.PortsTable[issPort].stpState;
		return ENET_SUCCESS;

	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetSpanningTreeMode ERROR: NO issPort(%d) found!!\n",issPort);
    return ENET_FAILURE;
}

ADAP_Uint32 adap_SetCepPortState (ADAP_Int16 issPort, ADAP_Int16 vlanId, ADAP_Uint8 status)
{

	if((issPort >= MAX_NUM_OF_SUPPORTED_PORTS) || (vlanId >= ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetCepPortState ERROR: issPort:%d vlanId :%d!!\n",issPort, vlanId);
		return ENET_FAILURE;
	}


	tAdapDatabase.PortsTable[issPort].cepPortState[vlanId] = status;
	return ENET_SUCCESS;
}


ADAP_Uint32 adap_GetCepPortState (ADAP_Int16 issPort, ADAP_Int16 vlanId, ADAP_Uint8 *status)
{
	if(( issPort < MAX_NUM_OF_SUPPORTED_PORTS) && (vlanId < ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		(*status) = tAdapDatabase.PortsTable[issPort].cepPortState[vlanId];
		return ENET_SUCCESS;

	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetCepPortState ERROR: issPort:%d vlanId :%d!!\n",issPort, vlanId);
    return ENET_FAILURE;
}

ADAP_Uint8 adap_MstpGetState(ADAP_Uint32 u4IfIndex,ADAP_Uint16 VlanId)
{
	ADAP_Uint32 InstanceIdx;

	for(InstanceIdx=0;InstanceIdx<ADAP_MAX_NUM_OF_MSTP_INSTANCES;InstanceIdx++)
	{
		if(tAdapDatabase.MstpInstTable[InstanceIdx].MSTPInst_valid != 0)
		{
			if( (tAdapDatabase.MstpVlanTable[VlanId].isValid == 1) && (tAdapDatabase.MstpVlanTable[VlanId].MSTPInstId == InstanceIdx) )
			{
				return tAdapDatabase.MstpInstTable[InstanceIdx].port_state[u4IfIndex];
			}
		}
	}
	return ENET_AST_PORT_STATE_DISABLED;
}

void adap_MstpSetVlanInstanceParams(ADAP_Uint16 VlanId, ADAP_Bool isValid, ADAP_Uint16 u2InstanceId)
{
	if(VlanId < ADAP_NUM_OF_SUPPORTED_VLANS)
	{
		tAdapDatabase.MstpVlanTable[VlanId].isValid = isValid;
		tAdapDatabase.MstpVlanTable[VlanId].MSTPInstId = u2InstanceId;
	}
	return;
}

void adap_MstpSetInstanceParams(ADAP_Uint16 u2InstanceId, ADAP_Uint16 instValue, ADAP_Uint32 u4IfIndex, ADAP_Uint16 portState)
{
	if(u2InstanceId < ADAP_MAX_NUM_OF_MSTP_INSTANCES)
	{
		if(instValue != 0)
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_MstpSetInstanceParams: u2InstanceId: %d instValue:%d u4IfIndex:%d portState:%d\n", u2InstanceId, instValue, u4IfIndex, portState);
		tAdapDatabase.MstpInstTable[u2InstanceId].MSTPInst_valid = instValue;
		tAdapDatabase.MstpInstTable[u2InstanceId].port_state[u4IfIndex] = portState;
	}
	return;
}

tMstpVlanDb* adap_MstpGetVlanInstance(ADAP_Uint16 VlanId)
{
	if(VlanId < ADAP_NUM_OF_SUPPORTED_VLANS)
		return &tAdapDatabase.MstpVlanTable[VlanId];
	else
		return NULL;
}

tMstpInstDb* adap_MstpGetInstance(ADAP_Uint16 u2InstanceId)
{
	if(u2InstanceId < ADAP_MAX_NUM_OF_MSTP_INSTANCES)
		return &tAdapDatabase.MstpInstTable[u2InstanceId];
	else
		return NULL;
}

ADAP_Uint8 adap_MstpGetStateForVlan(ADAP_Uint32 u4IfIndex,ADAP_Uint16 VlanId)
{
	ADAP_Uint32 InstanceIdx;

	for(InstanceIdx=0;InstanceIdx<ADAP_MAX_NUM_OF_MSTP_INSTANCES;InstanceIdx++)
	{
		if(tAdapDatabase.MstpInstTable[InstanceIdx].MSTPInst_valid != 0)
		{
			if( (tAdapDatabase.MstpVlanTable[VlanId].isValid == 1) && (tAdapDatabase.MstpVlanTable[VlanId].MSTPInstId == InstanceIdx) )
			{
				return tAdapDatabase.MstpInstTable[InstanceIdx].port_state[u4IfIndex];
			}
		}
	}
	return ENET_AST_PORT_STATE_DISABLED;
}

ADAP_Uint8 adap_MstpGetStateForInstance(ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2InstanceId)
{
	ADAP_Uint16 vlanId=0;

	if(tAdapDatabase.MstpInstTable[u2InstanceId].MSTPInst_valid != 0)
	{
		for(vlanId=0;vlanId<ADAP_NUM_OF_SUPPORTED_VLANS;vlanId++)
		{
			if( (tAdapDatabase.MstpVlanTable[vlanId].isValid == ADAP_TRUE) && (tAdapDatabase.MstpVlanTable[vlanId].MSTPInstId == u2InstanceId) )
			{
				return tAdapDatabase.MstpInstTable[u2InstanceId].port_state[u4IfIndex];
			}
		}
	}
	return 0;
}

tPnacAutorizedDb *adap_getPnacAutorized(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.pnacAutorized[u4IfIndex];
	}
	return NULL;
}

void adap_setServiceVlanRelay(ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.svlanRelay[u4IfIndex].u2LocalSVlan = u2LocalSVlan;
		tAdapDatabase.svlanRelay[u4IfIndex].u2RelaySVlan = u2RelaySVlan;
	}
}

tSvlanRelayDb *adap_getServiceVlanRelay(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.svlanRelay[u4IfIndex];
	}
	return NULL;
}

void adap_SetSvlanMap(ADAP_Uint16 PortIndex,tEnetHal_VlanSVlanMap   *pVlanSVlanMap)
{
	if(PortIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		adap_memcpy(&tAdapDatabase.PortsTable[PortIndex].VlanSVlanMap,pVlanSVlanMap,sizeof(tEnetHal_VlanSVlanMap));
	}
}

tEnetHal_VlanSVlanMap *adap_GetSvlanMap(ADAP_Uint16 PortIndex)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetSvlanMap : port out of range:%d\n",PortIndex);
		return NULL;
	}

	return &tAdapDatabase.PortsTable[PortIndex].VlanSVlanMap;
}

ADAP_Uint16 adap_GetPBPortCustomerVlan(ADAP_Uint32 PortId)
{
	if(PortId >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[PortId].CustomerVlan;
}

void adap_SetPBPortCustomerVlan(ADAP_Uint32 PortId, ADAP_Uint16 CVlan)
{
	if(PortId < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		tAdapDatabase.PortsTable[PortId].CustomerVlan = CVlan;
	}
}

tEnetHal_VlanSVlanMap *adap_GetPointerSVlanClassTable(ADAP_Uint32 PortId, tEnetHal_VlanId sVlanId)
{
	ADAP_Uint16   		Index;
	tBridgeDomain 		*pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(sVlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(Index=0;Index<MAX_NUM_OF_SVLAN_PORT_TABLE;Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) &&
				(pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == PortId) &&
				(pBridgeDomain->SVlanClassTable[Index].selectedEntry == MEA_TRUE) )
			{
				Adap_bridgeDomain_semUnlock();
				return &pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();
	return NULL;
}

ADAP_Uint32 adap_SVlanClassTableUpdate(tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry)
{
	ADAP_Uint16 Index=0;
	tBridgeDomain 		*pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(pVlanSVlanMapEntry->SVlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if ( (pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) &&
				 (pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.CVlanId == pVlanSVlanMapEntry->CVlanId) &&
				 (pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == pVlanSVlanMapEntry->u2Port) )
			{
				adap_memcpy(&pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry,pVlanSVlanMapEntry,sizeof(tEnetHal_VlanSVlanMap));
				Adap_bridgeDomain_semUnlock();
				return ENET_SUCCESS;
			}
		}

		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if(pBridgeDomain->SVlanClassTable[Index].valid == MEA_FALSE)
			{
				adap_memcpy(&pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry,pVlanSVlanMapEntry,sizeof(tEnetHal_VlanSVlanMap));
				pBridgeDomain->SVlanClassTable[Index].valid=MEA_TRUE;

				//check who is the selected
				if(pBridgeDomain->SVlanClassTable[Index].selectedEntry == MEA_FALSE)
				{
					pBridgeDomain->SVlanClassTable[Index].selectedEntry = MEA_TRUE;
				}
				else
				{
					//check if I'm PepPvid
					if(pBridgeDomain->pepPvid[pVlanSVlanMapEntry->u2Port] == pVlanSVlanMapEntry->CVlanId)
					{
						adap_VlanClassTableForceSelection(pBridgeDomain, pVlanSVlanMapEntry);
					}
				}
				Adap_bridgeDomain_semUnlock();
				return ENET_SUCCESS;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DPLSVlanClassTableUpdate ERROR: iss_SVlanClassTable is FULL!!!\n");
	return ENET_FAILURE;
}

ADAP_Uint32 adap_VlanClassTableForceSelection(tBridgeDomain *pBridgeDomain, tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry)
{
	ADAP_Uint16   Index;
	if(pBridgeDomain != NULL)
	{
		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) &&
				(pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == pVlanSVlanMapEntry->u2Port) )
			{
				pBridgeDomain->SVlanClassTable[Index].selectedEntry = MEA_FALSE;
			}
		}

		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) &&
				(pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == pVlanSVlanMapEntry->u2Port) &&
				(pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.CVlanId == pVlanSVlanMapEntry->CVlanId) )
			{
				pBridgeDomain->SVlanClassTable[Index].selectedEntry = MEA_TRUE;
				return ENET_SUCCESS;
			}
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_SVlanClassTableDelete(tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry)
{
	ADAP_Uint16       Index;
	tBridgeDomain 		*pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(pVlanSVlanMapEntry->SVlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) &&
				(pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.CVlanId == pVlanSVlanMapEntry->CVlanId) )
			{
				pBridgeDomain->SVlanClassTable[Index].valid = MEA_FALSE;
				Adap_bridgeDomain_semUnlock();
				return ENET_SUCCESS;
			}
		}
	}
	Adap_bridgeDomain_semUnlock();

    return ENET_FAILURE;
 }

ADAP_Uint16 adap_SVlanClassTableGetFirst(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId)
{
	ADAP_Uint16       	Index=0;
	tBridgeDomain 		*pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(SVlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(Index = 0; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == PortId) &&
				(pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) )
			{
				Adap_bridgeDomain_semUnlock();
				return Index;
			}
		}
	}
    Adap_bridgeDomain_semUnlock();
    return ADAP_END_OF_TABLE;
}

tEnetHal_VlanId adap_SVlanClassTableGetFromIndex(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId,ADAP_Uint8 *u1PepUntag,ADAP_Uint8 *u1CepUntag,ADAP_Uint16 currIndex)
{
	tBridgeDomain 		*pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(SVlanId,0);

	if(pBridgeDomain != NULL)
	{
		if(pBridgeDomain->SVlanClassTable[currIndex].valid == MEA_TRUE)
		{
			(*u1CepUntag) = pBridgeDomain->SVlanClassTable[currIndex].VlanSVlanMapEntry.u1CepUntag;
			(*u1PepUntag) = pBridgeDomain->SVlanClassTable[currIndex].VlanSVlanMapEntry.u1PepUntag;
			Adap_bridgeDomain_semUnlock();
			return pBridgeDomain->SVlanClassTable[currIndex].VlanSVlanMapEntry.CVlanId;
		}
	}
	Adap_bridgeDomain_semUnlock();
	return ADAP_END_OF_TABLE;
}

ADAP_Uint16 adap_SVlanClassTableGetNext(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId,ADAP_Uint16 currIndex)
{
	ADAP_Uint32    		Index=0;
	tBridgeDomain 		*pBridgeDomain=NULL;

	currIndex++;

	if(currIndex >= MAX_NUM_OF_SVLAN_PORT_TABLE)
	{
		return ADAP_END_OF_TABLE;
	}

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(SVlanId,0);

	if(pBridgeDomain != NULL)
	{
		for(Index = currIndex; Index < MAX_NUM_OF_SVLAN_PORT_TABLE; Index++)
		{
			if( (pBridgeDomain->SVlanClassTable[Index].VlanSVlanMapEntry.u2Port == PortId) &&
				(pBridgeDomain->SVlanClassTable[Index].valid == MEA_TRUE) )
			{
				 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"index:%d\n",Index);
				 Adap_bridgeDomain_semUnlock();
				return Index;
			}
		}
	}
    Adap_bridgeDomain_semUnlock();
    return ADAP_END_OF_TABLE;
}

ADAP_Int32 adap_GetFlowPolicerProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *profileId)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetFlowPolicerProfile port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}

	*profileId = tAdapDatabase.PortsTable[u4IfIndex].flowPolicerProfileId;
	if(*profileId == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetFlowPolicerProfile not initialized flowPolicer Id\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 adap_SetFlowPolicerProfile(ADAP_Uint32 ifIndex, ADAP_Uint16 profileId)
{
	if(ifIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetFlowPolicerProfile : port out of range:%d\n",ifIndex);
		return ENET_FAILURE;
	}

	tAdapDatabase.PortsTable[ifIndex].flowPolicerProfileId = profileId;
	return ENET_SUCCESS;
}

tPcpConfiguration *adap_GetPcpConfiguration(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.pcpConfig[u4IfIndex];
	}
	return NULL;
}

tPbPcpInfoPri *adap_GetIngressPcpInfo(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		return &tAdapDatabase.ingressPcpInfo[u4IfIndex];
	}
	return NULL;
}

ADAP_Uint32 adap_SetTunnelCusToProActionId(ADAP_Uint16 PortIndex,MEA_Action_t id)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetTunnelCusToProActionId : port out of range:%d\n",PortIndex);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[PortIndex].tunnelCusToProAction_id = id;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetTunnelCusToProActionId(ADAP_Uint16 PortIndex)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetTunnelCusToProActionId : port out of range:%d\n",PortIndex);
		return 0;
	}
	return tAdapDatabase.PortsTable[PortIndex].tunnelCusToProAction_id;
}
ADAP_Uint32 adap_SetTunnelProToCusActionId(ADAP_Uint16 PortIndex,MEA_Action_t id)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetTunnelProToCusActionId : port out of range:%d\n",PortIndex);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[PortIndex].tunnelProToCusAction_id = id;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetTunnelProToCusActionId(ADAP_Uint16 PortIndex)
{
	if(PortIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetTunnelProToCusActionId : port out of range:%d\n",PortIndex);
		return 0;
	}
	return tAdapDatabase.PortsTable[PortIndex].tunnelProToCusAction_id;
}

ADAP_Int32 adap_SetDefaultPolicerId(ADAP_Uint16 DefaultPolicerId,ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetDefaultPolicerId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[issPort].defMeter.policer_prof_id = DefaultPolicerId;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetDefaultPolicerId(ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetDefaultPolicerId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	return tAdapDatabase.PortsTable[issPort].defMeter.policer_prof_id;
}

ADAP_Int32 adap_SetDefaultPolicerIdTmId(ADAP_Uint16 tmId,ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetDefaultPolicerIdTmId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[issPort].defMeter.tm_Id = tmId;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetDefaultPolicerIdTmId(ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetDefaultPolicerIdTmId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	return tAdapDatabase.PortsTable[issPort].defMeter.tm_Id;
}

ADAP_Int32 adap_SetDefaultPolicerIdPmId(ADAP_Uint16 pmId,ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetDefaultPolicerIdPmId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[issPort].defMeter.pmDb[0].pmId = pmId;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_GetDefaultPolicerIdPmId(ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetDefaultPolicerIdPmId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	return tAdapDatabase.PortsTable[issPort].defMeter.pmDb[0].pmId;
}

ADAP_Int32 adap_SetDefaultMappingId(ADAP_Uint16 mappingId,ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetDefaultMappingId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	tAdapDatabase.PortsTable[issPort].defMeter.mappingEditingIdx = mappingId;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetDefaultMappingId(ADAP_Uint16 issPort)
{
	if(issPort >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetDefaultMappingId : port out of range:%d\n",issPort);
		return ENET_FAILURE;
	}
	return tAdapDatabase.PortsTable[issPort].defMeter.mappingEditingIdx;
}

ADAP_Int32 adap_SetDefaultFlowMarkingId(ADAP_Uint16 markingId)
{
	tAdapDatabase.tGlobals.default_flowMarkingID = markingId;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetDefaultFlowMarkingId()
{
	return tAdapDatabase.tGlobals.default_flowMarkingID;
}

ADAP_Int32 adap_SetDefaultCosMappingId(ADAP_Uint16 mappingId)
{
	tAdapDatabase.tGlobals.default_cosMappingID = mappingId;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_GetDefaultCosMappingId()
{
	return tAdapDatabase.tGlobals.default_cosMappingID;
}

ADAP_Uint16 adap_SetEditMapping(MEA_EditingMappingProfile_Id_t      id)
{
	ADAP_Uint16 idx=0;

	for(idx=0;idx<ADAP_MAX_NUMBER_OF_VLAN_EDITING_MAPPING;idx++)
	{
		if(tAdapDatabase.editMapping[idx].valid == MEA_FALSE)
		{
			tAdapDatabase.editMapping[idx].valid = MEA_TRUE;
			tAdapDatabase.editMapping[idx].id = id;
			return idx;
		}
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}

ADAP_Uint16 adap_getEditMappingByIdx(ADAP_Uint16 idx)
{

	if(tAdapDatabase.editMapping[idx].valid == MEA_TRUE)
	{
		return tAdapDatabase.editMapping[idx].id;
	}
	return MEA_PLAT_GENERATE_NEW_ID;
}
ADAP_Uint32 adap_freeEditMappingByIdx(ADAP_Uint16 idx)
{

	if(tAdapDatabase.editMapping[idx].valid == MEA_TRUE)
	{
		tAdapDatabase.editMapping[idx].valid=MEA_FALSE;
		tAdapDatabase.editMapping[idx].id=ENET_PLAT_GENERATE_NEW_ID;
		return ENET_SUCCESS;
	}
	return ENET_FAILURE;
}

void adap_SetMeterIndexPerPort(ADAP_Uint32 u4IfIndex,ADAP_Int32 meterIndex)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetMeterIndexPerPort port:%d out of range\n", u4IfIndex);
		return ;
	}
	tAdapDatabase.PortsTable[u4IfIndex].meterIndex = meterIndex;
}

ADAP_Int32 adap_GetMeterIndexPerPort(ADAP_Uint32 u4IfIndex)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetMeterIndexPerPort port:%d out of range\n", u4IfIndex);
		return ADAP_END_OF_TABLE;
	}
	return tAdapDatabase.PortsTable[u4IfIndex].meterIndex;
}
ADAP_Uint32 adap_VlanGetPortByVlanId (tEnetHal_VlanId VlanId)
{
        ADAP_Uint32                     PortNumber=0;

        tBridgeDomain *pBridgeDomain=NULL;

        Adap_bridgeDomain_semLock();
        pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId, 0);

        if(pBridgeDomain != NULL)
        {
                PortNumber = pBridgeDomain->ifIndex[0];
        }
        Adap_bridgeDomain_semUnlock();
        return PortNumber;
}

void adap_SetTrafficBlockStatusPerPort(ADAP_Uint32 u4IfIndex,ADAP_Bool blockStatus)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetTrafficBlockStatusPerPort port:%d out of range\n", u4IfIndex);
		return ;
	}
	tAdapDatabase.PortsTable[u4IfIndex].lacpTrafficBlockStatus = blockStatus;
}

ADAP_Int32 adap_GetTrafficBlockStatusPerPort(ADAP_Uint32 u4IfIndex, ADAP_Bool *blockStatus)
{
	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetTrafficBlockStatusPerPort port:%d out of range\n", u4IfIndex);
		return ENET_FAILURE;
	}
	*blockStatus = tAdapDatabase.PortsTable[u4IfIndex].lacpTrafficBlockStatus;
	return ENET_SUCCESS;
}

ADAP_Int32 adap_SetMirroringStatus(ADAP_Uint8 MirroringStatus )
{
	tAdapDatabase.tGlobals.mirroringStatus = MirroringStatus;

	return ENET_SUCCESS;
}

ADAP_Uint8 adap_GetMirroringStatus(void)
{
	return tAdapDatabase.tGlobals.mirroringStatus;
}

void AdapSetNATRouterStatus(int status)
{
	tAdapDatabase.NATRouterStatus = status;
}

int AdapGetNATRouterStatus(void)
{
	return tAdapDatabase.NATRouterStatus;
}

// Wred db functions
MEA_WredProfileId_t AdapGetWredProfileByIdx(ADAP_Uint16 Idx)
{
    return tAdapDatabase.WredProfTable[Idx].WredProfileId;
}

void AdapSetWredProfileByIdx(MEA_WredProfileId_t WredProfileID, ADAP_Uint16 Idx)
{
    tAdapDatabase.WredProfTable[Idx].WredProfileId = WredProfileID;
}

ADAP_Bool AdapIsWredProfileInUse(ADAP_Uint16 Idx)
{
    if(tAdapDatabase.WredProfTable[Idx].UseCount == 0)
        return ENET_FALSE;
    else
        return ENET_TRUE;
}

ADAP_Uint32 AdapIncWredProfileUseCount(ADAP_Uint16 Idx)
{
    tAdapDatabase.WredProfTable[Idx].UseCount++;
    return ENET_SUCCESS;
}

void AdapClearAllWredProfileUseCount(void)
{
    unsigned int WredProfileIdx;

    for(WredProfileIdx=0; WredProfileIdx<MAX_NUM_OF_WRED_PROFILES; WredProfileIdx++)
    {
        tAdapDatabase.WredProfTable[WredProfileIdx].UseCount = 0;
    }
}

void AdapClearAllArpTable ()
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct dest_ipv4_route_entry input;
	struct dest_ipv4_route_entry *ipv4NextHop;

	status = adap_rbtree_iter(tAdapDatabase.adp_nextHopIpPool, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		ipv4NextHop = container_of(rbtree_elem, struct dest_ipv4_route_entry, RbNode);

		input.entry.ipAddr = ipv4NextHop->entry.ipAddr;

		status = adap_rbtree_remove(tAdapDatabase.adp_nextHopIpPool, &input.RbNode, NULL);
		if (status == ENETHAL_STATUS_SUCCESS) {
			adap_safe_free(ipv4NextHop);
		}

		MeaDrvHwDeleteDestIpForwarder(input.entry.ipAddr, D_IP_MASK_32,START_L3_VPN);
		/* For double tagged interface, entry can be present with VPN1, hence deleting for it */
		MeaDrvHwDeleteDestIpForwarder(input.entry.ipAddr, D_IP_MASK_32,START_L3_VPN1);

		status = adap_rbtree_iter(tAdapDatabase.adp_nextHopIpPool, &input.RbNode, &rbtree_elem);
	}
}

ADAP_Uint32 AdapCheckFailedArp (ADAP_Uint32 IpAddr)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUM_OF_FAILED_PPP_ARP;idx++)
    {
        if((tAdapDatabase.FailedPPPArp[idx].valid==ADAP_TRUE) &&
           (tAdapDatabase.FailedPPPArp[idx].IpAddr == IpAddr))
        {
            return ADAP_TRUE;
        }
    }
    return ADAP_FALSE;
}

 
ADAP_Uint32 AdapAddFailedArp (ADAP_Uint32 IpAddr)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUM_OF_FAILED_PPP_ARP;idx++)
    {
        if((tAdapDatabase.FailedPPPArp[idx].valid==ADAP_TRUE) &&
           (tAdapDatabase.FailedPPPArp[idx].IpAddr == IpAddr))
        {
            return ADAP_TRUE;
        }
    }

    for(idx=0;idx<MAX_NUM_OF_FAILED_PPP_ARP;idx++)
    {
        if(tAdapDatabase.FailedPPPArp[idx].valid==ADAP_FALSE)
        {
            tAdapDatabase.FailedPPPArp[idx].valid = ADAP_TRUE;
            tAdapDatabase.FailedPPPArp[idx].IpAddr = IpAddr;
            return ADAP_TRUE;
        }
    }

    return ADAP_FALSE;
}

ADAP_Uint32 AdapRemoveFailedArp (ADAP_Uint32 IpAddr)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MAX_NUM_OF_FAILED_PPP_ARP;idx++)
    {
        if((tAdapDatabase.FailedPPPArp[idx].valid==ADAP_TRUE) &&
           (tAdapDatabase.FailedPPPArp[idx].IpAddr == IpAddr))
        {
            tAdapDatabase.FailedPPPArp[idx].valid = ADAP_FALSE;
            return ADAP_TRUE;
        }
    }
    return ADAP_FALSE;
}

ADAP_Uint32 AdapGetOrAddMtuProfForVlan(ADAP_Uint32 vlanId, ADAP_Uint16 *mtuProfId)
{
    ADAP_Uint32 idx=0;

    for(idx=0;idx<MEA_MAX_ACT_MTU_ID;idx++)
    {
        if((tAdapDatabase.mtuFlowProfDb[idx].valid==ADAP_TRUE) &&
           (tAdapDatabase.mtuFlowProfDb[idx].vlanId == vlanId))
        {
            *mtuProfId = tAdapDatabase.mtuFlowProfDb[idx].mtuProfId;
            return ENET_SUCCESS;
        }
    }

    for(idx=0;idx<MEA_MAX_ACT_MTU_ID;idx++)
    {
        if(tAdapDatabase.mtuFlowProfDb[idx].valid==ADAP_FALSE)
        {
            tAdapDatabase.mtuFlowProfDb[idx].valid = ADAP_TRUE;
            tAdapDatabase.mtuFlowProfDb[idx].vlanId = vlanId;
            *mtuProfId = tAdapDatabase.mtuFlowProfDb[idx].mtuProfId;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

ADAP_Uint32 AdapGetRemoveMtuProf(ADAP_Uint16 mtuProfId)
{
    int idx;
    for(idx=0;idx<MEA_MAX_ACT_MTU_ID;idx++)
     {
         if ((tAdapDatabase.mtuFlowProfDb[idx].valid==ADAP_TRUE) &&
             (tAdapDatabase.mtuFlowProfDb[idx].mtuProfId == mtuProfId))
         {
             tAdapDatabase.mtuFlowProfDb[idx].valid = ADAP_FALSE;
             return ENET_SUCCESS;
         }
     }
    return ENET_FAILURE;
}

/* MAC limiter section */
void AdapInitLimiterProfDb()
{
    int idx;
    for(idx=0;idx<MAX_NUM_OF_LIMITER_PROFILE;idx++)
    {
        memset(&tAdapDatabase.limiterProfDb[idx], 0 , sizeof(tAdapDatabase.limiterProfDb[idx]));
        tAdapDatabase.limiterProfDb[idx].valid = ADAP_FALSE;
        tAdapDatabase.limiterProfDb[idx].type = LIMITER_PROF_UNSET;
    }
}

ADAP_Uint32 AdapAllocLimiterProf(ADAP_Uint32 vlanId, ADAP_Uint32 ifIndex, eLimiterProfType type, MEA_Limiter_t limiterId)
{
    int idx;
    for(idx=0;idx<MAX_NUM_OF_LIMITER_PROFILE;idx++)
    {
        if (tAdapDatabase.limiterProfDb[idx].valid==ADAP_FALSE)
        {
            tAdapDatabase.limiterProfDb[idx].limiterId = limiterId;
            tAdapDatabase.limiterProfDb[idx].type = type;
            tAdapDatabase.limiterProfDb[idx].vlanId = vlanId;
            tAdapDatabase.limiterProfDb[idx].ifIndex = ifIndex;
            tAdapDatabase.limiterProfDb[idx].valid = ADAP_TRUE;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

ADAP_Uint32 AdapGetDefaultLimiterId(MEA_Limiter_t *limiterId)
{
    int idx;
    for(idx=0;idx<MAX_NUM_OF_LIMITER_PROFILE;idx++)
    {
        if ((tAdapDatabase.limiterProfDb[idx].valid==ADAP_TRUE) &&
            (tAdapDatabase.limiterProfDb[idx].type == LIMITER_PROF_DEFAULT))
        {
            *limiterId = tAdapDatabase.limiterProfDb[idx].limiterId;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

ADAP_Uint32 AdapGetLimiterIdByVlan(ADAP_Uint32 vlanId, MEA_Limiter_t *limiterId)
{
    int idx;
    for(idx=0;idx<MAX_NUM_OF_LIMITER_PROFILE;idx++)
    {
        if ((tAdapDatabase.limiterProfDb[idx].valid == ADAP_TRUE) &&
            (tAdapDatabase.limiterProfDb[idx].type == LIMITER_PROF_VLAN) &&
            (tAdapDatabase.limiterProfDb[idx].vlanId == vlanId))
        {
            *limiterId = tAdapDatabase.limiterProfDb[idx].limiterId;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

ADAP_Uint32 AdapGetLimiterIdByPort(ADAP_Uint32 ifIndex, MEA_Limiter_t *limiterId)
{
    int idx;
    for(idx=0;idx<MAX_NUM_OF_LIMITER_PROFILE;idx++)
    {
        if ((tAdapDatabase.limiterProfDb[idx].valid==ADAP_TRUE) &&
            (tAdapDatabase.limiterProfDb[idx].type == LIMITER_PROF_IFINDEX) &&
            (tAdapDatabase.limiterProfDb[idx].ifIndex == ifIndex))
        {
            *limiterId = tAdapDatabase.limiterProfDb[idx].limiterId;
            return ENET_SUCCESS;
        }
    }

    return ENET_FAILURE;
}

/* Arp filter parameters section */

void AdapInitArpFilterParams()
{
    int i;
    memset(tAdapDatabase.arpFilterParams, 0,
           sizeof(tAdapDatabase.arpFilterParams[0])*MAX_NUM_OF_ARP_FILTERS);

    for (i=0; i<MAX_NUM_OF_ARP_FILTERS; i++)
    {
        tAdapDatabase.arpFilterParams[i].valid = ADAP_FALSE;
    }
}

ADAP_Uint32 AdapGetArpFilterParams(ADAP_Uint32 vlanId, ADAP_Int32 *filterId, ADAP_Int32 *filterPriority)
{
    int i;

    for (i=0; i<MAX_NUM_OF_ARP_FILTERS; i++)
    {
        if ((tAdapDatabase.arpFilterParams[i].valid == ADAP_TRUE)
            && (tAdapDatabase.arpFilterParams[i].vlanId == vlanId))
        {
            *filterPriority = tAdapDatabase.arpFilterParams[i].filterPriority;
            *filterId = tAdapDatabase.arpFilterParams[i].filterId;
            return ENET_SUCCESS;
        }
    }
    return ENET_FAILURE;
}

ADAP_Uint32 AdapCreateArpFilterParams(ADAP_Uint32 vlanId, ADAP_Int32 filterId, ADAP_Int32 filterPriority)
{
    int i;

    for (i=0; i<MAX_NUM_OF_ARP_FILTERS; i++)
    {
        if (tAdapDatabase.arpFilterParams[i].valid == ADAP_FALSE)
        {
            tAdapDatabase.arpFilterParams[i].valid = ADAP_TRUE;
            tAdapDatabase.arpFilterParams[i].vlanId = vlanId;
            tAdapDatabase.arpFilterParams[i].filterId = filterId;
            tAdapDatabase.arpFilterParams[i].filterPriority = filterPriority;
            return ENET_SUCCESS;
        }
    }
    return ENET_FAILURE;
}

ADAP_Uint32 AdapRemoveArpFilterParams(ADAP_Uint32 vlanId)
{
    int i;

    for (i=0; i<MAX_NUM_OF_ARP_FILTERS; i++)
    {
        if ((tAdapDatabase.arpFilterParams[i].valid == ADAP_TRUE)
          && (tAdapDatabase.arpFilterParams[i].vlanId == vlanId))
        {
            tAdapDatabase.arpFilterParams[i].valid = ADAP_FALSE;
            return ENET_SUCCESS;
        }
    }
    return ENET_FAILURE;
}
