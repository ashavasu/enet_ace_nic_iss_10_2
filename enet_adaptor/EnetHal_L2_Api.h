/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ENETHAL_L2_API_H
#define ENETHAL_L2_API_H

/* This file contains type definitions and prototypes for all ENET HAL Layer 2 external APIs */

#define   ARP_PROTOCOL                    0x0806
typedef ADAP_Uint16 tEnetHal_VlanId;

typedef struct ENETHAL_COUNTER64_TYPE {
	ADAP_Uint32  msn;
	ADAP_Uint32  lsn;
} tEnetHal_COUNTER64_TYPE;

typedef union {
	ADAP_Uint64 val;
	tEnetHal_COUNTER64_TYPE s;
} tEnetHal_counter_64;

typedef struct EnetHwPortArray {
	ADAP_Uint32  *pu4PortArray;
	ADAP_Int32   i4Length;
} tEnetHal_HwPortArray;


/* ACL part */

typedef enum {

   ENETHAL_TOS_NONE = 0,
   ENETHAL_TOS_HI_REL,
   ENETHAL_TOS_HI_THR,
   ENETHAL_TOS_HI_REL_HI_THR,
   ENETHAL_TOS_LO_DEL,
   ENETHAL_TOS_LO_DEL_HI_REL,
   ENETHAL_TOS_LO_DEL_HI_THR,
   ENETHAL_TOS_LO_DEL_HI_THR_HI_REL,
   ENETHAL_TOS_INVALID
}eEnetHal_Tos;

/* Vlan Action */
typedef enum {

   ENETHAL_NONE = 0,
   ENETHAL_MODIFY_VLAN,
   ENETHAL_NESTED_VLAN,
   ENETHAL_STRIP_OUTER_HDR
}eEnetHal_SubAction;

typedef enum {

   ENETHAL_ALLOW = 1,
   ENETHAL_DROP,
   ENETHAL_REDIRECT_TO,
   ENETHAL_SWITCH_COPYTOCPU,
   ENETHAL_DROP_COPYTOCPU,
   ENETHAL_FORQOS,
   ENETHAL_TOMIRRORPORT,
   ENETHAL_AND,
   ENETHAL_OR,
   ENETHAL_NOT
}eEnetHal_FilterAction;

typedef enum {

	ENETHAL_ICMP = 1,
	ENETHAL_IGMP = 2,
	ENETHAL_GGP = 3,
	ENETHAL_IP = 4,
	ENETHAL_TCP = 6,
	ENETHAL_EGP = 8,
	ENETHAL_IGP = 9,
	ENETHAL_NVP = 11,
	ENETHAL_UDP = 17,
	ENETHAL_IRTP = 28,
	ENETHAL_IDPR = 35,
	ENETHAL_RSVP = 46,
	ENETHAL_MHRP = 48,
	ENETHAL_ICMP6 = 58,
	ENETHAL_IGRP = 88,
	ENETHAL_OSPFIGP = 89,
	ENETHAL_PROTO_ANY = 255

}eEnetHal_FilterProtocol;

typedef struct {

	ADAP_Int32                 filterNoId;
	ADAP_Int32                 filterPriority;
	ADAP_Uint32                filterProtocolType;
	ADAP_Uint32                filterCustomerVlanId;
	ADAP_Uint32                filterServiceVlanId;
	ADAP_Uint32                filterMatchCount;
	ADAP_Uint32                filterRedirectPort;
	ADAP_Uint32                u4StatsTransitFlag;

	eEnetHal_FilterAction  		FilterAction;
	tEnetHal_MacAddr            L2FilterDstMacAddr;
	tEnetHal_MacAddr            L2FilterSrcMacAddr;
	ADAP_Uint8         			InPortList[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint8         			OutPortList[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint16               	u2InnerEtherType;
	ADAP_Uint16                	u2OuterEtherType;
	ADAP_Int32                 	FilterCVlanPriority;
	ADAP_Int32                 	FilterSVlanPriority;
	ADAP_Uint32                	FilterTagType;


	ADAP_Int32                 	DropPrecedence;
	ADAP_Int32                 	CfiDei;
	ADAP_Uint32                	FilterUserPriority;

	// Redirection params
	ADAP_Uint32                 RedirEgressIfIndex;
	ADAP_Uint16                 L2SubActionId;
	eEnetHal_SubAction          L2SubAction;
	ADAP_Uint8             		L2FilterStatus;

    ADAP_Uint64                FilterDstMacAddrMask;
    ADAP_Uint64                FilterSrcMacAddrMask;
}tEnetHal_L2FilterEntry;

typedef struct  {


	ADAP_Int32                 	filterNoId;
    ADAP_Int32                 	filterPriority;
    eEnetHal_FilterProtocol   	L3FilterProtocol;

    ADAP_Int32                 FilterMessageType;
    ADAP_Int32                 FilterMessageCode;
    ADAP_Uint32                CVlanId;
    ADAP_Uint32                SVlanId;
    ADAP_Uint32                FilterDstIpAddr;
    ADAP_Uint32                FilterSrcIpAddr;
    ADAP_Uint32                FilterDstIpAddrMask;
    ADAP_Uint32                FilterSrcIpAddrMask;
    ADAP_Uint32                FilterMinDstProtPort;
    ADAP_Uint32                FilterMaxDstProtPort;
    ADAP_Uint32                FilterMinSrcProtPort;
    ADAP_Uint32                FilterMaxSrcProtPort;
    ADAP_Uint32                	FilterRedirectPort;
    eEnetHal_Tos           	   	FilterTos;
    ADAP_Int32                	FilterDscp;
    eEnetHal_FilterAction  		FilterAction;

	ADAP_Uint8         			InPortList[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint8         			OutPortList[MAX_NUM_OF_SUPPORTED_PORTS];

	tEnetHal_IPv6Addr             	ipv6SrcIpAddress;
	tEnetHal_IPv6Addr             	ipv6DstIpAddress;

   ADAP_Int32                 	CVlanPriority;
   ADAP_Int32                 	SVlanPriority;
   ADAP_Uint32                	FilterTagType;

   // Redirection params
   ADAP_Uint32                 RedirEgressIfIndex;
   ADAP_Uint16                 L3SubActionId;
   eEnetHal_SubAction          L3SubAction;
   ADAP_Uint8             	   L3FilterStatus;
}tEnetHal_L3FilterEntry;

#define		ENETHAL_CREATE_ENET_FILTER			1
#define		ENETHAL_MODIFY_ENET_FILTER			2
#define		ENETHAL_DELETE_ENET_FILTER			3

ADAP_Int32 EnetHal_HwUpdateL2Filter (tEnetHal_L2FilterEntry * pL2FilterEntry, ADAP_Int32 i4Value);
ADAP_Int32 EnetHal_HwUpdateL3Filter (tEnetHal_L3FilterEntry * pL3FilterEntry, ADAP_Int32 i4Value);

/* Bridge part */
ADAP_Int8 EnetHal_FsBrgSetAgingTime (ADAP_Int32 i4AgingTime);
ADAP_Int8 EnetHal_FsBrgFindFdbMacEntry (tEnetHal_MacAddr *macAddr, ADAP_Uint32 *pu4IfIndex);
ADAP_Int8 EnetHal_FsBrgGetFdbEntryFirst (tEnetHal_MacAddr *macAddr);
ADAP_Int8 EnetHal_FsBrgGetFdbNextEntry(tEnetHal_MacAddr *nextMacAddr);
ADAP_Int8 EnetHal_FsBrgFlushPort (ADAP_Uint32 u4IfIndex);
ADAP_Int8 EnetHal_FsBrgAddMacEntry (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *macAddr);
ADAP_Int8 EnetHal_FsBrgDelMacEntry (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *macAddr);
ADAP_Int8 EnetHal_FsBrgGetLearnedEntryDisc (ADAP_Uint32 *pu4Val);

/* CFA part */
typedef int (*EnetHal_rx_packet_callback_func)(ADAP_Uint32 logport,void *Buf,ADAP_Uint32 len);
void register_rx_packet_callback(EnetHal_rx_packet_callback_func func);

typedef int (*EnetHal_linkstatus_callback_func)(ADAP_Uint32 u4IfIndex, ADAP_Uint8 status);

typedef struct
{
    ADAP_Uint32    u4IfIndex; /* Interface Index of the port */
    tEnetHal_MacAddr MacAddr;   /* Mac Address of the given port*/
    ADAP_Uint8    u1Opcode;  /* This can take  CFA_SET_TO_WAN/CFA_RESET_TO_LAN.
                           Based on this value the interface will be configured
                           either as WAN or LAN  */
}tEnetHal_IfWanInfo;

ADAP_Int32 EnetHal_CfaRegisterWithNpDrv (EnetHal_rx_packet_callback_func func);
ADAP_Int32 EnetHal_FsCfaHwClearStats (ADAP_Uint32 u4Port);
ADAP_Int32 EnetHal_FsHwUpdateAdminStatusChange (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1AdminEnable);
ADAP_Int32 EnetHal_CfaHwL3VlanIntfWrite (ADAP_Uint8 *pu1DataBuf, ADAP_Uint32 u4PktSize,tEnetHal_VlanId VlanId,
					ADAP_Uint32 ifIndex, ADAP_Uint16 u2InnerVlan, ADAP_Uint16 u2EtherType);
ADAP_Int32 EnetHal_FsCfaHwSetMtu (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4MtuSize);
ADAP_Int32 EnetHal_FsCfaHwSetMacAddr (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *PortMac);
ADAP_Int32 EnetHal_FsHwGetStat (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, ADAP_Uint32 *pu4Value);
ADAP_Int32 EnetHal_FsHwGetStat64 (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, tEnetHal_COUNTER64_TYPE * pPortValue);
ADAP_Int32 EnetHal_FsHwGetPortStat64 (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, tEnetHal_COUNTER64_TYPE * pValue);
ADAP_Uint8 EnetHal_CfaNpGetLinkStatus (ADAP_Uint32 u4IfIndex);
ADAP_Int32 EnetHal_CfaRegisterLinkStatus (EnetHal_linkstatus_callback_func func);
ADAP_Uint8 EnetHal_CfaNpGetPhyAndLinkStatus (ADAP_Uint16 u2IfIndex);
ADAP_Int32 EnetHal_FsCfaHwSetWanTye (tEnetHal_IfWanInfo * pCfaWanInfo);
void EnetHal_CfaNpUpdateSwitchMac (tEnetHal_MacAddr *pSwitchMac);

/* CFM/ECFM part */
typedef struct  {
	ADAP_Uint16 u2VlanId;
	ADAP_Uint8 u1TagType;
	ADAP_Uint8 u1Priority;
	ADAP_Uint8 u1DropEligible;
	ADAP_Uint8 au1Pad[3];
}tEnetHal_VlanTagInfo;

typedef struct  {
	tEnetHal_VlanTagInfo  OuterVlanTag;
	tEnetHal_VlanTagInfo  InnerVlanTag;
} tEnetHal_VlanTag;

/* Structure for information required to select a MEP.
 * This is used by the modules to call APIs provided by ECFM
 */
typedef struct
{
   ADAP_Uint32 u4ContextId;
   ADAP_Uint32 u4IfIndex;
   ADAP_Uint32 u4VlanIdIsid;
   ADAP_Uint32 u4MdIndex;
   ADAP_Uint32 u4MaIndex;
   ADAP_Uint32 u4SlaId;
   /* The below pointer to Application Info is provided by the registered
    * protocols like ERPS during packet tx for optimized pkt tx in HW.
    *
    * Aricent ECFM stack is currently not using this for packet tx. It is for
    * integration with third party stack.
    */
   void  	   *pAppInfo;
   ADAP_Uint16 	u2MepId;
   ADAP_Uint8 	u1MdLevel;
   ADAP_Uint8 	u1Direction;
   ADAP_Uint8 	u1Version;  /* Version is added here for filling Version Field in
                      * ECFM header for R-APS packets based on the version
                      * passed by ERPS to support G.8032 Versions: 1 and 2.
                      */
   ADAP_Uint8 	au1Pad[3];
}tEnetHal_EcfmMepInfoParams;

/* Structure required to configure LBM parameters*/
typedef struct
{
   tEnetHal_MacAddr  DestMacAddr; /* Target MAC address */
   ADAP_Uint16    u2TstTlvPatterSize; /* Size of Test Pattern */
   ADAP_Uint8     *pu1DataTlvData; /* Pointer to Data for Data TLV */
   ADAP_Uint32    u4DataTlvSize; /* Size of Data TLV */
   ADAP_Uint32    u4Deadline; /* Duration fof LBM transmission */
   ADAP_Uint32    u4LbmInterval; /* Interval between successive LBM frames */
   ADAP_Uint16    u2TxLbmMessages; /* No of LB Messages to be transmitted */
   ADAP_Uint8     u1Status; /* Status of LBM transmission */
   ADAP_Uint8     u1TstTlvPatterType; /* Type of Test Pattern */
   ADAP_Uint8     u1TlvOrNone; /* LBM type to be included in LBM  frame */
   ADAP_Uint8     u1LbmMode; /* LBM to be initiated in burst/req-response mode */
   ADAP_Bool/*BOOL1*/     b1VariableByte; /* Variable byte in TST PDU */
   ADAP_Bool/*BOOL1*/     b1DropEnable; /* Drop eligibility of LBM frames */
}tEnetHal_EcfmConfigLbmInfo;

/* Structure required to configure TST parameters*/
typedef struct
{
   tEnetHal_MacAddr	DestMacAddr; /* Target MAC address */
   ADAP_Uint16     	u2TstTlvPatterSize; /* Size of Test Pattern */
   ADAP_Uint32     	u4Deadline; /* Duration fof TST transmission */
   ADAP_Uint32     	u4TxTstMessages; /* No of TST Messages to be transmitted */
   ADAP_Uint32     	u4TstInterval; /* Interval between successive TST frames */
   ADAP_Uint8     	u1Status; /* Status of TST transmission */
   ADAP_Uint8     	u1TstTlvPatterType; /* Type of Test Pattern */
   ADAP_Bool/*BOOL1*/     		b1VariableByte; /* Variable byte in TST PDU */
   ADAP_Bool/*BOOL1*/     		b1DropEnable; /* Drop eligibility of TST frames */
}tEnetHal_EcfmConfigTstInfo;

typedef struct
{
	ADAP_Uint32 u4CcmRx;
	ADAP_Uint32 u4ExpectedSeqNum;
}tEnetHal_EcfmCcOffMepRxStats;

typedef ADAP_Int32 (*pEcfmCcmOffHandleRxCallBack)(ADAP_Uint32 u4ContextId, ADAP_Uint16 u2RxHandle,ADAP_Uint32 u4IfIndex);
#define ENETHAL_ECFM_HW_MA_HANDLER_SIZE			4
#define ENETHAL_ECFM_HW_MEP_HANDLER_SIZE			4

#define ENETHAL_ECFM_HW_CCM_PERIOD_BIT                        (1<<7)
#define ENETHAL_ECFM_HW_CCM_PRIORITY_BIT                      (1<<6)
#define ENETHAL_ECFM_HW_CCM_ZERO_PERIOD_BIT                   (1<<5)
#define ENETHAL_ECFM_HW_CCM_RX_RDI_BIT                        (1<<4)
#define ENETHAL_ECFM_HW_CCM_LOC_BIT                           (1<<3)
#define ENETHAL_ECFM_HW_CCM_MEP_ID_BIT                        (1<<2)
#define ENETHAL_ECFM_HW_CCM_MEG_ID_BIT                        (1<<1)
#define ENETHAL_ECFM_HW_CCM_MEG_LEVEL_BIT                     (1<<0)

typedef enum
{
	ENETHAL_ECFM_HW_EVENT_INIT     		= 0,
	ENETHAL_ECFM_HW_EVENT_CCM_PERIOD       = ENETHAL_ECFM_HW_CCM_PERIOD_BIT,  	 /* CCM: Interval Value Change Event */
	ENETHAL_ECFM_HW_EVENT_CCM_PRIORITY     = ENETHAL_ECFM_HW_CCM_PRIORITY_BIT,     /* CCM: Priority Field Value Change */
	ENETHAL_ECFM_HW_EVENT_CCM_ZERO_PERIOD  = ENETHAL_ECFM_HW_CCM_ZERO_PERIOD_BIT,  /* CCM: Interval Zero Received Event  */
	ENETHAL_ECFM_HW_EVENT_CCM_RX_RDI       = ENETHAL_ECFM_HW_CCM_RX_RDI_BIT,       /* CCM: RDI Flag Event */
	ENETHAL_ECFM_HW_EVENT_CCM_LOC          = ENETHAL_ECFM_HW_CCM_LOC_BIT,          /* CCM: Loss Of Continuity Event */
	ENETHAL_ECFM_HW_EVENT_CCM_MEP_ID       = ENETHAL_ECFM_HW_CCM_MEP_ID_BIT,       /* CCM: Invalid RMEP ID Event */
	ENETHAL_ECFM_HW_EVENT_CCM_MEG_ID       = ENETHAL_ECFM_HW_CCM_MEG_ID_BIT,       /* CCM: Mismatch in MEG ID Event */
	ENETHAL_ECFM_HW_EVENT_MEG_LEVEL        = ENETHAL_ECFM_HW_CCM_MEG_LEVEL_BIT     /* CCM: Unexpected MEG Level Event */
}eEnetHal_EcfmHwEvents;

typedef void (* adap_mep_event_callback_t)(eEnetHal_EcfmHwEvents  EcfmHwEvents, ADAP_Uint8   au1HwHandler[]);

typedef struct
{
	ADAP_Uint32             u4VlanIdIsid;
	ADAP_Uint32             u4CcmInterval;
	ADAP_Uint8              au1MAID[48];
	ADAP_Uint8              au1HwHandler [ENETHAL_ECFM_HW_MA_HANDLER_SIZE];
}tEnetHal_EcfmHwMaParams;

typedef struct
{
	ADAP_Uint8            au1HwHandler [ENETHAL_ECFM_HW_MEP_HANDLER_SIZE];
    tEnetHal_MacAddr      DstMacAddr;
    ADAP_Uint16           u2MepId;
    ADAP_Uint8            u1MdLevel;
    ADAP_Uint8            u1Direction; /* Indicates the direction of the MEP
                                   	    * (Up/Down) */
    ADAP_Uint8            u1CcmPriority;
    ADAP_Uint8            au1Pad [1];
}tEnetHal_EcfmHwMepParams;

typedef struct
{
	ADAP_Uint8            au1HwHandler [ENETHAL_ECFM_HW_MEP_HANDLER_SIZE];
	ADAP_Uint16           u2MepId;
	ADAP_Uint16           u2RMepId;
	ADAP_Uint8            u1MdLevel;
	ADAP_Uint8            u1Direction; /* Indicates the direction of the MEP */
	ADAP_Uint8            u1State;     /* RMEP State */
	ADAP_Uint8            au1Pad [1];
}tEnetHal_EcfmHwRMepParams;

/* Structure used to pass/get the information to/from H/w routines */
typedef struct
{
	tEnetHal_HwPortArray		PortList; /* Tagged Port list */
	tEnetHal_HwPortArray        UntagPortList; /* Untagged Port List */
	ADAP_Uint32                 u4ContextId; /* Virtual Context Id */
	ADAP_Uint32                 u4IfIndex; /* Interface Index */
	ADAP_Uint16                 u2TxFilterId; /* Handler */
	ADAP_Uint16                 u2PduLength; /* Size of the PDU */
	ADAP_Uint8                  *pu1Pdu; /* Pointer to the PDU */
}tEnetHal_EcfmHwCcTxParams;

typedef struct
{
	ADAP_Uint32   u4StartSeqNo;
	ADAP_Uint32   u4CcmRxTimeout;
	ADAP_Uint32   u4VlanIdIsid;
	ADAP_Uint16   u2RMepId;
	ADAP_Uint8    u1MdLevel;
	ADAP_Uint8    au1MAID[48];
	ADAP_Uint8    u1RDI;
}tEnetHal_EcfmCcOffRxInfo;

typedef struct
{
	tEnetHal_HwPortArray        PortList; /* Tagged Port List */
	tEnetHal_HwPortArray        UntagPortList; /* Untagged Port List */
    tEnetHal_EcfmCcOffRxInfo    *pEcfmCcRxInfo; /* Structure pointer to RxInfo */
    ADAP_Uint32                 u4ContextId; /* Virtual Context Id */
    ADAP_Uint32                 u4IfIndex; /* Interface Index */
    ADAP_Uint16                 u2RxFilterId; /* Handler */
    ADAP_Uint8                  au1Pad [2]; /* Structure Padding */
}tEnetHal_EcfmHwCcRxParams;

typedef struct
{
    tEnetHal_EcfmHwMaParams         EcfmHwMaParams;
    tEnetHal_EcfmHwMepParams        EcfmHwMepParams;
    tEnetHal_EcfmHwRMepParams       EcfmHwRMepParams;
    union {
        tEnetHal_EcfmHwCcTxParams   EcfmHwCcTxParams;
        tEnetHal_EcfmHwCcRxParams   EcfmHwCcRxParams;
    }unParam;
    eEnetHal_EcfmHwEvents           EcfmHwEvents;
    adap_mep_event_callback_t    *EcfmEventCallBack;
    ADAP_Uint32                  *pu4HwCapability;
    ADAP_Uint8                   au1HwHandler [ENETHAL_ECFM_HW_MEP_HANDLER_SIZE];
    ADAP_Uint8                   u1EcfmOffStatus;
    ADAP_Uint8                   u1Priority;
    ADAP_Uint8                   u1EcfmPerPortPerMepOffStatus;
    ADAP_Uint8                   au1Pad [1];
}tEnetHal_EcfmHwParams;

ADAP_Int32 EnetHal_FsMiEcfmHwInit (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiEcfmHwDeInit (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiEcfmTransmitDmm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1DmmPdu, ADAP_Uint16 u2PduLength, tEnetHal_VlanTag VlanTag, ADAP_Uint8 u1Direction);
ADAP_Int32 EnetHal_FsMiEcfmTransmitLmm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1LmmPdu, ADAP_Uint16 u2PduLength, tEnetHal_VlanTagInfo VlanTag, ADAP_Uint8 u1Direction);
ADAP_Int32 EnetHal_FsMiEcfmTransmit1Dm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1DmPdu, ADAP_Uint16 u2PduLength, tEnetHal_VlanTag VlanTag, ADAP_Uint8 u1Direction);
ADAP_Int32 EnetHal_FsMiEcfmStartLbmTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams, tEnetHal_EcfmConfigLbmInfo * pEcfmConfigLbmInfo);
ADAP_Int32 EnetHal_FsMiEcfmStopLbmTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams);
ADAP_Int32 EnetHal_FsMiEcfmStartTstTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams, tEnetHal_EcfmConfigTstInfo * pEcfmConfigTstInfo);
ADAP_Int32 EnetHal_FsMiEcfmStopTstTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams);
ADAP_Int32 EnetHal_FsMiEcfmHwGetCapability (ADAP_Uint32 u4ContextId, ADAP_Uint32 *pu4HwCapability);
ADAP_Int32 EnetHal_FsEcfmHwGetCcmRxStatistics (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2RxFilterId, tEnetHal_EcfmCcOffMepRxStats * pEcfmCcOffMepRxStats);
ADAP_Int32 EnetHal_FsMiEcfmHwCallNpApi (ADAP_Uint8 u1Type, tEnetHal_EcfmHwParams * pEcfmHwInfo);
ADAP_Int32 EnetHal_FsMiEcfmHwRegisterCb(pEcfmCcmOffHandleRxCallBack ecfmEventHandlerCb);

/* Interfaces part*/
#define ENETHAL_MIRR_MAX_SESSIONS           20
#define ENETHAL_MAX_MIRR_DEST_PORTS         100

#define ENETHAL_MIRR_MAX_DEST_RECORD        ENETHAL_MAX_MIRR_DEST_PORTS
#define ENETHAL_MIRR_MAX_SRC_RECORD         100
#define ENETHAL_MIRR_HW_DEST_RECD           ENETHAL_MAX_MIRR_DEST_PORTS
#define ENETHAL_MIRR_HW_SRC_RECD            20

typedef enum {
	ENETHAL_MIRR_PORT_BASED = 1,
	ENETHAL_MIRR_MAC_FLOW_BASED,
	ENETHAL_MIRR_VLAN_BASED,
	ENETHAL_MIRR_INVALID,
	ENETHAL_MIRR_IP_FLOW_BASED
}eEnetHal_MirroringType;

typedef enum {
	ENETHAL_MIRR_INGRESS = 1,
	ENETHAL_MIRR_EGRESS,
	ENETHAL_MIRR_BOTH,
	ENETHAL_MIRR_DISABLED
}eEnetHal_MirrMode;

typedef enum {
	ENETHAL_MIRR_ADD_IN_PROGRESS = 1,
	ENETHAL_MIRR_ADD_DONE,
	ENETHAL_MIRR_REMOVE_IN_PROGRESS,
	ENETHAL_MIRR_REMOVE_DONE
}eEnetHal_MirrHwStatus;

typedef struct
{
	ADAP_Uint32  u4SourceNo;
	ADAP_Uint32  u4ContextId;
	ADAP_Uint8  u1Mode;
	ADAP_Uint8  u1FilterDirection;
	ADAP_Uint8  au1Pad[2];
} tEnetHal_HwSourceId;

typedef struct
{
	tEnetHal_HwSourceId SourceList[ENETHAL_MIRR_HW_SRC_RECD];
	ADAP_Uint32       u4DestList[ENETHAL_MIRR_HW_DEST_RECD];
	ADAP_Uint32       u4RSpanContextId;
	ADAP_Uint16       u2RSpanVlanId;
	ADAP_Uint16       u2SessionId;
	ADAP_Uint8        u1Mode;
	ADAP_Uint8        u1MirrType;
	ADAP_Uint8        u1RSpanSessionType;
	ADAP_Uint8        u1Action;
}tEnetHal_HwMirrorInfo;

ADAP_Int32 EnetHal_HwSetPortEgressStatus (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1EgressEnable);
ADAP_Int32 EnetHal_HwSetPortStatsCollection (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1StatsEnable);
ADAP_Int32 EnetHal_HwSetPortDuplex (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value);
ADAP_Int32 EnetHal_HwGetPortDuplex (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortDuplexStatus);
ADAP_Int32 EnetHal_HwSetPortSpeed (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value);
ADAP_Int32 EnetHal_HwGetPortSpeed (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortSpeed);
ADAP_Int32 EnetHal_HwSetPortFlowControl (ADAP_Uint32 u4IfIndex, ADAP_Int32 u1FlowCtrlEnable);
ADAP_Int32 EnetHal_HwGetPortFlowControl (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortFlowControl);
ADAP_Int32 EnetHal_HwSetRateLimitingValue (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1PacketType,ADAP_Int32 i4RateLimitVal);
ADAP_Int32 EnetHal_HwSetPortMode (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value);
ADAP_Int32 EnetHal_HwGetLearnedMacAddrCount (ADAP_Int32 *pi4LearnedMacAddrCount);
ADAP_Int32 EnetHal_driver_init(void);
ADAP_Int32 EnetHal_driver_deinit(void);
ADAP_Int32 EnetHal_HwSetMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo);

/* Mac Limiter */
typedef enum {

   ENET_DROPPKT = 1,
   ENET_PURGELRU

}eEnetHal_PortCtrlAction;

ADAP_Uint32 EnetHal_HwSetPortMaxMacAddr (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value);
ADAP_Uint32 EnetHal_HwSetPortMaxMacAction (ADAP_Uint32 u4IfIndex, eEnetHal_PortCtrlAction i4Value);

/* LAG part */
ADAP_Int32  EnetHal_FsLaHwCreateAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 *pu2HwAggId);
ADAP_Int32  EnetHal_FsLaHwAddLinkToAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber, ADAP_Uint16 *pu2HwAggId);
ADAP_Int32  EnetHal_FsLaHwSetSelectionPolicy (ADAP_Uint16 u2AggIndex, ADAP_Uint8 u1SelectionPolicy);
ADAP_Int32  EnetHal_FsLaHwSetSelectionPolicyBitList (ADAP_Uint16 u2AggIndex, ADAP_Uint32 u4SelectionPolicyBitList);
ADAP_Int32  EnetHal_FsLaHwInit (void);
ADAP_Int32  EnetHal_FsLaHwDeInit (void);
ADAP_Int32  EnetHal_FsLaHwEnableCollection (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber);
ADAP_Int32  EnetHal_FsLaHwSetPortChannelStatus (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2Inst, ADAP_Uint8 u1StpState);
ADAP_Int32  EnetHal_FsLaHwAddPortToConfAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber);
ADAP_Int32  EnetHal_FsLaHwRemovePortFromConfAggGroup (ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber);

/* PNAC (802.x) part */
ADAP_Int32  EnetHal_PnacHwEnable (void);
ADAP_Int32  EnetHal_PnacHwDisable (void);
ADAP_Int32  EnetHal_PnacHwSetAuthStatus (ADAP_Uint16 u2PortNum, tEnetHal_MacAddr *pu1SuppAddr, ADAP_Uint8 u1AuthMode, ADAP_Uint8 u1AuthStatus, ADAP_Uint8 u1CtrlDir);

/* PPPoE part */
ADAP_Uint8 EnetHal_FsNpPppInit(void);

ADAP_Uint8 EnetHal_FsNpPPPoEAddNewSession(ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *SessMACAddr,
					tEnetHal_MacAddr *SrcMACAddr, ADAP_Uint32 u2Port,
					ADAP_Uint32 SessionId, ADAP_Uint32 ACCookieId,tEnetHal_VlanTag *pVlanTag);

ADAP_Uint8 EnetHal_FsNpPppIPCPUp (ADAP_Uint32 IfIndex, ADAP_Uint32 LocalIpAddr, ADAP_Uint32 UserIpAddr);

ADAP_Uint8 EnetHal_FsNpPPPoEDelSession (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u2Port, ADAP_Uint32 SessionId);

ADAP_Int32 EnetHal_FsNpPPPoEAssociateVlan (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Port, tEnetHal_VlanTag *pVlanTag);

/* QoS part */
typedef struct
{
	ADAP_Uint32 u4IfIndex;                  /* Ingress interface         */
	ADAP_Uint32 u4QoSPriorityMapId;         /* Priority map table id     */
	ADAP_Uint32 u4HwFilterId;                  /* Hw Index of filter crated for mpls exp*/
	ADAP_Uint16 u2VlanId;                   /*  VLAN id                  */
    ADAP_Uint8 u1InPriority;               /* Incoming priority         */
    ADAP_Uint8 u1InPriType;                /* Incoming Pri Type         */
    ADAP_Uint8 u1RegenPri;                 /* Regenerated priority      */
    ADAP_Uint8 u1RegenInnerPri;            /* Regenerated Inner-VLAN priority */
    ADAP_Uint8 u1PriorityMapStatus;        /* priority-map entry status       */
    ADAP_Int8  i1InDEI;                    /* Incoming DEI value       */
    ADAP_Uint8 u1Color;                    /* Incoming Color value       */
    ADAP_Uint8 au1Pack[3];                 /* packing                         */

}tEnetHal_PriorityMapEntry;

/* Incoming Vlan map Table */
typedef struct
{
    //tRBNodeEmbd RbNode;                  /* Link to Traverse the Table       */
    ADAP_Uint32 u4Id;                          /* Index of the Table               */
    ADAP_Uint32 u4IfIndex;                     /* Incoming Interface (port)        */
    ADAP_Uint32 u4VlanMapHwId;                 /* HW ID for scheduler              */
    ADAP_Uint16 u2VlanId;                      /* Incoming VLAN ID                 */
    ADAP_Uint8 u1Status;                      /* Status of this Entry             */
    ADAP_Uint8 au1Pack[1];                    /* packing                          */

} tEnetHal_InVlanMapEntry;


typedef struct
{
	tEnetHal_L2FilterEntry     *pL2FilterPtr;
    tEnetHal_L3FilterEntry     *pL3FilterPtr;
    tEnetHal_PriorityMapEntry  *pPriorityMapPtr;/* Ptr to priority-map entry  */
    tEnetHal_InVlanMapEntry    	*pInVlanMapPtr;  /* Ptr to vlan-map entry      */
	ADAP_Uint32                 QoSMFClass;  /* 1-100 */
	ADAP_Uint32                 TrafficClass;
	ADAP_Uint8                  QoSMFCStatus;
	ADAP_Uint8                  PreColor;  /* 0-3 */
    ADAP_Uint16            	    u2PPPSessId;     /* PPP Session identifier     */

}tEnetHal_QoSClassMapEntry;

typedef struct
{
	ADAP_Uint32   QoSClass;
	ADAP_Int32    IfIndex;
	ADAP_Int32    HwExpMapId;
	ADAP_Int32    QoSMeterId;
	ADAP_Int32    QoSPolicyMapId;
	ADAP_Uint8    PHBType;
	ADAP_Uint8    DefaultPHB;
	ADAP_Uint8    PolicyMapStatus;

}tEnetHal_QoSPolicyMapEntry;

/* In Profile Table Entry */
typedef struct
{
	ADAP_Uint32  QoSInProfConfActFlag;           /* Conform action flag        */
	ADAP_Uint32  QoSInProfConfNewClass;          /* Conform action new Class   */
	ADAP_Uint32  QoSInProfConfTrafficClass;      /* Conform action traffic Class   */
	ADAP_Uint32  QoSInProfExcActFlag;            /* Exceed action flag        */
	ADAP_Uint32  QoSInProfExcNewClass;           /* Exceed action new Class   */
	ADAP_Uint32  QoSInProfExcTrafficClass;       /* Exceed action traffic Class   */

	ADAP_Uint32  QoSInProfileActionVlanPrio;    /* Conform action -vlan prio  */
	ADAP_Uint32  QoSInProfileActionVlanDE;      /* Conform action -vlan DE    */
	ADAP_Uint32  QoSInProfileActionInnerVlanPrio;  /* Conform action-Inner
                                                  vlan prio               */
	ADAP_Uint32  QoSInProfileActionInnerVlanDE;  /* Conform action-Inner Vlan DE */
	ADAP_Uint32  QoSInProfileActionIpTOS;       /* Conform action -ip TOS     */
	ADAP_Uint32  QoSInProfileActionDscp;        /* Conform action -dscp       */
	ADAP_Uint32  QoSInProfileActionMplsExp;     /* Conform action -mpls exp   */
	ADAP_Uint32  QoSInProfileExceedActionVlanPrio;  /* Exceed action-vlan prio*/
	ADAP_Uint32  QoSInProfileExceedActionVlanDE;    /* Exceed action -vlan DE */
	ADAP_Uint32  QoSInProfileExceedActionInnerVlanPrio; /* Exceed action .
                                                       Inner vlan prio      */
	ADAP_Uint32  QoSInProfileExceedActionInnerVlanDE; /* Exceed action inner vlan DE */
	ADAP_Uint32  QoSInProfileExceedActionIpTOS;     /* Exceed action- ip TOS    */
	ADAP_Uint32  QoSInProfileExceedActionDscp;      /* Exceed action - dscp     */
	ADAP_Uint32  QoSInProfileExceedActionMplsExp;    /* Exceed action - mpls exp */
	ADAP_Uint32  QoSInProfileActionPort;            /* InProfile action - Port  */

} tEnetHal_QoSInProfileActionEntry;


typedef struct
{
	ADAP_Uint32   QoSOutProfileActionFlag;
	ADAP_Uint32   QoSOutProfileNewClass;
	ADAP_Uint32   QoSOutProfileTrafficClass;
	ADAP_Uint32   QoSOutProfileActionVlanPrio;
	ADAP_Uint32   QoSOutProfileActionVlanDE;
	ADAP_Uint32   QoSOutProfileActionInnerVlanPrio;
	ADAP_Uint32   QoSOutProfileActionInnerVlanDE;
	ADAP_Uint32   QoSOutProfileActionIpTOS;
	ADAP_Uint32   QoSOutProfileActionDscp;
	ADAP_Uint32   QoSOutProfileActionMplsExp;

}tEnetHal_QoSOutProfileActionEntry;

typedef struct
{
	ADAP_Uint32  u4QoSMeterFlag;
	ADAP_Int32   i4QoSMeterId;
	ADAP_Uint32  u4QoSCIR;
	ADAP_Uint32  u4QoSCBS;
	ADAP_Uint32  u4QoSEIR;
	ADAP_Uint32  u4QoSEBS;
	ADAP_Uint32  u4NextMeter;
	ADAP_Uint8  u1QoSMeterStatus;
	ADAP_Uint8  u1QoSMeterType;
	ADAP_Uint8  u1QoSInterval;
	ADAP_Uint8  u1QoSMeterColorMode;
	ADAP_Uint8  u1QoSMeterPacketBased;

}tEnetHal_QoSMeterEntry;


typedef struct
{
	ADAP_Uint32 u4QosCIR; /* 10G - 128K */
	ADAP_Uint32 u4QosCBS; /* 64K - 64 */
	ADAP_Uint32 u4QosEIR; /* 0 */
	ADAP_Uint32 u4QosEBS; /* 0 */
	ADAP_Uint32 u2ShaperId; /* 0 - 64 */

}tEnetHal_QoSShapeCfgEntry;

typedef struct
{
    tEnetHal_QoSShapeCfgEntry  	*pShapePtr;
    ADAP_Int32               QosIfIndex; /* 1-32 */
    ADAP_Int32               QosSchedulerId; /* 256*9 */
    ADAP_Uint32              QosSchedHwId;
    ADAP_Uint32              QosSchedChildren; /* 1 - 8 */
    ADAP_Uint8               QosSchedAlgo; /* 1 -4 */
    ADAP_Uint8               QosSchedulerStatus;
    ADAP_Uint8               HL; /* 1-3 */
    ADAP_Uint8               Flag;
}tEnetHal_QoSSchedulerEntry;

typedef struct
{
	ADAP_Uint32           u4QueueTypeId;
	ADAP_Uint32           u4QueueSize;
	ADAP_Uint8           u1DropAlgo;
	ADAP_Uint8           u1DropAlgoEnableFlag;
} tEnetHal_QoSQtypeEntry;

typedef struct
{
	ADAP_Uint32           u4MinAvgThresh;
	ADAP_Uint32           u4MaxAvgThresh;
	ADAP_Uint32           u4MaxPktSize;
	ADAP_Uint32           u4Gain;
	ADAP_Uint32           u4ECNThresh;
	ADAP_Uint8           u1MaxProbability;
	ADAP_Uint8           u1ExpWeight;
	ADAP_Uint8           u1DropPrecedence;
	ADAP_Uint8           u1DropThreshType;
	ADAP_Uint8           u1RDActionFlag;
	ADAP_Uint8           inUse;
} tEnetHal_QoSREDCfgEntry;

typedef struct
{
	ADAP_Uint32 u4Ifindex;
	ADAP_Uint32 au4PriToQueMap[8];
	ADAP_Uint32 u4PfcMinThreshold;
	ADAP_Uint32 u4PfcMaxThreshold;
	ADAP_Int32  i4PfcHwProfileId;
    ADAP_Uint8 u1PfcProfileBmp;
    ADAP_Uint8 u1PfcPriority;
    ADAP_Uint8 u1PfcHwCfgFlag;
}tEnetHal_QosPfcHwEntry;

typedef struct
{
	ADAP_Int32  i4IfIndex;
	ADAP_Int32  i4HwExpMapId;
	ADAP_Uint32 u4HwFilterId;
	ADAP_Uint16 u2VlanId;
	ADAP_Uint8 u1PriType;
	ADAP_Uint8 u1Priority;
	ADAP_Uint8 u1RegenPriority;
	ADAP_Uint8 u1Flag;
	ADAP_Uint8  i1DEI;
	ADAP_Uint8  i1Color;
} tEnetHal_QosClassToPriMapEntry;

typedef struct
{
//    tIssL2FilterEntry      *pL2FilterPtr;
//    tIssL3FilterEntry      *pL3FilterPtr;
	ADAP_Uint8 u1IntPriority;
	ADAP_Uint8 u1Flag;
} tEnetHal_QosClassToIntPriEntry;

typedef struct
{
	tEnetHal_QoSShapeCfgEntry  	*pShapePtr;
    tEnetHal_QoSSchedulerEntry 	*pSchedPtr;
	ADAP_Int32                i4QosQId;
	ADAP_Int32                i4QosSchedulerId;
	ADAP_Int32                i4QosQueueHwId;
    ADAP_Uint32               u4QueueType;  /* Type of Queue Unicast Queue or Multicast Queue */
    ADAP_Uint16               u2QosQWeight;
    ADAP_Uint8               u1QosQPriority;
    ADAP_Uint8               u1QosQStatus;
} tEnetHal_QoSQEntry;

ADAP_Int32 EnetHal_QoSHwInit (void);
ADAP_Int32 EnetHal_QoSHwMapClassToPolicy (tEnetHal_QoSClassMapEntry * pClsMapEntry,
						tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
						tEnetHal_QoSInProfileActionEntry * pInProActEntry,
						tEnetHal_QoSOutProfileActionEntry * pOutProActEntry,
						tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag);

ADAP_Int32 EnetHal_QoSHwUpdatePolicyMapForClass (tEnetHal_QoSClassMapEntry * pClsMapEntry,
								tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
								tEnetHal_QoSInProfileActionEntry * pInProActEntry,
								tEnetHal_QoSOutProfileActionEntry * pOutProActEntry,
								tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag);

ADAP_Int32 EnetHal_QoSHwUnmapClassFromPolicy (tEnetHal_QoSClassMapEntry * pClsMapEntry,
                           tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
                           tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag);

ADAP_Int32 EnetHal_QoSHwDeleteClassMapEntry (tEnetHal_QoSClassMapEntry * pClsMapEntry);

ADAP_Int32 EnetHal_QoSHwMeterCreate (tEnetHal_QoSMeterEntry * pMeterEntry);

ADAP_Int32 EnetHal_QoSHwMeterDelete (ADAP_Int32 i4MeterId);

ADAP_Int32 EnetHal_QoSHwSchedulerAdd (tEnetHal_QoSSchedulerEntry * pSchedEntry);

ADAP_Int32 EnetHal_QoSHwSchedulerUpdateParams (tEnetHal_QoSSchedulerEntry * pSchedEntry);

ADAP_Int32 EnetHal_QoSHwSchedulerDelete (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId);

ADAP_Int32 EnetHal_QoSHwQueueCreate (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4QId, tEnetHal_QoSQEntry * pQEntry,
                  	  	  	  	  tEnetHal_QoSQtypeEntry * pQTypeEntry,tEnetHal_QoSREDCfgEntry * papRDCfgEntry[], ADAP_Int16 i2HL);

ADAP_Int32 EnetHal_QoSHwQueueDelete (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4Id);

ADAP_Int32
EnetHal_QoSHwMapClassToQueueId (tEnetHal_QoSClassMapEntry * pClsMapEntry, ADAP_Int32 i4IfIndex,
								ADAP_Int32 i4ClsOrPriType, ADAP_Uint32 u4ClsOrPri,
								ADAP_Uint32 u4QId, ADAP_Uint8 u1Flag);

ADAP_Int32 EnetHal_QoSHwSchedulerHierarchyMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint16 u2Sweight,
		ADAP_Uint8 u1Spriority, ADAP_Uint32 u4NextSchedId,
		ADAP_Uint32 u4NextQId, ADAP_Int16 i2HL, ADAP_Uint8 u1Flag);

ADAP_Int32 EnetHal_QoSHwGetMeterStats (ADAP_Uint32 u4MeterId, ADAP_Uint32 u4StatsType,tEnetHal_COUNTER64_TYPE * pu8MeterStatsCounter);

ADAP_Int32 EnetHal_QoSHwGetCoSQStats (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4QId, ADAP_Uint32 u4StatsType,tEnetHal_COUNTER64_TYPE * pu8CoSQStatsCounter);

ADAP_Int32
EnetHal_QoSHwSetPbitPreferenceOverDscp (ADAP_Int32 i4Port, ADAP_Int32 i4PbitPref);

ADAP_Int32
EnetHal_QoSHwSetCpuRateLimit (ADAP_Int32 i4CpuQueueId, ADAP_Uint32 u4MinRate, ADAP_Uint32 u4MaxRate);


ADAP_Int32
EnetHal_QoSHwMapClasstoPriMap (tEnetHal_QosClassToPriMapEntry * pQosClassToPriMapEntry);

/* STP part */
ADAP_Int8 EnetHal_MiStpNpHwInit (ADAP_Uint32 u4ContextId);
void EnetHal_MiRstpNpInitHw (ADAP_Uint32 u4ContextId);
ADAP_Int8 EnetHal_MiRstpNpSetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1Status);
ADAP_Int8 EnetHal_MiRstNpGetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1Status);
ADAP_Int8 EnetHal_MiMstpNpCreateInstance (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstId);
ADAP_Int8 EnetHal_MiMstpNpAddVlanInstMapping (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2InstId);
ADAP_Int8 EnetHal_MiMstpNpDeleteInstance (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstId);
ADAP_Int8 EnetHal_MiMstpNpDelVlanInstMapping (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2InstId);
ADAP_Int32 EnetHal_MiMstpNpSetInstancePortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
		ADAP_Uint16 u2InstanceId, ADAP_Uint8 u1PortState);
void EnetHal_MiMstpNpInitHw (ADAP_Uint32 u4ContextId);
void EnetHal_MiMstpNpDeInitHw (ADAP_Uint32 u4ContextId);
ADAP_Int8 EnetHal_MiMstNpGetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstanceId, ADAP_Uint32 u4IfIndex,
		ADAP_Uint8 *pu1Status);

ADAP_Int8 EnetHal_MiMstpNpAddVlanListInstMapping (ADAP_Uint32 u4ContextId, ADAP_Uint8 *pu1VlanList,
												ADAP_Uint16 u2InstId, ADAP_Uint16 u2NumVlans, ADAP_Uint16 *pu2LastVlan);
ADAP_Int8 EnetHal_MiMstpNpDelVlanListInstMapping (ADAP_Uint32 u4ContextId, ADAP_Uint8 *pu1VlanList,
												ADAP_Uint16 u2InstId, ADAP_Uint16 u2NumVlans, ADAP_Uint16 *pu2LastVlan);

/* VLAN part */
typedef struct _tEnetHal_HwUnicastMacEntry {
		ADAP_Uint32              u4PwVcIndex;
        ADAP_Uint32              u4Port;
        tEnetHal_MacAddr           ConnectionId;
        ADAP_Uint8              u1EntryType;
        ADAP_Uint8              u1HitStatus;
} tEnetHal_HwUnicastMacEntry;

#define ENETHAL_VLAN_MAX_PROTO_SIZE            5

typedef struct _tEnetHal_VlanProtoTemplate{
	ADAP_Uint8 u1Length;                           /* Indicates the valid no      */
                                              /* of bytes in au1ProtoValue   */
	ADAP_Uint8 au1ProtoValue[ENETHAL_VLAN_MAX_PROTO_SIZE]; /* Protocol value to be mapped */
	ADAP_Uint8 u1TemplateProtoFrameType;
	ADAP_Uint8 u1Reserved;
} tEnetHal_VlanProtoTemplate;

typedef enum {
   ENETHAL_VLAN_NP_STP_PROTO_ID=1,
   ENETHAL_VLAN_NP_GVRP_PROTO_ID,
   ENETHAL_VLAN_NP_GMRP_PROTO_ID,
   ENETHAL_VLAN_NP_DOT1X_PROTO_ID,
   ENETHAL_VLAN_NP_LACP_PROTO_ID,
   ENETHAL_VLAN_NP_IGMP_PROTO_ID,
   ENETHAL_VLAN_NP_MVRP_PROTO_ID,
   ENETHAL_VLAN_NP_MMRP_PROTO_ID,
   ENETHAL_VLAN_NP_ELMI_PROTO_ID,
   ENETHAL_VLAN_NP_LLDP_PROTO_ID,
   ENETHAL_VLAN_NP_ECFM_PROTO_ID,
   ENETHAL_VLAN_NP_EOAM_PROTO_ID,
   ENETHAL_VLAN_MAX_PROT_ID
}eEnetHal_VlanHwTunnelFilters;

typedef struct
{
    tEnetHal_MacAddr MacAddr;
    tEnetHal_VlanId  VlanId;
    ADAP_Uint32    u4Opcode;
    ADAP_Uint32    u4McastIndex;
}tEnetHal_HwMcastIndexInfo;

typedef struct {
	ADAP_Uint16      u2OpCode;
	ADAP_Uint16      u2AllowableTPID1;
	ADAP_Uint16      u2AllowableTPID2;
	ADAP_Uint16      u2AllowableTPID3;
	ADAP_Uint16      u2Flag;
	ADAP_Uint16      u2EgressEtherType;
    tEnetHal_VlanId    VlanId;
    ADAP_Uint8      u1SVlanPriorityType;
    ADAP_Uint8      u1SVlanPriority;
    ADAP_Uint8      u1EgressTPIDType;
    ADAP_Uint8      au1Pad[3];
}tEnetHal_HwVlanPortProperty;

ADAP_Int32 EnetHal_FsMiVlanHwInit (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwDeInit (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwAddStaticUcastEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_MacAddr *MacAddr,
		                                         ADAP_Uint32 u4Port, tEnetHal_HwPortArray * pHwAllowedToGoPorts,ADAP_Uint8 u1Status);
ADAP_Int32 EnetHal_FsMiVlanHwDelStaticUcastEntry (ADAP_Uint32 u4ContextId,ADAP_Uint32 u4Fid, tEnetHal_MacAddr *MacAddr,ADAP_Int32 u4Port);
ADAP_Int32 EnetHal_FsMiVlanHwGetFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwUnicastMacEntry * pEntry);
ADAP_Int32 EnetHal_FsMiVlanHwGetFdbCount (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, ADAP_Uint32 *pu4Count);
ADAP_Int32 EnetHal_FsMiVlanHwGetFirstTpFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 *pu4FdbId,tEnetHal_MacAddr *MacAddr);
ADAP_Int32 EnetHal_FsMiVlanHwGetNextTpFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, tEnetHal_MacAddr *MacAddr,
					ADAP_Uint32 *pu4NextContextId, ADAP_Uint32 *pu4NextFdbId,tEnetHal_MacAddr *NextMacAddr);
ADAP_Int32 EnetHal_FsMiVlanHwAddMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwPortArray * pHwMcastPorts);
ADAP_Int32 EnetHal_FsMiVlanHwAddStMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,
		ADAP_Int32 i4RcvPort, tEnetHal_HwPortArray * pHwMcastPorts);
ADAP_Int32 EnetHal_FsMiVlanHwSetMcastPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Uint32 u4IfIndex);
ADAP_Int32 EnetHal_FsMiVlanHwResetMcastPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Uint32 u4IfIndex);
ADAP_Int32 EnetHal_FsMiVlanHwDelMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr);
ADAP_Int32 EnetHal_FsMiVlanHwDelStMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Int32 i4RcvPort);
ADAP_Int32 EnetHal_FsMiVlanHwDelVlanEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwSetVlanMemberPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged);
ADAP_Int32 EnetHal_FsMiVlanHwResetVlanMemberPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortPvid (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwSetDefaultVlanId (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortAccFrameType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int8 u1AccFrameType);
ADAP_Int32 EnetHal_FsMiVlanHwAddVlanEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,tEnetHal_HwPortArray * pHwEgressPorts,tEnetHal_HwPortArray * pHwUnTagPorts);
ADAP_Int32 EnetHal_FsMiVlanHwSetDefUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4DefPriority);
ADAP_Int32 EnetHal_FsMiVlanHwSetRegenUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4UserPriority, ADAP_Int32 i4RegenPriority);
ADAP_Int32 EnetHal_FsMiVlanHwSetTraffClassMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4UserPriority, ADAP_Int32 i4TraffClass);
ADAP_Int32 EnetHal_FsMiVlanHwGmrpEnable (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwGmrpDisable (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwGvrpEnable (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwGvrpDisable (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiNpDeleteAllFdbEntries (ADAP_Uint32 u4ContextId);
ADAP_Int32 EnetHal_FsMiVlanHwAddVlanProtocolMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint32 u4GroupId,
		tEnetHal_VlanProtoTemplate * pProtoTemplate, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwDelVlanProtocolMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
												ADAP_Uint32 u4GroupId,tEnetHal_VlanProtoTemplate * pProtoTemplate);
ADAP_Int32 EnetHal_FsMiVlanHwGetPortStats (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, tEnetHal_VlanId VlanId,
		ADAP_Uint8 u1StatsType, ADAP_Uint32 *pu4PortStatsValue);
ADAP_Int32 EnetHal_FsMiVlanHwGetPortStats64 (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, tEnetHal_VlanId VlanId,
		ADAP_Uint8 u1StatsType, tEnetHal_COUNTER64_TYPE * pValue);
ADAP_Int32 EnetHal_FsMiVlanHwGetVlanStats (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Uint8 u1StatsType, ADAP_Uint32 *pu4VlanStatsValue);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortTunnelMode (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Mode);
ADAP_Int32 EnetHal_FsMiVlanHwSetTunnelFilter (ADAP_Uint32 u4ContextId, ADAP_Int32 i4BridgeMode);
ADAP_Int32 EnetHal_FsMiVlanHwCreateFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid);
ADAP_Int32 EnetHal_FsMiVlanHwDeleteFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid);
ADAP_Int32 EnetHal_FsMiVlanHwAssociateVlanFdb (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwDisassociateVlanFdb (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwFlushPortFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Uint32 u4Fid,ADAP_Int32 i4OptimizeFlag);
ADAP_Int32 EnetHal_FsMiVlanHwFlushPort (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Int32 i4OptimizeFlag);
ADAP_Int32 EnetHal_FsMiVlanHwFlushFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid);
ADAP_Int32 EnetHal_FsMiVlanHwSetShortAgeout (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Int32 i4AgingTime);
ADAP_Int32 EnetHal_FsMiVlanHwResetShortAgeout (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Int32 i4LongAgeout);
ADAP_Int32 EnetHal_FsMiVlanHwGetMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwPortArray * pHwMcastPorts);
ADAP_Int32 EnetHal_FsMiVlanHwSetBrgMode (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4BridgeMode);
ADAP_Int32 EnetHal_FsMiVlanHwSetBaseBridgeMode (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Mode);
ADAP_Int32 EnetHal_FsMiVlanHwResetVlanStats (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId);
ADAP_Int32 EnetHal_FsMiVlanHwAddPortMacVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
                               tEnetHal_MacAddr *MacAddr, tEnetHal_VlanId VlanId,ADAP_Int8 bSuppressOption);
ADAP_Int32 EnetHal_FsMiVlanHwDeletePortMacVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_MacAddr *MacAddr);
ADAP_Int32 EnetHal_FsMiVlanHwMacLearningStatus (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2FdbId,
                             tEnetHal_HwPortArray * pHwEgressPorts, ADAP_Uint8 u1Status);
ADAP_Int32 EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
												eEnetHal_VlanHwTunnelFilters ProtocolId,ADAP_Uint32 u4TunnelStatus);
ADAP_Int32 EnetHal_FsMiVlanHwPortMacLearningStatus (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Status);
ADAP_Int32 EnetHal_FsVlanHwGetMacLearningMode (ADAP_Uint32 *pu4LearningMode);
ADAP_Int32 EnetHal_FsMiVlanHwSetMcastIndex (tEnetHal_HwMcastIndexInfo *pHwMcastIndexInfo,
		ADAP_Uint32 *pu4McastIndex);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortIngressEtherType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2EtherType);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortEgressEtherType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2EtherType);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortProperty (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_HwVlanPortProperty *pVlanPortProperty);
ADAP_Int32 EnetHal_FsMiVlanHwPortUnicastMacSecType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4MacSecType);
ADAP_Int32 EnetHal_FsMiVlanHwSetVlanLoopbackStatus (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Int32 i4LoopbackStatus);
ADAP_Int32 EnetHal_FsMiVlanBlockTrafficExceptLacp (ADAP_Uint32 u4IfIndex, ADAP_Bool enableBlocking);

/* Mac limiter via vlan */
ADAP_Uint32 EnetHal_VlanHwSwitchMacLearningLimit (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4MacLimit);
ADAP_Uint32 EnetHal_VlanHwMacLearningLimit (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2FdbId, ADAP_Uint32 u4MacLimit);
/* Provider Bridge part*/
typedef struct {

    tEnetHal_MacAddr   	SrcMac;
    tEnetHal_MacAddr   	DstMac;
    ADAP_Uint32      u4Dscp;
    ADAP_Uint32      u4SrcIp;
    ADAP_Uint32      u4DstIp;
    ADAP_Uint32      u4TableType;
    tEnetHal_VlanId    	SVlanId;
    tEnetHal_VlanId    	CVlanId;
    ADAP_Uint16      u2Port;
    ADAP_Uint16      u2Reserved;
    ADAP_Uint8      u1PepUntag ;
    ADAP_Uint8      u1CepUntag ;
    ADAP_Uint8      u1SVlanPriorityType;
    ADAP_Uint8      u1SVlanPriority;
}tEnetHal_VlanSVlanMap;

typedef struct {
	ADAP_Int32     i4DefUserPri;
   tEnetHal_VlanId     Cpvid;
   ADAP_Uint8      u1AccptFrameType;
   ADAP_Uint8      u1IngFiltering;
}tEnetHal_HwVlanPbPepInfo;

typedef struct {
	ADAP_Uint16 u2PcpSelRow;
	ADAP_Uint16 u2Priority;
	ADAP_Uint16 u2PcpValue;
	ADAP_Uint8 u1DropEligible;
}tEnetHal_HwVlanPbPcpInfo;

ADAP_Int32 EnetHal_FsMiVlanHwSetProviderBridgePortType (ADAP_Uint32 u4ContextId,ADAP_Uint32 u4IfIndex,ADAP_Uint32 u4PortType);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortSVlanTranslationStatus (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Status);
ADAP_Int32 EnetHal_FsMiVlanHwAddSVlanTranslationEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan);
ADAP_Int32 EnetHal_FsMiVlanHwDelSVlanTranslationEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan);
ADAP_Int32 EnetHal_FsMiVlanHwAddSVlanMap (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Int32 EnetHal_FsMiVlanHwDeleteSVlanMap (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortSVlanClassifyMethod (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1TableType);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortCustomerVlan (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId CVlanId);
ADAP_Int32 EnetHal_FsMiVlanHwResetPortCustomerVlan (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex);
ADAP_Int32 EnetHal_FsMiVlanHwSetPepPvid (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, tEnetHal_VlanId SVlanId,tEnetHal_VlanId Pvid);
ADAP_Int32 EnetHal_FsMiVlanHwSetPepAccFrameType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 u1AccepFrameType);
ADAP_Int32 EnetHal_FsMiVlanHwSetPepDefUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Int32 i4DefUsrPri);
ADAP_Int32 EnetHal_FsMiVlanHwSetCvidUntagPep (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Int32 EnetHal_FsMiVlanHwSetPcpEncodTbl (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_HwVlanPbPcpInfo *pNpPbVlanPcpInfo);
ADAP_Int32 EnetHal_FsMiVlanHwSetPortUseDei (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1UseDei);
ADAP_Int32 EnetHal_FsMiVlanHwSetServicePriRegenEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Int32 i4RecvPriority,ADAP_Int32 i4RegenPriority);
ADAP_Int32 EnetHal_FsMiVlanHwSetTunnelMacAddress (ADAP_Uint32 u4ContextId, tEnetHal_MacAddr *MacAddr,ADAP_Uint16 u2Protocol);

/* Unregistered Multicast, Unicast, Broadcast */
typedef enum
{
    eUnicast = 0,
    eMulticast,
    eBroadcast
} eEnetHal_TrfType;

ADAP_Uint32 EnetHal_UnknownTrafficForward(tEnetHal_VlanId vlanId, eEnetHal_TrfType type, ADAP_Bool notAllowed);

#endif
