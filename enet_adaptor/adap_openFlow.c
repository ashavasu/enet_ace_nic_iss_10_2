/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "unistd.h"
#include "pthread.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_openFlow.h"
#include "EnetHal_OF_Api.h"

ADAP_Int32 EnetHal_FsOfcHwOpenflowInitAclFilters (ADAP_Uint32 u4Entries)
{
	ADAP_UNUSED_PARAM (u4Entries);


    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwOpenflowInitMeterEntries (ADAP_Uint32 u4Entries)
{
	ADAP_UNUSED_PARAM (u4Entries);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwOpenflowClientCfgParamsUpdate (eEnetHal_OfcCfgParam OfcCfgParam,ADAP_Uint32 u4CfgParamValue)
{
	ADAP_UNUSED_PARAM (OfcCfgParam);
	ADAP_UNUSED_PARAM (u4CfgParamValue);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwUpdateGroupEntry (tEnetHal_OfcHwGroupInfo * pOfcGroupEntry, eEnetHal_OfcCmd OfcCmd)
{
	ADAP_UNUSED_PARAM (OfcCmd);
	ADAP_UNUSED_PARAM (pOfcGroupEntry);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwUpdateMeterEntry (tEnetHal_OfcHwMeterInfo * pOfcMeterEntry, eEnetHal_OfcCmd OfcCmd)
{
	ADAP_UNUSED_PARAM (OfcCmd);
	ADAP_UNUSED_PARAM (pOfcMeterEntry);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwGetGroupStats (tEnetHal_OfcHwGroupInfo * pOfcGroupEntry,tEnetHal_OfcGroupStats * pOfcGroupStats)
{
	ADAP_UNUSED_PARAM (pOfcGroupEntry);
	ADAP_UNUSED_PARAM (pOfcGroupStats);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwGetMeterStats (tEnetHal_OfcHwMeterInfo * pOfcMeterEntry,tEnetHal_OfcHwMeterStats * pOfcMeterStats)
{
	ADAP_UNUSED_PARAM (pOfcMeterEntry);
	ADAP_UNUSED_PARAM (pOfcMeterStats);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwAddVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint32 *pu1UntagPorts)
{

	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4VlanId);
	ADAP_UNUSED_PARAM (pu1EgressPorts);
    ADAP_UNUSED_PARAM (pu1UntagPorts);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwDelVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4VlanId);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwSetVlanMemberPorts (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint8 *pu1UntagPorts)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4VlanId);
	ADAP_UNUSED_PARAM (pu1EgressPorts);
	ADAP_UNUSED_PARAM (pu1UntagPorts);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsOfcHwResetVlanMemberPorts (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4VlanId,ADAP_Uint8 *pu1EgressPorts, ADAP_Uint8 *pu1UntagPorts)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4VlanId);
	ADAP_UNUSED_PARAM (pu1EgressPorts);
	ADAP_UNUSED_PARAM (pu1UntagPorts);
    return ENET_SUCCESS;
}

void EnetHal_FsOfcHwInitFilter (ADAP_Uint32 u4Index, tEnetHal_OfcHwFlowInfo * pFlow)
{
	ADAP_UNUSED_PARAM (u4Index);
	ADAP_UNUSED_PARAM (pFlow);
}

ADAP_Int32 EnetHal_FsOfcHwOpenflowPortUpdate (ADAP_Uint32 u4IfIndex, eEnetHal_OfcCmd OfcCmd)
{
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (OfcCmd);
    return ENET_SUCCESS;
}
