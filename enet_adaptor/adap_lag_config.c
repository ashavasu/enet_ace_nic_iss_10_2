/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "unistd.h"
#include "pthread.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_lag_config.h"
#include "adap_enetConvert.h"

MEA_Status Adap_CreateLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 *id)
{
	MEA_LAG_dbt entry;

	*id=MEA_PLAT_GENERATE_NEW_ID;

	adap_memset(&entry,0,sizeof(entry));



	entry.cluster[entry.numof_clusters++] = clusterId;
	//entry.clustermask

	if(MEA_API_LAG_Create_Entry(MEA_UNIT_0,&entry,id)!=MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwCreateLagEntry : failed to call MEA_API_LAG_Create_Entry \n");
		return MEA_ERROR;
	}
	return (MEA_OK);
}

MEA_Status Adap_DeleteLagEntry(MEA_Uint16 id)
{
	MEA_LAG_dbt entry;

	if(id < 64)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"FsHwDeleteLagEntry : lag Id:%d is not in the range\n",id);
		return MEA_ERROR;
	}

	adap_memset(&entry,0,sizeof(entry));

	if(MEA_API_LAG_Get_Entry(MEA_UNIT_0,id,&entry) != 	MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwDeleteLagEntry : failed to call MEA_API_LAG_Get_Entry id:%d not found\n",id);
		return MEA_ERROR;
	}

	if(MEA_API_LAG_Delete_Entry(MEA_UNIT_0,id) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwDeleteLagEntry : failed to call MEA_API_LAG_Delete_Entry \n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsHwDeleteLagEntry : lag deleted \n");
	return MEA_OK;
}

MEA_Status Adap_AddLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 id)
{
	MEA_LAG_dbt entry;

	adap_memset(&entry,0,sizeof(entry));

	//entry.clustermask

	if(MEA_API_LAG_Get_Entry(MEA_UNIT_0,id,&entry) != 	MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwAddLagEntry : failed to call MEA_API_LAG_Get_Entry \n");
		return MEA_ERROR;
	}
	if(entry.numof_clusters < MEA_LAG_MAX_NUM_OF_CLUSTERS)
	{
		entry.clustermask |= (1 << entry.numof_clusters);
		entry.cluster[entry.numof_clusters++] = clusterId;
	}

	if(MEA_API_LAG_Delete_Entry(MEA_UNIT_0,id) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwDeleteLagEntry : failed to call MEA_API_LAG_Delete_Entry \n");
		return MEA_ERROR;
	}
	if(MEA_API_LAG_Create_Entry(MEA_UNIT_0,&entry,&id)!=MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwCreateLagEntry : failed to call MEA_API_LAG_Create_Entry \n");
		return MEA_ERROR;
	}

	 return MEA_OK;
}


MEA_Status Adap_RemoveLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 id,ENET_QueueId_t *pMasterclusterId)
{
	MEA_LAG_dbt entry;
	MEA_LAG_dbt temp_entry;
	MEA_Uint32 i;

	adap_memset(&entry,0,sizeof(entry));
	adap_memset(&temp_entry,0,sizeof(temp_entry));

	//entry.clustermask

	if(MEA_API_LAG_Get_Entry(MEA_UNIT_0,id,&temp_entry) != 	MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwRemoveLagEntry : failed to call MEA_API_LAG_Get_Entry \n");
		return MEA_ERROR;
	}

	(*pMasterclusterId) = MEA_PLAT_GENERATE_NEW_ID;

	for(i=0;i<temp_entry.numof_clusters;i++)
	{
		if(temp_entry.cluster[i] == clusterId)
			continue;

		if( (*pMasterclusterId) == MEA_PLAT_GENERATE_NEW_ID)
		{
			(*pMasterclusterId) =  temp_entry.cluster[i];
		}
		entry.cluster[entry.numof_clusters++] = temp_entry.cluster[i];
	}

	entry.clustermask = (1 >> temp_entry.clustermask);

	if(MEA_API_LAG_Delete_Entry(MEA_UNIT_0,id) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwCreateLagEntry : failed to call MEA_API_LAG_Delete_Entry \n");
		return MEA_ERROR;
	}

	if(entry.numof_clusters > 0)
	{
		if(MEA_API_LAG_Create_Entry(MEA_UNIT_0,&entry,&id)!=MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsHwCreateLagEntry : failed to call MEA_API_LAG_Create_Entry \n");
			return MEA_ERROR;
		}
	}

	 return MEA_OK;
}

MEA_Status Adap_SetLagGlobalLag(MEA_Uint32 enable)
{
	MEA_Globals_Entry_dbt      entry;

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed\n");
        return MEA_ERROR;
    }

	entry.if_global1.val.lag_enable = enable;

	if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Globals_Entry failed\n");
		return MEA_ERROR;
	}
	return (MEA_OK);
}
MEA_Status Adap_SetLagSetLagSelection (MEA_Port_t enetPort,MEA_Uint32 selection)
{
 	MEA_IngressPort_Entry_dbt entry;


	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwSetLagSetLagSelection: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
/*
#define MEA_PARSER_DIST_SET_L2            0x1
#define MEA_PARSER_DIST_SET_L3            0x2
#define MEA_PARSER_DIST_SET_L4            0x4
#define MEA_PARSER_DIST_SET_MPLS          0x8
#define MEA_PARSER_DIST_SET_ALL           0xf
*/
	entry.parser_info.lag_distribution_mask = selection;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwSetLagSetLagSelection: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	return (MEA_OK);
}

MEA_Status Adap_SetIngressMasterLag (MEA_Port_t enetPort,MEA_Port_t masterLag)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwSetIngressMasterLag: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
        entry.parser_info.LAG_src_port_valid = MEA_TRUE;
		entry.parser_info.LAG_src_port_value = masterLag;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwSetIngressMasterLag: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    return (MEA_OK);
}

MEA_Status Adap_clearIngressMasterLag (MEA_Port_t enetPort)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwClearIngressMasterLag: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
        entry.parser_info.LAG_src_port_valid = MEA_FALSE;
		entry.parser_info.LAG_src_port_value = 0;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"HwClearIngressMasterLag: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    return (MEA_OK);
}
