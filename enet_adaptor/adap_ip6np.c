/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#include  <string.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_ip6np.h"
#include "adap_ipnp.h"
#include "adap_enetConvert.h"
#include "adap_lpm.h"
#include "adap_pppnp.h"
#include "EnetHal_L3_Api.h"

ADAP_Uint32 EnetHal_FsNpIpv6Init(void)
{
    MEA_FWD_VPN_dbt entry;
    MEA_LxCp_t lxcp_Id = 0;
    ADAP_Uint32 idx = 0;

    entry.fwd_vpn = START_L3_VPN_IPV6;
    entry.fwd_KeyType = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
    if(ENET_ADAPTOR_Create_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Neighbor entry not deleted");
        return ENET_FAILURE;
    }

    /* LXCP for DHCP client IP address allocation */
    for(idx = 1; idx < MeaAdapGetNumOfPhyPorts(); idx++)
    {
        if (getLxcpId(idx, &lxcp_Id) == ENET_SUCCESS)
        {
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER_IPv6, MEA_LXCP_PROTOCOL_ACTION_CPU, lxcp_Id, NULL, NULL);
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT_IPv6, MEA_LXCP_PROTOCOL_ACTION_CPU, lxcp_Id, NULL, NULL);
        }
    }
 
    return adapIpv6Init();
}

void EnetHal_FsNpIpv6Deinit(void)
{
    //TODO - deregister ethertype ETHERTYPE_IPV6 0x86dd
    //TODO - remove forwarder entry for CFA_NP_IP6_ALL_ROUTER ff02::2
    //TODO - remove forwarder entry for CFA_NP_IP6_ALL_NODE ff02::1
    //TODO - remove forwarder entry for CFA_NP_IP6_SOLICITED_NODE
}

ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheAdd (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex,
    ADAP_Uint8 *pu1HwAddr, ADAP_Uint8 u1HwAddrLen, ADAP_Uint8 u1ReachStatus, ADAP_Uint16 u2VlanId)
{
    tIpv6Neighbor *neighbor = NULL;
    tEnetHal_MacAddr srcMac;
    ADAP_Uint32 logicalPort = 0;
    MEA_Port_t PhyPort = 0;
    MEA_OutPorts_Entry_dbt outPorts;
    MEA_EHP_Info_dbt EHP_info;
    ADAP_Uint16 EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    ADAP_Uint32 VLANTag = 0;
    tIpv6InterfaceTable *ipv6Interface = NULL;
    MEA_Action_t actionId = 0;
    tBridgeDomain *pBridgeDomain = NULL;
    int index = 0;

    if (u2VlanId != 0)
    {
        logicalPort = adap_VlanGetPortByVlanId (u2VlanId);
    }
    else
    {
        logicalPort = u4IfIndex;
    }

    if (u1ReachStatus == ADAP_IPV6_NH_INCOMPLETE)
    {
        neighbor = AdapRemoveIpv6NeigborEntry(u4IfIndex, pu1Ip6Addr);
        if (neighbor == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Neighbor entry not deleted");
            return ENET_FAILURE;
        }
        adap_safe_free(neighbor);

        /* Delete forwarder entry from hardware */
        if(MeaDrvHwDeleteDestIpv6Forwarder(pu1Ip6Addr, MEA_TYPE_TEID_IPV6_MASK_64, START_L3_VPN) != MEA_OK)
        {
            return ENET_FAILURE;
        }
    }
    else if (u1ReachStatus == ADAP_IPV6_NH_REACHABLE)
    {
        neighbor = AdapGetIpv6NeighborEntry(u4IfIndex, pu1Ip6Addr);
        if (neighbor == NULL)
        {
            neighbor = adap_malloc(sizeof(tIpv6Neighbor));
            if (neighbor == NULL)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
                return ENET_FAILURE;
            }

            neighbor->valid = ADAP_TRUE;
            adap_memcpy(neighbor->ipv6Addr, pu1Ip6Addr, sizeof(tIPv6Addr));
            adap_memcpy(neighbor->macAddress.ether_addr_octet, pu1HwAddr, u1HwAddrLen);
            neighbor->reachStatus = u1ReachStatus;
            neighbor->VrId = u4VrId;
            neighbor->VlanId = u2VlanId;

            AdapAddIpv6NeighborEntry(u4IfIndex, neighbor);
        }
        else
        {
            neighbor->valid = ADAP_TRUE;
            adap_memcpy(neighbor->ipv6Addr, pu1Ip6Addr, sizeof(tIPv6Addr));
            adap_memcpy(neighbor->macAddress.ether_addr_octet, pu1HwAddr, u1HwAddrLen);
            neighbor->reachStatus = u1ReachStatus;
            neighbor->VrId = u4VrId;
            neighbor->VlanId = u2VlanId;
        }

	/* Create forwarder entry in hardware */
        ipv6Interface = AdapGetIpv6Interface(u4IfIndex);
        if (ipv6Interface == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Interface table for ifIndex:%d not exist\n", u4IfIndex);
            return ENET_FAILURE;
        }
       
        adap_memset(&EHP_info, 0, sizeof(MEA_EHP_Info_dbt));
        adap_memset(&outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
        pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);
        for(index = 0; index < pBridgeDomain->num_of_ports; index++)
        {
            if(pBridgeDomain->ifType[index] == 0)
            {
                continue;
            }

            if (u2VlanId != 0)
            {
                logicalPort = pBridgeDomain->ifIndex[index];;
            }
            else
            {
                logicalPort = u4IfIndex;
            }

            MeaAdapGetPhyPortFromLogPort (logicalPort, &PhyPort);
            MEA_SET_OUTPORT(&outPorts, PhyPort);
            MEA_SET_OUTPORT(&EHP_info.output_info, PhyPort);
        }
        MeaDrvGetFirstGlobalMacAddress(&srcMac);
        if  ((u2VlanId != 0) && (adap_VlanCheckTaggedPort(u2VlanId, logicalPort) == ADAP_TRUE))
        {
            if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
                (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
            {
                ADAP_Uint32 portBridgeMode = 0;
                AdapGetProviderCoreBridgeMode(&portBridgeMode, logicalPort);
                if (portBridgeMode == ADAP_PROVIDER_NETWORK_PORT)
                {
                    EtherType = MEA_GLOBAL_WBRG_ACCESS_ETH_DEF_VAL;
                }
            }

            if (ipv6Interface->u2InnerVlanId == 0)
            {
                EHP_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
            }
            else
            {
                EHP_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_APPEND;
            }
            VLANTag = (EtherType << 16) | (u2VlanId & 0x0f);

            MiFiHwCreateRouterEhp(&EHP_info, &srcMac, &neighbor->macAddress, MEA_EGRESS_HEADER_PROC_CMD_SWAP,
                VLANTag, ipv6Interface->u2InnerVlanId);
        }
        else
        {
            EHP_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
            MiFiHwCreateRouterEhp(&EHP_info, &srcMac, &neighbor->macAddress, MEA_EGRESS_HEADER_PROC_CMD_EXTRACT, 0, 0);
        }

        /* create action for the route */
        if(FsMiHwCreateRouterAction(&actionId, &EHP_info, 1, &outPorts, MEA_ACTION_TYPE_FWD, 0, 0,
            MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
            return ENET_FAILURE;
        }

        /* create entry in forwarder */
        if (MeaDrvHwCreateDestIpv6Forwarder(actionId, MEA_FALSE, pu1Ip6Addr, MEA_TYPE_TEID_IPV6_MASK_64, 
            START_L3_VPN_IPV6, &outPorts) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ipv6 address to forwarder\n");
            return ENET_FAILURE;
        }
    }
    else
    {
	/* No need to update cache if state is other than INCOMPLETE and REACHABLE */
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheDel (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex)
{
    tIpv6Neighbor *neighbor = NULL;

    neighbor = AdapRemoveIpv6NeigborEntry(u4IfIndex, pu1Ip6Addr);
    if (neighbor == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Neighbor entry not deleted for ifindex %d\n", u4IfIndex);
        return ENET_FAILURE;
    }
    adap_safe_free(neighbor);

    /* Delete forwarder entry from hardware */
    if(MeaDrvHwDeleteDestIpv6Forwarder(pu1Ip6Addr, MEA_TYPE_TEID_IPV6_MASK_64, START_L3_VPN_IPV6) != MEA_OK)
    {
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpIpv6CheckHitOnNDCacheEntry (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex)
{
    //TODO - add code to get hit bit 
    return ADAP_FALSE;
}

ADAP_Uint32 EnetHal_FsNpIpv6UcRouteAdd (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix, ADAP_Uint8 u1PrefixLen,
    ADAP_Uint8 *pu1NextHop, ADAP_Uint8 u4NHType, tEnetHal_NpIntInfo * pIntInfo)
{
    tIpv6InterfaceTable *Ipv6Interface = NULL;
    tIpv6NeighborCache *neighborCache = NULL;
    tIpv6Route *Ipv6Route = NULL;
    tEnetHal_MacAddr srcMac;
    tEnetHal_MacAddr dstMac;
    tEnetHal_MacAddr hostMac = {.ether_addr_octet = { 0, 0, 0, 0, 0, 0} };
    ADAP_Uint32 logicalPort = 0;
    MEA_Port_t PhyPort = 0;
    MEA_OutPorts_Entry_dbt outPorts;
    MEA_EHP_Info_dbt EHP_info;
    ADAP_Uint16 EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    ADAP_Uint32 VLANTag = 0;
    MEA_Action_t actionId = 0;
    MEA_IPCS_IPV6_MASK_Type_t IPv6_mask_type = 0;
    tIPv6Addr null_ipv6Addr = { [0 ... 15] = 0 };
    tBridgeDomain *pBridgeDomain = NULL;
    int index = 0;

    if (pIntInfo->u2VlanId != 0)
    {
        logicalPort = adap_VlanGetPortByVlanId (pIntInfo->u2VlanId);
    }
    else
    {
        logicalPort = pIntInfo->u4PhyIfIndex;
    }

    //TODO - remove following after link status handling 
    EnetHal_FsNpIpv6IntfStatus(u4VrId, ADAP_PORT_LINK_UP, pIntInfo);

    if (pIntInfo->u1IfType == 131 /*CFA_TUNNEL*/)
    {
        //TODO - update tunnel conf 
    }
    else
    {
        neighborCache = AdapFindNextIpv6Neighbor(pu1NextHop);
        if (neighborCache != NULL)
        {
            Ipv6Interface = AdapGetIpv6InterfaceByVlanId(pIntInfo->u2VlanId);
            if ((Ipv6Interface == NULL) || (Ipv6Interface->ifIndex != neighborCache->ifIndex))
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Interface not found for route.\n");
                return ENET_FAILURE;
            }
            adap_memcpy(&hostMac, &neighborCache->neighbor.macAddress, sizeof(tEnetHal_MacAddr));
        }
    }

    if (u1PrefixLen == ADAP_IP6_ADDR_LEN)
    {
        /* If the request is for the host routes, program it as a neighbor entry
         * and not to the routing table
         */
	if (EnetHal_FsNpIpv6NeighCacheAdd (u4VrId, pu1Ip6Prefix, logicalPort,
            hostMac.ether_addr_octet, ETH_ALEN, ADAP_IPV6_NH_REACHABLE, pIntInfo->u2VlanId) != ENET_SUCCESS)
        {
            return ENET_FAILURE;
        }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "Route added to routing table.\n");
        return ENET_SUCCESS;
    }

    Ipv6Route = AdapGetIpv6Route(pu1Ip6Prefix);
    if (Ipv6Route != NULL)
    {
        Ipv6Route->valid = MEA_TRUE;
        adap_memcpy(Ipv6Route->destIpv6Addr, pu1Ip6Prefix, sizeof(tIPv6Addr));
        Ipv6Route->destIpv6PrefixLen = u1PrefixLen;
        adap_memcpy(&Ipv6Route->nextHopMAC, &hostMac, sizeof(tEnetHal_MacAddr));
        Ipv6Route->ifIndex = logicalPort;
        Ipv6Route->VlanId = pIntInfo->u2VlanId;
    }
    else
    {
        Ipv6Route = adap_malloc(sizeof(tIpv6Route));
        if (Ipv6Route == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
            return ENET_FAILURE;
        }

        Ipv6Route->valid = MEA_TRUE;
        adap_memcpy(Ipv6Route->destIpv6Addr, pu1Ip6Prefix, sizeof(tIPv6Addr));
        Ipv6Route->destIpv6PrefixLen = u1PrefixLen;
        adap_memcpy(&Ipv6Route->nextHopMAC, &hostMac, sizeof(tEnetHal_MacAddr));
        adap_memcpy(Ipv6Route->nextHopIpv6Addr, pu1NextHop, sizeof(tIPv6Addr));
        Ipv6Route->ifIndex = logicalPort;
        Ipv6Route->VlanId = pIntInfo->u2VlanId;

        if (AdapAddIpv6Route(Ipv6Route) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Failed to add entry to routing table.\n");
            return ENET_FAILURE;
        }
    }

    if (adap_memcmp(null_ipv6Addr, pu1NextHop, sizeof(tIPv6Addr)) == 0)
    {
	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "No need to add connected route to hardware.\n");
         return ENET_SUCCESS;
    }

    /* Update routing entry in hardware */
    adap_memset(&EHP_info, 0, sizeof(MEA_EHP_Info_dbt));
    adap_memset(&outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
    pBridgeDomain = Adap_getBridgeDomainByVlan(Ipv6Route->VlanId,0);
    if (pBridgeDomain == NULL)
    {
          ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "BridgeDomain not found.\n");
          return ENET_FAILURE;
    }
    for(index = 0; index < pBridgeDomain->num_of_ports; index++)
    {
        if(pBridgeDomain->ifType[index] == 0)
        {
            continue;
        }

        if (Ipv6Route->VlanId != 0)
        {
            logicalPort = pBridgeDomain->ifIndex[index];;
        }
        else
        {
            logicalPort = pIntInfo->u4PhyIfIndex;
        }

        MeaAdapGetPhyPortFromLogPort (logicalPort, &PhyPort);
        MEA_SET_OUTPORT(&outPorts, PhyPort);
        MEA_SET_OUTPORT(&EHP_info.output_info, PhyPort);
    }

    adap_memcpy(&dstMac, &hostMac, sizeof(tEnetHal_MacAddr));
    MeaDrvGetFirstGlobalMacAddress(&srcMac);

    if ((Ipv6Route->VlanId != 0) &&
        (adap_VlanCheckTaggedPort(Ipv6Route->VlanId, logicalPort) == ADAP_TRUE))
    {
        if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
            (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
        {
            ADAP_Uint32 portBridgeMode = 0;
            AdapGetProviderCoreBridgeMode(&portBridgeMode, logicalPort);
            if (portBridgeMode == ADAP_PROVIDER_NETWORK_PORT)
            {
                    EtherType = MEA_GLOBAL_WBRG_ACCESS_ETH_DEF_VAL;
            }
        }

        EHP_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_APPEND;
        VLANTag = (EtherType << 16) | (Ipv6Route->VlanId & 0x0f);

        MiFiHwCreateRouterEhp(&EHP_info, &srcMac, &dstMac, MEA_EGRESS_HEADER_PROC_CMD_SWAP, VLANTag, 0);
    }
    else
    {
        EHP_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
        MiFiHwCreateRouterEhp(&EHP_info, &srcMac, &dstMac, MEA_EGRESS_HEADER_PROC_CMD_EXTRACT, 0, 0);
    }

    /* create action for the route */
    if(FsMiHwCreateRouterAction(&actionId, &EHP_info, 1, &outPorts, MEA_ACTION_TYPE_FWD, 0, 0,
        MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
        return ENET_FAILURE;
    }

    /* create entry in forwarder */
    if (u1PrefixLen == 64)
    {
        IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_64;
    }
    else if (u1PrefixLen == 63)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_63;
    }
    else if (u1PrefixLen == 62)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_62;
    }
    else if (u1PrefixLen == 61)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_61;
    }
    else if (u1PrefixLen == 60)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_60;
    }
    else if (u1PrefixLen == 59)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_59;
    }
    else
    {
        //TODO - handle other prefix length
    }

    if (MeaDrvHwCreateDestIpv6Forwarder(actionId, MEA_FALSE, pu1Ip6Prefix, IPv6_mask_type,
        START_L3_VPN_IPV6, &outPorts) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed add to add ipv6 address to forwarder\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6UcRouteDelete (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix, ADAP_Uint8 u1PrefixLen,
    ADAP_Uint8 *pu1NextHop, ADAP_Uint32 u4IfIndex, tEnetHal_NpRouteInfo * pRouteInfo)
{
    tIpv6Route Ipv6Route;
    MEA_IPCS_IPV6_MASK_Type_t IPv6_mask_type = 0;

    if (u1PrefixLen == ADAP_IP6_ADDR_LEN)
    {
        /* If the request is for the host routes, delete it from the neighbor Cache */
        if (EnetHal_FsNpIpv6NeighCacheDel (u4VrId, pu1Ip6Prefix, u4IfIndex) != ENET_SUCCESS)
        {
            return ENET_FAILURE;
        }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "Route deleted from routing table.");
        return ENET_SUCCESS;
    }

    adap_memcpy(Ipv6Route.destIpv6Addr, pu1Ip6Prefix, sizeof(tIPv6Addr));
    if (AdapDeleteIpv6Route(&Ipv6Route) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Failed to add entry to routing table.");
        return ENET_FAILURE;
    }

    /* Delete forwarder entry from hardware */
    if (u1PrefixLen == 64)
    {
        IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_64;
    }
    else if (u1PrefixLen == 63)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_63;
    }
    else if (u1PrefixLen == 62)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_62;
    }
    else if (u1PrefixLen == 61)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_61;
    }
    else if (u1PrefixLen == 60)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_60;
    }
    else if (u1PrefixLen == 59)
    {
       IPv6_mask_type = MEA_TYPE_TEID_IPV6_MASK_59;
    }
    else
    {
        //TODO - handle other prefix length
    }

    if(MeaDrvHwDeleteDestIpv6Forwarder(pu1Ip6Prefix, IPv6_mask_type, START_L3_VPN_IPV6) != MEA_OK)
    {
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6RtPresentInFastPath (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix,
    ADAP_Uint8 u1PrefixLen, ADAP_Uint8 *pu1NextHop,  ADAP_Uint32 u4IfIndex)
{
    tIpv6Route *Ipv6Route = AdapGetIpv6Route(pu1Ip6Prefix);
    if (Ipv6Route == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Dest Ipv6 entry not found in routing table.");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6UcRouting (ADAP_Uint32 u4VrId, ADAP_Uint32 u4RoutingStatus)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6GetStats (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4StatsFlag, 
    ADAP_Uint32 *pu4StatsValue)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6IntfStatus (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfStatus, tEnetHal_NpIntInfo *pIntfInfo)
{
    MEA_Port_t PhyPort = 0;
    MEA_OutPorts_Entry_dbt outPorts;
    tIpv6InterfaceTable *Ipv6Interface = NULL;
    MEA_LxCp_t lxcp_Id = 0;
    ADAP_Uint32 logicalPort = 0;

    if (pIntfInfo->u2VlanId != 0)
    {
        logicalPort = adap_VlanGetPortByVlanId (pIntfInfo->u2VlanId);
    }
    else
    {
        logicalPort = pIntfInfo->u4PhyIfIndex;
    }

    MeaAdapGetPhyPortFromLogPort(logicalPort, &PhyPort);
    MEA_SET_OUTPORT(&outPorts,PhyPort);
	
    if (u4IfStatus == ADAP_PORT_LINK_UP)
    {
        Ipv6Interface = AdapAllocateIpv6Interface(pIntfInfo->u4PhyIfIndex);
	if (Ipv6Interface == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "interface entry not created\n");
            return ENET_FAILURE;
        }

        Ipv6Interface->valid = MEA_TRUE;
        Ipv6Interface->ifIndex = logicalPort;
        Ipv6Interface->IfType = pIntfInfo->u1IfType;
        Ipv6Interface->u4CfaIfIndex = pIntfInfo->u4PhyIfIndex;
	Ipv6Interface->phyPort = PhyPort;
        adap_memcpy(&Ipv6Interface->macAddress, 
            &pIntfInfo->au1MacAddr, sizeof(tEnetHal_MacAddr));
        Ipv6Interface->u2VlanId = pIntfInfo->u2VlanId;
	Ipv6Interface->TunnelIfIndex = pIntfInfo->u4TnlIfIndex;
	adap_memcpy(&Ipv6Interface->TunnelInfo, &pIntfInfo->TunnelInfo,
	    sizeof(tEnetHal_NpTunnelInfo)); 

        if (getLxcpId(logicalPort, &lxcp_Id) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Failed to get LXCP id\n");
            return ENET_FAILURE;
        }
        if (MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ICMPoV6, MEA_LXCP_PROTOCOL_ACTION_CPU,
            lxcp_Id, NULL, NULL) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Failed to set LXCP params for icmpv6\n");
            return ENET_FAILURE;
        }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "IPv6 Interface entry created for ifindex %d\n", pIntfInfo->u4PhyIfIndex);
    }
    else
    {
	if (AdapDeleteIpv6Interface(pIntfInfo->u4PhyIfIndex) != ENET_SUCCESS)
	{
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Interface entry not deleted for ifindex %d\n", pIntfInfo->u4PhyIfIndex);
            return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "IPv6 Interface entry deleted for ifindex %d\n", pIntfInfo->u4PhyIfIndex);
    }
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6AddrCreate (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4AddrType,
    ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint8 u1PrefixLen)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6AddrDelete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4AddrType, 
    ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint8 u1PrefixLen)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6TunlParamSet (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4TunlType,
    ADAP_Uint32 u4SrcAddr, ADAP_Uint32 u4DstAddr)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6AddMcastMAC (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1MacAddr, 
    ADAP_Uint8 u1MacAddrLen)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6DelMcastMAC (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1MacAddr, 
    ADAP_Uint8 u1MacAddrLen)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6HandleFdbMacEntryChange (ADAP_Uint8 *pu1L3Nexthop, ADAP_Uint32 u4IfIndex, 
ADAP_Uint8 *pu1MacAddr, ADAP_Uint8 u4IsRemoved)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpOspf3Init (void)
{
    return ENET_SUCCESS;
}

void EnetHal_FsNpOspf3DeInit (void)
{

}

ADAP_Uint32 EnetHal_FsNpRip6Init (void)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheGet (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, 
    EnetHal_NpIpv6Neighbor *Ipv6Neigbor)
{
    tIpv6NeighborCache *neighborCache = AdapFindIpv6Neighbor(pu1Ip6Addr);
    if(neighborCache == NULL)
    {
        return ENET_FAILURE;
    }

    adap_memset(Ipv6Neigbor, 0, sizeof(EnetHal_NpIpv6Neighbor));
    adap_memcpy(Ipv6Neigbor->Ip6Addr, neighborCache->neighbor.ipv6Addr, sizeof(tIPv6Addr));
    adap_memcpy(&Ipv6Neigbor->MacAddr, &neighborCache->neighbor.macAddress, sizeof(tEnetHal_MacAddr));
    Ipv6Neigbor->u4CfaIndex = neighborCache->ifIndex;
    Ipv6Neigbor->u1ReachState = neighborCache->neighbor.reachStatus;
    Ipv6Neigbor->u4VrId = neighborCache->neighbor.VrId;

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheGetNext (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr,
    EnetHal_NpIpv6Neighbor *Ipv6Neigbor)
{
    tIpv6NeighborCache *neighborCache = AdapFindNextIpv6Neighbor(pu1Ip6Addr);
    if(neighborCache == NULL)
    {
        return ENET_FAILURE;
    }

    adap_memset(Ipv6Neigbor, 0, sizeof(EnetHal_NpIpv6Neighbor));
    adap_memcpy(Ipv6Neigbor->Ip6Addr, neighborCache->neighbor.ipv6Addr, sizeof(tIPv6Addr));
    adap_memcpy(&Ipv6Neigbor->MacAddr, &neighborCache->neighbor.macAddress, sizeof(tEnetHal_MacAddr));
    Ipv6Neigbor->u4CfaIndex = neighborCache->ifIndex;
    Ipv6Neigbor->u1ReachState = neighborCache->neighbor.reachStatus;
    Ipv6Neigbor->u4VrId = neighborCache->neighbor.VrId;

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6UcGetRoute (tEnetHal_NpRtm6Input Rtm6NpInParam, 
    tEnetHal_NpRtm6Output *pRtm6NpOutParam)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6Enable (ADAP_Uint32 u4Index, ADAP_Uint16 u2VlanId)
{
    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv6Disable (ADAP_Uint32 u4Index, ADAP_Uint16 u2VlanId)
{
    return ENET_SUCCESS;
}

