/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ADAP_SRV_RM_H__
#define __ADAP_SRV_RM_H__

#include "adap_types.h"
#include "EnetHal_status.h"
#include "mea_api.h"

struct adap_srv_rm_params {
	ADAP_Uint32 max_action_tables;
	ADAP_Uint8 max_entries_per_action_table;

	ADAP_Uint32 max_qos_tables;
	ADAP_Uint8 max_entries_per_qos_table;
};

EnetHal_Status_t adap_srv_rm_init(struct adap_srv_rm_params *params);

EnetHal_Status_t adap_srv_rm_deinit(void);

EnetHal_Status_t adap_srv_rm_alloc_qos_table(MEA_TFT_t *qos_table_id);

EnetHal_Status_t adap_srv_rm_free_qos_table(MEA_TFT_t qos_table_id);

EnetHal_Status_t adap_srv_rm_alloc_qos_entry(ADAP_Uint32 qos_table_id, ADAP_Uint8 *qos_entry_id);

EnetHal_Status_t adap_srv_rm_free_qos_entry(ADAP_Uint32 qos_table_id, ADAP_Uint8 qos_entry_id);

EnetHal_Status_t adap_srv_rm_alloc_action_table(ADAP_Uint32 *action_table_id);

EnetHal_Status_t adap_srv_rm_free_action_table(ADAP_Uint32 action_table_id);

EnetHal_Status_t adap_srv_rm_alloc_action(ADAP_Uint32 action_table_id, ADAP_Uint8 *action_id);

EnetHal_Status_t adap_srv_rm_free_action(ADAP_Uint32 action_table_id, ADAP_Uint8 action_id);


#endif /* __ADAP_SRV_RM_H__ */





