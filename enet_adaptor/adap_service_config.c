/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "unistd.h"
#include "pthread.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_enetConvert.h"
#include "adap_lpm.h"

MEA_Status MeaAdapSetGwGlobalSetVxlanVlanRange(MEA_Uint16 vlanId)
{
	   MEA_Gw_Global_dbt  Gw_Global_entry;



	    adap_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

	    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
	    {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_GW_Global_Entry FAIL\n");
			return MEA_ERROR;
	    }

	    Gw_Global_entry.vxlan_upto_vlan = vlanId;


	    if (MEA_API_Set_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
	    {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_GW_Global_Entry FAIL\n");
			return MEA_ERROR;
	    }
	    return MEA_OK;
}

MEA_Status MeaAdapSetGwGlobalSetIpAddress(MEA_Uint32 ipAddress)
{
    MEA_Gw_Global_dbt  Gw_Global_entry;


    adap_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }

    Gw_Global_entry.S_GW_IP[0] = ipAddress;


    if (MEA_API_Set_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status MeaAdapSetGwGlobalSetVxlanUdpDest(MEA_Uint16 dst_port)
{
    MEA_Gw_Global_dbt  Gw_Global_entry;

    adap_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }

    Gw_Global_entry.vxlan_L4_dst_port = dst_port;


    if (MEA_API_Set_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status MeaAdapSetGwGlobalSetMacAddress(tEnetHal_MacAddr *macaddress)
{
    MEA_Gw_Global_dbt  Gw_Global_entry;

    adap_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }
    adap_mac_to_arr(macaddress, Gw_Global_entry.My_Host_MAC.b);


    if (MEA_API_Set_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_GW_Global_Entry FAIL\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status MeaAdapSetShaperPerPort(ADAP_Uint32 portId)
{
	tDatabaseShaperParams *pShaper;
	MEA_EgressPort_Entry_dbt entry;
	MEA_Port_t enetPort;
	if(portId < MeaAdapGetNumOfPhyPorts())
	{
		pShaper = ADAP_getPortShaper(portId);
		if(pShaper != NULL)
		{
			MeaAdapGetPhyPortFromLogPort(portId,&enetPort);
			if (ENET_ADAPTOR_Get_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)

			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_EgressPort_Entry FAIL\n");
				return MEA_ERROR;
			}
			if(pShaper->resolution == MEA_SHAPER_RESOLUTION_TYPE_PKT)
			{
				entry.shaper_info.CIR = pShaper->cir*1000/750;
				entry.shaper_info.CBS = pShaper->cbs*1000/750;
			}
			else
			{
				entry.shaper_info.CIR = pShaper->cir*1000;
				entry.shaper_info.CBS = pShaper->cbs*1000;
			}
			entry.shaper_info.mode=0;
			entry.shaper_info.overhead=pShaper->overhead;
			entry.shaper_info.resolution_type=pShaper->resolution;
			entry.Shaper_compensation=pShaper->consempetion;
			entry.shaper_enable=pShaper->admin;

//			if(ENET_ADAPTOR_Set_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
//			{
//				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Set_EgressPort_Entry FAIL\n");
//				return MEA_ERROR;
//			}

		}
	}
	return (MEA_OK);
}


MEA_Status MeaAdapSetShaperParams(void)
{
	ADAP_Uint32 portId;
	tDatabaseShaperParams *pShaper;
	MEA_EgressPort_Entry_dbt entry;
	MEA_Port_t enetPort;


	for(portId=1;portId<MeaAdapGetNumOfPhyPorts();portId++)
	{
		pShaper = ADAP_getPortShaper(portId);
		if(pShaper != NULL)
		{
			MeaAdapGetPhyPortFromLogPort(portId,&enetPort);
			if (ENET_ADAPTOR_Get_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)

			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_EgressPort_Entry FAIL\n");
				return MEA_ERROR;
			}
			if(pShaper->resolution == MEA_SHAPER_RESOLUTION_TYPE_PKT)
			{
				entry.shaper_info.CIR = pShaper->cir*1000/750;
				entry.shaper_info.CBS = pShaper->cbs*1000/750;
			}
			else
			{
				entry.shaper_info.CIR = pShaper->cir*1000;
				entry.shaper_info.CBS = pShaper->cbs*1000;
			}
			entry.shaper_info.mode=0;
			entry.shaper_info.overhead=pShaper->overhead;
			entry.shaper_info.resolution_type=pShaper->resolution;
			entry.Shaper_compensation=pShaper->consempetion;
			entry.shaper_enable=pShaper->admin;

//			if(ENET_ADAPTOR_Set_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
//			{
//				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Set_EgressPort_Entry FAIL\n");
//				return MEA_ERROR;
//			}

		}
	}
	return (MEA_OK);
}

MEA_Status MeaAdapsetIngressPortGlobalMac(MEA_Port_t enetPort,MEA_Uint8 globalMacBitMask)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	entry.parser_info.local_MAC_enable = globalMacBitMask;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}


    return (MEA_OK);
}

MEA_Status MeaAdapSetIngressPortDefaultPriority(MEA_Port_t enetPort, MEA_Uint32 def_pri)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	entry.parser_info.default_pri=def_pri;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}


    return (MEA_OK);
}

MEA_Status MeaAdapSetIngressPort (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool crc_check)
{
	MEA_IngressPort_Entry_dbt entry;


	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}

	entry.rx_enable = portEnable;
	entry.crc_check = crc_check;
    //entry.parser_info.pri_wildcard_valid=1;
    //entry.parser_info.pri_wildcard=0;

	if( (enetPort == INTERNAL_PORT1_NUMBER) ||
		(enetPort == INTERNAL_PORT2_NUMBER) ||
		(enetPort == INTERNAL_PORT3_NUMBER) ||
		(enetPort == INTERNAL_PORT4_NUMBER) )
	{
		//do nothing
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"internal port cos not set\r\n");
	}
	else
	{
		entry.parser_info.port_cos_aw=0;
		entry.parser_info.pri_wildcard_valid=0;
		entry.parser_info.pri_wildcard=7;
	}
//	if( (DPLGetBridgeMode() == VLAN_PBB_ICOMPONENT_BRIDGE_MODE) || (DPLGetBridgeMode() == VLAN_PBB_BCOMPONENT_BRIDGE_MODE) )
//	{
//		entry.parser_info.port_cos_aw=0;
//	}
	//entry.parser_info.port_ip_aw=1;

	if(enetPort == ADAP_CPU_PORT_ID)
	{
		entry.parser_info.net2_wildcard_valid = MEA_TRUE;
        entry.parser_info.net2_wildcard = 0xffffff;
	}

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaAdapSetIngressPortAdminStatus (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool crc_check)
{
	MEA_IngressPort_Entry_dbt entry;


	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}

	entry.rx_enable = portEnable;
	entry.crc_check = crc_check;

	if(enetPort == ADAP_CPU_PORT_ID)
	{
		entry.parser_info.net2_wildcard_valid = MEA_TRUE;
        entry.parser_info.net2_wildcard = 0xffffff;
	}

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaAdapSetIngressInterfaceType (ADAP_Int32 i4Port,MEA_Filter_key_interface_type_te interfaceType)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t				enetPort;

	if(MeaAdapGetPhyPortFromLogPort(i4Port, &enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",i4Port);
		return MEA_ERROR;
	}

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaAdapSetIngressInterfaceType: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
        entry.parser_info.filter_key_interface_type=interfaceType;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaAdapSetIngressInterfaceType: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    return (MEA_OK);
}

MEA_Status MeaAdapSetEgressPort (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool calc_crc)
{
	MEA_EgressPort_Entry_dbt entry;


	if (ENET_ADAPTOR_Get_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}

	entry.tx_enable = portEnable;
	entry.calc_crc = calc_crc;


	if (ENET_ADAPTOR_Set_EgressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set ingress port database: port%d\r\n",enetPort);
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaDrvHwSetGlobalUCMac(tEnetHal_MacAddr *macAddr)
{
    MEA_Globals_Entry_dbt 		entry;

    if (ENET_ADAPTOR_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetGlobalUCMac: MEA_API_Get_Globals_Entry FAIL\n");
		return MEA_ERROR;
    }

    adap_mac_to_arr(macAddr, entry.SA_My_Mac.b);

    if (ENET_ADAPTOR_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetGlobalUCMac: MEA_API_Set_Globals_Entry FAIL\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status MeaDrvHwGetGlobalUCMac(tEnetHal_MacAddr *macAddr)
{
    MEA_Globals_Entry_dbt 		entry;

    if (ENET_ADAPTOR_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetGlobalUCMac: MEA_API_Get_Globals_Entry FAIL\n");
		return MEA_ERROR;
    }

    adap_mac_from_arr(macAddr, entry.SA_My_Mac.b);

    if (ENET_ADAPTOR_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwSetGlobalUCMac: MEA_API_Set_Globals_Entry FAIL\n");
		return MEA_ERROR;
    }
    return MEA_OK;
}


MEA_Status MeaDrvHwSetInterfaceCfmOamUCMac (MEA_Port_t enetPort,tEnetHal_MacAddr *macAddr)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwSetInterfaceCfmOamUCMac: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	adap_mac_to_arr(macAddr, entry.parser_info.my_Mac.b);

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwSetInterfaceCfmOamUCMac: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    return (MEA_OK);
}

MEA_Status MeaDrvHwGetInterfaceCfmOamUCMac (MEA_Port_t enetPort,tEnetHal_MacAddr *macAddr)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetInterfaceCfmOamUCMac: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	adap_mac_from_arr(macAddr, entry.parser_info.my_Mac.b);


	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvHwGetInterfaceCfmOamUCMac: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    return (MEA_OK);
}

MEA_Status MeaDrvCreatePolicer (ADAP_Uint32 rate,ADAP_Uint32 burst_size,MEA_Policer_prof_t  *pPolicer_id)
{
	   MEA_Policer_Entry_dbt                 tPolicer;
	   MEA_AcmMode_t                         acm_mode;

	   adap_memset(&tPolicer,0,sizeof(tPolicer));



	tPolicer.CIR = rate;
	tPolicer.CBS = burst_size;
	tPolicer.comp = 0;
    tPolicer.gn_type= 1;
	(*pPolicer_id) = MEA_PLAT_GENERATE_NEW_ID;
	acm_mode=0;


	if(ENET_ADAPTOR_Create_Policer_ACM_Profile(MEA_UNIT_0,
											pPolicer_id,
										    acm_mode,
										    MEA_INGRESS_PORT_PROTO_TRANS,
										    &tPolicer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create policer\r\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"generate policer Id:%d\r\n",*pPolicer_id);
	return MEA_OK;
}



MEA_Status MeaDrvCreateDefaultPolicer (MEA_Policer_prof_t *pPolicer_id,MEA_Port_t portId)
{
	   MEA_Policer_Entry_dbt                 tPolicer;
	   MEA_AcmMode_t                         acm_mode;

	   adap_memset(&tPolicer,0,sizeof(tPolicer));


	if( (adapGetBoardSpecific() & SW_ENET_NIC_BOARD) == SW_ENET_NIC_BOARD)
	{
		/* setting to 10G */
		tPolicer.CIR = (10E9); 
		tPolicer.gn_type= 5;
	}
	else
	{
		tPolicer.CIR = 1000000000 + portId;
		tPolicer.gn_type= 1;
	}
	tPolicer.CBS = 256000;
	tPolicer.comp = 0;

	(*pPolicer_id) = MEA_PLAT_GENERATE_NEW_ID;
	acm_mode=0;


	if(ENET_ADAPTOR_Create_Policer_ACM_Profile(MEA_UNIT_0,
											pPolicer_id,
										    acm_mode,
										    MEA_INGRESS_PORT_PROTO_TRANS,
										    &tPolicer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create policer\r\n");
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"generate policer Id:%d for portId:%d\r\n",*pPolicer_id,portId);
	return MEA_OK;
}

MEA_Status MeaDrvHwSetDefaultService (MEA_Port_t enetPort, MEA_Service_t service_id, MEA_Def_SID_Action_te action)
{
 	MEA_IngressPort_Entry_dbt entry_ingress;

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry_ingress) != MEA_OK)
	{
        //printf("IssHwSetPortIngressStatus: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

    entry_ingress.parser_info.default_sid_info.action = action;
    entry_ingress.parser_info.default_sid_info.def_sid= service_id;


	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry_ingress) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"IssHwSetPortIngressStatus: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
    return MEA_OK;
}

MEA_Status MeaDrvCreateDefaultService (ADAP_Uint32 u4IfIndex)
{
    MEA_Port_t                            EnetPort;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    MEA_Policer_Entry_dbt                 PolicerEntry;
    MEA_Uint16                            policer_id;
    MEA_AcmMode_t                         acm_mode;
    tServiceDb							  *pServiceAlloc=NULL;

    adap_memset(&Service_key,0,sizeof(Service_key));

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex,&EnetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		return MEA_ERROR;
	}

    Service_key.src_port         = EnetPort;
    Service_key.net_tag          = 0x10000|u4IfIndex;

    Service_key.L2_protocol_type =  MEA_PARSING_L2_KEY_DEF_SID;

    /* Made the service priority-unaware (default priority) */
    Service_key.evc_enable       = MEA_FALSE;
    Service_key.pri              = 0;

    /* Clear the upstream service data attributes */
    adap_memset(&Service_data  , 0 , sizeof(Service_data));

    /* Build the upstream service data attributes for the forwarder  */
#ifdef DEFAULT_SERVICE_FORWARDING
    Service_data.DSE_forwarding_enable   = MEA_TRUE;
//        Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.vpn 					 = DEFAULT_PROTOCOLS_TRAFFIC;
#else
    Service_data.DSE_forwarding_enable   = MEA_FALSE;
//        Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.vpn = 0;
#endif
//        Service_data.DSE_forwarding_enable   = MEA_FALSE;
    Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.DSE_learning_enable     = MEA_FALSE;
    Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    Service_data.DSE_learning_actionId   = 0;
    Service_data.DSE_learning_actionId_valid = MEA_FALSE;
    Service_data.DSE_learning_srcPort	 = 0;
    Service_data.DSE_learning_srcPort_force = MEA_FALSE;


    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;

    /* As the Reverse actoin is used, most of the traffic cames through it and is policed by the Action_policer
     * Service itself processes the not learned packets ONLY, so a little bandwidth allocated for this here
     */
    /* Clear the Upstream Service policing */
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer));//
    /* Set the Upstream Service policing. T.B.D. Should be changed by QoS!!!*/
    Service_policer.CIR = 100000000;  //100M
    Service_policer.CBS = 256000;
    Service_policer.EIR = 0;
    Service_policer.EBS = 0;
    Service_policer.comp = 0;
    Service_policer.gn_type = 1;


    /* Build the Upstream Service editing */
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
    adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
    Service_editing.ehp_info       = Service_editing_info;
    //MEA_SET_OUTPORT(&Service_editing.ehp_info[0].output_info, 127);

    /* Build the upstream service outPorts */
    adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));

    //MEA_SET_OUTPORT(&Service_outPorts, 127);

    /* Attach the LxCp to the service */
    Service_data.LxCp_enable = 1;

	if(getLxcpId(u4IfIndex,&Service_data.LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateDefaultService failed by getLxcpId\r\n");
		return ENET_FAILURE;
	}

    if(Service_data.LxCp_Id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateDefaultService FAIL: LxCp Id NOT FOUND for ISS port %d\n", u4IfIndex);
		return ENET_FAILURE;
	}

	adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));

	policer_id = MEA_PLAT_GENERATE_NEW_ID;

	if(MeaAdapGetDefaultPolicer(u4IfIndex) == MEA_PLAT_GENERATE_NEW_ID)
	{
		Service_policer.CIR = 1000000000;
		Service_policer.CBS = 256000;
		Service_policer.EIR = 1000000000;
		Service_policer.EBS = 256000;
		Service_policer.comp = 0;
		Service_policer.gn_type= 1;
		Service_policer.color_aware=1;
		policer_id = MEA_PLAT_GENERATE_NEW_ID;
		acm_mode=0;

		if(MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
											  &policer_id,
											  acm_mode,
											  MEA_INGRESS_PORT_PROTO_TRANS,
											  &Service_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwInit FAIL: MEA_API_Create_Policer_ACM_Profile FAIL\n");
			return ENET_FAILURE;
		}

		/* Add the default Policer ID to the DB */
		MeaAdapSetDefaultPolicer(policer_id,u4IfIndex);

		Service_data.policer_prof_id = policer_id;
	}
	else
	{
		policer_id = MeaAdapGetDefaultPolicer(u4IfIndex);
		Service_data.policer_prof_id = policer_id;

	    if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
	    		policer_id,
	                                        0,/* ACM_Mode */
	                                        &Service_policer) != MEA_OK)
	    {
	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
	        return ENET_FAILURE;
	    }
	}

//	if(MeaAdapGetDefaultTmId(u4IfIndex) == MEA_PLAT_GENERATE_NEW_ID)
//	{
		Service_data.tmId            = 0;/* Force ENET to use a new tm bucket*/
//	}
//	else
//	{
//		Service_data.tmId = MeaAdapGetDefaultTmId(u4IfIndex);
//	}

#ifdef 	USE_UPDATE_ALLOWED
	Service_data.DSE_learning_update_allow_enable=MEA_TRUE;
#endif

	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	/* Create new service for the given Vlan. */
    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &Service_outPorts,
                               &Service_policer,
                               &Service_editing,
                               &pServiceAlloc->serviceId) != MEA_OK)
    {
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwInit FAIL: Unable to create the new service %d\n",  pServiceAlloc->serviceId);
        /*T.B.D. Rollback some of the previous actions if fail! */
        return ENET_FAILURE;

    }

	pServiceAlloc->inPort=EnetPort;
	pServiceAlloc->ifIndex=u4IfIndex;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->pmId = Service_data.pmId;
	pServiceAlloc->vpnId = Service_data.vpn;

	MeaDrvHwSetDefaultService (EnetPort, pServiceAlloc->serviceId, MEA_DEF_SID_ACTION_DEF_SID);

	MeaAdapSetDefaultTmId(Service_data.tmId,u4IfIndex);
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"tm id %d created on port:%d\n",Service_data.tmId,u4IfIndex);

    adap_SetDefaultSid(u4IfIndex,pServiceAlloc->serviceId);
	return MEA_OK;
}

/*****************************************************************************/
/*    Function Name       : CreateCpuService                                 */
/*                                                                           */
/*    Description         : This function create service traffic for CPU     */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : ENET_Source_Port - ingress port                  */
/*                          ENET_Port  - destination port.                   */
/*                          vlanId - VLAN id                                 */
/*                                                                           */
/*    Output(s)           : none                               .             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MEA_OK/MEA_ERROR                                  */
/*****************************************************************************/
MEA_Status MeaDrvCreateServicesFromCpu (tServiceAdapDb *pServiceDb)
{
	/* local variable */

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_AcmMode_t                         acm_mode;
    ADAP_Uint32						      ports;
    tServiceDb							  *pServiceAlloc=NULL;

    /* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	adap_memset(&Service_editing,0,sizeof(Service_editing));
	acm_mode = 0;
	ports=0;

	if(pServiceDb == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by pServiceDb == NULL\r\n");
		return MEA_ERROR;
	}

	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}

	/* Set the service key */
	Service_key.src_port         = pServiceDb->inPort;
	Service_key.pri              = 7;
	Service_key.net_tag          = pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Untagged?0:pServiceDb->outervlanId;
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;

	Service_key.L2_protocol_type = pServiceDb->L2_protocol_type; //MEA_PARSING_L2_KEY_Ctag_Ctag;
	Service_key.inner_netTag_from_valid          = MEA_TRUE;

	if( (pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Untagged) || (pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Ctag) ||
			(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Ctag_Ctag))
	{
		Service_key.inner_netTag_from_value          = 0xFFFFFFF;
		Service_key.inner_netTag_from_valid          = MEA_TRUE;
	}
	else if ((pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Stag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Ctag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Stag_Ctag))
	{
		Service_key.inner_netTag_from_value          = 0xFFFFFFF;
		Service_key.inner_netTag_from_valid          = MEA_TRUE;
	}
	else
	{
		Service_key.inner_netTag_from_value          = pServiceDb->innervlanId;

	}

	//Service_key.inner_netTag_from_value         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	//Service_key.evc_enable 						= MEA_FALSE;
	//Service_data.SRV_mode						= 1;

	/* Clear the service data attributes */
	adap_memset(&Service_data     , 0 , sizeof(Service_data));
	/* Build the  service data attributes */
	Service_data.DSE_forwarding_enable   = MEA_FALSE;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;

	/* Set the service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_FALSE;
	Service_data.COLOR              = 0;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	/* Clear the Service policing */
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer));
	/* Set the Service policing */


	Service_data.policer_prof_id = pServiceDb->policer_id;

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										pServiceDb->policer_id,
										acm_mode,/* ACM_Mode */
										&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
		return MEA_ERROR;
	}

	Service_data.tmId            = pServiceDb->tmId;
	Service_data.pmId			= 0;


	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	Service_editing.num_of_entries = 1;
	Service_editing.ehp_info       = &(Service_editing_info);

	Service_editing.ehp_info[0].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;



	Service_editing.ehp_info[0].ehp_data.eth_info.val.all           = 0;

	if(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Untagged)
	{
		Service_editing.ehp_info[0].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS; // MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		//Service_editing.ehp_info[0].ehp_data.eth_info.val.all = ADAP_ETHERTYPE | pServiceDb->vlanId;
	}
	else if((pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Ctag) ||
			(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Ctag_Ctag))
	{
		Service_editing.ehp_info[0].ehp_data.EditingType                = MEA_EDITINGTYPE_EXTRACT;
		Service_editing.ehp_info[0].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	}
	else if ((pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Stag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Ctag) ||
		(pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag_Stag_Ctag))
	{
		/* for provider bridge case, strip only ENET added tag */
		Service_editing.ehp_info[0].ehp_data.EditingType                = MEA_EDITINGTYPE_EXTRACT;
	}
	else
	{
		Service_editing.ehp_info[0].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
		Service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd    = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	}



	/* Build the upstream service outPorts */
	adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));

	for(ports=0;ports<pServiceDb->num_of_output_ports;ports++)
	{
		MEA_SET_OUTPORT(&Service_outPorts, pServiceDb->outports[ports]);
		MEA_SET_OUTPORT(&Service_editing.ehp_info->output_info, pServiceDb->outports[ports]);
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	// support 1588
	Service_data.set_1588= MEA_TRUE;

	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
		// roll down
		MeaAdapFreeServiceId(pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->ifIndex = ADAP_CPU_PORT_ID;
	pServiceAlloc->inPort=pServiceDb->inPort;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	pServiceDb->tmId = Service_data.tmId;

	return MEA_OK;
}

MEA_Status MeaDrvDeleteAcion(MEA_Action_t 		actionId)
{
	if(actionId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Delete_Action ERROR: Unable to delete the service %d\n",actionId);
			return MEA_ERROR;
		}
	}
	return MEA_OK;
}

MEA_Status MeaDrvDeleteVlanBridging(tServiceDb	*pServiceFree)
{
	if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, pServiceFree->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Delete_Service ERROR: Unable to delete the service %d\n",  pServiceFree->serviceId);
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"deleted serviceId:%d\r\n",pServiceFree->serviceId);
	if(pServiceFree->actionId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pServiceFree->actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Delete_Action ERROR: Unable to delete the service %d\n",  pServiceFree->actionId);
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"deleted actionId:%d\r\n",pServiceFree->actionId);

		pServiceFree->actionId = MEA_PLAT_GENERATE_NEW_ID;
	}
	MeaAdapFreeServiceId(pServiceFree->serviceId);
	return MEA_OK;
}

ADAP_Int32 MeaDeleteDefaultServiceId(MEA_Service_t   Service_id)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Bool							  valid=MEA_FALSE;

	adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&Service_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);

	if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteDefaultServiceId ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
		return ENET_FAILURE;
	}
	if(valid == MEA_TRUE)
	{
		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								  Service_id,
								  NULL,
								  &Service_data,
								  &Service_outPorts,
								  &Service_policer,
								  &Service_editing) == MEA_OK)
		{

			if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, Service_id) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteDefaultServiceId ERROR: Unable to delete the service %d\n",  Service_id);
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);
			if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
			{
				if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0, Service_data.DSE_learning_actionId) == MEA_TRUE)
				{
					if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, Service_data.DSE_learning_actionId) != MEA_OK )
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteDefaultServiceId ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
						return ENET_FAILURE;
					}
				}
			}
			MeaAdapFreeServiceId(Service_id);
		}

	}
	return ENET_SUCCESS;
}

void MeaDrvVlanSetEHPInfoData(MEA_EHP_Info_dbt *pInfo,eAdap_EhpEditing editType,ADAP_Uint32 learnEnable,ADAP_Uint16 primaryVlan,ADAP_Uint16 seconderyVlan)
{
	if(pInfo == NULL)
	{
		return;
	}

	adap_memset(pInfo,0,sizeof(MEA_EHP_Info_dbt));

	switch(editType)
	{
		case ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED:
		   if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
			{
			   pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
			}
			else
			{
				pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
			}
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			pInfo->ehp_data.EditingType        	  = MEA_EDITINGTYPE_EXTRACT;
			pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;
			pInfo->ehp_data.eth_info.stamp_color       = MEA_FALSE;
			pInfo->ehp_data.eth_info.stamp_priority    = MEA_FALSE;
			break;
		case ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG:
		   if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
			{
			   pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
			}
			else
			{
				pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
			}
			pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			pInfo->ehp_data.EditingType        	  = MEA_EDITINGTYPE_EXTRACT;
			pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;
			pInfo->ehp_data.eth_info.stamp_color       = MEA_FALSE;
			pInfo->ehp_data.eth_info.stamp_priority    = MEA_FALSE;
			break;
		case ADAP_EHP_EXTRACT_DOUBLE_EDITING:
		   if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
			{
			   pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
			}
			else
			{
				pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
			}
			pInfo->ehp_data.EditingType        	  = MEA_EDITINGTYPE_EXTRACT_EXTRACT;
			pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;
			pInfo->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			pInfo->ehp_data.atm_info.val.all = ADAP_ETHERTYPE | primaryVlan;
			pInfo->ehp_data.eth_info.stamp_color       = MEA_FALSE;
			pInfo->ehp_data.eth_info.stamp_priority    = MEA_FALSE;
			break;

		case ADAP_EHP_SWAP_CVLAN_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		    pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_SWAP_SVLAN_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		    pInfo->ehp_data.eth_info.val.all = 0x88A80000 | primaryVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_APPEND_STAG_TO_UNTAGGED_EDITING:

		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = 0x88A80000 | primaryVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = 0x88A80000 | primaryVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_APPEND_STAG_AND_CVLAN_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }

		    pInfo->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = 0x88A80000 | primaryVlan;
			pInfo->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			pInfo->ehp_data.atm_info.val.all = ADAP_ETHERTYPE | seconderyVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		    break;
		case ADAP_EHP_APPEND_CTAG_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_APPEND_DOUBLE_CTAG_EDITING:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }

		    pInfo->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.atm_info.val.all = ADAP_ETHERTYPE | primaryVlan ;
		    pInfo->ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | seconderyVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		    break;
		case ADAP_EHP_SWAP_INNER_ADD_OUTER:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		    }
		    else
		    {
		    	pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		    }

		    pInfo->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.atm_info.val.all = 0x88a80000 | primaryVlan;
		    pInfo->ehp_data.atm_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.atm_info.stamp_priority    = MEA_TRUE;



		    pInfo->ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		    pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | seconderyVlan;

		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_TRANSPARENT_UNTTAGED:
		case ADAP_EHP_TRANSPARENT_ONE_TAG:
		    if(learnEnable == ADAP_VLAN_LEARNING_ENABLED)
			{
		    	if(editType == ADAP_EHP_TRANSPARENT_UNTTAGED)
		    	{
		    		pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
		    	}
		    	else
		    	{
		    		pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		    	}
			}
			else
			{
				pInfo->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
			}
			pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
			pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
			pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
			pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
			pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
			pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
			pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
			pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
			pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
			pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_EXTRACT_VXLAN_AND_APPEND_VLAN_TAG:
			pInfo->ehp_data.EditingType =MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Append_vlan;
		    pInfo->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		    pInfo->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | primaryVlan;
		    pInfo->ehp_data.eth_info.stamp_color       = MEA_TRUE;
		    pInfo->ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		    pInfo->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		    pInfo->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		case ADAP_EHP_EXTRACT_VXLAN:
			pInfo->ehp_data.EditingType =MEA_EDITINGTYPE_VXLAN_UL_Extract_header;
			break;
		default:
			break;
	}
}

static void MeaDrvVlanSetEHPInfo(MEA_EHP_Info_dbt	*pEhp_editing,ADAP_Uint32 learning_status,tEnetHal_VlanId VlanId,tEnetHal_VlanId SecondVlanId)
{
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED],ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG],ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_EXTRACT_DOUBLE_EDITING],ADAP_EHP_EXTRACT_DOUBLE_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_SWAP_CVLAN_EDITING],ADAP_EHP_SWAP_CVLAN_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_SWAP_SVLAN_EDITING],ADAP_EHP_SWAP_SVLAN_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_APPEND_STAG_TO_UNTAGGED_EDITING],ADAP_EHP_APPEND_STAG_TO_UNTAGGED_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING],ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_APPEND_STAG_AND_CVLAN_EDITING],ADAP_EHP_APPEND_STAG_AND_CVLAN_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_APPEND_CTAG_EDITING],ADAP_EHP_APPEND_CTAG_EDITING,learning_status,VlanId,SecondVlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_APPEND_DOUBLE_CTAG_EDITING],ADAP_EHP_APPEND_DOUBLE_CTAG_EDITING,learning_status,VlanId,VlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_SWAP_INNER_ADD_OUTER],ADAP_EHP_SWAP_INNER_ADD_OUTER,learning_status,VlanId,VlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_TRANSPARENT_UNTTAGED],ADAP_EHP_TRANSPARENT_UNTTAGED,learning_status,VlanId,VlanId);
	MeaDrvVlanSetEHPInfoData(&pEhp_editing[ADAP_EHP_TRANSPARENT_ONE_TAG],ADAP_EHP_TRANSPARENT_ONE_TAG,learning_status,VlanId,VlanId);

}

MEA_Status EnetAdapLxcpAction(ADAP_Uint32 ifIndexIn, ADAP_Uint32 ifIndexOut[],ADAP_Uint32 numOfOutputPort,ADAP_Bool toCpu,MEA_LxCP_Protocol_te  protocol, MEA_LxCP_Protocol_Action_te action)
{
	ADAP_Int32 	ret = ENET_SUCCESS;

    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
	MEA_LxCp_t                             LxCp_Id;
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;
    MEA_EHP_Info_dbt                       Action_editing_info[2];
    MEA_Port_t 							   phyPort;
    ADAP_Uint32							   index=0;

    adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
    adap_memset(&LxCp_data, 0, sizeof(LxCp_data));
    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));
	adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Action_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Action_editing_info, 0, sizeof(Action_editing_info));

	ret = getLxcpId(ifIndexIn,&LxCp_Id);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
		return MEA_ERROR;
	}

	if(ENET_ADAPTOR_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_LxCP_Protocol FAIL! LxCp ID:%d\n",LxCp_Id);
		return MEA_ERROR;
	}

	Action_editing.ehp_info = &Action_editing_info[0];

	Action_editing.num_of_entries = 0;

	for(index=0;index<numOfOutputPort;index++)
	{
		if(ifIndexOut[index] == MEA_PLAT_GENERATE_NEW_ID)
			continue;

		if(ifIndexOut[index] == ifIndexIn)
		{
			continue;
		}
		if(MeaAdapGetPhyPortFromLogPort(ifIndexOut[index],&phyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndexOut[index]);
			return MEA_ERROR;
		}
		MEA_SET_OUTPORT(&Action_editing_info[Action_editing.num_of_entries].output_info, phyPort);
		MEA_SET_OUTPORT(&Action_outPorts, phyPort);

		/* Build the action editing */
	}
	Action_editing_info[Action_editing.num_of_entries].ehp_data.EditingType                        = MEA_EDITINGTYPE_NORMAL;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	Action_editing.num_of_entries++;


	if(toCpu == ADAP_TRUE)
	{
		Action_editing_info[Action_editing.num_of_entries].ehp_data.EditingType                        = MEA_EDITINGTYPE_APPEND_APPEND;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.val.all=ADAP_ETHERTYPE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_info.stamp_color               = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		Action_editing_info[Action_editing.num_of_entries].ehp_data.atm_info.val.all= (MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_VLANID);
		Action_editing_info[Action_editing.num_of_entries].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;

		MEA_SET_OUTPORT(&Action_editing_info[Action_editing.num_of_entries].output_info, 127);
		MEA_SET_OUTPORT(&Action_outPorts, 127);
		Action_editing.num_of_entries++;
	}




	// create action
	Action_data.output_ports_valid  = MEA_TRUE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_TRUE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_TRUE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;

	/* Build the action policer */
	adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
	Action_data.tm_id              = 0;
	Action_data.policer_prof_id    = MeaAdapGetDefaultPolicer(ifIndexIn);
	Action_data.tm_id              = MeaAdapGetDefaultTmId(ifIndexIn);


	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Action_data.policer_prof_id,
										0,/* ACM_Mode */
										&Action_policer)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"enetSetCepCnpTunnelConfiguration FAIL: MEA_API_Get_Policer_ACM_Profile for PolilcingProfile %d failed\n",
				Action_data.policer_prof_id);
		return MEA_ERROR;
	}

	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   &Action_policer,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Create_Action failed for action:%d\n",
			  Action_id);
	   return MEA_ERROR;
	}
	LxCp_key.protocol = protocol;
	LxCp_data.action_type = action;

	if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION)
	{
		LxCp_data.ActionId_valid = MEA_TRUE;
		LxCp_data.ActionId = Action_id;
		LxCp_data.OutPorts_valid = MEA_TRUE;
		adap_memcpy(&LxCp_data.OutPorts,&Action_outPorts,sizeof(LxCp_data.OutPorts));
	}
	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"enetSetCepCnpTunnelConfiguration FAIL: MEA_API_Set_LxCP_Protocol FAIL!\n");
		return MEA_ERROR;
	}

	return MEA_OK;
}

MEA_Status EnetAdapDhcpRedirectDemo(ADAP_Uint32	ifIndexIn,ADAP_Uint32	ifIndexOut,MEA_LxCP_Protocol_te  protocol)
{
	ADAP_Int32 	ret = ENET_SUCCESS;

    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
	MEA_LxCp_t                             LxCp_Id;
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;
    MEA_EHP_Info_dbt                       Action_editing_info;
    MEA_Port_t 							  phyPort;


    adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
    adap_memset(&LxCp_data, 0, sizeof(LxCp_data));
    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));
	adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Action_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Action_editing_info, 0, sizeof(Action_editing_info));

	ret = getLxcpId(ifIndexIn,&LxCp_Id);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
		return MEA_ERROR;
	}

	if(ENET_ADAPTOR_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_LxCP_Protocol FAIL! LxCp ID:%d\n",LxCp_Id);
		return MEA_ERROR;
	}

	if(MeaAdapGetPhyPortFromLogPort(ifIndexOut,&phyPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndexOut);
		return MEA_ERROR;
	}

	// create action
	Action_data.output_ports_valid  = MEA_TRUE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_TRUE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_TRUE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;

	/* Build the action policer */
	adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
	Action_data.tm_id              = 0;
	Action_data.policer_prof_id    = MeaAdapGetDefaultPolicer(ifIndexIn);
	Action_data.tm_id              = MeaAdapGetDefaultTmId(ifIndexIn);


	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Action_data.policer_prof_id,
										0,/* ACM_Mode */
										&Action_policer)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"enetSetCepCnpTunnelConfiguration FAIL: MEA_API_Get_Policer_ACM_Profile for PolilcingProfile %d failed\n",
				Action_data.policer_prof_id);
		return MEA_ERROR;
	}



	/* Build the action editing */
	Action_editing.num_of_entries = 1;

	MEA_SET_OUTPORT(&Action_editing_info.output_info, phyPort);
	MEA_SET_OUTPORT(&Action_outPorts, phyPort);
	Action_editing.ehp_info = &Action_editing_info;



	Action_editing_info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing_info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;


	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   &Action_policer,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Create_Action failed for action:%d output port:%d\n",
			  Action_id,phyPort);
	   return MEA_ERROR;
	}

	LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
	LxCp_data.ActionId_valid = MEA_TRUE;
	LxCp_data.ActionId = Action_id;
	LxCp_data.OutPorts_valid = MEA_TRUE;

	LxCp_key.protocol = protocol;

	adap_memcpy(&LxCp_data.OutPorts,&Action_outPorts,sizeof(LxCp_data.OutPorts));

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"enetSetCepCnpTunnelConfiguration FAIL: MEA_API_Set_LxCP_Protocol FAIL!\n");
		return MEA_ERROR;
	}

	return MEA_OK;

}

MEA_Status MeaDrvCreateQinQVlanBridging(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type)
{
    MEA_Def_SID_dbt 			  DefSIDentry;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_AcmMode_t                         acm_mode;
    ADAP_Uint32						      ports;
    tServiceDb							  *pServiceAlloc=NULL;
    MEA_Port_t 							  phyPort;
    MEA_Port_t 							  phyOutputPort;

    MEA_Policer_Entry_dbt          		  Action_policer;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
//    ADAP_Bool							  found=ADAP_TRUE;
    MEA_LxCp_t 					  		  lxcp=MEA_PLAT_GENERATE_NEW_ID;
    ADAP_Uint8 							  IntType=0;

    ADAP_Uint32							  bEditing[ADAP_EHP_LAST_EDITING];
    MEA_EHP_Info_dbt					  ehp_editing[ADAP_EHP_LAST_EDITING];
    ADAP_Uint32  						  providerMode,providerModeOtherSide;
    ADAP_Uint32							 index=0;


    // init variables
	adap_memset(&ehp_editing[0] , 0 , sizeof(MEA_EHP_Info_dbt )*ADAP_EHP_LAST_EDITING);
	adap_memset(&bEditing[0]    , 0 , sizeof(ADAP_Uint32 )*ADAP_EHP_LAST_EDITING);
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
	adap_memset(&Service_editing,0,sizeof(Service_editing));

	adap_memset(&Action_policer,0,sizeof(Action_policer));
	adap_memset(&Action_data,0,sizeof(Action_data));
	adap_memset(&Action_outPorts,0,sizeof(Action_outPorts));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	adap_memset(&Action_editing,0,sizeof(Action_editing));
	acm_mode = 0;
	ports=0;
	phyPort=0;



	if(MeaAdapGetPhyPortFromLogPort(ifIndex,&phyPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}




	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	AdapGetProviderBridgeMode(&providerMode,ifIndex);

	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		// configure the actionId
		Action_data.output_ports_valid = MEA_TRUE;
		Action_data.force_color_valid  = MEA_TRUE;
		Action_data.COLOR              = 2;
		Action_data.force_cos_valid    = MEA_FALSE;
		Action_data.COS                = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;

		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_FALSE;
		Action_data.tm_id              = 0;
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE;


		MEA_SET_OUTPORT(&Action_outPorts, phyPort);
		Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
		Action_editing.ehp_info = &Action_editing_info;
		MEA_SET_OUTPORT(&Action_editing_info.output_info, phyPort);

		Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
		Action_data.tm_id = MeaAdapGetDefaultTmId(ifIndex);


		if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
											Action_data.policer_prof_id,
											acm_mode,/* ACM_Mode */
											&Action_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler ifIndex:%d\n",ifIndex);
			return MEA_ERROR;
		}

		if(pBridgeDomain->vlanId == getPortToPvid(ifIndex))
		{
			Action_editing.ehp_info->ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			Action_editing.ehp_info->ehp_data.EditingType        	  = MEA_EDITINGTYPE_EXTRACT;
		}
		else if(providerMode == ADAP_VLAN_ACCESS_PORT)
		{
			Action_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			Action_editing.ehp_info->ehp_data.EditingType        	  = MEA_EDITINGTYPE_APPEND;
			Action_editing.ehp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | pBridgeDomain->vlanId;
			Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_priority            = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_color               = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		}
		else if(providerMode == ADAP_VLAN_TRUNK_PORT)
		{
			Action_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			Action_editing.ehp_info->ehp_data.EditingType        	  = MEA_EDITINGTYPE_APPEND;
			Action_editing.ehp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | pBridgeDomain->vlanId;
			Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_priority            = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_color               = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		}

		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;

		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts,
								   &Action_policer,
								   &Action_editing,
								   &pServiceAlloc->actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Create_Action\n");
			return MEA_ERROR;
		}
		MeaAdapSetDefaultTmId(Action_data.tm_id,ifIndex);
	}

	Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Service_data.policer_prof_id,
										acm_mode,/* ACM_Mode */
										&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
		return MEA_ERROR;
	}


	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.inner_netTag_from_valid          = MEA_FALSE;
	Service_key.inner_netTag_from_value          = 0xFFFFFFF;
	Service_key.evc_enable 						= MEA_FALSE;

	/* Set the service key */
	Service_key.src_port         = phyPort;
	Service_key.pri              = 7;
	if( pBridgeDomain->vlanId == getPortToPvid(ifIndex) )
	{
	    if (type == VLAN_TAGGED_MEMBER_PORT)
	    {
		Service_key.net_tag          = 0x01000|ifIndex;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_DEF_SID;
		Service_key.pri              = 0;
	    }
	    else if (type == VLAN_UNTAGGED_MEMBER_PORT)
	    {
		Service_key.net_tag          = 0xff000;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
	    }
	}
	else if(providerMode == ADAP_VLAN_ACCESS_PORT)
	{
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
	}
	else if(providerMode == ADAP_VLAN_TRUNK_PORT)
	{
	    if (type == VLAN_TAGGED_MEMBER_PORT)
	    {
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag_Ctag;
		Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
		Service_key.inner_netTag_from_valid          = MEA_TRUE;
		Service_key.inner_netTag_from_value = 0xFFFFFF;
		Service_key.evc_enable 		= MEA_TRUE;
	    }
	    else if (type == VLAN_UNTAGGED_MEMBER_PORT)
	    {
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
		Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	    }
	}


	//Service_data.SRV_mode						= 1;


	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		/* Build the  service data attributes */
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_TRUE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_TRUE;
		Service_data.DSE_learning_actionId   = pServiceAlloc->actionId;
		Service_data.DSE_learning_actionId_valid = MEA_TRUE;
		Service_data.DSE_learning_srcPort	 = phyPort; //TODO, should be cluster!
		Service_data.DSE_learning_srcPort_force = MEA_TRUE;
	}
	else
	{
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_FALSE;
		Service_data.DSE_learning_actionId   = 0;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.DSE_learning_srcPort	 = 0;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	}

	/* Set the service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	Service_data.tmId = MeaAdapGetDefaultTmId(ifIndex);

	/* Set the service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_FALSE;
	Service_data.COLOR              = 0;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	Service_data.tmId            = 0; // create tm per port
	Service_data.vpn			= pBridgeDomain->vpn;

	MeaAdapGetIntType(&IntType,ifIndex);

	if(Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID)
	{
		Service_data.DSE_forwarding_enable   = MEA_FALSE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_actionId   = 0;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.DSE_learning_srcPort	 = 0;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
    		Service_data.vpn = 0;
	}

	// only if this is L3 interface
	if(AdapIsVlanIpInterface(pBridgeDomain->vlanId) == ENET_SUCCESS)
	{
		Service_data.DSE_mask_field_type=MEA_DSE_MASK_IP_32;
		Service_data.L3_L4_fwd_enable = MEA_TRUE;
		Service_data.L4port_mask_enable	= MEA_FALSE;

	    Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	    Service_data.DSE_learning_srcPort	 = 0;
	    Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	}






	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	//Service_editing.num_of_entries = 1;

	Service_editing.ehp_info       = &Service_editing_info[0];
	Service_editing.num_of_entries=0;



	if(getLxcpId(ifIndex,&lxcp) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}

	Service_data.LxCp_enable = 1;
	Service_data.LxCp_Id = lxcp;

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;


	for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
	{
		if(pBridgeDomain->ifIndex[ports] == ifIndex)
		{
			continue;
		}
		AdapGetProviderBridgeMode(&providerModeOtherSide,ifIndex);
		if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
			return MEA_ERROR;
		}

		if(pBridgeDomain->vlanId == getPortToPvid(ifIndex))
		{
			if(pBridgeDomain->vlanId == getPortToPvid(pBridgeDomain->ifIndex[ports]))
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED],
					ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED].output_info,phyOutputPort);
			}
			else if(providerModeOtherSide == ADAP_VLAN_ACCESS_PORT)
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING],
					ADAP_EHP_APPEND_CTAG_EDITING,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_APPEND_CTAG_EDITING] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].output_info,phyOutputPort);
			}
			else if((providerModeOtherSide == ADAP_VLAN_TRUNK_PORT) ||
				(providerModeOtherSide == ADAP_VLAN_HYBRID_PORT))
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING],
					ADAP_EHP_APPEND_CTAG_EDITING,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_APPEND_CTAG_EDITING] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].output_info,phyOutputPort);
				if ((Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID) &&
						(type == VLAN_TAGGED_MEMBER_PORT))
				{
					ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
				}
			}
		}
		else if(providerMode == ADAP_VLAN_ACCESS_PORT)
		{
			if(pBridgeDomain->vlanId == getPortToPvid(pBridgeDomain->ifIndex[ports]))
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED],
					ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED].output_info,phyOutputPort);
			}
			else if(providerModeOtherSide == ADAP_VLAN_ACCESS_PORT)
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING],
					ADAP_EHP_APPEND_CTAG_EDITING,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_APPEND_CTAG_EDITING] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].output_info,phyOutputPort);
			}
			else if(providerModeOtherSide == ADAP_VLAN_TRUNK_PORT)
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING],
					ADAP_EHP_APPEND_CTAG_EDITING,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_APPEND_CTAG_EDITING] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].output_info,phyOutputPort);
			}
		}
		else if(providerMode == ADAP_VLAN_TRUNK_PORT)
		{
			if(pBridgeDomain->vlanId == getPortToPvid(pBridgeDomain->ifIndex[ports]))
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG],
					ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG].output_info,phyOutputPort);
			}
			else if(providerModeOtherSide == ADAP_VLAN_ACCESS_PORT)
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG],
					ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG].output_info,phyOutputPort);
			}
			else if(providerModeOtherSide == ADAP_VLAN_TRUNK_PORT)
			{
				MeaDrvVlanSetEHPInfoData(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING],
					ADAP_EHP_APPEND_CTAG_EDITING,
					pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,pBridgeDomain->vlanId);
				bEditing[ADAP_EHP_APPEND_CTAG_EDITING] = ADAP_TRUE;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].output_info,phyOutputPort);
				if ((Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID) &&
						(type == VLAN_TAGGED_MEMBER_PORT))
				{
					ehp_editing[ADAP_EHP_APPEND_CTAG_EDITING].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
				}
			}
		}
	}


	for(index=0;index<ADAP_EHP_LAST_EDITING;index++)
	{
		if(bEditing[index] == ADAP_TRUE)
		{
			adap_memcpy(&Service_editing.ehp_info[Service_editing.num_of_entries++],&ehp_editing[index],sizeof(MEA_EHP_Info_dbt));
		}
	}

	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	MeaAdapSetDefaultTmId(Service_data.tmId,ifIndex);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->inPort=phyPort;
	pServiceAlloc->ifIndex=ifIndex;
	pServiceAlloc->outerVlan=pBridgeDomain->vlanId;
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	// add the service to bridge domain
	if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	if (pServiceAlloc->L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID)
	{
		adap_memset(&DefSIDentry,0,sizeof(DefSIDentry));
		DefSIDentry.def_sid = pServiceAlloc->serviceId;
		DefSIDentry.action  = 2;
		DefSIDentry.valid   = MEA_TRUE;

		if(ENET_ADAPTOR_Set_IngressPort_Default_L2Type_Sid(MEA_UNIT_0, pServiceAlloc->inPort, 1, &DefSIDentry )!= MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set default L2type serviceId:%d\r\n",pServiceAlloc->serviceId);
		}
	}

	return MEA_OK;
}

MEA_Status MeaDrvCreatePbVlanPNP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain)
{
	ADAP_UNUSED_PARAM (ifIndex);
	ADAP_UNUSED_PARAM (pBridgeDomain);
	return MEA_OK;
}

MEA_Status MeaDrvCreatePbVlanPNPsMap(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap)
{
	ADAP_UNUSED_PARAM (ifIndex);
	ADAP_UNUSED_PARAM (pBridgeDomain);
	ADAP_UNUSED_PARAM (sVlanMap);
	return MEA_OK;
}

MEA_Status MeaDrvCreatePbVlanCNP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap)
{
	ADAP_UNUSED_PARAM (ifIndex);
	ADAP_UNUSED_PARAM (pBridgeDomain);
	ADAP_UNUSED_PARAM (sVlanMap);
	return MEA_OK;
}

MEA_Status MeaDrvCreatePbVlanCEP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap,ADAP_Uint32 tagged)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_AcmMode_t                         acm_mode;
    ADAP_Uint32						      ports;
    tServiceDb							  *pServiceAlloc=NULL;
    MEA_Port_t 							  phyPort;
    MEA_Port_t 							  phyOutputPort;

    MEA_Policer_Entry_dbt          		  Action_policer;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
//    ADAP_Bool							  found=ADAP_TRUE;
//    MEA_LxCp_t 					  		  lxcp=MEA_PLAT_GENERATE_NEW_ID;
//    ADAP_Uint8 							  IntType=0;
    ADAP_Uint32							  PortType;
    ADAP_Uint32							  bEditing[ADAP_EHP_LAST_EDITING];
    MEA_EHP_Info_dbt					  ehp_editing[ADAP_EHP_LAST_EDITING];
    ADAP_Uint32							  index=0;


    // init variables
	adap_memset(&ehp_editing[0] , 0 , sizeof(MEA_EHP_Info_dbt )*ADAP_EHP_LAST_EDITING);
	adap_memset(&bEditing[0]    , 0 , sizeof(ADAP_Uint32 )*ADAP_EHP_LAST_EDITING);

    /* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
	adap_memset(&Service_editing,0,sizeof(Service_editing));

	adap_memset(&Action_policer,0,sizeof(Action_policer));
	adap_memset(&Action_data,0,sizeof(Action_data));
	adap_memset(&Action_outPorts,0,sizeof(Action_outPorts));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	adap_memset(&Action_editing,0,sizeof(Action_editing));

	acm_mode = 0;
	ports=0;
	phyPort=0;
	/* end of init variables */

	if(sVlanMap == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex:%d tEnetHal_VlanSVlanMap=NULL\r\n",ifIndex);
		return MEA_ERROR;
	}

	// in case is u1CepUntag then its need to create untagged traffic
	if( (tagged==1) && (sVlanMap->u1CepUntag == ADAP_TRUE) )
	{
		return MEA_OK;
	}

	MeaDrvVlanSetEHPInfo(&ehp_editing[0],pBridgeDomain->learningMode[ifIndex],pBridgeDomain->vlanId,sVlanMap->CVlanId);

	if(MeaAdapGetPhyPortFromLogPort(ifIndex,&phyPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		// configure the actionId
		Action_data.output_ports_valid = MEA_TRUE;
		Action_data.force_color_valid  = MEA_TRUE;
		Action_data.COLOR              = 2;
		Action_data.force_cos_valid    = MEA_FALSE;
		Action_data.COS                = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;

		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_FALSE;
		Action_data.tm_id              = 0;
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE;


		MEA_SET_OUTPORT(&Action_outPorts, phyPort);
		Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
		Action_editing.ehp_info = &Action_editing_info;
		MEA_SET_OUTPORT(&Action_editing_info.output_info, phyPort);

		Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
		Action_data.tm_id = MeaAdapGetDefaultTmId(ifIndex);

		if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
											Action_data.policer_prof_id,
											acm_mode,/* ACM_Mode */
											&Action_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler ifIndex:%d\n",ifIndex);
			return MEA_ERROR;
		}

		Action_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		Action_editing.ehp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | sVlanMap->CVlanId;
		Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		Action_editing.ehp_info->ehp_data.eth_info.stamp_priority            = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_info.stamp_color               = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts,
								   &Action_policer,
								   &Action_editing,
								   &pServiceAlloc->actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Create_Action\n");
			return MEA_ERROR;
		}
		MeaAdapSetDefaultTmId(Action_data.tm_id,ifIndex);
	}
	Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
	Service_data.tmId = MeaAdapGetDefaultTmId(ifIndex);

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Service_data.policer_prof_id,
										acm_mode,/* ACM_Mode */
										&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
		return MEA_ERROR;
	}

	if(sVlanMap->u1CepUntag != ADAP_TRUE)
	{
		Service_key.src_port         = phyPort;
		Service_key.pri              = 7;
		Service_key.net_tag          = sVlanMap->CVlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
	}
	else
	{
		Service_key.net_tag          = 0xff000;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
	}


	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.inner_netTag_from_valid          = MEA_FALSE;
	Service_key.inner_netTag_from_value          = 0xFFFFFFF;
	Service_key.evc_enable 						= MEA_FALSE;
	//Service_data.SRV_mode						= 1;

	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	//Service_editing.num_of_entries = 1;

	Service_editing.ehp_info       = &Service_editing_info[0];
	Service_editing.num_of_entries=0;

	for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
	{
		if(pBridgeDomain->ifIndex[ports] == ifIndex)
		{
			continue;
		}
		if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
			return MEA_ERROR;
		}

		AdapGetProviderCoreBridgeMode(&PortType,pBridgeDomain->ifIndex[ports]);

		if( (PortType == ADAP_PROVIDER_NETWORK_PORT) || (PortType == ADAP_CNP_STAGGED_PORT) )
		{
			if(sVlanMap->u1PepUntag == ADAP_TRUE )
			{
				if(sVlanMap->u1CepUntag != ADAP_TRUE)
				{
					//swap
					bEditing[ADAP_EHP_SWAP_SVLAN_EDITING] = ADAP_TRUE;
					MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
					MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_SWAP_SVLAN_EDITING].output_info,phyOutputPort);
				}
				else
				{
					//append S-VLAN
					bEditing[ADAP_EHP_APPEND_STAG_TO_UNTAGGED_EDITING] = ADAP_TRUE;
					MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
					MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_STAG_TO_UNTAGGED_EDITING].output_info,phyOutputPort);
				}
			}
			else //sVlanMap->u1PepUntag != ADAP_TRUE
			{
				if(sVlanMap->u1CepUntag != ADAP_TRUE)
				{
					// append
					bEditing[ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING] = ADAP_TRUE;
					MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
					MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING].output_info,phyOutputPort);
				}
				else
				{
					// double append
					bEditing[ADAP_EHP_APPEND_STAG_AND_CVLAN_EDITING] = ADAP_TRUE;
					MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
					MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_APPEND_STAG_AND_CVLAN_EDITING].output_info,phyOutputPort);
				}
			}
		}
		else if(PortType == ADAP_CNP_PORTBASED_PORT)
		{
			//swap
			bEditing[ADAP_EHP_SWAP_CVLAN_EDITING] = ADAP_TRUE;
			MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
			MEA_SET_OUTPORT(&ehp_editing[ADAP_EHP_SWAP_CVLAN_EDITING].output_info,phyOutputPort);
		}
	}
	for(index=0;index<ADAP_EHP_LAST_EDITING;index++)
	{
		if(bEditing[index] == ADAP_TRUE)
		{
			adap_memcpy(&Service_editing.ehp_info[Service_editing.num_of_entries++],&ehp_editing[index],sizeof(MEA_EHP_Info_dbt));
		}
	}


	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	MeaAdapSetDefaultTmId(Action_data.tm_id,ifIndex);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->inPort=phyPort;
	pServiceAlloc->ifIndex=ifIndex;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	// add the service to bridge domain
	if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	return MEA_OK;

}

MEA_Status MeaAdapGetLpmStatus();

MEA_Status MeaDrvCreateVlanBridging(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_AcmMode_t                         acm_mode;
    ADAP_Uint32						      ports;
    ADAP_Uint32						      masterPort = 0;
    ADAP_Uint32						      bridge_masterPort = 0;
    tServiceDb							  *pServiceAlloc=NULL;
    MEA_Port_t 							  phyPort;
    MEA_Port_t 							  phyOutputPort;

    MEA_Policer_Entry_dbt          		  Action_policer;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    ADAP_Bool							  found=ADAP_TRUE;
    MEA_LxCp_t 					  		  lxcp=MEA_PLAT_GENERATE_NEW_ID;
    ADAP_Uint8 							  IntType=0;
    ADAP_Uint32 						  defPriority;
    tMeterDb 						  	  *pMeterDb=NULL;
	tDbaseHwAggEntry 					*pLagEntry=NULL;
	MEA_Port_t                            EnetPortId;
	ADAP_Uint8 IngIntfType=0;
	ADAP_Uint16 InnerVlan=0;
	tIpInterfaceTable *pInterfaceTable=NULL;
	ADAP_Uint32 		modeType=0;

    /* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
	adap_memset(&Service_editing,0,sizeof(Service_editing));

	adap_memset(&Action_policer,0,sizeof(Action_policer));
	adap_memset(&Action_data,0,sizeof(Action_data));
	adap_memset(&Action_outPorts,0,sizeof(Action_outPorts));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	adap_memset(&Action_editing,0,sizeof(Action_editing));

	acm_mode = 0;
	ports=0;
	phyPort=0;

	if(adap_IsPortChannel(ifIndex) == ENET_SUCCESS)
	{
		if( adap_NumOfLagPortFromAggrId(ifIndex)  > 0)
		{
			pLagEntry = adap_getLagInstanceFromAggrId(ifIndex);
			if (pLagEntry != NULL)
			{
				masterPort = pLagEntry->masterPort;
				phyPort = pLagEntry->lagVirtualPort;
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lag entry by AggrId:%d\r\n", ifIndex);
				return MEA_ERROR;
			}
		}
		else
		{
			return MEA_OK;
		}
	}
	else
	{
		if(MeaAdapGetPhyPortFromLogPort(ifIndex,&phyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
			return MEA_ERROR;
		}
	}

	if(pBridgeDomain->meterId[ifIndex] != 0)
	{
		pMeterDb=MeaAdapGetMeterParams(pBridgeDomain->meterId[ifIndex]);
	}

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
		(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
	{
		AdapGetProviderCoreBridgeMode(&modeType,ifIndex);
	}

	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		// configure the actionId
		Action_data.output_ports_valid = MEA_TRUE;
		Action_data.force_color_valid  = MEA_TRUE;
		Action_data.COLOR              = 2;
		Action_data.force_cos_valid    = MEA_FALSE;
		Action_data.COS                = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;

		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_FALSE;
		Action_data.tm_id              = 0;
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE;


		if(!masterPort)
		{
			MEA_SET_OUTPORT(&Action_outPorts, phyPort);
			MEA_SET_OUTPORT(&Action_editing_info.output_info, phyPort);
		}

		Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
		Action_editing.ehp_info = &Action_editing_info;

		if(MeaAdapGetNumOfPhyPorts() > ifIndex)
		{
			Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(masterPort ? masterPort : ifIndex);
		}
		else
		{
			Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(1);
		}

		if(pMeterDb != NULL)
		{
			Action_data.policer_prof_id = pMeterDb->policer_prof_id;
		}



		if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
											Action_data.policer_prof_id,
											acm_mode,/* ACM_Mode */
											&Action_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler ifIndex:%d\n",ifIndex);
		    goto clear;
        }

		if(type == VLAN_UNTAGGED_MEMBER_PORT)
		{
			Action_editing.ehp_info->ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			Action_editing.ehp_info->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
		}
		else
		{
			Action_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			if ((modeType == ADAP_PROVIDER_NETWORK_PORT) && (type == VLAN_TAGGED_MEMBER_PORT))
			{
				Action_editing.ehp_info->ehp_data.eth_info.val.all = 0x88a80000 | pBridgeDomain->vlanId;
				Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
			}
			else
			{
				Action_editing.ehp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | pBridgeDomain->vlanId;
				Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
			}
			Action_editing.ehp_info->ehp_data.EditingType        	  = MEA_EDITINGTYPE_APPEND;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_priority            = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_info.stamp_color               = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
			Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		}

		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;
		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts,
								   &Action_policer,
								   &Action_editing,
								   &pServiceAlloc->actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Create_Action\n");
		    goto clear;
        }
	}
	if(MeaAdapGetNumOfPhyPorts() > ifIndex)
	{
		Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
	}
	else
	{
		Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(1);
	}

	if(pMeterDb != NULL)
	{
		Service_data.policer_prof_id = pMeterDb->policer_prof_id;
	}

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Service_data.policer_prof_id,
										acm_mode,/* ACM_Mode */
										&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
	    goto clear;
    }



	/* Set the service key */
	Service_key.src_port         = phyPort;
	Service_key.pri              = 7;
	if( type == VLAN_UNTAGGED_MEMBER_PORT)
	{
		Service_key.net_tag          = 0xff000;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
	}
	else
	{
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
	}

	if ((modeType == ADAP_PROVIDER_NETWORK_PORT) && (type == VLAN_TAGGED_MEMBER_PORT))
	{
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Stag;
	}

	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.inner_netTag_from_valid          = MEA_FALSE;
	Service_key.inner_netTag_from_value          = 0xFFFFFFF;
	Service_key.evc_enable 						= MEA_FALSE;
	//Service_data.SRV_mode						= 1;
	Service_key.virtual_port = masterPort ? MEA_TRUE : MEA_FALSE;


	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		/* Build the  service data attributes */
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable = MEA_TRUE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_TRUE;

		if(pBridgeDomain->learn_without_action == ADAP_FALSE)
		{
			Service_data.DSE_learning_actionId   = pServiceAlloc->actionId;
			Service_data.DSE_learning_actionId_valid = MEA_TRUE;
		}
		else
		{
			Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		}

		if (masterPort)
		{
			if(MeaAdapGetPhyPortFromLogPort(masterPort,&EnetPortId) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",masterPort);
			    goto clear;
            }

			Service_data.DSE_learning_srcPort	 = EnetPortId; //TODO, should be cluster!
		}
		else
		{
			Service_data.DSE_learning_srcPort	 = phyPort; //TODO, should be cluster!
		}
		Service_data.DSE_learning_srcPort_force = MEA_TRUE;
	}
	else
	{
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_FALSE;
		Service_data.DSE_learning_actionId   = 0;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.DSE_learning_srcPort	 = 0;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	}

	/* Set the service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_FALSE;
	Service_data.COLOR              = 0;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	Service_data.tmId            = 0; // create tm per port
	Service_data.vpn			= pBridgeDomain->vpn;

	//MeaAdapGetIntType(&IntType,ifIndex);

	if(pMeterDb != NULL)
	{
		Service_data.tmId = pMeterDb->tm_Id;
	}
        else
	{
		Service_data.tmId = MeaAdapGetDefaultTmId(masterPort != 0 ? masterPort : ifIndex);
	}



	// only if this is L3 interface
	if(AdapIsVlanIpInterface(pBridgeDomain->vlanId) == ENET_SUCCESS)
	{
		pInterfaceTable = AdapGetIpInterfaceByVlanId(pBridgeDomain->vlanId);
		if (pInterfaceTable != NULL)
		{
			InnerVlan = AdapL3InterfaceGetInnerVlan (pInterfaceTable->u4CfaIfIndex);
			if (InnerVlan != 0)
			{
				Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Stag_Ctag;
				Service_key.net_tag          = pBridgeDomain->vlanId;
				Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
				Service_key.inner_netTag_from_valid          = MEA_TRUE;
				Service_key.inner_netTag_from_value          = InnerVlan;
				Service_key.inner_netTag_from_value          |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;

				/* create L2 service with double tag also */
                                if(MeaDrvCreateInnerVlanL2Service(ifIndex, pBridgeDomain,InnerVlan) != MEA_OK)
				{
				    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create double tag L2 service \r\n");
				}
			}
		}
		Service_data.DSE_mask_field_type=MEA_DSE_MASK_IP_32;
		Service_data.L3_L4_fwd_enable = MEA_TRUE;
		Service_data.L4port_mask_enable	= MEA_TRUE;
		Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_L3;
		Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	        Service_data.DSE_learning_srcPort	 = 0;
	        Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	}

	if (AdapIsPppInterface (ifIndex) == ENET_SUCCESS)
	{
		Service_data.ADM_ENA            = MEA_TRUE;
#ifdef PPP_MAC
		Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
#else
		Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
#endif
        if ((Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Ctag) ||
                    (Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Stag))
        {
            Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_L3;
        }
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.L4port_mask_enable	     = MEA_FALSE;
		Service_data.DSE_learning_actionId   = 0;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.L3_L4_fwd_enable = MEA_FALSE;
		Service_data.DSE_learning_srcPort	 = 0;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
		Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL;
	}


	AdapGetUserPriority(ifIndex,&defPriority);



	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	//Service_editing.num_of_entries = 1;

	Service_editing.ehp_info       = &Service_editing_info[0];
	Service_editing.num_of_entries=0;

	if(getDefaultVlanId() == pBridgeDomain->vlanId)
	{
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}

			if(adap_IsPortChannel(pBridgeDomain->ifIndex[ports]) == ENET_SUCCESS)
			{
				bridge_masterPort = adap_GetLagMasterPort(pBridgeDomain->ifIndex[ports]);
				if( bridge_masterPort != ADAP_END_OF_TABLE)
				{
					if(MeaAdapGetPhyPortFromLogPort(bridge_masterPort, &phyOutputPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from masterPort:%d\r\n", pLagEntry->masterPort);
						goto clear;
					}
				}
				else
				{
					continue;
				}
			}
			else
			{
				if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
					goto clear;
				}
			}

			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
			MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
			found=ADAP_TRUE;
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
	}
	else if((Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Untagged) &&
		(Service_key.sub_protocol_type != MEA_PARSING_L2_Sub_Ctag_protocol_L3))
	{
		found=ADAP_FALSE;
		/* this source port  untagged and send to untagged */
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}
			if(pBridgeDomain->ifType[ports] == VLAN_UNTAGGED_MEMBER_PORT)
			{

				if(adap_IsPortChannel(pBridgeDomain->ifIndex[ports]) == ENET_SUCCESS)
				{
					bridge_masterPort = adap_GetLagMasterPort(pBridgeDomain->ifIndex[ports]);
					if( bridge_masterPort != ADAP_END_OF_TABLE)
					{
						if(MeaAdapGetPhyPortFromLogPort(bridge_masterPort, &phyOutputPort) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from masterPort:%d\r\n", pLagEntry->masterPort);
							goto clear;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
						goto clear;
					}
				}

				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
				found=ADAP_TRUE;
			}
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
		/* this source port  untagged and send to untagged */
		found=ADAP_FALSE;
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}
			if(pBridgeDomain->ifType[ports] == VLAN_TAGGED_MEMBER_PORT)
			{
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = ADAP_ETHERTYPE | pBridgeDomain->vlanId;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

				if(adap_IsPortChannel(pBridgeDomain->ifIndex[ports]) == ENET_SUCCESS)
				{
					bridge_masterPort = adap_GetLagMasterPort(pBridgeDomain->ifIndex[ports]);
					if( bridge_masterPort != ADAP_END_OF_TABLE)
					{
						if(MeaAdapGetPhyPortFromLogPort(bridge_masterPort, &phyOutputPort) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from masterPort:%d\r\n", pLagEntry->masterPort);
							goto clear;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
						goto clear;
					}
				}

				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);

				found=ADAP_TRUE;
			}
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
	}
	else if((Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Ctag) &&
		(Service_key.sub_protocol_type != MEA_PARSING_L2_Sub_Ctag_protocol_L3))
	{
		found=ADAP_FALSE;
		/* this source port  untagged and send to untagged */
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}
			if(pBridgeDomain->ifType[ports] == VLAN_UNTAGGED_MEMBER_PORT)
			{
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;

				if(adap_IsPortChannel(pBridgeDomain->ifIndex[ports]) == ENET_SUCCESS)
				{
					bridge_masterPort = adap_GetLagMasterPort(pBridgeDomain->ifIndex[ports]);
					if( bridge_masterPort != ADAP_END_OF_TABLE)
					{
						if(MeaAdapGetPhyPortFromLogPort(bridge_masterPort, &phyOutputPort) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from masterPort:%d\r\n", pLagEntry->masterPort);
							goto clear;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
						goto clear;
					}
				}

				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
				found=ADAP_TRUE;
			}
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
		found=ADAP_FALSE;
		/* this source port  untagged and send to untagged */
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}
			if(pBridgeDomain->ifType[ports] == VLAN_TAGGED_MEMBER_PORT)
			{
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

				if(adap_IsPortChannel(pBridgeDomain->ifIndex[ports]) == ENET_SUCCESS)
				{
					bridge_masterPort = adap_GetLagMasterPort(pBridgeDomain->ifIndex[ports]);
					if( bridge_masterPort != ADAP_END_OF_TABLE)
					{
						if(MeaAdapGetPhyPortFromLogPort(bridge_masterPort, &phyOutputPort) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from masterPort:%d\r\n", pLagEntry->masterPort);
							goto clear;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",pBridgeDomain->ifIndex[ports]);
						goto clear;
					}
				}

				MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
				found=ADAP_TRUE;
			}
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
	}

	if(getLxcpId(masterPort ? masterPort : ifIndex, &lxcp) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
	    goto clear;
    }

	if(Service_editing.num_of_entries == 0)
	{
		if( (AdapIsVlanIpInterface(pBridgeDomain->vlanId) == ENET_SUCCESS) || (IntType == ADAP_SET_TO_WAN) )

		{
#ifdef LPM_ADAPTOR_WANTED
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.atm_info.val.all =
					(MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_LPM_VLANID);
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.atm_info.double_tag_cmd =
					MEA_EGRESS_HEADER_PROC_CMD_TRANS;

			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command =
					MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all =ADAP_ETHERTYPE;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = 1;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc  = 12;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid  = MEA_TRUE;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc    = 13;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid  = MEA_TRUE;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc    = 14;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid  = MEA_TRUE;
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc    = 15;

			Service_data.vpn			= START_L3_VPN;
			MEA_SET_OUTPORT(&Service_outPorts, ADAP_CPU_PORT_ID);
			MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,ADAP_CPU_PORT_ID);
#endif
			if (AdapGetNATRouterStatus() == ADAP_TRUE)
			{
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.atm_info.val.all =
					(MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_VLANID);
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.atm_info.double_tag_cmd =
					MEA_EGRESS_HEADER_PROC_CMD_TRANS;

				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command =
					MEA_EGRESS_HEADER_PROC_CMD_TRANS;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all =ADAP_ETHERTYPE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = 1;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc  = 12;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid  = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc    = 13;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid  = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc    = 14;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid  = MEA_TRUE;
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc    = 15;

				Service_data.vpn			= START_L3_VPN;
				MEA_SET_OUTPORT(&Service_outPorts, ADAP_CPU_PORT_ID);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,ADAP_CPU_PORT_ID);
			}
			else
			{
				// send packet to internal port
				if (InnerVlan != 0)
				{
					Service_data.vpn			= START_L3_VPN1;
				}
				else
				{
					if (AdapIsPppInterface (ifIndex) != ENET_SUCCESS)
					{
						Service_data.vpn			= START_L3_VPN;
					}
				}
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.EditingType = 
						MEA_EDITINGTYPE_NORMAL;
				MEA_SET_OUTPORT(&Service_outPorts, INTERNAL_PORT1_NUMBER);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,INTERNAL_PORT1_NUMBER);
				Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
			}


			if (AdapGetNATRouterStatus() == ADAP_TRUE)
			{
				MeaAdapGetIntType(&IngIntfType, ifIndex);
				if (IngIntfType == ADAP_SET_TO_WAN)
				{
					Service_data.DSE_forwarding_key_type = 
							MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
					Service_data.vpn			= VPN_NETWORK_TO_HOST;
				}
				else
				{
					Service_data.DSE_forwarding_key_type = 
							MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
					Service_data.vpn			= VPN_HOST_TO_NETWORK;
				}
			}
#ifdef	MEA_NAT_DEMO
			if(ifIndex == NAT_LAN_IFINDEX)
			{
				Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
				Service_data.vpn			= VPN_HOST_TO_NETWORK;
			}
			else
			{
				Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
			}
#endif

		}
		Service_editing.num_of_entries++;
	}

	Service_data.LxCp_enable = 1;
	Service_data.LxCp_Id = lxcp;

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
	    goto clear;
    }

	if(pMeterDb != NULL)
	{
		pMeterDb->tm_Id = Service_data.tmId;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->inPort=phyPort;
	pServiceAlloc->ifIndex=ifIndex;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->pmId = Service_data.pmId;
	pBridgeDomain->pmId[ifIndex] = Service_data.pmId;
	pServiceAlloc->vpnId = Service_data.vpn;

	// add the service to bridge domain
	if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",pServiceAlloc->serviceId);
	    goto clear;
    }

	if((Service_key.sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3)  &&
		(AdapIsPppInterface (ifIndex) != ENET_SUCCESS))
	{
		if (InnerVlan != 0)
		{
		    Service_data.vpn			= START_L3_VPN1;
		}
		else
		{
		    Service_data.vpn			= START_L3_VPN;
		}
		MeaDrvVlanCreateL3InternalService (INTERNAL_PORT1_NUMBER,INTERNAL_PORT2_NUMBER,MEA_DSE_MASK_IP_24,
							&Service_key, Service_data.vpn, pBridgeDomain);
		MeaDrvVlanCreateL3InternalService (INTERNAL_PORT2_NUMBER,INTERNAL_PORT3_NUMBER,MEA_DSE_MASK_IP_16,
							&Service_key, Service_data.vpn, pBridgeDomain);
		MeaDrvVlanCreateL3InternalService (INTERNAL_PORT3_NUMBER,ADAP_CPU_PORT_ID,MEA_DSE_MASK_IP_8,
							&Service_key, Service_data.vpn, pBridgeDomain);
	}

	return MEA_OK;
clear:
    if (pServiceAlloc)
    {
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"cleared fail service \r\n");
	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
        pServiceAlloc->valid = MEA_FALSE;
	if(pServiceAlloc->actionId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pServiceAlloc->actionId) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"deleted actionId:%d\r\n",pServiceAlloc->actionId);
		}
		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;
	}
    }
    return MEA_ERROR;
}


MEA_Status MeaDrvVlanCreateL3InternalService(MEA_Port_t enetInPutPort,MEA_Port_t enetOutPutPort,
						MEA_Uint32 DSE_mask_field_type,
						MEA_Service_Entry_Key_dbt *pService_key,MEA_Uint32 vpn,
						tBridgeDomain *pBridgeDomain) 
{
	MEA_Service_Entry_Key_dbt             Service_key;
	MEA_Service_Entry_Data_dbt            Service_data;
	MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_OutPorts_Entry_dbt                Service_outPorts;
	MEA_EHP_Info_dbt                      Service_editing_info;
	MEA_Policer_prof_t		      policer_prof_id=MEA_PLAT_GENERATE_NEW_ID;
	tServiceDb			     *pServiceAlloc=NULL;
        MEA_Bool                              ServiceExists;
        MEA_Status                            result = MEA_OK;
        MEA_Uint16 			      profId = 0;
        mea_Ingress_Flow_policer_dbt          Flow_policer_entry;

	adap_memset(&Service_key,0,sizeof(MEA_Service_Entry_Key_dbt));
	adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Service_outPorts , 0 , sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_editing_info , 0 , sizeof(MEA_EHP_Info_dbt));

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	Service_key.src_port         = enetInPutPort;
	Service_key.net_tag          = pService_key->net_tag;
	Service_key.L2_protocol_type = pService_key->L2_protocol_type;
	Service_key.inner_netTag_from_valid = pService_key->inner_netTag_from_valid;
	Service_key.inner_netTag_from_value = pService_key->inner_netTag_from_value;
	Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_L3;

	Service_key.evc_enable             = MEA_FALSE;
	Service_key.pri              = 7;

	Service_data.DSE_forwarding_enable   = MEA_TRUE;
	Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	Service_data.DSE_mask_field_type=DSE_mask_field_type;
	Service_data.L3_L4_fwd_enable = MEA_TRUE;
	Service_data.L4port_mask_enable	= MEA_TRUE;
	Service_data.vpn = vpn;

	/* Set the upstream service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the upstream service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the upstream service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_FALSE;
	Service_data.COLOR              = 0;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;


	/* Attach the default Policer to the service */
	policer_prof_id = MeaAdapGetDefaultPolicer(1);
	if (policer_prof_id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error: MeaAdapGetDefaultPolicer FAIL\n",__FILE__);
	    goto clear;
        }

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
				policer_prof_id,
				0,/* ACM_Mode */
				&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
	    goto clear;
        }
	Service_data.policer_prof_id = policer_prof_id;



	if(enetOutPutPort != NULL_OUTPUT_PORT)
	{
		MEA_SET_OUTPORT(&Service_editing_info.output_info,enetOutPutPort);
		Service_editing.ehp_info = &Service_editing_info;
		Service_editing.num_of_entries=1;
		MEA_SET_OUTPORT(&Service_outPorts,enetOutPutPort);
	}
	else
	{
		Service_editing.num_of_entries=0;
		Service_editing.ehp_info = &Service_editing_info;
	}

	Service_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	Service_editing.ehp_info->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	Service_editing.ehp_info->ehp_data.eth_info.val.all	      = 0;

	Service_editing.ehp_info->ehp_data.eth_info.stamp_color       = MEA_FALSE;
	Service_editing.ehp_info->ehp_data.eth_info.stamp_priority    = MEA_FALSE;

	if(enetOutPutPort == ADAP_CPU_PORT_ID)
	{
		Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
		Service_editing.ehp_info->ehp_data.atm_info.val.all =
			(MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_VLANID);
		Service_editing.ehp_info->ehp_data.atm_info.double_tag_cmd =
			MEA_EGRESS_HEADER_PROC_CMD_TRANS;

		Service_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
		Service_editing.ehp_info->ehp_data.eth_info.val.all =ADAP_ETHERTYPE;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = 1;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc  = 12;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid  = MEA_TRUE;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc    = 13;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid  = MEA_TRUE;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc    = 14;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid  = MEA_TRUE;
		Service_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc    = 15;

		MEA_SET_OUTPORT(&Service_outPorts, ADAP_CPU_PORT_ID);
		MEA_SET_OUTPORT(&Service_editing.ehp_info->output_info,ADAP_CPU_PORT_ID);
	}
	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

    /* check if service is already created */

    if (ENET_ADAPTOR_IsExist_Service_ByKey(MEA_UNIT_0, &Service_key, &ServiceExists,
            &pServiceAlloc->serviceId) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "Unable to check service existence");
        goto clear;
    }

    if (ServiceExists == MEA_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Service is already exists. We don't need to create\n");
        goto clear_success;
    }

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
	/* Create new service for the given Vlan. */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
				&Service_key,
				&Service_data,
				&Service_outPorts,
				&Service_policer,
				&Service_editing,
				&pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to create the new service %d\n",pServiceAlloc->serviceId);
		/*T.B.D. Rollback some of the previous actions if fail! */
	    goto clear;
    }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service id:%d created\n",pServiceAlloc->serviceId);

	pServiceAlloc->inPort=enetInPutPort;
	pServiceAlloc->ifIndex=enetInPutPort;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	// add the service to bridge domain
	if (pBridgeDomain != NULL)
	{
	    if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	    {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",
					pServiceAlloc->serviceId);
	        goto clear;
        }
    }

        if(enetOutPutPort == ADAP_CPU_PORT_ID)
        {
            adap_memset(&Flow_policer_entry ,0,sizeof(mea_Ingress_Flow_policer_dbt));

            profId = MEA_PLAT_GENERATE_NEW_ID;

            if(MEA_API_Create_Ingress_flow_policerId(MEA_UNIT_0, &profId) !=MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Create_Ingress_flow_policer failed - profId:%d\r\n", profId);
            }

            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].tmId = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].tmId_enable =  MEA_PORT_RATE_MEETRING_ENABLE_DEF_VAL;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.CIR = 65000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.EIR = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.CBS = 32000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.EBS = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.cup = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.color_aware = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.comp = 0;

            if (MEA_API_Set_Ingress_flow_policer(MEA_UNIT_0, profId, &Flow_policer_entry) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Ingress_flow_policer failed - profId:%d\r\n", profId);
            }           

            adap_SetFlowPolicerProfile(enetInPutPort, profId);
	    if(MeaDrvSetFlowPolicerProfile (enetInPutPort, pServiceAlloc->serviceId, profId) != MEA_OK)
	    {
	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set flowMarkProfile:%d to service:%d\r\n", profId, pServiceAlloc->serviceId);
            }           
        }
	return MEA_OK;
clear:
    result = MEA_ERROR;
clear_success:
    if (pServiceAlloc)
    {
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"cleared fail service \r\n");
	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
        pServiceAlloc->valid = MEA_FALSE;
	if(pServiceAlloc->actionId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pServiceAlloc->actionId) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"deleted actionId:%d\r\n",pServiceAlloc->actionId);
		}
		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;
	}
    }
    return result;
}


MEA_Status MeaDrvVlanHwSetL3InternalService(MEA_Port_t enetInPutPort,MEA_Port_t enetOutPutPort,MEA_Uint32 DSE_mask_field_type,MEA_Service_t *pService_id,MEA_Uint32 vpn)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Port_t                            EnetPortId;
	MEA_OutPorts_Entry_dbt                Service_outPorts;
	MEA_EHP_Info_dbt                      Service_editing_info;
	MEA_Policer_prof_t 					  policer_prof_id=MEA_PLAT_GENERATE_NEW_ID;
	tServiceDb							  *pServiceAlloc=NULL;

	adap_memset(&Service_key,0,sizeof(MEA_Service_Entry_Key_dbt));
	adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Service_outPorts , 0 , sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_editing_info , 0 , sizeof(MEA_EHP_Info_dbt));

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}



	EnetPortId = enetInPutPort;

	Service_key.src_port         = EnetPortId;
	Service_key.net_tag          = 0;
        Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;

        Service_key.L2_protocol_type =  MEA_PARSING_L2_KEY_Untagged;
	Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_L3;

	Service_key.evc_enable             = MEA_FALSE;
	Service_key.pri              = 7;
	//Service_data.SRV_mode		=1;

	Service_data.DSE_forwarding_enable   = MEA_TRUE;
	Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	Service_data.DSE_mask_field_type=DSE_mask_field_type;
	Service_data.L3_L4_fwd_enable = MEA_TRUE;
	Service_data.L4port_mask_enable	= MEA_TRUE;

	Service_data.vpn = vpn;

    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;

//	Service_data.LxCp_enable = 1;
//	Service_data.LxCp_Id = DPLGetPortLxCpId(u4IfIndex);


    /* Attach the default Policer to the service */
    policer_prof_id = MeaAdapGetDefaultPolicer(1);
    if (policer_prof_id == MEA_PLAT_GENERATE_NEW_ID)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error: MeaAdapGetDefaultPolicer FAIL\n",__FILE__);
        return MEA_ERROR;
    }

     if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
    		 	 	 	 	 	 	 	 policer_prof_id,
                                        0,/* ACM_Mode */
                                        &Service_policer) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
        return MEA_ERROR;
    }
    Service_data.policer_prof_id = policer_prof_id;



	if(enetOutPutPort != NULL_OUTPUT_PORT)
	{
		MEA_SET_OUTPORT(&Service_editing_info.output_info,enetOutPutPort);
		Service_editing.ehp_info = &Service_editing_info;
		Service_editing.num_of_entries=1;
		MEA_SET_OUTPORT(&Service_outPorts,enetOutPutPort);
	}
	else
	{
		Service_editing.num_of_entries=0;
		Service_editing.ehp_info = &Service_editing_info;
	}

	Service_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	Service_editing.ehp_info->ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	Service_editing.ehp_info->ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid      = 0;

	Service_editing.ehp_info->ehp_data.eth_info.stamp_color       = MEA_FALSE;
	Service_editing.ehp_info->ehp_data.eth_info.stamp_priority    = MEA_FALSE;


	if(enetOutPutPort == 127)
	{
		Service_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		Service_editing.ehp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | EnetPortId;
		Service_editing_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		Service_editing_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

   /* Create new service for the given Vlan. */
    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &Service_outPorts,
                               &Service_policer,
                               &Service_editing,
                               &pServiceAlloc->serviceId) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to create the new service %d\n",pServiceAlloc->serviceId);
        /*T.B.D. Rollback some of the previous actions if fail! */
        return MEA_ERROR;
    }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service id:%d created\n",pServiceAlloc->serviceId);

	pServiceAlloc->inPort=enetInPutPort;
	pServiceAlloc->ifIndex=enetInPutPort;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	(*pService_id) = pServiceAlloc->serviceId;

	MeaDrvHwSetDefaultService (EnetPortId,(*pService_id), MEA_DEF_SID_ACTION_DEF_SID);

	return MEA_OK;
}

MEA_Status MeaDrvHwSetAction(MEA_Action_t action_Id, tEnetHal_HwPortArray * pHwMcastPorts)
{
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;
	MEA_Port_t								enetOutPort;
	ADAP_Int32		 						index=0;

	adap_memset(&Action_data     , 0 , sizeof(Action_data    ));
    adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
    adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
    adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
    Action_editing.num_of_entries = 1;
    Action_editing.ehp_info = &Action_editing_info;

    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
								action_Id,
								&Action_data,
							    &Action_outPorts,
							    &Action_policer,
							    &Action_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Get_Action action_Id:%d\n", action_Id);
		return ENET_FAILURE;
	}

    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	for(index = 0; index < pHwMcastPorts->i4Length; index++)
	{
		MeaAdapGetPhyPortFromLogPort(pHwMcastPorts->pu4PortArray[index],&enetOutPort);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enetOutPort:%d\n", enetOutPort);
		MEA_SET_OUTPORT(&Action_outPorts,enetOutPort);
	}
	Action_data.ed_id=0;

	if (ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
								action_Id,
								&Action_data,
							    &Action_outPorts,
							    &Action_policer,
							    &Action_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Set_Action action_Id:%d\n", action_Id);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d set\n", action_Id);

	return MEA_OK;
}

static MEA_Status MeaDrvHwCreateAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,
										int num_of_entries,MEA_OutPorts_Entry_dbt *pOutPorts,
										MEA_PmId_t *pPm_id,MEA_Action_Entry_Data_dbt *pAction_data,
										MEA_Uint16 UEPDN_Id, MEA_Uint8 uepdn_Qos,
										MEA_Policer_prof_t policer_prof_id)
{
	MEA_Action_Entry_Data_dbt               Action_data;
	MEA_Policer_Entry_dbt                   Action_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	mea_TFT_Action_rule_t 					entry;

	adap_memset(&Action_data,0,sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&Action_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Action_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	(*pAction_id) = MEA_PLAT_GENERATE_NEW_ID;

	if(pAction_data == NULL)
	{
		Action_data.proto_llc_valid = MEA_TRUE;
		Action_data.Llc = 0;
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.Protocol = 1;
		Action_data.ed_id_valid  =  1;
		//Action_data.ed_id        =  0;
		MEA_API_Get_EditingFree_id(MEA_UNIT_0, &Action_data.ed_id);
		Action_data.pm_id_valid = 1;
		Action_data.pm_id        =  0;
		Action_data.fwd_ingress_TS_type = MEA_INGGRESS_TS_TYPE_NONE;
		Action_data.tm_id        =  0;
		Action_data.tm_id_valid=0;
	}
	else
	{
		adap_memcpy(&Action_data,pAction_data,sizeof(MEA_Action_Entry_Data_dbt));
	}

    adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
    Action_data.tm_id              = 0;
    if(policer_prof_id != MEA_PLAT_GENERATE_NEW_ID)
    {
	    Action_data.policer_prof_id = policer_prof_id;

	    if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
				    Action_data.policer_prof_id,
				    0,/* ACM_Mode */
				    &Action_policer) != MEA_OK)
	    {
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
		    return MEA_ERROR;
	    }
	    Action_data.tm_id_valid=1;
    }


	Action_editing.ehp_info = pEhp_info;
	Action_editing.num_of_entries = num_of_entries;

	if(Action_data.Action_type == MEA_ACTION_TYPE_HPM)
	{
		entry.action_params = &Action_data;
		entry.OutPorts_Entry = pOutPorts;
		entry.Policer_Entry = &Action_policer;
		entry.EHP_Entry = &Action_editing;

		if(ENET_ADAPTOR_Create_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, uepdn_Qos,&entry)!= MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_UEPDN_TEID_ConfigAction failed for action:%d\n",(*pAction_id));
		   return MEA_ERROR;
		}
	}
	else
	{
		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   pOutPorts,
								   (Action_data.tm_id_valid == 1) ? &Action_policer : NULL,
								   &Action_editing,
								   pAction_id) != MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Create_Action failed for action:%d\n",(*pAction_id));
		   return MEA_ERROR;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"succeeded to create action:%d command:%d vlanid:%d num of entries:%d\n",
											(*pAction_id),
											Action_editing.ehp_info->ehp_data.eth_info.command,
											Action_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,
											Action_editing.num_of_entries);

	if(pPm_id != NULL)
	{
		(*pPm_id) = Action_data.pm_id;
	}
	return MEA_OK;
}

MEA_Status MeaDrvCreateMulticastAction(MEA_Action_t   *Action_id,MEA_Uint32 *outPorts,MEA_Uint32 num_of_ports)
{
	MEA_OutPorts_Entry_dbt	tOutPorts;
	MEA_EHP_Info_dbt 		tEhp_info;
	MEA_Uint32 				ports;
	MEA_Port_t				enetOutPort;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tEhp_info.ehp_data.eth_info.val.all 	 	= ADAP_ETHERTYPE;



	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	for(ports=0;ports<num_of_ports;ports++)
	{
		if (adap_IsPortChannel(outPorts[ports]) == ENET_SUCCESS)
		{
			if( (adap_NumOfLagPortFromAggrId(outPorts[ports])) > 0)
			{
				outPorts[ports] = adap_GetLagMasterPort(outPorts[ports]);
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"LAG Id:%d has no any physical ports\n", outPorts[ports]);
			}
		}

		MeaAdapGetPhyPortFromLogPort(outPorts[ports],&enetOutPort);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enetOutPort:%d\n", enetOutPort);
		MEA_SET_OUTPORT(&tEhp_info.output_info,enetOutPort);
		MEA_SET_OUTPORT(&tOutPorts, enetOutPort);
	}

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,NULL,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);
}


MEA_Status MeaDrvCreateActionToCpu(MEA_Action_t   *Action_id)
{
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_EHP_Info_dbt tEhp_info;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	// create action send to cpu
	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	//tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	//tEhp_info.ehp_data.eth_info.val.vlan.vid      = 0;
	tEhp_info.ehp_data.eth_info.val.all 	 	= ADAP_ETHERTYPE;

#ifdef USING_DOUBLE_TAG_ACTION
	tEhp_info.ehp_data.atm_info.val.all = 		ADAP_ETHERTYPE ;
	tEhp_info.ehp_data.atm_info.double_tag_cmd        = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
#endif


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info.output_info,127);
	MEA_SET_OUTPORT(&tOutPorts, 127);

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,NULL,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);
}



MEA_Status FsMiHwCreateLpmProfile(MEA_TFT_t  *tft_profile_Id)
{
	 MEA_TFT_entry_data_dbt entry;
	 adap_memset(&entry,0,sizeof(MEA_TFT_entry_data_dbt));

	return ENET_ADAPTOR_TFT_Create_QoS_Profile(MEA_UNIT_0,tft_profile_Id,&entry);
}

MEA_Status FsMiHwCreateRouterAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,int num_of_entries,
									MEA_OutPorts_Entry_dbt *pOutPorts,mea_action_type_te type,MEA_Uint16 UEPDN_Id,
									MEA_Uint8 index,MEA_Policer_prof_t policer_prof_id)
{
	MEA_Action_Entry_Data_dbt               Action_data;
	MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_OutPorts_Entry_dbt 					tOutPorts;

	adap_memset(&Action_data,0,sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&Action_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	adap_memcpy(&tOutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	(*pAction_id) = MEA_PLAT_GENERATE_NEW_ID;

	Action_data.proto_llc_valid = MEA_TRUE;
	Action_data.Llc = 0;
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.Protocol = 1;
	Action_data.ed_id_valid  =  1;
	//Action_data.ed_id        =  0;
	MEA_API_Get_EditingFree_id(MEA_UNIT_0, &Action_data.ed_id);
	Action_data.pm_id_valid = 1;
	Action_data.pm_id        =  0;
	Action_data.fwd_ingress_TS_type = MEA_INGGRESS_TS_TYPE_NONE;
	Action_data.tm_id        =  0;
	Action_data.tm_id_valid=0; /* TM ID disable for action */

	Action_data.Action_type=type;

	if(pEhp_info->ehp_data.EditingType == MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL)
	{
		Action_data.Protocol = 2;
		Action_data.Llc = 1;
	}

	if( (pEhp_info->ehp_data.EditingType == MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL) ||
		(pEhp_info->ehp_data.EditingType == MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL) ||
		(pEhp_info->ehp_data.EditingType == MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL) )
	{
		Action_data.Protocol = 2;
		Action_data.Llc = 0;
	}



//	ehp_info.ehp_data.eth_info.val.all        = 0;
//	ehp_info.ehp_data.eth_info.stamp_color    = 0;
//	ehp_info.ehp_data.eth_info.stamp_priority = 0;
//	ehp_info.ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

	Action_editing.ehp_info = pEhp_info;
	Action_editing.num_of_entries = num_of_entries;

	return MeaDrvHwCreateAction(pAction_id,pEhp_info,1,&tOutPorts,NULL,&Action_data,UEPDN_Id,index,policer_prof_id);
}


MEA_Status MeaDrvCreatePPPoEAction(MEA_Action_t   *Action_id, tEnetHal_MacAddr *srcMac, tEnetHal_MacAddr *destMac, ADAP_Uint32 editingtype,
					ADAP_Uint32 vlanId,ADAP_Uint32 sessionId,MEA_Port_t enetPort)
{
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_EHP_Info_dbt tEhp_info;
    	ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    	ADAP_Uint32         modeType = 0;
        ADAP_Uint32         logPort = 0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	tEhp_info.ehp_data.EditingType = editingtype;
	adap_memcpy(&tEhp_info.ehp_data.martini_info.DA, destMac, sizeof(tEnetHal_MacAddr));
	adap_memcpy(&tEhp_info.ehp_data.martini_info.SA, srcMac, sizeof(tEnetHal_MacAddr));

	tEhp_info.ehp_data.LmCounterId_info.valid   = MEA_TRUE;
	tEhp_info.ehp_data.LmCounterId_info.LmId    = 0 ;
	tEhp_info.ehp_data.LmCounterId_info.Command_dasa = 1 ; // routing mode 

        MeaAdapGetLogPortFromPhyPort (&logPort, enetPort);
	if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
			(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
	{
		AdapGetProviderCoreBridgeMode(&modeType,logPort);
		if (modeType == ADAP_PROVIDER_NETWORK_PORT)
		{
			EtherType = 0x88a8;
		}
	}
	if (editingtype == MEA_EDITINGTYPE_PPPoE_DL_Append_header_Extract_valn_Stamp_DA_SA)
	{
		tEhp_info.ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info.ehp_data.atm_info.val.all = (sessionId << 16);
	}
	else if (editingtype == MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA)
	{
		tEhp_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = EtherType;
		tEhp_info.ehp_data.eth_info.val.vlan.vid = vlanId;
	}
	else if (editingtype == MEA_EDITINGTYPE_PPPoE_DL_Append_header_Swap_vlan)
	{
		tEhp_info.ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info.ehp_data.atm_info.val.all = (sessionId << 16);
		tEhp_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = EtherType;
		tEhp_info.ehp_data.eth_info.val.vlan.vid = vlanId;
	}
	else if (editingtype == MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan)
	{
		tEhp_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = EtherType;
		tEhp_info.ehp_data.eth_info.val.vlan.vid = vlanId;
	}

	MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,NULL,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);
}

MEA_Status MeaDrvVxLanCreateAction(MEA_Action_t   *Action_id,tVxLanAdap *pVxLan,MEA_Port_t enetPort,MEA_PmId_t *pPm_id,MEA_Uint32 editingType)
{
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	MEA_EHP_Info_dbt 		tEhp_info;
//	MEA_Uint32				idx=0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	tEhp_info.ehp_data.VxLan_info.valid = MEA_TRUE;
	adap_mac_to_arr(&pVxLan->destMac, tEhp_info.ehp_data.VxLan_info.vxlan_Da.b);
	tEhp_info.ehp_data.VxLan_info.vxlan_dst_Ip = pVxLan->destIp;
	tEhp_info.ehp_data.VxLan_info.vxlan_vni = pVxLan->tunnel_id;
	tEhp_info.ehp_data.VxLan_info.l4_dest_Port = pVxLan->dest_port;
	tEhp_info.ehp_data.VxLan_info.l4_src_Port = pVxLan->source_port;
	tEhp_info.ehp_data.VxLan_info.ttl = pVxLan->ttl;
	tEhp_info.ehp_data.VxLan_info.sgw_ipId=enetPort-104; // according to source port

	if(editingType == ADAP_EHP_APPEND_VXLAN_AND_EXTRACT_VLAN_TAG)
	{
		tEhp_info.ehp_data.EditingType           = MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan;

	}
	else if(editingType == ADAP_EHP_APPEND_VXLAN)
	{
		tEhp_info.ehp_data.EditingType           = MEA_EDITINGTYPE_VXLAN_header;
	}


	// create action send to cpu

	MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,pPm_id,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);
}



MEA_Status MeaDrvNatCreateAction(MEA_Action_t   *Action_id,tEnetHal_MacAddr *destMac,MEA_Uint32 ipAddr,MEA_Uint16 l4Port,MEA_Bool userToNetwork,MEA_Port_t enetPort,MEA_PmId_t *pPm_id)
{
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	MEA_EHP_Info_dbt 		tEhp_info;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	tEhp_info.ehp_data.NAT_info.valid = MEA_TRUE;
	if(userToNetwork == MEA_TRUE)
	{
		tEhp_info.ehp_data.NAT_info.NAT_type=2; /*  2 - User_to_Network   3- Network_to_User*/
	}
	else
	{
		tEhp_info.ehp_data.NAT_info.NAT_type=3; /*  2 - User_to_Network   3- Network_to_User*/
	}

	adap_mac_to_arr(destMac, tEhp_info.ehp_data.NAT_info.Da.b);

	tEhp_info.ehp_data.NAT_info.Ip = ipAddr;
	if (l4Port)
	    tEhp_info.ehp_data.NAT_info.L4Port = l4Port;

	// create action send to cpu

	MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,pPm_id,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);
}



MEA_Status MeaDrvUpdateService (tUpdateServiceParam *pParams)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    ADAP_Bool 							  bSetService=ADAP_FALSE;
    ADAP_Uint32 						  ports=0;
    ADAP_Bool							  ehpAllocate=ADAP_FALSE;
	MEA_Uint16        					  TmIdToUpdate = ADAP_END_OF_TABLE;
	MEA_Uint16        					  PmIdToUpdate = ADAP_END_OF_TABLE;
	MEA_Uint16        					  EnetPolilcingProfile;

	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	adap_memset(&Service_editing,0,sizeof(Service_editing));


	if(ENET_ADAPTOR_Get_Service (MEA_UNIT_0,
								pParams->Service,
	                            &Service_key,
	                            &Service_data,
	                            &Service_outPorts,
	                            &Service_policer,
	                            &Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get first time serviceId:%d parameters\r\n",pParams->Service);
		return MEA_ERROR;
	}

	if(Service_editing.num_of_entries > 0)
	{
		Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

	    if (Service_editing.ehp_info == NULL )
	    {
	         ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate ehp_info\n");
	         return MEA_ERROR;
	    }
	    ehpAllocate=ADAP_TRUE;
	}

     if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
    		 	 	 	 	 pParams->Service,
    		 	 	 	 	 &Service_key,
                             &Service_data,
                             &Service_outPorts, /* outPorts */
                             &Service_policer, /* policer  */
                             &Service_editing) != MEA_OK)
     {
    	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get second time serviceId:%d parameters\r\n",pParams->Service);
    	 if (ehpAllocate == ADAP_TRUE )
    	 {
    		 MEA_OS_free(Service_editing.ehp_info);
    	 }
         return MEA_ERROR;
     }

     switch(pParams->type)
     {
     case E_UPDATE_FORWARDDING_TYPE:
		 Service_data.DSE_forwarding_enable = pParams->x.forwardType.DSE_forwarding_enable;
		 Service_data.DSE_forwarding_key_type = pParams->x.forwardType.forwadingKeyType;

//		 Service_data.DSE_learning_enable = pParams->x.forwardType.DSE_learning_enable;
//		 Service_data.DSE_learning_key_type = pParams->x.forwardType.learningKeyType;

//		 if(pParams->x.forwardType.DSE_learning_enable == MEA_FALSE)
//		 {
//			 Service_data.DSE_learning_actionId_valid = MEA_FALSE;
//		 }
		 bSetService = ADAP_TRUE;
    	 break;
     case E_UPDATE_LPM_ROUTING_PARAMETERS:
		 Service_data.Uepdn_valid=pParams->x.lpm.Uepdn_valid;
		 Service_data.Uepdn_id = pParams->x.lpm.hpmId;
		 Service_data.HPM_valid = pParams->x.lpm.HPM_valid;
		 Service_data.HPM_profileId = pParams->x.lpm.HPM_profileId;
		 Service_data.HPM_prof_mask = pParams->x.lpm.HPM_prof_mask;
		 Service_data.vpn = pParams->x.lpm.vpn;
		 Service_data.DSE_forwarding_enable = pParams->x.lpm.DSE_forwarding_enable;
		 Service_data.DSE_forwarding_key_type = pParams->x.lpm.forwadingKeyType;
		 Service_data.L3_L4_fwd_enable = MEA_TRUE;
		 Service_data.L4port_mask_enable = MEA_TRUE;
		 bSetService = ADAP_TRUE;
    	 break;
     case E_UPDATE_VPN_PARAMETERS:
    	 Service_data.vpn = pParams->x.vpnParam.vpn;
    	 Service_data.DSE_forwarding_enable = pParams->x.vpnParam.DSE_forwarding_enable;
    	 Service_data.DSE_forwarding_key_type = pParams->x.vpnParam.forwadingKeyType;
    	 Service_data.L3_L4_fwd_enable = MEA_TRUE;

    	 if(pParams->x.vpnParam.forwadingKeyType == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN)
    	 {
    		 Service_data.L4port_mask_enable = MEA_TRUE;
    	 }
    	 else
    	 {
    		 Service_data.L4port_mask_enable = MEA_FALSE;
    	 }
		 bSetService = ADAP_TRUE;
    	 break;
     case E_UPDATE_OUTPUT_PARAMETERS:
    	 if (ehpAllocate == ADAP_TRUE )
    	 {
    		MEA_OS_free(Service_editing.ehp_info);
    		ehpAllocate = ADAP_FALSE;
			adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
			adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
			adap_memset(&Service_editing,0,sizeof(Service_editing));
    	 }
    	 Service_editing.ehp_info = (MEA_EHP_Info_dbt*)&Service_editing_info;
		for(ports=0;ports<pParams->x.editing.num_of_output_ports;ports++)
		{
			MEA_SET_OUTPORT(&Service_outPorts, pParams->x.editing.outports[ports]);
			MEA_SET_OUTPORT(&Service_editing_info.output_info, pParams->x.editing.outports[ports]);
			Service_editing.num_of_entries=1;

		}
		Service_editing_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		Service_editing_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Service_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		Service_editing_info.ehp_data.eth_info.val.all           = 0;

		Service_editing_info.ehp_data.eth_info.command = pParams->x.editing.outercommand;
		Service_editing_info.ehp_data.eth_info.val.all = ( ( (MEA_Uint32)pParams->x.editing.editOuterEthertype) << 16);
		Service_editing_info.ehp_data.eth_info.val.all |= ( (MEA_Uint32)pParams->x.editing.editOutervlanId);
		Service_editing_info.ehp_data.atm_info.double_tag_cmd    = pParams->x.editing.innercommand;
		Service_editing_info.ehp_data.atm_info.val.all = ( ( (MEA_Uint32)pParams->x.editing.editInnerEthertype) << 16);
		Service_editing_info.ehp_data.eth_info.val.all |= ( (MEA_Uint32)pParams->x.editing.editInnervlanId);
		bSetService = ADAP_TRUE;
    	 break;

     case E_UPDATE_FILTER_PARAMETERS:
    	 if(pParams->mode == E_GET_SERVICE_CONFIGURATION)
    	 {
			 adap_memcpy(&pParams->x.filterPararms.ACL_filter_info,&Service_data.ACL_filter_info,sizeof(MEA_ACL_prof_Key_mask_dbt));
    	 }
    	 else if(pParams->mode == E_CLEAR_SERVICE_CONFIGURATION)
    	 {
    		 adap_memset(&Service_data.ACL_filter_info,0,sizeof(MEA_ACL_prof_Key_mask_dbt));
			 Service_data.ACL_Prof = 0;
			 Service_data.ACL_Prof_valid = MEA_FALSE;
			 Service_data.filter_enable = MEA_FALSE;
			 bSetService = ADAP_TRUE;
    	 }
    	 else
    	 {
			 adap_memcpy(&Service_data.ACL_filter_info,&pParams->x.filterPararms.ACL_filter_info,sizeof(MEA_ACL_prof_Key_mask_dbt));
			 Service_data.ACL_Prof = pParams->x.filterPararms.ACL_Prof;
			 Service_data.ACL_Prof_valid = pParams->x.filterPararms.ACL_Prof_valid;
			 Service_data.filter_enable = pParams->x.filterPararms.filter_enable;
			 bSetService = ADAP_TRUE;
    	 }
    	 break;
     case E_UPDATE_VPLS_INSTANCE:
    	 Service_data.vpls_Ins_Id = pParams->x.vplsInst.vpls_Ins_Id;
    	 Service_data.en_vpls_Ins=pParams->x.vplsInst.en_vpls_Ins;
    	bSetService = ADAP_TRUE;
    	break;
     case E_UPDATE_POLICER:
  		if(MeaDrvGetPolicerMeterParams(pParams->x.policerData.MeterIdx, pParams->ifIndex, Service_data.pmId, &EnetPolilcingProfile, &TmIdToUpdate, &PmIdToUpdate,  &Service_policer)!= ENET_SUCCESS)
 		{
 			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateService ERROR: Unable to get PolicerMeterParams for service:%d\n",  pParams->Service);
 			return ENET_FAILURE;
 		}

        Service_data.policer_prof_id = EnetPolilcingProfile;
        Service_data.tmId = TmIdToUpdate;
		Service_data.pmId = PmIdToUpdate;
	Service_data.tmId_disable = MEA_FALSE;
    	bSetService = ADAP_TRUE;
     	break;
     case E_UPDATE_MTU_PROF:
        Service_data.mtu_Id = pParams->x.mtuFlowPolicerId;
        bSetService = ADAP_TRUE;
        break;
     case E_UPDATE_LIMITER:
        Service_data.limiter_enable = MEA_TRUE;
        Service_data.limiter_id = pParams->x.limiterId;
        break;
    case E_UPDATE_MCBCUC_FLOODING:
        switch (pParams->x.trfParam.type)
        {
        case eUnicast:
            Service_data.UC_NotAllowed = pParams->x.trfParam.notAllowed;
            break;
        case eBroadcast:
            Service_data.BC_NotAllowed = pParams->x.trfParam.notAllowed;
            break;
        case eMulticast:
            Service_data.MC_NotAllowed = pParams->x.trfParam.notAllowed;
        }
        bSetService = ADAP_TRUE;
        break;
     default:
    	 break;
     }

     if(bSetService == ADAP_TRUE)
     {
          Service_data.editId = 0; /*create new Id*/
          if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
        		  	  	  	  	  pParams->Service,
                                  &Service_data,
                                  &Service_outPorts,
                                  &Service_policer,
                                  &Service_editing) != MEA_OK)
          {
              ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," ENET_ADAPTOR_Set_Service failed for service:%d\n",pParams->Service);
              if (ehpAllocate == ADAP_TRUE )
              {
            	  MEA_OS_free(Service_editing.ehp_info);
              }
              return MEA_ERROR;
          }
     }
     if (ehpAllocate == ADAP_TRUE )
     {
    	 MEA_OS_free(Service_editing.ehp_info);
     }
     return MEA_OK;
}



MEA_Status MeaDrvCreatePointToPointServices (tServiceAdapDb *pServiceDb)
{
	/* local variable */

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_AcmMode_t                         acm_mode;
    ADAP_Uint32						      ports;
    tServiceDb							  *pServiceAlloc=NULL;

    /* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	adap_memset(&Service_editing,0,sizeof(Service_editing));
	/* Clear the service data attributes */
	adap_memset(&Service_data     , 0 , sizeof(Service_data));
	acm_mode = 0;
	ports=0;

	if(pServiceDb == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by pServiceDb == NULL\r\n");
		return MEA_ERROR;
	}

	/* Set the service key */
	Service_key.src_port         = pServiceDb->inPort;
	Service_key.pri              = 7;
	Service_key.net_tag          = pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Untagged?0:pServiceDb->outervlanId;
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;

	printf("Service_key.net_tag: 0x%08x\r\n",Service_key.net_tag);

	Service_key.L2_protocol_type = pServiceDb->L2_protocol_type; //MEA_PARSING_L2_KEY_Ctag_Ctag;
	Service_key.inner_netTag_from_valid          = MEA_TRUE;

	if( (pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Untagged)
			|| (pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Ctag)
			|| (pServiceDb->L2_protocol_type==MEA_PARSING_L2_KEY_Stag))
	{
		if(pServiceDb->ip_plus_vlan == ADAP_TRUE)
		{
			Service_key.inner_netTag_from_value          = pServiceDb->da_ip_addr;
			Service_key.inner_netTag_from_valid          = MEA_TRUE;
			Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_DL;
			printf("ip address:%x\r\n",Service_key.inner_netTag_from_value);
		}
		else
		{
			Service_key.inner_netTag_from_value          = 0xFFFFFFF;
			Service_key.inner_netTag_from_valid          = MEA_FALSE;
		}
	}
	else if(pServiceDb->L2_protocol_type== MEA_PARSING_L2_KEY_DEF_SID)
	{
		Service_key.inner_netTag_from_value          = 0xFFFFFFF;
		Service_key.net_tag          = 0x10000|pServiceDb->inPort;
		Service_data.vpls_Ins_Id = 0;
		Service_data.en_vpls_Ins=MEA_TRUE;
	}
	else
	{
		Service_key.inner_netTag_from_value          = pServiceDb->innervlanId;
		Service_key.inner_netTag_from_value         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	}





	if(pServiceDb->vxlan_enable == ADAP_TRUE)
	{
		Service_key.net_tag         = MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL | 0xFFF;
		Service_key.priType=MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP;
		Service_key.inner_netTag_from_valid = MEA_TRUE;
		Service_key.inner_netTag_from_value = pServiceDb->vxlan_vni;
		Service_key.pri              = 63;
		if(pServiceDb->L2_protocol_type == MEA_PARSING_L2_KEY_Untagged)
		{
			Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Untagged_protocol_vxlan;
		}
		else
		{
			Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_vxlan;
		}
	}

	Service_key.evc_enable 						= MEA_FALSE;
	//Service_data.SRV_mode						= 1;

	if(pServiceDb->ip_plus_vlan == ADAP_TRUE)
	{
		Service_data.FWD_DaMAC_valid = MEA_TRUE;
		Service_data.do_sa_search_only=MEA_TRUE;
		adap_mac_to_arr(&pServiceDb->editVxLan.destMac, Service_data.FWD_DaMAC_value.b);
	}

	/* Build the  service data attributes */
	Service_data.DSE_forwarding_enable   = MEA_FALSE;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;

	if(pServiceDb->forwadingKeyType != 0xFFFFFFFF)
	{
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = pServiceDb->forwadingKeyType;
		//Service_data.DSE_mask_field_type=DSE_mask_field_type;
		Service_data.L3_L4_fwd_enable = MEA_TRUE;
		Service_data.L4port_mask_enable	= MEA_TRUE;
		Service_data.vpn = 1; // temporary

		//Service_data.vpn = vpn;
	}

	/* Set the service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	//Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;

	if(Service_data.DSE_forwarding_enable == MEA_FALSE)
		Service_data.ADM_ENA            = MEA_TRUE;  // Alex allow to  back to same port
	else{
		if((Service_data.DSE_forwarding_enable == MEA_TRUE) && (Service_key.src_port == MEA_CPU_PORT)){
			Service_data.ADM_ENA            = MEA_TRUE;  //because of one port of 40G
		}

	}

	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_TRUE;  // Alex
	Service_data.COLOR              = 2;         //Alex
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	/* Clear the Service policing */
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer));
	/* Set the Service policing */


	Service_data.policer_prof_id = pServiceDb->policer_id;

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
										pServiceDb->policer_id,
										acm_mode,/* ACM_Mode */
										&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
		return MEA_ERROR;
	}

	Service_data.tmId            = pServiceDb->tmId;
	Service_data.pmId			= 0;


	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	Service_editing.num_of_entries = 1;
	Service_editing.ehp_info       = &(Service_editing_info);


	if(pServiceDb->editing == ADAP_EHP_APPEND_VXLAN_AND_EXTRACT_VLAN_TAG)
	{
		Service_editing.ehp_info[0].ehp_data.EditingType           = MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.valid = MEA_TRUE;
		adap_mac_to_arr(&pServiceDb->editVxLan.destMac, Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_Da.b);
		Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_dst_Ip = pServiceDb->editVxLan.destIp;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_vni = pServiceDb->editVxLan.tunnel_id;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.l4_dest_Port = pServiceDb->editVxLan.dest_port;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.l4_src_Port = pServiceDb->editVxLan.source_port;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.ttl = pServiceDb->editVxLan.ttl;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.sgw_ipId = pServiceDb->editVxLan.sourceIp;

	}
	else if(pServiceDb->editing == ADAP_EHP_APPEND_VXLAN)
	{
		Service_editing.ehp_info[0].ehp_data.EditingType           = MEA_EDITINGTYPE_VXLAN_header;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.valid = MEA_TRUE;
		adap_mac_to_arr(&pServiceDb->editVxLan.destMac, Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_Da.b);
		Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_dst_Ip = pServiceDb->editVxLan.destIp;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_vni = pServiceDb->editVxLan.tunnel_id;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.l4_dest_Port = pServiceDb->editVxLan.dest_port;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.l4_src_Port = pServiceDb->editVxLan.source_port;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.ttl = pServiceDb->editVxLan.ttl;
		Service_editing.ehp_info[0].ehp_data.VxLan_info.sgw_ipId = pServiceDb->editVxLan.sourceIp;
	}
	else
	{
		MeaDrvVlanSetEHPInfoData(&Service_editing.ehp_info[0],pServiceDb->editing,MEA_FALSE,pServiceDb->editOutervlanId,pServiceDb->editInnervlanId);
	}
	/* Build the upstream service outPorts */
	adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));

	for(ports=0;ports<pServiceDb->num_of_output_ports;ports++)
	{
		MEA_SET_OUTPORT(&Service_outPorts, pServiceDb->outports[ports]);
		MEA_SET_OUTPORT(&Service_editing.ehp_info->output_info, pServiceDb->outports[ports]);
	}
	pServiceDb->isAllreadyExist=ADAP_FALSE;

	pServiceAlloc = MeaAdapGetServiceIdByEnetPort(pServiceDb->L2_protocol_type,
													Service_key.net_tag,
													Service_key.inner_netTag_from_value,
													Service_key.sub_protocol_type,
													pServiceDb->inPort);

	if(pServiceAlloc != NULL)
	{
		pServiceDb->Service_id=pServiceAlloc->serviceId;
		pServiceDb->isAllreadyExist=ADAP_TRUE;
		pServiceAlloc->refCount++;
		return MEA_OK;
	}


	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}
	pServiceAlloc->refCount=1;

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	// support 1588
	Service_data.set_1588= MEA_FALSE;

	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
		// roll down
		MeaAdapFreeServiceId(pServiceAlloc->serviceId);
		return MEA_ERROR;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->inPort=pServiceDb->inPort;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	pServiceDb->pmId = Service_data.pmId;
	pServiceDb->Service_id=pServiceAlloc->serviceId;
	pServiceDb->tmId = Service_data.tmId;


	return MEA_OK;
}

ADAP_Uint32 MeaDrvDeleteVlanPriority(void)
{
	ADAP_Uint8			iss_port;
	MEA_Service_t	Service_id;
	tServiceDb *pServiceId=NULL;
	MEA_Bool		valid;

	for(iss_port=1;iss_port<MeaAdapGetNumOfPhyPorts();iss_port++)
	{
		pServiceId = MeaAdapGetServiceIdByVlanIfIndex(0, iss_port, MEA_PARSING_L2_KEY_Ctag);
		if(pServiceId != NULL)
		{
			Service_id = pServiceId->serviceId;
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) == MEA_OK)
			{
				if(valid == MEA_TRUE)
				{
					MeaDrvDeleteVlanBridging(pServiceId);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvDeleteVlanPriority delete service %d\n",Service_id);
				}
			}
		}
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 meaDrvCreateDefaultLagService(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 						  masterPort;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_Service_t                         Service_id;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    MEA_Uint16                            policer_id;
    MEA_AcmMode_t                         acm_mode;
    MEA_Bool							  exist=MEA_FALSE;
	MEA_Port_t 							  EnetPortId;
	MEA_LxCp_t 							  lxcpId;
    tServiceDb							  *pServiceAlloc=NULL;

    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
    adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
    adap_memset(&Service_key,0,sizeof(MEA_Service_Entry_Key_dbt));
    adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
    adap_memset(&Service_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));

	if(adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
		if( (adap_NumOfLagPortFromAggrId(u4IfIndex)) > 0)
		{
			masterPort = adap_GetLagMasterPort(u4IfIndex);
			if(masterPort == ADAP_END_OF_TABLE)
			{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"ifIndex:%d does not have master port\n",u4IfIndex);
					return ENET_SUCCESS;
			}
			Service_key.src_port         = adap_PortGetVirtualPort(masterPort);
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"ifIndex:%d is PortChannel with no member ports \n",u4IfIndex);
			return ENET_SUCCESS;
		}
	}
	else
	{
		masterPort = u4IfIndex;
		if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &EnetPortId) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",u4IfIndex, EnetPortId);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
			return ENET_FAILURE;
		}
		Service_key.src_port         = EnetPortId;

	}

	Service_key.net_tag          = 0x10000|adap_PortGetVirtualPort(masterPort);
	Service_key.virtual_port = MEA_TRUE;

	Service_key.L2_protocol_type =  MEA_PARSING_L2_KEY_DEF_SID;

	/* Made the service priority-unaware (default priority) */
	Service_key.evc_enable       = MEA_FALSE;
	Service_key.pri              = 0;

	/* Clear the upstream service data attributes */
	adap_memset(&Service_data  , 0 , sizeof(Service_data));

	/* Build the upstream service data attributes for the forwarder  */

	Service_data.DSE_forwarding_enable   = MEA_FALSE;
	Service_data.vpn = 0;
	Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
	Service_data.DSE_learning_actionId   = 0;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort	 = 0;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;


	/* Set the upstream service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the upstream service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the upstream service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_TRUE;
	Service_data.COLOR              = 2;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	/* As the Reverse actoin is used, most of the traffic cames through it and is policed by the Action_policer
	 * Service itself processes the not learned packets ONLY, so a little bandwidth allocated for this here
	 */
	/* Clear the Upstream Service policing */
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer));//
	/* Set the Upstream Service policing. T.B.D. Should be changed by QoS!!!*/
	Service_policer.CIR = 100000000;  //100M
	Service_policer.CBS = 64000;
	Service_policer.EIR = 0;
	Service_policer.EBS = 0;
	Service_policer.comp = 0;
	Service_policer.gn_type = 1;


	/* Build the Upstream Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	Service_editing.ehp_info       = &Service_editing_info;

	Service_id = MEA_PLAT_GENERATE_NEW_ID;

	/* Build the upstream service outPorts */
	adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));

	/* Attach the LxCp to the service */
	Service_data.LxCp_enable = 1;

	if(getLxcpId(masterPort,&lxcpId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
		return ENET_FAILURE;
	}
	Service_data.LxCp_Id = lxcpId;

	policer_id = MEA_PLAT_GENERATE_NEW_ID;

	if(MeaAdapGetDefaultPolicer(masterPort) == MEA_PLAT_GENERATE_NEW_ID)
	{
		Service_policer.CIR = 1000000000;
		Service_policer.CBS = 64000;
		Service_policer.EIR = 1000000000;
		Service_policer.EBS = 64000;
		Service_policer.comp = 0;
		Service_policer.gn_type= 1;
		Service_policer.color_aware=1;
		policer_id = MEA_PLAT_GENERATE_NEW_ID;
		acm_mode=0;

		if(MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
											  &policer_id,
											  acm_mode,
											  MEA_INGRESS_PORT_PROTO_TRANS,
											  &Service_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," MEA_API_Create_Policer_ACM_Profile FAIL\n");
			return ENET_FAILURE;
		}

		/* Add the default Policer ID to the DB */
		MeaAdapSetDefaultPolicer(policer_id,masterPort);
		Service_data.policer_prof_id = policer_id;
	}
	else
	{
		policer_id = MeaAdapGetDefaultPolicer(masterPort);
		Service_data.policer_prof_id = policer_id;

		if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
				policer_id,
											0,/* ACM_Mode */
											&Service_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
			return ENET_FAILURE;
		}
	}

//	if(MeaAdapGetDefaultTmId(masterPort) == MEA_PLAT_GENERATE_NEW_ID)
//	{
		Service_data.tmId            = 0;/* Force ENET to use a new tm bucket*/
//	}
//	else
//	{
//		Service_data.tmId = MeaAdapGetDefaultTmId(masterPort);
//	}

	if(ENET_ADAPTOR_IsExist_Service_ByKey(MEA_UNIT_0,&Service_key,&exist,&Service_id) == MEA_OK)
	{
		if(exist == MEA_TRUE)
		{
			MeaDrvHwSetDefaultService (masterPort,Service_id, MEA_DEF_SID_ACTION_DEF_SID);
			return ENET_SUCCESS;
		}
	}

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}
	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	/* Create new service for the given Vlan. */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwInit FAIL: Unable to create the new service %d\n",  pServiceAlloc->serviceId);
		MeaAdapFreeServiceId(pServiceAlloc->serviceId);
		return ENET_FAILURE;

	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);
	// save configuration
	pServiceAlloc->ifIndex = masterPort;
	pServiceAlloc->inPort=EnetPortId;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;

	if(MeaAdapGetPhyPortFromLogPort(masterPort, &EnetPortId) != ENET_SUCCESS)
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", masterPort);
		return ENET_FAILURE;
	}

	MeaDrvHwSetDefaultService (EnetPortId,pServiceAlloc->serviceId,MEA_DEF_SID_ACTION_DEF_SID);
	adap_SetDefaultSid(u4IfIndex,pServiceAlloc->serviceId);
	adap_SetDefaultSid(masterPort,pServiceAlloc->serviceId);

	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDeleteEnetServiceId(MEA_Service_t   Service_id)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Bool							  valid=MEA_FALSE;

	adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&Service_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);

	if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteEnetServiceId ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
		return ENET_FAILURE;
	}
	if(valid == MEA_TRUE)
	{
		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								  Service_id,
								  NULL,
								  &Service_data,
								  &Service_outPorts,
								  &Service_policer,
								  &Service_editing) == MEA_OK)
		{

			if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, Service_id) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteEnetServiceId ERROR: Unable to delete the service %d\n",  Service_id);
				return ENET_FAILURE;
			}
			MeaAdapFreeServiceId(Service_id);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);
			if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
			{
				if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0, Service_data.DSE_learning_actionId) == MEA_TRUE)
				{
					if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, Service_data.DSE_learning_actionId) != MEA_OK )
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteEnetServiceId ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
						return ENET_FAILURE;
					}
				}
			}
		}
	}
	return ENET_SUCCESS;
}

/**
 *
 *  Removing servicem which were copied from another service data.
 *  The difference is that we don't need to delete DSE_Learning_ActionId
 *
 */
ADAP_Uint32 MeaDeleteCopiedEnetServiceId(MEA_Service_t   Service_id)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Bool                              valid=MEA_FALSE;

    adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
    adap_memset(&Service_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
    adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
    adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);

    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteEnetServiceId ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
        return ENET_FAILURE;
    }
    if(valid == MEA_TRUE)
    {
        if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                  Service_id,
                                  NULL,
                                  &Service_data,
                                  &Service_outPorts,
                                  &Service_policer,
                                  &Service_editing) == MEA_OK)
        {

            if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, Service_id) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDeleteEnetServiceId ERROR: Unable to delete the service %d\n",  Service_id);
                return ENET_FAILURE;
            }
            MeaAdapFreeServiceId(Service_id);
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);
        }
    }
    return ENET_SUCCESS;
}


ADAP_Uint32 MeaDrvDelVlanEntryEnet(tEnetHal_VlanId VlanId)
{
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Int32 ret_check=ENET_SUCCESS;
	tServiceDb *pServiceInfo=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);
	if(pBridgeDomain != NULL)
	{
		ret_check = Adap_getFirstServiceFromLinkList(&pServiceInfo,pBridgeDomain);

		while( (ret_check == ENET_SUCCESS) && (pServiceInfo != NULL) )
		{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId to delete:%d\r\n",pServiceInfo->serviceId);
				if(pServiceInfo != NULL)
				{
					MeaDrvDeleteVlanBridging(pServiceInfo);
				}
			ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
		}
	}
	Adap_bridgeDomain_semUnlock();

	return ENET_SUCCESS;
}

ADAP_Int32 meaDrvDeleteDefaultService(ADAP_Uint32 ifIndex)
{
	MEA_Service_t                         Service_id;
    MEA_Bool							  valid=MEA_FALSE;
    MEA_Port_t							  enetPort;

	Service_id = adap_GetDefaultSid(ifIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Delete default service %d\n",Service_id);
	if(Service_id != ADAP_END_OF_TABLE)
	{
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"meaDrvDeleteDefaultService ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}
		if(valid == MEA_TRUE)
		{
			if(MeaDeleteDefaultServiceId(Service_id) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvDeleteVlanBridging ifIndex:%d\r\n",ifIndex);
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);
		}
	}

	if(adap_IsPortChannel(ifIndex) != ENET_SUCCESS)
	{
		if(MeaAdapGetPhyPortFromLogPort(ifIndex, &enetPort) == ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"logPort:%d, enetPortId:%d\n",ifIndex, enetPort);
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to convert u4IfIndex:%d to the ENET portId\n", ifIndex);
			return ENET_FAILURE;
		}

		MeaDrvHwSetDefaultService (enetPort, 0, MEA_DEF_SID_ACTION_DISCARD);
	}

	adap_SetDefaultSid(ifIndex,ADAP_END_OF_TABLE);
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvVlanHwLacpLxcpUpdate (MEA_Bool add,MEA_LxCp_t   LxCp_Id)
{
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

    /* Update the action for the specific protocol */
	key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_2;

    /* Update all ports' LxCp */
	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLacpLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

	switch (add)
	{
        case MEA_FALSE :
            /* Remove LxCp protocol by defining it as "Transparent" */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        break;
        case MEA_TRUE :
            /* Forward the LxCp protocol to CPU */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
        break;
        default:
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLacpLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
            return ENET_FAILURE;
	}

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLacpLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvPortMacSetFilterMask( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_VlanId vlanId, tEnetHal_VlanId editVlanId,tEnetHal_MacAddr *MacAddr,ADAP_Uint32 edit,MEA_Filter_t *pFilterId, ADAP_Uint32 L2_protocol_type)
{
	MEA_Filter_Key_dbt 						tFilterKey;
	MEA_Filter_Data_dbt      				tFilterData;
	MEA_Policer_Entry_dbt   				PolicerEntry;
	MEA_Uint16                  			policer_id;
	MEA_EgressHeaderProc_Array_Entry_dbt    filter_editing;
	ADAP_Uint32								PortIndex=0;
	ADAP_Uint32								num_of_ports=0;
	MEA_OutPorts_Entry_dbt					outPorts;
	MEA_EHP_Info_dbt						info;
	MEA_Service_Entry_Data_dbt 				Service_data;
	MEA_OutPorts_Entry_dbt 					Service_outPorts;
	MEA_Policer_Entry_dbt					Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt	Service_editing;
	MEA_Bool								valid;
	tEnetHal_VlanId                         localVlan;
	MEA_Service_Entry_Key_dbt           	Service_Key;
	tAclHierarchy 							*pHierarchy=NULL;
	tServiceDb							    *pServiceAlloc=NULL;
	MEA_Port_t 								enetPort;

	adap_memset(&tFilterKey,0,sizeof(MEA_Filter_Key_dbt));
	adap_memset(&tFilterData,0,sizeof(MEA_Filter_Data_dbt));
	adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
	adap_memset(&filter_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&info  , 0 , sizeof(MEA_EHP_Info_dbt ));

	// first get service Id
	adap_memset(&Service_data  , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	adap_memset(&Service_outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&Service_policer  , 0 , sizeof(MEA_Policer_Entry_dbt ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_Key  , 0 , sizeof(MEA_Service_Entry_Key_dbt ));

   if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
		return ENET_FAILURE;
	}
	if(valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ServiceId %d does not exists\n", Service_id);
		return ENET_FAILURE;
	}

	localVlan = vlanId;
	if(vlanId < 2)
	{
		localVlan=editVlanId;
	}

	for(PortIndex=1;PortIndex<MAX_NUM_OF_SUPPORTED_PORTS;PortIndex++)
	{
		if( (u4IfIndex == PortIndex) || (PortIndex < MeaAdapGetNumOfPhyPorts()) )
		{
			continue;
		}

		if(adap_VlanCheckUntaggedPort(localVlan,PortIndex) == ADAP_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"untagged iss port :%d\n", PortIndex);
			if(MeaAdapGetPhyPortFromLogPort(PortIndex, &enetPort) == ENET_SUCCESS)
			{
				printf("logPort:%d, enetPortId:%d\n",PortIndex, enetPort);
			}
			else
			{
				printf("Unable to convert u4IfIndex:%d to the ENET portId\n", PortIndex);
				return ENET_FAILURE;
		    }
			MEA_SET_OUTPORT(&outPorts, enetPort);
			MEA_SET_OUTPORT(&info.output_info,enetPort);
			num_of_ports++;
		}
		else if(adap_VlanCheckTaggedPort(localVlan,PortIndex) == ADAP_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"tagged iss port :%d\n", PortIndex);
			if(MeaAdapGetPhyPortFromLogPort(PortIndex, &enetPort) == ENET_SUCCESS)
			{
				printf("logPort:%d, enetPortId:%d\n",PortIndex, enetPort);
			}
			else
			{
				printf("Unable to convert u4IfIndex:%d to the ENET portId\n", PortIndex);
				return ENET_FAILURE;
		    }
			MEA_SET_OUTPORT(&outPorts, enetPort);
			MEA_SET_OUTPORT(&info.output_info,enetPort);
			num_of_ports++;
		}
	}
	if(num_of_ports == 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"no port found on vlan:%d\n", editVlanId);
		return ENET_FAILURE;
	}

	info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
	info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;


	info.ehp_data.eth_info.command = edit;

	info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	info.ehp_data.eth_info.val.vlan.vid = editVlanId;


	/* Add the  the ServiceEditingTable with the flooding ports lists per table entry */
	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							&Service_Key,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service failed for service:%d\n", Service_id);
		return ENET_FAILURE;
	}

	/* Allocate the memory for the EHP table. Reserve one more entry in advance in case we'll need to add an entry */
	Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * (Service_editing.num_of_entries+1));

	if (Service_editing.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							&Service_Key,
							&Service_data,
							&Service_outPorts, /* outPorts */
							&Service_policer, /* policer  */
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",Service_id);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	/* set filter parameters */
	tFilterKey.layer2.ethernet.SA_valid = MEA_TRUE;
	adap_mac_to_arr(MacAddr, tFilterKey.layer2.ethernet.SA.b);

	pHierarchy = AdapGetAclHierarchyByType(u4IfIndex, L2_protocol_type, vlanId);
	if(pHierarchy != NULL)
	{
		 Service_data.ACL_Prof = pHierarchy->acl_profile_Hier.acl_profile;
		 Service_data.ACL_Prof_valid = pHierarchy->acl_profile_Hier.valid;
	}

	tFilterKey.interface_key.src_port_valid = MEA_FALSE;
	tFilterKey.interface_key.src_port = 0;
	//tFilterKey.interface_key.sid_valid = pHierarchy->acl_profile_Hier.valid;
	//tFilterKey.interface_key.acl_prof  =  pHierarchy->acl_profile_Hier.acl_profile;
	tFilterKey.interface_key.acl_prof = Service_data.ACL_Prof;
	tFilterKey.interface_key.sid_valid = Service_data.ACL_Prof_valid;


	tFilterData.action_type = MEA_FILTER_ACTION_TO_ACTION;
	tFilterData.action_params.proto_llc_valid = MEA_TRUE;
	tFilterData.action_params.protocol_llc_force = MEA_TRUE;
	tFilterData.action_params.Protocol = 0x1;
	tFilterData.action_params.Llc = 0;
	tFilterData.action_params.ed_id_valid=MEA_TRUE;
	tFilterData.action_params.ed_id=0;

	tFilterData.action_params.pm_id_valid = MEA_TRUE;
	tFilterData.action_params.pm_id = 0;

	tFilterData.action_params.tm_id_valid = MEA_TRUE;
	tFilterData.action_params.tm_id  = 0;

	policer_id = MeaAdapGetDefaultPolicer(u4IfIndex);
	if(policer_id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get policer from IfIndex:%d\n",u4IfIndex);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,policer_id, 0, &PolicerEntry )!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," MEA_API_Get_Policer_ACM_Profile FAIL\n");
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}


	filter_editing.ehp_info = &info;
	filter_editing.num_of_entries=1;
	tFilterData.action_params.output_ports_valid = MEA_TRUE;


	(*pFilterId)=MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
							   &tFilterKey,
							   NULL,
							   &tFilterData,
							   &outPorts,
							   &PolicerEntry,
							   &filter_editing,
							   pFilterId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: ENET_ADAPTOR_Create_Filter FAIL for FilterId %d failed\n",*pFilterId);
		return ENET_FAILURE;
	}

	Service_data.filter_enable = MEA_TRUE;
	Service_data.ACL_filter_info.filter_Whitelist_enable = MEA_TRUE;
	Service_data.ACL_filter_info.data_info[0].valid = MEA_TRUE;
	Service_data.ACL_filter_info.data_info[0].filter_key_type = MEA_FILTER_KEY_TYPE_SA_MAC;

	MeaAdapFreeServiceId(Service_id);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);

	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

	if(ENET_ADAPTOR_Create_Service (MEA_UNIT_0,
		                           &Service_Key,
		                           &Service_data,
		                           &Service_outPorts,
		                           &Service_policer,
		                           &Service_editing,
		                           &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Create_Service failed for service:%d\n", pServiceAlloc->serviceId);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	// save configuration
	pServiceAlloc->ifIndex = u4IfIndex;
	pServiceAlloc->inPort=enetPort;
	pServiceAlloc->outerVlan=Service_Key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_Key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_Key.L2_protocol_type;
	pServiceAlloc->pmId = Service_data.pmId;
	pServiceAlloc->vpnId = Service_data.vpn;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create/set the new service %d\n",pServiceAlloc->serviceId);

	MEA_OS_free(Service_editing.ehp_info);

	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvPortMacDeleteFilterMask(MEA_Service_t Service_id,MEA_Filter_t filterId)
{
	MEA_Service_Entry_Data_dbt 				Service_data;
	MEA_OutPorts_Entry_dbt 					Service_outPorts;
	MEA_Policer_Entry_dbt					Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt	Service_editing;
	MEA_Bool								valid;
	ADAP_Uint32								i=0;

	// first get service Id
	adap_memset(&Service_data  , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	adap_memset(&Service_outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&Service_policer  , 0 , sizeof(MEA_Policer_Entry_dbt ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

	if(filterId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if(ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,filterId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Delete_Filter for filter Id %d failed\n", filterId);
			return ENET_FAILURE;
		}

		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}
		if(valid == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ServiceId %d does not exists\n", Service_id);
			return ENET_FAILURE;
		}
	}

	if(Service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								Service_id,
								NULL,
								&Service_data,
								&Service_outPorts,
								&Service_policer,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service failed for service:%d\n", Service_id);
			return ENET_FAILURE;
		}

		/* Allocate the memory for the EHP table. Reserve one more entry in advance in case we'll need to add an entry */
		Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * (Service_editing.num_of_entries+1));

		if (Service_editing.ehp_info == NULL )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
			return ENET_FAILURE;
		}

		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								Service_id,
								NULL,
								&Service_data,
								&Service_outPorts, /* outPorts */
								&Service_policer, /* policer  */
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",Service_id);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}

		/* set filter parameters */
		for(i=0;i<Service_editing.num_of_entries;i++)
		{
			if(Service_editing.ehp_info[i].ehp_data.LmCounterId_info.valid == MEA_TRUE)
			{
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.valid = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_enable = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_value = 0;
			}
		}

		Service_data.ACL_filter_info.filter_Whitelist_enable = MEA_FALSE;
		Service_data.ACL_filter_info.data_info[0].valid = MEA_FALSE;
		Service_data.filter_enable = MEA_FALSE;
        Service_data.editId = 0; /*create new Id*/

		if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
								Service_id,
								&Service_data,
								&Service_outPorts,
								&Service_policer,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);
		MEA_OS_free(Service_editing.ehp_info);
	}
	return ENET_SUCCESS;
}

ADAP_Int32	MeaDrvVlanHwLxcpUpdate (eEnetHal_VlanHwTunnelFilters LxcpServiceProtocol, MEA_Bool add)
{
    MEA_LxCp_t                    LxCp_Id;
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;
    ADAP_Uint32                   Port;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

    /* Update the action for the specific protocol */
    switch (LxcpServiceProtocol)
    {
    case    ENETHAL_VLAN_NP_GVRP_PROTO_ID:
    	key_pi.protocol = MEA_LXCP_PROTOCOL_GVRP;
     break;
    case   ENETHAL_VLAN_NP_GMRP_PROTO_ID:
    	key_pi.protocol = MEA_LXCP_PROTOCOL_GMRP;
    break;
    case ENETHAL_VLAN_NP_STP_PROTO_ID:
    	key_pi.protocol = MEA_LXCP_PROTOCOL_STP;
    break;
	case ENETHAL_VLAN_NP_LACP_PROTO_ID:
		key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_2;
	break;
	case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
		key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_13;
		break;
    default:
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate:  Invalid LxCp Protocol:%d \n", LxcpServiceProtocol);
		return ENET_FAILURE;
    }

    if(LxcpServiceProtocol == ENETHAL_VLAN_NP_STP_PROTO_ID)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"bridge mode:%d\n", AdapGetBridgeMode());
    	key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_8;
		for(Port=1; Port<MeaAdapGetNumOfPhyPorts(); Port++)
		{
			if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) )
			{
				/* Get the LxCp entry */
				if(getLxcpId(Port,&LxCp_Id) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
					return MEA_ERROR;
				}

				if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK){
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for ISS port %d LxCp Id:%d\n",
							Port, LxCp_Id);
					return ENET_FAILURE;
				}

				switch (add)
				{
				case MEA_FALSE :
					/* Remove LxCp protocol by defining it as "Transparent" */
					data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
				break;
				case MEA_TRUE :
					/* Forward the LxCp protocol to CPU */
					data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
				break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
					return ENET_FAILURE;
				}

				/* Update the ENET */
				if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! ISS port: %d LxCp Id:%d\n",
							Port, LxCp_Id);
					return ENET_FAILURE;
				}
			}
    	}
		key_pi.protocol = MEA_LXCP_PROTOCOL_STP;
    }

    /* Update all ports' LxCp */
    for(Port=1; Port<MeaAdapGetNumOfPhyPorts(); Port++)
    {
        /* Get the LxCp entry */
		if(getLxcpId(Port,&LxCp_Id) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
			return MEA_ERROR;
		}

        if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK){
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for ISS port %d LxCp Id:%d\n",
                    Port, LxCp_Id);
            return ENET_FAILURE;
        }

        switch (add) {
        case MEA_FALSE :
            /* Remove LxCp protocol by defining it as "Transparent" */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        break;
        case MEA_TRUE :
            /* Forward the LxCp protocol to CPU */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
        break;
        default:
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
            return ENET_FAILURE;
        }

        data_pi.ActionId_valid = MEA_FALSE;
        data_pi.OutPorts_valid = MEA_FALSE;

        /* Update the ENET */
        if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK){
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! ISS port: %d LxCp Id:%d\n",
                    Port, LxCp_Id);
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}

MEA_Status MeaDrvUpdateUserPriority (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Int32 i4DefPriority)
{
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
    MEA_Service_Entry_Key_dbt           	Service_Key;
	MEA_Bool							    valid=MEA_FALSE;
	ADAP_Uint16								index=0;
	ADAP_Uint16                             EnetPriQueueIdx = ADAP_END_OF_TABLE;

	adap_memset(&Service_data  , 0 , sizeof(Service_data ));
	adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_Key  , 0 , sizeof(MEA_Service_Entry_Key_dbt ));

	if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: ENET_ADAPTOR_IsExist_Service_ById for service Id %d failed\n", Service_id);
		return  ENET_FAILURE;
	}

	if(valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: ServiceId %d does not exists\n", Service_id);
		return ENET_FAILURE;
	}
	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							&Service_Key,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
		return ENET_FAILURE;
	}

	if(Service_Key.L2_protocol_type == MEA_PARSING_L2_KEY_Untagged)
	{
		Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

		if (Service_editing.ehp_info == NULL )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: FAIL to malloc ehp_info\n");
			return ENET_FAILURE;
		}

		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								Service_id,
								&Service_Key,
								&Service_data,
								&Service_outPorts, /* outPorts */
								&Service_policer, /* policer  */
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service_id);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}

		for(index=0;index<Service_editing.num_of_entries;index++)
		{
			Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.pri = i4DefPriority;
			Service_editing.ehp_info[index].ehp_data.atm_info.val.double_vlan_tag.pri = i4DefPriority;

			Service_editing.ehp_info[index].ehp_data.eth_info.stamp_color       = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_info.stamp_priority    = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_valid = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_loc   = 0;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_FALSE;
			Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;
		}
		Service_data.editId=0;

		//TODO - get priQueue index EnetPriQueueIdx = QoSGetPriQueueIdx (u4IfIndex);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateUserPriority EnetPriQueueIdx:%d\n", EnetPriQueueIdx);
		//if(EnetPriQueueIdx != MEA_PLAT_GENERATE_NEW_ID)
		//{
		//	Service_data.COS_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
		//	Service_data.COS       = EnetPriQueueIdx;
		//}
		//else
	//	{
			Service_data.flowCoSMappingProfile_force = MEA_FALSE;
			Service_data.COS_FORCE = MEA_ACTION_FORCE_COMMAND_DISABLE;
			Service_data.COS       = 0;
	//	}

		if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
								Service_id,
								&Service_data,
								&Service_outPorts,
								&Service_policer,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateUserPriority ERROR: ENET_ADAPTOR_Set_Service failed for service:%d, \n", Service_id);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);
		MEA_OS_free(Service_editing.ehp_info);
	}
    return MEA_OK;
}

MEA_Status MeaDrvSetFlowMarkingProfile (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Uint16 flowMarkProfile)
{
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
    MEA_Service_Entry_Key_dbt           	Service_Key;
	MEA_Bool							    valid=MEA_FALSE;

	adap_memset(&Service_data  , 0 , sizeof(Service_data ));
	adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_Key  , 0 , sizeof(MEA_Service_Entry_Key_dbt ));

    /* check if the service exist */
    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: ENET_ADAPTOR_IsExist_Service_ById for service Id %d failed\n", Service_id);
        return ENET_FAILURE;
    }
    if(valid == MEA_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: ServiceId %d does not exists\n", Service_id);
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service_id,
                            NULL,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
        return ENET_FAILURE;
    }
    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

    if (Service_editing.ehp_info == NULL )
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: FAIL to malloc ehp_info\n");
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service_id,
                            NULL,
                            &Service_data,
                            &Service_outPorts, /* outPorts */
                            &Service_policer, /* policer  */
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service_id);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }

    if(flowMarkProfile != MEA_PLAT_GENERATE_NEW_ID)
    {
        Service_data.flowMarkingMappingProfile_force = 1;
        Service_data.flowMarkingMappingProfile_id = flowMarkProfile;
    }
    else
    {
		Service_data.flowMarkingMappingProfile_force = 0;
		Service_data.flowMarkingMappingProfile_id = 0;
		Service_data.flowCoSMappingProfile_force = 0;
    }
    Service_data.editId = 0; /*create new Id*/
    if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
                            Service_id,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowMarkingProfile ERROR: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);

    return MEA_OK;
}

MEA_Status MeaDrvSetFlowPolicerProfile (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Uint16 flowPolicerProfile)
{
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
    MEA_Service_Entry_Key_dbt           	Service_Key;
	MEA_Bool							    valid=MEA_FALSE;

	adap_memset(&Service_data  , 0 , sizeof(Service_data ));
	adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
	adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_Key  , 0 , sizeof(MEA_Service_Entry_Key_dbt ));

    /* check if the service exist */
    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: ENET_ADAPTOR_IsExist_Service_ById for service Id %d failed\n", Service_id);
        return ENET_FAILURE;
    }
    if(valid == MEA_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: ServiceId %d does not exists\n", Service_id);
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service_id,
                            NULL,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
        return ENET_FAILURE;
    }
    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

    if (Service_editing.ehp_info == NULL )
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: FAIL to malloc ehp_info\n");
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service_id,
                            NULL,
                            &Service_data,
                            &Service_outPorts, /* outPorts */
                            &Service_policer, /* policer  */
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service_id);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }

    if(flowPolicerProfile != MEA_PLAT_GENERATE_NEW_ID)
    {
        Service_data.Ingress_flow_policer_ProfileId = flowPolicerProfile;
    }
    else
    {
    	Service_data.Ingress_flow_policer_ProfileId = 0;
    }
    Service_data.editId = 0; /*create new Id*/
    if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
                            Service_id,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetFlowPolicerProfile ERROR: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);

    return MEA_OK;
}

ADAP_Int32
MeaDrvAddTunnelInTranslation (void)
{
	ADAP_Uint32							   PortIndex;
	ADAP_Uint32							   PortType;
    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
	MEA_LxCp_t                             LxCp_Id;
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;
    ADAP_Uint16                            ServiceVlan;
    MEA_EHP_Info_dbt                       Action_editing_info;
    MEA_Port_t               			   EnetPortId;

    /* Add the Customer->Trunk port translation (Tunnel In) */

    adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
    adap_memset(&LxCp_data, 0, sizeof(LxCp_data));
    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));

    /* Build the target ports' list */
    adap_memset(&Action_outPorts, 0, sizeof(Action_outPorts));
    for(PortIndex = 1; PortIndex < MeaAdapGetNumOfPhyPorts(); PortIndex ++)
    {
        PortType = adap_GetVlanPortTunnelType(PortIndex);
        if(PortType == ADAP_VLAN_TUNNEL_EXTERNAL)
        {
            /* Add the port to the flooding domain lists */
        	if(MeaAdapGetPhyPortFromLogPort(PortIndex,&EnetPortId) != ENET_SUCCESS)
        	{
        		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",PortIndex);
        		return ENET_FAILURE;
        	}
            MEA_SET_OUTPORT(&Action_outPorts, EnetPortId);
        }
    }
    if(MEA_IS_CLEAR_ALL_OUTPORT(&Action_outPorts))
    {
        /* No target ports - nothing to do! */
        return ENET_SUCCESS;
    }

    /* For all ports in VLAN: */
    for(PortIndex = 1; PortIndex < MeaAdapGetNumOfPhyPorts(); PortIndex ++)
    {
        /* If the source port is of the customer type */
        PortType = adap_GetVlanPortTunnelType(PortIndex);
        if(PortType == ADAP_VLAN_TUNNEL_INTERNAL)
            continue;

    	if(getLxcpId(PortIndex,&LxCp_Id) != ENET_SUCCESS)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
    		return MEA_ERROR;
    	}

        ServiceVlan = adap_GetPBPortServVlanCustomerVlan(PortIndex);
        if(ServiceVlan == ADAP_END_OF_TABLE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: Customer \n");
            return ENET_FAILURE;
        }

        /* Create the action which translates: */
        // STP D-MAC -> gVlanProviderStpAddr
        // GMRP D-MAC -> gVlanProviderGmrpAddr
        // GVRP D-MAC -> gVlanProviderGvrpAddr
        /* Commit the action */
        /* Commit the LxCp */
        /* Attach the LxCp to all the ports in the source ports list */

        Action_data.output_ports_valid  = MEA_TRUE;
        Action_data.force_color_valid = MEA_FALSE;
        Action_data.COLOR             = 0;
        Action_data.force_cos_valid   = MEA_FALSE;
        Action_data.COS               = 0;
        Action_data.force_l2_pri_valid = MEA_FALSE;
        Action_data.L2_PRI             = 0;

        Action_data.pm_id_valid        = MEA_TRUE;
        Action_data.pm_id              = 0; /* Request new id */
        Action_data.tm_id_valid        = MEA_TRUE;
        Action_data.tm_id              = 0; /* Request new id */
        Action_data.ed_id_valid        = MEA_TRUE;
        Action_data.ed_id              = 0; /* Request new id */
        Action_data.protocol_llc_force = MEA_TRUE;
        Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
        Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
        Action_data.Llc                = MEA_FALSE;

        /* Build the action policer */
        adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
        Action_data.tm_id              = 0;
        Action_data.policer_prof_id    = MeaAdapGetDefaultPolicer(PortIndex);
        if(Action_data.policer_prof_id == MEA_PLAT_GENERATE_NEW_ID)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: Default policer profile NOT FOUND\n");
            return ENET_FAILURE;
        }
        if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                            Action_data.policer_prof_id,
                                            0,/* ACM_Mode */
                                            &Action_policer)
            != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: MEA_API_Get_Policer_ACM_Profile for PolilcingProfile %d failed\n",
                    Action_data.policer_prof_id);
            return ENET_FAILURE;
        }

        /* Build the action editing */
        adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
        adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
        Action_editing.num_of_entries = 1;
        Action_editing.ehp_info = &Action_editing_info;
        adap_memcpy(&Action_editing_info.output_info,
                      &Action_outPorts,
                      sizeof(Action_editing_info.output_info));

        Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
        Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

        Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
        Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = ServiceVlan;

        Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
        Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

        adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);

        /* Create Action and set LxCp for GMRP */
        adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetProvGmrpMacAddress(), ETH_ALEN);

        /* Create the action */
        Action_id = MEA_PLAT_GENERATE_NEW_ID;

        if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                   &Action_data,
                                   &Action_outPorts,
                                   &Action_policer,
                                   &Action_editing,
                                   &Action_id) != MEA_OK)
        {
           ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Create_Action failed for action:%d(GMRP)\n",
                  Action_id);
           return ENET_FAILURE;
        }

        /* Get the LxCp entry */
        if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: MEA_API_Get_LxCP_Protocol(GMRP) FAIL! LxCp ID:%d\n",
                   LxCp_Id);
            return ENET_FAILURE;
        }

        LxCp_key.protocol = MEA_LXCP_PROTOCOL_GMRP;
        LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
        LxCp_data.ActionId_valid = MEA_TRUE;
        LxCp_data.ActionId = Action_id;
        LxCp_data.OutPorts_valid = MEA_TRUE;
        adap_memcpy(&LxCp_data.OutPorts,
            &Action_outPorts,
            sizeof(LxCp_data.OutPorts));

        /* Update the ENET */
        if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Set_LxCP_Protocol(GMRP) FAIL!\n");
            return ENET_FAILURE;
        }

        /* Create Action and set LxCp for GVRP */
        adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetGvrpMacAddress(), ETH_ALEN);

        /* Create the action */
        Action_data.ed_id_valid = MEA_TRUE;
        Action_data.ed_id              = 0; /* Request new id */
        Action_id = MEA_PLAT_GENERATE_NEW_ID;

        if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                   &Action_data,
                                   &Action_outPorts,
                                   &Action_policer,
                                   &Action_editing,
                                   &Action_id) != MEA_OK)
        {
           ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Create_Action failed for action:%d(GVRP)\n", Action_id);
           return ENET_FAILURE;
        }

        /* Get the LxCp entry */
        if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: MEA_API_Get_LxCP_Protocol(GVRP) FAIL!\n");
            return ENET_FAILURE;
        }

        LxCp_key.protocol = MEA_LXCP_PROTOCOL_GVRP;
        LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
        LxCp_data.ActionId_valid = MEA_TRUE;
        LxCp_data.ActionId = Action_id;
        LxCp_data.OutPorts_valid = MEA_TRUE;
        adap_memcpy(&LxCp_data.OutPorts,
                      &Action_outPorts,
                      sizeof(LxCp_data.OutPorts));

        /* Update the ENET */
        if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Set_LxCP_Protocol(GVRP) FAIL!\n");
            return ENET_FAILURE;
        }

        /* Create Action and set LxCp for STP */
        adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetStpMacAddress(), ETH_ALEN);

        /* Create the action */
        Action_data.ed_id_valid = MEA_TRUE;
        Action_data.ed_id              = 0; /* Request new id */
        Action_id = MEA_PLAT_GENERATE_NEW_ID;

        if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                   &Action_data,
                                   &Action_outPorts,
                                   &Action_policer,
                                   &Action_editing,
                                   &Action_id) != MEA_OK)
        {
           ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Create_Action failed for action:%d(xSTP)\n", Action_id);
           return ENET_FAILURE;
        }

        /* Get the LxCp entry */
        if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK){
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: MEA_API_Get_LxCP_Protocol(xSTP) FAIL!\n");
            return ENET_FAILURE;
        }

        LxCp_key.protocol = MEA_LXCP_PROTOCOL_STP;
        LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
        LxCp_data.ActionId_valid = MEA_TRUE;
        LxCp_data.ActionId = Action_id;
        LxCp_data.OutPorts_valid = MEA_TRUE;
        adap_memcpy(&LxCp_data.OutPorts,
            &Action_outPorts,
            sizeof(LxCp_data.OutPorts));

        /* Update the ENET */
        if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK){
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelInTranslation FAIL: ENET_ADAPTOR_Set_LxCP_Protocol(xSTP) FAIL!\n");
            return ENET_FAILURE;
        }
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"exit\n");

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvAddTunnelOutTranslation (void)
{
	ADAP_Uint32                           VlanId, PortIndex;
	ADAP_Uint32                           PortType;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          Action_id;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_SE_Entry_dbt                      entry;
    ADAP_Uint16                           VpnIndex;
	tBridgeDomain 						  *pBridgeDomain=NULL;
	MEA_Port_t               			   EnetPortId;

    adap_memset(&Action_data, 0, sizeof(Action_data));
    /* Add the Trunk port->Customer translation (Tunnel Out) */
    /* For all VLANS: */
    for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
    {
    	Adap_bridgeDomain_semLock();
    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

    	if(pBridgeDomain != NULL)
        {
            /* Clear the SE entry struct */
            adap_memset(&entry, 0, sizeof(entry));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_memcpy(entry.key.mac_plus_vpn.MAC.b, AdapGetMmrpMacAddress(), ETH_ALEN);

            VpnIndex = pBridgeDomain->vpn;
            entry.key.mac_plus_vpn.VPN = VpnIndex;

            /* For all ports in VLAN: */
            adap_memset(&Action_outPorts, 0, sizeof(Action_outPorts));

            for(PortIndex = 1; PortIndex < MeaAdapGetNumOfPhyPorts(); PortIndex ++)
            {
    			if( adap_VlanCheckPortsInDomain(pBridgeDomain, PortIndex) == ADAP_TRUE)
                {
                    PortType = adap_GetVlanPortTunnelType(PortIndex);
                    if(PortType == ADAP_VLAN_TUNNEL_EXTERNAL)
                    {
                        /*Add the port to the flooding domain lists */
                    	if(MeaAdapGetPhyPortFromLogPort(PortIndex,&EnetPortId) != ENET_SUCCESS)
                    	{
                    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",PortIndex);
                    		Adap_bridgeDomain_semUnlock();
                    		return ENET_FAILURE;
                    	}
                        MEA_SET_OUTPORT(&entry.data.OutPorts, EnetPortId);
                    }
                }
            }
            /* Fill in the SE data parameters */
            entry.data.aging = 3; //Default
            entry.data.static_flag  = MEA_TRUE;
            entry.data.actionId_valid  = MEA_TRUE;

            Action_data.output_ports_valid  = MEA_FALSE;
            Action_data.force_color_valid = MEA_FALSE;
            Action_data.COLOR             = 0;
            Action_data.force_cos_valid   = MEA_FALSE;
            Action_data.COS               = 0;
            Action_data.force_l2_pri_valid = MEA_FALSE;
            Action_data.L2_PRI             = 0;

            Action_data.pm_id_valid        = MEA_FALSE;
            Action_data.pm_id              = 0; /* Request new id */
            Action_data.tm_id_valid        = MEA_FALSE;
            Action_data.tm_id              = 0; /* Request new id */
            Action_data.ed_id_valid        = MEA_TRUE;
            Action_data.ed_id              = 0; /* Request new id */
            Action_data.protocol_llc_force = MEA_TRUE;
            Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
            Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
            Action_data.Llc                = MEA_FALSE;

            /* Build the action editing */
            adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
            adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
            Action_editing.num_of_entries = 1;
            Action_editing.ehp_info = &Action_editing_info;
            Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
            Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

            Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
            Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
            Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = VlanId;

            Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
            Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

            adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);

            /* Create Action and set LxCp for GMRP */
            adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetProvGmrpMacAddress(), ETH_ALEN);

            /* Create the action */
            Action_id = MEA_PLAT_GENERATE_NEW_ID;

            if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                       &Action_data,
                                       &Action_outPorts,
                                       NULL,
                                       &Action_editing,
                                       &Action_id) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            entry.data.actionId = Action_id;
            /* Commit the new entry to the forwarder */
            if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelOutTranslation FAIL: ENET_ADAPTOR_Create_SE_Entry for GMRP FAIL!!!\n");
                Adap_bridgeDomain_semUnlock();
                return ENET_FAILURE;
            }

            /* Create Action and SE entry for GVRP */
            adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetProvGvrpMacAddress(), ETH_ALEN);

            /* Create the action */
            Action_data.ed_id_valid = MEA_TRUE;
            Action_data.ed_id              = 0; /* Request new id */
            Action_id = MEA_PLAT_GENERATE_NEW_ID;

            if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                       &Action_data,
                                       &Action_outPorts,
                                       NULL,
                                       &Action_editing,
                                       &Action_id) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelOutTranslation FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            adap_memcpy(entry.key.mac_plus_vpn.MAC.b, AdapGetGvrpMacAddress(), ETH_ALEN);
            entry.data.actionId = Action_id;
            /* Commit the new entry to the forwarder */
            if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelOutTranslation FAIL: ENET_ADAPTOR_Create_SE_Entry for GVRP FAIL!!!\n");
                Adap_bridgeDomain_semUnlock();
                return ENET_FAILURE;
            }

            /* Create Action and SE entry for STP */
            adap_memcpy(Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetStpMacAddress(), ETH_ALEN);

            /* Create the action */
            Action_data.ed_id_valid = MEA_TRUE;
            Action_data.ed_id              = 0; /* Request new id */
            Action_id = MEA_PLAT_GENERATE_NEW_ID;

            if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                       &Action_data,
                                       &Action_outPorts,
                                       NULL,
                                       &Action_editing,
                                       &Action_id) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelOutTranslation FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            adap_memcpy(entry.key.mac_plus_vpn.MAC.b, AdapGetStpMacAddress(), ETH_ALEN);
            entry.data.actionId = Action_id;
            /* Commit the new entry to the forwarder */
            if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddTunnelOutTranslation FAIL: ENET_ADAPTOR_Create_SE_Entry for STP FAIL!!!\n");
                Adap_bridgeDomain_semUnlock();
                return ENET_FAILURE;
            }
    	}
        Adap_bridgeDomain_semUnlock();
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"exit\n");

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvRemoveTunnelOutTranslation (void)
{
	ADAP_Uint32        VlanId;
    MEA_Action_t       Action_id;
    MEA_SE_Entry_dbt   entryGmrp, entryGvrp, entryStp;
    ADAP_Uint16        VpnIndex;
    MEA_Bool           exist;
	tBridgeDomain 	   *pBridgeDomain=NULL;

    /* Remove the Trunk port->Customer translation (Tunnel Out) */
    /* For all VLANS: */
    for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
    {
    	Adap_bridgeDomain_semLock();
    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

    	if(pBridgeDomain != NULL)
        {
            /* Clear the SE entry struct for GMRP*/
            adap_memset(&entryGmrp, 0, sizeof(entryGmrp));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entryGmrp.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_memcpy(entryGmrp.key.mac_plus_vpn.MAC.b, AdapGetGmrpMacAddress(), ETH_ALEN);

            VpnIndex = pBridgeDomain->vpn;
            entryGmrp.key.mac_plus_vpn.VPN = VpnIndex;

            if ( ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,
                                      &entryGmrp) != MEA_OK)
            {
                 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: Error while getting SE entry\n");
                 Adap_bridgeDomain_semUnlock();
                 return ENET_FAILURE;
            }

            Action_id = entryGmrp.data.actionId;

            if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,
                                        &(entryGmrp.key)) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: FsMiVlanHwRemoveTunnelOutTranslation: ENET_ADAPTOR_Delete_SE_Entry for GMRP FAIL!!!\n");
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            /* Delete Action for GMRP */
            if (MEA_API_IsExist_Action  (MEA_UNIT_0, Action_id , &exist)!=MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: MEA_API_IsExist_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }
            if(exist == MEA_TRUE)
            {
                if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Action_id)!=MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n", Action_id);
                    Adap_bridgeDomain_semUnlock();
                    return ENET_FAILURE;
                }
            }

            /* Clear the SE entry struct for GVRP*/
            adap_memset(&entryGvrp, 0, sizeof(entryGvrp));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entryGvrp.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_memcpy(entryGvrp.key.mac_plus_vpn.MAC.b, AdapGetGvrpMacAddress(), ETH_ALEN);

            VpnIndex = pBridgeDomain->vpn;

            entryGvrp.key.mac_plus_vpn.VPN = VpnIndex;
            if ( ENET_ADAPTOR_Get_SE_Entry (MEA_UNIT_0,
                                      &entryGvrp) != MEA_OK)
            {
                 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: Error while getting SE entry\n");
                 Adap_bridgeDomain_semUnlock();
                 return ENET_FAILURE;
            }

            Action_id = entryGvrp.data.actionId;

            if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,
                                        &(entryGvrp.key)) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: FsMiVlanHwRemoveTunnelOutTranslation: ENET_ADAPTOR_Delete_SE_Entry for GVRP FAIL!!!\n");
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            /* Delete Action for GVRP */
            if (MEA_API_IsExist_Action  (MEA_UNIT_0, Action_id , &exist)!=MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: MEA_API_IsExist_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }
            if(exist == MEA_TRUE)
            {
                if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Action_id)!=MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n", Action_id);
                    Adap_bridgeDomain_semUnlock();
                    return ENET_FAILURE;
                }
            }

            /* Clear the SE entry struct for STP*/
            adap_memset(&entryStp, 0, sizeof(entryStp));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entryStp.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_memcpy(entryStp.key.mac_plus_vpn.MAC.b, AdapGetStpMacAddress(), ETH_ALEN);

            VpnIndex = pBridgeDomain->vpn;

            entryStp.key.mac_plus_vpn.VPN = VpnIndex;

            if ( ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,
                                      &entryStp) != MEA_OK)  {
                 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: Error while getting SE entry\n");
                 Adap_bridgeDomain_semUnlock();
                 return ENET_FAILURE;
            }

            Action_id = entryStp.data.actionId;

            if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,
                                        &(entryStp.key)) != MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: ENET_ADAPTOR_Delete_SE_Entry for STP FAIL!!!\n");
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }

            /* Delete Action for STP */
            if (MEA_API_IsExist_Action  (MEA_UNIT_0, Action_id , &exist)!=MEA_OK)
            {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: MEA_API_IsExist_Action failed for action:%d\n", Action_id);
               Adap_bridgeDomain_semUnlock();
               return ENET_FAILURE;
            }
            if(exist == MEA_TRUE)
            {
                if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Action_id)!=MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvRemoveTunnelOutTranslation FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n", Action_id);
                    Adap_bridgeDomain_semUnlock();
                    return ENET_FAILURE;
                }
            }
        }
        Adap_bridgeDomain_semUnlock();
    }

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvLxcpUpdateProtocols (ADAP_Uint32 ifIndex,MEA_LxCP_Protocol_Action_te action,MEA_LxCP_Protocol_te lxcp_type)
{
    MEA_LxCp_t                    LxCp_Id;
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

	key_pi.protocol = lxcp_type;

	if(getLxcpId(ifIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
		return MEA_ERROR;
	}

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed MEA_API_Get_LxCP_Protocol ifIndex:%d \n",ifIndex);
		return ENET_FAILURE;
	}

	if(data_pi.action_type == action)
	{
		return ENET_SUCCESS;
	}
	data_pi.action_type = action;

	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed ENET_ADAPTOR_Set_LxCP_Protocol ifIndex:%d \n",ifIndex);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvRemProviderTunnel(tEnetHal_MacAddr *daMac)
{
	ADAP_Uint32                           VlanId;
	MEA_SE_Entry_dbt                      entry;
	ADAP_Uint16                           VpnIndex;
	tBridgeDomain 						  *pBridgeDomain=NULL;

	for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if( (pBridgeDomain != NULL) || (VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID) )
		{
           adap_memset(&entry, 0, sizeof(entry));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_mac_to_arr(daMac, entry.key.mac_plus_vpn.MAC.b);

            if(VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID)
            	VpnIndex = ADAP_DEFAULT_PROTOCOLS_TRAFFIC;
            else
            	VpnIndex = pBridgeDomain->vpn;

            entry.key.mac_plus_vpn.VPN = VpnIndex;

        	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
        	{
            	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK)
            	{
            		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed ENET_ADAPTOR_Delete_SE_Entry \n");
            		Adap_bridgeDomain_semUnlock();
            		return MEA_ERROR;
            	}
        		if(entry.data.actionId_valid == MEA_TRUE)
        		{
                    if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,entry.data.actionId)!=MEA_OK)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n",entry.data.actionId);
                        Adap_bridgeDomain_semUnlock();
                        return ENET_FAILURE;
                    }
        		}
        	}
		}
		Adap_bridgeDomain_semUnlock();
	}

	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvRemCustomerTunnel (tEnetHal_MacAddr *daMac)
{
	ADAP_Uint32                           VlanId;
	MEA_SE_Entry_dbt                      entry;
	ADAP_Uint16                           VpnIndex;
	tBridgeDomain 						  *pBridgeDomain=NULL;

	for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if( (pBridgeDomain != NULL) || (VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID) )
		{
            adap_memset(&entry, 0, sizeof(entry));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_mac_to_arr(daMac, entry.key.mac_plus_vpn.MAC.b);

            if(VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID)
            	VpnIndex = ADAP_DEFAULT_PROTOCOLS_TRAFFIC;
            else
            	VpnIndex = pBridgeDomain->vpn;

            entry.key.mac_plus_vpn.VPN = VpnIndex;

        	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
        	{
            	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK)
            	{
            		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed ENET_ADAPTOR_Delete_SE_Entry \n");
            		Adap_bridgeDomain_semUnlock();
            		return MEA_ERROR;
            	}
        	}
		}
		Adap_bridgeDomain_semUnlock();
	}

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvAddCustomerTunnel(tEnetHal_MacAddr *origMac,tEnetHal_MacAddr *newMac,eEnetHal_VlanHwTunnelFilters protocol)
{

	ADAP_Uint32                           VlanId, PortIndex;
    ADAP_Uint32                           PortType;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          Action_id;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_SE_Entry_dbt                      entry;
    ADAP_Uint16                           VpnIndex;
    MEA_Bool							  found_ext=MEA_FALSE;
    MEA_Bool							  found_int=MEA_FALSE;
	tBridgeDomain 						  *pBridgeDomain=NULL;
	MEA_Port_t enetPort;

    adap_memset(&Action_data, 0, sizeof(Action_data));
    /* Add the Trunk port->Customer translation (Tunnel Out) */
    /* For all VLANS: */
	for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if( (pBridgeDomain != NULL) || (VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID) )
		{
        	found_ext=MEA_FALSE;
        	found_int=MEA_FALSE;
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Add the Trunk port->Customer translation for Vlan: %d \n", VlanId);
            /* Clear the SE entry struct */
            adap_memset(&entry, 0, sizeof(entry));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_mac_to_arr(origMac, entry.key.mac_plus_vpn.MAC.b);

            if(VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID)
            	VpnIndex = ADAP_DEFAULT_PROTOCOLS_TRAFFIC;
            else
            	VpnIndex = pBridgeDomain->vpn;

            entry.key.mac_plus_vpn.VPN = VpnIndex;

            /* For all ports in VLAN: */
            adap_memset(&Action_outPorts, 0, sizeof(Action_outPorts));

            for(PortIndex = 1; PortIndex < MeaAdapGetNumOfPhyPorts(); PortIndex ++)
            {
                if(adap_VlanCheckPortsInDomain(pBridgeDomain, PortIndex) != 0)
                {
                    PortType = adap_GetVlanPortTunnelType(PortIndex);

                     if(PortType == ADAP_VLAN_TUNNEL_EXTERNAL)
                    {
                        if(adap_GetTunnelOption(PortIndex,protocol) == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL)
                        {
                        	found_ext=MEA_TRUE;
                        }
                    }
                    else
                    {
                    	ADAP_Uint32	    masterPort=0;

                    	found_int = MEA_TRUE;
                    	masterPort = PortIndex;

                    	if(adap_IsPortChannel(PortIndex) == ENET_SUCCESS)
                    	{
                    		if( (adap_NumOfLagPortFromAggrId(PortIndex)) > 0)
                    		{
                    			masterPort = adap_GetLagMasterPort(PortIndex);
                    		}
                    		else
                    		{
                    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"ifindex:%d not have any physical ports\n",PortIndex);
                    			continue;
                    		}
                    	}
                        /*Add the port to the flooding domain lists */
                    	if(MeaAdapGetPhyPortFromLogPort(masterPort, &enetPort) != ENET_SUCCESS)
                    	{
                    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",PortIndex);
                    		Adap_bridgeDomain_semUnlock();
                    		return ENET_FAILURE;
                    	}
                        MEA_SET_OUTPORT(&entry.data.OutPorts, enetPort);
                    }
                }
            }
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"external_tunnel:%d internal_tunnel:%d\n", found_ext,found_int);
            if( (found_ext == MEA_TRUE) && (found_int == MEA_TRUE) )
            {
				/* Fill in the SE data parameters */
				entry.data.aging = 3; //Default
				entry.data.static_flag  = MEA_TRUE;
				entry.data.actionId_valid  = MEA_TRUE;

				Action_data.output_ports_valid  = MEA_FALSE;
				Action_data.force_color_valid = MEA_FALSE;
				Action_data.COLOR             = 0;
				Action_data.force_cos_valid   = MEA_FALSE;
				Action_data.COS               = 0;
				Action_data.force_l2_pri_valid = MEA_FALSE;
				Action_data.L2_PRI             = 0;

				Action_data.pm_id_valid        = MEA_TRUE;
				Action_data.pm_id              = 0; /* Request new id */
				Action_data.tm_id_valid        = MEA_FALSE;
				Action_data.tm_id              = 0; /* Request new id */
				Action_data.ed_id_valid        = MEA_TRUE;
				Action_data.ed_id              = 0; /* Request new id */
				Action_data.protocol_llc_force = MEA_TRUE;
				Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
				Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
				Action_data.Llc                = MEA_FALSE;

				/* Build the action editing */
				adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
				adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
				Action_editing.num_of_entries = 1;
				Action_editing.ehp_info = &Action_editing_info;
				Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
				Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
				Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = VlanId;

				Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
				Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS; //MEA_EGRESS_HEADER_PROC_CMD_SWAP;

				Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
				Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa=1;

				adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);

				/* Create Action and set LxCp for GMRP */
				adap_mac_to_arr(newMac, Action_editing.ehp_info[0].ehp_data.martini_info.DA.b);

				/* Create the action */
				Action_id = MEA_PLAT_GENERATE_NEW_ID;

				if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
										   &Action_data,
										   &Action_outPorts,
										   NULL,
										   &Action_editing,
										   &Action_id) != MEA_OK)
				{
				   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
				   Adap_bridgeDomain_semUnlock();
				   return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"  created for GMRP with ActionId: %d\n", Action_id);

				entry.data.actionId = Action_id;
				entry.data.actionId_valid  = MEA_TRUE;
				/* Commit the new entry to the forwarder */
				if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Create_SE_Entry for GMRP FAIL!!!\n");
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SE entry created for GMRP with ActionId: %d macAddress:%s\n",
							Action_id,adap_mac_to_string(newMac));
            }
        }
		Adap_bridgeDomain_semUnlock();
    }

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvAddProviderTunnel (tEnetHal_MacAddr *origMac,tEnetHal_MacAddr *newMac,eEnetHal_VlanHwTunnelFilters protocol)
{

	ADAP_Uint32                           VlanId, PortIndex;
    ADAP_Uint32                           PortType;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          Action_id;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_SE_Entry_dbt                      entry;
    ADAP_Uint16                           VpnIndex;
    MEA_Bool							  found_ext=MEA_FALSE;
    MEA_Bool							  found_int=MEA_FALSE;
	tBridgeDomain 						  *pBridgeDomain=NULL;
	MEA_Port_t							  enetPort;

    adap_memset(&Action_data, 0, sizeof(Action_data));
    /* Add the Trunk port->Customer translation (Tunnel Out) */
    /* For all VLANS: */
	for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if( (pBridgeDomain != NULL) || (VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID) )
		{
        	found_ext=MEA_FALSE;
        	found_int=MEA_FALSE;
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Add the Trunk port->Customer translation for Vlan: %d \n", VlanId);
            /* Clear the SE entry struct */
            adap_memset(&entry, 0, sizeof(entry));

            /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            adap_mac_to_arr(origMac, entry.key.mac_plus_vpn.MAC.b);

            if(VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID)
            	VpnIndex = ADAP_DEFAULT_PROTOCOLS_TRAFFIC;
            else
            	VpnIndex = pBridgeDomain->vpn;

            entry.key.mac_plus_vpn.VPN = VpnIndex;

            /* For all ports in VLAN: */
            adap_memset(&Action_outPorts, 0, sizeof(Action_outPorts));

            for(PortIndex = 1; PortIndex < MeaAdapGetNumOfPhyPorts(); PortIndex ++)
            {
                if(adap_VlanCheckPortsInDomain(pBridgeDomain, PortIndex) != 0)
                {
                    PortType = adap_GetVlanPortTunnelType(PortIndex);

                     if(PortType == ADAP_VLAN_TUNNEL_EXTERNAL)
                    {
                        /*Add the port to the flooding domain lists */
						if(MeaAdapGetPhyPortFromLogPort(PortIndex, &enetPort) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",PortIndex);
							Adap_bridgeDomain_semUnlock();
							return ENET_FAILURE;
						}

                        MEA_SET_OUTPORT(&entry.data.OutPorts, enetPort);
                        if(adap_GetTunnelOption(PortIndex,protocol) == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL)
                        {
                        	found_ext=MEA_TRUE;
                        }
                    }
                    else
                    {
                    	found_int = MEA_TRUE;
                    }
                }
            }
        }
		Adap_bridgeDomain_semUnlock();
        if( (found_ext == MEA_TRUE) && (found_int == MEA_TRUE) )
        {
        	break;
        }
    }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"external_tunnel:%d internal_tunnel:%d\n", found_ext,found_int);
	if( (found_ext == MEA_TRUE) && (found_int == MEA_TRUE) )
	{
		for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
		{
			Adap_bridgeDomain_semLock();
			pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

			if( (pBridgeDomain != NULL) || (VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID) )
			{
	            if(VlanId == ADAP_DEFAULT_SERVICE_VLAN_ID)
	            	VpnIndex = ADAP_DEFAULT_PROTOCOLS_TRAFFIC;
	            else
	            	VpnIndex = pBridgeDomain->vpn;

	            entry.key.mac_plus_vpn.VPN = VpnIndex;

				/* Fill in the SE data parameters */
				entry.data.aging = 3; //Default
				entry.data.static_flag  = MEA_TRUE;
				entry.data.actionId_valid  = MEA_TRUE;

				Action_data.output_ports_valid  = MEA_FALSE;
				Action_data.force_color_valid = MEA_FALSE;
				Action_data.COLOR             = 0;
				Action_data.force_cos_valid   = MEA_FALSE;
				Action_data.COS               = 0;
				Action_data.force_l2_pri_valid = MEA_FALSE;
				Action_data.L2_PRI             = 0;

				Action_data.pm_id_valid        = MEA_TRUE;
				Action_data.pm_id              = 0; /* Request new id */
				Action_data.tm_id_valid        = MEA_FALSE;
				Action_data.tm_id              = 0; /* Request new id */
				Action_data.ed_id_valid        = MEA_TRUE;
				Action_data.ed_id              = 0; /* Request new id */
				Action_data.protocol_llc_force = MEA_TRUE;
				Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
				Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
				Action_data.Llc                = MEA_FALSE;

				/* Build the action editing */
				adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
				adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
				Action_editing.num_of_entries = 1;
				Action_editing.ehp_info = &Action_editing_info;
				Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
				Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
				Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = VlanId;

				Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
				Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS; //MEA_EGRESS_HEADER_PROC_CMD_SWAP;

				Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
				Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa=1;

				adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);

				/* Create Action and set LxCp for GMRP */
				adap_mac_to_arr(newMac, Action_editing.ehp_info[0].ehp_data.martini_info.DA.b);

				/* Create the action */
				Action_id = MEA_PLAT_GENERATE_NEW_ID;

				if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
										   &Action_data,
										   &Action_outPorts,
										   NULL,
										   &Action_editing,
										   &Action_id) != MEA_OK)
				{
				   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
				   Adap_bridgeDomain_semUnlock();
				   return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Action created for GMRP with ActionId: %d\n", Action_id);

				entry.data.actionId = Action_id;
				entry.data.actionId_valid  = MEA_TRUE;
				/* Commit the new entry to the forwarder */
				if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Create_SE_Entry for GMRP FAIL!!!\n");
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"SE entry created for GMRP with ActionId: %d macAddress:%s\n",
							Action_id,adap_mac_to_string(newMac));
			}
	    }
		Adap_bridgeDomain_semUnlock();
	}

    return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvSetLacpTunnelConfiguration(ADAP_Uint32 u4ContextId,ADAP_Uint32 u4IfIndex, ADAP_Uint16 VpnIndex)
{
    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
    MEA_EHP_Info_dbt                       Action_editing_info;
    MEA_SE_Entry_dbt                       entry;
    tEnetHal_MacAddr					   destMacAddress = {.ether_addr_octet = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd4 }};
    tEnetHal_MacAddr					   newDestMacAddress = {.ether_addr_octet = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x02 }};
	MEA_Port_t							   enetPort;

    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));
    adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&Action_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&entry, 0, sizeof(MEA_SE_Entry_dbt));
	adap_memset(&Action_editing_info,0,sizeof(MEA_EHP_Info_dbt));

	adap_mac_to_arr(&destMacAddress, entry.key.mac_plus_vpn.MAC.b);

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}

	/* Fill in the SE data parameters */
	Action_data.output_ports_valid  = MEA_FALSE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_FALSE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_FALSE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;

	/* Build the action editing */
	Action_editing.num_of_entries = 1;
	Action_editing.ehp_info = &Action_editing_info;

	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;


	Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
	Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	Action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;

	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = 0;

	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.LmId = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_cfm = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
	Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

	adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);
	adap_mac_to_arr(&newDestMacAddress, Action_editing.ehp_info[0].ehp_data.martini_info.DA.b);

	/* Create the action */
	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   NULL,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
	   return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetLacpTunnelConfiguration: Action created with ActionId: %d\n", Action_id);

	entry.data.actionId = Action_id;
	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;
	entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	/* Commit the new entry to the forwarder */
	if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetLacpTunnelConfiguration FAIL: ENET_ADAPTOR_Create_SE_Entry FAIL!!!\n");
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetLacpTunnelConfiguration: SE entry created ActionId: %d\n", Action_id);
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvAddPnpTunnel(eEnetHal_VlanHwTunnelFilters ProtocolId)
{
	ADAP_Uint16 VlanId=0;
	ADAP_Uint32 u4IfIndex=0;
	tBridgeDomain 	  *pBridgeDomain=NULL;
	ADAP_Uint32 		modeType=0;

	for(VlanId = 1; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId ++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if(pBridgeDomain != NULL)
		{
			for(u4IfIndex=1;u4IfIndex<MeaAdapGetNumOfPhyPorts();u4IfIndex++)
			{
				if(adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) != 0)
				{
					if( (ProtocolId == ENETHAL_VLAN_NP_GVRP_PROTO_ID) || (ProtocolId == ENETHAL_VLAN_NP_GMRP_PROTO_ID) )
					{
						//TODO for MSTP
						//ADAP_Uint16 VpnId=0;
						//VpnId = pBridgeDomain->vpn;
						//MeaDrvDeleteTxForwarder((char *)adap_Mac_gmrp_all_routes,VpnId);
						//MeaDrvDeleteTxForwarder((char *)gMac_gvrp_all_routes,VpnId);
						//MeaDrvDeleteTxForwarder((char *)gMac_mvrp_all_routes,VpnId);
					}
					AdapGetProviderCoreBridgeMode(&modeType,u4IfIndex);

					if(modeType == ADAP_PROVIDER_NETWORK_PORT)
					{
						switch(ProtocolId)
						{
							case ENETHAL_VLAN_NP_STP_PROTO_ID:
								MeaDrvLxcpUpdateProtocols (u4IfIndex,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,MEA_LXCP_PROTOCOL_STP);
								break;
							case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
								MeaDrvLxcpUpdateProtocols (u4IfIndex,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
								MeaDrvLxcpUpdateProtocols (u4IfIndex,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,MEA_LXCP_PROTOCOL_IN_BPDU_MMRP);
								break;
							case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
								MeaDrvLxcpUpdateProtocols (u4IfIndex,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
								MeaDrvLxcpUpdateProtocols (u4IfIndex,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,MEA_LXCP_PROTOCOL_IN_BPDU_MMRP);
								break;
							default:
								break;
						}
					}
				}
			}
		}
		Adap_bridgeDomain_semUnlock();
	}
	return ENET_SUCCESS;
}

MEA_Status MeaDrvCreateTxForwarder(MEA_Action_t action_id,tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      entry;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, entry.key.mac_plus_vpn.MAC.b);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvCreateTxForwarder vpn:%d entry allready exist!!!\n",VpnIndex);
		return MEA_OK;
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, entry.key.mac_plus_vpn.MAC.b);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;

	if(action_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		entry.data.actionId_valid  = MEA_TRUE;
		entry.data.actionId = action_id;
	}
	else
	{
		entry.data.actionId_valid  = MEA_FALSE;
	}

	entry.data.OutPorts_valid = MEA_TRUE;
	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateTxForwarder FAIL: ENET_ADAPTOR_Create_SE_Entry for Tx CCM traffic!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaDrvDeleteTxForwarder(tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_key_dbt key;
	MEA_SE_Entry_dbt                      entry;

	adap_memset(&entry, 0, sizeof(entry));


	key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, key.mac_plus_vpn.MAC.b);
	key.mac_plus_vpn.VPN = VpnIndex;

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, entry.key.mac_plus_vpn.MAC.b);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvDeleteTxForwarder entry not found!!!\n");
		return MEA_OK;
	}

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvDeleteTxForwarder entry not found!!!\n");
		return MEA_OK;
	}
	return MEA_OK;
}

MEA_Status MeaDrvMstpCreateCpuAction(MEA_Action_t   *Action_id)
{
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_EHP_Info_dbt tEhp_info;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	// create action send to cpu
	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	//tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	//tEhp_info.ehp_data.eth_info.val.vlan.vid      = 0;
	tEhp_info.ehp_data.eth_info.val.all 	 	= ADAP_ETHERTYPE;

#ifdef USING_DOUBLE_TAG_ACTION
	tEhp_info.ehp_data.atm_info.val.all = 		ADAP_ETHERTYPE ;
	tEhp_info.ehp_data.atm_info.double_tag_cmd        = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
#endif


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info.output_info,127);
	MEA_SET_OUTPORT(&tOutPorts, 127);

	return MeaDrvHwCreateAction(Action_id,&tEhp_info,1,&tOutPorts,NULL,NULL,0,0,MEA_PLAT_GENERATE_NEW_ID);

}

ADAP_Int32 MeaDrvSetPort802Dot1Lxcp(ADAP_Int32 ifIndex,MEA_LxCP_Protocol_Action_te action)
{
	MEA_LxCp_t                 				LxCp_Id;
   MEA_LxCP_Protocol_key_dbt              	LxCp_key;
   MEA_LxCP_Protocol_data_dbt            	LxCp_data;

	adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
	adap_memset(&LxCp_data, 0, sizeof(LxCp_data));

	// not configure
	if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetPort802Dot1Lxcp not configured to action\n");
		return ENET_SUCCESS;
	}

	if(getLxcpId(ifIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	LxCp_key.protocol = MEA_LXCP_PROTOCOL_802_1X;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_LxCP_Protocol(GMRP) FAIL! LxCp ID:%d\n",
			   LxCp_Id);
		return ENET_FAILURE;
	}

	if(LxCp_data.action_type == action)
	{
		return ENET_SUCCESS;
	}

	LxCp_data.action_type = action;
	LxCp_key.protocol = MEA_LXCP_PROTOCOL_802_1X;

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Set_LxCP_Protocol FAIL!\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvSetBpdu3Lxcp(ADAP_Int32 ifIndex,MEA_LxCP_Protocol_Action_te action)
{
	MEA_LxCp_t                 				LxCp_Id;
   MEA_LxCP_Protocol_key_dbt              	LxCp_key;
   MEA_LxCP_Protocol_data_dbt            	LxCp_data;

	adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
	adap_memset(&LxCp_data, 0, sizeof(LxCp_data));

	// not configure
	if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetPort802Dot1Lxcp not configured to action\n");
		return ENET_SUCCESS;
	}

	if(getLxcpId(ifIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_3;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_LxCP_Protocol(GMRP) FAIL! LxCp ID:%d\n",
			   LxCp_Id);
		return ENET_FAILURE;
	}

	if(LxCp_data.action_type == action)
	{
		return ENET_SUCCESS;
	}

	LxCp_data.action_type = action;
	LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_3;

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Set_LxCP_Protocol FAIL!\n");
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvPortMacSetFilterSrcMacAddress( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_VlanId vlanId, tEnetHal_VlanId editVlanId,tEnetHal_MacAddr *MacAddr,ADAP_Uint32 edit,MEA_Filter_t *pFilterId)
{
	MEA_Filter_Key_dbt 						tFilterKey;
	MEA_Filter_Data_dbt      				tFilterData;
	MEA_Policer_Entry_dbt   				PolicerEntry;
	MEA_Uint16                  			policer_id;
	MEA_EgressHeaderProc_Array_Entry_dbt    filter_editing;
	ADAP_Uint32								PortIndex=0;
	ADAP_Uint32								num_of_ports=0;
	MEA_OutPorts_Entry_dbt					outPorts;
	MEA_EHP_Info_dbt						info;
	tBridgeDomain 							*pBridgeDomain=NULL;
	MEA_Port_t							   enetPort;
    MEA_Service_Entry_Data_dbt              Service_data;

	adap_memset(&Service_data  , 0 , sizeof(Service_data ));
	adap_memset(&tFilterKey,0,sizeof(MEA_Filter_Key_dbt));
	adap_memset(&tFilterData,0,sizeof(MEA_Filter_Data_dbt));
	adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
	adap_memset(&filter_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&info  , 0 , sizeof(MEA_EHP_Info_dbt ));


	for(PortIndex=1;PortIndex<MAX_NUM_OF_SUPPORTED_PORTS;PortIndex++)
	{
		if(u4IfIndex == PortIndex)
		{
			continue;
		}

    	Adap_bridgeDomain_semLock();
    	pBridgeDomain = Adap_getBridgeDomainByVlan(vlanId,0);

    	if(pBridgeDomain != NULL)
        {
   			if( adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) == ADAP_TRUE)
			{
   				if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) != ENET_SUCCESS)
   				{
   					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
   					Adap_bridgeDomain_semUnlock();
   					return ENET_FAILURE;
   				}
				MEA_SET_OUTPORT(&outPorts, enetPort);
				MEA_SET_OUTPORT(&info.output_info,enetPort);
				num_of_ports++;
			}
        }
    	Adap_bridgeDomain_semUnlock();
	}
	if(num_of_ports == 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"no port found on vlan:%d\n", editVlanId);
		return ENET_SUCCESS;
	}


	info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
	info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;


	info.ehp_data.eth_info.command = edit; //MEA_EGRESS_HEADER_PROC_CMD_APPEND;

	info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	info.ehp_data.eth_info.val.vlan.vid = editVlanId;



	/* set filter parameters */
	tFilterKey.layer2.ethernet.SA_valid = MEA_TRUE;
	adap_mac_to_arr(MacAddr, tFilterKey.layer2.ethernet.SA.b);

	tFilterKey.interface_key.src_port_valid = MEA_TRUE;
	tFilterKey.interface_key.src_port = u4IfIndex;
	tFilterKey.interface_key.sid_valid = MEA_FALSE;
	tFilterKey.interface_key.acl_prof       = 0;

	if(Service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
	    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
	                            Service_id,
	                            NULL,
	                            &Service_data,
								NULL, /* outPorts */
								NULL, /* policer  */
								NULL) != MEA_OK)
	    {
	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetRegenUserPriority ERROR: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service_id);
	        return ENET_FAILURE;
	    }

		tFilterKey.interface_key.src_port_valid = MEA_FALSE;
		tFilterKey.interface_key.src_port = 0;
		tFilterKey.interface_key.sid_valid      =  Service_data.ACL_Prof_valid;  //MEA_TRUE;
		tFilterKey.interface_key.acl_prof       = Service_data.ACL_Prof;
	}


	tFilterData.action_type = MEA_FILTER_ACTION_TO_ACTION;
	tFilterData.action_params.proto_llc_valid = MEA_TRUE;
	tFilterData.action_params.protocol_llc_force = MEA_TRUE;
	tFilterData.action_params.Protocol = 0x1;
	tFilterData.action_params.Llc = 0;
	tFilterData.action_params.ed_id_valid=MEA_TRUE;
	tFilterData.action_params.ed_id=0;

	tFilterData.action_params.pm_id_valid = MEA_TRUE;
	tFilterData.action_params.pm_id = 0;

	tFilterData.action_params.tm_id_valid = MEA_TRUE;
	tFilterData.action_params.tm_id  = 0;

	policer_id = MeaAdapGetDefaultPolicer(u4IfIndex);
	if(policer_id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get policer from IfIndex:%d\n",u4IfIndex);
		return ENET_FAILURE;
	}



	if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,policer_id, 0, &PolicerEntry )!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," MEA_API_Get_Policer_ACM_Profile FAIL\n");
		return ENET_FAILURE;
	}



	filter_editing.ehp_info = &info;
	filter_editing.num_of_entries=1;
	tFilterData.action_params.output_ports_valid = MEA_TRUE;



	(*pFilterId)=MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
							   &tFilterKey,
							   NULL,
							   &tFilterData,
							   &outPorts,
							   &PolicerEntry,
							   &filter_editing,
							   pFilterId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: ENET_ADAPTOR_Create_Filter FAIL for FilterId %d failed\n",*pFilterId);
		return ENET_FAILURE;
	}
	return ENET_SUCCESS;

}

ADAP_Uint32 MeaDrvSetPortWhiteListFlag(MEA_Service_t  *Service_id,MEA_Bool enable,ADAP_Uint16 u2PortNum)
{
	MEA_Service_Entry_Data_dbt 				Service_data;
	MEA_OutPorts_Entry_dbt 					Service_outPorts;
	MEA_Policer_Entry_dbt					Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt	Service_editing;
	MEA_Bool								valid;
	MEA_Service_Entry_Key_dbt           	Service_Key;
	MEA_Action_t							actionId;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;
    tEnetHal_VlanId							VlanId;
    tServiceDb							    *pServiceAlloc=NULL;
	tServiceDb 								*pServiceInfo=NULL;

    ADAP_UNUSED_PARAM(u2PortNum);
	// first get service Id
	adap_memset(&Service_data  , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	adap_memset(&Service_outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&Service_policer  , 0 , sizeof(MEA_Policer_Entry_dbt ));
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_Key  , 0 , sizeof(MEA_Service_Entry_Key_dbt ));

    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,*Service_id,&valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_IsExist_Service_ById for service Id %d failed\n", *Service_id);
		return ENET_FAILURE;
	}
	if(valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ServiceId %d does not exists\n", *Service_id);
		return ENET_FAILURE;
	}

	/* Add the  the ServiceEditingTable with the flooding ports lists per table entry */
	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							*Service_id,
							&Service_Key,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service failed for service:%d\n", *Service_id);
		return ENET_FAILURE;
	}

	// allready configured
	if(enable == Service_data.ACL_filter_info.filter_Whitelist_enable)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"the white list already in :%d mode\n", Service_data.ACL_filter_info.filter_Whitelist_enable);
		return ENET_SUCCESS;
	}

	/* Allocate the memory for the EHP table. Reserve one more entry in advance in case we'll need to add an entry */
	Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * (Service_editing.num_of_entries+1));

	if (Service_editing.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							*Service_id,
							&Service_Key,
							&Service_data,
							&Service_outPorts, /* outPorts */
							&Service_policer, /* policer  */
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",*Service_id);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
	{
		actionId = Service_data.DSE_learning_actionId;
		if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0,actionId) == MEA_TRUE)
		{
			adap_memset(&Action_data     , 0 , sizeof(Action_data    ));


			adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
			adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
			adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

			if(ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
								Service_data.DSE_learning_actionId,
								&Action_data,
								&Action_outPorts,
								&Action_policer,
								&Action_editing)!= MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Action failed for actionId:%d\n", Service_data.DSE_learning_actionId);
				MEA_OS_free(Service_editing.ehp_info);
				return ENET_FAILURE;
			}

			Action_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * (Action_editing.num_of_entries+1));

			if (Action_editing.ehp_info == NULL )
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Action_editing.ehp_info\n");
					MEA_OS_free(Service_editing.ehp_info);
					return ENET_FAILURE;
				}

			if(ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
								Service_data.DSE_learning_actionId,
								&Action_data,
								&Action_outPorts,
								&Action_policer,
								&Action_editing)!= MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Action failed for actionId:%d\n", Service_data.DSE_learning_actionId);
				MEA_OS_free(Service_editing.ehp_info);
				MEA_OS_free(Action_editing.ehp_info);
				return ENET_FAILURE;
			}

		}
	}
	if(enable == MEA_TRUE)
	{
		Service_data.ACL_filter_info.data_info[0].mask.mask_0_31 = 0xFFFFFFFF;
		Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 = 0x000FFFFF;
		Service_data.ACL_filter_info.data_info[0].valid = MEA_TRUE;

		Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 |= ((MEA_Uint32)0x1FF << 20);


		Service_data.ACL_filter_info.filter_Whitelist_enable = MEA_TRUE;
		Service_data.filter_enable = MEA_TRUE;
		Service_data.ACL_filter_info.data_info[0].filter_key_type = MEA_FILTER_KEY_TYPE_SA_MAC;
	}
	else
	{
		Service_data.ACL_filter_info.data_info[0].mask.mask_0_31 = 0;
		Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 = 0;
		Service_data.ACL_filter_info.data_info[0].valid = MEA_FALSE;

		Service_data.ACL_filter_info.filter_Whitelist_enable = MEA_FALSE;
		Service_data.filter_enable = MEA_FALSE;
	}

	pServiceInfo =  MeaAdapGetServiceByServiceId(*Service_id);
    if (!pServiceInfo)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Failed to get service info:%d\n", *Service_id);
        MEA_OS_free(Service_editing.ehp_info);
        MEA_OS_free(Action_editing.ehp_info);
        return ENET_FAILURE;
    }

	VlanId = pServiceInfo->outerVlan;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete serviceid:%d\n", *Service_id);
	MeaDeleteEnetServiceId(*Service_id);

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create serviceid:%d\n", *Service_id);
   // Service_id=ENET_PLAT_GENERATE_NEW_ID;

    if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
    {
		if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0,Service_data.DSE_learning_actionId) != MEA_TRUE)
		{
			actionId = MEA_PLAT_GENERATE_NEW_ID;
			Action_data.ed_id_valid = MEA_TRUE;
			Action_data.ed_id              = 0;			/* Request new id */
			if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
									   &Action_data,
									   &Action_outPorts,
									   &Action_policer,
									   &Action_editing,
									   &actionId) != MEA_OK)
			{

			   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwAddMcastEntry ERROR: ENET_ADAPTOR_Create_Action failed for action:%d\n", actionId);
			   MEA_OS_free(Service_editing.ehp_info);
			   MEA_OS_free(Action_editing.ehp_info);
			   return ENET_FAILURE;
			}
			Service_data.DSE_learning_actionId_valid = MEA_TRUE;
			Service_data.DSE_learning_actionId = actionId;
		}
    }

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	pServiceAlloc->serviceId = ENET_PLAT_GENERATE_NEW_ID;
    *Service_id = pServiceAlloc->serviceId;
	if(ENET_ADAPTOR_Create_Service (MEA_UNIT_0,
							   &Service_Key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)

	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Create_Service failed for service:%d\n", *Service_id);
		MEA_OS_free(Service_editing.ehp_info);
		 MEA_OS_free(Action_editing.ehp_info);
		 MeaAdapFreeServiceId(pServiceAlloc->serviceId);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create/set the new service %d\n",*Service_id);
	MEA_OS_free(Service_editing.ehp_info);
	 MEA_OS_free(Action_editing.ehp_info);

		// save configuration
		MeaAdapGetPhyPortFromLogPort(u2PortNum, &pServiceAlloc->inPort);
		pServiceAlloc->ifIndex=u2PortNum;
		pServiceAlloc->outerVlan=VlanId;
		pServiceAlloc->innerVlan=Service_Key.inner_netTag_from_value;
		pServiceAlloc->L2_protocol_type=Service_Key.L2_protocol_type;
		pServiceAlloc->vpnId = Service_data.vpn;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"white list set to :%d mode service_id:%d\n", Service_data.ACL_filter_info.filter_Whitelist_enable,*Service_id);

	return ENET_SUCCESS;

}

ADAP_Int32 MeaDrvProviderStpHwLxcpUpdate (ADAP_Uint32 ifIndex, MEA_Bool sendToCpu)
{
    MEA_LxCp_t                    LxCp_Id;
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

	key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_11;

	if(getLxcpId(ifIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed MEA_API_Get_LxCP_Protocol ifIndex:%d \n",ifIndex);
		return ENET_FAILURE;
	}

	if( (sendToCpu == MEA_TRUE) && (data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_CPU) )
	{
		return ENET_SUCCESS;
	}
	else if( (sendToCpu == MEA_FALSE) && (data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT) )
	{
		return ENET_SUCCESS;
	}

	if(sendToCpu == MEA_TRUE)
	{
		data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
	}
	else
	{
		data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
	}

	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed ENET_ADAPTOR_Set_LxCP_Protocol ifIndex:%d \n",ifIndex);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvSetIngressPortPriRule(MEA_Uint32 pri_rule , ADAP_Uint32 ifIndex)
{
	MEA_Port_t                              EnetPortId;
	MEA_Parser_Entry_dbt 					parser_entry_val_def;

	if(MeaAdapGetPhyPortFromLogPort(ifIndex,&EnetPortId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	if (MEA_API_Get_Parser_Entry(MEA_UNIT_0,EnetPortId,&parser_entry_val_def) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortPriRule failed to call MEA_API_Get_Parser_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;
	}

	parser_entry_val_def.f.pri_rule=pri_rule;

	if(MEA_API_Set_Parser_Entry(MEA_UNIT_0,EnetPortId,&parser_entry_val_def) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortPriRule failed to call MEA_API_Set_Parser_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;

	}
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvDelEntryDeleteSrv (ADAP_Uint32 u4IssPort,tEnetHal_VlanId VlanId, ADAP_Uint8 u1IsTagged)
{
	MEA_Service_t   					  Service_id;
	tServiceDb 							  *pServiceId=NULL;

	pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,u4IssPort,u1IsTagged);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;

		if(Service_id == MEA_PLAT_GENERATE_NEW_ID)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvDelEntryDeleteSrv: service not found:%d\n",u1IsTagged);
			return ENET_SUCCESS;
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvDelEntryDeleteSrv: service not found:%d\n",u1IsTagged);
		return ENET_SUCCESS;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvDelEntryDeleteSrv: serviceId:%d\n",Service_id);

	MeaDeleteEnetServiceId(Service_id);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service:%d\n",Service_id);

	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvCreateSVlanTranslationEntry(ADAP_Uint32 ifIndex,MEA_Bool relayIngress,ADAP_Uint16 u2RelaySVlan,ADAP_Uint16 localVlan)
{
	MEA_Bool        valid;
	MEA_Service_Entry_Data_dbt            Service_data;
	MEA_OutPorts_Entry_dbt                Service_outPorts;
	MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_Service_t   					  Service_id;
	MEA_EHP_Info_dbt                      Service_editing_info[8];
	MEA_Service_Entry_Key_dbt      		  serviceKey;
	ADAP_Uint32							  index=0;

	adap_memset(&Service_data,0,sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&Service_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
	adap_memset(&serviceKey,0,sizeof(MEA_Service_Entry_Key_dbt));

	tServiceDb 							  *pServiceId=NULL;
    tServiceDb							  *pServiceAlloc=NULL;

	pServiceId = MeaAdapGetServiceIdByVlanIfIndex(u2RelaySVlan,ifIndex,D_PROVIDER_SERVICE_TAG);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;

		if(Service_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateSVlanTranslationEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
				return ENET_FAILURE;
			}
			if(valid == MEA_TRUE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId:%d exist\n",Service_id);

				if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
									  Service_id,
									  &serviceKey,
									  &Service_data,
									  &Service_outPorts,
									  &Service_policer,
									  &Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateSVlanTranslationEntry ERROR: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
					return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId:%d read second time\n",Service_id);
				Service_editing.ehp_info = &Service_editing_info[0];
				if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
									  Service_id,
									  &serviceKey,
									  &Service_data,
									  &Service_outPorts,
									  &Service_policer,
									  &Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateSVlanTranslationEntry ERROR: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
					return ENET_FAILURE;
				}

				MeaDeleteEnetServiceId(Service_id);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);

				if(relayIngress == MEA_TRUE) //from PNP to otherr
				{
					serviceKey.net_tag          = localVlan;
					serviceKey.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
				}
				else // to pnp
				{
					for(index=0;index<Service_editing.num_of_entries;index++)
					{
						Service_editing.ehp_info[index].ehp_data.atm_info.val.double_vlan_tag.vid = localVlan;     /* Egress S-tag */
						Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.vid      = localVlan;
					}
				}

				Service_data.editId = 0;
				pServiceAlloc = MeaAdapAllocateServiceId();
				if(pServiceAlloc == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
				}

				pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

				if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
										   &serviceKey,
										   &Service_data,
										   &Service_outPorts,
										   &Service_policer,
										   &Service_editing,
										   &pServiceAlloc->serviceId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to create the new service %d\n",  Service_id);
					return ENET_FAILURE;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create the new service %d\n",Service_id);

				// save configuration
				pServiceAlloc->ifIndex=ifIndex;
				MeaAdapGetPhyPortFromLogPort(ifIndex, &pServiceAlloc->inPort);
				pServiceAlloc->outerVlan=localVlan;
				pServiceAlloc->innerVlan=serviceKey.inner_netTag_from_value;
				pServiceAlloc->L2_protocol_type=serviceKey.L2_protocol_type;
				pServiceAlloc->vpnId = Service_data.vpn;
			}
		}
	}
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvSetTunnelToNormalMode(ADAP_Uint16 u4IfIndex,ADAP_Uint32 u4ContextId,eEnetHal_VlanHwTunnelFilters ProtocolId,tEnetHal_MacAddr *MacAddr,tEnetHal_VlanSVlanMap *pVlanSVlanMap,ADAP_Uint32 u4TunnelStatus)
{
	MEA_SE_Entry_dbt  				entry;
	tEnetHal_MacAddr				destMacAddress;
	ADAP_Uint16            			VpnIndex=0;;
	ADAP_Uint16						Action_id=0;;
	MEA_Bool						exist=MEA_FALSE;
	MEA_LxCp_t                     	LxCp_Id=0;
    MEA_LxCP_Protocol_key_dbt       LxCp_key;
    MEA_LxCP_Protocol_data_dbt      LxCp_data;
	tBridgeDomain 			  	    *pBridgeDomain=NULL;

    adap_memset(&entry, 0, sizeof(entry));
    adap_memset(&LxCp_key, 0, sizeof(MEA_LxCP_Protocol_key_dbt));
    adap_memset(&LxCp_data, 0, sizeof(MEA_LxCP_Protocol_data_dbt));

    adap_memcpy(&destMacAddress, MacAddr, sizeof(tEnetHal_MacAddr));
	if(pVlanSVlanMap->SVlanId != ADAP_END_OF_TABLE)
	{
    	Adap_bridgeDomain_semLock();
    	pBridgeDomain = Adap_getBridgeDomainByVlan(pVlanSVlanMap->SVlanId,0);

    	if(pBridgeDomain == NULL)
        {
			   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: domail not found for SVlan:%d!!!\n", pVlanSVlanMap->SVlanId);
			   Adap_bridgeDomain_semUnlock();
			   return ENET_FAILURE;
        }

        VpnIndex = pBridgeDomain->vpn;
        Adap_bridgeDomain_semUnlock();

		/* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
		entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		adap_mac_to_arr(&destMacAddress, entry.key.mac_plus_vpn.MAC.b);

		entry.key.mac_plus_vpn.VPN = VpnIndex;

		/* Get the action ID from the SE */
		if ( ENET_ADAPTOR_Get_SE_Entry (MEA_UNIT_0, &(entry)) == MEA_OK )
		{
			if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,
										&(entry.key)) != MEA_OK)
			{
			   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: ENET_ADAPTOR_Delete_SE_Entry FAIL!!!\n");
			   return ENET_FAILURE;
			}
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetTunnelToNormalMode: SE entry No Entry found\n");
	}
	Action_id = adap_GetTunnelCusToProActionId(u4IfIndex);
	if(Action_id != ADAP_END_OF_TABLE)
	{
		if (MEA_API_IsExist_Action  (MEA_UNIT_0, Action_id , &exist)!=MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: MEA_API_IsExist_Action failed for action:%d\n", Action_id);
		   return ENET_FAILURE;
		}
		if(exist == MEA_TRUE)
		{
			if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Action_id)!=MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n", Action_id);
				return ENET_FAILURE;
			}
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetTunnelToNormalMode: action id not found\n");
	}

	adap_SetTunnelCusToProActionId(u4IfIndex,ADAP_END_OF_TABLE);
	Action_id = adap_GetTunnelProToCusActionId(u4IfIndex);
	if(Action_id != ADAP_END_OF_TABLE)
	{
		if (MEA_API_IsExist_Action  (MEA_UNIT_0, Action_id , &exist)!=MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: MEA_API_IsExist_Action failed for action:%d\n", Action_id);
		   return ENET_FAILURE;
		}
		if(exist == MEA_TRUE)
		{
			if (ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Action_id)!=MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: ENET_ADAPTOR_Delete_Action failed for action:%d\n", Action_id);
				return ENET_FAILURE;
			}
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetTunnelToNormalMode: action id not found\n");

	}
	adap_SetTunnelProToCusActionId(u4IfIndex,ADAP_END_OF_TABLE);

	if(getLxcpId(u4IfIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}

	switch(ProtocolId)
	{
		case ENETHAL_VLAN_NP_STP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_STP;
			break;
		case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_GVRP;
			break;
		case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_GMRP;
			break;
		case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LACP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LLDP_PROTO_ID:
            LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_14;
			break;
		default:
			break;
	}


	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: MEA_API_Get_LxCP_Protocol FAIL for ISS port %d LxCp Id:%d\n",
				u4IfIndex, LxCp_Id);
		return ENET_FAILURE;
	}

	switch ( u4TunnelStatus )
	{
	case ENET_VLAN_TUNNEL_PROTOCOL_DISCARD :
		/* Remove LxCp protocol by defining it as "Transparent" */
		LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_DISCARD;
	break;
	case ENET_VLAN_TUNNEL_PROTOCOL_PEER :
		/* Forward the LxCp protocol to CPU */
		LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
	break;
	default:
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode FAIL: Invalid tunnelStatus:%d \n", u4TunnelStatus);
		return ENET_FAILURE;
	}
	LxCp_data.ActionId_valid = MEA_FALSE;
	LxCp_data.OutPorts_valid = MEA_FALSE;

	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK){
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetTunnelToNormalMode:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! ISS port: %d LxCp Id:%d\n",
				u4IfIndex, LxCp_Id);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"==== end DPLSetTunnelToNormalMode\n");
	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvSetPortTunnelConfiguration(ADAP_Uint32 u4IfIndex,eEnetHal_VlanHwTunnelFilters ProtocolId,ADAP_Uint32 u4ContextId,tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
	MEA_LxCp_t                             LxCp_Id;
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;
    MEA_EHP_Info_dbt                       Action_editing_info;
    ADAP_Uint16								issPort=0;
	tEnetHal_MacAddr						destMacAddress;
    MEA_SE_Entry_dbt                      	entry;
    ADAP_Uint16                            	VpnIndex;
    ADAP_Uint32							  PortType;
    tServiceDb							  *pServiceAlloc=NULL;
    MEA_Port_t                            EnetPortId;
	tBridgeDomain 			  	    	*pBridgeDomain=NULL;

    adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
    adap_memset(&LxCp_data, 0, sizeof(LxCp_data));
    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));
	adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Action_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

    for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
    {
		if(issPort == u4IfIndex)
			continue;

		AdapGetProviderCoreBridgeMode(&PortType,issPort);

		if(PortType == ADAP_PROVIDER_NETWORK_PORT)
		{
			pServiceAlloc = MeaAdapGetServiceIdByEnetPort(MEA_PARSING_L2_KEY_Stag_Ctag,
																pVlanSVlanMap->SVlanId,
																pVlanSVlanMap->CVlanId,
																0,
																issPort);
			if(pServiceAlloc != NULL)
			{
				if(pServiceAlloc->serviceId != MEA_PLAT_GENERATE_NEW_ID)
				{
					if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &EnetPortId) == ENET_SUCCESS)
					{
						printf("logPort:%d, enetPortId:%d\n",u4IfIndex, EnetPortId);
					}
					else
					{
						printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
						return ENET_FAILURE;
				    }
					MEA_SET_OUTPORT(&Action_outPorts, EnetPortId);
				}
			}
		}
	}

	if(MEA_IS_CLEAR_ALL_OUTPORT(&Action_outPorts))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetPortTunnelConfiguration: PNP not found\n");
		return ENET_SUCCESS;
	}

	if(getLxcpId(u4IfIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}

	Action_data.output_ports_valid  = MEA_TRUE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_TRUE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_TRUE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;

	/* Build the action policer */
	adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
	Action_data.tm_id              = 0;
	Action_data.policer_prof_id    = MeaAdapGetDefaultPolicer(u4IfIndex);
	if(Action_data.policer_prof_id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: Default policer profile NOT FOUND\n");
		return ENET_FAILURE;
	}
	if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Action_data.policer_prof_id,
										0,/* ACM_Mode */
										&Action_policer)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: MEA_API_Get_Policer_ACM_Profile for PolilcingProfile %d failed\n",
				Action_data.policer_prof_id);
		return ENET_FAILURE;
	}

	/* Build the action editing */
	adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	Action_editing.num_of_entries = 1;
	Action_editing.ehp_info = &Action_editing_info;
	adap_memcpy(&Action_editing_info.output_info,
				  &Action_outPorts,
				  sizeof(Action_editing_info.output_info));


	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.LmId = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_cfm = 0;

	if(pVlanSVlanMap->u1PepUntag == 1)
	{
		Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	}

	else
	{
		Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	}

	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x88A8;
	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = pVlanSVlanMap->SVlanId;

	Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
	Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

	adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);
	adap_memset(&entry, 0, sizeof(entry));
	adap_mac_to_arr(&destMacAddress, entry.key.mac_plus_vpn.MAC.b);

	switch(ProtocolId)
	{
		case ENETHAL_VLAN_NP_STP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_STP;
			break;
		case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_GVRP;
			break;
		case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
			LxCp_key.protocol = MEA_LXCP_PROTOCOL_GMRP;
			break;
		case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LACP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LLDP_PROTO_ID:
            LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_14;
			break;
		default:
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration unknown protocol option:%d\n",ProtocolId);
			break;
	}
	if(getLxcpId(u4IfIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}

	adap_mac_to_arr(&destMacAddress, Action_editing.ehp_info[0].ehp_data.martini_info.DA.b);

	/* Create the action */
	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   &Action_policer,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n",
			  Action_id);
	   return ENET_FAILURE;
	}

	/* Get the LxCp entry */
	adap_SetTunnelCusToProActionId(u4IfIndex,Action_id);

	LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
	LxCp_data.ActionId_valid = MEA_TRUE;
	LxCp_data.ActionId = Action_id;
	LxCp_data.OutPorts_valid = MEA_TRUE;
	adap_memcpy(&LxCp_data.OutPorts,&Action_outPorts,sizeof(LxCp_data.OutPorts));

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: ENET_ADAPTOR_Set_LxCP_Protocol FAIL!\n");
		return ENET_FAILURE;
	}

   adap_memset(&Action_outPorts, 0, sizeof(Action_outPorts));

   for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
   {
		AdapGetProviderCoreBridgeMode(&PortType,issPort);

		if(PortType == ADAP_CUSTOMER_EDGE_PORT)
		{
			if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &EnetPortId) == ENET_SUCCESS)
			{
				printf("logPort:%d, enetPortId:%d\n",u4IfIndex, EnetPortId);
			}
			else
			{
				printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
				return ENET_FAILURE;
			}
			MEA_SET_OUTPORT(&entry.data.OutPorts, EnetPortId);
		}
   }

	/* Fill in the SE data parameters */
	Action_data.output_ports_valid  = MEA_FALSE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_FALSE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_FALSE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;


	/* Build the action editing */
	adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	Action_editing.num_of_entries = 1;
	Action_editing.ehp_info = &Action_editing_info;

	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	if( (pVlanSVlanMap->u1CepUntag == 0) && (pVlanSVlanMap->u1PepUntag == 1) )
	{
		Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	}
	else if(pVlanSVlanMap->u1CepUntag == 1)
	{
		Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
		Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
		Action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	}
	else
	{
		Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
		Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	}
	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = pVlanSVlanMap->CVlanId;

	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.LmId = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_cfm = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	Action_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
	Action_editing.ehp_info[0].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

	adap_memset(Action_editing.ehp_info[0].ehp_data.martini_info.SA.b, 0, ETH_ALEN);

	switch(ProtocolId)
	{
		case ENETHAL_VLAN_NP_STP_PROTO_ID:
			adap_memcpy(&Action_editing.ehp_info[0].ehp_data.martini_info.DA.b, AdapGetStpMacAddress(), sizeof(tEnetHal_MacAddr));
			break;
		case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
			adap_memcpy(&Action_editing.ehp_info[0].ehp_data.martini_info.DA.b[0], AdapGetGvrpMacAddress(), sizeof(tEnetHal_MacAddr));
			break;
		case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
			adap_memcpy(&Action_editing.ehp_info[0].ehp_data.martini_info.DA.b[0], AdapGetGmrpMacAddress(), sizeof(tEnetHal_MacAddr));
			break;
		case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LACP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
			break;
		case ENETHAL_VLAN_NP_LLDP_PROTO_ID:
            adap_memcpy(&Action_editing.ehp_info[0].ehp_data.martini_info.DA.b[0], AdapGetLldpMacAddress(), sizeof(tEnetHal_MacAddr));
			break;
		default:
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration unknown protocol option:%d\n",ProtocolId);
			break;
	}


	/* Create the action */
	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   NULL,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
	   return ENET_FAILURE;
	}
	adap_SetTunnelProToCusActionId(u4IfIndex,Action_id);
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(pVlanSVlanMap->SVlanId,0);
	if(pBridgeDomain == NULL)
    {
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: domail not found for SVlan:%d!!!\n", pVlanSVlanMap->SVlanId);
		   Adap_bridgeDomain_semUnlock();
		   return ENET_FAILURE;
    }

    VpnIndex = pBridgeDomain->vpn;
    Adap_bridgeDomain_semUnlock();

	entry.data.actionId = Action_id;
	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;
    entry.key.mac_plus_vpn.VPN = VpnIndex;
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	/* Commit the new entry to the forwarder */
	if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPortTunnelConfiguration FAIL: ENET_ADAPTOR_Create_SE_Entry FAIL!!!\n");
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32 MeaDrvSetAllPortTunnelConfigBack(tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	tEnetHal_MacAddr MacAddr={.ether_addr_octet = {0x01,0x00,0x0c,0xcd,0xcd,0xd0}};
	MEA_Filter_t FilterId=MEA_PLAT_GENERATE_NEW_ID;
	tEnetHal_MacAddr MacSwap={.ether_addr_octet = {0x01,0x80,0xc2,0x00,0x00,0x00}};
	ADAP_Uint32 issPort=0;
    tServiceDb		  *pServiceAlloc=NULL;

	for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
	{
		if(issPort == pVlanSVlanMap->u2Port)
		{
			continue;
		}
		if((adap_VlanCheckUntaggedPort(pVlanSVlanMap->SVlanId,issPort) == ADAP_TRUE) ||
			(adap_VlanCheckTaggedPort(pVlanSVlanMap->SVlanId,issPort) == ADAP_TRUE))
		{
			break;
		}
	}

	if(issPort >= MeaAdapGetNumOfPhyPorts() )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetAllPortTunnelConfigBack: ifIndex not found vlan:%d\n",pVlanSVlanMap->SVlanId);
		return ENET_SUCCESS;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetAllPortTunnelConfigBack: portId:%d svlan:%d\n",issPort,pVlanSVlanMap->SVlanId);

	pServiceAlloc = MeaAdapGetServiceIdByEnetPort(MEA_PARSING_L2_KEY_Stag,
														pVlanSVlanMap->SVlanId,
														pVlanSVlanMap->CVlanId,
														0,
														issPort);
	if(pServiceAlloc != NULL)
	{
		if(pServiceAlloc->serviceId != MEA_PLAT_GENERATE_NEW_ID)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLSetAllPortTunnelConfigBack: tagged serviceId:%d\n",pServiceAlloc->serviceId);

			FilterId = adap_DeleteEntryFromFilterList(pServiceAlloc->serviceId);
			if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLSetAllPortTunnelConfigBack: remove FilerId:%d\n",FilterId);
				if(ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,FilterId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Delete_Filter for filter Id %d failed\n", FilterId);
					return ENET_FAILURE;
				}
			}

			if(MeaDrvPortMacSetFilteDestMacAddress(pServiceAlloc->serviceId,issPort,&MacAddr,&FilterId, &MacSwap) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create filter on serviceId\n",pServiceAlloc->serviceId);
			}
			adap_InsertEntryToFilterList(FilterId,pServiceAlloc->serviceId,&MacAddr);
		}
	}

	pServiceAlloc = MeaAdapGetServiceIdByEnetPort(MEA_PARSING_L2_KEY_Stag_Ctag,
														pVlanSVlanMap->SVlanId,
														pVlanSVlanMap->CVlanId,
														0,
														issPort);
	if(pServiceAlloc != NULL)
	{
		if(pServiceAlloc->serviceId != MEA_PLAT_GENERATE_NEW_ID)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLSetAllPortTunnelConfigBack: tagged serviceId:%d\n",pServiceAlloc->serviceId);

			FilterId = adap_DeleteEntryFromFilterList(pServiceAlloc->serviceId);
			if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPLSetAllPortTunnelConfigBack: remove FilerId:%d\n",FilterId);
				if(ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,FilterId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Delete_Filter for filter Id %d failed\n", FilterId);
					return ENET_FAILURE;
				}
			}

			if(MeaDrvPortMacSetFilteDestMacAddress(pServiceAlloc->serviceId,issPort,&MacAddr,&FilterId, &MacSwap) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create filter on serviceId\n",pServiceAlloc->serviceId);
			}
			adap_InsertEntryToFilterList(FilterId,pServiceAlloc->serviceId,&MacAddr);
		}
	}

	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvPortMacSetFilteDestMacAddress( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_MacAddr *MacAddr,MEA_Filter_t *pFilterId,tEnetHal_MacAddr *MacSwap)
{
	MEA_Filter_Key_dbt 						tFilterKey;
	MEA_Filter_Data_dbt      				tFilterData;
	MEA_Policer_Entry_dbt   				PolicerEntry;
	MEA_Uint16                  			policer_id;
	MEA_EgressHeaderProc_Array_Entry_dbt    filter_editing;
	MEA_OutPorts_Entry_dbt					outPorts;
    MEA_Service_Entry_Data_dbt             Service_data;
    MEA_OutPorts_Entry_dbt                 Service_outPorts;
    MEA_Policer_Entry_dbt                  Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Service_editing;
    MEA_Service_Entry_Key_dbt      			key;
    ADAP_Uint32								idx=0;

	adap_memset(&tFilterKey,0,sizeof(MEA_Filter_Key_dbt));
	adap_memset(&tFilterData,0,sizeof(MEA_Filter_Data_dbt));
	adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
	adap_memset(&filter_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&outPorts  , 0 , sizeof(MEA_OutPorts_Entry_dbt ));

    adap_memset(&key,       0 , sizeof(key));
    adap_memset(&Service_data,      0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							&key,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvPortMacSetFilteDestMacAddress Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service_id);
		return ENET_FAILURE;
	}

	Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) *(Service_editing.num_of_entries+1));

	if (Service_editing.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&Service_outPorts, /* outPorts */
							&Service_policer, /* policer  */
							&Service_editing)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",Service_id);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	Service_data.ACL_filter_info.data_info[0].filter_key_type = MEA_FILTER_KEY_TYPE_DA_MAC;
	Service_data.ACL_filter_info.data_info[0].mask.mask_0_31 = 0xFFFFFFFF;
	Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 = 0x000FFFFF;
	Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 |= ((MEA_Uint32)0x1FF << 20);
	Service_data.ACL_filter_info.filter_Whitelist_enable = MEA_FALSE;
	Service_data.filter_enable = MEA_TRUE;

    Service_data.editId = 0; /*create new Id*/

	if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
							Service_id,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);

	/* set filter parameters */
	tFilterKey.layer2.ethernet.DA_valid = MEA_TRUE;
	adap_mac_to_arr(MacAddr, tFilterKey.layer2.ethernet.DA.b);


	tFilterKey.interface_key.src_port_valid = MEA_FALSE;
	tFilterKey.interface_key.src_port = 0;
	tFilterKey.interface_key.sid_valid = MEA_TRUE;
	tFilterKey.interface_key.acl_prof       = Service_data.ACL_Prof;


	tFilterData.action_type = MEA_FILTER_ACTION_TO_ACTION;
	tFilterData.action_params.proto_llc_valid = MEA_TRUE;
	tFilterData.action_params.protocol_llc_force = MEA_TRUE;
	tFilterData.action_params.Protocol = 0x1;
	tFilterData.action_params.Llc = 0;
	tFilterData.action_params.ed_id_valid=MEA_TRUE;
	tFilterData.action_params.ed_id=0;

	tFilterData.action_params.pm_id_valid = MEA_TRUE;
	tFilterData.action_params.pm_id = 0;

	tFilterData.action_params.tm_id_valid = MEA_TRUE;
	tFilterData.action_params.tm_id  = 0;

	policer_id = MeaAdapGetDefaultPolicer(u4IfIndex);
	if(policer_id == MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get policer from IfIndex:%d\n",u4IfIndex);
		return ENET_FAILURE;
	}

	if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,policer_id, 0, &PolicerEntry )!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," MEA_API_Get_Policer_ACM_Profile FAIL\n");
		return ENET_FAILURE;
	}

	filter_editing.ehp_info = &Service_editing.ehp_info[0];
	filter_editing.num_of_entries=Service_editing.num_of_entries;
	tFilterData.action_params.output_ports_valid = MEA_TRUE;

	adap_memcpy(&outPorts , &Service_outPorts , sizeof(MEA_OutPorts_Entry_dbt ));


	for(idx=0;idx<Service_editing.num_of_entries;idx++)
	{
		filter_editing.ehp_info[idx].ehp_data.martini_info.EtherType = 0;
		filter_editing.ehp_info[idx].ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

		adap_memset(filter_editing.ehp_info[idx].ehp_data.martini_info.SA.b, 0, ETH_ALEN);
		adap_mac_to_arr(MacSwap, filter_editing.ehp_info[idx].ehp_data.martini_info.DA.b);

		filter_editing.ehp_info[0].ehp_data.LmCounterId_info.LmId = 0;
		filter_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_cfm = 0;
		filter_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
		filter_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	}


	(*pFilterId)=MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
							   &tFilterKey,
							   NULL,
							   &tFilterData,
							   &outPorts,
							   &PolicerEntry,
							   &filter_editing,
							   pFilterId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: ENET_ADAPTOR_Create_Filter FAIL for FilterId %d failed\n",*pFilterId);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	MEA_OS_free(Service_editing.ehp_info);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FilterId %d created\n",*pFilterId);

	return ENET_SUCCESS;

}

ADAP_Uint32 MeaDrvVlanHwCreateFilterPriorityVlan(MEA_Service_t ExistServiceId,ADAP_Uint16 trafficType,ADAP_Uint16 u4IfIndex,ADAP_Uint8 priority,ADAP_Uint16 VlanId,ADAP_Uint8 RegenPriority)
{

    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt				  OutPorts_Entry;
    MEA_Policer_Entry_dbt          		  Policer_Entry;
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_EHP_Info_dbt                      Service_editing_info;
	MEA_Service_Entry_Key_dbt      		  Service_key;
	MEA_Bool        					  valid=MEA_FALSE;
	MEA_Service_t 						  enet_ServID;
	tServiceDb 							  *pServiceId=NULL;
	tServiceDb							  *pServiceAlloc=NULL;

	adap_memset(&Service_data     , 0 , sizeof(Service_data));
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Policer_Entry, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing, 0, sizeof(Service_editing));
	adap_memset(&Service_editing_info, 0, sizeof(MEA_EHP_Info_dbt));
	adap_memset(&Service_key, 0, sizeof(MEA_Service_Entry_Key_dbt));

	// vlan priority cannot be greater then 7
	if(priority > 7)
	{
		return ENET_SUCCESS;
	}

   if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,ExistServiceId,&valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", "MeaDrvVlanHwCreateFilterPriorityVlan",ExistServiceId);
		return ENET_FAILURE;
	}
	if(valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error: ServiceId %d does not exists\n", "MeaDrvVlanHwCreateFilterPriorityVlan",ExistServiceId);
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							ExistServiceId,
							&Service_key,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s : ERROR by getting service id from Enet issPort:%d,  Port type:%d Vlan:%d \n",
				"MeaDrvVlanHwCreateFilterPriorityVlan",u4IfIndex, trafficType, VlanId);
		return ENET_FAILURE;
	}

	if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
	{
		//create action
	}
	Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

	if (Service_editing.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error:  FAIL to malloc ehp_info\n","MeaDrvVlanHwCreateFilterPriorityVlan");
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							ExistServiceId,
							&Service_key,
							&Service_data,
							&OutPorts_Entry, /* outPorts */
							&Policer_Entry, /* policer  */
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n","MeaDrvVlanHwCreateFilterPriorityVlan",ExistServiceId);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	if(Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Untagged)
	{
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_SUCCESS;
	}
	pServiceId = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,u4IfIndex,trafficType,priority,
	                MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP, Service_key.sub_protocol_type);

	if(pServiceId != NULL)
	{
		/* Check if the service exist */
		enet_ServID = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,enet_ServID,&valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwCreateFilterPriorityVlan Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", enet_ServID);
			return ENET_FAILURE;
		}
		if(valid == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvVlanHwCreateFilterPriorityVlan Error: ServiceId %d does not exists\n", enet_ServID);
			return ENET_FAILURE;
		}

		MeaDeleteEnetServiceId(enet_ServID);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",enet_ServID);
	}

	Service_key.priType = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP;
	Service_key.pri = priority;

	Service_key.evc_enable = MEA_FALSE;
	Service_key.external_internal = MEA_FALSE;

	if(RegenPriority < 8)
	{
		Service_data.L2_PRI_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
		Service_data.L2_PRI       = RegenPriority ;
	}

	Service_data.flowMarkingMappingProfile_force=MEA_FALSE;
	Service_data.flowCoSMappingProfile_force=MEA_FALSE;
	Service_data.DSE_forwarding_enable   = MEA_TRUE;
	Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;

	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

   if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &OutPorts_Entry,
                               &Policer_Entry,
                               &Service_editing,
                               &pServiceAlloc->serviceId) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s FAIL: Unable to create the new service %d\n", "MeaDrvVlanHwCreateFilterPriorityVlan",pServiceAlloc->serviceId);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }

   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvVlanHwCreateFilterPriorityVlan: create the new service %d\n",pServiceAlloc->serviceId);

	MEA_OS_free(Service_editing.ehp_info);

	//Update PM and TM for service
	pServiceAlloc->ifIndex=u4IfIndex;
	MeaAdapGetPhyPortFromLogPort(u4IfIndex, &pServiceAlloc->inPort);
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;
	pServiceAlloc->pmId = Service_data.pmId;

	//Update service priority
	pServiceAlloc->enet_IncPriority = Service_key.pri;
	pServiceAlloc->enet_priority_type = Service_key.priType;

	return ENET_SUCCESS;

}

ADAP_Int32 MeaDrvVlanHwUpdateParameters(MEA_Service_t    Service_id,ADAP_Uint32 ifIndex,MEA_Bool bSet1588, ADAP_Uint32 set_1588,ADAP_Uint16 vlanId)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt				  OutPorts_Entry;
    MEA_Policer_Entry_dbt          		  Policer_Entry;
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_EHP_Info_dbt                      Service_editing_info;
	MEA_Service_Entry_Key_dbt      		  Service_key;
	MEA_Bool        					  valid=MEA_FALSE;
	ADAP_Uint16							  vpn=0;

	adap_memset(&Service_data     , 0 , sizeof(Service_data));
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Policer_Entry, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing, 0, sizeof(Service_editing));
	adap_memset(&Service_editing_info, 0, sizeof(MEA_EHP_Info_dbt));
	adap_memset(&Service_key, 0, sizeof(MEA_Service_Entry_Key_dbt));

   if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id,&valid) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
		return ENET_FAILURE;
	}
	if(valid == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: ServiceId %d does not exists\n", Service_id);
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR by getting service id from Enet ServiceId:%d\n",Service_id);
		return ENET_FAILURE;
	}
	Service_editing.ehp_info=NULL;
	if(Service_editing.num_of_entries)
	{
		Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

		if (Service_editing.ehp_info == NULL )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s Error:  FAIL to malloc ehp_info\n","MeaDrvVlanHwUpdateParameters");
			return ENET_FAILURE;
		}
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry, /* outPorts */
							&Policer_Entry, /* policer  */
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR by getting service id from Enet ServiceId:%d\n",Service_id);
		if(Service_editing.ehp_info != NULL)
			MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	if(Service_data.L4port_mask_enable	== MEA_TRUE)
	{
		Service_data.L3_L4_fwd_enable = MEA_FALSE;
		Service_data.L4port_mask_enable = MEA_FALSE;
		vpn = adap_GetVpnByVlan (vlanId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvVlanHwUpdateParameters vlanId:%d vpn:%d!\n",vlanId,vpn);
		if(vpn != ADAP_END_OF_TABLE)
		{
			Service_data.DSE_forwarding_enable   = MEA_TRUE;
			Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
			Service_data.vpn = vpn;
		}
	}

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," serviceId:%d ISS Port:%d tmId:%d\n", Service_id, ifIndex,Service_data.tmId);

		// set 1588
	if(bSet1588 == MEA_TRUE)
	{
		Service_data.set_1588= set_1588;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DPL_VlanHwUpdateParameters set 1588=%s\r\n",Service_data.set_1588==MEA_TRUE?"true":"false");
	}
    Service_data.editId = 0; /*create new Id*/
    if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
							Service_id,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"QoSUnmapPolicyfromService Error: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
		if(Service_editing.ehp_info != NULL)
			MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"setService serviceId:%d ISS Port:%d tmId:%d\n",	Service_id, ifIndex,Service_data.tmId);

	if(Service_editing.ehp_info != NULL)
		MEA_OS_free(Service_editing.ehp_info);

	return ENET_SUCCESS;
}

ADAP_Int32  MeaDrvVlanHwUpdatePnac (tServiceDb * pServiceId)
{
	MEA_Service_t   				Service_id;
	MEA_Filter_t 					FilterId;
	MEA_Bool						valid=MEA_FALSE;
	tPnacAutorizedDb				*pnacDb = NULL;

	pnacDb = adap_getPnacAutorized (pServiceId->ifIndex);

	if (pnacDb != NULL)
	{
		if(pServiceId != NULL)
		{
			Service_id = pServiceId->serviceId;
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_PnacHwSetAuthStatus ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
				return ENET_FAILURE;
			}

			if(valid == MEA_TRUE)
			{
				if(pnacDb->u1AuthStatus == ADAP_PNAC_PORTSTATUS_AUTHORIZED)
				{
					if(pnacDb->u1AuthMode == ADAP_PNAC_PORT_AUTHMODE_MACBASED)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service:%d VlanId:%d create filter\n",Service_id,0);
						MeaAdapSetEgressPort(INTERNAL_PORT1_NUMBER,1,1);
						MeaAdapSetIngressPort(INTERNAL_PORT1_NUMBER,1,0);
						MeaDrvSetPortWhiteListFlag(&Service_id,MEA_TRUE, pServiceId->ifIndex);
					}
					else
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"clear service white list:%d\n",Service_id);
						MeaDrvSetPortWhiteListFlag(&Service_id,MEA_FALSE,pServiceId->ifIndex);
					}
				}
				else if( pnacDb->u1AuthStatus == ADAP_PNAC_PORTSTATUS_UNAUTHORIZED)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service white list:%d\n",Service_id);
					MeaAdapSetEgressPort(INTERNAL_PORT1_NUMBER,1,1);
					MeaAdapSetIngressPort(INTERNAL_PORT1_NUMBER,1,0);
					MeaDrvSetPortWhiteListFlag(&Service_id,MEA_TRUE,pServiceId->ifIndex);
					if(pnacDb->u1AuthMode == ADAP_PNAC_PORT_AUTHMODE_MACBASED)
					{
						// vlan priority - check if the specific port is part of vpn
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"untagged service:%d VlanId:%d delete filter\n",Service_id,0);
					}
					else if(pnacDb->u1AuthMode == ADAP_PNAC_PORT_AUTHMODE_PORTBASED)
					{
						// remove all MAC from list
						FilterId = adap_DeleteEntryFromFilterList(Service_id);
						while(FilterId != MEA_PLAT_GENERATE_NEW_ID)
						{
							if(ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,FilterId) != MEA_OK)
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Delete_Filter for filter Id %d failed\n", FilterId);
								return ENET_FAILURE;
							}
							FilterId = MEA_PLAT_GENERATE_NEW_ID;
							FilterId = adap_DeleteEntryFromFilterList(Service_id);
						}
					}
				}
			}
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to get ServiceId\r\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to get pnacDb\r\n");
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;

}

MEA_Status MeaDrvCreateMulticastService(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type)
{
	MEA_Service_Entry_Key_dbt             Service_key;
	MEA_Service_Entry_Data_dbt            Service_data;
	MEA_OutPorts_Entry_dbt                Service_outPorts;
	MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EHP_Info_dbt                      Service_editing_info;
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	tServiceDb		             *pServiceAlloc=NULL;
	MEA_Port_t 			      phyPort = 0;

	/* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing,0,sizeof(Service_editing));
        adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));

	if(MeaAdapGetNumOfPhyPorts() > ifIndex)
	{
		MeaAdapGetPhyPortFromLogPort(ifIndex,&phyPort);
	}

	Service_editing.ehp_info       = &Service_editing_info;
	/* Set the service key */
	Service_key.src_port         = phyPort;
	Service_key.pri              = 7;
	if( type == VLAN_UNTAGGED_MEMBER_PORT)
	{
		Service_key.net_tag          = 0xff000;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
		Service_key.sub_protocol_type = 0;
	}
	else
	{
		Service_key.net_tag          = pBridgeDomain->vlanId;
		Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
		Service_key.sub_protocol_type = 0;
	}

	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.inner_netTag_from_valid     = MEA_FALSE;
	Service_key.inner_netTag_from_value     = 0xFFFFFFF;
	Service_key.evc_enable 			= MEA_FALSE;

	pServiceAlloc = MeaAdapGetServiceIdByEnetPort(Service_key.L2_protocol_type,
			pBridgeDomain->vlanId,
			Service_key.inner_netTag_from_value,
			Service_key.sub_protocol_type,
			Service_key.src_port);

	if(pServiceAlloc != NULL)
	{
		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								  pServiceAlloc->serviceId,
								  NULL,
								  &Service_data,
								  &Service_outPorts,
								  &Service_policer,
								  &Service_editing) == MEA_OK)
		{
            if (AdapGetMcastMode() == ADAP_MCAST_MAC_FORWARDING_MODE)
            { 
			    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            }
            else if (AdapGetMcastMode() == ADAP_MCAST_IP_FORWARDING_MODE)
            { 
			    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
            }
                        
			Service_data.DSE_forwarding_enable   = MEA_TRUE;
			Service_data.DSE_learning_enable     = MEA_FALSE;
			Service_data.DSE_learning_actionId_valid = MEA_FALSE;
			Service_data.DSE_learning_srcPort_force = MEA_FALSE;
			Service_data.L3_L4_fwd_enable = MEA_TRUE;
			Service_data.L4port_mask_enable	= MEA_FALSE;

			/* Build the Service editing */
          		Service_data.editId = 0; /*create new Id*/
			Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
			Service_editing.ehp_info[0].ehp_data.atm_info.val.all =
				(MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_VLANID);
			Service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

			Service_editing.ehp_info[0].ehp_data.eth_info.command =	MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			Service_editing.ehp_info[0].ehp_data.eth_info.val.all = ADAP_ETHERTYPE;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = 1;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc  = 12;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid  = MEA_TRUE;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc    = 13;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid  = MEA_TRUE;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc    = 14;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid  = MEA_TRUE;
			Service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc    = 15;

			MEA_SET_OUTPORT(&Service_outPorts, ADAP_CPU_PORT_ID);
			MEA_SET_OUTPORT(&Service_editing.ehp_info[0].output_info,ADAP_CPU_PORT_ID);

			if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
						pServiceAlloc->serviceId,
						&Service_data,
						&Service_outPorts,
						&Service_policer,
						&Service_editing) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
					"FAIL: ENET_ADAPTOR_Set_Service failed for service:%d\n",pServiceAlloc->serviceId);
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", pServiceAlloc->serviceId);
		}
	}

        return MEA_OK;
}

MEA_Status MeaAdapAddStaticNatEntry(MEA_SE_Entry_dbt *pEntry)
{
   tNaptAddressTable NatEntry;

   adap_memset(&NatEntry, 0 ,sizeof(tNaptAddressTable));
   
   NatEntry.localIpAddr = pEntry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4;
   NatEntry.localPort = pEntry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port;
   NatEntry.globalIpAddr = pEntry->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4;
   NatEntry.globalPort = pEntry->key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port;

   if (AdapAddStaticNatEntry (&NatEntry) != ENET_SUCCESS)
   {
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
			"Failed to add static NAT entry\n");
       return MEA_ERROR;
   }
   return MEA_OK;	
}

ADAP_Int32 MeaDrvSetIngressColorAwareInterfaceType (ADAP_Int32 i4Port,MEA_Uint32 port_col_aw)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t				enetPort;

	if(MeaAdapGetPhyPortFromLogPort(i4Port, &enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",i4Port);
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressColorAwareInterfaceType: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
		return ENET_FAILURE;
	}
        entry.parser_info.port_col_aw = port_col_aw;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressColorAwareInterfaceType: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
		return ENET_FAILURE;
	}
    return (ENET_SUCCESS);
}

ADAP_Uint32 MeaDrvSetIngressPortMode(ADAP_Uint32  IP_pri_mask_type , ADAP_Uint32 ifIndex)
{
	MEA_Port_t                              EnetPortId;
	MEA_IngressPort_Entry_dbt 				entry;

	if(MeaAdapGetPhyPortFromLogPort(ifIndex, &EnetPortId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,EnetPortId,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortMode failed to call MEA_API_Get_IngressPort_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;
	}

	entry.parser_info.IP_pri_mask_type = IP_pri_mask_type;

	if(MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,EnetPortId,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortMode failed to call MEA_API_Set_IngressPort_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;

	}
	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvSetIngressPortProtocol(ADAP_Uint32  port_cos_aw,ADAP_Uint32 port_ip_aw , ADAP_Uint32 ifIndex)
{
	MEA_Port_t                              EnetPortId;
	MEA_IngressPort_Entry_dbt 				entry;

	if(MeaAdapGetPhyPortFromLogPort(ifIndex, &EnetPortId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return ENET_FAILURE;
	}

	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,EnetPortId,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortProtocol failed to call MEA_API_Get_IngressPort_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;
	}

       entry.parser_info.port_cos_aw =port_cos_aw;
       entry.parser_info.port_ip_aw = port_ip_aw;

	if(MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,EnetPortId,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortProtocol failed to call MEA_API_Set_IngressPort_Entry:%d\n",ifIndex);
	   return ENET_FAILURE;

	}

	//In case of IP aware port - attach default flow marking profile to all evc services related to this port - TODO
//	if(port_ip_aw)
//	{
//		if(QoSAttachDefaultProfiles (ifIndex) != ENET_SUCCESS)
//		{
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetIngressPortProtocol Unable to attach default flow marking profile for Index: %d!!\n", ifIndex);
//			return ENET_FAILURE;
//		}
//	}
	return ENET_SUCCESS;
}

ADAP_Int32
MeaDrvGetPolicerMeterParams(ADAP_Uint16 MeterIdx, ADAP_Uint16 IssPort, ADAP_Uint16 ServicePM, ADAP_Uint16 *PolicingProfileId,
						 ADAP_Uint16 *TmIdValue, ADAP_Uint16 *PmIdValue, MEA_Policer_Entry_dbt *pPolicerEntry)
{
	tMeterDb   *pMeter=NULL;

	if(MeterIdx == ADAP_END_OF_TABLE)
	{
		// No Meter for this specified policy found - use the default policer and default tm. Pm will be created a new
		*PolicingProfileId = MeaAdapGetDefaultPolicer(IssPort);
		if(*PolicingProfileId == MEA_PLAT_GENERATE_NEW_ID)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvGetPolicerMeterParams Error: No Default ENET Policer for ISS Port:%d\n", IssPort);
			return ENET_FAILURE;
		}

		//No Meter - TmId should be taken from default Meter per port
		*TmIdValue = (adap_GetDefaultPolicerIdTmId(IssPort) == MEA_PLAT_GENERATE_NEW_ID) ? 0 : adap_GetDefaultPolicerIdTmId(IssPort);
		*PmIdValue = 0;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"No Meter - PolicingProfile:%d, TmIdValue:%d, PmIdValue:%d\n", *PolicingProfileId, *TmIdValue, *PmIdValue);
	}
	else
	{
		pMeter = MeaAdapGetMeterByIndex(MeterIdx);
		if(pMeter == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvGetPolicerMeterParams Error: get meter pointer failed:%d\n", MeterIdx);
			return ENET_FAILURE;
		}

		//Meter exists for this policer. 2 different states - first time or no
		if(pMeter->tm_Id == UNDEFINED_VALUE16)
		{
			*PolicingProfileId = 0;
			*TmIdValue = 0;
			*PmIdValue = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Meter first time - PolicingProfile:%d, TmIdValue:%d, PmIdValue:%d\n", *PolicingProfileId, *TmIdValue, *PmIdValue);
		}
		else
		{
			*PolicingProfileId = pMeter->policer_prof_id;
			if(*PolicingProfileId == UNDEFINED_VALUE16)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"QoSGetPolicerMeterParams Error: No ENET Policer profile for Meter Idx:%d\n",MeterIdx);
				return ENET_FAILURE;
			}
			*PmIdValue = ServicePM;
			*TmIdValue = pMeter->tm_Id;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Meter not first time - PolicingProfile:%d, TmIdValue:%d, PmIdValue:%d\n", *PolicingProfileId, *TmIdValue, *PmIdValue);
		}

		if(MEA_API_Get_Policer_ACM_Profile (MEA_UNIT_0,
										pMeter->policer_prof_id,
										0,/*ACM_Mode*/
										pPolicerEntry)
				!= MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"QoSSetAclPolicy Error: MEA_API_Get_Policer_ACM_Profile FAIL for profile:%d\n", pMeter->policer_prof_id);
			return ENET_FAILURE;
		}
	}

	return ENET_SUCCESS;
}

MEA_Bool MeaDrvSetPolicerInDestIpForwarder (ADAP_Uint32 destIpv4, ADAP_Uint16 vpn,
                                            MEA_Policer_prof_t policer_prof_id, MEA_Action_Entry_Data_dbt *pAction)
{
    MEA_SE_Entry_dbt                        entry;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;

    adap_memset(&entry, 0, sizeof(entry));
    adap_memset(&Action_data     , 0 , sizeof(Action_data    ));
    adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
    adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
    adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
   
    Action_editing.num_of_entries = 1;
    Action_editing.ehp_info = &Action_editing_info;

    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=destIpv4;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = vpn;

    if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
    {
        if((entry.data.actionId_valid  == MEA_FALSE) &&
	   (entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 != destIpv4))
	{
	    return MEA_FALSE;
	}
	/* if the action is not send to cpu & a valid action */
	if( (entry.data.actionId != AdapGetRouteActionToCpu()) || (entry.data.actionId != 0) )
	{
	    if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
					entry.data.actionId,
					&Action_data,
					&Action_outPorts,
					&Action_policer,
					&Action_editing) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get action_Id:%d\n", entry.data.actionId);
		return MEA_FALSE;
	    }
	    
    	    if(policer_prof_id != MEA_PLAT_GENERATE_NEW_ID)
            {
                Action_data.policer_prof_id = policer_prof_id;
		Action_data.policer_prof_id_valid = MEA_TRUE;
	    	Action_data.tm_id_valid 	= MEA_TRUE;
	    	Action_data.tm_id           = pAction->tm_id;
	    	Action_data.pm_id_valid 	= MEA_TRUE;
	    	Action_data.pm_id           = 0;
                if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                    			Action_data.policer_prof_id, 0,/* ACM_Mode */
                                    			&Action_policer) != MEA_OK)
            	{
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Policer_ACM_Profile failed\n");
                    return MEA_FALSE;
            	}
    	    }
	     
            if (ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
            				entry.data.actionId,
                                        &Action_data,
                                        &Action_outPorts,
                                        &Action_policer,
                                        &Action_editing) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set action_Id:%d\n", entry.data.actionId);
		return MEA_FALSE;
	    }
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Set policer on action_Id:%d \n", entry.data.actionId);
        pAction->tm_id = Action_data.tm_id;
	    return MEA_TRUE;
	}
    }

    return MEA_FALSE;
}



MEA_Status MeaDrvCreateInnerVlanL2Service(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 InnerVlan)
{
	MEA_Service_Entry_Key_dbt             Service_key;
	MEA_Service_Entry_Data_dbt            Service_data;
	MEA_OutPorts_Entry_dbt                Service_outPorts;
	MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EHP_Info_dbt                      Service_editing_info[8];
	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_AcmMode_t                         acm_mode=0;
	tServiceDb			      *pServiceAlloc=NULL;
	MEA_Port_t 			      phyPort=0;
	MEA_Port_t 			      phyOutputPort;

	MEA_Policer_Entry_dbt          	      Action_policer;
	MEA_Action_Entry_Data_dbt             Action_data;
	MEA_OutPorts_Entry_dbt                Action_outPorts;
	MEA_EHP_Info_dbt                      Action_editing_info;
	MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
	ADAP_Bool			      found=ADAP_TRUE;
	MEA_LxCp_t 			      lxcp=MEA_PLAT_GENERATE_NEW_ID;
	tMeterDb 			      *pMeterDb=NULL;
    	ADAP_Uint32			      ports;

	/* init variables */
	adap_memset(&Service_key,0,sizeof(Service_key));
	adap_memset(&Service_data,0,sizeof(Service_data));
	adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
	adap_memset(&Service_policer,0,sizeof(Service_policer));
	adap_memset(&Service_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
	adap_memset(&Service_editing,0,sizeof(Service_editing));

	adap_memset(&Action_policer,0,sizeof(Action_policer));
	adap_memset(&Action_data,0,sizeof(Action_data));
	adap_memset(&Action_outPorts,0,sizeof(Action_outPorts));
	adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	adap_memset(&Action_editing,0,sizeof(Action_editing));

	if(MeaAdapGetPhyPortFromLogPort(ifIndex,&phyPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}

	if(pBridgeDomain->meterId[ifIndex] != 0)
	{
		pMeterDb=MeaAdapGetMeterParams(pBridgeDomain->meterId[ifIndex]);
	}

	pServiceAlloc = MeaAdapAllocateServiceId();
	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
	}

	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		// configure the actionId
		Action_data.output_ports_valid = MEA_TRUE;
		Action_data.force_color_valid  = MEA_TRUE;
		Action_data.COLOR              = 2;
		Action_data.force_cos_valid    = MEA_FALSE;
		Action_data.COS                = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;

		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_FALSE;
		Action_data.tm_id              = 0;
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE;

		MEA_SET_OUTPORT(&Action_outPorts, phyPort);
		MEA_SET_OUTPORT(&Action_editing_info.output_info, phyPort);

		Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
		Action_editing.ehp_info = &Action_editing_info;

		if(MeaAdapGetNumOfPhyPorts() > ifIndex)
		{
			Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
		}
		else
		{
			Action_data.policer_prof_id = MeaAdapGetDefaultPolicer(1);
		}

		if(pMeterDb != NULL)
		{
			Action_data.policer_prof_id = pMeterDb->policer_prof_id;
		}

		if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
											Action_data.policer_prof_id,
											acm_mode,/* ACM_Mode */
											&Action_policer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler ifIndex:%d\n",ifIndex);
			goto clear;
		}

		Action_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		Action_editing.ehp_info->ehp_data.eth_info.val.all = 0x88a80000 | pBridgeDomain->vlanId;
		Action_editing.ehp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		Action_editing.ehp_info->ehp_data.EditingType        	  = MEA_EDITINGTYPE_APPEND;
		Action_editing.ehp_info->ehp_data.eth_info.stamp_priority            = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_info.stamp_color               = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Action_editing.ehp_info->ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;
		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts,
								   &Action_policer,
								   &Action_editing,
								   &pServiceAlloc->actionId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Create_Action\n");
			goto clear;
		}
	}
	if(MeaAdapGetNumOfPhyPorts() > ifIndex)
	{
		Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(ifIndex);
	}
	else
	{
		Service_data.policer_prof_id = MeaAdapGetDefaultPolicer(1);
	}

	if(pMeterDb != NULL)
	{
		Service_data.policer_prof_id = pMeterDb->policer_prof_id;
	}

	if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
				Service_data.policer_prof_id,
				acm_mode,/* ACM_Mode */
				&Service_policer) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
		goto clear;
	}

	/* Set the service key */
	Service_key.src_port         = phyPort;
	Service_key.pri              = 7;

	Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Stag_Ctag;
	Service_key.net_tag          = pBridgeDomain->vlanId;
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.inner_netTag_from_valid          = MEA_TRUE;
	Service_key.inner_netTag_from_value          = InnerVlan;
	Service_key.inner_netTag_from_value          |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.evc_enable 						= MEA_FALSE;
	Service_key.virtual_port = MEA_FALSE;


	if(pBridgeDomain->learningMode[ifIndex] == ADAP_VLAN_LEARNING_ENABLED)
	{
		/* Build the  service data attributes */
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable = MEA_TRUE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_TRUE;

		if(pBridgeDomain->learn_without_action == ADAP_FALSE)
		{
			Service_data.DSE_learning_actionId   = pServiceAlloc->actionId;
			Service_data.DSE_learning_actionId_valid = MEA_TRUE;
		}
		else
		{
			Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		}
		Service_data.DSE_learning_srcPort	 = phyPort; //TODO, should be cluster!
		Service_data.DSE_learning_srcPort_force = MEA_TRUE;
	}
	else
	{
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.DSE_learning_update_allow_enable = MEA_FALSE;
		Service_data.DSE_learning_actionId   = 0;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.DSE_learning_srcPort	 = 0;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
	}

	/* Set the service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_FALSE;
	Service_data.COLOR              = 0;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;

	Service_data.tmId            = 0; // create tm per port
	Service_data.vpn			= pBridgeDomain->vpn;

	if(pMeterDb != NULL)
	{
		Service_data.tmId = pMeterDb->tm_Id;
	}
        else
	{
		Service_data.tmId = MeaAdapGetDefaultTmId(ifIndex);
	}

	/* Build the Service editing */
	adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));

	Service_editing.ehp_info       = &Service_editing_info[0];
	Service_editing.num_of_entries=0;

	if(getDefaultVlanId() == pBridgeDomain->vlanId)
	{
		for(ports=0;ports<pBridgeDomain->num_of_ports;ports++)
		{
			if(pBridgeDomain->ifIndex[ports] == ifIndex)
			{
				continue;
			}

			if(MeaAdapGetPhyPortFromLogPort(pBridgeDomain->ifIndex[ports],&phyOutputPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",
					pBridgeDomain->ifIndex[ports]);
				goto clear;
			}

			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			MEA_SET_OUTPORT(&Service_outPorts, phyOutputPort);
			MEA_SET_OUTPORT(&Service_editing.ehp_info[Service_editing.num_of_entries].output_info,phyOutputPort);
			Service_editing.ehp_info[Service_editing.num_of_entries].ehp_data.eth_info.val.all           = 0;
			found=ADAP_TRUE;
		}
		if(found == ADAP_TRUE)
		{
			Service_editing.num_of_entries++;
		}
	}

	if(getLxcpId(ifIndex, &lxcp) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",ifIndex);
		goto clear;
	}

	Service_data.LxCp_enable = 1;
	Service_data.LxCp_Id = lxcp;

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
	/* Create new service CPU_Port->Port */
	if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceAlloc->serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by ENET_ADAPTOR_Create_Service serviceId:%d\r\n",pServiceAlloc->serviceId);
		goto clear;
	}

	if(pMeterDb != NULL)
	{
		pMeterDb->tm_Id = Service_data.tmId;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d created\n",pServiceAlloc->serviceId);

	// save configuration
	pServiceAlloc->inPort=phyPort;
	pServiceAlloc->ifIndex=ifIndex;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->pmId = Service_data.pmId;
	pBridgeDomain->pmId[ifIndex] = Service_data.pmId;
	pServiceAlloc->vpnId = Service_data.vpn;

	// add the service to bridge domain
	if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",pServiceAlloc->serviceId);
		goto clear;
	}

	return MEA_OK;
clear:
    if (pServiceAlloc)
    {
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"cleared fail service \r\n");
	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
        pServiceAlloc->valid = MEA_FALSE;
	if(pServiceAlloc->actionId != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pServiceAlloc->actionId) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"deleted actionId:%d\r\n",pServiceAlloc->actionId);
		}
		pServiceAlloc->actionId = MEA_PLAT_GENERATE_NEW_ID;
	}
    }
    return MEA_ERROR;
}


void MeaDrvHandleDefaultRouteService (MEA_Service_t Service_id, tEnetHal_NpNextHopInfo *pRouteEntry)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    tEnetHal_MacAddr srcMac;
    tEnetHal_MacAddr daMac;
    ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    ADAP_Uint32         modeType = 0;
    tBridgeDomain      *pBridgeDomain=NULL;
    tIpDestV4Routing *pIpv4Routing=NULL;
    ADAP_Uint32         idx=0;
    ADAP_Uint32         logPort = 0;
    MEA_Port_t          PhyPort;
        
    /* init variables */
    adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
    adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

    adap_memset(&Service_key,0,sizeof(Service_key));
    adap_memset(&Service_data,0,sizeof(Service_data));
    adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
    adap_memset(&Service_policer,0,sizeof(Service_policer));
    adap_memset(&Service_editing,0,sizeof(Service_editing));
    adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));

    Service_editing.ehp_info       = Service_editing_info;

    if(ENET_ADAPTOR_Get_Service (MEA_UNIT_0,
                                    Service_id,
                                    &Service_key,
                                    &Service_data,
                                    &Service_outPorts,
                                    &Service_policer,
                                    &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"unable to get service %d\n",Service_id);
        return;
    }

    if (Service_data.DSE_forwarding_key_type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
    {
        /* Build the Service editing */
        Service_data.editId = 0; /*create new Id*/
	adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
	adap_memset(&Service_editing.ehp_info[0].output_info, 0 , sizeof(Service_editing.ehp_info[0].output_info));

        pBridgeDomain = Adap_getBridgeDomainByVlan(pRouteEntry->u2VlanId,0);
        for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
        {
            if(pBridgeDomain->ifType[idx] == 0)
            {
                continue;
            }
            if (pRouteEntry->u2VlanId != 0)
            {
                logPort = pBridgeDomain->ifIndex[idx];
            }
            else
            {
                logPort = pRouteEntry->u4IfIndex;
            }
            MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
            if (Service_key.src_port == PhyPort)
            {
		MEA_SET_OUTPORT(&Service_outPorts, INTERNAL_PORT1_NUMBER);
		MEA_SET_OUTPORT(&Service_editing.ehp_info[0].output_info,INTERNAL_PORT1_NUMBER);
                return;
            }
	    MEA_SET_OUTPORT(&Service_outPorts, PhyPort);
	    MEA_SET_OUTPORT(&Service_editing.ehp_info[0].output_info,PhyPort);
        }

        MeaDrvGetFirstGlobalMacAddress(&srcMac);
        if(AdapGetEntryIpTable(&pIpv4Routing,pRouteEntry->u4NextHopGt) == ENET_SUCCESS)
        {
            adap_memcpy(&daMac,&pIpv4Routing->macAddress,sizeof(tEnetHal_MacAddr));
        }
        Service_editing.ehp_info[0].ehp_data.martini_info.EtherType = 0;
        adap_mac_to_arr(&srcMac, Service_editing.ehp_info[0].ehp_data.martini_info.SA.b);
        adap_mac_to_arr(&daMac, Service_editing.ehp_info[0].ehp_data.martini_info.DA.b);

	Service_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Service_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa=1;
	Service_editing.ehp_info[0].ehp_data.eth_info.stamp_color       = MEA_FALSE;
	Service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority    = MEA_FALSE;

        if  ((pRouteEntry->u2VlanId != 0) &&
             (adap_VlanCheckTaggedPort(pRouteEntry->u2VlanId,logPort) == ADAP_TRUE))
        {
            if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
               (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
            {
                AdapGetProviderCoreBridgeMode(&modeType,logPort);
                if (modeType == ADAP_PROVIDER_NETWORK_PORT)
                {
                    EtherType = 0x88a8;
                }
            }        

            if (Service_data.vpn == START_L3_VPN1)
            {
	        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_EXTRACT;
            }
            else
            {
	        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
            }
	    Service_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
	    Service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	    Service_editing.ehp_info[0].ehp_data.eth_info.val.all = ((EtherType << 16) | pRouteEntry->u2VlanId);
        }
        else
        {
	    Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
	    Service_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
	    Service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
        }

        if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
				    Service_id,
				    &Service_data,
				    &Service_outPorts,
				    &Service_policer,
				    &Service_editing) != MEA_OK)
        {
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				    "FAIL: default route failed for service:%d\n",Service_id);
	    return ;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default route set on service:%d\n",Service_id);
    }

    return;
}

void MeaDrvResetDefaultRouteService (MEA_Service_t Service_id)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[8];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
        
    adap_memset(&Service_key,0,sizeof(Service_key));
    adap_memset(&Service_data,0,sizeof(Service_data));
    adap_memset(&Service_outPorts,0,sizeof(Service_outPorts));
    adap_memset(&Service_policer,0,sizeof(Service_policer));
    adap_memset(&Service_editing,0,sizeof(Service_editing));
    adap_memset(&Service_editing_info,0,sizeof(Service_editing_info));

    Service_editing.ehp_info       = Service_editing_info;

    if(ENET_ADAPTOR_Get_Service (MEA_UNIT_0,
                                    Service_id,
                                    &Service_key,
                                    &Service_data,
                                    &Service_outPorts,
                                    &Service_policer,
                                    &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"unable to get service %d\n",Service_id);
        return;
    }

    if (Service_data.DSE_forwarding_key_type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
    {
        /* Build the Service editing */
        Service_data.editId = 0; /*create new Id*/
	adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
	adap_memset(&Service_editing.ehp_info[0].output_info, 0 , sizeof(Service_editing.ehp_info[0].output_info));

	Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_NORMAL;
	MEA_SET_OUTPORT(&Service_outPorts, INTERNAL_PORT1_NUMBER);
	MEA_SET_OUTPORT(&Service_editing.ehp_info[0].output_info,INTERNAL_PORT1_NUMBER);
	Service_editing.ehp_info[0].ehp_data.eth_info.val.all           = 0;

        if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
				    Service_id,
				    &Service_data,
				    &Service_outPorts,
				    &Service_policer,
				    &Service_editing) != MEA_OK)
        {
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				    "FAIL: default route failed for service:%d\n",Service_id);
	    return ;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default route reset on service:%d\n",Service_id);
    }

    return;
}

MEA_Bool MeaDrvSetPolicerInPPPoEForwarder (ADAP_Uint16 sessionId, ADAP_Uint16 vpn,
                                            MEA_Policer_prof_t policer_prof_id)
{
    MEA_SE_Entry_dbt                        entry;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;

    adap_memset(&entry, 0, sizeof(entry));
    adap_memset(&Action_data     , 0 , sizeof(Action_data    ));
    adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
    adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
    adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
   
    Action_editing.num_of_entries = 1;
    Action_editing.ehp_info = &Action_editing_info;

    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
    entry.key.PPPOE_plus_vpn.PPPoE_session_id = sessionId;
    entry.key.PPPOE_plus_vpn.VPN = vpn;

    if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
    {
        if((entry.data.actionId_valid  == MEA_FALSE) &&
	   (entry.key.PPPOE_plus_vpn.PPPoE_session_id != sessionId))
	{
	    return MEA_FALSE;
	}
	/* if the action is not send to cpu & a valid action */
	if( (entry.data.actionId != AdapGetRouteActionToCpu()) || (entry.data.actionId != 0) )
	{
	    if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
					entry.data.actionId,
					&Action_data,
					&Action_outPorts,
					&Action_policer,
					&Action_editing) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get action_Id:%d\n", entry.data.actionId);
		return MEA_FALSE;
	    }
	    
    	    if(policer_prof_id != MEA_PLAT_GENERATE_NEW_ID)
            {
                Action_data.policer_prof_id = policer_prof_id;
		Action_data.policer_prof_id_valid = MEA_TRUE;
	    	Action_data.tm_id_valid 	= MEA_TRUE;
	    	Action_data.tm_id           = 0;
	    	Action_data.pm_id_valid 	= MEA_TRUE;
	    	Action_data.pm_id           = 0;
                if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                    			Action_data.policer_prof_id, 0,/* ACM_Mode */
                                    			&Action_policer) != MEA_OK)
            	{
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Policer_ACM_Profile failed\n");
                    return MEA_FALSE;
            	}
    	    }
	     
            if (ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
            				entry.data.actionId,
                                        &Action_data,
                                        &Action_outPorts,
                                        &Action_policer,
                                        &Action_editing) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set action_Id:%d\n", entry.data.actionId);
		return MEA_FALSE;
	    }
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Set policer on action_Id:%d \n", entry.data.actionId);
	    return MEA_TRUE;
	}
    }

    return MEA_FALSE;
}

/* Set ungeristered traffic flooding */
ADAP_Uint32 EnetHal_UnknownTrafficForward(tEnetHal_VlanId vlanId, eEnetHal_TrfType type, ADAP_Bool notAllowed)
{
    tUpdateServiceParam param;
    tServiceDb *srv = MeaAdapGetFirstServiceVlan(vlanId);

    MEA_OS_memset(&param, 0, sizeof(param));
    param.type = E_UPDATE_MCBCUC_FLOODING;
    param.x.trfParam.type = type;
    param.x.trfParam.notAllowed = notAllowed;
    while (srv)
    {
        if (srv->valid == ADAP_TRUE)
        {
            param.Service = srv->serviceId;
            if (MeaDrvUpdateService(&param) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Error updating service: %d\n", srv->serviceId);
                return ENET_FAILURE;
            }
        }
        srv = MeaAdapGetNextServiceVlan(vlanId, srv->serviceId);
    }
    return ENET_SUCCESS;
}
