/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_queue_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "adap_database.h"
#include "adap_utils.h"



ADAP_Int32 delete_AceVirtualFuncApp(unsigned char 	*AceName)
{
	tAceVirtualFunc *virtualFunc=NULL;
	ADAP_Int32 index=0;
	
	printf("called delete_AceVirtualFuncApp\r\n");
	
	virtualFunc = adap_get_AceVirtualFunc(AceName);
	
	if(virtualFunc == NULL)
	{
		printf("failed to get virtual function name:%s\r\n",AceName);
		return ENET_FAILURE;
	}	
	
	if(virtualFunc->phy_to_cpu_service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, virtualFunc->phy_to_cpu_service_id) != MEA_OK)
		{
			printf("MEA_API_Delete_Service ERROR: Unable to delete the service %d\n",  virtualFunc->phy_to_cpu_service_id);
			//return MEA_ERROR;
		}
		virtualFunc->phy_to_cpu_service_id = MEA_PLAT_GENERATE_NEW_ID;
	}
	if(virtualFunc->cpu_to_phy_service_id != MEA_PLAT_GENERATE_NEW_ID)
	{	
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, virtualFunc->cpu_to_phy_service_id) != MEA_OK)
		{
			printf("MEA_API_Delete_Service ERROR: Unable to delete the service %d\n",  virtualFunc->cpu_to_phy_service_id);
			//return MEA_ERROR;
		}
		virtualFunc->cpu_to_phy_service_id = MEA_PLAT_GENERATE_NEW_ID;
	}	
	for(index=0;index<MEA_ACL_HIERARC_NUM_OF_FLOW;index++)
	{
		if(virtualFunc->phy_to_cpu_FilterId[index] != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,virtualFunc->phy_to_cpu_FilterId[index]) != MEA_OK)
			{
				printf("failed to delete filter id:%d\n",virtualFunc->phy_to_cpu_FilterId[index]);
				//return MEA_ERROR;
			}	
			virtualFunc->phy_to_cpu_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
		}
	}	
	if(virtualFunc->enetPolilcingProfile != MeaAdapGetDefaultPolicer(1) )
	{
		// delete policer
	}
	if(virtualFunc->tClusterId != ENET_PLAT_GENERATE_NEW_ID)
	{
		// delete cluster
		MeaDeleteCluster(virtualFunc->tClusterId);
		adapDeleteCluster(ADAP_CPU_PORT_ID,virtualFunc->tClusterId);
	}
	
	return ENET_SUCCESS;
}

ADAP_Int32 create_AceVirtualFuncApp(unsigned char 	*AceName)
{
	tServiceAdapDb ServiceAdapDb;
	tAdapAclFilterType tAclType;
	tAclFilterParams tAclParams;
	tUpdateServiceParam tParams;
	MEA_EHP_Info_dbt 	acl_ehp_info;
	MEA_ACL_mask_dbt 	arrAclMask[MEA_ACL_HIERARC_NUM_OF_FLOW];
	unsigned int 		MaxNumOfAcl=MEA_ACL_HIERARC_NUM_OF_FLOW;
	unsigned int		acl_index=0;
	unsigned int		index=0;
	unsigned int		acl_profile=0;
	//unsigned int		port_acl_prof=0;
	unsigned int		numOfAclReq=0;
    MEA_Port_t 							  phyOutputPort;
	ENET_QueueId_t 		tClusterId=ENET_PLAT_GENERATE_NEW_ID;
	tAceVirtualFunc *virtualFunc=NULL;
	
	adap_memset(&tAclParams,0,sizeof(tAclFilterParams));
	adap_memset(&tAclType,0,sizeof(tAdapAclFilterType));
	adap_memset(&acl_ehp_info,0,sizeof(MEA_EHP_Info_dbt));
	
	virtualFunc = adap_get_AceVirtualFunc(AceName);
	
	if(virtualFunc == NULL)
	{
		printf("failed to get virtual function name:%s\r\n",AceName);
	}
	
	if( (virtualFunc->tVirtualFunc.AceVnoTag_set == ADAP_TRUE) && (virtualFunc->tVirtualFunc.AceVlan_set == ADAP_TRUE) )
	{
		
		/* check if the ACL configuration */
		if(virtualFunc->tVirtualFunc.AceAppDstMac_set == ADAP_TRUE)
		{
			numOfAclReq++;
			printf("set mac dest mac address\r\n");
			tAclType.bitmask |= ADAP_FILTER_DA_MAC_KEY_TYPE;
			adap_memcpy(&tAclType.da_mac,&virtualFunc->tVirtualFunc.AceAppDstMac,sizeof(tEnetHal_MacAddr));
		}
		if(virtualFunc->tVirtualFunc.AceAppSrcMac_set == ADAP_TRUE)
		{
			numOfAclReq++;
			printf("set mac src mac address\r\n");
			tAclType.bitmask |= ADAP_FILTER_SA_MAC_KEY_TYPE;
			adap_memcpy(&tAclType.src_mac,&virtualFunc->tVirtualFunc.AceAppSrcMac,sizeof(tEnetHal_MacAddr));
		}		
		if(virtualFunc->tVirtualFunc.AceAppSrcIp_set == ADAP_TRUE)
		{
			numOfAclReq++;
			printf("set source ip address\r\n");
			tAclType.bitmask |= ADAP_FILTER_SOURCE_IPV4_KEY_TYPE;
			tAclType.srcIpv4= virtualFunc->tVirtualFunc.AceAppSrcIp;
			tAclType.SourceIpv4Mask =0xFFFFFFFF;
		}
		if(virtualFunc->tVirtualFunc.AceAppSrcIp6_set == ADAP_TRUE)
		{
			tAclType.bitmask |= ADAP_FILTER_SOURCE_IPV6_KEY_TYPE;
			adap_memcpy(&tAclType.srcIpv6,&virtualFunc->tVirtualFunc.AceAppSrcIp6,sizeof(tEnetHal_IPv6Addr));
		}			
		if(virtualFunc->tVirtualFunc.AceAppDstIp_set == ADAP_TRUE)
		{
			numOfAclReq++;
			printf("set dest ip address\r\n");
			tAclType.bitmask |= ADAP_FILTER_DEST_IPV4_KEY_TYPE;
			tAclType.destIpv4= virtualFunc->tVirtualFunc.AceAppDstIp;
			tAclType.destIpv4Mask =0xFFFFFFFF;
		}
		if(virtualFunc->tVirtualFunc.AceAppSrcIp6_set == ADAP_TRUE)
		{
			tAclType.bitmask |= ADAP_FILTER_DEST_IPV6_KEY_TYPE;
			adap_memcpy(&tAclType.dstIpv6,&virtualFunc->tVirtualFunc.AceAppDstIp6,sizeof(tEnetHal_IPv6Addr));
		}
		if(virtualFunc->tVirtualFunc.AceAppIpProto_set == ADAP_TRUE)
		{
			printf("set source ip proto\r\n");
			if(numOfAclReq < 4)
			{
				tAclType.bitmask |= ADAP_FILTER_IP_PROTOCOL_KEY_TYPE;
				tAclType.ipProto = virtualFunc->tVirtualFunc.AceAppIpProto;
			}
			else
			{
				printf("ip protocol not set bacause there is no ACL room\r\n");
			}
		}		
		if(virtualFunc->tVirtualFunc.AceAppSrcL4Port_set == ADAP_TRUE)
		{
			tAclType.bitmask |= ADAP_FILTER_SOURCE_PORT_KEY_TYPE;
			tAclType.SourcePort= virtualFunc->tVirtualFunc.AceAppSrcL4Port;
			printf("set source port :%d\r\n",tAclType.SourcePort);
		}
		if(virtualFunc->tVirtualFunc.AceAppDstL4Port_set == ADAP_TRUE)
		{
			tAclType.bitmask |= ADAP_FILTER_DEST_PORT_KEY_TYPE;
			tAclType.DestPort= virtualFunc->tVirtualFunc.AceAppDstL4Port;
			printf("set dest port :%d\r\n",tAclType.DestPort);
		}		
		
		phyOutputPort=ADAP_CPU_PORT_ID;
		if(adapGetFirstCluster(ADAP_CPU_PORT_ID ,&tClusterId) == ENET_SUCCESS)
		{
			phyOutputPort = tClusterId;
		}	

		/* create cluster if needed */
		if( (virtualFunc->tVirtualFunc.AceShaperState_set == ADAP_TRUE) && (virtualFunc->tVirtualFunc.AceShaperState == 1) )
		{
			printf("MeaCreateCluster\r\n");
			tClusterId=ENET_PLAT_GENERATE_NEW_ID;
			if(MeaCreateCluster("hw_cluster",ADAP_CPU_PORT_ID,&tClusterId) != MEA_OK)
			{
				printf("failed by call MeaCreateCluster ifIndex:%d\r\n",ADAP_CPU_PORT_ID);
				return ENET_FAILURE;
			}
			printf("MeaSetCluster_Shaper\r\n");
			if(MeaSetCluster_Shaper(tClusterId,MEA_TRUE,virtualFunc->tVirtualFunc.AceShaperCir*1000,virtualFunc->tVirtualFunc.AceShaperCbs) != MEA_OK)
			{
				printf("failed by call MeaCreateCluster ifIndex:%d\r\n",ADAP_CPU_PORT_ID);
				return ENET_FAILURE;
			}
			adapCreateCluster(ADAP_CPU_PORT_ID,tClusterId);
			phyOutputPort = tClusterId;
			virtualFunc->tClusterId = tClusterId;
		}		
		// traffic from physical port to CPU
		adap_memset(&ServiceAdapDb,0,sizeof(tServiceAdapDb));
		ServiceAdapDb.Service_id=MEA_PLAT_GENERATE_NEW_ID;
		ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
		ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Stag_Ctag;
		ServiceAdapDb.outervlanId=virtualFunc->tVirtualFunc.AceVnoTag;
		ServiceAdapDb.innervlanId=virtualFunc->tVirtualFunc.AceVlan;
		ServiceAdapDb.editing = ADAP_EHP_EXTRACT_ONE_EDITING_TO_CTAG;
		//ServiceAdapDb.inPort = 104;
		
		if(tAclType.bitmask)
		{
			ServiceAdapDb.num_of_output_ports=1;
			ServiceAdapDb.outports[0] = phyOutputPort;
		}
		ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
		ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);		
		
		virtualFunc->enetPolilcingProfile=MeaAdapGetDefaultPolicer(1);
		if( (virtualFunc->tVirtualFunc.AcePolicerCir_set == ADAP_TRUE) && (virtualFunc->tVirtualFunc.AcePolicerCbs_set == ADAP_TRUE) )/* create policer */
		{
			virtualFunc->enetPolilcingProfile=MEA_PLAT_GENERATE_NEW_ID;
			MeaDrvCreatePolicer (virtualFunc->tVirtualFunc.AcePolicerCir*1000,
						virtualFunc->tVirtualFunc.AcePolicerCbs*1000,
						&virtualFunc->enetPolilcingProfile);
						
			printf("profile ACL id:%d\r\n",virtualFunc->enetPolilcingProfile);
		}		
		
		ServiceAdapDb.policer_id = virtualFunc->enetPolilcingProfile;
		
		MeaAdapGetPhyPortFromLogPort(virtualFunc->tVirtualFunc.AcePort,&ServiceAdapDb.inPort);
		
		if(MeaDrvCreatePointToPointServices(&ServiceAdapDb) != MEA_OK)
		{
			printf("failed to create service fron phy to cpu\r\n");
			return ENET_FAILURE;
		}
		virtualFunc->phy_to_cpu_service_id=ServiceAdapDb.Service_id;
				
		

		acl_profile = AdapAllocAclProfile();
		if(acl_profile == 0xFFFFFFFF){
			printf("ACL - acl_profile %d  Error\r\n",acl_profile);
						return ENET_FAILURE;
		}

		// collect the service filter pasrameter
		adap_memset(&tParams,0,sizeof(tParams));
		tParams.x.filterPararms.ACL_filter_info.filter_mode_OR_AND_type = MaxNumOfAcl-1;
		
		tParams.mode = E_SET_SERVICE_CONFIGURATION;
		tParams.Service = ServiceAdapDb.Service_id;
		tParams.type = E_UPDATE_FILTER_PARAMETERS;
		tParams.x.filterPararms.filter_enable=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof_valid=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof=acl_profile; /* need to allocate acl_profile */
		tParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable=MEA_FALSE;

//		port_acl_prof=0;
//		if(tParams.x.filterPararms.ACL_Prof_valid == ADAP_TRUE){
//			//ACL profile
//			port_acl_prof = tParams.x.filterPararms.ACL_Prof;
//		}

		// check for ACL
		if(adap_SetAclFilterMaskParameters(&tAclType,&arrAclMask[0],&tAclParams.arrFilterKey[0],&MaxNumOfAcl) != MEA_OK)
		{
			printf("ACL - number of ACLs is bigger then MAX\r\n");
			return ENET_FAILURE;
		}
		
		// set the mask parameters according to adap_SetAclFilterParameters
		for(index=0;index<MaxNumOfAcl;index++)
		{
			tParams.x.filterPararms.ACL_filter_info.data_info[index].filter_key_type=arrAclMask[index].filter_key_type;
			tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_0_31 = arrAclMask[index].mask.mask_0_31;
			tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_32_63 = arrAclMask[index].mask.mask_32_63;
			tParams.x.filterPararms.ACL_filter_info.data_info[index].valid = MEA_TRUE;
		}

		// update the service with filter pasrameter
		if(MeaDrvUpdateService (&tParams) != MEA_OK)
		{
			printf("failed to update service\r\n");
			return ENET_FAILURE;
		}		
		MEA_SET_OUTPORT(&tAclParams.outPorts,phyOutputPort);
		MEA_SET_OUTPORT(&acl_ehp_info.output_info,phyOutputPort);			
		// send to packet to CPU
		tAclParams.action_type = MEA_FILTER_ACTION_TO_ACTION;			
		
		
		/* ACL editing */
		acl_ehp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		acl_ehp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		acl_ehp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		acl_ehp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		acl_ehp_info.ehp_data.eth_info.val.all=0;
		
		acl_ehp_info.ehp_data.eth_info.command= MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
		acl_ehp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		
		
		tAclParams.EHP_Entry.num_of_entries=1;
		tAclParams.EHP_Entry.ehp_info = &acl_ehp_info;
		
		
		/* need to create metering */
		tAclParams.enetPolilcingProfile=virtualFunc->enetPolilcingProfile;
		tAclParams.num_of_filters=MaxNumOfAcl;
		printf("number of ACLs %d\r\n",tAclParams.num_of_filters);
		//tAclParams.enetPolilcingProfile = MeaAdapGetDefaultPolicer(ServiceAdapDb.inPort);
		if(adap_SetAclFilterConfiguration(&tAclParams,acl_index,acl_profile) != MEA_OK)
		{
			printf("failed to create filters\r\n");
			return ENET_FAILURE;
		}
		printf("succeeded to create ACL\r\n");
		
		for(index=0;index<MEA_ACL_HIERARC_NUM_OF_FLOW;index++)
		{
			virtualFunc->phy_to_cpu_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
			virtualFunc->cpu_to_phy_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
		}
		
		for(index=0;index<MaxNumOfAcl;index++)
		{
			virtualFunc->phy_to_cpu_FilterId[index] = tAclParams.FilterId[index];
		}
		virtualFunc->acl_profile_virt.acl_profile = acl_profile;
		virtualFunc->acl_profile_virt.valid       =  MEA_TRUE ;
		/******************************************************************************/
		
		// create service from CPU to phy
		adap_memset(&ServiceAdapDb,0,sizeof(tServiceAdapDb));
		ServiceAdapDb.Service_id=MEA_PLAT_GENERATE_NEW_ID;
		ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
		ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Ctag;
		ServiceAdapDb.outervlanId=virtualFunc->tVirtualFunc.AceVlan;
		ServiceAdapDb.editing = ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING;
		ServiceAdapDb.inPort = ADAP_CPU_PORT_ID;
		ServiceAdapDb.editOutervlanId=virtualFunc->tVirtualFunc.AceVnoTag;
		ServiceAdapDb.num_of_output_ports=1;
		ServiceAdapDb.outports[0] = ADAP_CPU_PORT_ID;
		ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
		ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);		
		MeaAdapGetPhyPortFromLogPort(virtualFunc->tVirtualFunc.AcePort,&ServiceAdapDb.outports[0]);		
		
		if(MeaDrvCreatePointToPointServices(&ServiceAdapDb) != MEA_OK)
		{
			printf("failed to create cpu to phy service\r\n");
			return ENET_FAILURE;
		}		
		virtualFunc->cpu_to_phy_service_id=ServiceAdapDb.Service_id;
	}
	
	return ENET_SUCCESS;
}

ADAP_Int32 delete_AceAdminCtrl(unsigned int accesslist_priorityid)
{
	tAceCtrlAdmin *pAceCtrlAdmin=NULL;
	ADAP_Int32 index=0;
	tServiceDb	*pService=NULL;
	
	printf("called delete_AceVirtualFuncApp\r\n");
	
	pAceCtrlAdmin = adap_get_AceAdminCtrl(accesslist_priorityid);
	if(pAceCtrlAdmin == NULL)
	{
		printf("failed to get admin control function priority:%d\r\n",accesslist_priorityid);
		return ENET_FAILURE;
	}	

	
	if(pAceCtrlAdmin->phy_to_cpu_service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		// check reference count
		pService = MeaAdapGetServiceId(pAceCtrlAdmin->phy_to_cpu_service_id);
		
		if(pService == NULL)
		{
			printf("pService is serviceId:%d NULL\n", pAceCtrlAdmin->phy_to_cpu_service_id);
			return MEA_ERROR;
		}
		printf("ref count:%d\n", pService->refCount);
		pService->refCount--;
		AdapfreeAclProfile(pService->acl_profile,0);
		if(pService->refCount == 0)
		{
			printf("delete serviceid:%d\n", pAceCtrlAdmin->phy_to_cpu_service_id);
			if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, pAceCtrlAdmin->phy_to_cpu_service_id) != MEA_OK)
			{
				printf("MEA_API_Delete_Service ERROR: Unable to delete the service %d\n",  pAceCtrlAdmin->phy_to_cpu_service_id);
				//return MEA_ERROR;
			}
			MeaAdapFreeServiceId(pAceCtrlAdmin->phy_to_cpu_service_id);
		}
		pAceCtrlAdmin->phy_to_cpu_service_id = MEA_PLAT_GENERATE_NEW_ID;
	}
	pAceCtrlAdmin->cpu_to_phy_service_id = MEA_PLAT_GENERATE_NEW_ID;

	for(index=0;index<MEA_ACL_HIERARC_NUM_OF_FLOW;index++)
	{
		if(pAceCtrlAdmin->phy_to_cpu_FilterId[index] != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0,pAceCtrlAdmin->phy_to_cpu_FilterId[index]) != MEA_OK)
			{
				printf("failed to delete filter id:%d\n",pAceCtrlAdmin->phy_to_cpu_FilterId[index]);
				//return MEA_ERROR;
			}	
			pAceCtrlAdmin->phy_to_cpu_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
		}
	}	
	if(pAceCtrlAdmin->enetPolilcingProfile != MeaAdapGetDefaultPolicer(1) )
	{
		// delete policer
	}
		
	return ENET_SUCCESS;	
}

ADAP_Int32 create_AceAdminCtrl(unsigned int accesslist_priorityid)
{
	tAceCtrlAdmin *pAceCtrlAdmin=NULL;
	tServiceAdapDb ServiceAdapDb;
	tAdapAclFilterType tAclType;
	tAclFilterParams tAclParams;
	tUpdateServiceParam tParams;
	MEA_EHP_Info_dbt 	acl_ehp_info;
	MEA_ACL_mask_dbt 	arrAclMask[MEA_ACL_HIERARC_NUM_OF_FLOW];
	unsigned int 		MaxNumOfAcl=MEA_ACL_HIERARC_NUM_OF_FLOW;
	unsigned int		acl_index=0;
	unsigned int		index=0;
	unsigned int		acl_profile=0;
	//unsigned int        port_acl_prof=0;
	unsigned int		numOfAclReq=0;
    MEA_Port_t 							  phyOutputPort;
	ENET_QueueId_t 		tClusterId;	
	tEnetHal_MacAddr	SrcMac;
	
	
	adap_memset(&tAclParams,0,sizeof(tAclFilterParams));
	adap_memset(&tAclType,0,sizeof(tAdapAclFilterType));
	adap_memset(&acl_ehp_info,0,sizeof(MEA_EHP_Info_dbt));
	
	printf("In function name:%s\r\n",__FUNCTION__);

	pAceCtrlAdmin = adap_get_AceAdminCtrl(accesslist_priorityid);
	if(pAceCtrlAdmin == NULL)
	{
		printf("failed to get admin control function priority:%d\r\n",accesslist_priorityid);
		return ENET_FAILURE;
	}
	pAceCtrlAdmin->cpu_to_phy_service_id = 0;

	/* check if the ACL configuration */
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlDstMac_set == ADAP_TRUE)
	{
		numOfAclReq++;
		printf("set mac dest mac address\r\n");
		tAclType.bitmask |= ADAP_FILTER_DA_MAC_KEY_TYPE;
		adap_memcpy(&tAclType.da_mac,&pAceCtrlAdmin->tCtrlAdmin.AceCntlDstMac,sizeof(tEnetHal_MacAddr));
	}
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcMac_set == ADAP_TRUE)
	{
		numOfAclReq++;
		printf("set mac src mac address\r\n");
		tAclType.bitmask |= ADAP_FILTER_SA_MAC_KEY_TYPE;
		adap_memcpy(&tAclType.src_mac,&pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcMac,sizeof(tEnetHal_MacAddr));
	}		
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcIp_set == ADAP_TRUE)
	{
		numOfAclReq++;
		printf("set source ip address\r\n");
		tAclType.bitmask |= ADAP_FILTER_SOURCE_IPV4_KEY_TYPE;
		tAclType.srcIpv4= pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcIp;
		tAclType.SourceIpv4Mask =0xFFFFFFFF;
	}
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcIp6_set == ADAP_TRUE)
	{
		tAclType.bitmask |= ADAP_FILTER_SOURCE_IPV6_KEY_TYPE;
		adap_memcpy(&tAclType.srcIpv6,&pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcIp6,sizeof(tEnetHal_IPv6Addr));
	}
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlDstIp_set == ADAP_TRUE)
	{
		numOfAclReq++;
		printf("set dest ip address\r\n");
		tAclType.bitmask |= ADAP_FILTER_DEST_IPV4_KEY_TYPE;
		tAclType.destIpv4= pAceCtrlAdmin->tCtrlAdmin.AceCntlDstIp;
		tAclType.destIpv4Mask =0xFFFFFFFF;
	}
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlDstIp6_set == ADAP_TRUE)
	{
		tAclType.bitmask |= ADAP_FILTER_DEST_IPV6_KEY_TYPE;
		adap_memcpy(&tAclType.dstIpv6,&pAceCtrlAdmin->tCtrlAdmin.AceCntlDstIp6,sizeof(tEnetHal_IPv6Addr));
	}	
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlIpProto_set == ADAP_TRUE)
	{
		printf("set source ip proto\r\n");
		if(numOfAclReq < 4)
		{
			tAclType.bitmask |= ADAP_FILTER_IP_PROTOCOL_KEY_TYPE;
			tAclType.ipProto = pAceCtrlAdmin->tCtrlAdmin.AceCntlIpProto;
		}
		else
		{
			printf("ip protocol not set bacause there is no ACL room\r\n");
		}
	}		
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcPort_set == ADAP_TRUE)
	{
		tAclType.bitmask |= ADAP_FILTER_SOURCE_PORT_KEY_TYPE;
		tAclType.SourcePort= pAceCtrlAdmin->tCtrlAdmin.AceCntlSrcPort;
		printf("set source port :%d\r\n",tAclType.SourcePort);
	}
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlDstPort_set == ADAP_TRUE)
	{
		tAclType.bitmask |= ADAP_FILTER_DEST_PORT_KEY_TYPE;
		tAclType.DestPort= pAceCtrlAdmin->tCtrlAdmin.AceCntlDstPort;
		printf("set dest port :%d\r\n",tAclType.DestPort);
	}		
	
	phyOutputPort=ADAP_CPU_PORT_ID;
	if(adapGetFirstCluster(ADAP_CPU_PORT_ID ,&tClusterId) == ENET_SUCCESS)
	{
		phyOutputPort = tClusterId;
	}		
	// traffic from physical port to CPU
	adap_memset(&ServiceAdapDb,0,sizeof(tServiceAdapDb));
	ServiceAdapDb.Service_id=MEA_PLAT_GENERATE_NEW_ID;
	ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntrlVlanTag_set == ADAP_TRUE)
	{
		ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Ctag; //MEA_PARSING_L2_KEY_Untagged
		ServiceAdapDb.outervlanId=pAceCtrlAdmin->tCtrlAdmin.AceCntrlVlanTag;
	}
	else
	{
		ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Untagged;
	}
	ServiceAdapDb.editing = ADAP_EHP_EXTRACT_ONE_EDITING_TO_UNTAGGED;
	//ServiceAdapDb.inPort = 104;
	if(tAclType.bitmask == 0)
	{
		ServiceAdapDb.num_of_output_ports=1;
		ServiceAdapDb.outports[0] = phyOutputPort;
	}
	ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
	ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);		
	
	pAceCtrlAdmin->enetPolilcingProfile=MeaAdapGetDefaultPolicer(1);
	if( (pAceCtrlAdmin->tCtrlAdmin.AcePolicerCir_set == ADAP_TRUE) && (pAceCtrlAdmin->tCtrlAdmin.AcePolicerCbr_set == ADAP_TRUE) )/* create policer */
	{
		pAceCtrlAdmin->enetPolilcingProfile=MEA_PLAT_GENERATE_NEW_ID;
		MeaDrvCreatePolicer (pAceCtrlAdmin->tCtrlAdmin.AcePolicerCir*1000,
					pAceCtrlAdmin->tCtrlAdmin.AcePolicerCbr*1000,
					&pAceCtrlAdmin->enetPolilcingProfile);
					
		printf("profile ACL id:%d\r\n",pAceCtrlAdmin->enetPolilcingProfile);
	}		
	
	ServiceAdapDb.policer_id = pAceCtrlAdmin->enetPolilcingProfile;
	
	MeaAdapGetPhyPortFromLogPort(pAceCtrlAdmin->tCtrlAdmin.AceCntrlPort,&ServiceAdapDb.inPort);
	
	if(MeaDrvCreatePointToPointServices(&ServiceAdapDb) != MEA_OK)
	{
		printf("%s  failed to create service from phy to cpu\r\n",__FUNCTION__);
		return ENET_FAILURE;
	}
	pAceCtrlAdmin->phy_to_cpu_service_id=ServiceAdapDb.Service_id;
	
	if(ServiceAdapDb.isAllreadyExist == ADAP_FALSE)
	{
		acl_profile = AdapAllocAclProfile();
		if(acl_profile == 0xFFFFFFFF)
		{
			printf("ACL - acl_profile %d  Error\r\n",acl_profile);
			return ENET_FAILURE;
		}

		// collect the service filter pasrameter
		adap_memset(&tParams,0,sizeof(tParams));
		tParams.x.filterPararms.ACL_filter_info.filter_mode_OR_AND_type = MaxNumOfAcl-1;
		
		tParams.mode = E_SET_SERVICE_CONFIGURATION;
		tParams.Service = ServiceAdapDb.Service_id;
		tParams.type = E_UPDATE_FILTER_PARAMETERS;
		tParams.x.filterPararms.filter_enable=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof_valid=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof=acl_profile; /* need to allocate acl_profile */
		tParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable=MEA_FALSE;

//		port_acl_prof=0;
//		if(tParams.x.filterPararms.ACL_Prof_valid == ADAP_TRUE){
//			//ACL profile
//			port_acl_prof = tParams.x.filterPararms.ACL_Prof;
//		}

	// check for ACL
		if(adap_SetAclFilterMaskParameters(&tAclType,&arrAclMask[0],&tAclParams.arrFilterKey[0],&MaxNumOfAcl) != MEA_OK)
		{
			printf("ACL - number of ACLs is bigger then MAX\r\n");
			return ENET_FAILURE;
		}
	
	// set the mask parameters according to adap_SetAclFilterParameters
	for(index=0;index<MaxNumOfAcl;index++)
	{
		tParams.x.filterPararms.ACL_filter_info.data_info[index].filter_key_type=arrAclMask[index].filter_key_type;
		tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_0_31 = arrAclMask[index].mask.mask_0_31;
		tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_32_63 = arrAclMask[index].mask.mask_32_63;
		tParams.x.filterPararms.ACL_filter_info.data_info[index].valid = MEA_TRUE;
	}

	// update the service with filter pasrameter
	if(MeaDrvUpdateService (&tParams) != MEA_OK)
	{
		printf("failed to update service\r\n");
		return ENET_FAILURE;
	}		
	
	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlAction == CHA_DROP)
	{
		if(pAceCtrlAdmin->tCtrlAdmin.AceEgressPort_set == ADAP_TRUE)
		{
			MeaAdapGetPhyPortFromLogPort(pAceCtrlAdmin->tCtrlAdmin.AceEgressPort,&phyOutputPort);
			printf("output port:%d ",phyOutputPort);
			adap_memset(&tParams,0,sizeof(tParams));
			tParams.Service = ServiceAdapDb.Service_id;
			tParams.type = E_UPDATE_OUTPUT_PARAMETERS;
			tParams.x.editing.outports[0] = phyOutputPort;
			tParams.x.editing.num_of_output_ports=1;
			tParams.x.editing.outercommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			tParams.x.editing.innercommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
			if(MeaDrvUpdateService (&tParams) != MEA_OK)
			{
				printf("failed to update service ");
				return ENET_FAILURE;
			}
			}
		}
	}
	else
	{
		if(adap_SetAclFilterMaskParameters(&tAclType,&arrAclMask[0],&tAclParams.arrFilterKey[0],&MaxNumOfAcl) != MEA_OK)
		{
			printf("ACL - number of ACLs is bigger then MAX\r\n");
			return ENET_FAILURE;
		}		
		// need to get the get_service
		adap_memset(&tParams,0,sizeof(tParams));
		tParams.mode = E_GET_SERVICE_CONFIGURATION;
		tParams.Service = ServiceAdapDb.Service_id;
		tParams.type = E_UPDATE_FILTER_PARAMETERS;

		if(MeaDrvUpdateService (&tParams) != MEA_OK)
		{
			printf("failed to update service\r\n");
			return ENET_FAILURE;
		}
		
		acl_profile = tParams.x.filterPararms.ACL_Prof;
		IncRefCountAclProfile(acl_profile);
	}
	
	// send to packet to CPU
	tAclParams.action_type = MEA_FILTER_ACTION_TO_ACTION;			

	
//	MEA_SET_OUTPORT(&tAclParams.outPorts,phyOutputPort);
//	MEA_SET_OUTPORT(&acl_ehp_info.output_info,phyOutputPort);		
	
	/* ACL editing */
	acl_ehp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	acl_ehp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	acl_ehp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	acl_ehp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	acl_ehp_info.ehp_data.eth_info.val.all=0;
	
	acl_ehp_info.ehp_data.eth_info.command= MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	acl_ehp_info.ehp_data.EditingType = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	

	if(pAceCtrlAdmin->tCtrlAdmin.AceCntlAction == CHA_DROP)
	{
		printf("discard\r\n");
		tAclParams.action_type = MEA_FILTER_ACTION_DISCARD;
	}
	else 
	{
		if(pAceCtrlAdmin->tCtrlAdmin.AceCntlAction == CHA_REDIRECT)
		{
			printf("redirect:%d\r\n",pAceCtrlAdmin->tCtrlAdmin.AceEgressPort_set);
			if(pAceCtrlAdmin->tCtrlAdmin.AceEgressPort_set == ADAP_TRUE)
			{
				MeaAdapGetPhyPortFromLogPort(pAceCtrlAdmin->tCtrlAdmin.AceEgressPort,&phyOutputPort);			
				printf("output port:%d\r\n",phyOutputPort);
			}
			if(pAceCtrlAdmin->tCtrlAdmin.AceEgressDstMac_set == ADAP_TRUE) // router
			{
				MeaDrvHwGetGlobalUCMac(&SrcMac);
				acl_ehp_info.ehp_data.martini_info.EtherType = 0;
				adap_mac_to_arr(&SrcMac, acl_ehp_info.ehp_data.martini_info.SA.b);
				adap_mac_to_arr(&pAceCtrlAdmin->tCtrlAdmin.AceEgressDstMac, acl_ehp_info.ehp_data.martini_info.DA.b);
				
				acl_ehp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
				acl_ehp_info.ehp_data.LmCounterId_info.Command_dasa=1;
				acl_ehp_info.ehp_data.eth_info.stamp_color       = MEA_FALSE;
				acl_ehp_info.ehp_data.eth_info.stamp_priority    = MEA_FALSE;					
			}					
		}
		MEA_SET_OUTPORT(&tAclParams.outPorts,phyOutputPort);
		MEA_SET_OUTPORT(&acl_ehp_info.output_info,phyOutputPort);
		tAclParams.EHP_Entry.num_of_entries=1;
	}
	tAclParams.EHP_Entry.ehp_info = &acl_ehp_info;

	
	
	/* need to create metering */
	tAclParams.enetPolilcingProfile=pAceCtrlAdmin->enetPolilcingProfile;
	tAclParams.num_of_filters=MaxNumOfAcl;
	printf(" %s number of ACLs %d cluster:%d\r\n",__FUNCTION__,tAclParams.num_of_filters,phyOutputPort);
	//tAclParams.enetPolilcingProfile = MeaAdapGetDefaultPolicer(ServiceAdapDb.inPort);
	if(adap_SetAclFilterConfiguration(&tAclParams,acl_index,acl_profile) != MEA_OK)
	{
		printf("%s failed to create filters\r\n",__FUNCTION__);
		return ENET_FAILURE;
	}
	printf("succeeded to create ACL\r\n");
	
	for(index=0;index<MEA_ACL_HIERARC_NUM_OF_FLOW;index++)
	{
		pAceCtrlAdmin->phy_to_cpu_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
		pAceCtrlAdmin->cpu_to_phy_FilterId[index] = MEA_PLAT_GENERATE_NEW_ID;
	}
	
	
	for(index=0;index<MaxNumOfAcl;index++)
	{
		pAceCtrlAdmin->phy_to_cpu_FilterId[index] = tAclParams.FilterId[index];
	}
	pAceCtrlAdmin->acl_profile_ctrl.acl_profile = acl_profile;
	
	return ENET_SUCCESS;
	
}
