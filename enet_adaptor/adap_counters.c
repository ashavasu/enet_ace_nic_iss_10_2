/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_deviceInfo_drv.h"
#include "unistd.h"
#include "pthread.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_enetConvert.h"
#include "adap_counters.h"

MEA_Status MeaAdapGetPortCounter (ADAP_Uint32  ifIndex,tEnetAdapRmonCounter *pCounter)
{
	MEA_Port_t enetPort;
	MEA_Counters_RMON_dbt entry;

	if(MeaAdapGetPhyPortFromLogPort(ifIndex,&enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}
	if(ENET_ADAPTOR_Collect_Counters_RMON(MEA_UNIT_0,enetPort,&entry) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get port statistics from ifIndex:%d\r\n",ifIndex);
		return MEA_ERROR;
	}
	pCounter->rx_packets = MEA_OS_htonll64(entry.Rx_Pkts.val);      /* Number of received packets. */
	pCounter->tx_packets = MEA_OS_htonll64(entry.Tx_Pkts.val);      /* Number of transmitted packets. */
	pCounter->rx_bytes   = MEA_OS_htonll64(entry.Rx_Bytes.val);        /* Number of received bytes. */
	pCounter->tx_bytes   = MEA_OS_htonll64(entry.Tx_Bytes.val);        /* Number of transmitted bytes. */
	pCounter->rx_dropped = MEA_OS_htonll64(entry.Rx_Mac_Drop_Pkts.val);      /* Number of packets dropped by RX. */
	pCounter->tx_dropped = MEA_OS_htonll64(entry.Tx_CRC_Err.val);      /* Number of packets dropped by TX. */
	pCounter->rx_errors  = MEA_OS_htonll64(entry.Rx_Error_Pkts.val);       /* Number of receive errors.  This is a
                                 super-set of receive errors and should be
                                 great than or equal to the sum of all
                                 rx_*_err values. */
	pCounter->tx_errors     = MEA_OS_htonll64(entry.Tx_Overrun_Pkts.val);       /* Number of transmit errors.  This is a
                                                                                   super-set of transmit errors. */
	pCounter->rx_frame_err  = MEA_OS_htonll64(entry.Rx_Error_Pkts.val);    /* Number of frame alignment errors. */
	pCounter->rx_over_err   = MEA_OS_htonll64(entry.Rx_Oversize_Bytes.val);     /* Number of packets with RX overrun. */
	pCounter->rx_crc_err    = MEA_OS_htonll64(entry.Rx_CRC_Err.val);      /* Number of CRC errors. */
	pCounter->collisions = 0;      /* Number of collisions. */

	return MEA_OK;
}



MEA_Status MeaAdap_get_counter_records(ADAP_Uint32 counter_id_base, ADAP_Uint32 num_counters, flow_stats_record *records_arr)
{
	MEA_Uint32 startBlock;
	MEA_Uint32 block ;
	MEA_Uint32 to_block ;
	MEA_Counters_PM_dbt     entry;
	MEA_PmId_t      pmId ,from_pmId,to_pmId;
	MEA_PmId_t      upPmId;
	MEA_Uint32     index=0;
	MEA_Bool                  pmId_exist;

	if(records_arr == NULL){
		return MEA_ERROR;
	}

	if(counter_id_base >= MEA_MAX_NUM_OF_PM_ID){
		return MEA_ERROR;
	}

	if((counter_id_base + num_counters) >= MEA_MAX_NUM_OF_PM_ID){
		return MEA_ERROR;
	}


	/**
	 * check if the PM work Block mode or regular
	 *
	**/
	if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_UNIT_0, MEA_DB_PM_TYPE) == MEA_TRUE){
		startBlock = counter_id_base / MEA_COUNTERS_PM_BLK_READ;
		to_block=  (counter_id_base+num_counters)/MEA_COUNTERS_PM_BLK_READ;


		index=0;
		upPmId =  counter_id_base + num_counters -1;

		for(block=startBlock ; block <= to_block ; block++)
		{
			//** get the block

			 MEA_API_Collect_Counters_PMs_Block(MEA_UNIT_0, block);

			if(block == startBlock){
				from_pmId=counter_id_base;
				if(    (upPmId) > ((startBlock*MEA_COUNTERS_PM_BLK_READ) + MEA_COUNTERS_PM_BLK_READ ))
				{
				   to_pmId = (startBlock*MEA_COUNTERS_PM_BLK_READ)+ MEA_COUNTERS_PM_BLK_READ;
				}else{
					to_pmId = upPmId ;
				}

			}else{
				from_pmId=(block*MEA_COUNTERS_PM_BLK_READ);
				if( ( (block*MEA_COUNTERS_PM_BLK_READ) +  MEA_COUNTERS_PM_BLK_READ ) <  upPmId ){
				   to_pmId = (block*MEA_COUNTERS_PM_BLK_READ) + (MEA_COUNTERS_PM_BLK_READ);
				}else{
					to_pmId = upPmId;
				}

			}

			//pmId from up to block
			 for(pmId = from_pmId; pmId <=to_pmId ;pmId++ ){
				 adap_memset(&entry, 0, sizeof(entry));
				 MEA_API_IsExist_PmId(MEA_UNIT_0,
							          pmId,
							          &pmId_exist,
							          NULL);

				records_arr[index].byte_count =  0 ;
				records_arr[index].packet_count = 0;

				if(pmId_exist){
					 MEA_API_Get_Counters_PM(MEA_UNIT_0,pmId,&entry);
					 records_arr[index].byte_count   = ( entry.green_fwd_bytes.val +  entry.yellow_fwd_bytes.val);
					 records_arr[index].packet_count = (entry.green_fwd_pkts.val + entry.yellow_fwd_pkts.val);
                 }
				 index++;


			 }


		}
	}else{
		index=0;
		from_pmId = counter_id_base;
		to_pmId  =  counter_id_base+num_counters-1;

		 for(pmId = from_pmId; pmId <=to_pmId ;pmId++ ){

			 adap_memset(&entry, 0, sizeof(entry));
			 pmId_exist=MEA_FALSE;
			 MEA_API_IsExist_PmId(MEA_UNIT_0,
			                       pmId,
			                       &pmId_exist,
			                       NULL);

			 records_arr[index].byte_count =  0 ;
			 records_arr[index].packet_count = 0;

			 if(pmId_exist){
				 MEA_API_Get_Counters_PM(MEA_UNIT_0,pmId,&entry);
				 records_arr[index].byte_count   =  (entry.green_fwd_bytes.val +  entry.yellow_fwd_bytes.val);
				 records_arr[index].packet_count = (entry.green_fwd_pkts.val + entry.yellow_fwd_pkts.val);
			 }
			 index++;



		 }



	}






	return MEA_OK;
}
