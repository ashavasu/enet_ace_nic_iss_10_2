#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_brgnp.h"
#include "adap_service_config.h"
#include "mea_drv_common.h"
#include "adap_enetConvert.h"
#include "EnetHal_L2_Api.h"

ADAP_Int8 EnetHal_FsBrgSetAgingTime (ADAP_Int32 i4AgingTime)
{
	// If i4AgingTime is 0 aging will be disabled
	#ifdef USE_ENET_MUTEX
	    ENET_FWD_Lock();
	#endif
	    if (MEA_API_Set_SE_Aging(MEA_UNIT_0, i4AgingTime, (i4AgingTime > 0) ? MEA_TRUE : MEA_FALSE) != MEA_OK)
	    {
	        printf("Unable to set Aging time %d\n", i4AgingTime);
	#ifdef USE_ENET_MUTEX
	        ENET_FWD_Unlock();
	#endif
	        return ENET_FAILURE;
	    }
	#ifdef USE_ENET_MUTEX
	    ENET_FWD_Unlock();
	#endif
	    adap_SetAgingTime(i4AgingTime);

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgSetTempAgingTime (ADAP_Int32 i4AgingTime)
{
#ifdef USE_ENET_MUTEX
    ENET_FWD_Lock();
#endif
    if (MEA_API_Set_SE_Aging(MEA_UNIT_0, i4AgingTime, MEA_TRUE) != MEA_OK)
    {
        printf("Unable to set Aging time %d\n", i4AgingTime);
#ifdef USE_ENET_MUTEX
        ENET_FWD_Unlock();
#endif
        return ENET_FAILURE;
    }
#ifdef USE_ENET_MUTEX
    ENET_FWD_Unlock();
#endif
    //adap_SetTempAgingTime(i4AgingTime);

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgFindFdbMacEntry (tEnetHal_MacAddr *macAddr, ADAP_Uint32 *pu4IfIndex)
{
   MEA_Port_t                     port;
   MEA_SE_Entry_dbt               entry;
   ADAP_Uint32					  IssPortId;

	adap_memset(&entry, 0, sizeof(entry));
	*pu4IfIndex = 0;

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(macAddr, entry.key.mac_plus_vpn.MAC.b);

    if ( ENET_ADAPTOR_Get_SE_Entry (MEA_UNIT_0,
							  &entry) != MEA_OK)  {
		 printf("EnetHal_FsBrgFindFdbMacEntry: Error while getting SE entry\n");
		 return ENET_FAILURE;
	}

	for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
	{
		if (((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] & (1 << (port%32)))
		{
			if(MeaAdapGetLogPortFromPhyPort(&IssPortId, port) == ENET_SUCCESS)
			{
				printf("issPort:%d, enetPortId:%d\n",IssPortId, port);
				*pu4IfIndex = IssPortId;
				printf("FsBrgFindFdbMacEntry - entry is found for port:%d\n", port);
			}
		}
	}

	return ENET_SUCCESS;
}


ADAP_Int8 EnetHal_FsBrgGetFdbEntryFirst (tEnetHal_MacAddr *macAddr)
{
	MEA_SE_Entry_dbt      entry;
	MEA_Bool    	      found;

    adap_memset(&entry,0,sizeof(MEA_SE_Entry_dbt));
#ifdef USE_ENET_MUTEX
	ENET_FWD_Lock();
#endif
    if (ENET_ADAPTOR_GetFirst_SE_Entry(MEA_UNIT_0,
								 &entry,
								 &found) != MEA_OK)
	{
	    printf("ENET_ADAPTOR_GetFirst_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
		ENET_FWD_Unlock();
#endif
	    return ENET_FAILURE;
	}

	if(found)
	{
		adap_mac_from_arr(macAddr, entry.key.mac.MAC.b);
		printf("EnetHal_FsBrgGetFdbEntryFirst - FDB entry is found!\n");
	}
	else
	{
		printf("EnetHal_FsBrgGetFdbEntryFirst - FDB table is empty! \n");
	}
#ifdef USE_ENET_MUTEX
	ENET_FWD_Unlock();
#endif

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgGetFdbNextEntry(tEnetHal_MacAddr *nextMacAddr)
{
	MEA_SE_Entry_dbt      entry;
	MEA_Bool    	      found;

    adap_memset(&entry,0,sizeof(MEA_SE_Entry_dbt));

#ifdef USE_ENET_MUTEX
	ENET_FWD_Lock();
#endif
    if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,
								 &entry,
								 &found) != MEA_OK)
	{
	    printf("ENET_ADAPTOR_GetNext_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
		ENET_FWD_Unlock();
#endif
	    return ENET_FAILURE;
	}

	if(found)
	{
		adap_mac_from_arr(nextMacAddr, entry.key.mac.MAC.b);
		printf("EnetHal_FsBrgGetFdbNextEntry - FDB entry is found!\n");
	}
	else
	{
		printf("EnetHal_FsBrgGetFdbNextEntry - next entry doesn't exist \n");
	}
#ifdef USE_ENET_MUTEX
	ENET_FWD_Unlock();
#endif

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgFlushPort (ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32			CurrentAgingTime;

	CurrentAgingTime = adap_GetAgingTime();

	EnetHal_FsBrgSetAgingTime(3);

	sleep(3);

	EnetHal_FsBrgSetAgingTime(CurrentAgingTime);

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgAddMacEntry (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *macAddr)
{
	   MEA_SE_Entry_dbt              entry;
	   MEA_Port_t                    EnetPortId;
	   tEnetHal_VlanId	VlanId;
	   ADAP_Uint16	masterPort=0;
	   ADAP_Bool portInVlan;
	   ADAP_Uint16   Vpn = 0;

	   	masterPort = u4IfIndex;

		if(adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
		{
			masterPort = adap_GetLagMasterPort(u4IfIndex);
		}

	   for(VlanId = 0; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId++)
	   {
		   	portInVlan = adap_CheckVlanForPort(masterPort, VlanId, &Vpn);
	        if(portInVlan == ADAP_TRUE)
	        {
	            adap_memset(&entry, 0, sizeof(entry));

	            entry.key.type  = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	            adap_mac_to_arr(macAddr, entry.key.mac_plus_vpn.MAC.b);
	            entry.key.mac_plus_vpn.VPN = Vpn;

	            /* Convert u4IfIndex to the ENET portId */
				if(MeaAdapGetPhyPortFromLogPort(masterPort, &EnetPortId) == ENET_SUCCESS)
				{
					printf("issPort:%d, enetPortId:%d\n",masterPort, EnetPortId);
				}
				else
				{
					printf("Unable to convert u4IfIndex:%d to the ENET portId\n", masterPort);
					return ENET_FAILURE;
	            }

	           entry.data.valid = MEA_TRUE;
	           entry.data.aging = 3;
	           entry.data.static_flag = MEA_TRUE;
	           entry.data.OutPorts_valid=MEA_TRUE;
	           MEA_SET_OUTPORT(&entry.data.OutPorts, EnetPortId);

	           if (ENET_ADAPTOR_Create_SE_Entry(MEA_UNIT_0, &entry) != MEA_OK)
	           {
	               printf("Error while create the SE entry\n");
	               return ENET_FAILURE;
	           }

	           printf("FsBrgAddMacEntry - entry  for ISS port:%d VPN: %d is added!\n", u4IfIndex, Vpn);
	        }
	   }

	   return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgDelMacEntry (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *macAddr)
{
	MEA_SE_Entry_dbt    entry;
	ADAP_Uint16	VlanId;
	ADAP_Uint16 Vpn;
	ADAP_Uint16	masterPort=0;
	ADAP_Bool portInVlan;

  	masterPort = u4IfIndex;

	if(adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
		masterPort = adap_GetLagMasterPort(u4IfIndex);
	}

   for(VlanId = 0; VlanId < ADAP_NUM_OF_SUPPORTED_VLANS; VlanId++)
   {
		portInVlan = adap_CheckVlanForPort(masterPort, VlanId, &Vpn);

		if(portInVlan == ADAP_TRUE)
		{
			adap_memset(&entry, 0, sizeof(entry));
			adap_mac_to_arr(macAddr, entry.key.mac_plus_vpn.MAC.b);
			entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
			entry.key.mac_plus_vpn.VPN = Vpn;

			if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,
										&(entry.key)) != MEA_OK)
			{
			   printf("Error while deleting the SE entry\n");
			   return ENET_FAILURE;
			}

			printf("FsBrgDelMacEntry - entry  for ISS port:%d VPN: %d is deleted!\n", u4IfIndex, Vpn);
		}

   }

   return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_FsBrgGetLearnedEntryDisc (ADAP_Uint32 *pu4Val)
{
	MEA_Counters_Forwarder_dbt   MEA_Stats;

	*pu4Val = 0;
 	if (MEA_API_Get_Counters_Forwarder(MEA_UNIT_0, &MEA_Stats) != MEA_OK)
	{
		return (ENET_FAILURE);
	}

	*pu4Val = MEA_Stats.learn_failed.val;

    return ENET_SUCCESS;
}
