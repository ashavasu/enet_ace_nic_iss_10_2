/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef __ADAP_UTILS_H__
#define __ADAP_UTILS_H__

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stddef.h>
#include "adap_types.h"

#define ADAP_CONCAT(a,b) a##b
#define ADAP_CASSERT_CONCAT(a,b) ADAP_CONCAT(a,b)
#define ADAP_CASSERT(p,m) \
	;enum {ADAP_CASSERT_CONCAT(cassert, __COUNTER__) = 1/(int)(!!(p))}

/* This is a shorter version of kernel's container_of macro,
 *  without the type checking. */
#define container_of(ptr, type, member) \
	((type *)((char*)(ptr) - __builtin_offsetof(type, member)))

#define adap_safe_free(ptr) \
	if (ptr != NULL) { \
		free(ptr); \
	}

#define adap_malloc malloc

#define adap_calloc calloc

#define adap_memset memset

#define adap_memcpy memcpy

#define adap_memcmp memcmp

#define adap_assert assert


ADAP_Uint64 identity_func(ADAP_Uint64 value);


#endif /* __ADAP_UTILS_H__ */
