/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_igmp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "EnetHal_L3_Api.h"

ADAP_Int32 EnetHal_FsMiIgsHwEnableIgmpSnooping (ADAP_Uint32 u4Instance)
{
    ADAP_UNUSED_PARAM (u4Instance);
    ADAP_Uint32 ifIndex=0;
    ADAP_Uint8 ret = ENET_SUCCESS;
    MEA_LxCp_t lxcp_Id=0;

    for(ifIndex=1; ifIndex < MeaAdapGetNumOfPhyPorts(); ifIndex++)
    {
        ret = getLxcpId(ifIndex,&lxcp_Id);
        if(ret == ENET_SUCCESS)
        {
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_IGMPoV4,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
        }
    }

    AdapSetMcastMode (ADAP_MCAST_MAC_FORWARDING_MODE);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiIgsHwDisableIgmpSnooping (ADAP_Uint32 u4Instance)
{
    ADAP_UNUSED_PARAM (u4Instance);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiIgsHwEnableIpIgmpSnooping (ADAP_Uint32 u4Instance)
{
    ADAP_UNUSED_PARAM (u4Instance);
    ADAP_Uint32 ifIndex=0;
    ADAP_Uint8 ret = ENET_SUCCESS;
    MEA_LxCp_t lxcp_Id=0;

    for(ifIndex=1; ifIndex < MeaAdapGetNumOfPhyPorts(); ifIndex++)
    {
        ret = getLxcpId(ifIndex,&lxcp_Id);
        if(ret == ENET_SUCCESS)
        {
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_IGMPoV4,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
        }
    }

    AdapSetMcastMode (ADAP_MCAST_IP_FORWARDING_MODE);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiIgsHwDisableIpIgmpSnooping (ADAP_Uint32 u4Instance)
{
    ADAP_UNUSED_PARAM (u4Instance);

    AdapSetMcastMode (ADAP_MCAST_MAC_FORWARDING_MODE);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsIgmpHwEnableIgmp (void)
{
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

        for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		getLxcpId(idx,&lxcp_Id);
		MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_IGMPoV4,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

    	return ENET_SUCCESS;
}

