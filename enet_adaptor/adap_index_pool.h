/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ADAP_INDEX_POOL_H__
#define __ADAP_INDEX_POOL_H__

#include "adap_types.h"
#include "EnetHal_status.h"

typedef struct adap_index_pool adap_index_pool;

struct adap_index_pool_params {
	ADAP_Uint32 capacity;
	ADAP_Bool thread_safe;
};


EnetHal_Status_t adap_index_pool_init(adap_index_pool** pool, struct adap_index_pool_params *params);

EnetHal_Status_t adap_index_pool_deinit(adap_index_pool* pool);

EnetHal_Status_t adap_index_pool_alloc(adap_index_pool* pool, ADAP_Uint32 *index);

EnetHal_Status_t adap_index_pool_free(adap_index_pool* pool, ADAP_Uint32 index);

ADAP_Uint32 adap_index_pool_size(adap_index_pool* pool);



#endif /* __ADAP_INDEX_POOL_H__ */





