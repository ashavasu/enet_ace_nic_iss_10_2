
ENET_ADAP_BASE=..
ENET_INCLUDE_DIR=../../Ethernity/SW/drivers/pub
ENET_PLATFORM_INCLUDE_DIR=../../Ethernity/SW/platform/h


#export NIC_PLATFORM="no"


#BASE_DIR=/opt/petalinux_2014/linux-i386
#BUILDS_COMPILER_PATH=$(BASE_DIR)/arm-xilinx-linux-gnueabi
#CROSS_COMPILE=bin/arm-xilinx-linux-gnueabi-

CC                           = ${BUILDS_COMPILER_PATH}/${CROSS_COMPILE}gcc
CPP                          = ${BUILDS_COMPILER_PATH}/${CROSS_COMPILE}cpp
AR                           = ${BUILDS_COMPILER_PATH}/${CROSS_COMPILE}ar
LD                           = ${BUILDS_COMPILER_PATH}/${CROSS_COMPILE}ld

OBJCOPY                      = ${BUILDS_COMPILER_PATH}/$(CROSS_COMPILE)objcopy
AS                           = ${BUILDS_COMPILER_PATH}/${CROSS_COMPILE}as
NM                           = ${BUILDS_COMPILER_PATH}/$(CROSS_COMPILE)nm
