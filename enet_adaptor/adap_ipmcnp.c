
/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "EnetHal_L3_Api.h"

ADAP_Int32 EnetHal_FsPimNpInitHw ()
{
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

        for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		getLxcpId(idx,&lxcp_Id);
		MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_IN_PIM,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

	AdapSetMcastMode (ADAP_MCAST_IP_FORWARDING_MODE);

    	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsNpIpv4VlanMcInit (ADAP_Uint32 vlanid)
{
       	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx=0;

       	pBridgeDomain = Adap_getBridgeDomainByVlan(vlanid,0);

        if (pBridgeDomain == NULL)
        {
            return ENET_FAILURE;
        }
	for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
	{
		if(pBridgeDomain->ifType[idx] != 0)
		{
			if(MeaDrvCreateMulticastService(pBridgeDomain->ifIndex[idx],pBridgeDomain,
							pBridgeDomain->ifType[idx]) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateMulticastService ifIndex:%d\r\n",
							pBridgeDomain->ifIndex[idx]);
				return ENET_FAILURE;
			}
		}
	}

       	return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_FsNpIpv4McAddRouteEntry (tEnetHal_McRouteInfo *pMcRouteInfo, 
					    tEnetHal_McDownStreamIf *pDownStreamIf)
{
	MEA_Action_t                	action_Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_OutPorts_Entry_dbt          Action_outPorts;
	MEA_EHP_Info_dbt                      Action_editing_info[8];
	MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;	
	MEA_Port_t                      enetOutPort;
	ADAP_Uint32			index = 0;
	ADAP_Uint32			idx = 0;
	ADAP_Uint16			vpn = 0;
	tEnetHal_MacAddr	srcMac;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsNpIpv4McAddRouteEntry Source %x DestGrp %x Vlan %d\n",
		pMcRouteInfo->SrcIpAddr,pMcRouteInfo->GrpAddr,pMcRouteInfo->VlanId);

	adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&Action_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*8);
    adap_memset(&Action_editing,0,sizeof(Action_editing));

	adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
	MeaDrvGetFirstGlobalMacAddress(&srcMac);

	Action_editing.num_of_entries = pMcRouteInfo->NoOfDownStreamIf;    /* Number of the entries in the Editing table */
	Action_editing.ehp_info = &Action_editing_info[0];

	for(index = 0; index < pMcRouteInfo->NoOfDownStreamIf; index++)
	{
		adap_mac_to_arr(&srcMac, Action_editing.ehp_info[index].ehp_data.martini_info.SA.b);

		for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
		{
			if (pDownStreamIf->McFwdPortList[idx] != 0)
			{
				if (adap_VlanCheckTaggedPort(pDownStreamIf->VlanId,pDownStreamIf->McFwdPortList[idx]) == ADAP_TRUE)
				{
					Action_editing.ehp_info[index].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
					Action_editing.ehp_info[index].ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
					Action_editing.ehp_info[index].ehp_data.eth_info.val.all      = 
							(MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE << 16) | pDownStreamIf->VlanId;
				}
				else
				{
					Action_editing.ehp_info[index].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
					Action_editing.ehp_info[index].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
				}
				MeaAdapGetPhyPortFromLogPort(pDownStreamIf->McFwdPortList[idx],&enetOutPort);
				MEA_SET_OUTPORT(&Action_outPorts,enetOutPort);
				MEA_SET_OUTPORT(&Action_editing.ehp_info[index].output_info,enetOutPort);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"vlan %d OutPort:%d\n", pDownStreamIf->VlanId, enetOutPort);
			}
		}
		Action_editing.ehp_info[index].ehp_data.eth_info.stamp_color       = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Action_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		pDownStreamIf++;
	}

        if(FsMiHwCreateRouterAction(&action_Id,&Action_editing_info[0],Action_editing.num_of_entries,
                                    &Action_outPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
        {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create multicast router action\r\n");
        }

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d created\n", action_Id);

	vpn = adap_GetVpnByVlan (pMcRouteInfo->VlanId);
	if (MeaDrvHwCreateDestIpSaDaForwarder(action_Id, MEA_TRUE,pMcRouteInfo->SrcIpAddr,
						pMcRouteInfo->GrpAddr,vpn,&Action_outPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create SA DA entry in forwarder\r\n");
	    	return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsNpIpv4McDeleteRouteEntry (tEnetHal_McRouteInfo *pMcRouteInfo) 
{
	ADAP_Uint16			vpn = 0;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsNpIpv4McDeleteRouteEntry Source %x DestGrp %x Vlan %d\n",
		pMcRouteInfo->SrcIpAddr,pMcRouteInfo->GrpAddr,pMcRouteInfo->VlanId);

	vpn = adap_GetVpnByVlan (pMcRouteInfo->VlanId);

	if (MeaDrvHwDeleteDestIpSaDaForwarder(pMcRouteInfo->SrcIpAddr,
						pMcRouteInfo->GrpAddr,vpn) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete SA DA entry in forwarder\r\n");
	    	return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}
