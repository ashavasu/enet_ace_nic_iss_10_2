/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*ENET Init*/
#ifndef _ENETINIT_C
#define _ENETINIT_C

#include "EnetHal_status.h"
#include "adap_taskManager.h"
#include "enetadaptor.h"
#include "adap_types.h"
#include "mea_api.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_lag_config.h"
#include "adap_acl.h"
#include "adap_queue_config.h"
#include "adap_ecfmminp.h"
//#include "../future/inc/lr.h"

//void EnetAdaptorInit(void);

static ADAP_Int32 MeaAdapServiceManagement(void)
{
	tServiceAdapDb tCpuService;
	adap_memset(&tCpuService,0,sizeof(tCpuService));
	// create services from CPU to output port
	

	tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Untagged;
	tCpuService.inPort=ADAP_MANAGEMENT_PORT;
	tCpuService.outports[0] = ADAP_CPU_PORT_ID;
	tCpuService.num_of_output_ports=1;
	tCpuService.pmId=1;
	tCpuService.tmId=0; // create new TMid
	tCpuService.policer_id=MeaAdapGetDefaultPolicer(1);
	tCpuService.outervlanId = 100;
	tCpuService.innervlanId = 100;
	if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
		return ENET_FAILURE;
	}

	tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Untagged;
	tCpuService.inPort=ADAP_CPU_PORT_ID;
	tCpuService.outports[0] = ADAP_MANAGEMENT_PORT;
	tCpuService.num_of_output_ports=1;
	tCpuService.pmId=1;
	tCpuService.tmId=0; // create new TMid
	tCpuService.policer_id=MeaAdapGetDefaultPolicer(1);
	tCpuService.outervlanId = 100;
	tCpuService.innervlanId = 100;
	if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
		return ENET_FAILURE;
	}

	MeaAdapSetEgressPort (ADAP_MANAGEMENT_PORT,ADAP_TRUE,ADAP_TRUE);
	MeaAdapSetEgressPort (ADAP_CPU_PORT_ID,ADAP_TRUE,ADAP_TRUE);

	MeaAdapSetIngressPort (ADAP_MANAGEMENT_PORT,ADAP_TRUE,ADAP_TRUE);
	MeaAdapSetIngressPort (ADAP_CPU_PORT_ID,ADAP_TRUE,ADAP_FALSE);
	return ENET_SUCCESS;
}

static ADAP_Int32 MeaAdapServiceFromCpu(void)
{
	tServiceAdapDb tCpuService;
	ADAP_Uint32 idx=0;
	ADAP_Uint32 logPort=0;

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Ctag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}

		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=0; // create new TMid
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];
		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
		MeaAdapSetDefaultTm(tCpuService.tmId,idx);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Ctag_Ctag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=MeaAdapGetDefaultTm(idx);
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];

		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Stag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}

		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=0; // create new TMid
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];
		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
		MeaAdapSetDefaultTm(tCpuService.tmId,idx);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Stag_Ctag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}

		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=0; // create new TMid
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];
		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
		MeaAdapSetDefaultTm(tCpuService.tmId,idx);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Stag_Stag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}

		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=0; // create new TMid
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];
		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
		MeaAdapSetDefaultTm(tCpuService.tmId,idx);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		adap_memset(&tCpuService,0,sizeof(tCpuService));
		// create services from CPU to output port

		tCpuService.L2_protocol_type=MEA_PARSING_L2_KEY_Stag_Stag_Ctag;

		if(MeaAdapGetPhyPortFromLogPort(0,&tCpuService.inPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&tCpuService.outports[0]) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		tCpuService.num_of_output_ports=1;
		tCpuService.pmId=1;
		tCpuService.tmId=MeaAdapGetDefaultTm(idx);
		tCpuService.policer_id=MeaAdapGetDefaultPolicer(idx);
		tCpuService.outervlanId = tCpuService.outports[0];
		tCpuService.innervlanId = tCpuService.outports[0];

		if(MeaDrvCreateServicesFromCpu (&tCpuService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateServicesFromCpu\r\n");
			return ENET_FAILURE;
		}
		MeaAdapSetDefaultTm(tCpuService.tmId,idx);
	}

	return ENET_SUCCESS;
}

static EnetHal_Status_t MeaAdapCreateDefaultFlowPolicerPerPort(ADAP_Uint32 ifIndex)
{
    MEA_Uint16 profId;
    mea_Ingress_Flow_policer_dbt Flow_policer_entry;
    MEA_Uint32 policer_type;
    MEA_Bool IsExist;

//    if (MEA_API_Get_Ingress_flow_policer(MEA_UNIT_0, vpls_ingressPolicerFlow, &Flow_policer_entry) != MEA_OK)
//     {
//         return MEA_ERROR;
//     }



    adap_memset(&Flow_policer_entry ,0,sizeof(mea_Ingress_Flow_policer_dbt));

    profId = MEA_PLAT_GENERATE_NEW_ID;

    if(MEA_API_Create_Ingress_flow_policerId(MEA_UNIT_0, &profId) !=MEA_OK){
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Create_Ingress_flow_policer - profId:%d\r\n", profId);
        return ENETHAL_STATUS_MEA_ERROR;
    }

    if (MEA_API_Ingress_flow_policer_Prof_IsExist(MEA_UNIT_0, profId, &IsExist) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Ingress_flow_policer_Prof_IsExist - profId:%d\r\n", profId);
        return ENETHAL_STATUS_MEA_ERROR;
    }

    for(policer_type = MEA_INGRESS_PORT_POLICER_TYPE_MC; policer_type < MEA_INGRESS_PORT_POLICER_TYPE_TOTAL; policer_type++)
    {
        Flow_policer_entry.policer_vec[policer_type].tmId = 0;
        Flow_policer_entry.policer_vec[policer_type].tmId_enable =  MEA_PORT_RATE_MEETRING_ENABLE_DEF_VAL;
        Flow_policer_entry.policer_vec[policer_type].sla_params.CIR = 128000;
        Flow_policer_entry.policer_vec[policer_type].sla_params.EIR = 0;
        Flow_policer_entry.policer_vec[policer_type].sla_params.CBS = 32000;
        Flow_policer_entry.policer_vec[policer_type].sla_params.EBS = 0;
        Flow_policer_entry.policer_vec[policer_type].sla_params.cup = 0;
        Flow_policer_entry.policer_vec[policer_type].sla_params.color_aware = 0;
        Flow_policer_entry.policer_vec[policer_type].sla_params.comp = 0;
    }

    if (MEA_API_Set_Ingress_flow_policer(MEA_UNIT_0, profId, &Flow_policer_entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Ingress_flow_policer - profId:%d\r\n", profId);
        return ENETHAL_STATUS_MEA_ERROR;
    }

    adap_SetFlowPolicerProfile(ifIndex, profId);

    return ENETHAL_STATUS_SUCCESS;
}

static ADAP_Int32 MeaAdapCreatePortPolicer(void)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 logPort=0;
	MEA_Port_t phyPort;
	MEA_Policer_prof_t policer=MEA_PLAT_GENERATE_NEW_ID;

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		if(MeaAdapGetLogPortFromIndex(&logPort,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get MeaAdapGetLogPortFromIndex\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapGetPhyPortFromLogPort(logPort,&phyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		policer=MEA_PLAT_GENERATE_NEW_ID;
		if(MeaDrvCreateDefaultPolicer (&policer,phyPort) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get MeaDrvCreateDefaultPolicer\r\n");
			return ENET_FAILURE;
		}
		if(MeaAdapSetDefaultPolicer(policer,idx) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get MeaAdapSetDefaultPolicer\r\n");
			return ENET_FAILURE;
		}

		if (MeaAdapCreateDefaultFlowPolicerPerPort(idx) != ENETHAL_STATUS_SUCCESS)
	    {
	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaAdapCreateDefaultFlowPolicerPerPort failed for ifIndex:%d\r\n", idx);
	        return ENET_FAILURE;
	    }

		MeaAdapSetDefaultTmId(0,idx);
	}
	return ENET_SUCCESS;
}

ADAP_Int32 enet_init_enet_hw(void)
{
	char    *readEnv=NULL;
	ADAP_Uint32	ifIndex=0;
	MEA_LxCp_t lxcp=MEA_PLAT_GENERATE_NEW_ID;
	ADAP_Int32 	ret = ENET_SUCCESS;

	// create default policer per interface
	if(MeaAdapCreatePortPolicer() != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapCreatePortPolicer\r\n");
		return ENET_FAILURE;
	}

	// create service for management only for ZYNC
    readEnv = getenv("MEA_CREATE_MANAGEMENT_PORT");
    if (readEnv)
	{
		fprintf(stdout,"MEA_CREATE_MANAGEMENT_PORT :%s\r\n",readEnv);
		if(MeaAdapServiceManagement() != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapServiceManagement\r\n");
			return ENET_FAILURE;
		}
    }
    if( (adapGetBoardSpecific() & SW_ENET_NIC_BOARD) == SW_ENET_NIC_BOARD)
    {
    	// create services from CPU to output port
    	if(MeaAdapServiceFromCpu() != ENET_SUCCESS)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapServiceFromCpu\r\n");
    		return ENET_FAILURE;
    	}
	adap_FsMiHwSetPortShaper(127,100000000,64);
    }

	// create lxcp handle
	for(ifIndex=1;ifIndex<MeaAdapGetNumOfPhyPorts();ifIndex++)
	{
		lxcp=MEA_PLAT_GENERATE_NEW_ID;
		if(MeaDrvCreateLxcp(&lxcp) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create lxcpr\n");
			ret = ENET_FAILURE;
			break;
		}
		ret = setLxcpId(ifIndex,lxcp);
		if(ret != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to save lxcp in database\r\n");
			break;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"LxCp:%d is set for ifIndex:%d\n", lxcp, ifIndex);
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE1,"exit\r\n");
	return ENET_SUCCESS;
}

ADAP_Uint32 enet_hw_per_port_config(void)
{
	ADAP_Uint32 index=0;
	//ADAP_Uint32 i=0;
	ADAP_Uint32 ifIndex=0;
	MEA_LxCp_t lxcp=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t enetPort;
	MEA_Uint16 lagId;
	tDbaseHwAggEntry *pAggEntry=NULL;
	ADAP_Bool lag_created=ADAP_FALSE;
	ENET_QueueId_t  queueId=ENET_PLAT_GENERATE_NEW_ID;

	for(index=0;index<MeaAdapGetNumOfOnfPhyPorts();index++)
	{
		if(adap_get_onf_log_port(index,&ifIndex) == ENET_SUCCESS)
		{
			if(getLxcpId(ifIndex,&lxcp) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp index\r\n");
			}
			if(lxcp == MEA_PLAT_GENERATE_NEW_ID)
			{
				if(MeaDrvCreateLxcp(&lxcp) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create lxcpr\n");
					return ENET_FAILURE;
				}
				if(setLxcpId(ifIndex,lxcp) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to save lxcp in database\r\n");
					return ENET_FAILURE;
				}
			}

			adap_get_onf_phy_port(ifIndex,&enetPort);

			if(MeaAdapSetIngressPort (enetPort,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",ifIndex);
				return ENET_FAILURE;
			}
			if(MeaAdapSetEgressPort (enetPort,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",ifIndex);
				return ENET_FAILURE;
			}
		}
	}

	for(ifIndex=1;ifIndex<MeaAdapGetNumOfPhyPorts();ifIndex++)
	{
		if (MeaAdapCreateDefaultFlowPolicerPerPort(ifIndex) != ENETHAL_STATUS_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaAdapCreateDefaultFlowPolicerPerPort failed for ifIndex:%d\r\n", ifIndex);
			return ENET_FAILURE;
		}
	}

	// enable internal ports for vpls
	if(MeaAdapSetIngressPort (INTERNAL_PORT1_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",INTERNAL_PORT1_NUMBER);
		return ENET_FAILURE;
	}
	if(MeaAdapSetEgressPort (INTERNAL_PORT1_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",INTERNAL_PORT1_NUMBER);
		return ENET_FAILURE;
	}

	if(MeaAdapSetIngressPort (INTERNAL_PORT2_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",INTERNAL_PORT2_NUMBER);
		return ENET_FAILURE;
	}
	if(MeaAdapSetEgressPort (INTERNAL_PORT2_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",INTERNAL_PORT2_NUMBER);
		return ENET_FAILURE;
	}


	if(MeaAdapSetIngressPort (INTERNAL_PORT3_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",INTERNAL_PORT3_NUMBER);
		return ENET_FAILURE;
	}
	if(MeaAdapSetEgressPort (INTERNAL_PORT3_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",INTERNAL_PORT3_NUMBER);
		return ENET_FAILURE;
	}

	if(MeaAdapSetIngressPort (INTERNAL_PORT4_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",INTERNAL_PORT4_NUMBER);
		return ENET_FAILURE;
	}
	if(MeaAdapSetEgressPort (INTERNAL_PORT4_NUMBER,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",INTERNAL_PORT4_NUMBER);
		return ENET_FAILURE;
	}

	MeaAdapSetEgressPort (ADAP_CPU_PORT_ID,ADAP_TRUE,ADAP_TRUE);
	MeaAdapSetIngressPort (ADAP_CPU_PORT_ID,ADAP_TRUE,ADAP_FALSE);


	// create cluster to CPU
	queueId=80;
	if(MeaCreateCluster("cpu_cluster",ADAP_CPU_PORT_ID,&queueId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaCreateCluster ifIndex:%d\r\n",ADAP_CPU_PORT_ID);
		return ENET_FAILURE;
	}

	printf("cpu_cluster = %d\r\n",queueId);

	adapCreateCluster(ADAP_CPU_PORT_ID,queueId);
#if 0
	// create 8 queues per port
	if( (adapGetBoardSpecific() & SW_ENET_NIC_BOARD ) == SW_ENET_NIC_BOARD)
	{
		for(index=1;index<MeaAdapGetNumOfOnfPhyPorts();index++)
		{
			MeaAdapGetPhyPortFromLogPort(index,&enetPort);
			for(i=0;i<4;i++)
			{
				queueId = ENET_PLAT_GENERATE_NEW_ID;
				if(MeaCreateCluster("hw_cluster",enetPort,&queueId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaCreateCluster ifIndex:%d\r\n",enetPort);
					return ENET_FAILURE;
				}
				printf("created queueId:%d on port:%d index:%d\r\n",queueId,enetPort,index);
				adapCreateCluster(enetPort,queueId);
			}
		}
	}
#endif
	if( (adapGetBoardSpecific() & SW_ENET_NIC_LAG_MODE) == SW_ENET_NIC_LAG_MODE)
	{
		for(index=0;index<MeaAdapGetNumOfOnfPhyPorts();index++)
		{
			if(adap_get_onf_log_port(index,&ifIndex) == ENET_SUCCESS)
			{
				if(adap_is_part_lag_port(ifIndex) == ADAP_TRUE)
				{
					adap_get_onf_phy_port(ifIndex,&enetPort);
					if(lag_created == ADAP_FALSE)
					{
						Adap_SetLagGlobalLag(MEA_TRUE);
						Adap_CreateLagEntry(enetPort,&lagId);
						printf("created create lagid:%d\r\n",lagId);
						pAggEntry = AdapAllocateAggrDbByHdLagId(lagId);
						pAggEntry->portbitmask |= (1 << ifIndex);
						pAggEntry->lagVirtualPort = START_OF_LAG_VIRTUAL_PORTS;
						Adap_SetIngressMasterLag (enetPort,enetPort);
						pAggEntry->masterPort=enetPort;
						lag_created=ADAP_TRUE;
					}
					else
					{
						Adap_SetIngressMasterLag (enetPort,pAggEntry->masterPort);
						Adap_AddLagEntry(enetPort,lagId);
						printf("add port:%d to lagId:%d\r\n",enetPort,lagId);
						pAggEntry->portbitmask |= (1 << ifIndex);
					}
				}
			}
		}
	}
	return ENET_SUCCESS;
}

/***********************************************/
/*  this function init the ADAP related to HW  */
/***********************************************/
ADAP_Int32 enet_init_adaptor_database(void)
{
//	ADAP_Uint32 idx=0;
	EnetAdaptorInit(SW_ENET_NIC_BOARD);

	adapSetBoardSpecific(SW_ENET_NIC_BOARD); 

	read_pring_log_level();

	enet_init_enet_hw();

	return ENET_SUCCESS;
}

ADAP_Int32 enet_external_init_adaptor_database(ADAP_Int32 software_key)
{
	EnetAdaptorInit(software_key);

	read_pring_log_level();

	return ENET_SUCCESS;
}

#endif
