/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef ADAP_ENETNP_H
#define ADAP_ENETNP_H

#include "EnetHal_L2_Api.h"

void adap_HwRestartSystem (void);
void init_timer_thread(void);
ADAP_Int32 adap_HwAddMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo);
ADAP_Int32 adap_HwRemoveMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo);

#endif
