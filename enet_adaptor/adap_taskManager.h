#ifndef __ADAP_TASK_MANAGER_H__
#define __ADAP_TASK_MANAGER_H__

#include "EnetHal_status.h"

typedef pthread_t adap_task_id;
typedef void* (*adap_task_func)(void* arg);

/** 
 * @brief Initialize the Task Manager
 * @return EnetHal_Status_t
 */
EnetHal_Status_t adap_task_manager_init(void);


/** 
 * @brief De-initialize the Task Manager. All running tasks will be cancelled.
 * @return EnetHal_Status_t.
 */
EnetHal_Status_t adap_task_manager_deinit(void);


/** 
 * @brief Run a task. The Task will be added to Task DB
 * @param[in] func - the function to be executed.
 * @param[in] context - context to be sent to the task function as a parameter.
 * @param[out] id - the ID of the task.
 * @return EnetHal_Status_t.
 */
EnetHal_Status_t adap_task_manager_run(adap_task_func func, void* context, adap_task_id* id);


/** 
 * @brief Joins the task with <id>. The Task will be removed from Task DB
 * @param[in] id - the ID of the task (generated with <adap_task_manager_run>).
 * @return EnetHal_Status_t.
 */
EnetHal_Status_t adap_task_manager_join(adap_task_id id);


/** 
 * @brief Cancels the task with <id>. The Task will be removed from Task DB
 * @param[in] id - the ID of the task (generated with <adap_task_manager_run>).
 * @return EnetHal_Status_t.
 */
EnetHal_Status_t adap_task_manager_cancel(adap_task_id id);


#endif /*__ADAP_TASK_MANAGER_H__ */
