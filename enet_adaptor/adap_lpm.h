#ifndef _ADAP_LPM_H
#define _ADAP_LPM_H

#include "adap_types.h"
#include "adap_rbtree.h"
#include "adap_trie.h"
#include "EnetHal_L2_Api.h"

#define LPM_SUCCESS               0
#define LPM_FAILURE               1

struct arp_entry {
	/* RB-tree element */
	adap_rbtree_element elem;

	/* key */
	ADAP_Uint32         DstIp;
	ADAP_Uint32         u4VrId;

    /* result */
	ADAP_Uint32         u4IfIndex;
    ADAP_Uint16         u2vlanId;
    tEnetHal_MacAddr      MacAddr;
};

struct nd_entry {
	/* RB-tree element */
	adap_rbtree_element elem;

	/* key */
    tEnetHal_IPv6Addr   DstIp6;
    ADAP_Uint32         u4VrId;

    /* result */
    ADAP_Uint32         u4IfIndex;
    ADAP_Uint16         u2vlanId;
    tEnetHal_MacAddr      MacAddr;
};

/* TODO - in the future need to introduce alternative paths with different metrics' */
struct ipv4_next_hop_entry {
	adap_trie_element elem;
	ADAP_Uint32 u4NextHopGt;
	ADAP_Uint32 u4IfIndex;
	ADAP_Uint16 u2VlanId;
};

struct ipv6_next_hop_entry {
	adap_trie_element elem;
	tEnetHal_IPv6Addr Ip6NextHop;
	ADAP_Uint32 u4IfIndex;
	ADAP_Uint16 u2VlanId;
};

struct route_entry {
	/* RB-tree element */
	adap_rbtree_element elem;

	/* key */
	ADAP_Uint32         u4DestAddr;
	ADAP_Uint32         u4VrId;

    /* result */
	tEnetHal_MacAddr      DestMacAddr;
	ADAP_Uint32         u4NextHopGt;
	ADAP_Uint32         u4IfIndex;
	ADAP_Uint16         u2VlanId;
	ADAP_Uint16         u2Port;
};


ADAP_Uint32
Enet_Lpm_Arp_Add (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IfIdx,
		ADAP_Uint16 u2VlanId, tEnetHal_MacAddr *pMacAddr);

ADAP_Uint32
Enet_Lpm_Arp_Delete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr);

struct arp_entry *
Enet_Lpm_Arp_Get (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr);

ADAP_Uint32
Enet_Lpm_ND_Add(ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr, ADAP_Uint32 u4IfIdx,
		ADAP_Uint16 u2VlanId, tEnetHal_MacAddr *pMacAddr);

ADAP_Uint32
Enet_Lpm_ND_Delete(ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr);

struct nd_entry *
Enet_Lpm_ND_Get(ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr);

ADAP_Uint32
Enet_Lpm_Ipv6Route_Add(ADAP_Uint8 vrid, tEnetHal_IPv6Addr *prefix, ADAP_Uint8 prefix_len,
		tEnetHal_IPv6Addr *next_hop, ADAP_Uint32 ifindex, ADAP_Uint16 vlan_id);

ADAP_Uint32
Enet_Lpm_Ipv6Route_Delete(ADAP_Uint8 vrid, tEnetHal_IPv6Addr *prefix, ADAP_Uint8 prefix_len);


ADAP_Uint32 Enet_Lpm_Route_Add(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len,
		ADAP_Uint32 next_hop, ADAP_Uint32 ifindex, ADAP_Uint16 vlan_id);

ADAP_Uint32 Enet_Lpm_Route_Delete(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len);

ADAP_Uint32 Enet_Lpm_Route_Get(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len,
		ADAP_Uint32 *next_hop, ADAP_Uint32 *ifindex, ADAP_Uint16 *vlan_id);

ADAP_Uint32
Enet_Lpm_Add_FP_Entry (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr,
		ADAP_Uint32 u4NextHop, ADAP_Uint32 u4IfIndex,
					   ADAP_Uint16 u2VlanId, ADAP_Uint16 u2VpnId, tEnetHal_MacAddr *pDestMacAddr);

ADAP_Uint32
Enet_Lpm_Delete_FP_Entry (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr, ADAP_Uint32 u4IpSubNetMask);

ADAP_Int32
Enet_Lpm_Handle_FPGA_Arp_Delete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr);

void
DumpLPMRouteTable (void);

void
DumpArpTbl (void);

void
DumpFPShadowRouteTbl (void);

int LpmInitialize(void);

#endif
