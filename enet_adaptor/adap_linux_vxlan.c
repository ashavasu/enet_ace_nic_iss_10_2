/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#include<stdio.h> //for printf
#include<sys/socket.h>    //for socket ofcourse
#include<stdlib.h> //for exit(0);
#include<errno.h> //For errno - the error number
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include <netinet/ether.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stddef.h>   /* offsetof */
#include <unistd.h>
#include <net/if.h>

#include "adap_types.h"
#include "mea_api.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "adap_vlanminp.h"
#include "adap_ipnp.h"
#include "adap_acl.h"
#include "mea_drv_common.h"
#include "enetadaptor.h"

#define BUFSIZE 2000


#define ifreq_offsetof(x)  offsetof(struct ifreq, x)



/* ARP Header, (assuming Ethernet+IPv4)            */ 
#define ARP_REQUEST 1   /* ARP Request             */ 
#define ARP_REPLY 2     /* ARP Reply               */ 
struct arpheader { 
    u_int16_t htype;    /* Hardware Type           */ 
    u_int16_t ptype;    /* Protocol Type           */ 
    u_char hlen;        /* Hardware Address Length */ 
    u_char plen;        /* Protocol Address Length */ 
    u_int16_t oper;     /* Operation Code          */ 
    tEnetHal_MacAddr sha;      /* Sender hardware address */
    u_char spa[4];      /* Sender IP address       */ 
    tEnetHal_MacAddr tha;      /* Target hardware address */
    u_char tpa[4];      /* Target IP address       */ 
}; 


struct vxlan_hdr
{
	unsigned char		flags;
	unsigned char		reserved;
	unsigned short		policy;
	unsigned int		vxlan; // 3 bytes vxlan and 1 byte reserved
};

struct vxLanParameters
{
	char 			*data;
	unsigned int 		data_len;
	tEnetHal_MacAddr	source_mac;
	tEnetHal_MacAddr	destination_mac;
	unsigned int		source_ip;
	unsigned int		dest_ip;
	unsigned short		source_port;
	unsigned short		dest_port;
	unsigned int		vniId;
};	


unsigned short csum(unsigned short *ptr,int nbytes) 
{
    register long sum;
    unsigned short oddbyte;
    register short answer;
 

    sum=0;
    while(nbytes>1) {
        sum+=*ptr++;
        nbytes-=2;
    }

    if(nbytes==1) {
        oddbyte=0;
        *((u_char*)&oddbyte)=*(u_char*)ptr;
        sum+=oddbyte;
    }

 

    sum = (sum>>16)+(sum & 0xffff);
    sum = sum + (sum>>16);
    answer=(short)~sum;
     

    return(answer);

}

int build_vxlan_packet(char *datagram,struct vxLanParameters *vxLanParams)
{
	struct iphdr *iph;
	struct udphdr *udph;
	struct vxlan_hdr *vxlanh;
	struct ether_header *ethh;
//	char *vmData;
//	unsigned short checksum=0;

	ethh = (struct ether_header *) datagram;
	adap_mac_to_arr(&vxLanParams->destination_mac, ethh->ether_dhost);
	adap_mac_to_arr(&vxLanParams->source_mac, ethh->ether_shost);
	ethh->ether_type=htons(0x0800);
		

	iph = (struct iphdr *) &datagram[sizeof(struct ether_header)];
	iph->ihl = 5;
    	iph->version = 4;
    	iph->tos = 0;
    	iph->tot_len = sizeof (struct iphdr) + sizeof (struct udphdr) + sizeof(struct vxlan_hdr) + vxLanParams->data_len;
    	iph->id = htonl (54321); //Id of this packet
   	iph->frag_off = 0;
    	iph->ttl = 255;
    	iph->protocol = IPPROTO_UDP;
    	iph->check = 0;      //Set to 0 before calculating checksum
    	iph->saddr = htonl(vxLanParams->source_ip);    //Spoof the source ip address
    	iph->daddr = htonl(vxLanParams->dest_ip);
	iph->check = csum ((unsigned short *)&datagram[sizeof(iph->tot_len)],iph->tot_len);	
	iph->tot_len = htons(iph->tot_len);

	udph = (struct udphdr *)&datagram[sizeof (struct ip) + sizeof(struct ether_header)];
    	udph->source = htons (50000);
    	udph->dest = htons (4789);
    	udph->len = htons(8 + vxLanParams->data_len + sizeof(struct vxlan_hdr)); //udp header size
    	udph->check = 0; //leave checksum 0 now, filled later by pseudo header
	    //udph->len = htons(udph->len);
	    vxlanh = (struct vxlan_hdr *)&datagram[sizeof (struct ip) + sizeof(struct ether_header) + sizeof(struct udphdr)];
	
        vxlanh->flags=0x08;
	vxlanh->reserved=0;
        vxlanh->policy=0x00;
        vxlanh->vxlan=(htonl)(vxLanParams->vniId << 8); // 3 bytes vxlan and 1 byte reserved

	// create crc ethernet
	//vmData = (char *)&datagram[sizeof (struct ip) + sizeof(struct ether_header) + sizeof(struct udphdr) + sizeof(struct vxlan_hdr)];
	//adap_memcpy(vmData,vxLanParams->data,vxLanParams->data_len);
	// need to do checksum
	//checksum = csum ((unsigned short *)&vmData,vxLanParams->data_len);
	//vmData[vxLanParams->data_len] = (char)( (checksum & 0xFF00)>>8);		
	//vmData[vxLanParams->data_len+1] = (char)(checksum & 0x00FF);		
	
	return  sizeof(struct ip) + sizeof(struct ether_header) + 
			sizeof(struct udphdr) + sizeof(struct vxlan_hdr) 
			+ vxLanParams->data_len;
	
}

int send_raw_socket(char *data,unsigned int len)
{
	int sockfd;
    	struct ifreq eth_req;
    	struct sockaddr_ll addr;
	unsigned int res=0;
	unsigned int bytesSend=0;
	int eth_ifindex=0;



	sockfd=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL));
	if(sockfd == -1)
	{
		printf("failed to create socket\r\n");
		return -1;
	}

   	//strcpy(eth_req.ifr_name, "ens1");
    if(MEA_device_environment_info.MEA_DEVICE_CPU_ETH_enable == MEA_TRUE)
    {
        strncpy(eth_req.ifr_name,
            &MEA_device_environment_info.MEA_DEVICE_CPU_ETH[0], /*&device_cpu127[0],*/
            sizeof(eth_req.ifr_name));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"------ ifr_name %s -----\n",eth_req.ifr_name);

    }else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The cpu is not set the ethX  use the MEA_DEVICE_CPU_ETH\n");
        return MEA_ERROR;
    }
    	if (ioctl(sockfd, SIOCGIFINDEX, &eth_req) < 0) 
	{
        	printf("Error getting ifindex for %s\n",eth_req.ifr_name);
        	return -1;
    	}

    	eth_ifindex=eth_req.ifr_ifindex;

    	if (ioctl(sockfd, SIOCGIFFLAGS, &eth_req) < 0) 
	{
        	printf("Error binding RAW socket to %s\n",eth_req.ifr_name);
        	return -1;
   	 }



    	eth_req.ifr_flags |= IFF_PROMISC;
    	if (ioctl(sockfd, SIOCSIFFLAGS, &eth_req) < 0)
    	{
       		return -1;
    	}

    	adap_memset(&addr,0, sizeof(addr));
    	addr.sll_family=AF_PACKET;
    	addr.sll_protocol=htons(ETH_P_ALL);
    	addr.sll_ifindex = eth_ifindex;

    	res = bind(sockfd, (struct sockaddr*)(&addr), sizeof(addr));

    	if(res!=0)
    	{
        	printf("Error binding raw socket: res=%i\n",res);
    	}

	bytesSend = write(sockfd,data,len);
	if ( bytesSend < 0)
	{
		printf("failed to write to socket packetSize:%d\r\n",len);
		return -1;
	}


	close(sockfd);


	return 0;	
	
}


int tun_alloc(char *dev, int flags) {

  struct ifreq ifr;
  int fd, err;
  char *clonedev = "/dev/net/tun";

  if( (fd = open(clonedev , O_RDWR)) < 0 ) {
    return fd;
  }

  adap_memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = flags;

  if (*dev) {
    strncpy(ifr.ifr_name, dev, IFNAMSIZ);
  }

  if( (err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0 ) {
    close(fd);
    return err;
  }
  printf("tun alloc\r\n");
  strcpy(dev, ifr.ifr_name);

  return fd;
}


int create_tun_device(char *if_name)
{
	int flags = IFF_TUN;
	int tap_fd=0;

	if ( (tap_fd = tun_alloc(if_name, flags | IFF_NO_PI)) < 0 ) 
	{
    		printf("tun/tap interface %s! device allready running\n", if_name);
  	}
	printf("create tun device if:%s\r\n",if_name);
	return 0;
}

int set_if_admin(int sockfd)
{
	struct ifreq ifr;
	ifr.ifr_flags = IFF_UP | IFF_RUNNING;
    ioctl(sockfd, SIOCSIFFLAGS, &ifr);

	return 0;
}


int set_ip_address(int sockfd,char *if_name,char *ip_addr) 
{
        struct ifreq ifr;
        struct sockaddr_in sai;
//        int selector;
//        unsigned char mask;
        char *p;
	int on = 1;

       strcpy(ifr.ifr_name, if_name);
        adap_memset(&sai, 0, sizeof(struct sockaddr));
        sai.sin_family = AF_INET;
        sai.sin_port = 0;
        sai.sin_addr.s_addr = inet_addr(ip_addr);
        p = (char *) &sai;
        adap_memcpy( (((char *)&ifr + ifreq_offsetof(ifr_addr) )),p, sizeof(struct sockaddr));
        ioctl(sockfd, SIOCSIFADDR, &ifr);
        ioctl(sockfd, SIOCGIFFLAGS, &ifr);

       	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, (char *)&on, sizeof(on)) < 0) 
	{
    		printf("failed to set socket option\r\n");
	}
        return 0;	

}



int is_arp_request(char *packet,int len,unsigned int *destip)
{
	struct arpheader *arph;
	int i;
	printf("%x %x\r\n",packet[12],packet[13]);
	if( (packet[12] == 0x08) && (packet[13] == 0x06) )
	{
		arph = (struct arpheader *)&packet[14];
		printf("arp oper:%d\r\n",htons(arph->oper));
		if(htons(arph->oper) != ARP_REQUEST)
			return 0;

		printf("source ip ");
		for(i=0; i<4;i++)
		{
        		printf("%d.", arph->spa[i]);
		}	
		printf("\r\n");
		printf("destination ip ");

		(*destip)=0;
		(*destip) |= ((unsigned int)(arph->tpa[0] & 0xFF) << 24);
		(*destip) |= ((unsigned int)(arph->tpa[1] & 0xFF) << 16);
		(*destip) |= ((unsigned int)(arph->tpa[2] & 0xFF) << 8);
		(*destip) |= ((unsigned int)(arph->tpa[3] & 0xFF) );

		for(i=0; i<4; i++)
		{
        	printf("%d.", arph->tpa[i]);
		}	
		printf("\r\n");
		return 1;
	}
	return 0;
}

int received_message(char *if_name)
{
	struct ifreq eth_req;
	struct sockaddr_ll addr;
	int saddr_size=0;
	int nread;
	char *str=NULL;
	char buffer[BUFSIZE];
	int header_size=0;
	int sock_fd;
	struct vxLanParameters vxLan;
	int len=0;
	fd_set socks;
	struct timeval t;
	ADAP_Uint32 index=0;
	tLinuxArpCache *newArpCache=NULL;
	ADAP_Uint32 res;
	tLinuxIpInterface *interface=NULL;
	ADAP_Uint32 tunnelInx=0;
	tEnetOfVxlanTunnel *vxlanTunnel;
	unsigned int destip;
	unsigned int destIpAddr=0;


	if ( (sock_fd = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL))) < 0)
	{
  		printf("failed to create socket\r\n");
		return -1;
	}

	set_if_admin(sock_fd);
	//set_ip_address(sock_fd,if_name,"1.1.1.1");

	strcpy(eth_req.ifr_name, if_name);
	if (ioctl(sock_fd, SIOCGIFFLAGS, &eth_req) < 0) 
	{
		printf("failed to set SIOCGIFFLAGS\r\n");
		return -1;
    	}

   		eth_req.ifr_flags |= IFF_PROMISC;
    	if (ioctl(sock_fd, SIOCSIFFLAGS, &eth_req) < 0) 
    	{
    		printf("failed to set SIOCSIFFLAGS\r\n");
        	return -1;
    	}


    	adap_memset(&addr,0, sizeof(addr));
    	addr.sll_family=AF_PACKET;
    	addr.sll_protocol=htons(ETH_P_ALL);
    	addr.sll_ifindex = if_nametoindex(eth_req.ifr_name);

    	if (bind(sock_fd, (struct sockaddr*) &addr, sizeof(addr)) < 0) 
    	{
    		printf("failed  to bind\r\n");
    		return -1;
    	}

    	interface = adap_get_interface_by_index(0);

    	if(interface == NULL)
    	{
    		printf("interface id not found\r\n");
    		return -1;
    	}
    	MeaAdapSetGwGlobalSetMacAddress(&interface->macAddress);
    	MeaAdapSetGwGlobalSetIpAddress(interface->ip_address);

	header_size= sizeof (struct ip) + sizeof(struct ether_header) + sizeof(struct udphdr) + sizeof(struct vxlan_hdr);
  	while(ADAP_TRUE)
  	{
		adap_memset(&buffer[0],0,BUFSIZE);
		str = &buffer[header_size];
		saddr_size = sizeof (struct sockaddr);

		FD_ZERO(&socks);
		FD_SET(sock_fd, &socks);
		t.tv_sec = 1;
		t.tv_usec = 0;

		if (select(sock_fd + 1, &socks, NULL, NULL, &t))
		{
			if(is_process_killed() == 1)
			{
				break;
			}
        	nread = recvfrom(sock_fd , str ,BUFSIZE-header_size , 0 ,(struct sockaddr *) &addr , (socklen_t*)&saddr_size);

        	if(nread <0 )
        	{
            		printf("Recvfrom error , failed to get packets\n");
            		return 1;
       		}	
        	else
        	{
				printf("get %d bytes\r\n",nread);
				// check if arp or multicast
				if(is_arp_request(str,nread,&destip) == 1)
				{
					res = adap_getfirst_ArpEntry(&index);
					while(res == ENET_SUCCESS )
					{
						newArpCache = adap_get_ArpEntry(index);
						adap_memcpy(&vxLan.source_mac,&interface->macAddress,sizeof(tEnetHal_MacAddr));
						adap_memcpy(&vxLan.destination_mac,&newArpCache->macAddress,sizeof(tEnetHal_MacAddr));

						vxLan.source_ip=interface->ip_address;
						vxLan.dest_ip=newArpCache->ip_address;
						vxLan.source_port=50000;
						vxLan.dest_port=4789;
						vxLan.data_len=nread;

						tunnelInx = getfirst_vxLanTunnel();

						while(tunnelInx != ADAP_END_OF_TABLE)
						{
							vxlanTunnel = get_vxLanTunnel(tunnelInx);
							destIpAddr=0;
							destIpAddr |= ( (ADAP_Uint32)(((ADAP_Uint8*)&vxlanTunnel->ipv6_dst)[12] & 0xFF) ) << 24;
							destIpAddr |= ( (ADAP_Uint32)(((ADAP_Uint8*)&vxlanTunnel->ipv6_dst)[13] & 0xFF) ) << 16;
							destIpAddr |= ( (ADAP_Uint32)(((ADAP_Uint8*)&vxlanTunnel->ipv6_dst)[14] & 0xFF) ) << 8;
							destIpAddr |= ( (ADAP_Uint32)(((ADAP_Uint8*)&vxlanTunnel->ipv6_dst)[15] & 0xFF) ) ;

							printf("tunnel ip addr:%x arp ip addr:%x index:%d\r\n",destIpAddr,destip,tunnelInx);

							if(destIpAddr == destip)
							{
								vxLan.vniId=(ADAP_Uint32)(ntohll(vxlanTunnel->in_key) & 0xFFFFFFFF);

								len =build_vxlan_packet(buffer,&vxLan);

								send_raw_socket(buffer,len);

							}
							tunnelInx = getnext_vxLanTunnel(tunnelInx);
						}
						res = adap_getNext_ArpEntry(&index);
					}
				}
        	}

		}
	}
	close(sock_fd);
	return 0;
}

int file_exist (char *filename)
{
	struct stat   buffer;
	return (stat (filename, &buffer) == 0);
}

#define BROADCAST_VXLAN_INTERFACE "bcast_vxlan"

static void*  thread_vxlan_manager(void* arg)
{
	char dev_id[30];
	char command[100];

	strcpy(dev_id,BROADCAST_VXLAN_INTERFACE);


	sprintf(command,"ls /sys/class/net | grep %s >/tmp/vxLan.txt",dev_id);

	// first need to check if the interface exist
	while(1)
	{
			//printf("check file :%s\r\n","/tmp/vxLan.txt");
			if(getfirst_vxLanTunnel() != ADAP_END_OF_TABLE)
			{
				system(command);
				if(file_exist ("/tmp/vxLan.txt") != 0)
				{
					break;
				}
			}
			sleep(5);
	}

	received_message(dev_id);
	return NULL;
}

uint32_t init_vxlan_thread(void)
{
	adap_task_id taskId;
	EnetHal_Status_t status = adap_task_manager_run(thread_vxlan_manager, NULL, &taskId);
	if(status != ENETHAL_STATUS_SUCCESS)	{
		printf("failed to run vxlan task (status = %d)\n", status);
	    return 0;
	}
	return 1;

}
