/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_SERVICE_CONFIG_H
#define ADAP_SERVICE_CONFIG_H

typedef enum
{
	E_ADAP_POINT_TO_POINT_CONFIGURATION,
	E_ADAP_BRIDGE_CONFIGURATION,

	E_END_ADAP_CONFIGURATION
}eAdapServiceType;



typedef struct
{
    tEnetHal_MacAddr							sourceMac;
    tEnetHal_MacAddr							destMac;
    ADAP_Uint32								sourceIp;
    ADAP_Uint32								destIp;
    ADAP_Uint32								tunnel_id;
    ADAP_Uint16								dest_port;
	ADAP_Uint16								source_port;
	ADAP_Uint8								ttl;
}tVxLanAdap;

typedef struct
{
	MEA_Service_t                         	Service_id;
	ADAP_Uint32 							L2_protocol_type;

	ADAP_Bool								ip_plus_vlan;
	ADAP_Uint32								da_ip_addr;

	MEA_Port_t  							inPort;
	ADAP_Uint32 							num_of_output_ports;
	MEA_Port_t 								outports[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_Policer_prof_t						policer_id;
    MEA_PmId_t    							pmId;
    MEA_TmId_t    							tmId;
    tEnetHal_VlanId								outervlanId;
    tEnetHal_VlanId								innervlanId;
    ADAP_Bool								vxlan_enable;
	ADAP_Uint32								vxlan_vni; // look only on first 24 bits

	eAdap_EhpEditing							editing;
    tEnetHal_VlanId								editOutervlanId;
    tEnetHal_VlanId								editOuterEthertype;
    tEnetHal_VlanId								editInnervlanId;
    tEnetHal_VlanId								editInnerEthertype;
    // incase of tunnel editing ( like VxLAN)
    tVxLanAdap								editVxLan;

    ADAP_Uint32								forwadingKeyType;
	ADAP_Bool								isAllreadyExist;

}tServiceAdapDb;


typedef struct
{
	ADAP_Uint32								ingressPort;
	eAdapServiceType 						type;
	MEA_Service_t                         	pointToPointService; // for point to point configuration
	ADAP_Uint32 							swFdb;				 // for bridge configuration
	ADAP_Uint32								bridgeId;			 // for bridge configuration
	MEA_PmId_t    							pmId;
	ADAP_Uint16 							vlanDomain;
}tHwDataHandle;

typedef struct
{
	MEA_Action_t 							Action_id;
	MEA_TFT_t  								hpm_profile_Id;
}tHwIpv4Route;

typedef struct
{
    MEA_Uint32        en_vpls_Ins;
    MEA_Uint32        vpls_Ins_Id;
}tHwVplsInstance;

typedef enum
{
	E_UPDATE_LPM_ROUTING_PARAMETERS,
	E_UPDATE_FORWARDDING_TYPE,
	E_UPDATE_VPN_PARAMETERS,
	E_UPDATE_OUTPUT_PARAMETERS,
	E_UPDATE_FILTER_PARAMETERS,
	E_UPDATE_VPLS_INSTANCE,
	E_UPDATE_POLICER,
	E_UPDATE_MTU_PROF,
	E_UPDATE_LIMITER,
    E_UPDATE_MCBCUC_FLOODING,
	E_UPDATE_LAST_PARAMETERS
}eUpdateServiceType;

typedef enum
{
	E_SET_SERVICE_CONFIGURATION,
	E_CLEAR_SERVICE_CONFIGURATION,
	E_GET_SERVICE_CONFIGURATION,

	E_LAST_SERVICE_CONFIGURATION
}eServiceMode;

// E_UPDATE_LPM_ROUTING_PARAMETERS
typedef struct
{
	ADAP_Bool 	Uepdn_valid;
	ADAP_Uint32 uePdn;
	ADAP_Uint16 vpn;
	ADAP_Bool DSE_forwarding_enable;
	ADAP_Uint32 forwadingKeyType;

	ADAP_Bool HPM_valid;
	ADAP_Uint32 HPM_profileId;
	ADAP_Uint32 HPM_prof_mask;
	ADAP_Uint8  hpmId;
}lpmRoutingParams;

typedef struct
{
	ADAP_Bool DSE_forwarding_enable;
	ADAP_Uint32 forwadingKeyType;
	ADAP_Bool DSE_learning_enable;
	ADAP_Uint32 learningKeyType;
}tForwardingType;

typedef struct
{
	ADAP_Uint16 vpn;
	ADAP_Bool DSE_forwarding_enable;
	ADAP_Uint32 forwadingKeyType;
}vpnConfigParams;

//E_UPDATE_FILTER_PARAMETERS
typedef struct
{
    ADAP_Bool 						filter_enable;
    MEA_ACL_prof_Key_mask_dbt 		ACL_filter_info;
    ADAP_Bool   					ACL_Prof_valid;              /*0-will take from source port   1- from profile*/
    ADAP_Uint16 					ACL_Prof;
}tSerDataFilterParams;

typedef struct
{
	ADAP_Uint32 							num_of_output_ports;
	MEA_Port_t 								outports[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32								outercommand;
    tEnetHal_VlanId								editOutervlanId;
    tEnetHal_VlanId								editOuterEthertype;
    ADAP_Uint32								innercommand;
    tEnetHal_VlanId								editInnervlanId;
    tEnetHal_VlanId								editInnerEthertype;
}editConfigParams;

typedef struct
{
    eEnetHal_TrfType type;
    ADAP_Bool notAllowed;
} tTrafficParams;

typedef struct
{
	MEA_Service_t		Service;
	eUpdateServiceType 	type;
	eServiceMode		mode;
	union
	{
		lpmRoutingParams 	lpm;
		vpnConfigParams 	vpnParam;
		editConfigParams 	editing;
		tSerDataFilterParams filterPararms;
		tForwardingType 	forwardType;
		tHwVplsInstance		vplsInst;
		tPolicyDb			policerData;
		ADAP_Uint16         mtuFlowPolicerId;
		MEA_Limiter_t       limiterId;
        tTrafficParams      trfParam;
	}x;
    ADAP_Uint16 ifIndex;
	MEA_Bool serviceCreated;
}tUpdateServiceParam;

MEA_Status MeaDrvCreateServicesFromCpu (tServiceAdapDb *pServiceDb);
MEA_Status MeaDrvCreateDefaultPolicer (MEA_Policer_prof_t *pPolicer_id,MEA_Port_t portId);
MEA_Status MeaDrvHwSetDefaultService (MEA_Port_t enetPort, MEA_Service_t service_id, MEA_Def_SID_Action_te action);
MEA_Status MeaAdapSetIngressPort (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool crc_check);
MEA_Status MeaAdapsetIngressPortGlobalMac(MEA_Port_t enetPort,MEA_Uint8 globalMacBitMask);
MEA_Status MeaAdapSetIngressPortDefaultPriority(MEA_Port_t enetPort, MEA_Uint32 def_pri);
MEA_Status MeaAdapSetShaperParams(void);
MEA_Status MeaAdapSetShaperPerPort(ADAP_Uint32 portId);
MEA_Status MeaAdapSetEgressPort (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool crc_check);
MEA_Status MeaDrvCreateVlanBridging(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type);
MEA_Status MeaDrvCreateQinQVlanBridging(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type);
MEA_Status MeaDrvCreateQinQVlanBridging(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type);
MEA_Status MeaDrvDeleteVlanBridging(tServiceDb	*pServiceFree);
MEA_Status MeaDrvDeleteAcion(MEA_Action_t 		actionId);
MEA_Status EnetAdapDhcpRedirectDemo(ADAP_Uint32	ifIndexIn,ADAP_Uint32	ifIndexOut,MEA_LxCP_Protocol_te  protocol);
MEA_Status MeaDrvHwSetInterfaceCfmOamUCMac (MEA_Port_t enetPort,tEnetHal_MacAddr *macAddr);
MEA_Status MeaDrvHwGetInterfaceCfmOamUCMac (MEA_Port_t enetPort,tEnetHal_MacAddr *macAddr);
MEA_Status MeaDrvCreatePolicer (ADAP_Uint32 rate,ADAP_Uint32 burst_size,MEA_Policer_prof_t  *pPolicer_id);
MEA_Status MeaDrvHwSetGlobalUCMac(tEnetHal_MacAddr *macAddr);
MEA_Status MeaDrvHwGetGlobalUCMac(tEnetHal_MacAddr *macAddr);
MEA_Status MeaDrvVlanHwSetL3InternalService(MEA_Port_t enetInPutPort,MEA_Port_t enetOutPutPort,MEA_Uint32 DSE_mask_field_type,MEA_Service_t *pService_id,MEA_Uint32 vpn);
MEA_Status MeaDrvCreateActionToCpu(MEA_Action_t   *Action_id);
MEA_Status MeaDrvNatCreateAction(MEA_Action_t   *Action_id,tEnetHal_MacAddr *destMac,MEA_Uint32 ipAddr,MEA_Uint16 l4Port,MEA_Bool userToNetwork,MEA_Port_t enetPort,MEA_PmId_t *pPm_id);
MEA_Status FsMiHwCreateRouterAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,int num_of_entries,MEA_OutPorts_Entry_dbt *pOutPorts,mea_action_type_te type,MEA_Uint16 UEPDN_Id, MEA_Uint8 index,MEA_Policer_prof_t policer_prof_id);
MEA_Status FsMiHwCreateLpmProfile(MEA_TFT_t  *tft_profile_Id);
MEA_Status MeaDrvCreatePbVlanCEP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap,ADAP_Uint32 tagged);
MEA_Status MeaDrvCreatePbVlanCNP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap);
MEA_Status MeaDrvCreatePbVlanPNP(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain);
MEA_Status MeaDrvCreatePbVlanPNPsMap(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,tEnetHal_VlanSVlanMap *sVlanMap);
MEA_Status MeaDrvCreatePointToPointServices (tServiceAdapDb *pServiceDb);
MEA_Status EnetAdapLxcpAction(ADAP_Uint32	ifIndexIn,ADAP_Uint32 ifIndexOut[],ADAP_Uint32 numOfOutputPort,ADAP_Bool toCpu,MEA_LxCP_Protocol_te  protocol, MEA_LxCP_Protocol_Action_te action);
MEA_Status MeaDrvUpdateService (tUpdateServiceParam *pParams);
MEA_Status MeaDrvVxLanCreateAction(MEA_Action_t   *Action_id,tVxLanAdap *pVxLan,MEA_Port_t enetPort,MEA_PmId_t *pPm_id,MEA_Uint32 editingType);
MEA_Status MeaDrvCreateMulticastAction(MEA_Action_t   *Action_id,MEA_Uint32 *outPorts,MEA_Uint32 num_of_ports);
MEA_Status MeaAdapSetGwGlobalSetMacAddress(tEnetHal_MacAddr *macaddress);
MEA_Status MeaAdapSetGwGlobalSetIpAddress(MEA_Uint32 ipAddress);
MEA_Status MeaAdapSetGwGlobalSetVxlanVlanRange(MEA_Uint16 vlanId);
MEA_Status MeaAdapSetGwGlobalSetVxlanUdpDest(MEA_Uint16 dst_port);
MEA_Status ADAP_API_Get_Vxlan_snooping_Info(tAdapVxlanSnoop *vxLanSnoopArr,MEA_Uint32 *max_entries);
MEA_Status MeaDrvHwSetAction(MEA_Action_t action_Id, tEnetHal_HwPortArray * pHwMcastPorts);
ADAP_Uint32 MeaDrvDeleteVlanPriority(void);
ADAP_Int32 meaDrvDeleteDefaultService(ADAP_Uint32 ifIndex);
ADAP_Int32 MeaDeleteDefaultServiceId(MEA_Service_t   Service_id);
ADAP_Uint32 MeaDrvDelVlanEntryEnet(tEnetHal_VlanId VlanId);
ADAP_Uint32 meaDrvCreateDefaultLagService(ADAP_Uint32 u4IfIndex);
ADAP_Int32 MeaDrvVlanHwLacpLxcpUpdate (MEA_Bool add,MEA_LxCp_t   LxCp_Id);
ADAP_Uint32 MeaDeleteEnetServiceId(MEA_Service_t   Service_id);
ADAP_Uint32 MeaDeleteCopiedEnetServiceId(MEA_Service_t   Service_id);
void MeaDrvVlanSetEHPInfoData(MEA_EHP_Info_dbt *pInfo,eAdap_EhpEditing editType,ADAP_Uint32 learnEnable,ADAP_Uint16 primaryVlan,ADAP_Uint16 seconderyVlan);
MEA_Status MeaAdapSetIngressInterfaceType (ADAP_Int32 i4Port,MEA_Filter_key_interface_type_te interfaceType);
ADAP_Uint32 MeaDrvPortMacSetFilterMask( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_VlanId vlanId, tEnetHal_VlanId editVlanId,tEnetHal_MacAddr *MacAddr,ADAP_Uint32 edit,MEA_Filter_t *pFilterId, ADAP_Uint32 L2_protocol_type);
ADAP_Uint32 MeaDrvPortMacDeleteFilterMask(MEA_Service_t Service_id,MEA_Filter_t filterId);
ADAP_Int32	MeaDrvVlanHwLxcpUpdate (eEnetHal_VlanHwTunnelFilters LxcpServiceProtocol, MEA_Bool add);
MEA_Status MeaDrvUpdateUserPriority (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Int32 i4DefPriority);
MEA_Status MeaDrvSetFlowMarkingProfile (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Uint16 flowMarkProfile);
ADAP_Int32 MeaDrvAddTunnelInTranslation (void);
ADAP_Int32 MeaDrvAddTunnelOutTranslation (void);
ADAP_Int32 MeaDrvRemoveTunnelOutTranslation (void);
ADAP_Int32 MeaDrvLxcpUpdateProtocols (ADAP_Uint32 ifIndex,MEA_LxCP_Protocol_Action_te action,MEA_LxCP_Protocol_te lxcp_type);
ADAP_Int32 MeaDrvRemProviderTunnel (tEnetHal_MacAddr *daMac);
ADAP_Int32 MeaDrvRemCustomerTunnel (tEnetHal_MacAddr *daMac);
ADAP_Int32 MeaDrvAddCustomerTunnel (tEnetHal_MacAddr *origMac,tEnetHal_MacAddr *newMac,eEnetHal_VlanHwTunnelFilters protocol);
ADAP_Int32 MeaDrvAddProviderTunnel (tEnetHal_MacAddr *origMac,tEnetHal_MacAddr *newMac,eEnetHal_VlanHwTunnelFilters protocol);
ADAP_Int32 MeaDrvSetLacpTunnelConfiguration(ADAP_Uint32 u4ContextId,ADAP_Uint32 u4IfIndex, ADAP_Uint16 VpnIndex);
ADAP_Int32 MeaDrvAddPnpTunnel(eEnetHal_VlanHwTunnelFilters ProtocolId);
MEA_Status MeaDrvDeleteTxForwarder(tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex);
MEA_Status MeaDrvCreateTxForwarder(MEA_Action_t action_id,tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvMstpCreateCpuAction(MEA_Action_t   *Action_id);
ADAP_Int32 MeaDrvSetPort802Dot1Lxcp(ADAP_Int32 ifIndex,MEA_LxCP_Protocol_Action_te action);
ADAP_Int32 MeaDrvSetBpdu3Lxcp(ADAP_Int32 ifIndex,MEA_LxCP_Protocol_Action_te action);
ADAP_Uint32 MeaDrvPortMacSetFilterSrcMacAddress( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_VlanId vlanId, tEnetHal_VlanId editVlanId,tEnetHal_MacAddr *MacAddr,ADAP_Uint32 edit,MEA_Filter_t *pFilterId);
ADAP_Uint32 MeaDrvSetPortWhiteListFlag(MEA_Service_t  *Service_id,MEA_Bool enable,ADAP_Uint16 u2PortNum);
ADAP_Int32 MeaDrvProviderStpHwLxcpUpdate (ADAP_Uint32 ifIndex, MEA_Bool sendToCpu);
ADAP_Uint32 MeaDrvSetIngressPortPriRule(MEA_Uint32 pri_rule , ADAP_Uint32 ifIndex);
ADAP_Int32 MeaDrvDelEntryDeleteSrv (ADAP_Uint32 u4IssPort,tEnetHal_VlanId VlanId, ADAP_Uint8 u1IsTagged);
ADAP_Int32 MeaDrvCreateSVlanTranslationEntry(ADAP_Uint32 ifIndex,MEA_Bool relayIngress,ADAP_Uint16 u2RelaySVlan,ADAP_Uint16 localVlan);
MEA_Status MeaDrvSetFlowPolicerProfile (ADAP_Uint32 u4IfIndex, MEA_Service_t Service_id, ADAP_Uint16 flowPolicerProfile);
ADAP_Int32 MeaDrvSetTunnelToNormalMode(ADAP_Uint16 u4IfIndex,ADAP_Uint32 u4ContextId,eEnetHal_VlanHwTunnelFilters ProtocolId,tEnetHal_MacAddr *MacAddr,tEnetHal_VlanSVlanMap *pVlanSVlanMap,ADAP_Uint32 u4TunnelStatus);
ADAP_Int32 MeaDrvSetPortTunnelConfiguration(ADAP_Uint32 u4IfIndex,eEnetHal_VlanHwTunnelFilters ProtocolId,ADAP_Uint32 u4ContextId,tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Int32 MeaDrvSetAllPortTunnelConfigBack(tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Uint32 MeaDrvPortMacSetFilteDestMacAddress( MEA_Service_t  Service_id,ADAP_Uint32 u4IfIndex,tEnetHal_MacAddr *MacAddr,MEA_Filter_t *pFilterId,tEnetHal_MacAddr *MacSwap);
ADAP_Uint32 MeaDrvVlanHwCreateFilterPriorityVlan(MEA_Service_t ExistServiceId,ADAP_Uint16 trafficType,ADAP_Uint16 u4IfIndex,ADAP_Uint8 priority,ADAP_Uint16 VlanId,ADAP_Uint8 RegenPriority);
MEA_Status MeaDrvCreateDefaultService (ADAP_Uint32 u4IfIndex);
ADAP_Int32 MeaDrvVlanHwUpdateParameters(MEA_Service_t    Service_id,ADAP_Uint32 ifIndex,MEA_Bool bSet1588, ADAP_Uint32 set_1588,ADAP_Uint16 vlanId);
ADAP_Int32  MeaDrvVlanHwUpdatePnac (tServiceDb * pServiceId);
MEA_Status MeaDrvCreatePPPoEAction(MEA_Action_t   *Action_id, tEnetHal_MacAddr *srcMac, tEnetHal_MacAddr *destMac, ADAP_Uint32 editingtype,
                                        ADAP_Uint32 vlanId,ADAP_Uint32 sessionId,MEA_Port_t enetPort);

MEA_Status MeaDrvCreateMulticastService(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 type);
ADAP_Int32 MeaDrvSetIngressColorAwareInterfaceType (ADAP_Int32 i4Port,MEA_Uint32 port_col_aw);
ADAP_Uint32 MeaDrvSetIngressPortMode(ADAP_Uint32  IP_pri_mask_type , ADAP_Uint32 ifIndex);
ADAP_Uint32 MeaDrvSetIngressPortProtocol(ADAP_Uint32  port_cos_aw,ADAP_Uint32 port_ip_aw , ADAP_Uint32 ifIndex);

MEA_Status MeaAdapAddStaticNatEntry(MEA_SE_Entry_dbt *pEntry);
ADAP_Int32
MeaDrvGetPolicerMeterParams(ADAP_Uint16 MeterIdx, ADAP_Uint16 IssPort, ADAP_Uint16 ServicePM, ADAP_Uint16 *PolicingProfileId,
						 ADAP_Uint16 *TmIdValue, ADAP_Uint16 *PmIdValue, MEA_Policer_Entry_dbt *pPolicerEntry);

MEA_Status MeaDrvVlanCreateL3InternalService(MEA_Port_t enetInPutPort,MEA_Port_t enetOutPutPort,
						MEA_Uint32 DSE_mask_field_type,
						MEA_Service_Entry_Key_dbt *pService_key,MEA_Uint32 vpn,
						tBridgeDomain *pBridgeDomain); 
MEA_Bool MeaDrvSetPolicerInDestIpForwarder (ADAP_Uint32 destIpv4, ADAP_Uint16 vpn,
                                            MEA_Policer_prof_t policer_prof_id, MEA_Action_Entry_Data_dbt *pAction);
MEA_Status MeaAdapSetIngressPortAdminStatus (MEA_Port_t enetPort,ADAP_Bool portEnable,ADAP_Bool crc_check);
MEA_Status MeaDrvCreateInnerVlanL2Service(ADAP_Uint32 ifIndex,tBridgeDomain *pBridgeDomain,ADAP_Uint32 InnerVlan);

void MeaDrvHandleDefaultRouteService (MEA_Service_t Service_id, tEnetHal_NpNextHopInfo *pRouteEntry);
void MeaDrvResetDefaultRouteService (MEA_Service_t Service_id);

MEA_Bool MeaDrvSetPolicerInPPPoEForwarder (ADAP_Uint16 sessionId, ADAP_Uint16 vpn,
                                            MEA_Policer_prof_t policer_prof_id);

#endif
