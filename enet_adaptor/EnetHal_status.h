/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ENETHAL_STATUS_H__
#define __ENETHAL_STATUS_H__

#include "adap_types.h"

/* Error codes */
#define ENETHAL_STATUS_SUCCESS                     0  /* No error */

#define ENETHAL_STATUS_INVALID_PARAM               1  /* One of the arguments is invalid */
#define ENETHAL_STATUS_NULL_PARAM                  2  /* Unexpected NULL in one of the arguments */
#define ENETHAL_STATUS_NOT_SUPPORTED               3  /* The combination of arguments is not supported currently */

#define ENETHAL_STATUS_LIMIT_REACHED               4  /* Configuration limit reached */
#define ENETHAL_STATUS_NOT_FOUND                   5  /* An element unexpectedly wasn't found in internal database */
#define ENETHAL_STATUS_ALREADY_FOUND               6  /* An element unexpectedly found in internal database */

#define ENETHAL_STATUS_NOT_INITIALIZED             7  /* A module was called without proper initialization */

#define ENETHAL_STATUS_MEM_ALLOC                   8  /* Could not allocate memory */
#define ENETHAL_STATUS_MUTEX_ERROR                 9  /* Mutex operation failed (create/destroy/lock/unlock) */
#define ENETHAL_STATUS_THREAD_ERROR                10 /* Thread operation failed (create/destroy/cancel/join) */

#define ENETHAL_STATUS_MEA_ERROR                   11 /* MEA returned unexpected error */

#define ENETHAL_STATUS_GENERIC_ERROR               12 /* A generic error code */



/* Return type of APIs */
typedef ADAP_Int32 EnetHal_Status_t;



#endif /* __ENETHAL_STATUS_H__ */
