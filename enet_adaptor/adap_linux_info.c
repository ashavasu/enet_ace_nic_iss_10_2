/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*
 * adap_linux_info.c
 *
 *  Created on: Feb 15, 2017
 *      Author: root
 */


#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_vlanminp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_linux_info.h"

//cat /proc/net/arp | awk '{print $1,$4,$6}' | grep eno1
ADAP_Int32 get_linux_interface_name(char *fileName,tLinuxIpInterface *pInterface)
{
	FILE *fd=NULL;
	char line[100];
	int i;

	fd = fopen(fileName, "r");

    if (!fd)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"popen :%s\r\n",fileName);
		return ENET_FAILURE;
    }
    while (fgets(line, sizeof(line), fd))
    {
    	i=0;

    	while( (i < 100) && (i < strlen(line) ) && (line[i] != '\n') )
    	{
    			pInterface->interface_name[i] = line[i];
    			i++;
    	}

    	printf("got interface name:%s\r\n",pInterface->interface_name);

    }
    fclose(fd);
    return ENET_SUCCESS;
}



ADAP_Int32 get_arp_cache_per_interface(char *interface_name,ADAP_Int32 *maxOfEntries,tLinuxIpInterface *pInterfaceArray)
{
	char command[100];
	FILE *arpCache=NULL;
	char line[100];
	ADAP_Int32 numOfEntries=(*maxOfEntries);
	char ip_address[30];
	char mac_address[50];
	ADAP_Int32 i=0;
	char *str=NULL;
	ADAP_Uint8 bytes[ETH_ALEN];

    //printf("check arp on interface:%s\r\n",interface_name);

	(*maxOfEntries) = 0;


	sprintf(command,"cat /proc/net/arp | awk '{print $1,$4,$6}' | grep %s >/tmp/arp_table",interface_name);
	system(command);

	sprintf(command,"chmod 777 /tmp/arp_table");
	system(command);


    arpCache = fopen("/tmp/arp_table", "r");
    if (!arpCache)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"popen :%s\r\n",command);
		return ENET_FAILURE;
    }

    while (fgets(line, sizeof(line), arpCache))
    {
    	//printf("%s\r\n",line);
    	if((*maxOfEntries) < numOfEntries)
    	{
    		i=0;
    		while(line[i] != ' ')
    		{
    			i++;
    		}
    		i++;
    		adap_memcpy(ip_address,line,i);
    		ip_address[i]=0;
    		str = &line[i];

    		i=0;
    		while(str[i] != ' ')
    		{
    			i++;
    		}
    		i++;
    		adap_memcpy(mac_address,str,i);
    		mac_address[i]=0;
    		//printf("mac:%s\r\n",mac_address);
			if (sscanf (ip_address, "%hhu.%hhu.%hhu.%hhu%*c",
			                  &bytes[3], &bytes[2],
							  &bytes[1], &bytes[0]) == 4)
			{
				pInterfaceArray[(*maxOfEntries)].ip_address  = bytes[3] << 24;
				pInterfaceArray[(*maxOfEntries)].ip_address |= bytes[2] << 16;
				pInterfaceArray[(*maxOfEntries)].ip_address |= bytes[1] << 8;
				pInterfaceArray[(*maxOfEntries)].ip_address |= bytes[0];

//				printf("got ipaddr:%x\r\n",pInterfaceArray[(*maxOfEntries)].ip_address);
			}
			if (sscanf (mac_address, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c",
							&bytes[5], &bytes[4], &bytes[3],
							&bytes[2], &bytes[1], &bytes[0]) >= ETH_ALEN)
			{
				adap_mac_from_arr(&pInterfaceArray[(*maxOfEntries)].macAddress, bytes);

//				printf("mac address:%x:%x:%x:%x:%x:%x\r\n",
//								pInterfaceArray[(*maxOfEntries)].macAddress[0],
//								pInterfaceArray[(*maxOfEntries)].macAddress[1],
//								pInterfaceArray[(*maxOfEntries)].macAddress[2],
//								pInterfaceArray[(*maxOfEntries)].macAddress[3],
//								pInterfaceArray[(*maxOfEntries)].macAddress[4],
//								pInterfaceArray[(*maxOfEntries)].macAddress[5]);
			}
			(*maxOfEntries)++;
    	}
    }
    fclose(arpCache);
    return ENET_SUCCESS;
}

ADAP_Int32 get_source_mac_by_host_name(ADAP_Int32 num_of_interfaces,tLinuxIpInterface *pInterfaceArray)
{
	struct ifreq s;
	ADAP_Int32 i=0;
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

	for(i=0;i<num_of_interfaces;i++)
	{
		strcpy(s.ifr_name, pInterfaceArray[i].interface_name);
		if (0 == ioctl(fd, SIOCGIFHWADDR, &s))
		{
			adap_mac_from_arr(&pInterfaceArray[i].macAddress, (ADAP_Uint8 *)s.ifr_addr.sa_data);
			printf("mac address %s\n",
					adap_mac_to_string(&pInterfaceArray[i].macAddress));
		}
	}
	close(fd);
	return ENET_SUCCESS;
}

ADAP_Int32 get_source_ip_address_by_host_name(ADAP_Int32 num_of_interfaces,tLinuxIpInterface *pInterfaceArray)
{
    struct ifaddrs *ifaddr, *ifa;
    int s;
    char host[NI_MAXHOST];
    ADAP_Int32 i=0;
    unsigned int            byte3;
    unsigned int            byte2;
    unsigned int            byte1;
    unsigned int            byte0;
    char              		dummyString[2];

    if (getifaddrs(&ifaddr) == -1)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call getifaddrs\r\n");
		return ENET_FAILURE;
    }


    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        for(i=0;i<num_of_interfaces;i++)
        {
			if((strcmp(ifa->ifa_name,pInterfaceArray[i].interface_name)==0)&&(ifa->ifa_addr->sa_family==AF_INET))
			{
				if (s != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call getifaddrs\r\n");
					return ENET_FAILURE;
				}
				adap_memcpy(pInterfaceArray[i].host_ip,host,MAX_SIZEOFOF_INTERFACE_IP);
				printf("\tInterface : %s\n",ifa->ifa_name );
				printf("\t  Address : %s\n", host);
				if (sscanf (host, "%u.%u.%u.%u%1s",
				                  &byte3, &byte2, &byte1, &byte0, dummyString) == 4)
				{
					pInterfaceArray[i].ip_address  = (byte3 & 0xFF)<< 24;
					pInterfaceArray[i].ip_address  |= (byte2 & 0xFF)<< 16;
					pInterfaceArray[i].ip_address  |= (byte1 & 0xFF)<< 8;
					pInterfaceArray[i].ip_address  |= (byte0 & 0xFF);
					printf("ip address : 0x%x\n", pInterfaceArray[i].ip_address);
					pInterfaceArray[i].valid_ip = ADAP_TRUE;
				}
			}
        }
    }

    freeifaddrs(ifaddr);
    return ENET_SUCCESS;
}



