#include "adap_lpminc.h"
#include "adap_lpmrb.h"
#include "adap_utils.h"

tUtilSemId gu4SemArray[MAX_RED_BLK_TREE_INST] = {0};
/** Local Functions *********************************************************/
static NODE      *
__setchild (NODE * p, int dir, NODE * c)
{
    p->child[dir] = c;
    if (c)
    {
        c->parent = p;
    }
    return c;
}

/** Binary Tree Routines ****************************************************/
static NODE      *
__ytFirst (TREE * tree, int order)
{
    NODE             *p = tree->root.child[0];

    if (!p)
    {
        return 0;
    }

    switch (order)
    {
        case T_INORDER:
            while (p->child[0])
            {
                p = p->child[0];
            }
            return p;
        case T_POSTORDER:
            while (p->child[0] || p->child[1])
            {
                if (p->child[0])
                {
                    p = p->child[0];
                }
                else
                {
                    p = p->child[1];
                }
            }
            return p;
        case T_PREORDER:
            return p;
        default:
            return 0;
    }
}

static NODE      *
__ytNext (NODE * p, int order)
{
    switch (order)
    {
        case T_PREORDER:
            if (p->child[0])
            {
                return p->child[0];
            }
            else if (p->child[1])
            {
                return p->child[1];
            }
            else if (IS_ROOT (p))
            {
                return 0;
            }
            else if (IS_LEFT (p))
            {
                if (p->parent->child[1])
                {
                    return p->parent->child[1];
                }
                else
                {
                    p = p->parent;
                }
            }
            else
            {
                p = p->parent;
            }

            while (!IS_ROOT (p))
            {
                if (IS_LEFT (p))
                {
                    if (p->parent->child[1])
                    {
                        return p->parent->child[1];
                    }
                    else
                    {
                        p = p->parent;
                    }
                }
                else
                {
                    p = p->parent;
                }
            }
            return 0;

        case T_POSTORDER:
            if (IS_ROOT (p))
            {
                return 0;
            }
            else if (IS_LEFT (p))
            {
                if (p->parent->child[1])
                {
                    p = p->parent->child[1];
                    while (p->child[0] || p->child[1])
                    {
                        if (p->child[0])
                        {
                            p = p->child[0];
                        }
                        else
                        {
                            p = p->child[1];
                        }
                    }
                    return (NODE *) p;
                }
                else
                {
                    return p->parent;
                }
            }
            else
            {
                return p->parent;
            }
        case T_INORDER:
            if (p->child[1])
            {
                p = p->child[1];
                while (p->child[0])
                {
                    p = p->child[0];
                }
                return (NODE *) p;
            }
            else
            {
                while (IS_RIGHT (p))
                {
                    p = p->parent;
                    if (IS_ROOT (p))
                    {
                        return 0;
                    }
                }
                return IS_ROOT (p) ? 0 : p->parent;
            }

        default:
            return 0;
    }
}

static NODE      *
__ytFind (TREE * tree, void *item)
{
    NODE             *p;

    ASSERT (tree != 0);

    for (p = tree->root.child[0]; p;)
    {
        int                 diff = tree->compare (item, tree->keyof (tree, p));

        if (diff < 0)
        {
            p = p->child[0];
        }
        else if (diff > 0)
        {
            p = p->child[1];
        }
        else
        {
            return p;
        }
    }

    return 0;
}

/** Red-Black Trees *********************************************************/
static void
__ytInit (TREE * tree, INT4 (*compare) (void *, void *),
        void *(*keyof) (TREE *, NODE *))
{
    ASSERT (compare != 0);
    ASSERT (keyof != 0);

    tree->root.child[0] = 0;
    tree->root.child[1] = 0;
    tree->root.parent = 0;
    tree->root.red = 0;
    tree->compare = compare;
    tree->keyof = keyof;
    tree->count = 0;
}

static NODE      *
basicRotate (NODE * p, int d)
{
    NODE             *t = p->child[!d];

    __setchild (p, !d, t->child[d]);
    __setchild (t, d, p);
    return t;
}

static void
Rotate (NODE * n, int d)
{
    NODE             *p = n->parent, *r;
    int                 s = SLANT (n);

    r = basicRotate (n, d);
    __setchild (p, s, r);
}

static NODE      *
singleRotate (NODE * p, int d)
{
    NODE             *t = basicRotate (p, d);

    p->red = 1;
    t->red = 0;
    return t;
}

static NODE      *
doubleRotate (NODE * p, int d)
{
    __setchild (p, !d, singleRotate (p->child[!d], !d));
    return singleRotate (p, d);
}

static void
swap (NODE * a, NODE * b)
{
    NODE             *ap, *bp, *ac[2], *bc[2];
    int                 ad, bd, red;

    ap = a->parent;
    ad = (a == ap->child[1]);
    ac[0] = a->child[0];
    ac[1] = a->child[1];

    bp = b->parent;
    bd = (b == bp->child[1]);
    bc[0] = b->child[0];
    bc[1] = b->child[1];

    if (bp == a)
    {
        __setchild (ap, ad, b);
        __setchild (b, bd, a);
        __setchild (b, !bd, ac[!bd]);

        __setchild (a, 0, bc[0]);
        __setchild (a, 1, bc[1]);
    }
    else if (ap == b)
    {
        __setchild (bp, bd, a);
        __setchild (a, ad, b);
        __setchild (a, !ad, bc[!ad]);

        __setchild (b, 0, ac[0]);
        __setchild (b, 1, ac[1]);
    }
    else
    {
        __setchild (ap, ad, b);
        __setchild (bp, bd, a);

        __setchild (a, 0, bc[0]);
        __setchild (a, 1, bc[1]);

        __setchild (b, 0, ac[0]);
        __setchild (b, 1, ac[1]);
    }

    /* Swap the colours */
    red = a->red;
    a->red = b->red;
    b->red = red;
}

static void
__ytInsert (TREE * tree, NODE * entry)
{
    NODE              dummy;
    NODE             *g = 0, *t = &dummy, *p = 0, *q;
    int                 d = 0, last = 0;

    if ((tree == 0) || (entry == 0))
    {
        return;
    }

    if ((q = tree->root.child[0]) == 0)
    {
        /* Empty tree */
        entry->red = 0;            /* Root is always black */
        entry->child[0] = entry->child[1] = 0;
        __setchild (&(tree->root), 0, entry);
        tree->count++;

        ASSERT (tree->count == 1);
        return;
    }

    dummy.parent = 0;
    dummy.child[0] = 0;
    dummy.child[1] = q;
    dummy.red = 1;

    for (;;)
    {
        int                 v;

        if (q == 0)
        {
            entry->child[0] = entry->child[1] = 0;
            entry->red = 1;
            q = __setchild (p, d, entry);
        }
        else if (q->child[0] && q->child[1] && q->child[0]->red
                 && q->child[1]->red)
        {
            /* Node with 2 red children -- flip colors */
            q->red = 1;
            q->child[0]->red = q->child[1]->red = 0;
        }

        ASSERT (q);

        /* Fix red violation */
        if (p && q->red && p->red)
        {
            int                 e = (g == t->child[1]);

            ASSERT (g);
            if (q == p->child[last])
            {
                __setchild (t, e, singleRotate (g, !last));
            }
            else
            {
                __setchild (t, e, doubleRotate (g, !last));
            }
        }

        v = tree->compare (tree->keyof (tree, q), tree->keyof (tree, entry));
        if (v == 0)
        {
            break;
        }

        last = d;
        d = (v < 0);

        if (g)
        {
            t = g;
        }

        g = p;
        p = q;
        q = q->child[d];
    }

    /* Update the root */
    __setchild (&(tree->root), 0, dummy.child[1]);

    /* Ensure tree root is black */
    tree->root.child[0]->red = 0;

    tree->count++;
}

static void
adjust (NODE * e)
{
    for (;;)
    {
        NODE             *s, *p, *n[2], *c[2];
        int                 d;

        if (e->parent == 0)
        {
            /* Can't really happen -- note that this does NOT mean root -- Keep Klocworks happy */
            return;
        }

        if (IS_ROOT (e))
        {
            /* Case 1: The node is the root */
            log ("Case 1");
            return;
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        c[0] = e->child[0];
        c[1] = e->child[1];

        if (IS_RED (s))
        {
            /* Case 2: The node's sibling is red */
            /* -- this falls through to the next case */
            log ("Case 2");
            p->red = 1;
            s->red = 0;
            Rotate (p, d);

            ASSERT (c[0] == e->child[0]);
            ASSERT (c[1] == e->child[1]);
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        n[0] = s->child[0];
        n[1] = s->child[1];

        if (IS_BLACK (p) && IS_BLACK_STRICT (s) && IS_BLACK (n[0])
            && IS_BLACK (n[1]))
        {
            /* Case 3: The node's parent, sibling & nephews are black */
            log ("Case 3");
            s->red = 1;            /* FIXME */
            e = e->parent;

            continue;
        }

        if (IS_RED (p) && IS_BLACK_STRICT (s) && IS_BLACK (n[0])
            && IS_BLACK (n[1]))
        {
            /* Case 4: The node's parent is red, sibling & nephews are black */
            log ("Case 4");
            s->red = 1;
            p->red = 0;
            return;
        }

        if (IS_BLACK_STRICT (s) && IS_RED (n[d]) && IS_BLACK (n[!d]))
        {
            /* Case 5: The node's sibling is black, near nephew is red and far nephew is black */
            /* -- this falls through to the next case */
            log ("Case 5");
            s->red = 1;
            n[d]->red = 0;
            Rotate (s, !d);
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        ASSERT (s);
        n[0] = s->child[0];
        n[1] = s->child[1];

        /* Case 6: The node's sibling is black, far nephew is red */
        log ("Case 6");
        ASSERT (IS_BLACK (s) && IS_RED (n[!d]));

        s->red = p->red;
        p->red = 0;
        n[!d]->red = 0;
        Rotate (p, d);

        return;
    }
}

static void
__ytDelete (TREE * tree, NODE * entry)
{
    int                 d;
    NODE             *p;

    if ((tree == 0) || (entry == 0))
    {
        return;
    }

    if (entry->child[0] && entry->child[1])
    {
        NODE             *m = entry->child[0];

        while (m->child[1])
        {
            m = m->child[1];
        }

        /* Swap the node and its predecessor */
        swap (entry, m);
    }

    ASSERT (!(entry->child[0] && entry->child[1]));

    if (!entry->red)
    {
        if (IS_RED (entry->child[0]))
        {
            log ("Black, Left Child Red");
            entry->red = 1;
            entry->child[0]->red = 0;
        }
        else if (IS_RED (entry->child[1]))
        {
            log ("Black, Right Child Red");
            entry->red = 1;
            entry->child[1]->red = 0;
        }
        else
        {
            log ("Black, No Red Children, Adjusting");
            adjust (entry);
        }
    }
    else
    {
        log ("Red");
    }

    ASSERT (!(entry->child[0] && entry->child[1]));

    p = entry->parent;
    d = (entry == p->child[1]);

    if (entry->child[0])
    {
        __setchild (p, d, entry->child[0]);
    }
    else
    {
        __setchild (p, d, entry->child[1]);
    }

    entry->parent = entry->child[0] = entry->child[1] = 0;
    tree->count--;
}

#define    NODEPTR(T, N)    (((T)->NodeType == RedBlk_EMBD_NODE) ? \
                         ((void *)((unsigned char *)(N) - (T)->u4Offset)) : \
                         (((tRedBlkNode *)(N))->key) )

static UINT4        gu4SemCnt;

UINT4
RedBlkTreeLibInit (void)
{
    gu4SemCnt = 1;
    adap_memset(gu4SemArray, 0, sizeof(gu4SemArray));
    return (LPM_SUCCESS);

}

void
RedBlkTreeLibShut (void)
{
    gu4SemCnt = 1;
    return;
}

static void        *
keyof (TREE * T, NODE * N)
{
    void               *z;
    tRedBlkTree             t = (tRedBlkTree) T;

    z = NODEPTR (t, N);

    return z;
}

static              tRedBlkTree
create (tRedBlkCompareFn compare, unsigned int offset, UINT1 au1SemName[])
{
    tRedBlkTree             T;
    UINT1               au1Name[8];
    UINT1               u1AppSem = TRUE;
    UINT4               u4LoopIndex = 0;

    if (STRCMP (au1SemName, "") == 0)
    {
        u1AppSem = FALSE;
    }

    if ((T = (struct rbtree *)adap_malloc(sizeof(struct rbtree))) == 0)
    {
        return (0);
    }

    T->u4Offset = offset;

    __ytInit (&(T->__tree__), compare, keyof);

    adap_memset(au1Name, '\0', 8);

    /* When two threads tries to get the semaphore name, there is a
     * possibility that both the threads get the same semaphore name,
     * since the semaphore name is generated based on the loop index. 
     * LOCK is taken before using the loop index, so that the the next 
     * thread doesnt use the same loopindex. */
    if (u1AppSem == FALSE)
    {
        gu4SemCnt++;
        for (u4LoopIndex = 0; u4LoopIndex < MAX_RED_BLK_TREE_INST; 
                u4LoopIndex++)	
        {
            if (gu4SemArray[u4LoopIndex] == FALSE)
            {
                au1Name[0] = (UINT1) ((u4LoopIndex % U1MAX) + 1);
                au1Name[1] = (UINT1) (((u4LoopIndex / U1MAX) % U1MAX) + 1);
                au1Name[2] = 'b';
                au1Name[3] = 'r';
                break;
            }
        }
    }
    else
    {
        STRCPY (au1Name, au1SemName);

    }

    if (UtilSemCrt (au1Name, &T->SemId) != 0)
    {
        free (T);
        return (0);
    }
    if ((u1AppSem == FALSE) &&
	(u4LoopIndex < MAX_RED_BLK_TREE_INST))
    {
        gu4SemArray[u4LoopIndex] = T->SemId;
    }

    T->b1MutualExclusive = TRUE;
    RedBlk_SEM_GIVE (T);
    return (T);
}


tRedBlkTree
RedBlkTreeCreateEmbedded (UINT4 u4Offset, tRedBlkCompareFn Cmp)
{
    tRedBlkTree             T;

    if ((T = create (Cmp, u4Offset, (UINT1 *) "")) == 0)
    {
        return (0);
    }

    T->NodeType = RedBlk_EMBD_NODE;
    T->next_cache = NULL;

    return (T);
}

static void
destroynode (NODE * N, unsigned int offset, tRedBlkKeyFreeFn fn, UINT4 arg)
{
    void               *z = (void *) (((unsigned char *) N) - offset);

    if (N->child[0])
    {
        destroynode (N->child[0], offset, fn, arg);
    }

    if (N->child[1])
    {
        destroynode (N->child[1], offset, fn, arg);
    }

    if (fn)
    {
        fn (z, arg);
    }

}

static void
destroy (tRedBlkTree T, tRedBlkKeyFreeFn fn, UINT4 arg)
{
    TREE              *t = &(T->__tree__);

    if (t->root.child[0])
    {
        destroynode (t->root.child[0], T->u4Offset,
                       fn, arg);
        t->root.child[0] = 0;
    }

    t->count = 0;
}

void
RedBlkTreeDrain (tRedBlkTree T, tRedBlkKeyFreeFn FreeFn, UINT4 u4Arg)
{
    if (T == 0)
    {
        return;
    }

    destroy (T, FreeFn, u4Arg);
    T->next_cache = NULL;
}

void
RedBlkTreeDestroy (tRedBlkTree T, tRedBlkKeyFreeFn FreeFn, UINT4 u4Arg)
{
    UINT4 u4RedBlkIndex = 0;

    RedBlkTreeDrain (T, FreeFn, u4Arg);

    if (T->b1MutualExclusive == TRUE)
    {
        for (u4RedBlkIndex = 0; u4RedBlkIndex < MAX_RED_BLK_TREE_INST; u4RedBlkIndex++)
        {
            if (gu4SemArray[u4RedBlkIndex] == T->SemId)
            {
                gu4SemArray[u4RedBlkIndex] = FALSE;
                break;
            }
        }
        RedBlk_SEM_DEL (T->SemId);
    }

    free (T);
}

UINT4
RedBlkTreeCount (tRedBlkTree T, UINT4 *pu4Count)
{
    if (T == 0)
    {
        return (LPM_FAILURE);
    }

    *pu4Count = T->__tree__.count;
    return (LPM_SUCCESS);
}

UINT4
RedBlkTreeAdd (tRedBlkTree T, tRedBlkElem * key)
{
    UINT4               r;

    if (T == 0)
    {
        return (LPM_FAILURE);
    }

    RedBlk_SEM_TAKE (T);

    if (__ytFind (&(T->__tree__), key) != 0)
    {
        r = LPM_FAILURE;
    }
    else
    {
        NODE             *N =
            (NODE *) ((VOID *) (((unsigned char *) key) + T->u4Offset));
        __ytInsert (&(T->__tree__), N);

        r = LPM_SUCCESS;
    }

    RedBlk_SEM_GIVE (T);

    return r;
}

tRedBlkElem            *
RedBlkTreeRem (struct rbtree * T, tRedBlkElem * key)
{
    NODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RedBlk_SEM_TAKE (T);

    if ((N = __ytFind (&(T->__tree__), key)) == 0)
    {
        node = 0;
    }
    else
    {
        if (T->next_cache == (tRedBlkNode *) N)
        {
            T->next_cache = NULL;
        }

        __ytDelete (&(T->__tree__), N);

        node = NODEPTR (T, N);
    }

    RedBlk_SEM_GIVE (T);

    return node;
}

tRedBlkElem            *
RedBlkTreeGet (struct rbtree * T, tRedBlkElem * key)
{
    NODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RedBlk_SEM_TAKE (T);

    if ((T->next_cache != NULL) && ((*(T->__tree__.compare))
                                    (key,
                                     (T->NodeType ==
                                      RedBlk_NOT_EMBD_NODE) ? T->next_cache->
                                     key : (tRedBlkElem *) ((UINT1 *) T->
                                                        next_cache -
                                                        (T->u4Offset))) == 0))
    {
        N = (NODE *) (T->next_cache);
        node = NODEPTR (T, N);
    }
    else if ((N = __ytFind (&(T->__tree__), key)) == 0)
    {
        node = 0;
    }
    else
    {
        node = NODEPTR (T, N);
    }

    RedBlk_SEM_GIVE (T);

    return node;
}

static INT4
walk (tRedBlkTree T, NODE * N, tRedBlkWalkFn action, void *arg, UINT4 level,
        void *out)
{
    if (N == 0)
    {
        return RedBlk_WALK_CONT;
    }

    if ((N->child[0] == 0) && (N->child[1] == 0))
    {
        if (action (NODEPTR (T, N), leaf, level, arg, out) == RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }
    }
    else
    {
        if (action (NODEPTR (T, N), preorder, level, arg, out) == RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }

        if (walk (T, N->child[0], action, arg, level + 1, out) ==
            RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }

        if (action (NODEPTR (T, N), postorder, level, arg, out) ==
            RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }

        if (walk (T, N->child[1], action, arg, level + 1, out) ==
            RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }

        if (action (NODEPTR (T, N), endorder, level, arg, out) == RedBlk_WALK_BREAK)
        {
            return RedBlk_WALK_BREAK;
        }
    }

    return RedBlk_WALK_CONT;
}

VOID
RedBlkTreeWalk (tRedBlkTree T, tRedBlkWalkFn action, void *arg, void *out)
{
    if (T == 0)
    {
        return;
    }

    RedBlkSemTake (T);
    walk (T, T->__tree__.root.child[0], action, arg, 0, out);
    RedBlkSemGive (T);
}

tRedBlkElem            *
RedBlkTreeGetFirst (tRedBlkTree T)
{
    NODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RedBlk_SEM_TAKE (T);

    if ((N = __ytFirst (&(T->__tree__), T_INORDER)) == 0)
    {
        node = 0;
    }
    else
    {
        node = NODEPTR (T, N);
    }

    if (N != 0)
    {
        T->next_cache = (tRedBlkNode *) N;
    }

    RedBlk_SEM_GIVE (T);

    return node;
}

tRedBlkElem            *
RedBlkTreeGetNext (tRedBlkTree T, tRedBlkElem * key, tRedBlkCompareFn compare)
{
    int                 c;
    NODE             *x, *y, *z;
    void               *r;

    if (T == 0)
    {
        return 0;
    }

    RedBlk_SEM_TAKE (T);

    y = 0;
    x = T->__tree__.root.child[0];

    compare = compare ? compare : (T->__tree__.compare);

    if (T->next_cache != NULL &&
        (*compare) (key, (T->NodeType == RedBlk_NOT_EMBD_NODE) ?
                    T->next_cache->key : (tRedBlkElem *) ((UINT1 *) T->next_cache -
                                                      (T->u4Offset))) == 0)
    {
        x = (NODE *) (T->next_cache);
    }

    if (x)
    {
        compare = compare ? compare : (T->__tree__.compare);

        while (x)
        {
            y = x;

            if ((c = compare (key, NODEPTR (T, x))) < 0)
            {
                x = x->child[0];
            }
            else if (c > 0)
            {
                x = x->child[1];
            }
            else
            {
                break;
            }
        }

        if (x)
        {
            z = __ytNext (x, T_INORDER);
            r = z ? NODEPTR (T, z) : 0;
            if (z != 0)
            {
                T->next_cache = (tRedBlkNode *) z;
            }
        }
        else
        {
            z = (compare (key, NODEPTR (T, y)) > 0) ? __ytNext (y, T_INORDER) : y;
            r = z ? NODEPTR (T, z) : 0;
            if (z != 0)
            {
                T->next_cache = (tRedBlkNode *) z;
            }
        }
    }
    else
    {
        r = 0;
    }

    RedBlk_SEM_GIVE (T);
    return r;
}

void
RedBlkTreeDelete (tRedBlkTree T)
{
    RedBlkTreeDestroy (T, 0, 0);
}

UINT4
RedBlkTreeRemove (struct rbtree *T, tRedBlkElem * key)
{
    UINT4               r;

    if (T == 0)
    {
        return LPM_FAILURE;
    }

    RedBlk_SEM_TAKE (T);

    if (T->NodeType == RedBlk_EMBD_NODE)
    {
        /* In this case, 'key' is guaranteed to be a pointer to a node in the tree */
        if (T->next_cache == (key + T->u4Offset))
        {
            T->next_cache = NULL;
        }
        __ytDelete (&(T->__tree__),
                  (NODE *) ((VOID *) ((UINT1 *) key + T->u4Offset)));
        r = LPM_SUCCESS;
    }

    RedBlk_SEM_GIVE (T);

    return r;
}

VOID
RedBlkSemTake (tRedBlkTree T)
{
    do
    {
        if (T->b1MutualExclusive == TRUE)
        {
            UtilSemTake (T->SemId);
        }
    }
    while (0);
}

VOID
RedBlkSemGive (tRedBlkTree T)
{
    do
    {
        if (T->b1MutualExclusive == TRUE)
        {
            UtilSemGive (T->SemId);
        }
    }
    while (0);
}

