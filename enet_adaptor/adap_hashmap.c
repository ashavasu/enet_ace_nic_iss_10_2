/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#include "adap_hashmap.h"
#include "adap_utils.h"

#define BUCKET(map, key) ((map->hash(key)) % map->num_of_buckets)

struct adap_hashmap {
	ADAP_Uint64 (*hash)(ADAP_Uint64 key);
	EnetHal_Status_t (*flush)(ADAP_Uint64 key, adap_hashmap_element* element);

	adap_linkedlist ** buckets;
    ADAP_Bool thread_safe;
    ADAP_Uint32 capacity;
    ADAP_Uint32 num_of_buckets;
    ADAP_Uint32 size;
};


EnetHal_Status_t adap_hashmap_init(adap_hashmap** map, struct adap_hashmap_params *params)
{
	adap_hashmap* t_map = NULL;
	adap_linkedlist_params list_params;
	ADAP_Uint32 i;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

    if ((params == NULL) || (map == NULL)) {
        rc = ENETHAL_STATUS_NULL_PARAM;
        goto bail;
    }

    if ((params->capacity == 0) || (params->num_of_buckets == 0) ||
        (params->hash == NULL)) {
        rc = ENETHAL_STATUS_INVALID_PARAM;
        goto bail;
    }
    
    t_map = (adap_hashmap*)adap_malloc(sizeof(adap_hashmap));
    if (t_map == NULL) {
    	rc = ENETHAL_STATUS_MEM_ALLOC;
    	goto bail;
    }

    t_map->buckets = (adap_linkedlist **)adap_malloc(params->num_of_buckets * sizeof(adap_linkedlist *));
    if (t_map->buckets == NULL) {
        rc = ENETHAL_STATUS_MEM_ALLOC;
        goto bail;
    }

    t_map->num_of_buckets = params->num_of_buckets;

	list_params.capacity = params->capacity;
	list_params.thread_safe = params->thread_safe;
    for(i=0; i<t_map->num_of_buckets; i++) {
    	rc = adap_linkedlist_create(&(t_map->buckets[i]), &list_params);
    	if (rc != ENETHAL_STATUS_SUCCESS) {
    		goto bail;
    	}
    }

    t_map->thread_safe = params->thread_safe;
    t_map->capacity = params->capacity;
    t_map->hash = params->hash;
    t_map->flush = params->flush;
    t_map->size = 0;

    *map = t_map;
    t_map = NULL;

bail:
	if (t_map != NULL) {
		adap_safe_free(t_map->buckets);
	}
	adap_safe_free(t_map);
    return rc;
}

EnetHal_Status_t adap_hashmap_deinit(adap_hashmap* map)
{
	EnetHal_Status_t rc;
	adap_hashmap_element * hashmap_element;
	adap_linkedlist_element * list_element = NULL;
	ADAP_Uint32 i;

    if (map == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if ((map->size > 0) && (map->flush == NULL)) {
    	return ENETHAL_STATUS_NOT_SUPPORTED;
    }

	for (i = 0; i < map->num_of_buckets; i++) {
		rc = adap_linkedlist_remove_first(map->buckets[i], &list_element);
		while (rc == ENETHAL_STATUS_SUCCESS) {
			hashmap_element = container_of(list_element, adap_hashmap_element, list_item);
			rc = map->flush(hashmap_element->key, hashmap_element);
			if (rc != ENETHAL_STATUS_SUCCESS) {
				return rc;
			}
			rc = adap_linkedlist_remove_first(map->buckets[i], &list_element);
		}

		if (rc != ENETHAL_STATUS_NOT_FOUND) {
			return rc;
		}

		adap_assert(adap_linkedlist_size(map->buckets[i]) == 0);

		rc = adap_linkedlist_destroy(map->buckets[i]);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			return rc;
		}
    }

    adap_safe_free(map);

 	return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_hashmap_insert(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element* value)
{
	EnetHal_Status_t rc;
    ADAP_Uint32 bucket;
    
    if ((map == NULL) || (value == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }
    
    if (map->size == map->capacity) {
    	return ENETHAL_STATUS_LIMIT_REACHED;
    }

    rc = adap_hashmap_find(map, key, NULL);
    if (rc == ENETHAL_STATUS_SUCCESS) {
    	return ENETHAL_STATUS_ALREADY_FOUND;
    }
    adap_assert(rc == ENETHAL_STATUS_NOT_FOUND);

    bucket = BUCKET(map, key);

    value->key = key;
    rc = adap_linkedlist_add_first(map->buckets[bucket], &value->list_item);
    if (rc != ENETHAL_STATUS_SUCCESS) {
    	return rc;
    }
    
    map->size++;

    return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_hashmap_remove(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element** value)
{
	adap_hashmap_element * hashmap_element;
	EnetHal_Status_t rc;
    ADAP_Uint32 bucket;
    
    if (map == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    rc = adap_hashmap_find(map, key, &hashmap_element);
    if (rc != ENETHAL_STATUS_SUCCESS) {
    	return rc;
    }

    bucket = BUCKET(map, key);

    rc = adap_linkedlist_remove(map->buckets[bucket], &hashmap_element->list_item);
    if (rc != ENETHAL_STATUS_SUCCESS) {
    	return rc;
    }

    if (value != NULL) {
    	*value = hashmap_element;
    }

	map->size--;

    return ENETHAL_STATUS_SUCCESS;
}

ADAP_Uint32 adap_hashmap_size(adap_hashmap* map)
{
    if (map == NULL) {
        return 0;
    }

    return map->size;
}


EnetHal_Status_t adap_hashmap_find(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element **value)
{
	adap_hashmap_element * hashmap_element;
	adap_linkedlist_element * list_element = NULL;
	EnetHal_Status_t rc;
    ADAP_Uint32 bucket;
    
    if (map == NULL) {
        return ENETHAL_STATUS_NULL_PARAM;
    }
    
    bucket = BUCKET(map, key);

    rc = adap_linkedlist_iter(map->buckets[bucket], &list_element);
    while(rc == ENETHAL_STATUS_SUCCESS) {
    	hashmap_element = container_of(list_element, adap_hashmap_element, list_item);
    	if (hashmap_element->key == key) {
    		if (value != NULL) {
    			*value = hashmap_element;
    		}
    		return ENETHAL_STATUS_SUCCESS;
    	}
    	rc = adap_linkedlist_iter(map->buckets[bucket], &list_element);
    }

    return rc;
}

