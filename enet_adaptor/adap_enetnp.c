/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_enetnp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_service_config.h"
#include "adap_enetConvert.h"
#include "enetadaptor.h"
#include "adap_acl.h"
#include "adap_vlanminp.h"
#include "EnetHal_L2_Api.h"

MEA_Status ethernity_aricent_init (void);

ADAP_Int32 EnetHal_HwSetPortEgressStatus (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1EgressEnable)
{
    ADAP_UNUSED_PARAM (u4IfIndex);
    ADAP_UNUSED_PARAM (u1EgressEnable);
    ADAP_Int32 ret = ENET_SUCCESS;
    MEA_Port_t enetPort=0;

	ret = MeaAdapGetPhyPortFromLogPort(u4IfIndex,&enetPort);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		ret = ENET_FAILURE;
	}
	else if(u1EgressEnable == ADAP_ENABLE)
    {
    	// at enable set ingress first
    	if(MeaAdapSetIngressPort (enetPort,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",u4IfIndex);
    		ret = ENET_FAILURE;
    	}
    	if(MeaAdapSetEgressPort (enetPort,ADAP_TRUE,ADAP_TRUE) != MEA_OK)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",u4IfIndex);
    		ret = ENET_FAILURE;
    	}
    }
    else if(u1EgressEnable == ADAP_DISABLE)
	{
    	// at disable set egress first
    	if(MeaAdapSetEgressPort (enetPort,ADAP_FALSE,ADAP_FALSE) != MEA_OK)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",u4IfIndex);
    		ret = ENET_FAILURE;
    	}
    	if(MeaAdapSetIngressPort (enetPort,ADAP_FALSE,ADAP_FALSE) != MEA_OK)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",u4IfIndex);
    		ret = ENET_FAILURE;
    	}
	}
	//save the current state
	setPortSpape(u4IfIndex,(eAdap_PortStatus)u1EgressEnable);

    return ret;
}


ADAP_Int32 EnetHal_HwSetPortStatsCollection (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1StatsEnable)
{
	MEA_Port_t						enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

	if(u1StatsEnable==1) /* enable */
	{
		if (MEA_API_Clear_Counters_RMON(MEA_UNIT_0, enetPort) != MEA_OK)
		{
			return ENET_FAILURE;
		}
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetPortMode (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value)
{
	MEA_Interface_Entry_dbt     	entry;
	MEA_Port_t						enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

    if (MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to MEA_API_Get_Interface_ConfigureEntry\r\n");
         return ENET_FAILURE;
     }

    entry.speed=0;
    entry.autoneg=(i4Value==ENET_AUTO)?1:0;

    if (MEA_API_Set_Interface_ConfigureEntry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to MEA_API_Get_Interface_ConfigureEntry\r\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetPortDuplex (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value)
{
    ADAP_UNUSED_PARAM (u4IfIndex);
    ADAP_UNUSED_PARAM (i4Value);
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetPortSpeed (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value)
{
	MEA_Interface_Entry_dbt     entry;
	MEA_Port_t					enetPort;
	ADAP_Uint32						enetSpeed;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

	switch(i4Value)
	{
		case ENET_10MBPS:
			enetSpeed = 2;
		break;
		case ENET_100MBPS:
			enetSpeed = 1;
		break;
		case ENET_1GB:
		case ENET_10GB:
			enetSpeed = 0;
			break;
		case ENET_40GB:
		case ENET_56GB:
		case ENET_2500MBPS:
        default:
			printf("Unsupported port speed %d \n", i4Value);
			return ENET_FAILURE;
	}

    if (MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to MEA_API_Get_Interface_ConfigureEntry\r\n");
         return ENET_FAILURE;
    }

    if(entry.interfaceTyps == MEA_INTERFACETYPE_SINGLE10G)
    {
    	if(i4Value != ENET_10GB)
    	{
        	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Port:%d is 10G, wrong port speed value:%d \r\n", u4IfIndex);
    		return ENET_FAILURE;
    	}else
    	{
    		/*currently no need to configure the HW*/
    		return ENET_SUCCESS;
    	}
    }

    entry.speed=enetSpeed;
    entry.autoneg=0;

    if (MEA_API_Set_Interface_ConfigureEntry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to MEA_API_Get_Interface_ConfigureEntry\r\n");
        return ENET_FAILURE;
    }


    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetPortFlowControl (ADAP_Uint32 u4IfIndex, ADAP_Int32 u1FlowCtrlEnable)
{
 	MEA_IngressPort_Entry_dbt 	Ingress_entry;
	MEA_Port_t					enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

	if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &Ingress_entry) != MEA_OK)
	{
		return ENET_FAILURE;
	}

	Ingress_entry.ingress_fifo.val.pause_enable = (u1FlowCtrlEnable==ENET_STATUS_ENABLE)? MEA_TRUE:MEA_FALSE;

	if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &Ingress_entry) != MEA_OK)
	{
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetRateLimitingValue (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1PacketType,ADAP_Int32 i4RateLimitVal)
{
	//MEA_IngressPort_Entry_dbt 	    entry;
	//MEA_Policer_Entry_dbt			*pPolicer;
	//MEA_Port_t						port, enetPort, startVal, limitVal ;
	MEA_IngressPort_PolicerType_te	enetPolicerType;
	tServiceDb 						*pServiceInfo=NULL;
	ADAP_Uint16						flowPolicerProfile;
	mea_Ingress_Flow_policer_dbt Flow_policer_entry;

	switch(u1PacketType)
	{
		case ENET_RATE_DLF:
			enetPolicerType = MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN;
		break;
		case ENET_RATE_BCAST:
			enetPolicerType = MEA_INGRESS_PORT_POLICER_TYPE_BC;
		break;
		case ENET_RATE_MCAST:
			enetPolicerType = MEA_INGRESS_PORT_POLICER_TYPE_MC;
		break;
			default:
				printf("EnetHal_HwSetRateLimitingValue ERROR u1PacketType:%d \n",u1PacketType );
				return ENET_FAILURE;
	}

	//Update Flow policer profile with new limit value
	if(adap_GetFlowPolicerProfile(u4IfIndex, &flowPolicerProfile) == ENET_FAILURE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_GetFlowPolicerProfile ERROR: failed to get flowMarkProfile from port:%d\n", u4IfIndex);
		return ENET_FAILURE;
	}

    if (MEA_API_Get_Ingress_flow_policer(MEA_UNIT_0, flowPolicerProfile, &Flow_policer_entry) != MEA_OK)
     {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Ingress_flow_policer - profId:%d\r\n", flowPolicerProfile);
		return ENET_FAILURE;
     }

		Flow_policer_entry.policer_vec[enetPolicerType].sla_params.CIR = i4RateLimitVal*8;
		Flow_policer_entry.policer_vec[enetPolicerType].sla_params.EIR = 0;
		Flow_policer_entry.policer_vec[enetPolicerType].sla_params.CBS = 32000;
		Flow_policer_entry.policer_vec[enetPolicerType].sla_params.EBS = 0;

	if (MEA_API_Set_Ingress_flow_policer(MEA_UNIT_0, flowPolicerProfile, &Flow_policer_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Ingress_flow_policer - profId:%d\r\n", flowPolicerProfile);
		return ENET_FAILURE;
	}

	//Look for all services related to the u4IfIndex and map FlowPolicer profile to each service
    pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
	while(pServiceInfo != NULL)
	{
		if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
		{
			if(MeaDrvSetFlowPolicerProfile (u4IfIndex, pServiceInfo->serviceId, flowPolicerProfile) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set flowMarkProfile:%d to service:%d\r\n", flowPolicerProfile, pServiceInfo->serviceId);
				return ENET_FAILURE;
			}
		}
		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
    }
#if 0
	if (u4IfIndex != 0)
	{
		if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
			return ENET_FAILURE;
	    }
		startVal = enetPort;
		limitVal = enetPort + 1;
	}
	else
	{
		enetPort = MEA_MAX_PORT_NUMBER + 1;
		startVal = 0;
		limitVal = MEA_MAX_PORT_NUMBER+1;  /*MEA_NUM_OF_ELEMENTS(MEA_IngressPort_Table); */
	}


	for (port=startVal;port<limitVal;port++)
	{

		if (MEA_API_Get_IsPortValid(port,MEA_PORTS_TYPE_EGRESS_TYPE.MEA_TRUE)==MEA_FALSE) {
            continue;
		}

		if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, port, &entry) != MEA_OK)
		{
			return ENET_FAILURE;
		}
		int policerIndex;

		if(enetPolicerType == MEA_INGRESS_PORT_POLICER_TYPE_TOTAL)
		{
			for(policerIndex=0;policerIndex<MEA_INGRESS_PORT_POLICER_TYPE_TOTAL;policerIndex++)
			{
				pPolicer = &entry.policer_vec[policerIndex].sla_params;
				printf("EnetHal_HwSetRateLimitingValue Old CBS:%d EBS:%d CIR:%d  EIR:%d \n",
						(unsigned int)(pPolicer->CBS),(unsigned int)(pPolicer->EBS),
						(unsigned int)(pPolicer->CIR),(unsigned int)(pPolicer->EIR) );

				if(i4RateLimitVal)
					entry.policer_vec[policerIndex].tmId_enable=MEA_TRUE;
				else
					entry.policer_vec[policerIndex].tmId_enable=MEA_FALSE;

				pPolicer->CIR = i4RateLimitVal*8; //*1000;
				pPolicer->CBS = 3000; //64000;
				/* EIR and EBS must be zero if only one bucket support */

				pPolicer->EIR = 0;
				pPolicer->EBS = 0;

				printf("EnetHal_HwSetRateLimitingValue New CBS:%d EBS:%d CIR:%d  EIR:%d \n",
						(unsigned int)(pPolicer->CBS),(unsigned int)(pPolicer->EBS),
						(unsigned int)(pPolicer->CIR),(unsigned int)(pPolicer->EIR) );
			}
		}
		else
		{
			pPolicer = &entry.policer_vec[enetPolicerType].sla_params;
			printf("EnetHal_HwSetRateLimitingValue Old CBS:%d EBS:%d CIR:%d  EIR:%d \n",
					(unsigned int)(pPolicer->CBS),(unsigned int)(pPolicer->EBS),
					(unsigned int)(pPolicer->CIR),(unsigned int)(pPolicer->EIR) );
			if(i4RateLimitVal)
				entry.policer_vec[enetPolicerType].tmId_enable = MEA_TRUE;
			else
				entry.policer_vec[enetPolicerType].tmId_enable = MEA_FALSE;

			pPolicer->CIR = i4RateLimitVal*8; //*1000;
			pPolicer->CBS = 3000; //64000;
			/* EIR and EBS must be zero if only one bucket support */

			pPolicer->EIR = 0;
			pPolicer->EBS = 0;

			printf("EnetHal_HwSetRateLimitingValue New CBS:%d EBS:%d CIR:%d  EIR:%d \n",
					(unsigned int)(pPolicer->CBS),(unsigned int)(pPolicer->EBS),
					(unsigned int)(pPolicer->CIR),(unsigned int)(pPolicer->EIR) );
		}

		if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, port, &entry) != MEA_OK)
		{
			return ENET_FAILURE;
		}
	}
#endif

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_driver_deinit(void)
{
       ENET_Lock();

       if (MEA_API_Delete_all_entities(MEA_UNIT_0) != MEA_OK)
       {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_driver_deinit: MEA_API_Delete_all_entities failed\n");
               goto fail;
       }

       if (MEA_Platform_ResetDevice(MEA_UNIT_0) != MEA_OK)
       {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_driver_deinit: MEA_Platform_ResetDevice failed\n");
               goto fail;
       }

       if (MEA_API_Conclude(MEA_UNIT_0) != MEA_OK)
       {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_driver_deinit: MEA_API_Conclude failed\n");
               goto fail;
       }

       ENET_Unlock();

       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_driver_deinit: Ethernity DeInit Done\n");

       return ENET_SUCCESS;

fail:
       ENET_Unlock();
       return ENET_FAILURE;
}



void adap_HwRestartSystem (void)
{
	adap_setProcessTerminated();

	//TODO wait_on_terminate_periodic_func();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IssHwRestartSystem - rmon periodic thread terminate\r\n");
	//TODO wait_for_end_cfa_threads();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IssHwRestartSystem - cpa periodic thread terminate\r\n");
	//TODO wait_for_end_cfm_threads();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IssHwRestartSystem - cfm periodic thread terminate\r\n");

    ENET_Lock();

    if (MEA_API_Delete_all_entities(MEA_UNIT_0) != MEA_OK)
    {
		printf("adap_HwRestartSystem -  MEA_API_Delete_all_entities failed\n");
        ENET_Unlock();
		return;
	}

    if (MEA_API_Conclude(MEA_UNIT_0) != MEA_OK)
    {
		printf("adap_HwRestartSystem -  MEA_API_Conclude failed\n");
        ENET_Unlock();
		return;
	}

    EnetAdaptorInit(SW_ENET_NIC_BOARD);
#ifdef ENET_WITH_THIRD_PARTY
    if (ethernity_aricent_init() != MEA_OK)
    {
		printf("adap_HwRestartSystem -  ethernity_aricent_init failed\n");
        ENET_Unlock();
		return;
	}
#endif
    ENET_Unlock();

    return;
}


ADAP_Int32 EnetHal_HwGetPortDuplex (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortDuplexStatus)
{
	MEA_Port_t		enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

	if (MEA_API_Get_IsPortValid(enetPort,MEA_PORTS_TYPE_INGRESS_TYPE,MEA_TRUE)==MEA_TRUE)
	{
		*pi4PortDuplexStatus = ENET_FULLDUP;
	}
	else
		return ENET_FAILURE;

    return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_HwGetPortSpeed (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortSpeed)
{
    MEA_IngressPort_Entry_dbt entry;
    MEA_Port_t		enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

    if (MEA_API_Get_IsPortValid(enetPort,MEA_PORTS_TYPE_INGRESS_TYPE,MEA_TRUE)==MEA_TRUE)
    {
        if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) == MEA_OK)
        {
            switch(entry.GigaDisable)
            {
                case MEA_INGRESSPORT_GIGA_DISABLE_10M:
                    *pi4PortSpeed = ENET_10MBPS;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_HwGetPortSpeed set to ISS_10MBPS\r\n");
                break;
                case MEA_INGRESSPORT_GIGA_DISABLE_100M:
                    *pi4PortSpeed = ENET_100MBPS;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_HwGetPortSpeed set to ISS_100MBPS\r\n");
                break;
                case MEA_INGRESSPORT_GIGA_DISABLE_GIGA:
                    *pi4PortSpeed = ENET_1GB;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_HwGetPortSpeed set to ISS_1GB\r\n");
                break;
                case MEA_INGRESSPORT_GIGA_DISABLE_10G:
                    *pi4PortSpeed = ENET_10GB;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_HwGetPortSpeed set to ISS_10GB\r\n");
                break;
                case MEA_INGRESSPORT_GIGA_DISABLE_2dot5G:
                    *pi4PortSpeed = ENET_2500MBPS;
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_HwGetPortSpeed set to ISS_2500MBPS\r\n");
                break;
                default:
                	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_HwGetPortSpeed: Unsupported port speed %d \n", entry.GigaDisable);
                    return ENET_FAILURE;
            }
            return ENET_SUCCESS;
        }
    }
    else
    {
    	return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"exit EnetHal_HwGetPortSpeed\r\n");


    return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_HwGetPortFlowControl (ADAP_Uint32 u4IfIndex, ADAP_Int32 *pi4PortFlowControl)
{
	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t		enetPort;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, enetPort);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
    }

	if (MEA_API_Get_IsPortValid(enetPort,MEA_PORTS_TYPE_INGRESS_TYPE,MEA_TRUE)==MEA_TRUE)
	{
		if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) == MEA_OK)
		{
			*pi4PortFlowControl = entry.ingress_fifo.val.pause_enable;
			return ENET_SUCCESS;
		}
	}
	else
		return ENET_FAILURE;

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwGetLearnedMacAddrCount (ADAP_Int32 *pi4LearnedMacAddrCount)
{
    *pi4LearnedMacAddrCount = 0;

    MEA_Counters_Forwarder_dbt Forwarder_Counters;

    if(MEA_API_Collect_Counters_Forwarder(MEA_UNIT_0) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Collect_Counters_Forwarder failed !!! \n");
        return ENET_FAILURE;
    }

    if (MEA_API_Get_Counters_Forwarder(MEA_UNIT_0, &Forwarder_Counters) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Counters_Forwarder failed !!! \n");
        return ENET_FAILURE;
    }
    //Alexw 14-Apr-2018 learn by HW
    *pi4LearnedMacAddrCount = Forwarder_Counters.learn_dynamic.s.lsw; /* Only least 32 bytes are returned */

    if (MEA_API_Clear_Counters_Forwarder(MEA_UNIT_0) != MEA_OK)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Clear_Counters_Forwarder failed !!! \n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static void* enetTimerHandler(void *arg)
{
	ADAP_UNUSED_PARAM (arg);


#ifdef	MEA_NAT_DEMO
	tTcpUdpTable *pTcpTable=NULL;
	 MEA_Counters_PM_dbt entry;
#endif
	while(1)
	{
		if(MEA_API_Collect_Counters_PMs_Block(MEA_UNIT_0,0) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Collect_Counters_PMs_Block failed !!! \n");
		}
#ifdef	MEA_NAT_DEMO
		pTcpTable =  getFirstTcpUdpTable();
		while(pTcpTable)
		{
			if(pTcpTable->handle_by_cpu == ADAP_FALSE)
			{
				if(MEA_API_Get_Counters_PM(MEA_UNIT_0,pTcpTable->HostToNetpmId,&entry) == MEA_OK)
				{
					pTcpTable->lastCountHostToNet = entry.rate_green_fwd_pkts.val;
				}
				if(MEA_API_Get_Counters_PM(MEA_UNIT_0,pTcpTable->NetToHostpmId,&entry) == MEA_OK)
				{
					pTcpTable->lastCountNetToHost = entry.rate_green_fwd_pkts.val;
				}
			}
			pTcpTable = getNextTcpUdpTable(pTcpTable);
		}
#endif
		// in case of OVS it need to know the source ip address of ports 104-107

		enetMilisecondSleep(10000);
	}
	return NULL;
}

void init_timer_thread(void)
{
	adap_task_id taskId;
	EnetHal_Status_t status = adap_task_manager_run(enetTimerHandler, NULL, &taskId);
	if(status != ENETHAL_STATUS_SUCCESS)
	{
		printf("failed to run timer task (status = %d)\n", status);
	}
}

#ifdef ENET_WITH_THIRD_PARTY
ADAP_Int32 EnetHal_driver_init(void)
{
	ADAP_Int32 ret=ENET_SUCCESS;
	/* Ethernity Init*/
	printf("EnetHal_driver_init: Ethernity Init\n");
	ethernity_aricent_init();
	printf("EnetHal_driver_init: Ethernity Init Done\n");

	ret = enet_init_adaptor_database();

	return ret;
}
#endif

ADAP_Int32 adap_HwAddMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo)
{
	MEA_Globals_Entry_dbt entry;
	MEA_Port_t           port_cesId;
    MEA_Port_t           dest_src_port;

	if(adap_GetMirroringStatus() == MEA_TRUE)
	{
		printf ("adap_HwAddMirroring: Mirroring is already enabled!\n");
		return ENET_FAILURE;
	}

    if(pMirrorInfo->u1MirrType != ENETHAL_MIRR_PORT_BASED)
	{
		printf ("adap_HwAddMirroring: Unsupported Mirroring type: %d\n", pMirrorInfo->u1MirrType);
		return ENET_FAILURE;
    }

	/* Only Egress mirroring is supported by Ethernity*/
	if(pMirrorInfo->SourceList[0].u1Mode == ENETHAL_MIRR_EGRESS)
	{
		if(MeaAdapGetPhyPortFromLogPort(pMirrorInfo->SourceList[0].u4SourceNo, &port_cesId) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",pMirrorInfo->SourceList[0].u4SourceNo, port_cesId);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", pMirrorInfo->SourceList[0].u4SourceNo);
			return ENET_FAILURE;
	    }
	}
	else
	{
		printf ("adap_HwAddMirroring: Unsupported Mirroring mode: %d\n", pMirrorInfo->SourceList[0].u1Mode);
		return ENET_FAILURE;
	}

	if(MeaAdapGetPhyPortFromLogPort(pMirrorInfo->u4DestList[0], &dest_src_port) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",pMirrorInfo->u4DestList[0], dest_src_port);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", pMirrorInfo->u4DestList[0]);
		return ENET_FAILURE;
    }

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
		 printf("MEA_API_Get_Globals_Entry failed\n");
		 return ENET_FAILURE;
    }

    entry.if_mirror_ces_port.val.enable         = MEA_TRUE;
    entry.if_mirror_ces_port.val.port_ces_type  = 0; //port
	/* Ethernity suppports only one source and one destination ports for mirroring */
    entry.if_mirror_ces_port.val.port_cesId     = port_cesId;
    entry.if_mirror_ces_port.val.dest_src_port =  dest_src_port;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
		 printf("MEA_API_Set_Globals_Entry failed\n");
		 return ENET_FAILURE;
    }

	//Update the database with the new Mirroring status
	adap_SetMirroringStatus(MEA_TRUE);

    return ENET_SUCCESS;
}

ADAP_Int32 adap_HwRemoveMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo)
{
	MEA_Globals_Entry_dbt entry;
	MEA_Port_t           port_cesId;
    MEA_Port_t           dest_src_port;

	if(adap_GetMirroringStatus() == MEA_FALSE)
	{
		printf ("adap_HwRemoveMirroring: Mirroring is disabled!\n");
		return ENET_FAILURE;
	}

    if(pMirrorInfo->u1MirrType != ENETHAL_MIRR_PORT_BASED)
	{
		printf ("adap_HwRemoveMirroring: Unsupported Mirroring type: %d\n", pMirrorInfo->u1MirrType);
		return ENET_FAILURE;
    }

	/* Only Egress mirroring is supported by Ethernity*/
	if(pMirrorInfo->SourceList[0].u1Mode == ENETHAL_MIRR_EGRESS)
	{
		if(MeaAdapGetPhyPortFromLogPort(pMirrorInfo->SourceList[0].u4SourceNo, &port_cesId) == ENET_SUCCESS)
		{
			printf("logPort:%d, enetPortId:%d\n",pMirrorInfo->SourceList[0].u4SourceNo, port_cesId);
		}
		else
		{
			printf("Unable to convert u4IfIndex:%d to the ENET portId\n", pMirrorInfo->SourceList[0].u4SourceNo);
			return ENET_FAILURE;
	    }
	}
	else
	{
		printf ("adap_HwRemoveMirroring: Unsupported Mirroring mode: %d\n", pMirrorInfo->SourceList[0].u4SourceNo);
		return ENET_FAILURE;
	}

	if(MeaAdapGetPhyPortFromLogPort(pMirrorInfo->u4DestList[0], &dest_src_port) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",pMirrorInfo->u4DestList[0], dest_src_port);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", pMirrorInfo->u4DestList[0]);
		return ENET_FAILURE;
    }

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
		 printf("MEA_API_Get_Globals_Entry failed\n");
		 return ENET_FAILURE;
    }

    entry.if_mirror_ces_port.val.enable         = MEA_FALSE;
    entry.if_mirror_ces_port.val.port_ces_type  = 0; //port
	/* Ethernity suppports only one source and one destination ports for mirroring */
    entry.if_mirror_ces_port.val.port_cesId     = port_cesId;
    entry.if_mirror_ces_port.val.dest_src_port =  dest_src_port;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
		 printf("MEA_API_Set_Globals_Entry failed\n");
		 return ENET_FAILURE;
    }

	//Update the database with the new Mirroring status
	adap_SetMirroringStatus(MEA_FALSE);
	printf("Mirroring is removed - Src Port: %d Dest Port: %d \n", pMirrorInfo->SourceList[0].u4SourceNo, pMirrorInfo->u4DestList[0]);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_HwSetMirroring (tEnetHal_HwMirrorInfo * pMirrorInfo)
{
    if (pMirrorInfo == NULL){
        printf ("EnetHal_HwSetMirroring: Mirror info is NULL\r\n");
        return ENET_FAILURE;
    }

	switch ((pMirrorInfo->u1Action)) {
	case ENETHAL_MIRR_ADD_DONE:
		if (adap_HwAddMirroring(pMirrorInfo) != ENET_SUCCESS)
		{
			 printf("EnetHal_HwSetMirroring failed\n");
			 return ENET_FAILURE;
		}
	break;
	case ENETHAL_MIRR_REMOVE_DONE:
		if (adap_HwRemoveMirroring(pMirrorInfo) != ENET_SUCCESS)
		{
			 printf("EnetHal_HwSetMirroring failed\n");
			 return ENET_FAILURE;
		}
	break;
	default:
		printf ("EnetHal_HwSetMirroring: Unrecognized mirror action(%d)\r\n", pMirrorInfo->u1Action );
		return ENET_FAILURE;
    }

   return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_HwSetPortMaxMacAddr (ADAP_Uint32 u4IfIndex, ADAP_Int32 i4Value)
{
    MEA_Limiter_t limiterId;

    if (AdapGetLimiterIdByPort(u4IfIndex, &limiterId) == ENET_SUCCESS)
    {
        // Updating existing mac limiter
        if(adap_UpdateMacLimiterProfile(limiterId, i4Value, MEA_LIMIT_ACTION_LAST)
                != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed update limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }
    else
    {
        // Creating new limiterId
        if(adap_CreateMacLimiterProfile(i4Value, MEA_LIMIT_ACTION_DISCARD, &limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }

        // Allocating new profile
        if(AdapAllocLimiterProf(0, u4IfIndex, LIMITER_PROF_IFINDEX, limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter profile id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }

    // Updating limiterid on services
    if(adap_SetLimiterIdToPort(u4IfIndex, limiterId)
                  != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to set limiter id %d to port\n",
            __FUNCTION__, limiterId);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_HwSetPortMaxMacAction (ADAP_Uint32 u4IfIndex, eEnetHal_PortCtrlAction i4Value)
{
    MEA_Limiter_t limiterId;
    MEA_Limit_Action_te action_type = (i4Value == ENET_DROPPKT) ? MEA_LIMIT_ACTION_DISCARD : MEA_LIMIT_ACTION_SEND_TO_CPU;
    if (AdapGetLimiterIdByPort(u4IfIndex, &limiterId) == ENET_SUCCESS)
    {
        // Updating existing mac limiter
        if(adap_UpdateMacLimiterProfile(limiterId, 0, action_type)
                != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed update limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }
    else
    {
        // Creating new limiterId
        if(adap_CreateMacLimiterProfile(0, action_type, &limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }

        // Allocating new profile
        if(AdapAllocLimiterProf(0, u4IfIndex, LIMITER_PROF_IFINDEX, limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter profile id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }

    // Updating limiterid on services
    if(adap_SetLimiterIdToPort(u4IfIndex, limiterId)
                  != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to set limiter id %d to port\n",
            __FUNCTION__, limiterId);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}
