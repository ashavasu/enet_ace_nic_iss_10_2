/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"


/*
 * adap_vxlan_info.c
 *
 *  Created on: Mar 14, 2017
 *      Author: root
 */
//#define ADAP_DISABLE_GET_VXLAN 1  
MEA_Status ADAP_API_Get_Vxlan_snooping_Info(tAdapVxlanSnoop *vxLanSnoopArr,MEA_Uint32 *max_entries)
{
    MEA_Uint32 i;
    MEA_Uint32 number_words;
    MEA_Snoop_info_dbt infoEntry;
    MEA_Snoop_Entry_dbt Entry;


    number_words = (*max_entries);
    (*max_entries)=0;

    adap_memset(&infoEntry, 0, sizeof(infoEntry));
#ifdef ADAP_DISABLE_GET_VXLAN
    return MEA_OK;  //Alex  only for Debuging
#endif

    if (!MEA_VXLAN_SUPPORT) {
    	return MEA_OK;
    }

    if(MEA_API_Get_Vxlan_snooping_Info(MEA_UNIT_0, &infoEntry) != MEA_OK)
    {
        return MEA_ERROR;
    }
    for (i = 0; i < infoEntry.number_words; i++)
    {
		adap_memset(&Entry, 0, sizeof(Entry));
		if (MEA_API_Get_Vxlan_snooping_Entry(MEA_UNIT_0, &Entry) != MEA_OK)
		{
			return MEA_ERROR;
		}
		if(number_words>(*max_entries))
		{
			printf("found vxlan snoop\r\n");
			printf("=================\r\n");
			printf("sourcePort:%d\r\n",Entry.src_port);
			printf("internal ip address:%x\r\n",Entry.src_ipv4_int);
			printf("external ip address:%x\r\n",Entry.src_ipv4_ext);
			printf("vni:%d\r\n",Entry.vni);
			printf("vlanId:%d\r\n",Entry.vlan_vxlan_int);
			printf("source port:%d\r\n",Entry.src_port);
			printf("source port:%x:%x:%x:%x:%x:%x\r\n",
					Entry.sa_mac_ext.b[0],
					Entry.sa_mac_ext.b[1],
					Entry.sa_mac_ext.b[2],
					Entry.sa_mac_ext.b[3],
					Entry.sa_mac_ext.b[4],
					Entry.sa_mac_ext.b[5]);


			vxLanSnoopArr[(*max_entries)].src_port=Entry.src_port;
			vxLanSnoopArr[(*max_entries)].src_ipv4_int=Entry.src_ipv4_int;
			vxLanSnoopArr[(*max_entries)].src_ipv4_ext=Entry.src_ipv4_ext;
			adap_mac_from_arr(&vxLanSnoopArr[(*max_entries)].sa_mac_ext, Entry.sa_mac_ext.b);
			vxLanSnoopArr[(*max_entries)].vlan = (ADAP_Uint16)( Entry.vlan_vxlan_int & 0x0000FFFF);
			vxLanSnoopArr[(*max_entries)].vni = Entry.vni;

			(*max_entries)++;
		}
		else
		{
			break;
		}

    }

    return MEA_OK;
}

