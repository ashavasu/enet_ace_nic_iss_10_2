/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

#include "mea_api.h"
#include "adap_index_pool.h"
#include "adap_linkedlist.h"
#include "adap_srv_rm.h"
#include "adap_utils.h"
#include "adap_logger.h"
#include "adap_database.h"
#include "adap_enetConvert.h"

#include "adap_ipsec.h"


#define ADAP_IPSEC_MAX_OUT_POLICY  32
#define ADAP_IPSEC_MAX_IN_POLICY   32

#define ADAP_IPSEC_MAX_SA          32

struct adap_ipsec_sa_descriptor {
	adap_ipsec_sa user_sa;
	ADAP_Bool attached;
	adap_ipsec_policy_id policy_id;
	MEA_Uint32 mea_ipsec_esp_id;
};

struct adap_ipsec_policy_descriptor {
	adap_ipsec_policy user_policy;
	ADAP_Uint8 rule_id;
	ADAP_Uint8 action_id;
	ADAP_Bool attached;
	adap_ipsec_sa_id sa_id;
};

/* initialization flag */
static ADAP_Bool g_initialized = ADAP_FALSE;

static adap_index_pool *out_policy_pool = NULL;
static adap_index_pool *in_policy_pool = NULL;
static adap_index_pool *sa_pool = NULL;
static struct adap_ipsec_policy_descriptor *policy_db[ADAP_IPSEC_MAX_OUT_POLICY + ADAP_IPSEC_MAX_IN_POLICY] = {NULL};
static struct adap_ipsec_sa_descriptor *sa_db[ADAP_IPSEC_MAX_SA] = {NULL};

static MEA_Uint16 tft_profile_mask_id;


/*****************************************************************************/
/* Internal functions' prototypes                                            */
/*****************************************************************************/
static
MEA_IPSec_Security_Type_t EnetHal_ipsec_get_security_type(adap_auth_algorithm auth_alg,
		                                                  adap_encr_algorithm encr_alg);

static
adap_ipsec_policy_direction EnetHal_ipsec_get_direction(adap_ipsec_policy_id policy_id);

static
EnetHal_Status_t EnetHal_ipsec_alloc_policy_id(adap_ipsec_policy_direction dir, adap_ipsec_policy_id *policy_id);

static
EnetHal_Status_t EnetHal_ipsec_free_policy_id(adap_ipsec_policy_id policy_id);

static
EnetHal_Status_t EnetHal_ipsec_get_out_service(adap_ipsec_policy *policy, MEA_Service_t *service_id);

static
EnetHal_Status_t EnetHal_ipsec_get_in_service(ADAP_Uint32 ifindex, ADAP_Uint32 sip, ADAP_Uint32 spi, ADAP_Uint32 mea_ipsec_esp_id, MEA_Service_t *service_id);

static
EnetHal_Status_t EnetHal_ipsec_get_service_action_table(MEA_Service_t service_id, ADAP_Uint32 *action_table_id, ADAP_Bool forced);

static
EnetHal_Status_t EnetHal_ipsec_get_service_qos_table(MEA_Service_t service_id, MEA_TFT_t *qos_table_id, ADAP_Bool forced);


/*****************************************************************************/
/* API implementation                                                        */
/*****************************************************************************/
EnetHal_Status_t EnetHal_ipsec_init(adap_ipsec_params *params)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	struct adap_srv_rm_params rm_params;
	struct adap_index_pool_params pool_params;
	MEA_TFT_mask_data_dbt tft_mask_data_dbt;
	MEA_Globals_Entry_dbt globals_entry;
	MEA_IngressPort_Entry_dbt ingress_port_entry;
	MEA_EgressPort_Entry_dbt egress_port_entry;
	MEA_Status mea_status;

	if (g_initialized == ADAP_FALSE) {

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG, "IPSEC init\n");

		/* Initialize out policy pool */
		pool_params.thread_safe = ADAP_FALSE;
		pool_params.capacity = ADAP_IPSEC_MAX_OUT_POLICY;
		rc = adap_index_pool_init(&out_policy_pool, &pool_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (out policy pool).\n");
			goto bail;
		}

		/* Initialize in policy pool */
		pool_params.thread_safe = ADAP_FALSE;
		pool_params.capacity = ADAP_IPSEC_MAX_IN_POLICY;
		rc = adap_index_pool_init(&in_policy_pool, &pool_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (in policy pool).\n");
			goto bail;
		}

		/* Initialize SA pool */
		pool_params.thread_safe = ADAP_FALSE;
		pool_params.capacity = ADAP_IPSEC_MAX_SA;
		rc = adap_index_pool_init(&sa_pool, &pool_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (SA pool).\n");
			goto bail;
		}

		rm_params.max_action_tables = 16 * 1024;
		rm_params.max_entries_per_action_table = 8;
		rm_params.max_qos_tables = 64;
		rm_params.max_entries_per_qos_table = 32;
		rc = adap_srv_rm_init(&rm_params);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		/* Initialize profile mask */
		mea_status = MEA_API_TFT_Prof_Mask_Create(MEA_UNIT_0, &tft_profile_mask_id);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (TFT mask create).\n");
			goto bail;
		}

		tft_mask_data_dbt.mask = 0xffffffff;
		tft_mask_data_dbt.en_rule_type = ADAP_TRUE;
		tft_mask_data_dbt.rule_type = 0x7;
		mea_status = MEA_API_TFT_Prof_Mask_Set(MEA_UNIT_0, tft_profile_mask_id, &tft_mask_data_dbt);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (TFT mask set).\n");
			goto bail;
		}

	    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (globals get).\n");
			goto bail;
	    }

	    adap_mac_to_arr(&params->nvgre_smac, globals_entry.nvgre_info.Gre_src_mac.b);
	    globals_entry.nvgre_info.outer_vlanId = params->nvgre_vlan;
	    globals_entry.nvgre_info.Gre_ttl = params->nvgre_ttl;
	    globals_entry.nvgre_info.Gre_src_Ipv4 = params->nvgre_sip;
	    globals_entry.Ipsec_info.ipsec_ttl = params->ipsec_ttl;
	    globals_entry.Ipsec_info.ipsec_src_Ipv4 = params->my_ip;
	    globals_entry.Local_IP_my_Ipsec = params->my_ip;

	    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (globals set).\n");
			goto bail;
	    }

	    /* activate port 27 */
        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
        		                          27,
										  &ingress_port_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (get ingress port 27).\n");
			goto bail;
        }
        ingress_port_entry.rx_enable = MEA_TRUE;
        ingress_port_entry.crc_check = MEA_FALSE;

        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
        		                          27,
										  &ingress_port_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (set ingress port 27).\n");
			goto bail;
        }

	    /* activate port 27 */
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
        		                         27,
										 &egress_port_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (get egress port 27).\n");
			goto bail;
        }

        egress_port_entry.tx_enable = MEA_TRUE;

        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
        		                         27,
									     &egress_port_entry) != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (set egress port 27).\n");
			goto bail;
        }

        if(MEA_API_NVGRE_Set_Port_Remove_Header(MEA_UNIT_0,
        		                                27,
												MEA_TRUE) != MEA_OK){
			rc = ENETHAL_STATUS_MEA_ERROR;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC init failed (set port 27 with remove header).\n");
			goto bail;
        }

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "IPSEC init done.\n");

		g_initialized = ADAP_TRUE;
	}

bail:
	/* TODO - roll back */
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_deinit(void)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Status mea_status;

	if (g_initialized == ADAP_TRUE) {

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG, "IPSEC deinit\n");

		rc = adap_srv_rm_deinit();
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = adap_index_pool_deinit(out_policy_pool);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC deinit failed (out policy pool).\n");
			goto bail;
		}

		rc = adap_index_pool_deinit(in_policy_pool);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC deinit failed (in policy pool).\n");
			goto bail;
		}

		rc = adap_index_pool_deinit(sa_pool);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC deinit failed (SA pool).\n");
			goto bail;
		}

		mea_status = MEA_API_TFT_Prof_Mask_Delete(MEA_UNIT_0, tft_profile_mask_id);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "IPSEC deinit failed (TFT mask delete).\n");
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "IPSEC deinit done.\n");

		g_initialized = ADAP_FALSE;
	}

bail:
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_create_policy(adap_ipsec_policy *policy,
		                            adap_ipsec_policy_id *policy_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_t service_id;
	ADAP_Uint32 action_table_id;
	MEA_TFT_t qos_table_id;
	MEA_Status mea_status;
	MEA_TFT_data_dbt tft_data_dbt;
	mea_TFT_Action_rule_t tft_action_rule;
	MEA_Action_Entry_Data_dbt action_entry_data;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_arr_entry;
	MEA_EHP_Info_dbt ehp_info_dbt;

	adap_memset(&tft_action_rule, 0, sizeof(mea_TFT_Action_rule_t));
	adap_memset(&action_entry_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&ehp_arr_entry, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&ehp_info_dbt, 0, sizeof(MEA_EHP_Info_dbt));

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <policy_id> and <policy> is not NULL.
	 *        - TODO <policy> parameters are supported & valid.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if ((policy == NULL) || (policy_id == NULL)) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unexpected NULL pointer.\n");
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	/*
	 *   2) Update internal DB:
	 *        - Allocate <policy_id>.
	 *        - Allocate policy in DB.
	 *        - initialize policy descriptor in DB.
	 */

	rc = EnetHal_ipsec_alloc_policy_id(policy->dir, policy_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID allocation failed.\n");
		goto bail;
	}

	policy_db[*policy_id] = (struct adap_ipsec_policy_descriptor *)adap_malloc(sizeof(struct adap_ipsec_policy_descriptor));
	if (policy_db[*policy_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Memory allocation failed.\n");
		rc = ENETHAL_STATUS_MEM_ALLOC;
		goto bail;
	}

	adap_memcpy(&policy_db[*policy_id]->user_policy, policy, sizeof(adap_ipsec_policy));
	policy_db[*policy_id]->attached = ADAP_FALSE;

	/*
	 *   3) Configure MEA (out policies only):
	 *        - Get service using ifindex & vlan_id.
	 *        - Create "Send to CPU" action.
	 *        - Set classification rule with "Send to CPU" action.
	 */

	if (policy->dir == ADAP_IPSEC_POLICY_DIR_OUT) {
		rc = EnetHal_ipsec_get_out_service(policy, &service_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Service not found.\n");
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_action_table(service_id, &action_table_id, ADAP_TRUE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed to retrieve service action group ID.\n");
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_qos_table(service_id, &qos_table_id, ADAP_TRUE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed to retrieve service HPM profile.\n");
			goto bail;
		}

		rc = adap_srv_rm_alloc_action(action_table_id, &policy_db[*policy_id]->action_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed to allocate action ID in group %d.\n", action_table_id);
			goto bail;
		}

		/* Create action */
		tft_action_rule.valid = MEA_TRUE;
		tft_action_rule.action_params = &action_entry_data;
		tft_action_rule.action_params->ed_id_valid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vpValid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vp_val = 127;
		tft_action_rule.EHP_Entry = &ehp_arr_entry;
		tft_action_rule.EHP_Entry->num_of_entries = 1;
		tft_action_rule.EHP_Entry->ehp_info = &ehp_info_dbt;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.stamp_color = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc = 12;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc = 0;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc = 15;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.atm_info.val.all= 0x91000fae;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		MEA_SET_OUTPORT(&tft_action_rule.EHP_Entry->ehp_info[0].output_info, 127);

		mea_status = MEA_API_Create_UEPDN_TEID_ConfigAction(MEA_UNIT_0,
															action_table_id,
															policy_db[*policy_id]->action_id,
															&tft_action_rule);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed to create TFT action rule in action group %d with action ID %d.\n", action_table_id, policy_db[*policy_id]->action_id);
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		tft_data_dbt.TFT_info.Type = MEA_TFT_TYPE_IPV4;
		tft_data_dbt.TFT_info.L2_enable = MEA_FALSE;
		tft_data_dbt.TFT_info.L3_DSCP_valid = MEA_FALSE;
		tft_data_dbt.TFT_info.L3_EtherType = 0;

		if (policy->selectors.dip_mask == 0) {
			tft_data_dbt.TFT_info.Dest_IPv4_valid = MEA_FALSE;
		} else {
			tft_data_dbt.TFT_info.Dest_IPv4_valid = MEA_TRUE;
			tft_data_dbt.TFT_info.Dest_IPv4 = policy->selectors.dip;
			tft_data_dbt.TFT_info.Dest_IPv4_mask = policy->selectors.dip_mask;
		}

		if (policy->selectors.sip_mask == 0) {
			tft_data_dbt.TFT_info.Dest_IPv4_valid = MEA_FALSE;
		} else {
			tft_data_dbt.TFT_info.Source_IPv4_valid = MEA_TRUE;
			tft_data_dbt.TFT_info.Source_IPv4 = policy->selectors.sip;
			tft_data_dbt.TFT_info.Source_IPv4_mask = policy->selectors.sip_mask;
		}

		tft_data_dbt.TFT_info.Ipv6_valid = MEA_FALSE;

		if (policy->selectors.next_proto_mode == ADAP_IPSEC_POLICY_SELECTOR_MODE_ANY) {
			tft_data_dbt.TFT_info.IP_protocol_valid = MEA_FALSE;
			tft_data_dbt.TFT_info.L4_Source_port_valid = MEA_FALSE;
			tft_data_dbt.TFT_info.L4_dest_valid = MEA_FALSE;
		} else {
			tft_data_dbt.TFT_info.IP_protocol_valid = MEA_TRUE;
			tft_data_dbt.TFT_info.IP_protocol = policy->selectors.next_proto;

			if (policy->selectors.next_proto == IPPROTO_ICMP) {
				tft_data_dbt.TFT_info.L4_Source_port_valid = MEA_FALSE;
				tft_data_dbt.TFT_info.L4_dest_valid = MEA_FALSE;
			} else {
				if (policy->selectors.port_selectors.sport_mode == ADAP_IPSEC_POLICY_SELECTOR_MODE_ANY) {
					tft_data_dbt.TFT_info.L4_Source_port_valid = MEA_FALSE;
				} else {
					tft_data_dbt.TFT_info.L4_Source_port_valid = MEA_TRUE;
					tft_data_dbt.TFT_info.L4_from_Source_port = policy->selectors.port_selectors.sport_start;
					tft_data_dbt.TFT_info.L4_end_Source_port = policy->selectors.port_selectors.sport_end;
				}
				if (policy->selectors.port_selectors.dport_mode == ADAP_IPSEC_POLICY_SELECTOR_MODE_ANY) {
					tft_data_dbt.TFT_info.L4_dest_valid = MEA_FALSE;
				} else {
					tft_data_dbt.TFT_info.L4_dest_valid = MEA_TRUE;
					tft_data_dbt.TFT_info.L4_from_Dest_port = policy->selectors.port_selectors.dport_start;
					tft_data_dbt.TFT_info.L4_end_Dest_port = policy->selectors.port_selectors.dport_end;
				}
			}
		}

		tft_data_dbt.TFT_info.Priority_Rule = policy->priority;
		tft_data_dbt.TFT_info.QoS_Index = policy_db[*policy_id]->action_id;

		/* rule_id */
		rc = adap_srv_rm_alloc_qos_entry(qos_table_id, &policy_db[*policy_id]->rule_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		mea_status = MEA_API_TFT_Set_ClassificationRule(MEA_UNIT_0, qos_table_id, policy_db[*policy_id]->rule_id, &tft_data_dbt);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Policy created with ID %d.\n", *policy_id);
	} else {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," In policy is not configured in MEA.\n");
	}

bail:
	/* TODO - rollback */
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_get_policy(adap_ipsec_policy_id policy_id,
		                adap_ipsec_policy *policy)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <policy> is not NULL.
	 *        - <policy_id> exists.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (policy == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unexpected NULL pointer.\n");
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	if (policy_db[policy_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID %d not found.\n", policy_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}


	/*
	 *   2) Retrieve data from DB.
	 */
	adap_memcpy(policy, &policy_db[policy_id]->user_policy, sizeof(adap_ipsec_policy));


bail:
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_destroy_policy(adap_ipsec_policy_id policy_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_t service_id;
	ADAP_Uint32 action_group_id;
	MEA_TFT_t qos_table_id;
	MEA_TFT_data_dbt tft_data_dbt;
	MEA_Status mea_status;

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <policy_id> exists.
	 *        - Policy is not attached to SA.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (policy_db[policy_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID %d not found.\n", policy_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	if (policy_db[policy_id]->attached != ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID %d is attached to an SA (%d). Please detach first.\n", policy_id, policy_db[policy_id]->sa_id);
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}


	/*
	 *   2) Configure MEA:
	 *        - Set classification rule.
	 */

	if (policy_db[policy_id]->user_policy.dir == ADAP_IPSEC_POLICY_DIR_OUT) {
		rc = EnetHal_ipsec_get_out_service(&policy_db[policy_id]->user_policy, &service_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_action_table(service_id, &action_group_id, ADAP_FALSE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_qos_table(service_id, &qos_table_id, ADAP_FALSE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = adap_srv_rm_free_action(action_group_id, policy_db[policy_id]->action_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		/* Destroy action */
		mea_status = MEA_API_Delete_UEPDN_TEID_ConfigAction(MEA_UNIT_0,
				                                            action_group_id,
															policy_db[policy_id]->action_id);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		tft_data_dbt.TFT_info.Type = MEA_TFT_TYPE_DISABLE;
		mea_status = MEA_API_TFT_Set_ClassificationRule(MEA_UNIT_0, qos_table_id, policy_db[policy_id]->rule_id, &tft_data_dbt);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		rc = adap_srv_rm_free_qos_entry(qos_table_id, policy_db[policy_id]->rule_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}
	}

	/*
	 *   3) Update internal DB:
	 *        - Free policy in DB.
	 *        - Free <policy_id>.
	 */

	adap_safe_free(policy_db[policy_id]);
	policy_db[policy_id] = NULL;

	rc = EnetHal_ipsec_free_policy_id(policy_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID %d release failed.\n", policy_id);
		goto bail;
	}

bail:
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_create_sa(adap_ipsec_sa *sa,
		               adap_ipsec_sa_id *sa_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <sa_id> and <sa> is not NULL.
	 *        - TODO <sa> parameters are supported & valid.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if ((sa == NULL) || (sa_id == NULL)) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unexpected NULL pointer.\n");
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	if (EnetHal_ipsec_get_security_type(sa->auth_alg, sa->encr_alg) == MEA_EDITING_IPSec_Security_CONFIDENT_LAST) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Encryption & authentation algorithms combination is not supported.\n");
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}

	/*
	 *   2) Update internal DB:
	 *        - Allocate <sa_id>.
	 *        - Allocate SA in DB.
	 *        - initialize policy descriptor in DB.
	 */

	rc = adap_index_pool_alloc(sa_pool, sa_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		goto bail;
	}

	sa_db[*sa_id] = (struct adap_ipsec_sa_descriptor *)adap_malloc(sizeof(struct adap_ipsec_sa_descriptor));
	if (sa_db[*sa_id] == NULL) {
		rc = ENETHAL_STATUS_MEM_ALLOC;
		goto bail;
	}

	adap_memcpy(&sa_db[*sa_id]->user_sa, sa, sizeof(adap_ipsec_sa));
	sa_db[*sa_id]->attached = ADAP_FALSE;


bail:
	/* TODO - rollback */
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_get_sa(adap_ipsec_sa_id sa_id,
		            adap_ipsec_sa *sa)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <sa> is not NULL.
	 *        - <sa_id> exists.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (sa == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unexpected NULL pointer.\n");
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	if (sa_db[sa_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," SA not found.\n");
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}


	/*
	 *   2) Retrieve data from DB.
	 */
	adap_memcpy(sa, &sa_db[sa_id]->user_sa, sizeof(adap_ipsec_sa));

bail:
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_destroy_sa(adap_ipsec_sa_id sa_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <sa_id> exists.
	 *        - SA is not attached to a policy.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IPsec module not initialized.\n");
		return ENETHAL_STATUS_NOT_INITIALIZED;
	}

	if (sa_db[sa_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," SA ID %d not found.\n", sa_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	if (sa_db[sa_id]->attached != ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," SA ID %d is attached to policy %d. Please detach first.\n", sa_id, sa_db[sa_id]->policy_id);
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}


	/*
	 *   2) Update internal DB:
	 *        - Free SA in DB.
	 *        - Free <sa_id>.
	 */

	adap_safe_free(sa_db[sa_id]);
	sa_db[sa_id] = NULL;

	rc = adap_index_pool_free(sa_pool, sa_id);
	if (rc != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," SA ID %d release failed.\n", sa_id);
		goto bail;
	}


bail:
	return rc;
}

EnetHal_Status_t EnetHal_ipsec_attach_sa(adap_ipsec_policy_id policy_id,
		               adap_ipsec_sa_id sa_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_t service_id;
	ADAP_Uint32 action_table_id;
	MEA_IPSec_dbt mea_ipsec_dbt;
	mea_TFT_Action_rule_t tft_action_rule;
	MEA_Action_Entry_Data_dbt action_entry_data;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_arr_entry;
	MEA_EHP_Info_dbt ehp_info_dbt;
	MEA_Status mea_status;


	adap_memset(&tft_action_rule, 0, sizeof(mea_TFT_Action_rule_t));
	adap_memset(&action_entry_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&ehp_arr_entry, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&ehp_info_dbt, 0, sizeof(MEA_EHP_Info_dbt));


	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <policy_id> exists.
	 *        - <sa_id> exists.
	 *        - SA & policy are not attahced.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (policy_db[policy_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Policy %d doesn't exist.\n", policy_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	if (sa_db[sa_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," SA %d doesn't exist.\n", sa_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	if (policy_db[policy_id]->attached != ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Policy %d already attached (SA %d). Not supported.\n", policy_id, policy_db[policy_id]->sa_id);
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}

	if (sa_db[sa_id]->attached != ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," SA %d already attached (policy %d). Not supported.\n", sa_id, sa_db[sa_id]->policy_id);
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}

	if (policy_db[policy_id]->user_policy.action != ADAP_IPSEC_POLICY_ACTION_PROTECT) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Policy %d is not defined with 'PROTECT' action, can't attach SA.\n", sa_id, sa_db[sa_id]->policy_id);
		rc = ENETHAL_STATUS_INVALID_PARAM;
		goto bail;
	}


	/*
	 *   2) Update internal DB.
	 */
	policy_db[policy_id]->attached = ADAP_TRUE;
	policy_db[policy_id]->sa_id = sa_id;
	sa_db[sa_id]->attached = ADAP_TRUE;
	sa_db[sa_id]->policy_id = policy_id;


	/*
	 *   3) Configure MEA:
	 *        -
	 *        -
	 */

	adap_memset(mea_ipsec_dbt.Integrity_key, 0, sizeof(mea_ipsec_dbt.Integrity_key));
	adap_memset(mea_ipsec_dbt.Confident_key, 0, sizeof(mea_ipsec_dbt.Confident_key));
	adap_memset(mea_ipsec_dbt.Integrity_IV, 0, sizeof(mea_ipsec_dbt.Integrity_IV));
	adap_memset(mea_ipsec_dbt.Confident_IV, 0, sizeof(mea_ipsec_dbt.Confident_IV));

	adap_memcpy(mea_ipsec_dbt.Confident_key, sa_db[sa_id]->user_sa.encr_key, sizeof(mea_ipsec_dbt.Confident_key));
	adap_memcpy(mea_ipsec_dbt.Integrity_key, sa_db[sa_id]->user_sa.auth_key, sizeof(mea_ipsec_dbt.Integrity_key));
	mea_ipsec_dbt.Confident_IV[0] = sa_db[sa_id]->user_sa.nonce;

	mea_ipsec_dbt.SPI = sa_db[sa_id]->user_sa.selectors.spi;
	mea_ipsec_dbt.TFC_padding_en = sa_db[sa_id]->user_sa.tfc_en;
	mea_ipsec_dbt.ESN_en = sa_db[sa_id]->user_sa.extended_seqnum;
	mea_ipsec_dbt.security_type = EnetHal_ipsec_get_security_type(sa_db[sa_id]->user_sa.auth_alg, sa_db[sa_id]->user_sa.encr_alg);
	sa_db[sa_id]->mea_ipsec_esp_id = MEA_PLAT_GENERATE_NEW_ID;
	mea_status = MEA_API_IPSecESP_Create_Entry(MEA_UNIT_0,
											   &mea_ipsec_dbt,
											   &sa_db[sa_id]->mea_ipsec_esp_id);
	if (mea_status != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsecESP entry creation failed.\n");
		rc = ENETHAL_STATUS_MEA_ERROR;
		goto bail;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IPsecESP entry ID %d.\n", sa_db[sa_id]->mea_ipsec_esp_id);

	if (policy_db[policy_id]->user_policy.dir == ADAP_IPSEC_POLICY_DIR_OUT) {

		/* Create action */
		tft_action_rule.valid = MEA_TRUE;
		tft_action_rule.action_params = &action_entry_data;
		tft_action_rule.action_params->ed_id_valid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vpValid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vp_val = sa_db[sa_id]->user_sa.output_port;
		tft_action_rule.EHP_Entry = &ehp_arr_entry;
		tft_action_rule.EHP_Entry->num_of_entries = 1;
		tft_action_rule.EHP_Entry->ehp_info = &ehp_info_dbt;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.encryp_decryp_type = MEA_EDITING_ENC_DEC_Encryption;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.crypto_ESP_Id = sa_db[sa_id]->mea_ipsec_esp_id;

		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.valid = MEA_TRUE;
		switch(sa_db[sa_id]->user_sa.encap) {
		case ADAP_IPSEC_POLICY_ENCAP_IPSEC_ONLY:
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.type = MEA_IPSec_tunnel_TYPE_only;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.IPSec_src_IP = sa_db[sa_id]->user_sa.selectors.tunnel_sip;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.IPSec_DST_IP = sa_db[sa_id]->user_sa.selectors.tunnel_dip;
//			adap_memcpy(&tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.martini_info.DA, sa_db[sa_id]->user_sa.ipsec.dmac, sizeof(MEA_MacAddr));
//			adap_memcpy(&tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.martini_info.SA, sa_db[sa_id]->user_sa.ipsec.smac, sizeof(MEA_MacAddr));
			break;
		case ADAP_IPSEC_POLICY_ENCAP_IPSEC_NVGRE:
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.type = MEA_IPSec_tunnel_TYPE_NVGRE;
			adap_mac_to_arr(&sa_db[sa_id]->user_sa.nvgre.outer_dmac, tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.NVGre_info.Outer_da_MAC.b);
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.IPSec_DST_IP = sa_db[sa_id]->user_sa.selectors.tunnel_dip;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.NVGre_info.GRE_DST_IP = sa_db[sa_id]->user_sa.nvgre.gre_dip;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.NVGre_info.VSID = sa_db[sa_id]->user_sa.nvgre.virtual_subnet_id;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.NVGre_info.FlowID = sa_db[sa_id]->user_sa.nvgre.flow_id;
			break;
		case ADAP_IPSEC_POLICY_ENCAP_IPSEC_VXLAN:
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.type = MEA_IPSec_tunnel_TYPE_VXLAN;
			tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.valid = MEA_TRUE;
			adap_mac_to_arr(&sa_db[sa_id]->user_sa.vxlan.outer_dmac, tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.vxlan_Da.b);
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.vxlan_dst_Ip = sa_db[sa_id]->user_sa.vxlan.dip;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.vxlan_vni = sa_db[sa_id]->user_sa.vxlan.network_id;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.l4_dest_Port = sa_db[sa_id]->user_sa.vxlan.dport;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.l4_src_Port = sa_db[sa_id]->user_sa.vxlan.sport;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.ttl = sa_db[sa_id]->user_sa.vxlan.ttl;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.VxLan_info.sgw_ipId = sa_db[sa_id]->user_sa.vxlan.sgw;
            tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.IPSec_info.IPSec_DST_IP = sa_db[sa_id]->user_sa.selectors.tunnel_dip;
			break;
		default:
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," IPsec encapsulation (%d) not supported.\n", sa_db[sa_id]->user_sa.encap);
			rc = ENETHAL_STATUS_NOT_SUPPORTED;
			goto bail;
		}

		rc = EnetHal_ipsec_get_out_service(&policy_db[policy_id]->user_policy, &service_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_action_table(service_id, &action_table_id, ADAP_FALSE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		mea_status = MEA_API_Set_UEPDN_TEID_ConfigAction(MEA_UNIT_0,
														 action_table_id,
														 policy_db[policy_id]->action_id,
														 &tft_action_rule);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

	} else {
		rc = EnetHal_ipsec_get_in_service(policy_db[policy_id]->user_policy.ifindex,
				                          sa_db[sa_id]->user_sa.selectors.tunnel_sip,
										  sa_db[sa_id]->user_sa.selectors.spi,
										  sa_db[sa_id]->mea_ipsec_esp_id,
										  &service_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Service not found.\n");
			goto bail;
		}
	}

bail:
	/* TODO - roolback */
	return rc;
}


EnetHal_Status_t EnetHal_ipsec_detach_sa(adap_ipsec_policy_id policy_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_t service_id;
	ADAP_Uint32 action_table_id;
	mea_TFT_Action_rule_t tft_action_rule;
	MEA_Action_Entry_Data_dbt action_entry_data;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_arr_entry;
	MEA_EHP_Info_dbt ehp_info_dbt;
	MEA_Status mea_status;


	adap_memset(&tft_action_rule, 0, sizeof(mea_TFT_Action_rule_t));
	adap_memset(&action_entry_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&ehp_arr_entry, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&ehp_info_dbt, 0, sizeof(MEA_EHP_Info_dbt));

	/*
	 *   1) Verification:
	 *        - IPsec module is initialized.
	 *        - <policy_id> exists.
	 *        - Policy are attahced.
	 */

	if (g_initialized == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IPsec module not initialized.\n");
		rc = ENETHAL_STATUS_NOT_INITIALIZED;
		goto bail;
	}

	if (policy_db[policy_id] == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy ID %d not found.\n", policy_id);
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	if (policy_db[policy_id]->attached == ADAP_FALSE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Policy %d not attached.\n", policy_id);
		rc = ENETHAL_STATUS_INVALID_PARAM;
		goto bail;
	}

	assert(sa_db[policy_db[policy_id]->sa_id]->attached == ADAP_TRUE);


	/*
	 *   2) Update internal DB.
	 */
	policy_db[policy_id]->attached = ADAP_FALSE;
	sa_db[policy_db[policy_id]->sa_id]->attached = ADAP_FALSE;


	/*
	 *   3) Configure MEA:
	 *        -
	 *        -
	 */

	if (policy_db[policy_id]->user_policy.dir == ADAP_IPSEC_POLICY_DIR_OUT) {
		rc = EnetHal_ipsec_get_out_service(&policy_db[policy_id]->user_policy, &service_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		rc = EnetHal_ipsec_get_service_action_table(service_id, &action_table_id, ADAP_FALSE);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			goto bail;
		}

		/* Create action */
		tft_action_rule.valid = MEA_TRUE;
		tft_action_rule.action_params = &action_entry_data;
		tft_action_rule.action_params->ed_id_valid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vpValid = MEA_TRUE;
		tft_action_rule.action_params->overRule_vp_val = 127;
		tft_action_rule.EHP_Entry = &ehp_arr_entry;
		tft_action_rule.EHP_Entry->num_of_entries = 1;
		tft_action_rule.EHP_Entry->ehp_info = &ehp_info_dbt;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_info.stamp_color = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc = 12;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc = 0;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc = 15;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.atm_info.val.all= 0x91000fae;
		tft_action_rule.EHP_Entry->ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		MEA_SET_OUTPORT(&tft_action_rule.EHP_Entry->ehp_info[0].output_info, 127);

		mea_status = MEA_API_Set_UEPDN_TEID_ConfigAction(MEA_UNIT_0,
														 action_table_id,
														 policy_db[policy_id]->action_id,
														 &tft_action_rule);
		if (mea_status != MEA_OK) {
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}
	} else {

	}

	mea_status = MEA_API_IPSecESP_Delete_Entry(MEA_UNIT_0,
											   sa_db[policy_db[policy_id]->sa_id]->mea_ipsec_esp_id);
	if (mea_status != MEA_OK) {
		rc = ENETHAL_STATUS_MEA_ERROR;
		goto bail;
	}

bail:
	/* TODO - roolback */
	return rc;
}





/*****************************************************************************/
/* Internal functions                                                        */
/*****************************************************************************/

static
MEA_IPSec_Security_Type_t sec_type[ADAP_AUTH_ALG_LAST][ADAP_ENCR_ALG_LAST] = {
		/* MD5 */
		{
				MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_MD5_96,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_MD5_96,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_MD5_96,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_MD5_96,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_MD5_96,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_MD5_96,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_MD5_96,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_HMC_MD5_96,   /* NONE */
		},

		/* SHA1 */
		{
				MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA1_96,   /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA1_96,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA1_96,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA1_96,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA1_96,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_HMC_SHA1_96,   /* NONE */
		},

		/* SHA256 */
		{
				MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA256_128,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA256_128,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA256_128,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA256_128,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA256_128,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_HMC_SHA256_128,  /* NONE */
		},

		/* SHA384 */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_HMC_SHA384_192,  /* NONE */
		},

		/* SHA512 */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_HMC_SHA512_256,  /* NONE */
		},

		/* AES_XCBC_MAC */
		{
				MEA_EDITING_IPSec_Security_AES_128_CBC_AES_XCBC_MAC_96,   /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_AES_192_CBC_AES_XCBC_MAC_96,   /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_AES_256_CBC_AES_XCBC_MAC_96,   /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_AES_256_CBC_AES_XCBC_MAC_96,   /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_AES_XCBC_MAC_96,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_AES_XCBC_MAC_96,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_AES_XCBC_MAC_96, /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_AES_XCBC_MAC_96,  /* NONE */
		},

		/* AES_CMAC */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_AES_CMAC_96,      /* NONE */
		},

		/* AES_128_GMAC */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_AES_128_GMAC_128,  /* NONE */
		},

		/* AES_192_GMAC */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_AES_192_GMAC_128,  /* NONE */
		},

		/* AES_256_GMAC */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_INTEG_AES_256_GMAC_128,  /* NONE */
		},

		/* NONE */
		{
				MEA_EDITING_IPSec_Security_CONFIDENT_3DES_CBC,      /* 3DES_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CBC,   /* AES_128_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CBC,   /* AES_192_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CBC,   /* AES_256_CBC */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR,   /* AES_128_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR,   /* AES_192_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR,   /* AES_256_CTR */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_64,  /* AES_128_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_96,  /* AES_128_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_128,  /* AES_128_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_64,  /* AES_192_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_96,  /* AES_192_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_128,  /* AES_192_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_64,  /* AES_256_GCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_96,  /* AES_256_GCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_128,  /* AES_256_GCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_64,  /* AES_128_CCM_64 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_96,  /* AES_128_CCM_96 */
				MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_128,  /* AES_128_CCM_128 */
				MEA_EDITING_IPSec_Security_CONFIDENT_LAST,  /* NONE */
		},
};

static
MEA_IPSec_Security_Type_t EnetHal_ipsec_get_security_type(adap_auth_algorithm auth_alg,
		                                                  adap_encr_algorithm encr_alg)
{
	return sec_type[auth_alg][encr_alg];
}

static
EnetHal_Status_t EnetHal_ipsec_get_in_service(ADAP_Uint32 ifindex, ADAP_Uint32 sip, ADAP_Uint32 spi, ADAP_Uint32 mea_ipsec_esp_id, MEA_Service_t *service_id)
{
	tServiceDb * service = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
    MEA_Service_Entry_Key_dbt              service_key;
	MEA_Service_Entry_Data_dbt             service_data;
	MEA_OutPorts_Entry_dbt                 service_outports;
	MEA_Policer_Entry_dbt                  service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   service_editing;
	MEA_EHP_Info_dbt                       ehp_info;
	MEA_Port_t                             in_port;


	adap_memset(&service_key, 0, sizeof(MEA_Service_Entry_Key_dbt));
	adap_memset(&service_data, 0, sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&service_outports, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&service_policer, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&service_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt));

	if (MeaAdapGetPhyPortFromLogPort (ifindex, &in_port) == ENET_FAILURE) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," ifindex %d not valid.\n", ifindex);
		rc = ENETHAL_STATUS_INVALID_PARAM;
		goto bail;
	}
	service_key.src_port         = in_port;
	service_key.net_tag          = sip;
	service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_IPSEC_NVGRE_ENCRYPT;
	service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
	service_key.inner_netTag_from_valid = MEA_TRUE;
	service_key.inner_netTag_from_value = spi;

	service_editing.num_of_entries = 1;
	service_editing.ehp_info = &ehp_info;
	service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY;
	service_editing.ehp_info->ehp_data.IPSec_info.type = MEA_IPSec_tunnel_TYPE_only;
	service_editing.ehp_info->ehp_data.encryp_decryp_type = MEA_EDITING_ENC_DEC_Decryption;
	service_editing.ehp_info->ehp_data.crypto_ESP_Id = mea_ipsec_esp_id;

	MEA_SET_OUTPORT(&service_outports, 27);

	service_policer.CIR = 1000000000;
	service_policer.CBS = 32000;
	service_policer.comp = 0;
	service_policer.gn_type= 5;
	service_data.policer_prof_id = MEA_PLAT_GENERATE_NEW_ID;

	if(MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
										  &service_data.policer_prof_id,
										  0,
										  MEA_INGRESS_PORT_PROTO_TRANS,
										  &service_policer) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Create_Policer_ACM_Profile failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	service_data.valid = MEA_TRUE;
	service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL;

	*service_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_Service /* TODO - ADAP */ (MEA_UNIT_0,
	                                &service_key,
	                                &service_data,
		                            &service_outports,
		                            &service_policer,
		                            &service_editing,
		                            service_id) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unable to create the new service.\n");
		rc = ENETHAL_STATUS_MEA_ERROR;
		goto bail;
    }

	service = MeaAdapAllocateServiceId();
	if(service == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to allocate service\n");
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}

	service->ifIndex = ifindex;
	service->valid = MEA_TRUE;
	service->enet_priority_type = service_key.priType;
	service->enet_IncPriority = service_key.pri;
	service->L2_protocol_type = service_key.L2_protocol_type;
	service->sub_protocol_type = service_key.sub_protocol_type;
	service->serviceId = *service_id;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," service ID %d created.\n", service->serviceId);

bail:
	/* TODO - rollback */
	return rc;
}

static
EnetHal_Status_t EnetHal_ipsec_get_out_service(adap_ipsec_policy *policy, MEA_Service_t *service_id)
{
	tServiceDb * service = NULL;
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	/* Get service from adaptor DB (by ifindex & vlan) */
	if (policy->tagged == ADAP_TRUE) {
		service = MeaAdapGetServiceIdByVlanIfIndex(policy->vlan_id, policy->ifindex, MEA_PARSING_L2_KEY_Ctag);
	} else {
		service = MeaAdapGetServiceIdByVlanIfIndex(0, policy->ifindex, MEA_PARSING_L2_KEY_Untagged);
	}
	if (service == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Service not found.\n");
		rc = ENETHAL_STATUS_NOT_FOUND;
		goto bail;
	}

	*service_id = service->serviceId;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," service ID %d.\n", service->serviceId);

bail:
	return rc;
}

//static
//EnetHal_Status_t adap_ipsec_put_service(ADAP_Uint32 ifindex, ADAP_Uint32 vlan_id)
//{
//	MEA_Status mea_status;
//	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
//
//
//
//
//bail:
//	return rc;
//}

static
adap_ipsec_policy_direction EnetHal_ipsec_get_direction(adap_ipsec_policy_id policy_id)
{
	/*
	 * OUT:  0 <= ID < ADAP_IPSEC_MAX_OUT_POLICY
	 * IN:  ADAP_IPSEC_MAX_OUT_POLICY <= ID < (ADAP_IPSEC_MAX_IN_POLICY + ADAP_IPSEC_MAX_OUT_POLICY)
	 */
	if (policy_id < ADAP_IPSEC_MAX_OUT_POLICY) {
		return ADAP_IPSEC_POLICY_DIR_OUT;
	} else {
		return ADAP_IPSEC_POLICY_DIR_IN;
	}
}

static
EnetHal_Status_t EnetHal_ipsec_alloc_policy_id(adap_ipsec_policy_direction dir, adap_ipsec_policy_id *policy_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	if (dir == ADAP_IPSEC_POLICY_DIR_OUT) {
		rc = adap_index_pool_alloc(out_policy_pool, policy_id);
	} else {
		rc = adap_index_pool_alloc(in_policy_pool, policy_id);
		*policy_id += ADAP_IPSEC_MAX_OUT_POLICY;
	}

	return rc;
}

static
EnetHal_Status_t EnetHal_ipsec_free_policy_id(adap_ipsec_policy_id policy_id)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	if (EnetHal_ipsec_get_direction(policy_id) == ADAP_IPSEC_POLICY_DIR_OUT) {
		rc = adap_index_pool_free(out_policy_pool, policy_id);
	} else {
		rc = adap_index_pool_free(in_policy_pool, policy_id - ADAP_IPSEC_MAX_OUT_POLICY);
	}

	return rc;
}

static
EnetHal_Status_t EnetHal_ipsec_get_service_action_table(MEA_Service_t service_id, ADAP_Uint32 *action_table_id, ADAP_Bool forced)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_Entry_Data_dbt             service_data;
	MEA_OutPorts_Entry_dbt                 service_outPorts;
	MEA_Policer_Entry_dbt                  service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   service_editing;
	tServiceDb * service;
	MEA_Status mea_status;

	adap_memset(&service_data, 0, sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&service_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&service_policer, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&service_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	service = MeaAdapGetServiceId(service_id);
	assert(service != NULL);

	if (service->action_table_valid == ADAP_TRUE) {
		*action_table_id = service->action_table_id;
	} else if (forced == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_FOUND;
	} else {
		mea_status = MEA_API_Get_Service /* TODO - change to ADAP */(MEA_UNIT_0,
			                                  service->serviceId,
											  NULL,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Get service %d failed (1).\n", service->serviceId);
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_calloc(service_editing.num_of_entries, sizeof(MEA_EHP_Info_dbt));
		mea_status = MEA_API_Get_Service /* TODO - change to ADAP */ (MEA_UNIT_0,
			                                  service->serviceId,
											  NULL,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Get service %d failed (2).\n", service->serviceId);
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		rc = adap_srv_rm_alloc_action_table(action_table_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed allocating action table ID.\n");
			goto bail;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," Service %d has now uepdn ID (%d).\n", service->serviceId, *action_table_id);

		service_data.Uepdn_id = *action_table_id;
		service_data.Uepdn_valid = MEA_TRUE;

		mea_status = MEA_API_Set_Service /* TODO - change to ADAP */ (MEA_UNIT_0,
			                                  service->serviceId,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed setting service.\n");
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		service->action_table_id = *action_table_id;
		service->action_table_valid = ADAP_TRUE;
	}

bail:
	adap_safe_free(service_editing.ehp_info);
	/* TODO - rollback */
	return rc;
}

static
EnetHal_Status_t EnetHal_ipsec_get_service_qos_table(MEA_Service_t service_id, MEA_TFT_t *qos_table_id, ADAP_Bool forced)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	MEA_Service_Entry_Data_dbt             service_data;
	MEA_OutPorts_Entry_dbt                 service_outPorts;
	MEA_Policer_Entry_dbt                  service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   service_editing;
	tServiceDb * service;
	MEA_Status mea_status;

	adap_memset(&service_data, 0, sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&service_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&service_policer, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&service_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	service = MeaAdapGetServiceId(service_id);
	assert(service != NULL);

	if (service->qos_table_valid == ADAP_TRUE) {
		*qos_table_id = service->qos_table_id;
	} else if (forced == ADAP_FALSE) {
		rc = ENETHAL_STATUS_NOT_FOUND;
	} else {
		mea_status = MEA_API_Get_Service /* TODO - change to ADAP */ (MEA_UNIT_0,
			                                  service->serviceId,
											  NULL,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Get service %d failed (1).\n", service->serviceId);
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_calloc(service_editing.num_of_entries, sizeof(MEA_EHP_Info_dbt));
		mea_status = MEA_API_Get_Service /* TODO - change to ADAP */ (MEA_UNIT_0,
			                                  service->serviceId,
											  NULL,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Get service %d failed (2).\n", service->serviceId);
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		rc = adap_srv_rm_alloc_qos_table(qos_table_id);
		if (rc != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed allocating QoS table ID.\n");
			goto bail;
		}

		/* set mask in service */
		service_data.HPM_prof_mask = tft_profile_mask_id;
		service_data.HPM_profileId = *qos_table_id;
		service_data.HPM_valid = MEA_TRUE;

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," QoS table ID %d (mask %d) allocated for service %d.\n", *qos_table_id, tft_profile_mask_id, service->serviceId);

		mea_status = MEA_API_Set_Service /* TODO - change to ADAP */ (MEA_UNIT_0,
			                                  service->serviceId,
											  &service_data,
											  &service_outPorts,
											  &service_policer,
											  &service_editing);
		if (mea_status != MEA_OK) {
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Failed setting service.\n");
			rc = ENETHAL_STATUS_MEA_ERROR;
			goto bail;
		}

		service->qos_table_id = *qos_table_id;
		service->qos_table_valid = ADAP_TRUE;
	}

bail:
	adap_safe_free(service_editing.ehp_info);
	/* TODO - rollback */
	return rc;
}
