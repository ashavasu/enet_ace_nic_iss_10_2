#!/bin/bash

#set -x


DIAG_FILE_LOCATION='/root/lspci_dump.dat'
PARAM_FILE_LOCATION='/root/enet_params.dat'
LABEL_FILE_LOCATION='/root/sys_class_net.dat'

if [ "$1" == '--dry' ]; then
        DRY='echo'
	set -x
fi


if [ ! -f  $PARAM_FILE_LOCATION ]; then
	touch /root/enet_params.dat
else	cp $PARAM_FILE_LOCATION  "$PARAM_FILE_LOCATION".BAK
	echo ' ' > $PARAM_FILE_LOCATION
fi


ls -alR /sys/class/net/ > $LABEL_FILE_LOCATION
lspci > $DIAG_FILE_LOCATION


NIC_COUNT=$(cat $DIAG_FILE_LOCATION | grep Xil | wc -l)
NIC=1

for (( i = 1 ; i<= $NIC_COUNT; i++ )); do
		ACENIC=ACENIC$NIC
		XIL_SLOT_SHORT=`cat $DIAG_FILE_LOCATION | grep Xil | sed -n "$i"p | cut -d':' -f1`
		XIL_SLOT_DEC=`printf "%01d\n" 0x$XIL_SLOT_SHORT`
		XL710_SLOT_DEC=$(( $XIL_SLOT_DEC-4 )) 
		XL710_SLOT_HEX=`echo "obase=16; $XL710_SLOT_DEC" | bc`
		XL710_SLOT=`printf "%02x\n" 0x$XL710_SLOT_HEX`:00.0
		XIL_SLOT=`cat $DIAG_FILE_LOCATION | grep Xil | sed -n "$i"p | cut -d' ' -f1` 
		LABEL=`cat $LABEL_FILE_LOCATION | grep $XL710_SLOT | cut -d'/' -f 10`
		MAC=`ip link show $LABEL | grep 00:e0:ed | cut -d' ' -f 6`
		##############################################################
		$DRY ip link set $LABEL down
		NEW_LABEL=$ACENIC"_127"
		sleep 0.25
		$DRY ip link set $LABEL name $NEW_LABEL
		sleep 0.25
		$DRY ip link set $NEW_LABEL up
		echo '######################## '$ACENIC' LAYOUT ########################: ' >> $PARAM_FILE_LOCATION
		echo $ACENIC'_710_SLOT'=$XL710_SLOT >> $PARAM_FILE_LOCATION 
		echo $ACENIC'_XIL_SLOT'=$XIL_SLOT >> $PARAM_FILE_LOCATION 
	 	echo $ACENIC'_MAC'=$MAC >> $PARAM_FILE_LOCATION 
		echo $ACENIC'_LABEL='$NEW_LABEL >> $PARAM_FILE_LOCATION
		(( ++NIC ))
done

if [ "$1" == '--dry' ]; then
	set +x
fi



cat $PARAM_FILE_LOCATION

#set +x

