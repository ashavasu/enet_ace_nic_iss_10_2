/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_DATABASE_H
#define ADAP_DATABASE_H

#include "adap_linklist.h"
#include "adap_rbtree.h"
#include "adap_stp.h"
#include "MEA_platform.h"
#include "mea_api.h"
#include "EnetHal_L2_Api.h"
#include "EnetHal_L3_Api.h"

#define LPM_ADAPTOR_WANTED 1

typedef void (*func_linklosk_delete)( void *data);


#define		MAX_NUMBER_OF_SERVICES				4000
#define		MAX_NUMBER_OF_ACTIONS				4000

#define		START_OF_SERVICE_ID					100
#define		START_OF_ACTION_ID					4000

#define		ADAP_CPU_PORT_ID					127

#define		ADAP_MANAGEMENT_PORT				100

#define		ADAP_NUMBER_OF_BRIDGE_DOMAIN		1024   //500

#define		ADAP_NUMBER_OF_MSTP_VLANS           512

#define 	ADAP_NUM_OF_SUPPORTED_VLANS        	4096
#define 	ADAP_DEFAULT_PROTOCOLS_TRAFFIC		478

#define 	ADAP_DEFAULT_SERVICE_VLAN_ID        4095

#define		ADAP_NUMBER_OF_MULTICAST_MAC		512
#define		ADAP_MAX_NUMBER_OF_MAC_LEARNING		20

#define		VLAN_UNTAGGED_MEMBER_PORT			0x0001
#define		VLAN_TAGGED_MEMBER_PORT				0x0002

#define		MAX_IP_ROUTE_TABLE					33000
#define 	MAX_NUM_OF_L3_INTERFACES			3000
#define 	MAX_NUM_OF_FAILED_PPP_ARP			5

#define 	ADAP_MAX_NUM_OF_MSTP_INSTANCES		64

#define 	MAX_NUM_OF_INTERNAL_PORTS			4
#define 	INTERNAL_PORT1_NUMBER				24
#define 	INTERNAL_PORT2_NUMBER				25
#define 	INTERNAL_PORT3_NUMBER				26
#define 	INTERNAL_PORT4_NUMBER				27

#define   	NP_ARP_DYNAMIC                3
#define   	NP_ARP_STATIC                 4

#define		NULL_OUTPUT_PORT					0xFFFF


#define		START_L3_VPN						488
#define		START_L3_VPN1						489
#define		START_PARCON_VPN					490
#define		START_L3_VPN2						491

#define		START_L3_VPN_IPV6					588
#define		START_L3_VPN1_IPV6					589

#define		VPN_HOST_TO_NETWORK					START_L3_VPN1
#define		VPN_NETWORK_TO_HOST					START_L3_VPN2

#define		MAX_NUM_OF_IP_CLASS_ENTRIES			255
#define		MAX_NEXTHOP_ENTRIES					150

#define		MAX_NUM_OF_AGGR_ENTRY				4

#define		MAX_NUM_OF_NIC_PORTS				4
#define 	ADAP_BITS_PER_BYTE 					8

#define START_OF_LAG_VIRTUAL_PORTS        		64
#define MAX_NUM_PORTS_IN_LAG					4

#define 	D_IP_MASK_08						0x01000000
#define 	D_IP_MASK_16						0x00010000
#define 	D_IP_MASK_24						0x00000100
#define 	D_IP_MASK_32						0x00000001

#define		ADAP_FILTER_DA_MAC_KEY_TYPE			0x00000001
#define		ADAP_FILTER_SA_MAC_KEY_TYPE			0x00000002
#define		ADAP_FILTER_L2_ETHTYPE_KEY_TYPE		0x00000004
#define		ADAP_FILTER_SOURCE_IPV4_KEY_TYPE	0x00000008
#define		ADAP_FILTER_DEST_IPV4_KEY_TYPE		0x00000010
#define		ADAP_FILTER_IP_PROTOCOL_KEY_TYPE	0x00000020
#define		ADAP_FILTER_SOURCE_PORT_KEY_TYPE	0x00000040
#define		ADAP_FILTER_DEST_PORT_KEY_TYPE		0x00000080
#define		ADAP_FILTER_VLAN_PCP_KEY_TYPE		0x00000100
#define		ADAP_FILTER_SOURCE_IPV6_KEY_TYPE	0x00000200
#define		ADAP_FILTER_DEST_IPV6_KEY_TYPE		0x00000400
#define     ADAP_FILTER_CVLAN_KEY_TYPE          0x00000800
#define     ADAP_FILTER_INNER_ETHTYPE_KEY_TYPE  0x00001000
#define     ADAP_FILTER_OUTER_ETHTYPE_KEY_TYPE  0x00002000
#define     ADAP_FILTER_CVLAN_PRIORITY_KEY_TYPE 0x00004000
#define     ADAP_FILTER_SVLAN_PRIORITY_KEY_TYPE 0x00008000
#define     ADAP_FILTER_L2_PROTOCOL_KEY_TYPE    0x00010000
#define     ADAP_FILTER_SVLAN_KEY_TYPE          0x00020000


#define 	ADAP_MAX_NUM_ACL_FILTERS  				5
#define 	ADAP_MAX_NUMBER_OF_VLAN_EDITING_MAPPING	16


#define			SW_ENET_NIC_LAG_MODE				0x00000001
#define			SW_ENET_NIC_BOARD					0x00000002

#define			ADAP_MAX_NUMBER_OF_VLAN_PRIORITY	8

#define			SW_ENET_NIC_WITH_LAG_BOARD					SW_ENET_NIC_BOARD | SW_ENET_NIC_LAG_MODE

 // ONLY FOR ENET_NIC_OVS_MODE
#define		ADAP_FORTEVILLE_PORTID				1

#define		MAX_NUM_OF_CLUSTERS_PER_PORT		8

#define MAX_SIZEOFOF_INTERFACE_NAME		128
#define MAX_SIZEOFOF_INTERFACE_IP		50

#define MAX_NUMBER_OF_TUNNEL_SESSION		100

#define MAX_NUM_OF_ARP_CACHE				100

#define		MAX_NUMBER_OF_WHITE_LIST		256

#define 	MAX_NUM_OF_PORTS_BRIDGES		64
#define 	MAX_NUM_OF_VXLAN_TUNNELS		64
#define 	VXLAN_MAX_STRING_SIZE			64
#define		MAX_STRING_SIZE					64

#define		MAX_NUMBER_OF_VXLAN_TUNNEL		256
#define		NUMBER_OF_VTEP_VNI				10

#define		MAX_NUM_OF_ACE_VIRTUAL_FUNC		10
#define		MAX_NUM_OF_FLOW_MARKING_PROFILES 	16
#define 	MAX_NUM_OF_SVLAN_PORT_TABLE         10

#define    MAX_ACE_NAME_LEN			32
#define MAX_NUM_OF_CLASSIFIERS		100
#define MAX_NUM_OF_POLICERS			100
#define MAX_NUM_OF_QUEUES			256
#define MAX_NUM_OF_WRED_PROFILES    256
//#define MAX_NUM_OF_FILTERS			256
#define MAX_NUM_OF_SCHEDULERS 		(MAX_NUM_OF_QUEUES*(ENET_PLAT_QUEUE_NUM_OF_PRI_Q+1))
#define NUM_OF_CLUSTERS_PER_PORT            4
//#define NUM_OF_CLUSTERS_PER_PORT_GROUP_0    3 //2
//#define NUM_OF_CLUSTERS_PER_PORT_GROUP_1    0 //5

#define MULTIPLAY_SHAPER_CIR_CBS  				 1000

#define	D_UNTAGGED_SERVICE_TAG			0
#define	D_TAGGED_SERVICE_TAG			1
#define	D_PROVIDER_SERVICE_TAG			7

#define MAX_HPM_ENTRY_ROLES             64
#define MAX_NUM_OF_TFP_ENTRIES          8
#define MAX_HPM_TABLES                  3
#define MAX_ACL_TABLES                  10
#define MAX_ACL_PROF_NUM                64
#define MAX_ACL_FILTER_NUM              256
#define MAX_NUM_OF_ARP_FILTERS          256
#define MAX_PARENT_CONTROL_ENTRIES      64
#define MAX_NAT_ENTRIES_PER_TABLE       512
#define MAX_MTU_FLOW_POLICERS           4096 //max vlans
#define MAX_NUM_OF_LIMITER_PROFILE      512


extern tEnetHal_MacAddr            adapStpAddress;
extern tEnetHal_MacAddr            adapGvrpAddress;

extern tEnetHal_MacAddr            adapVlanProviderStpAddr;
extern tEnetHal_MacAddr            adapVlanProviderGvrpAddr;



typedef enum
{
	CHA_DROP,
	CHA_PERMIT,	
	CHA_REDIRECT,
}CHA_action;

typedef struct
{
    MEA_Bool valid;
    MEA_Service_t serviceId;
    MEA_Bool filter_enable;
    MEA_ACL_prof_Key_mask_dbt ACL_filter_info;
    MEA_Bool   ACL_Prof_valid;
    MEA_Uint16 ACL_Prof;
    MEA_Bool serviceCreated;
}tServiceAclParam;


typedef enum {
    FILTER_TYPE_L2 = 0,
    FILTER_TYPE_L3,
    FILTER_TYPE_NONE = 255
} eAclFilterTypes;


typedef struct
{
    ADAP_Bool           valid;
    ADAP_Int32          acl_profile;
    ADAP_Int32          refCount;
    ADAP_Uint32         filter_id;
    ADAP_Uint32         vlanid;
    ADAP_Uint16         inPort;
    ADAP_Uint16         priField;
    eAclFilterTypes     ftype;
}tAdapAclFiltProfile;

typedef struct
{
    ADAP_Uint32 num_of_filters;
    MEA_OutPorts_Entry_dbt outPorts;
    ADAP_Uint16 enetPolilcingProfile;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;
    MEA_Filter_Action_te action_type;
    MEA_Service_t serviceId;

    MEA_Bool src_port_valid;
    MEA_Port_t src_port;

    tAdapAclFiltProfile acl_profile_filt;
    MEA_Filter_t FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
    MEA_Filter_Key_dbt arrFilterKey[MEA_ACL_HIERARC_NUM_OF_FLOW];

    eEnetHal_Tos tos;
    ADAP_Uint32 dscp;
    ADAP_Uint32 filterCustomerVlanId;
    ADAP_Uint32 filterServiceVlanId;
    ADAP_Uint16 cVlanPriority;

    ADAP_Bool isPolicerSet;
    ADAP_Uint32 filterNoId;
}tAclFilterParams;

typedef struct
{
	ADAP_Bool			valid;
	ADAP_Int32			acl_profile;
    ADAP_Int32          refCount;
    ADAP_Uint32         filter_id;
    ADAP_Uint32         inPort;
    eAclFilterTypes     ftype;
    tAclFilterParams tAclParams;
    tServiceAclParam servicePrevFilt[MAX_ACL_PROF_NUM];
    ADAP_Uint32         vlanId;
    ADAP_Uint32         priField;
}tAdapAclProfile;


typedef struct
{
	unsigned int 	unit;
	ADAP_Bool		AceName_set;
	unsigned char 	AceName[MAX_ACE_NAME_LEN];
	ADAP_Bool		AcePort_set;
	unsigned int 	AcePort;
	ADAP_Bool		AceVlan_set;
	unsigned short 	AceVlan;
	ADAP_Bool		AceVnoTag_set;
	unsigned short 	AceVnoTag;
	ADAP_Bool		AceAppSrcMac_set;
	tEnetHal_MacAddr  	AceAppSrcMac;
	ADAP_Bool		AceAppDstMac_set;
	tEnetHal_MacAddr   	AceAppDstMac;
	ADAP_Bool		AceAppEthType_set;
	unsigned short 	AceAppEthType;
	ADAP_Bool		AceAppSrcIp_set;
	unsigned int  	AceAppSrcIp;
	ADAP_Bool		AceAppSrcIp6_set;
	tEnetHal_IPv6Addr 	AceAppSrcIp6;
	ADAP_Bool		AceAppDstIp_set;
	unsigned int  	AceAppDstIp;
	ADAP_Bool		AceAppDstIp6_set;
	tEnetHal_IPv6Addr 	AceAppDstIp6;
	ADAP_Bool		AceAppIpProto_set;
	unsigned char 	AceAppIpProto;
	ADAP_Bool		AceAppSrcL4Port_set;
	unsigned short 	AceAppSrcL4Port;
	ADAP_Bool		AceAppDstL4Port_set;
	unsigned short 	AceAppDstL4Port;
	ADAP_Bool		AcePolicerCir_set;
	unsigned int  	AcePolicerCir;
	ADAP_Bool		AcePolicerCbs_set;
	unsigned int  	AcePolicerCbs;
	ADAP_Bool		AcePolicerEir_set;
	unsigned int  	AcePolicerEir;
	ADAP_Bool		AcePolicerEbs_set;
	unsigned int   	AcePolicerEbs;
	ADAP_Bool		AceShaperState_set;
	unsigned int   	AceShaperState;
	ADAP_Bool		AceShaperCir_set;
	unsigned int   	AceShaperCir;
	ADAP_Bool		AceShaperCbs_set;
	unsigned int   	AceShaperCbs;
	ADAP_Bool		AceMonStat_set;
	unsigned int    AceMonStat;
}tComAceVirtualFunc;

typedef struct
{
	unsigned int 	unit;
	ADAP_Bool		AceCntrlVlanTag_set;
	unsigned short  AceCntrlVlanTag;
	ADAP_Bool		AceCntrlPort_set;
	unsigned int 	AceCntrlPort;
	ADAP_Bool		AceCntlSrcMac_set;
	tEnetHal_MacAddr  	AceCntlSrcMac;
	ADAP_Bool		AceCntlDstMac_set;
	tEnetHal_MacAddr  	AceCntlDstMac;
	ADAP_Bool		AceCntlEthType_set;
	unsigned short  AceCntlEthType;
	ADAP_Bool		AceCntlSrcIp_set;
	unsigned int 	AceCntlSrcIp;
	ADAP_Bool		AceCntlSrcIp6_set;
	tEnetHal_IPv6Addr 	AceCntlSrcIp6;
	ADAP_Bool		AceCntlDstIp_set;
	unsigned int 	AceCntlDstIp;
	ADAP_Bool		AceCntlDstIp6_set;
	tEnetHal_IPv6Addr 	AceCntlDstIp6;
	ADAP_Bool		AceCntlIpProto_set;
	unsigned short  AceCntlIpProto;
	ADAP_Bool		AceCntlSrcPort_set;
	unsigned short  AceCntlSrcPort;
	ADAP_Bool		AceCntlDstPort_set;
	unsigned short  AceCntlDstPort;
	ADAP_Bool		AceCntlAction_set;
	CHA_action		AceCntlAction;
	ADAP_Bool		AcePolicerCir_set;
	unsigned int 	AcePolicerCir;
	ADAP_Bool		AcePolicerCbr_set;
	unsigned int 	AcePolicerCbr;
	ADAP_Bool		AcePolicerEir_set;
	unsigned int 	AcePolicerEir;
	ADAP_Bool		AcePolicerEbr_set;
	unsigned int 	AcePolicerEbr;
	ADAP_Bool		accesslist_priorityid_set;
	unsigned int 	accesslist_priorityid;	
	ADAP_Bool		AceEgressPort_set;
	unsigned int 	AceEgressPort;
	ADAP_Bool		AceEgressDstMac_set;
	tEnetHal_MacAddr  	AceEgressDstMac;
	
}tComAceVirtualCtrlAdmin;


typedef struct
{
	ADAP_Bool			valid;
	tComAceVirtualFunc	tVirtualFunc;
	ADAP_Uint16 		enetPolilcingProfile;
	MEA_Service_t       phy_to_cpu_service_id;
	MEA_Service_t       cpu_to_phy_service_id;
	MEA_Filter_t        phy_to_cpu_FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	MEA_Filter_t        cpu_to_phy_FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	tAdapAclProfile		acl_profile_virt;
	ENET_QueueId_t 		tClusterId;
}tAceVirtualFunc;

typedef struct
{
	ADAP_Bool			valid;
	tComAceVirtualCtrlAdmin	tCtrlAdmin;
	ADAP_Uint16 		enetPolilcingProfile;
	MEA_Service_t       phy_to_cpu_service_id;
	MEA_Service_t       cpu_to_phy_service_id;
	MEA_Filter_t        phy_to_cpu_FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	MEA_Filter_t        cpu_to_phy_FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	tAdapAclProfile		acl_profile_ctrl;
	tAdapAclProfile		acl_profile_ctrl_phy_to_cpu;
	tAdapAclProfile		acl_profile_ctrl_cpu_to_phy;

}tAceCtrlAdmin;


typedef struct
{
	ADAP_Bool			valid;
	MEA_Service_t		Service;
	ADAP_Bool			isEntrySetToHw;
	ADAP_Int32  		src_port;
	ADAP_Int32  		src_ipv4_int;
	ADAP_Int32  		src_ipv4_ext;
	tEnetHal_MacAddr 		sa_mac_ext;
	ADAP_Uint32			vni;
	ADAP_Uint16			vlan;

}tAdapVxlanSnoop;

typedef struct
{
	ADAP_Int8 bridge_name[MAX_STRING_SIZE];
	ADAP_Int8 interface_name[MAX_STRING_SIZE];
	ADAP_Int32 port_number;
}tBridgeIntName;

typedef struct
{
	ADAP_Int32			valid;
	tBridgeIntName  bridgePort;
}tBridgePortEntry;

typedef struct
{
	unsigned long long int in_key;
	tEnetHal_IPv6Addr ipv6_src;
	tEnetHal_IPv6Addr ipv6_dst;
    unsigned int  odp_port;
    unsigned int  pkt_mark;
    unsigned int in_key_flow;
    unsigned int ip_src_flow;
    unsigned int ip_dst_flow;
    unsigned int action;
	unsigned int tnl_port;
	char vxlan_name[VXLAN_MAX_STRING_SIZE];
}tEnetOfVxlanTunnel;


typedef struct
{

	unsigned char vtep_ipv_dest[4];
	tEnetHal_MacAddr vtep_mac_dest;
	unsigned int num_of_entries;
	unsigned int vniId[NUMBER_OF_VTEP_VNI];
	unsigned short action;
}tEnetOvsdbTunnel;

#define		MAX_NUM_OF_REMOTE_MAC	20

typedef struct
{
	unsigned int num_of_entries;
	tEnetOvsdbTunnel tTunnel[MAX_NUM_OF_REMOTE_MAC];
}tEnetOvsDbSync;

typedef struct
{
	unsigned int num_of_entries;
	unsigned int vniId[NUMBER_OF_VTEP_VNI];
}tEnetVtepGlobal;

typedef struct
{
	ADAP_Bool			valid[MAX_NUM_OF_NIC_PORTS];
	MEA_Service_t 		serviceId[MAX_NUM_OF_NIC_PORTS];
}tEnetVtepService;

typedef struct
{
	ADAP_Bool			valid;
	unsigned int 		vniId;
	unsigned int		refCount;
	tEnetVtepService	vtepService;
}tEnetVtepSerDb;



typedef struct
{
	ADAP_Int32			valid;
	tEnetOfVxlanTunnel 	tunnel;
}tVxlanTunnelEntry;



typedef struct
{
	tBridgePortEntry 	bridgePortDb[MAX_NUM_OF_PORTS_BRIDGES];
	tVxlanTunnelEntry	vxlanDb[MAX_NUM_OF_VXLAN_TUNNELS];
	tEnetOvsdbTunnel	ovsdbVtep[MAX_NUM_OF_VXLAN_TUNNELS];
	tEnetVtepGlobal		ovsdbGlobl;
	tEnetVtepSerDb		tVtepService[NUMBER_OF_VTEP_VNI];
	tEnetOvsdbTunnel	ovsdbLocalVtep;
}tEnetOpenvSwitchsDb;

typedef struct
{
	tEnetHal_MacAddr	macAddress;
	ADAP_Int32		ip_address;
	MEA_Port_t 		portId;
	ADAP_Bool		valid;
	ADAP_Bool		newEntry;
}tLinuxArpCache;

typedef struct
{
	ADAP_Int32 								tunnel_id;
	ADAP_Int16								vlanId;
	ADAP_Int16								valid;
	MEA_Port_t  							portId;
	MEA_Service_t             				Service_id;
}tTunnelService;

typedef struct
{
	ADAP_Bool 	valid;
	ADAP_Bool   valid_ip;
	ADAP_Int8  	interface_name[MAX_SIZEOFOF_INTERFACE_NAME];
	ADAP_Int8  	host_ip[MAX_SIZEOFOF_INTERFACE_IP];
	tEnetHal_MacAddr	macAddress;
	ADAP_Int32	ip_address;
}tLinuxIpInterface;

typedef struct
{
	ENET_QueueId_t 	clusterId;
	ADAP_Bool		valid;
}tAdapCluster;
typedef struct
{
	ADAP_Bool				valid;
	ADAP_Uint32     		portbitmap;
	tEnetHal_MacAddr		macAddress;
	ADAP_Uint16				vlanId;
}tMulticastMac;

typedef struct
{
    MEA_Bool				valid;
    tEnetHal_MacAddr		MacAddr;
	ADAP_Uint16				vpnId;
	ADAP_Uint32				u4FdbId;
}tMacLearningTable;

typedef struct
{
	tAdapCluster    clustrDb[MAX_NUM_OF_CLUSTERS_PER_PORT];
	MEA_Port_t 		enetPort;
}tAdapClusterPerPort;

typedef struct
{
	ADAP_Uint32 bitmask;
	tEnetHal_MacAddr src_mac;
	tEnetHal_MacAddr da_mac;
	ADAP_Uint32	srcIpv4;
	ADAP_Uint32  SourceIpv4Mask;
	tEnetHal_IPv6Addr srcIpv6;
	ADAP_Uint32  destIpv4;
	ADAP_Uint32  destIpv4Mask;
	tEnetHal_IPv6Addr dstIpv6;
	ADAP_Uint16  SourcePort;
	ADAP_Uint16 ethertype;
	ADAP_Uint16  DestPort;
	ADAP_Uint8  ipProto;
	ADAP_Uint8  vlanPcp;
	ADAP_Uint16 cVlanId;
	ADAP_Uint16 outerEtherType;
	ADAP_Uint16 innerEtherType;
	ADAP_Uint16 cvlanPriority;
	ADAP_Uint16 svlanPriority;
	ADAP_Uint16 l2ProtocolType;
	ADAP_Uint16 sVlanId;
	ADAP_Uint32 tos;
	eEnetHal_Tos dscp;

	ADAP_Uint64 SourceMacAddrMask;
	ADAP_Uint64 DestMacAddrMask;
}tAdapAclFilterType;

typedef struct
{
	MEA_Bool		valid;
	MEA_Filter_t 	FilterId;
	MEA_Service_t   Service_id;
	tEnetHal_MacAddr macAddr;
}tFilterList;

typedef struct
{
	MEA_Port_t    		inPort;
	MEA_Service_t 		serviceId;
	ADAP_Uint32			mask;
}tInternalPorts;

typedef struct
{

	ADAP_Uint32     		ifIndex;
	ADAP_Uint16				valid;
	ADAP_Uint16				pvId;
}tPvidDInfo;

typedef struct _ServiceDb
{
	ADAP_Bool	  		valid;
	MEA_Service_t 		serviceId;
	MEA_Action_t 		actionId;
	ADAP_Uint32			ifIndex;
	ADAP_Uint32 		L2_protocol_type;
	ADAP_Uint32			sub_protocol_type;
	ADAP_Uint16   		outerVlan;
	ADAP_Uint16   		innerVlan;
	MEA_Port_t    		inPort;
	MEA_PmId_t    		pmId;
	MEA_Filter_t        FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	ADAP_Uint16			enet_IncPriority;
    MEA_IngressPort_Ip_pri_type_t enet_priority_type;
	MEA_Uint32			vpnId;
	
	ADAP_Bool			isAclProfile;
	ADAP_Uint32			acl_profile;
	ADAP_Uint32			refCount;

	ADAP_Bool			qos_table_valid;
	MEA_TFT_t			qos_table_id;

	ADAP_Bool			action_table_valid;
	ADAP_Uint32			action_table_id;
}tServiceDb;

typedef struct
{
	ADAP_Uint32			ifIndex;
	ADAP_Uint32 		L2_protocol_type;
	ADAP_Uint16   		outerVlan;
	ADAP_Uint16   		innerVlan;
	ADAP_Uint32			filter_mode_OR_AND_type;
	tAdapAclProfile		acl_profile_Hier;
	ADAP_Uint16			priority[MEA_ACL_HIERARC_NUM_OF_FLOW];
	ADAP_Uint8			priority_valid[MEA_ACL_HIERARC_NUM_OF_FLOW];
	ADAP_Uint32			valid;
	MEA_Service_t		serviceId;
	ADAP_Uint8			table_id;
	MEA_Filter_t        FilterId[MEA_ACL_HIERARC_NUM_OF_FLOW];
	ADAP_Uint32         filter_key_type[MEA_ACL_HIERARC_NUM_OF_FLOW];
	MEA_Filter_mask_dbt filter_mask[MEA_ACL_HIERARC_NUM_OF_FLOW];
}tAclHierarchy;

typedef struct
{
	ADAP_Uint16				valid;
	ADAP_Uint16				vpn;
}tVpnDb;

typedef struct
{
	tEnetHal_VlanSVlanMap 	VlanSVlanMapEntry;
	MEA_Bool				selectedEntry;
	MEA_Bool				valid;
} tStagClassTableEntry;

typedef struct _BridgeDomain
{
	ADAP_Uint32 			swFdb;
	ADAP_Uint32				bridgeId;
	ADAP_Uint16				vpn; 			//hw vpn
	ADAP_Uint16				vlanId;
	ADAP_Uint32				num_of_ports; 	//number of ports
	ADAP_Uint32     		ifIndex[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32	    		ifType[MAX_NUM_OF_SUPPORTED_PORTS];

	// those parameters per ifIndex
	ADAP_Uint32	    		learningMode[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_PmId_t	    		pmId[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint16	    		meterId[MAX_NUM_OF_SUPPORTED_PORTS];

	ADAP_Bool				learn_without_action;
	tStagClassTableEntry	SVlanClassTable[MAX_NUM_OF_SVLAN_PORT_TABLE];
	tEnetHal_VlanId 		pepPvid[MAX_NUM_OF_SUPPORTED_PORTS+1];
	AdapLinkList    		tServiceLinkList; // list of services
}tBridgeDomain;

typedef struct
{
	ADAP_Bool				valid;
	ADAP_Uint32 			ipAddr;
	ADAP_Uint32 			subnetMask;
	MEA_Action_t    		ActionId;
	tEnetHal_MacAddr		macAddress;
}tIpV4Routing;

typedef struct
{
	ADAP_Bool				valid;
	MEA_Action_t    		ActionId;
	ADAP_Uint32 			ipAddr;
	ADAP_Uint16				refCount;
	tEnetHal_MacAddr		macAddress;
	ADAP_Uint32				numOfLabels;
	ADAP_Uint32				LabelId;
	ADAP_Bool				mpls_tag;
}tIpDestV4Routing;

typedef struct
{
	ADAP_Bool			valid;
	ADAP_Uint32 		u4IpDestAddr;
	ADAP_Uint32 		u4IpSubNetMask;
	ADAP_Uint32     	u4NextHopGt; /* Ip addr of next hop[ */
	ADAP_Uint32     	u4IfIndex;   /* cfa ifIndex */
    tEnetHal_VlanId     	u2VlanId;
}tIpV4NextRoute;

typedef struct
{
	ADAP_Uint8			pu1IfName[20];
	ADAP_Uint8			interfaceName[20];
	ADAP_Uint32			ifIndex;
	MEA_Action_t    	ActionId;
	ADAP_Uint32 		u4CfaIfIndex;
	ADAP_Uint16 		u2VlanId;
	ADAP_Uint16 		u2InnerVlanId;
	ADAP_Bool			valid;
	tIpV4Routing 		MyIpAddr;
	ADAP_Uint32		 	tagPort;
	ADAP_Uint32		 	ipAddr;
	ADAP_Uint32		 	subnet;
	ADAP_Uint32 			IfType;
	//tIpDestV4Routing 	routeIpTable[MAX_IP_ROUTE_TABLE]; // create linklist
	AdapLinkList    	tNextHopIpLinkList; // list of ip addresses belong to this interface
}tIpInterfaceTable;

typedef struct
{
	ADAP_Bool	valid;
	tIPv6Addr	destIpv6Addr;
	ADAP_Uint8	destIpv6PrefixLen;
	tIPv6Addr	nextHopIpv6Addr;
	tEnetHal_MacAddr nextHopMAC;
	ADAP_Uint32	ifIndex;
	ADAP_Uint16	VlanId;
} tIpv6Route;

typedef struct
{
	ADAP_Bool	valid;
	tIPv6Addr	ipv6Addr;
	tEnetHal_MacAddr	macAddress;
	ADAP_Uint8	reachStatus;
	ADAP_Uint32	VrId;
	ADAP_Uint16	VlanId;
} tIpv6Neighbor;

typedef struct 
{
	tIpv6Neighbor	neighbor;
	ADAP_Uint32	ifIndex;
} tIpv6NeighborCache;

typedef struct
{
	ADAP_Bool	valid;
	ADAP_Uint32	ifIndex;
	ADAP_Uint32	IfType;
	ADAP_Uint8	interfaceName[20];
	MEA_Action_t	ActionId;
	ADAP_Uint32	u4CfaIfIndex;
	tIPv6Addr	ipv6Addr;
	ADAP_Uint8	ipv6PrefixLen;
	tEnetHal_MacAddr	macAddress;
	ADAP_Uint16	u2VlanId;
	ADAP_Uint16	u2InnerVlanId;
	ADAP_Uint32	tagPort;
	ADAP_Uint32	phyPort;
	ADAP_Uint32	TunnelIfIndex;
	tEnetHal_NpTunnelInfo	TunnelInfo;
	AdapLinkList	Ipv6NeigborLinkList;
} tIpv6InterfaceTable;

#define	MAX_NUMBER_OF_METERING			255
#define	MAX_NUMBER_OF_PM_PER_METER		20

typedef struct
{
	ADAP_Bool					valid;
	MEA_PmId_t	    			pmId;
	MEA_Service_t				service;
	MEA_Action_t				action;
}tPmPerMeter;

typedef struct
{
	ADAP_Bool					valid;
	MEA_Policer_prof_t 			policer_prof_id;
	MEA_TmId_t    				tm_Id;
	tPmPerMeter					pmDb[MAX_NUMBER_OF_PM_PER_METER];
	tEnetHal_QoSMeterEntry      issMeter;
	ADAP_Uint16                 PolicyIdx;            //Associated Policy Index into the Policy table
	ADAP_Uint16					mappingEditingIdx;	  // index to mapping editing
}tMeterDb;

#define	MAX_NUMBER_OF_METER_PM		20


/* this part only for NAT demo */
//#define	MEA_NAT_DEMO		1

#define	MAX_NUMBER_ICMP_ECHO_SESSIONS		255

#define	NAT_LAN_IFINDEX					1
#define	NAT_WAN_IFINDEX					2

#define MAX_NUMBER_OF_L4_CONNECTION				10000

/* end of NAT demo */
typedef struct
{
	ADAP_Bool				valid;
	ADAP_Uint32				sourceIpAddr;
	ADAP_Uint32				destIpAddr;
	ADAP_Uint16				origEchoId;
	ADAP_Uint16				newEchoId;
}tIcmpTable;

typedef struct
{
	ADAP_Bool				valid;
	ADAP_Bool				handle_by_cpu;
	ADAP_Uint32				sourceIpAddr;
	ADAP_Uint32				destIpAddr;
	ADAP_Uint16				sourcePort;
	ADAP_Uint16				newSourcePort;
	ADAP_Uint16				destPort;
	ADAP_Uint16  			protocol;
	/* action from host to network */
	MEA_Action_t   			HostToNetAtionId;
	/* action from network to host */
	MEA_Action_t   			NetToHostAtionId;

	MEA_PmId_t 				HostToNetpmId;
	MEA_PmId_t 				NetToHostpmId;

	ADAP_Uint64 			lastCountHostToNet;
	ADAP_Uint64 			lastCountNetToHost;
}tTcpUdpTable;

typedef struct
{
	ADAP_Bool				valid;
	ADAP_Uint32				destIP;
	ADAP_Uint16				destPort;
	ADAP_Uint16				newSourcetPort;
}tTcpUdpAllocPort;

typedef struct
{
	ADAP_Bool	valid;
	ADAP_Uint32 u4IfIndex;
	tEnetHal_VlanId SVlanId;
	ADAP_Uint8 u1AccepFrameType;
}tAcceptFrameType;

typedef struct
{
	ADAP_Uint32 u4IfIndex;
	tEnetHal_VlanId SVlanId;
	tEnetHal_VlanId Pvid;
}tPepPvid;

typedef struct
{
	ADAP_Bool				valid;
	ADAP_Uint32				sourceIpAddr;
	ADAP_Uint32				destIpAddr;
	ADAP_Uint16				sourcePort;
	ADAP_Uint16				destPort;
	ADAP_Uint16  			protocol;
	ADAP_Uint32 			ifIndex;
	/* action from host to network or from network to host*/
	MEA_Action_t   			AtionId;
	MEA_PmId_t 				pmId;
	ADAP_Uint64 			lastRxPmCount;
	ADAP_Uint16				vpnId;
}tNaptRule;


typedef struct
{
	ADAP_Bool			valid;
	ADAP_Uint32			localIpAddr;
	ADAP_Uint32			globalIpAddr;
	ADAP_Uint16			localPort;
	ADAP_Uint16			globalPort;
}tNaptAddressTable;


typedef struct
{
	ADAP_Bool			valid;
	ADAP_Uint32			IpAddr;
}tFailedArp;

typedef struct
{
	ADAP_Bool			valid;
	mea_action_type_te 	type;
	ADAP_Uint32			ipType; //can be E_HW_IPV6_ROUTING_APP_TABLE E_HW_IPV4_ROUTING_APP_TABLE
	MEA_Action_t  		Action_id;
	ADAP_Uint32			daIpAddr;
	ADAP_Uint32			daMaskAddr;
	tEnetHal_IPv6Addr	daIpv6Addr;
	tEnetHal_IPv6Addr	dav6MaskAddr;
	tEnetHal_MacAddr	daMaAddr;
	tEnetHal_MacAddr	saMacAddr;
	ADAP_Bool			hwConfig;
	ADAP_Uint8			entryId;
	ADAP_Uint32			vlanEditing;
	ADAP_Uint32			editing;
	ADAP_Uint32			outPortId;
	ADAP_Uint16			meterId;
	ADAP_Uint16			vpnId;
}tIpRole;


typedef struct
{
	ADAP_Bool			valid;
	ADAP_Uint8 			tableId;
	ADAP_Uint16 		vpnId;
	MEA_TFT_t   		tft_profile_Id;
	MEA_Uint16 			MaskId;
	ADAP_Uint16			UEPDN_Id;

	tIpRole				roleArray[MAX_HPM_ENTRY_ROLES];
}tIpRoleData;

typedef struct
{
	ADAP_Bool			valid;
	mea_action_type_te 	type;
	MEA_Action_t  		Action_id;
	ADAP_Uint32			daIpAddr;
	ADAP_Uint32			saIpAddr;
}tParentControl;

typedef struct
{
	ADAP_Uint32 portid;
	ADAP_Bool admin;
	ADAP_Uint32 consempetion;
	ADAP_Uint32 cir;
	ADAP_Uint32 cbs;
	ADAP_Uint32 overhead;
	ADAP_Uint32 resolution;
}tDatabaseShaperParams;

typedef struct
{
		ADAP_Bool			valid;
		ADAP_Uint32  		portbitmask;
		ADAP_Uint32         swAggIdx;
		ADAP_Uint32         selPolicy;
		ADAP_Uint32			lagVirtualPort;
		ADAP_Uint32			masterPort;
		ADAP_Uint16			hwlagId;
		ADAP_Uint16			numLagPorts;
}tDbaseHwAggEntry;

typedef struct
{
	ADAP_Uint16	lagVirtualPort;
	ADAP_Uint16	free_virtual_port;
}tHwAggVirtualPort;

#define		D_MAX_VXLAN_FLOODING			128

typedef struct
{
	ADAP_Bool								valid;
	ADAP_Uint32  							UE_pdn_Id;
    tEnetHal_MacAddr							destMac;
    ADAP_Uint32								destIp;
    ADAP_Uint32								tunnel_id;
}tVxLanFlooding;


typedef struct
{
	ADAP_Bool			valid;
	ADAP_Uint16 		numOfDefinedQueues;
	ENET_QueueId_t		clusterId;
}tClusterQueuesType;

typedef struct
{
	ADAP_Uint16         iss_PortNumber;
	tClusterQueuesType	clusterInfo[NUM_OF_CLUSTERS_PER_PORT];
}tClusterData;

typedef struct
{
	ADAP_Int16 		iss_PortNum;
	ADAP_Uint16		MasterLagPort;
	ADAP_Uint16		lagVirtualPort;
	ADAP_Uint16		lagId;
	MEA_Service_t	defSid;
	ADAP_Uint16		priorityMapProfileId;
	ADAP_Uint16		priorityCosProfileId;
	ADAP_Uint16		flowMarkingProfileId;
	ADAP_Uint16		flowPolicerProfileId;
	ADAP_Uint16		portTunnelType;
	ADAP_Uint16		StagForCustomerVlan;
	ADAP_Uint8		tunnelOption[ENETHAL_VLAN_MAX_PROT_ID];
	eAdap_StpMode stpMode;
	ADAP_Uint32		stpState;
	ADAP_Uint8		cepPortState[ADAP_NUM_OF_SUPPORTED_VLANS];
	tEnetHal_VlanSVlanMap   VlanSVlanMap;
	ADAP_Uint16		CustomerVlan;
	MEA_Action_t  	tunnelCusToProAction_id;
	MEA_Action_t  	tunnelProToCusAction_id;
	tMeterDb		defMeter;		// default meter per port
	ADAP_Uint16		meterIndex;
	ADAP_Bool		lacpTrafficBlockStatus;
} tPortDb;

/* MSTP database*/
typedef struct
{
	ADAP_Bool		isValid;
	ADAP_Uint16		MSTPInstId;
}tMstpVlanDb;

typedef struct
{
	ADAP_Uint16     port_state[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint16		MSTPInst_valid;
}tMstpInstDb;

typedef struct
{
	ADAP_Uint16             UseCount;         //To manage the  Profile' reuse
	ADAP_Uint16             portNum; 			//The port, this profile is attached to.
	ADAP_Uint8             isFree;       	//Indicates that the FlowMarkingProfile is not assigned
} tFlowMarkingProfileUse;

typedef struct
{
	tEnetHal_MacAddr u1SuppAddr;
	ADAP_Uint8 u1AuthMode;
	ADAP_Uint8 u1AuthStatus;
	ADAP_Uint8 u1CtrlDir;
}tPnacAutorizedDb;

typedef struct
{
	ADAP_Uint16 u2LocalSVlan;
	ADAP_Uint16 u2RelaySVlan;
	MEA_Service_t ServiceId[D_PROVIDER_SERVICE_TAG+1];
}tSvlanRelayDb;

typedef struct
{
	MEA_Bool								valid;
	ADAP_Uint16								policer_id;
	ADAP_Uint16 							u2PcpSelection;
	ADAP_Uint8								u1UseDei;
	MEA_EditingMappingProfile_Id_t      	ProfileId;
	MEA_FlowMarkingMappingProfile_Id_t      mappingMarkingProfile;
	MEA_FlowCoSMappingProfile_Id_t      	cosProfile;
}tPcpConfiguration;

typedef struct
{
	MEA_Bool	valid;
	ADAP_Uint16 		u2PcpSelRow;
	ADAP_Uint16 		u2PcpValue;
	ADAP_Uint8 		u1DropEligible;
}tHwIngressVlanPbPcpInfo;

typedef struct
{
	tHwIngressVlanPbPcpInfo pcpInfo[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY];

}tPbPcpInfoPri;

typedef struct
{
	MEA_Bool					l2Valid[ADAP_MAX_NUM_ACL_FILTERS];
	tEnetHal_L2FilterEntry      L2Filter[ADAP_MAX_NUM_ACL_FILTERS];
    MEA_Bool					l3Valid[ADAP_MAX_NUM_ACL_FILTERS];
    tEnetHal_L3FilterEntry      L3Filter[ADAP_MAX_NUM_ACL_FILTERS];
    tEnetHal_PriorityMapEntry   VlanPriorityMap;
} tFilterDb;

typedef struct
{
	MEA_EditingMappingProfile_Id_t      id;
	MEA_Bool							valid;
}tEditingMapping;

/* QoS Database */
typedef struct
{
	ADAP_Bool					valid;
    tEnetHal_QoSClassMapEntry   issClassifier;
    tFilterDb                	issFilter;
    ADAP_Uint16                 VlanId;
    ADAP_Uint16                 iss_PortNumber;         //Ingress port number
    ADAP_Uint16                 PolicyIdx;              //Associated Policy Index into the Policy table
} tClassifierDb;

typedef struct
{
	ADAP_Bool							valid;
	ADAP_Uint8							u1CurrFlag;
	tEnetHal_QoSPolicyMapEntry  		issPolicy;
	tEnetHal_QoSInProfileActionEntry    issInProActEntry;
	ADAP_Bool                    		issInProActEntryValid;
	tEnetHal_QoSOutProfileActionEntry   issOutProActEntry;
	ADAP_Bool                    		issOutProActEntryValid;
    ADAP_Uint16                 		ClassifierIdx;         //Associated Classifier Index into the Classifier table
    ADAP_Uint16                 		MeterIdx;              //Associated Meter Index into the Meter table
    ADAP_Uint16                		 	QueueIdx;              //Associated Queue Index into the Queue table
} tPolicyDb;

typedef struct {
    eAclFilterTypes fType;
    union {
        tEnetHal_L2FilterEntry l2FilterEntry;
        tEnetHal_L3FilterEntry l3FilterEntry;
    } x;
    tPolicyDb policy;

    tAclFilterParams tAclParams;
}tAdap_FilterEntry;




typedef struct
{
	ADAP_Bool					valid;
	ADAP_Uint16                 iss_PortNumber;
    tEnetHal_QoSSchedulerEntry      issScheduler;
    tEnetHal_QoSShapeCfgEntry       issShaper;
    eSchedulerLevel        		level;
    ADAP_Uint16                 QueueIdx;               // Associated ISS queue Index into the Queue table
    ADAP_Uint16                 ChildsSchedIdx[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];	// Lower-level schedulers' indices in this table
    ADAP_Uint16                 ClusterId;              // ENET Cluster ID (==Port==Queue)
    ADAP_Uint16					weight;
} tSchedulerDb;

typedef struct
{
    ADAP_Uint16                      UseCount;      /*ENET Queues which are using this WRED profile */
    MEA_WredProfileId_t              WredProfileId;
} tWredProfile;

typedef struct
{
	ADAP_Bool					valid;
	tEnetHal_QoSQEntry          issQueue;
	tEnetHal_QoSQtypeEntry      issQueueType;
	tEnetHal_QoSREDCfgEntry     issWredEntry[3];
    ADAP_Uint16                 ClassIdx;               //Associated Class(ifier) Index into the Classifier table
    ADAP_Uint16                 iss_PortNumber;
    ADAP_Uint16                 SchedulerIdx;           //Associated Scheduler Index into the Scheduler table
    ADAP_Uint16                 PolicyIdx;              //Associated Policy Index into the Policy table
    ENET_QueueId_t              ClusterId;              //ENET Queue ID (Cluster) to which this scheduler is linked
    ENET_PriQueueId_t           PriQueueIdx;            //Priority queue inside the ENET Queue object the scheduler associated with
    ADAP_Uint16                 WredProfileIdx;         //The Wred Entry Table entry, which manages the ENET Wred Profile which this queue is use
} tQueueDb;

typedef struct
{
	ADAP_Uint16 			aging_Time;
	ADAP_Uint16 			temp_aging_Time;
	MEA_Bool 				recover_aging_Time;
	ADAP_Bool				L2TunnelingEnable;
	ADAP_Uint16				default_flowMarkingID;
	ADAP_Uint16				default_cosMappingID;
	ADAP_Uint8				mirroringStatus;
} tGlobalsDb;

// MTU Flow profile database
typedef struct
{
    ADAP_Bool valid;
    ADAP_Uint32 vlanId;
    ADAP_Uint16 mtuProfId;
} tMtuFlowProfDb;

typedef enum
{
    LIMITER_PROF_VLAN,
    LIMITER_PROF_IFINDEX,
    LIMITER_PROF_DEFAULT,
    LIMITER_PROF_UNSET
} eLimiterProfType;

typedef struct
{
    ADAP_Bool valid;
    MEA_Limiter_t limiterId;
    eLimiterProfType type;
    ADAP_Uint32 vlanId;
    ADAP_Uint32 ifIndex;
} tLimiterProfDb;

typedef struct{
  ADAP_Bool valid;
  ADAP_Uint32 vlanId;
  ADAP_Int32 filterId;
  ADAP_Int32 filterPriority;
} tArpFilterParams;

typedef struct
{
	// port mapping for third party stack
	ADAP_Uint32 	number_of_physical_ports;
	ADAP_Uint32 	logical_ports[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_Port_t 		physical_ports[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint8      interfaceType[MAX_NUM_OF_SUPPORTED_PORTS];

	// port mapping for open flow
	ADAP_Uint32 	num_of_onf_physical_ports;
	ADAP_Uint32 	logical_onf_ports[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_Port_t 		physical_onf_ports[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_Bool 		port_part_of_lag[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Bool		valid_onf_ports[MAX_NUM_OF_SUPPORTED_PORTS];

	// default policer id
	MEA_Policer_prof_t  aPolicer_id[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_TmId_t         aTm_id[MAX_NUM_OF_SUPPORTED_PORTS];

	MEA_TmId_t    		tm_Id[MAX_NUM_OF_SUPPORTED_PORTS];

	// serviceId database
	tServiceDb      tAllocateServiceId[MAX_NUMBER_OF_SERVICES];

	// bridge domain configuration
	tVpnDb			 vpnDbPool[ADAP_NUMBER_OF_BRIDGE_DOMAIN];
	AdapLinkList    tBridgeDomainDb; // tBridgeDomain data structure
	pthread_mutex_t bridge_mutex;
	pthread_mutex_t nat_mutex;

	tGlobalsDb		tGlobals;

	// pvid
	tPvidDInfo	tPvidDb[MAX_NUM_OF_SUPPORTED_PORTS];

	//state status
	eAdap_PortStatus tportState[MAX_NUM_OF_SUPPORTED_PORTS];

	//acceptable type
	ADAP_Uint8 acceptableType[MAX_NUM_OF_SUPPORTED_PORTS];

	// user default priority
	ADAP_Uint32  defPriority[MAX_NUM_OF_SUPPORTED_PORTS];

	// lxcp handle
	MEA_LxCp_t tLxcpId[MAX_NUM_OF_SUPPORTED_PORTS];
	MEA_LxCp_t tLxcpErpsId [MAX_NUM_OF_SUPPORTED_PORTS];

	// for ip routing
	adap_rbtree			*adp_nextHopIpPool;
	tIpInterfaceTable		adp_IpInterface[MAX_NUM_OF_L3_INTERFACES];
	adap_rbtree			*adp_nextRoute;
	tIpv6InterfaceTable             adp_Ipv6Interface[MAX_NUM_OF_L3_INTERFACES];
	AdapLinkList                    Ipv6NeighborCacheList;
	AdapLinkList                    Ipv6RoutingTable;
	tInternalPorts			internalPorts[MAX_NUM_OF_INTERNAL_PORTS];
	MEA_Action_t			RouteActionSendToCpu;

	// set only if hw init done
	ADAP_Bool				hw_init_done;

	ADAP_Uint32				globalBitmap; // this is for board specific HW configuration

#ifdef	MEA_NAT_DEMO
	// for nat demo
	tIcmpTable				icmpTableDb[MAX_NUMBER_ICMP_ECHO_SESSIONS];
	tTcpUdpTable			tcpUdpTable[MAX_NUMBER_OF_L4_CONNECTION];
	ADAP_Uint16				l4Port;
#endif

	AdapLinkList    	tAclLinkList;


	ADAP_Uint32 		BridgeMode;
	ADAP_Uint32	    	providerBridgePortMode[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32	    	providerCoreBridgePortMode[MAX_NUM_OF_SUPPORTED_PORTS];
	tAcceptFrameType	AccepFrameTypeMode[ADAP_VLAN_MAX_NUMBER_OF_ACCEPTABLE_FRAMES_TYPE];

	AdapLinkList    	tSVlanMapLinkList;

	AdapLinkList    	tPepPvidLinkList;

	tEnetHal_VlanId 		defaultVlanId;

	tEnetHal_FsNpL3IfInfo	tL3RouterPort[MAX_NUM_OF_SUPPORTED_PORTS];

	tIpRoleData			tIpRolesDb[MAX_HPM_TABLES];
	tParentControl		tParentControl[MAX_PARENT_CONTROL_ENTRIES];

	tDatabaseShaperParams tShaperDb[MAX_NUM_OF_SUPPORTED_PORTS];

	tAclHierarchy		aclHierarchy[MAX_ACL_PROF_NUM];
	tFilterList			tFilterList[MAX_NUMBER_OF_WHITE_LIST];

	tDbaseHwAggEntry	aggrEntry[MAX_NUM_OF_AGGR_ENTRY];
	tHwAggVirtualPort   lagVirtualPort[MAX_NUM_OF_AGGR_ENTRY];

	tAdapClusterPerPort	adapCluserDb[MAX_NUM_OF_SUPPORTED_PORTS];
	tMulticastMac		multicastMac[ADAP_NUMBER_OF_MULTICAST_MAC];
	tMacLearningTable	macLearning[ADAP_MAX_NUMBER_OF_MAC_LEARNING];
	tNaptRule			adapNaptRule[MAX_NAT_ENTRIES_PER_TABLE];

	tLinuxIpInterface   adapLinuxInf[MAX_NUM_OF_SUPPORTED_PORTS];

	tTunnelService		tunnelService[MAX_NUMBER_OF_TUNNEL_SESSION];

	tLinuxArpCache		linuxArpCache[MAX_NUM_OF_ARP_CACHE];

	tAdapVxlanSnoop		adapVxlanSnoop[MAX_NUMBER_OF_VXLAN_TUNNEL];

	tVxLanFlooding		vxlanFloodArr[D_MAX_VXLAN_FLOODING];


	
	//Ports clusters database
	tClusterData		ClustersTable[MAX_NUM_OF_SUPPORTED_PORTS];
	//Ports database
	tPortDb 			PortsTable[MAX_NUM_OF_SUPPORTED_PORTS];
	tMstpVlanDb			MstpVlanTable[ADAP_NUM_OF_SUPPORTED_VLANS];
	tMstpInstDb			MstpInstTable[ADAP_MAX_NUM_OF_MSTP_INSTANCES+1];
	tFlowMarkingProfileUse FlowMarkingUseTable[MAX_NUM_OF_FLOW_MARKING_PROFILES];
	tPnacAutorizedDb 	pnacAutorized[MAX_NUM_OF_SUPPORTED_PORTS];
	tSvlanRelayDb    	svlanRelay[MAX_NUM_OF_SUPPORTED_PORTS];
	tPcpConfiguration   pcpConfig[MAX_NUM_OF_SUPPORTED_PORTS];
	tPbPcpInfoPri 		ingressPcpInfo[MAX_NUM_OF_SUPPORTED_PORTS];
	tEditingMapping 	editMapping[ADAP_MAX_NUMBER_OF_VLAN_EDITING_MAPPING];
	// QoS
	tClassifierDb		ClassifierTable[MAX_NUM_OF_CLASSIFIERS];
	tPolicyDb			PolicyTable[MAX_NUM_OF_POLICERS];
	tMeterDb			MeterTable[MAX_NUMBER_OF_METERING];
	tSchedulerDb		SchedulerTable[MAX_NUM_OF_SCHEDULERS];
	tQueueDb			QueueTable[MAX_NUM_OF_QUEUES];
	tWredProfile        WredProfTable[MAX_NUM_OF_WRED_PROFILES];
	
	tAdapAclProfile     adapAclProfile[MAX_ACL_FILTER_NUM];
	tAdapAclFiltProfile adapAclProfileNum[MAX_ACL_PROF_NUM];
	tAceVirtualFunc		aceVirtualDb[MAX_NUM_OF_ACE_VIRTUAL_FUNC];
	
	tAceCtrlAdmin		aceCtrlAdmin[MAX_NUM_OF_ACE_VIRTUAL_FUNC];
	


	tEnetOpenvSwitchsDb openVswitchDb; //this database active only in case of openvswitch configuration
	ADAP_Uint32 		NATRouterStatus;
	tNaptAddressTable	adapNaptAddressTable[MAX_NAT_ENTRIES_PER_TABLE];
        tFailedArp              FailedPPPArp[MAX_NUM_OF_FAILED_PPP_ARP];

    tMtuFlowProfDb      mtuFlowProfDb[MAX_MTU_FLOW_POLICERS];
    tLimiterProfDb      limiterProfDb[MAX_NUM_OF_LIMITER_PROFILE];
    tArpFilterParams    arpFilterParams[MAX_NUM_OF_ARP_FILTERS];
	ADAP_Uint32 		McastMode;
}tAdap_AdapDatabase;

#define    IF_L3VLAN    0
#define    IF_PPP       23


ADAP_Uint32 MeaAdapGetNumOfPhyPorts(void);
ADAP_Int32 enet_init_adaptor_database(void);
ADAP_Int32 enet_external_init_adaptor_database(ADAP_Int32 software_key);
ADAP_Int32 enet_init_enet_hw(void);
ADAP_Uint32 MeaAdapGetPhyPortFromLogPort(ADAP_Uint32 logPort,MEA_Port_t *pPhyPort);
void MeaAdapPrintEnetPorts(void);
ADAP_Uint32 MeaAdapGetLogPortFromPhyPort(ADAP_Uint32 *pLogPort,MEA_Port_t PhyPort);
ADAP_Uint32 MeaAdapIsInternalPort(MEA_Port_t PhyPort);
tServiceDb * MeaAdapAllocateServiceId(void);
tServiceDb * MeaAdapGetServiceByServiceId(MEA_Service_t serviceId);
ADAP_Uint32 MeaAdapFreeServiceId(MEA_Service_t serviceId);
tServiceDb * MeaAdapGetServiceId(MEA_Service_t serviceId);
tServiceDb * MeaAdapGetServiceIdByEditType(ADAP_Uint32 L2_protocol_type,ADAP_Uint32 ifIndex);
tServiceDb * MeaAdapGetServiceIdByEnetPort(ADAP_Uint32 L2_protocol_type,ADAP_Uint16 net_tag,ADAP_Uint16 inner_netTag_from_value,
																					ADAP_Uint32 sub_protocol_type,MEA_Port_t inPort);
ADAP_Uint32 MeaAdapGetLogPortFromIndex(ADAP_Uint32 *pLogPort,ADAP_Uint32 index);
MEA_Policer_prof_t MeaAdapGetDefaultPolicer(ADAP_Uint32 index);
ADAP_Uint32 MeaAdapSetDefaultPolicer(MEA_Policer_prof_t policerId,ADAP_Uint32 index);
MEA_TmId_t MeaAdapGetDefaultTm(ADAP_Uint32 index);
ADAP_Uint32 MeaAdapSetDefaultTm(MEA_TmId_t tmId,ADAP_Uint32 index);
void init_vpn_database(void);
ADAP_Uint32 allocate_vpn_database(ADAP_Uint16 *pVpn);
ADAP_Uint32 free_vpn_database(ADAP_Uint16 vpn);
void *ADAP_Malloc(ADAP_Uint32 memory_size);
void ADAP_Free(void *buffer);
void EnetAdaptorInit(ADAP_Int32 software_key);
void EnetAdaptorDestroy();
void Adap_init_bridgeDomain(void);
void Adap_init_accesslist(void);

ADAP_Uint32 Adap_L2AccessListToFilterEntry(tEnetHal_L2FilterEntry *pL2FilterEntry,
        tPolicyDb *pPolicy, tAdap_FilterEntry *out);
ADAP_Uint32 Adap_L3AccessListToFilterEntry(tEnetHal_L3FilterEntry *pL3FilterEntry,
        tPolicyDb *pPolicy, tAdap_FilterEntry *out);
ADAP_Uint32 Adap_pushFilterEntryToLinkListSorted(tAdap_FilterEntry *pFilterEntry);
ADAP_Uint32 Adap_popFilterEntryLinkList(tAdap_FilterEntry *pFilter,tAdap_FilterEntry *out);
ADAP_Uint32 Adap_UpdateFilterEntryInLinkList(tAdap_FilterEntry *pFilterEntry);
tAdap_FilterEntry *Adap_GetFilterEntryFromLinkList(ADAP_Uint32 filterNoId, eAclFilterTypes fType);
tAdap_FilterEntry *Adap_GetFirstAccessListLinkList();
tAdap_FilterEntry *Adap_GetNextAccessListLinkList(tAdap_FilterEntry *entry);
ADAP_Uint32 Adap_allocateBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId);
ADAP_Uint32 Adap_freeBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId);
tBridgeDomain * Adap_getBridgeDomain(ADAP_Uint32 swFdb,ADAP_Uint32 bridgeId);
ADAP_Uint32 Adap_getFirstBridgeFromLinkList(tBridgeDomain **pBridgeDomain);
ADAP_Uint32 Adap_getNextBridgeFromLinkList(tBridgeDomain **pBridgeDomain);
tBridgeDomain * Adap_getBridgeDomainByVlan(tEnetHal_VlanId VlanId,ADAP_Uint32 bridgeId);
ADAP_Uint32 initVlanDatabase(void);
ADAP_Uint16 getPortToPvid(ADAP_Uint32 ifIndex);
ADAP_Uint32 setPortToPvid(ADAP_Uint32 ifIndex,ADAP_Uint16 pvid);
ADAP_Uint32 Adap_addServiceToLinkList(tServiceDb *pServiceInfo,tBridgeDomain *pBridgeDomain);
ADAP_Uint32 Adap_getFirstServiceFromLinkList(tServiceDb **pServiceInfo,tBridgeDomain *pBridgeDomain);
ADAP_Uint32 Adap_getNextServiceFromLinkList(tServiceDb **pServiceInfo,tBridgeDomain *pBridgeDomain);
ADAP_Uint32 Adap_deleteAllServiceFromLinkList(tBridgeDomain *pBridgeDomain,func_linklosk_delete func);
ADAP_Uint32 getPortSpape(ADAP_Uint32 ifIndex,eAdap_PortStatus *pState);
ADAP_Uint32 setPortSpape(ADAP_Uint32 ifIndex,eAdap_PortStatus newState);
ADAP_Uint32 setLxcpId(ADAP_Uint32 ifIndex,MEA_LxCp_t lxcp);
ADAP_Uint32 getLxcpId(ADAP_Uint32 ifIndex,MEA_LxCp_t *pNewLxcp);
ADAP_Uint32 getPhyFromCfaInterface(ADAP_Uint32 cfaPort,ADAP_Uint32 *pLogPort);
ADAP_Uint32 adapIpInit(void);
tIpInterfaceTable *AdapAllocateIpInterface(ADAP_Uint32 IfIndex);
tIpInterfaceTable *AdapGetIpInterface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapIsVlanIpInterface(ADAP_Uint16 vlanId);
tServiceDb *Adap_freeServiceFromLinkList(tBridgeDomain *pBridgeDomain,ADAP_Uint32 u4IfIndex,ADAP_Uint32 L2_protocol_type,ADAP_Uint16 vlanId);
ADAP_Uint32 AdapAllocateEntryIpTable(tIpDestV4Routing **pAddNextHop,ADAP_Uint32 ipAddr);
ADAP_Uint32 AdapGetEntryIpTable(tIpDestV4Routing **pAddNextHop,ADAP_Uint32 ipAddr);
ADAP_Uint32 AdapFreeEntryIpTable(tIpDestV4Routing *pAddNextHop);
tIpDestV4Routing *AdapGetEntryHopIpTable(tIpInterfaceTable *pRouteTable,ADAP_Uint32 ipAddr);
ADAP_Uint32 AdapAddEntryHopIpTable(tIpInterfaceTable *pRouteTable,tIpDestV4Routing *pAddNextHop);
tIpDestV4Routing *AdapDelEntryHopIpTable(tIpInterfaceTable *pRouteTable,ADAP_Uint32 ipAddr);
ADAP_Uint32 AdapgetRouteInternalServiceId(MEA_Service_t  *pServiceId,MEA_Port_t portId);
ADAP_Uint32 AdapSetRouteInternalServiceId(MEA_Service_t  ServiceId,MEA_Port_t portId);
MEA_Action_t AdapGetRouteActionToCpu(void);
void AdapSetRouteActionToCpu(MEA_Action_t action);
ADAP_Uint32 MeaAdapSetIntType(ADAP_Uint8 IntType,ADAP_Uint32 index);
ADAP_Uint32 MeaAdapGetIntType(ADAP_Uint8 *pIntType,ADAP_Uint32 index);
ADAP_Uint32 MeaAdapGetInternalPortMask(MEA_Port_t PhyPort,ADAP_Uint32 *pMask);
tIpInterfaceTable *AdapGetFirstIpInterface(void);
tIpInterfaceTable *AdapGetNextIpInterface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapGetIpNextRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop);
ADAP_Uint32 AdapGetIpRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop);
ADAP_Uint32 AdapAllocIpNextRouteTable(tIpV4NextRoute **pAddNextHop,tIpV4NextRoute *pMatchNextHop);
ADAP_Uint32 AdapGetDefaultGatewayRouteTable(tIpV4NextRoute **pAddNextHop);
ADAP_Uint32 AdapFreeIpNextRouteTable(tIpV4NextRoute *pMatchNextHop);
ADAP_Uint32 adapIpv6Init(void);
tIpv6InterfaceTable *AdapAllocateIpv6Interface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapDeleteIpv6Interface(ADAP_Uint32 IfIndex);
tIpv6InterfaceTable *AdapGetIpv6Interface(ADAP_Uint32 IfIndex);
tIpv6InterfaceTable *AdapGetIpv6InterfaceByVlanId(ADAP_Uint16 vlanId);
tIpv6InterfaceTable *AdapGetFirstIpv6Interface(void);
tIpv6InterfaceTable *AdapGetNextIpv6Interface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapIsVlanIpv6Interface(ADAP_Uint16 vlanId);
ADAP_Uint32 AdapSetIpv6InterfaceType (ADAP_Uint32 IfIndex, ADAP_Uint32 IfType);
ADAP_Uint32 AdapIsPppIpv6Interface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapIpv6L3InterfaceGetInnerVlan(ADAP_Uint32 IfIndex);
tIpv6NeighborCache *AdapFindIpv6Neighbor(tIPv6Addr ipv6Addr);
tIpv6NeighborCache *AdapFindNextIpv6Neighbor(tIPv6Addr ipv6Addr);
tIpv6Neighbor *AdapGetIpv6NeighborEntry(ADAP_Uint32 ifIndex, tIPv6Addr ipv6Addr);
ADAP_Uint32 AdapAddIpv6NeighborEntry(ADAP_Uint32 ifIndex, tIpv6Neighbor *neighbor);
tIpv6Neighbor *AdapRemoveIpv6NeigborEntry(ADAP_Uint32 ifIndex, tIPv6Addr ipv6Addr);
ADAP_Uint32 AdapAddIpv6Route(tIpv6Route *Ipv6Route);
ADAP_Uint32 AdapDeleteIpv6Route(tIpv6Route *Ipv6Route);
tIpv6Route *AdapGetIpv6Route(tIPv6Addr ipv6Addr);
tIpV4NextRoute *AdapGetFirstIpNextRouteTable(void);
tIpV4NextRoute *AdapGetNextIpNextRouteTable(tIpV4NextRoute *pEntry);
ADAP_Uint32 AdapDeleteIpInterface(ADAP_Uint32 IfIndex);
tIcmpTable	*getIcmpTableFromEchoReply(ADAP_Uint32 sourceIp,ADAP_Uint16 EchoId);
ADAP_Uint32 getCfaFromIfindexInterface(ADAP_Uint32 *cfaPort,ADAP_Uint32 logPort);
void enetMilisecondSleep(ADAP_Uint32 milisecond);
ADAP_Uint32 AdapGetAccepFrameType(ADAP_Uint32 u4IfIndex,ADAP_Uint8 *pAcceptableType);
ADAP_Uint32 AdapSetAccepFrameType(ADAP_Uint32 u4IfIndex,ADAP_Uint8 acceptableType);
ADAP_Uint32 AdapGetUserPriority(ADAP_Uint32 u4IfIndex,ADAP_Uint32 *defPriority);
ADAP_Uint32 AdapSetUserPriority(ADAP_Uint32 u4IfIndex,ADAP_Uint8 defPriority);
tEnetHal_FsNpL3IfInfo *getRouterPortInfo(ADAP_Uint32 ifIndex);
tIpRoleData *adap_getIpRolesDb(ADAP_Uint8 	tableId);
tIpRoleData *adap_allocateIpRolesDb(ADAP_Uint8 	tableId);
tIpRole *adap_allocateIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask,mea_action_type_te type);
tIpRole *adap_getIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask);
tIpRole *adap_getFirstIpRoleId(tIpRoleData *pRoleData);
tIpRole *adap_getNextIpRoleId(tIpRoleData *pRoleData,tIpRole *pCurrRole);
tIpRole *adap_getIpv6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask);
tIpRole *adap_allocateIpV6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask,mea_action_type_te type);
tParentControl *adap_allocateParentConrol(ADAP_Uint32 saIp,ADAP_Uint32 daIp);
tParentControl *adap_getParentConrol(ADAP_Uint32 saIp,ADAP_Uint32 daIp);
ADAP_Bool adap_isIpV6RolesDb(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask);
ADAP_Uint32 adap_freeIpRoleId(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask);
ADAP_Uint32 adap_freeIpv6RoleId(tIpRoleData *pRoleData,tEnetHal_IPv6Addr *ipv6,tEnetHal_IPv6Addr *mask);
ADAP_Bool adap_isIpRolesDb(tIpRoleData *pRoleData,ADAP_Uint32 ip,ADAP_Uint32 mask);
ADAP_Uint32 adap_add_onf_port(ADAP_Uint32 logPort,ADAP_Uint32 phyPort);
ADAP_Uint32 adap_add_lag_port(ADAP_Uint32 phyPort);
ADAP_Bool adap_is_part_lag_port(ADAP_Uint32 logPort);
ADAP_Uint32 adap_get_onf_phy_port(ADAP_Uint32 logPort,MEA_Port_t *pPhyPort);
ADAP_Uint32 MeaAdapGetNumOfOnfPhyPorts(void);
tMeterDb *MeaAdapGetMeterParams(ADAP_Uint16 meter_id);
tMeterDb *MeaAdapGetMeterParamsByPolicerId(MEA_Policer_prof_t policer_prof_id);
void MeaAdapDeleteServiceFromPmDb(MEA_Service_t	 service);
void MeaAdapDeleteActionFromPmDb(MEA_Action_t	action);
tMeterDb *MeaAdapAllocateMeterParams(ADAP_Uint16 meter_id);
ADAP_Uint32 MeaAdapFreeMeterParams(ADAP_Uint16 meter_id);
ADAP_Uint32 adap_get_onf_log_port(ADAP_Uint32 index,ADAP_Uint32 *plogPort);
ADAP_Uint32 enet_hw_per_port_config(void);
void enet_hw_init_done(void);
ADAP_Bool is_enet_init_done(void);
ADAP_Uint32 ADAP_setPortShaper(tDatabaseShaperParams *shaperDb);
tDatabaseShaperParams *ADAP_getPortShaper(ADAP_Uint32 portId);
ADAP_Uint32 AdapFreeAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId);
tAclHierarchy *AdapGetAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId);
tAclHierarchy *AdapGetAclHierarchyByType(ADAP_Uint32 ifIndex,ADAP_Uint32 L2_protocol_type,ADAP_Uint16 vlanId);
tAclHierarchy *AdapAllocateAclHierarchy(ADAP_Uint8 table_id,MEA_Service_t serviceId);
tAclHierarchy *AdapGetFirstAclHierarchy(void);
tAclHierarchy *AdapGetNextAclHierarchy(tAclHierarchy *AclHi);
void AdapDeleteAclHierarchyTableId(ADAP_Uint8 table_id);
tDbaseHwAggEntry *AdapGetAggrDbByHdLagId(ADAP_Uint16 hwlagId);
tDbaseHwAggEntry *AdapAllocateAggrDbByHdLagId(ADAP_Uint16 hwlagId);
tDbaseHwAggEntry *AdapGetAggrDbByVirtualPort(ADAP_Uint32 virtualPort);
ADAP_Uint32 adapGetBoardSpecific(void);
void adapSetBoardSpecific(ADAP_Uint32 bitmap);
ADAP_Uint32 adapCreateCluster(MEA_Port_t enetPort,ENET_QueueId_t clusterId);
ADAP_Uint32 adapDeleteCluster(MEA_Port_t enetPort,ENET_QueueId_t clusterId);
ADAP_Uint32 adapGetFirstCluster(MEA_Port_t enetPort ,ENET_QueueId_t *pClusterId);
ADAP_Uint32 adapGetNextCluster(MEA_Port_t enetPort ,ENET_QueueId_t prevClusterId, ENET_QueueId_t *pClusterId);
ADAP_Uint32 adapFreeNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex);
tNaptRule *adapGetNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex);
tNaptRule *adapAllocNaptEntry(ADAP_Uint32 sourceIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 sourcePort,ADAP_Uint16 destPort,ADAP_Uint16 protocol,ADAP_Uint32 ifIndex);
tLinuxIpInterface *adap_get_interface_by_name(ADAP_Int8 *name);
ADAP_Uint32 adap_set_interfaceip(ADAP_Int8 *name,ADAP_Int8  	*host_ip);
tLinuxIpInterface *adap_get_interface_by_index(ADAP_Int32 index);
ADAP_Uint32 adap_getIndex_interface_by_name(ADAP_Int8 *name);
uint64_t ntohll(uint64_t n);
ADAP_Uint32 adap_create_tunnelService(tTunnelService *tunnel);
ADAP_Uint32 adap_create_arpEntry(tLinuxArpCache *arpCache);
tLinuxArpCache *adap_getfirst_newArpEntry(MEA_Port_t portId);
tTunnelService *adap_getfirst_tunnelServiceByPortId(MEA_Port_t  portId);
tTunnelService *adap_getnext_tunnelServiceByPortId(tTunnelService *currService);
void process_killed(void);
int is_process_killed(void);
tLinuxArpCache *adap_get_ArpEntry(ADAP_Uint32 index);
ADAP_Uint32 adap_getfirst_ArpEntry(ADAP_Uint32 *index);
ADAP_Uint32 adap_getNext_ArpEntry(ADAP_Uint32 *index);
tBridgeIntName *get_bridgeportByPortNumber(ADAP_Uint16 port_number,ADAP_Uint8 *bridge_name);
ADAP_Uint32 delete_bridgeport(tBridgeIntName *pBridgePort);
ADAP_Uint32 create_bridgeport(tBridgeIntName *pBridgePort);
ADAP_Uint32 ovsdb_set_entry(tEnetOvsdbTunnel	*vtep);
ADAP_Uint32 update_ovsdb_vtep_tunnel(void);
ADAP_Uint32 upset_ovsdb_local_vtep(void);
ADAP_Uint32 create_service_vlan_dest_ip(tAdapVxlanSnoop *pVxLanSnoop);
tEnetOvsdbTunnel *ovsdb_getfirst_entry(void);
tEnetOvsdbTunnel *ovsdb_getnext_entry(tEnetOvsdbTunnel	*vtep);
ADAP_Uint32 create_vxLanTunnel(tEnetOfVxlanTunnel *pBTunnel);
tEnetOfVxlanTunnel *get_vxLanTunnel_by_nameAndPort(ADAP_Uint8 *interface_name);
ADAP_Uint32 getnext_vxLanTunnel(ADAP_Uint32 i);
ADAP_Uint32 getfirst_vxLanTunnel(void);
tEnetOfVxlanTunnel *get_vxLanTunnel(ADAP_Uint32 i);
ADAP_Uint32 adap_set_vxlan_snooping(tAdapVxlanSnoop *pnewEntry);
tAdapVxlanSnoop *adap_get_vxlan_snooping_by_index(ADAP_Uint32 index);
ADAP_Uint32 adap_getfirstIndex_vxlan_snooping(void);
ADAP_Uint32 adap_getNextIndex_vxlan_snooping(ADAP_Uint32 index);
ADAP_Uint32 ovsdb_set_global_entry(tEnetVtepGlobal	*pEntry);
ADAP_Uint32  ovsdb_sync_remote_mac_entry(tEnetOvsDbSync *pVtepSync);
ADAP_Uint32  ovsdb_sync_local_mac_entry(tEnetOvsDbSync *pVtepSync);
tEnetOvsdbTunnel  *ovsdb_get_local_mac_entry(void);
tEnetVtepGlobal *ovsdb_get_global_entry(void);
ADAP_Uint32 adap_is_vxlan_flooding_exist(tVxLanFlooding *pVxlan);
tVxLanFlooding *adap_create_vxlan_flooding_exist(void);
ADAP_Uint32 ovsdb_free_entry_by_vni(unsigned int vni);
tEnetVtepSerDb *ovsdb_get_entry_by_vni(unsigned int vni);
tEnetVtepSerDb *ovsdb_alloc_entry_by_vni(void);
ADAP_Uint32 getLxcpErpsId(ADAP_Uint32 ifIndex,MEA_LxCp_t *pNewLxcpErps);
ADAP_Uint32 setLxcpErpsId(ADAP_Uint32 ifIndex,MEA_LxCp_t lxcpErps);


tEnetHal_MacAddr *AdapGetProvStpMacAddress(void);
tEnetHal_MacAddr *AdapGetProvGvrpMacAddress(void);
tEnetHal_MacAddr *AdapGetProvGmrpMacAddress(void);
tEnetHal_MacAddr *AdapGetProvMvrpMacAddress(void);

tEnetHal_MacAddr *AdapGetProvDot1xMacAddress(void);
tEnetHal_MacAddr *AdapGetProvLacpMacAddress(void);
tEnetHal_MacAddr *AdapGetProvEmliMacAddress(void);
tEnetHal_MacAddr *AdapGetProvLldpMacAddress(void);
tEnetHal_MacAddr *AdapGetStpMacAddress(void);
tEnetHal_MacAddr *AdapGetMmrpMacAddress(void);
tEnetHal_MacAddr *AdapGetGmrpMacAddress(void);
tEnetHal_MacAddr *AdapGetGvrpMacAddress(void);
tEnetHal_MacAddr *AdapGetMvrpMacAddress(void);
tEnetHal_MacAddr *AdapGetBcastMacAddress(void);
tEnetHal_MacAddr *AdapGetLldpMacAddress(void);

tEnetHal_MacAddr *AdapGetOspfMacMulticastAllRoutes(void);
tEnetHal_MacAddr *AdapGetOspfMacMulticastAllDrs(void);
tEnetHal_MacAddr *AdapGetRipMacMulticastAllRoutes(void);



void Adap_bridgeDomain_semLock(void);
void Adap_bridgeDomain_semUnlock(void);
void Adap_natDomain_semLock(void);
void Adap_natDomain_semUnlock(void);
ADAP_Uint32 MeaAdapSetDefaultTmId(MEA_TmId_t tmId,ADAP_Uint32 index);
MEA_TmId_t  MeaAdapGetDefaultTmId(ADAP_Uint32 index);
tMulticastMac *adap_getMulticastMacEntry(tEnetHal_MacAddr *macAddress);
tMulticastMac *adap_createMulticastMacEntry(tEnetHal_MacAddr *macAddress);
void adap_freeMulticastMacEntry(tEnetHal_MacAddr *macAddress);



#ifdef	MEA_NAT_DEMO
tIcmpTable	*allocate_IcmpTable(tIcmpTable *pTable);
tIpRoleData *adap_getIpRolesDb(ADAP_Uint8 	tableId);
tIpRoleData *adap_allocateIpRolesDb(ADAP_Uint8 	tableId);
tIcmpTable	*getIcmpTableFromEchoReply(ADAP_Uint32 sourceIp,ADAP_Uint16 EchoId);
tTcpUdpTable	*allocate_tcpUdpTable(tTcpUdpTable *pTable);
tTcpUdpTable	*getTcpUdpTable(tTcpUdpTable *pTable);
tTcpUdpTable  *getFirstTcpUdpTable(void);
tTcpUdpTable  *getNextTcpUdpTable(tTcpUdpTable *pTable);
void refreshTcpUdpTable(void);
void printTcpUdpTable(void);
ADAP_Uint32	free_tcpUdpTable(tTcpUdpTable *pTable);
#endif

void AdapSetBridgeMode(ADAP_Uint32 u4BridgeMode);
tEnetHal_VlanId getDefaultVlanId(void);
void setDefaultVlanId(tEnetHal_VlanId vlanId);
ADAP_Uint32 AdapGetBridgeMode(void);
ADAP_Uint32 AdapSetProviderBridgeMode(ADAP_Uint32  mode,ADAP_Uint32 ifIndex);
ADAP_Uint32 AdapGetProviderBridgeMode(ADAP_Uint32  *pMode,ADAP_Uint32 ifIndex);
ADAP_Uint32 AdapSetProviderCoreBridgeMode(ADAP_Uint32  mode,ADAP_Uint32 ifIndex);
ADAP_Uint32 AdapGetProviderCoreBridgeMode(ADAP_Uint32  *pMode,ADAP_Uint32 ifIndex);
ADAP_Uint32 Adap_addSVlanMapListToLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap);
tEnetHal_VlanSVlanMap *Adap_freeSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap);
tEnetHal_VlanSVlanMap * Adap_getSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMap);
ADAP_Uint32 AdapSetAccepFrameTypeMode(ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 u1AccepFrameType);
ADAP_Uint32 AdapGetAccepFrameTypeMode(ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 *pu1AccepFrameType);
ADAP_Uint32 Adap_addPepPvidListToLinkList(tPepPvid *pPepPvid);
tPepPvid *Adap_freePepPvidListLinkList(tPepPvid *pPepPvid);
tPepPvid * Adap_getPepPvidListLinkList(tPepPvid *pPepPvid);
ADAP_Uint32 Adap_IsPepPvidFoundLinkList(ADAP_Uint16 Svlan, ADAP_Uint16 Cvlan,ADAP_Uint32 ifIndex);
ADAP_Uint32 Adap_getFirstSVlanMapListLinkList(tEnetHal_VlanSVlanMap **pVlanSVlanMap);
ADAP_Uint32 Adap_getNextSVlanMapListLinkList(tEnetHal_VlanSVlanMap **pVlanSVlanMap);
ADAP_Uint32 Adap_getAllSVlanMapListLinkList(tEnetHal_VlanSVlanMap *pVlanSVlanMapArr,ADAP_Uint32 *maxNumOfEntries,ADAP_Uint16 Svlan,ADAP_Uint32 ifIndex);
tServiceDb * MeaAdapGetServiceIdByVlanIfIndex(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 L2_protocol_type);
tServiceDb * MeaAdapGetServiceIdByPortTrafType(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 traffic_type);
tServiceDb * MeaAdapGetServiceIdByPortTrafTypeSubProtocol(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex,ADAP_Uint32 traffic_type,
        ADAP_Uint32 subProtocolType);


tAceVirtualFunc *adap_get_AceVirtualFunc(unsigned char 	*AceName);
ADAP_Uint32 adap_set_AceVirtualFunc(tComAceVirtualFunc *aceFunc);
ADAP_Int32 create_AceVirtualFuncApp(unsigned char 	*AceName);
ADAP_Int32 delete_AceVirtualFuncApp(unsigned char 	*AceName);\
ADAP_Uint32 AdapAllocAclProfile(void);
ADAP_Uint32 AdapAllocAclProfileFilterIdFtype(ADAP_Uint32 filterId, eAclFilterTypes ftype,
                    ADAP_Uint32 inPort, ADAP_Uint32 vlanId, ADAP_Uint32 priField);
tAdapAclProfile *AdapGetAclProfilePtr(ADAP_Uint32 acl_profile, ADAP_Uint32 filterId);
ADAP_Uint32 AdapGetAclProfileFtype(ADAP_Uint32 filterId, eAclFilterTypes ftype, ADAP_Uint32 InPort);
ADAP_Uint32 AdapGetAclProfileInPort(ADAP_Uint32 inPort, eAclFilterTypes ftype,ADAP_Uint32 vlanId, ADAP_Uint32 priField);
ADAP_Uint32 AdapfreeAclProfile(ADAP_Uint32 acl_profile, ADAP_Uint32 filterId);
ADAP_Uint32 IncRefCountAclProfile(ADAP_Uint32 acl_profile);
ADAP_Uint32 adap_del_AceVirtualFunc(unsigned char 	*AceName);
ADAP_Uint32 adap_del_AceAdminCtrl(unsigned int 	accesslist_priorityid);
tAceCtrlAdmin *adap_get_AceAdminCtrl(unsigned int 	accesslist_priorityid);
ADAP_Uint32 adap_set_AceAdminCtrl(tComAceVirtualCtrlAdmin *aceFunc);
ADAP_Int32 create_AceAdminCtrl(unsigned int accesslist_priorityid);
ADAP_Int32 delete_AceAdminCtrl(unsigned int accesslist_priorityid);





tSchedulerDb *adap_createSchedulerEntry(tEnetHal_QoSSchedulerEntry *pSchedTable);
ADAP_Uint32 adap_freeSchedulerEntry(ADAP_Int32 ifIndex, ADAP_Int32 SchedId);
tSchedulerDb *adap_getSchedulerEntry(ADAP_Int32 ifIndex, ADAP_Int32 SchedId);
tSchedulerDb *adap_getSchedulerEntryByIndex(ADAP_Uint16 SchedIdx);
ADAP_Uint16 adap_getSchedulerIndex(ADAP_Int32 ifIndex, ADAP_Int32 SchedId);
ADAP_Uint32 adap_updateSchedulerEntry(tEnetHal_QoSSchedulerEntry *pSchedTable);
ADAP_Uint32 adap_SchedHierarchyMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint32 u4NextSchedId, ADAP_Int16 i2HL,ADAP_Uint16 u2Sweight);
ADAP_Uint32 adap_SchedHierarchyUnMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint32 u4NextSchedId, ADAP_Uint16 i2HL);

tQueueDb *adap_createQueueEntry(ADAP_Int32 i4IfIndex, tEnetHal_QoSQEntry * pQEntry, tEnetHal_QoSQtypeEntry * pQTypeEntry, tEnetHal_QoSREDCfgEntry * papRDCfgEntry[]);
ADAP_Uint32 adap_freeQueueEntry(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId);
tQueueDb *adap_getQueueEntry(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId);
tQueueDb *adap_getQueueEntryByIndex(ADAP_Uint16 QueueIdx);
ADAP_Uint32 adap_CreateClusters(void);
ADAP_Uint32 adap_CreateDefaultLag(void);
ADAP_Uint16 adap_getNumDefinedQueuesForCluster(ADAP_Uint16 iss_PortNumber, ENET_QueueId_t clusterId);
ENET_QueueId_t adap_GetAssignedClusterId(ADAP_Int32 i4IfIndex, tSchedulerDb * pSchedEntry, ADAP_Uint16 SchedIndex);
ADAP_Int32 adap_SetAgingTime(ADAP_Uint32 AgingTime);
ADAP_Uint32 adap_GetAgingTime(void);
ADAP_Int32 adap_SetTempAgingTime(ADAP_Uint32 AgingTime);
ADAP_Uint32 adap_GetTempAgingTime(void);
ADAP_Int32 adap_SetRecoverAgingTime(ADAP_Bool recover_aging_Time);
ADAP_Bool adap_GetRecoverAgingTime(void);
ADAP_Bool adap_CheckVlanForPort(ADAP_Uint32 u4IfIndex, tEnetHal_VlanId VlanId, ADAP_Uint16 *vpn);
ADAP_Bool adap_isProcessTerminated(void);
void adap_setProcessTerminated(void);

ADAP_Uint16 adap_setLagPolicyFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint32 u4SelectionPolicy);
ADAP_Uint32 adap_getLagPolicyFromAggrId(ADAP_Uint16 u2AggIndex);
ADAP_Uint16 adap_clearLagPortFromAggrId(ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber);
ADAP_Uint16 adap_EnableLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber);
ADAP_Uint16 adap_NumOfLagPortFromAggrId(ADAP_Uint16 u2AggIndex);
ADAP_Uint16 adap_isLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber);
ADAP_Uint32 adap_isPortPartOfLag(ADAP_Uint16 u2PortNumber);
ADAP_Uint16 adap_RemoveLagPortFromAggrId(ADAP_Uint16 u2AggIndex,ADAP_Uint16 u2PortNumber);
ADAP_Uint16 adap_setLagPortFromAggrId(ADAP_Uint16 u2AggIndex, ADAP_Uint16 u2PortNumber);
ADAP_Uint16 adap_getLagIndexFromAggrId(ADAP_Uint16 u2HwAggIdx);
tDbaseHwAggEntry *adap_getFirstLagInstanceFromIndex(void);
tDbaseHwAggEntry *adap_getNextLagInstanceFromIndex(ADAP_Uint16 u2HwAggIdx);
tDbaseHwAggEntry *adap_getLagInstanceFromIndex(ADAP_Uint16 u2Idx);
tDbaseHwAggEntry *adap_getLagInstanceFromAggrId(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_DeleteLagInstace(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_CreateLagInstace(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_SetLagVirtualPort(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 virtualPort);
ADAP_Uint16 adap_GetLagVirtualPort(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_SetLagMasterPort(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 masterPort);
ADAP_Uint16 adap_GetLagMasterPort(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_GetLagAggrFromMasterPort(ADAP_Uint16 masterPort);
ADAP_Uint16 adap_SetEnetLagId(ADAP_Uint16 u2HwAggIdx,ADAP_Uint16 id);
ADAP_Uint16 adap_GetEnetLagId(ADAP_Uint16 u2HwAggIdx);
ADAP_Uint16 adap_getFreeVirtualPort(void);
ADAP_Uint16 adap_setFreeVirtualPort(ADAP_Uint16 virtualPort);
ADAP_Uint16 adap_IsPortChannel(ADAP_Uint16 issPort);
void adap_SetMasterPort(ADAP_Uint32 u4IfIndex,ADAP_Uint16 masterPort);
ADAP_Uint16 adap_GetMasterPort(ADAP_Uint32 u4IfIndex);
void adap_PortSetVirtualPort(ADAP_Uint32 u4IfIndex,ADAP_Uint16 lagVirtualPort);
ADAP_Uint16 adap_PortGetVirtualPort(ADAP_Uint32 u4IfIndex);
void adap_PortSetEnetLagId(ADAP_Uint32 u4IfIndex,ADAP_Uint16 lagId);
ADAP_Uint16 adap_PortGetEnetLagId(ADAP_Uint32 u4IfIndex);
ADAP_Int32 adap_SetDefaultSid(ADAP_Uint32 PortId, MEA_Service_t sId);
MEA_Service_t adap_GetDefaultSid(ADAP_Uint32 PortId);
ADAP_Bool adap_VlanCheckUntaggedPort(tEnetHal_VlanId VlanId,ADAP_Uint16 u2PortNumber);
ADAP_Bool adap_VlanCheckTaggedPort(tEnetHal_VlanId VlanId,ADAP_Uint16 u2PortNumber);
tBridgeDomain * Adap_getBridgeDomainByFdb(ADAP_Uint32 u4Fid);
ADAP_Uint32 adap_insertEntryToMacLearning(tMacLearningTable *newEntry);
ADAP_Uint32 adap_getEntryFromMacLearning(tMacLearningTable *newEntry,ADAP_Uint32 fdbId,tEnetHal_MacAddr *pMacAddr);
ADAP_Uint32 adap_getEntryFromMacLearningFromIndex(tMacLearningTable *newEntry,ADAP_Uint32 index);
ADAP_Uint32 adap_IsMacLearningTableFull(void);
ADAP_Uint32 adap_resetMacLearningTable(void);
tBridgeDomain * Adap_getBridgeDomainByVpn(ADAP_Uint16 u2Vpn);
tServiceDb * MeaAdapGetFirstServicePort(ADAP_Uint32 u4IfIndex);
tServiceDb * MeaAdapGetNextServicePort(ADAP_Uint32 u4IfIndex,MEA_Service_t ServId);
tServiceDb * MeaAdapGetFirstServiceVlan(ADAP_Uint32 vlanId);
tServiceDb * MeaAdapGetNextServiceVlan(ADAP_Uint32 vlanId,MEA_Service_t ServId);
ADAP_Uint32 adap_InsertEntryToFilterList(MEA_Filter_t 	FilterId,MEA_Service_t   Service_id, tEnetHal_MacAddr	*macAddr);
MEA_Filter_t adap_DeleteEntryFromFilterListMacAddr(MEA_Service_t   Service_id, tEnetHal_MacAddr *pMacAddr);
MEA_Filter_t adap_DeleteEntryFromFilterList(MEA_Service_t   Service_id);
ADAP_Uint16 adap_GetPortPriorityMapPriority(ADAP_Uint32 u4IfIndex);
ADAP_Uint16 adap_GetPortPriorityCosPriority(ADAP_Uint32 u4IfIndex);
ADAP_Int32 adap_SetPortPriorityMap(ADAP_Uint32 u4IfIndex, ADAP_Uint16 priMapProfileId);
ADAP_Int32 adap_GetPortPriorityMap(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *priMapProfileId);
ADAP_Int32 adap_SetPortCosPriority(ADAP_Uint32 u4IfIndex, ADAP_Uint16 priCosProfileId);
ADAP_Int32 adap_GetPortCosPriority(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *priCosProfileId);
ADAP_Int32 adap_SetFlowMarkingProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 flowMarkingProfile);
ADAP_Int32 adap_GetFlowMarkingProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *flowMarkingProfile);
ADAP_Uint32 adap_AllocateFlowMarkingProfile (ADAP_Uint16 *AllocatedProfileId, ADAP_Uint32 PortNum);
ADAP_Uint32 adap_ReuseFlowMarkingProfile (ADAP_Uint16 ProfileId);
MEA_Status adap_FreeFlowMarkingProfile (ADAP_Uint16 ProfileId);
void adap_SetTunnelTranslation(ADAP_Bool Enable);
ADAP_Bool adap_GetTunnelTranslation(void);
ADAP_Uint32 adap_SetVlanPortTunnelType(ADAP_Uint32 PortId, ADAP_Uint16 PortTunnelType);
ADAP_Uint16 adap_GetVlanPortTunnelType(ADAP_Uint32 PortId);
ADAP_Uint32 adap_SetPBPortServVlanCustomerVlan(ADAP_Uint32 PortId,ADAP_Uint16 ServiceVlan);
ADAP_Uint16 adap_GetPBPortServVlanCustomerVlan(ADAP_Uint32 PortId);
void adap_SetTunnelOption(ADAP_Uint16 PortIndex,eEnetHal_VlanHwTunnelFilters filterOption,ADAP_Uint8 value);
ADAP_Uint8 adap_GetTunnelOption(ADAP_Uint16 PortIndex, eEnetHal_VlanHwTunnelFilters filterOption);
ADAP_Bool adap_VlanCheckPortsInDomain(tBridgeDomain *pBridgeDomain, ADAP_Uint16 u2PortNumber);
ADAP_Uint32 adap_SetSpanningTreeMode (ADAP_Int16 issPort, eAdap_StpMode mode,ADAP_Uint8 status);
ADAP_Uint32 adap_GetSpanningTreeMode (ADAP_Int16 issPort, eAdap_StpMode *mode,ADAP_Uint8 *status);
ADAP_Uint32 adap_SetCepPortState (ADAP_Int16 issPort, ADAP_Int16 vlanId, ADAP_Uint8 status);
ADAP_Uint32 adap_GetCepPortState (ADAP_Int16 issPort, ADAP_Int16 vlanId, ADAP_Uint8 *status);
ADAP_Uint8 adap_MstpGetStateForVlan(ADAP_Uint32 u4IfIndex,ADAP_Uint16 VlanId);
ADAP_Uint8 adap_MstpGetStateForInstance(ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2InstanceId);
tMstpVlanDb* adap_MstpGetVlanInstance(ADAP_Uint16 VlanId);
tMstpInstDb* adap_MstpGetInstance(ADAP_Uint16 u2InstanceId);
void adap_MstpSetInstanceParams(ADAP_Uint16 u2InstanceId, ADAP_Uint16 instValue, ADAP_Uint32 u4IfIndex, ADAP_Uint16 portState);
void adap_MstpSetVlanInstanceParams(ADAP_Uint16 VlanId, ADAP_Bool isValid, ADAP_Uint16 u2InstanceId);
ENET_QueueId_t adap_GetPortQueueID(ADAP_Uint32 u4IfIndex, ADAP_Uint16 ClusterIdx);
tPnacAutorizedDb *adap_getPnacAutorized(ADAP_Uint32 u4IfIndex);
ADAP_Int32 MeaDrvDelEntryDeleteSrv (ADAP_Uint32 u4IssPort,tEnetHal_VlanId VlanId, ADAP_Uint8 u1IsTagged);
void adap_setServiceVlanRelay(ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan);
tSvlanRelayDb *adap_getServiceVlanRelay(ADAP_Uint32 u4IfIndex);
void adap_SetSvlanMap(ADAP_Uint16 PortIndex,tEnetHal_VlanSVlanMap   *pVlanSVlanMap);
tEnetHal_VlanSVlanMap *adap_GetSvlanMap(ADAP_Uint16 PortIndex);
ADAP_Uint16 adap_GetPBPortCustomerVlan(ADAP_Uint32 PortId);
void adap_SetPBPortCustomerVlan(ADAP_Uint32 PortId, ADAP_Uint16 CVlan);
tEnetHal_VlanSVlanMap *adap_GetPointerSVlanClassTable(ADAP_Uint32 PortId, tEnetHal_VlanId sVlanId);
ADAP_Uint32 adap_SVlanClassTableUpdate(tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry);
ADAP_Uint32 adap_VlanClassTableForceSelection(tBridgeDomain *pBridgeDomain, tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry);
tEnetHal_VlanId adap_SVlanClassTableGetFromIndex(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId,ADAP_Uint8 *u1PepUntag,ADAP_Uint8 *u1CepUntag,ADAP_Uint16 currIndex);
ADAP_Uint16 adap_SVlanClassTableGetNext(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId,ADAP_Uint16 currIndex);
ADAP_Uint16 adap_SVlanClassTableGetFirst(ADAP_Uint32 PortId, tEnetHal_VlanId SVlanId);
ADAP_Uint32 adap_SVlanClassTableDelete(tEnetHal_VlanSVlanMap 	*pVlanSVlanMapEntry);
ADAP_Int32 adap_GetFlowPolicerProfile(ADAP_Uint32 u4IfIndex, ADAP_Uint16 *profileId);
ADAP_Uint32 adap_SetFlowPolicerProfile(ADAP_Uint32 ifIndex, ADAP_Uint16 profileId);
tPcpConfiguration *adap_GetPcpConfiguration(ADAP_Uint32 u4IfIndex);
tPbPcpInfoPri *adap_GetIngressPcpInfo(ADAP_Uint32 u4IfIndex);
tServiceDb * MeaAdapGetServiceIdByPri(ADAP_Uint32 ifIndex,ADAP_Uint16 priority);
ADAP_Uint32 adap_SetTunnelCusToProActionId(ADAP_Uint16 PortIndex,MEA_Action_t id);
ADAP_Uint16 adap_GetTunnelCusToProActionId(ADAP_Uint16 PortIndex);
ADAP_Uint32 adap_SetTunnelProToCusActionId(ADAP_Uint16 PortIndex,MEA_Action_t id);
ADAP_Uint16 adap_GetTunnelProToCusActionId(ADAP_Uint16 PortIndex);
ADAP_Int32 adap_SetDefaultPolicerId(ADAP_Uint16 DefaultPolicerId,ADAP_Uint16 issPort);
ADAP_Uint16 adap_GetDefaultPolicerId(ADAP_Uint16 issPort);
ADAP_Int32 adap_SetDefaultPolicerIdTmId(ADAP_Uint16 tmId,ADAP_Uint16 issPort);
ADAP_Uint16 adap_GetDefaultPolicerIdTmId(ADAP_Uint16 issPort);
ADAP_Int32 adap_SetDefaultPolicerIdPmId(ADAP_Uint16 pmId,ADAP_Uint16 issPort);
ADAP_Int32 adap_GetDefaultPolicerIdPmId(ADAP_Uint16 issPort);
ADAP_Int32 adap_SetDefaultMappingId(ADAP_Uint16 mappingId,ADAP_Uint16 issPort);
ADAP_Uint16 adap_GetDefaultMappingId(ADAP_Uint16 issPort);
ADAP_Uint32 adap_VlanGetPortByVlanId(tEnetHal_VlanId VlanId);
ADAP_Int32 adap_SetDefaultFlowMarkingId(ADAP_Uint16 markingId);
ADAP_Uint16 adap_GetDefaultFlowMarkingId(void);
ADAP_Int32 adap_SetDefaultCosMappingId(ADAP_Uint16 mappingId);
ADAP_Uint16 adap_GetDefaultCosMappingId();
ADAP_Uint16 adap_getQueueIndexById(ADAP_Int32 i4IfIndex, ADAP_Uint32 QueueId);
tMeterDb *MeaAdapGetMeterByIndex(ADAP_Uint16 meterIdx);
ADAP_Uint16 MeaAdapGetMeterIdLinkedToIP (ADAP_Uint32 ipv4Addr);
ADAP_Uint32 adap_createClassifierEntry(tEnetHal_QoSClassMapEntry *pClassMapTable);
ADAP_Uint32 adap_freeClassifierEntry(ADAP_Int32 ClassId);
tClassifierDb *adap_getClassifierEntry(ADAP_Int32 ClassId);
tClassifierDb *adap_getClassifierEntryByIndex(ADAP_Uint16 ClassIdx);
ADAP_Uint16 adap_getClassifierIndex(ADAP_Int32 ClassId);
ADAP_Uint32 adap_updateClassifierEntry(ADAP_Uint32 idx, tEnetHal_QoSClassMapEntry *pClassMapTable);
ADAP_Uint32 adap_createPolicerEntry(tEnetHal_QoSPolicyMapEntry * pPlyMapEntry);
ADAP_Uint32 adap_freePolicerEntry(ADAP_Int32 PolicyId);
tPolicyDb *adap_getPolicerEntry(ADAP_Int32 PolicyId);
tPolicyDb *adap_getPolicerEntryByIndex(ADAP_Uint16 PolicyIdx);
ADAP_Uint16 adap_getPolicerIndex(ADAP_Int32 PolicyId);
ADAP_Uint32 adap_updatePolicerEntry(ADAP_Uint32 idx,
									tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
									tEnetHal_QoSInProfileActionEntry * pInProActEntry,
									tEnetHal_QoSOutProfileActionEntry * pOutProActEntry);
ADAP_Uint16 adap_getMeterIndex(ADAP_Int32 MeterId);
ADAP_Uint32 adap_updateMeterEntry(ADAP_Uint32 idx, tEnetHal_QoSMeterEntry * pMeterEntry);
ADAP_Uint32 adap_UpdateMeterPolicerClassifier(ADAP_Uint16 MeterIndex, ADAP_Uint16 PolicerIndex, ADAP_Uint16 ClassifierIndex, ADAP_Uint8 ToMap);
ADAP_Uint32 adap_MapClassToQueue(ADAP_Int32 i4IfIndex, ADAP_Uint16 ClassifierIndex, ADAP_Uint32 u4QId, ADAP_Uint8 u1Flag);
ADAP_Uint16 adap_SetEditMapping(MEA_EditingMappingProfile_Id_t      id);
ADAP_Uint16 adap_getEditMappingByIdx(ADAP_Uint16 idx);
ADAP_Uint32 adap_freeEditMappingByIdx(ADAP_Uint16 idx);
tServiceDb * MeaAdapGetServiceIdByVlanPortPri(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 L2_protocol_type,ADAP_Uint16 priority);
tServiceDb * MeaAdapGetServiceIdByTrafTypePortPri(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 traffic_type,
        ADAP_Uint16 priority, MEA_IngressPort_Ip_pri_type_t priorityType);
tServiceDb * MeaAdapGetServiceIdByTrafTypePortPriSubType(tEnetHal_VlanId vlanId, ADAP_Uint32 ifIndex, ADAP_Uint32 traffic_type,
        ADAP_Uint16 priority, MEA_IngressPort_Ip_pri_type_t priorityType, ADAP_Uint32 subProtocolType);
ADAP_Uint32 adap_UnmapClassFromPolicer(ADAP_Uint16 ClassifierIndex);
ADAP_Uint16 adap_GetVpnByVlan(tEnetHal_VlanId VlanId);
void adap_SetMeterIndexPerPort(ADAP_Uint32 u4IfIndex,ADAP_Int32 meterIndex);
ADAP_Int32 adap_GetMeterIndexPerPort(ADAP_Uint32 u4IfIndex);
ADAP_Uint32 adap_GetMeterIndexFromPolicer(ADAP_Uint16 PolicerIndex);

void adap_SetTrafficBlockStatusPerPort(ADAP_Uint32 u4IfIndex,ADAP_Bool blockStatus);
ADAP_Int32 adap_GetTrafficBlockStatusPerPort(ADAP_Uint32 u4IfIndex, ADAP_Bool *blockStatus);
tIpInterfaceTable *AdapGetIpInterfaceByVlanId(ADAP_Uint16 vlanId);
tServiceDb * MeaAdapGetSimpleServiceIdByPort(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged);
tServiceDb * MeaAdapGetSimpleServiceIdByPortSubType(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged,
        ADAP_Uint32 subProtocolType);
ADAP_Uint32 MeaAdapSetPmIdByMeter(tMeterDb *pMeter, MEA_PmId_t pmId, MEA_Service_t Service_Id);
tServiceDb * MeaAdapGetServiceIdByPortPriority(tEnetHal_VlanId VlanId, ADAP_Uint32 PortId, ADAP_Uint8 u1IsTagged,
										ADAP_Uint8  priority, MEA_IngressPort_Ip_pri_type_t priority_type);
ADAP_Int32 adap_SetMirroringStatus(ADAP_Uint8 MirroringStatus );
ADAP_Uint8 adap_GetMirroringStatus(void);

void AdapSetNATRouterStatus(int status);
int AdapGetNATRouterStatus(void);
ADAP_Uint32 AdapAddStaticNatEntry (tNaptAddressTable *pNatEntry);
tNaptAddressTable *
AdapGetNatEntryFromHosttoNetwork (ADAP_Uint32 localIpAddr, ADAP_Uint16 localPort);
tNaptAddressTable *
AdapGetNatEntryFromNetworktoHost (ADAP_Uint32 globalIpAddr, ADAP_Uint16 globalPort);

ADAP_Uint32 AdapSetInterfaceType (ADAP_Uint32 IfIndex, ADAP_Uint32 IfType);
ADAP_Uint32 AdapIsPppInterface(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapL3InterfaceGetInnerVlan(ADAP_Uint32 IfIndex);
ADAP_Uint32 AdapChkDoubleTaggedInt ();

void AdapClearAllArpTable ();

ADAP_Uint32 AdapCheckFailedArp (ADAP_Uint32 IpAddr);
ADAP_Uint32 AdapAddFailedArp (ADAP_Uint32 IpAddr);
ADAP_Uint32 AdapRemoveFailedArp (ADAP_Uint32 IpAddr);

ADAP_Uint32 AdapGetOrAddMtuProfForVlan(ADAP_Uint32 vlanId, ADAP_Uint16 *mtuProfId);
ADAP_Uint32 AdapGetRemoveMtuProf(ADAP_Uint16 mtuProfId);

tServiceDb * MeaAdapGetFirstL3ServiceIdByEnetPort(MEA_Port_t inPort);
tServiceDb * MeaAdapGetNextL3ServiceIdByEnetPort(MEA_Port_t inPort,MEA_Service_t ServId);
MEA_Status MeaDrvGetFirstGlobalMacAddress(tEnetHal_MacAddr *macAddr);

/* Mac Limiter DB functions */

ADAP_Uint32 AdapAllocLimiterProf(ADAP_Uint32 vlanId, ADAP_Uint32 ifIndex, eLimiterProfType type, MEA_Limiter_t limiterId);
ADAP_Uint32 AdapGetDefaultLimiterId(MEA_Limiter_t *limiterId);
ADAP_Uint32 AdapGetLimiterIdByVlan(ADAP_Uint32 vlanId, MEA_Limiter_t *limiterId);
ADAP_Uint32 AdapGetLimiterIdByPort(ADAP_Uint32 ifIndex, MEA_Limiter_t *limiterId);

// WRED db functions
MEA_WredProfileId_t AdapGetWredProfileByIdx(ADAP_Uint16 Idx);
void AdapSetWredProfileByIdx(MEA_WredProfileId_t WredProfileID, ADAP_Uint16 Idx);
ADAP_Bool AdapIsWredProfileInUse(ADAP_Uint16 Idx);
ADAP_Uint32 AdapIncWredProfileUseCount(ADAP_Uint16 Idx);
void AdapClearAllWredProfileUseCount(void);
ADAP_Bool adap_compareSchedulerEntry(tSchedulerDb *pSchedDb, tEnetHal_QoSSchedulerEntry *pSchedEntry);

/* Arp filter parameters */
ADAP_Uint32 AdapGetArpFilterParams(ADAP_Uint32 vlanId, ADAP_Int32 *filterId, ADAP_Int32 *filterPriority);
ADAP_Uint32 AdapCreateArpFilterParams(ADAP_Uint32 vlanId, ADAP_Int32 filterId, ADAP_Int32 filterPriority);
ADAP_Uint32 AdapRemoveArpFilterParams(ADAP_Uint32 vlanId);

void AdapSetMcastMode(ADAP_Uint32 u4McastMode);
ADAP_Uint32 AdapGetMcastMode(void);

#endif
