#ifndef _ADAP_LPM_TRIE_H
#define _ADAP_LPM_TRIE_H

#include "adap_lpminc.h"

#define  MAX_KEY_SIZE            256
#define  MAX_NUM_KEYS              4
#define  MAX_TRIE_SEM_NAME_SIZE   16
#define  MAX_NUM_TRIE               100
#define MAX_APPLICATIONS     64

typedef struct _RadixNodeHead * tTrieDbId;

typedef union _Key {
    UINT1  *pKey;
    UINT4  u4Key;
} tKey;

typedef struct _InputParams {
    tTrieDbId   pRoot;
    VOID     *pLeafNode;
    INT1      i1AppId;
    UINT1     u1PrefixLen;
    UINT1     u1Reserved1;
    UINT1     u1Reserved2;
    tKey      Key;
} tInputParams;


typedef struct _OutputParams {
    void   *pAppSpecInfo;
    tKey   Key;
} tOutputParams;

typedef struct _ScanOutParams {
    void   *pAppSpecInfo;
    tKey   *pKey;
    UINT4  u4NumEntries;
} tScanOutParams;

typedef tScanOutParams tDeleteOutParams;

typedef struct _TrieDbApp
{
    void      *pNextApp;
    void      *pNextAlternatepath;
    UINT4     u4Net;
    UINT4     u4Mask;
    UINT4     u4Tos;
    UINT4     u4NxtHop;
    UINT4     u4RtIfIndx;
    UINT2     u2RtType;
    UINT2     u2RtProto;
    UINT4     u4RtAge;
    UINT4     u4RtNxtHopAs;
    UINT4     u4RtMetric;
    UINT4     u4RowStatus;
} tTrieDbApp;


struct TrieDbAppFns 
{
    INT4 (*pAddAppSpecInfo) (tInputParams *pInputParams,
                             VOID *pOutputParams, VOID **ppAppPtr,
                             VOID *pAppSpecInfo);
    INT4 (*pDeleteEntry)    (tInputParams *pInputParams, VOID **ppAppPtr,
                             VOID *pOutputParams, VOID *pNxtHop, tKey Key);
    INT4 (*pSearchEntry)    (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID *pAppPtr);
    INT4 (*pLookupEntry)    (tInputParams *pInputParams, VOID *pOutputParams, 
                             VOID *pAppPtr, UINT2    u2KeySize, tKey Key);
    VOID *(*pInitScanCtx)   (tInputParams *pInputParams,
                             void (*AppSpecScanFunc) (VOID *),VOID *pScanOutParams); 
    VOID (*pDeInitScanCtx)  (VOID *pScanCtx);
    INT4 (*pScan)           (VOID  *pScanCtx, VOID **ppAppPtr, tKey Key);
    INT4 (*pScanAll)        (VOID *pScanCtx, VOID *pAppPtr, tKey Key);
    INT4 (*pUpdate)         (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID **ppAppPtr, void *pAppSpecInfo, UINT4 u4NewMetric);
    VOID *(*pInitDeleteCtx) (tInputParams *pInputParams, void (*AppSpecDelFunc)
                             (VOID *), VOID *pDeleteOutParams);
    VOID (*pDeInitDeleteCtx)(VOID *pDelCtx);
    INT4 (*pDelete)         (VOID *pDelCtx, VOID **ppAppPtr, tKey Key);
    INT4 (*pGetLeafInfo)    (tInputParams *pInParms, VOID * pOutParms);
    INT4 (*pBestMatch)      (UINT2  u2KeySize, tInputParams * pInputParams,
                             VOID *pOutputParams, VOID *pAppPtr, tKey Key);
    INT4 (*pGetNextEntry)   (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID *pAppPtr, tKey Key);
    INT4 (*pLookupEntryOverlap) (tInputParams *pInputParams,
                                 VOID *pOutputParams,
                                 VOID *AppSpecPtr,  UINT2 u2KeySize, tKey Key);
};

typedef struct _CrtParams {
    struct  TrieDbAppFns *AppFns;
    UINT4   u4Type;
    UINT4   u4NumRadixNodes;
    UINT4   u4NumLeafNodes;
    UINT4   u4NoofRoutes;
    int     u2KeySize;
    int     u1AppId;
    BOOLEAN bPoolPerInst;  /* TRUE - Indicates MEMPOOL per TrieDb instance
                            * FALSE - Indicates common MEMPOOL */
    BOOLEAN bSemPerInst;   /* TRUE - Indicates Sem per TrieDbInstance
                            * FALSE - Indicates common MEMPOOL */
    BOOLEAN bValidateType; /* TRUE - To Validate u4Type field 
                            * FALSE - u4Type field is invalid */
    UINT1   u1Reserved;
} tTrieDbCrtParams;

#define DLIST_NODE(type, ele) \
struct {            \
   type *pNext; \
   type *pPrev; \
} ele

#define DLIST_HEAD(type, ele) \
struct {              \
type *pNext; \
type *pPrev; \
} ele
struct _RadixNode;

typedef struct _RadixNodeHead
{
    struct _RadixNode   *pRadixNodeTop;
    struct TrieDbAppFns   *AppFns;
    tUtilSemId          SemId;
    UINT4               u4RouteCount;
    UINT4               u4Type;
    UINT2               u2KeySize;
    UINT2               u2AppMask;
    DLIST_HEAD (struct _LeafNode, NodeHead);
    BOOLEAN             bPoolPerInst; /* TRUE - Indicates MEMPOOL per TrieDb instance 
                                       * FALSE - Indicates common MEMPOOL */
    BOOLEAN             bSemPerInst;  /* TRUE - Indicates Sem per TrieDbInstance
                                       * FALSE - Indicates common MEMPOOL */
    BOOLEAN             bValidateType;/* TRUE - Validation will be done to avoid
                                       *        creating new trie instances with same key    
                                       * FALSE - New trie instance will be created
                                       *         without validation */
    UINT1               u1Reserved;
} tRadixNodeHead;

typedef struct _RadixNode
{
    struct _RadixNode *pParent;
    tKey              Mask;
    UINT1             u1NodeType;
    UINT1             u1Reserved1;
    UINT1             u1ByteToTest;
    UINT1             u1BitToTest;
    void              *pLeft;
    void              *pRight;
} tRadixNode;
                    
typedef struct _LeafNode
{
    tRadixNode  *pParent;
    tKey        Mask;
    UINT1       u1NodeType;
    UINT1       u1NumApp;
    UINT2       u2AppMask;  
    VOID        *pAppSpecPtr;
    tKey        Key;
    DLIST_NODE  (struct _LeafNode, NodeLink);
} tLeafNode;

typedef struct _TrieDbConfigParams
{
    UINT4     u4NumRadixNodes;
    UINT4     u4NumLeafNodes;
    UINT4     au4NumKey[MAX_KEY_SIZE + 1];
    UINT4     u4AlignmentByte;
} tTrieDbConfigParams;

#define DLIST_FIRSTNODE(root, ele)       ((root)->ele.pNext)
#define DLIST_LASTNODE(root, ele)        ((root)->ele.pPrev)
#define DLIST_NEXT(leaf, ele)            ((leaf)->ele.pNext)
#define DLIST_PREV(leaf, ele)            ((leaf)->ele.pPrev)

#define DLIST_INSERT_HEAD(root, head_ele, leaf , leaf_ele)\
do {                    \
       if ((leaf->leaf_ele.pNext = root->head_ele.pNext)) {     \
           root->head_ele.pNext->leaf_ele.pPrev = leaf;     \
       }                        \
       leaf->leaf_ele.pPrev = NULL;            \
       root->head_ele.pNext = leaf;            \
}while (0)


/* insert after */
#define DLIST_INSERT(root, root_ele, leaf, leaf_ele, after ) \
do {                \
    if ((leaf->leaf_ele.pNext = after->leaf_ele.pNext)) {      \
        after->leaf_ele.pNext->leaf_ele.pPrev = leaf;         \
    }                        \
    else {                        \
        root->root_ele.pPrev = leaf;        \
    }                        \
    leaf->leaf_ele.pPrev = after;            \
    after->leaf_ele.pNext = leaf;            \
}while (0)


#define DLIST_REMOVE_HEAD(root, head_ele, leaf_ele) \
do {                           \
    if (root->head_ele.pNext) {                    \
       if ((root->head_ele.pNext = root->head_ele.pNext->leaf_ele.pNext)) {\
            root->head_ele.pNext ->leaf_ele.pPrev = NULL;       \
        }\
        else                                  {  \
            root->head_ele.pPrev = NULL;       \
        }\
    }                            \
} while (0)

/* remove after */
#define DLIST_REMOVE(root, head_ele, leaf, leaf_ele, prev_leaf) \
do {            \
    if ((prev_leaf->leaf_ele.pNext = leaf->leaf_ele.pNext)) {        \
        leaf->leaf_ele.pNext->leaf_ele.pPrev = prev_leaf;    \
    }                                \
    else {                                \
        root->head_ele.pPrev = prev_leaf;     \
    }                                \
}while (0)


#define  RADIX_NODE                0
#define  LEAF_NODE                 1
#define  U4_SIZE                   4
#define  BYTE_LENGTH               8

#define  KEY_POOL_ID               2
#define  ALL_PROTO_ID             -1

/* error codes */
#define  LEAF_ALLOC_FAIL           0
#define  RADIX_ALLOC_FAIL          1
#define  KEY_ALLOC_FAIL            2
#define  MAX_KEY_SIZE_OVERFLOW     3
#define  MAX_APP_OVERFLOW          4
#define  MAX_INSTANCE_OVERFLOW     5
#define  SEM_CREATE_FAIL           6
#define  ALREADY_USED_APP_ID       7
#define  MEMORY_CREATE_FAIL        8

/* Maximum size of trie2 key.
 * which will be used in mempool creation */
#define  MAX_TRIE2_KEY_SIZE       32

#define  TRIE2_MAX_ROUTING_PROTOCOLS 16 /* MAX Routing protocols value is
                                         * 16 as per stdip.mib and 9 for
                                         * stdipv6.mib So setting for max
                                         * value. */
#define LPM_TRIE_APP_ID      1
#define LPM_TRIE_APP_IP6_ID  2
#define LPM_MAX_ROUTES     200

/* When FutureIp is not used IP_CFG_GET_NUM_MULTIPATH cannot   */
/* be called. Instead use a pre-defined value for the maximum  */
/* number of multipaths that can be stored.                    */
/* We choose the maximum number of multipaths to be the number */
/* of TOSs supported by FutureOspf, the other user of TrieDb     */

/* variables used by TrieDbConfigParams structure */
#define  NUM_1ST_KEY             4000

#define  KEY1_SIZE                  8
#define  KEY2_SIZE                 16
#define  KEY3_SIZE                 32
#define  KEY4_SIZE                 64

#define  NUM_2ND_KEY                0
#define  NUM_3RD_KEY                0
#define  NUM_4TH_KEY                0
#define  INVALID_NEXTHOP   0xffffffff
#define  TRIE_SEM_COUNT             1


#define TRIE_INIT_ROUTES_BITMASK(u1NoOfMultipath)   u1NoOfMultipath = 0
#define TRIE_INIT_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath = u1NoOfMultipath & 0xf0;
#define TRIE_INC_TOTAL_NUM_OF_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath += 16
#define TRIE_INC_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath += 1
#define TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE(u1NoOfMultipath) \
        { \
        u1NoOfMultipath = u1NoOfMultipath & 0xf0; \
        u1NoOfMultipath += 1; \
        }

#define TRIE_DEC_TOTAL_NUM_OF_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath -= 16

#define TRIE_DEC_NUM_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath -= 1

#define TRIE_COPY_TOTAL_NUM_OF_ROUTES(u1BitMask, u1NumOfMultipath) \
        u1NumOfMultipath = (u1BitMask >> 4)

#define TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES(u1BitMask, u1NumOfMultipath) \
        u1NumOfMultipath = (u1BitMask & 0x0f )

#define  COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT(pOutputParams,pDeleteOutParams,u4TmpNumEntries) \
   while (((tTrieDbApp *)pOutputParams->pAppSpecInfo) != NULL) {\
         ((tTrieDbApp **) pDeleteOutParams->pAppSpecInfo)[u4TmpNumEntries++] = pOutputParams->pAppSpecInfo;\
            pOutputParams->pAppSpecInfo = ((tTrieDbApp *)pOutputParams->pAppSpecInfo)->pNextAlternatepath;\
               }

/* Macros for Key test/manipulation */
#define   BIT_TEST(a, b)                  (a) & (b)
#define   SET_BIT(u1AppId, u2AppMask)     (u2AppMask) = (UINT2)((u2AppMask)| (0x0001 << (u1AppId)))
#define   RESET_BIT(i1AppId, u2AppMask)   (u2AppMask) = (UINT2)((u2AppMask)& (~(0x0001 << (i1AppId))))
#define   CHECK_BIT(i4AppId, u2AppMask)   ((u2AppMask) & (0x0001 << (i4AppId)))

#define DIFFBYTE(Key1, Key2, u1Size, u4Byte, u1Diff)\
 if ((u1Size) == U4_SIZE) {DIFF4BYTE(Key1.u4Key, Key2.u4Key, u4Byte, u1Diff);}\
 else               {DIFFNBYTE(Key1.pKey, Key2.pKey, u1Size, u4Byte, u1Diff);}

#define DIFF4BYTE(u4Key1, u4Key2, u4Byte, u1Diff) {u4Byte = (u4Key1 ^ u4Key2);\
    u4Byte =  (u1Diff  = (UINT1)(u4Byte >> 24)) ? 0 :\
              ((u1Diff = (UINT1)(u4Byte >> 16)) ? 1 :\
              ((u1Diff = (UINT1)(u4Byte >> 8))  ? 2 :\
              ((u1Diff = (UINT1)(u4Byte))       ? 3 : 4)));}

#define DIFFNBYTE(pKey1, pKey2, u1Size, u4Byte, u1Diff)\
   for (u4Byte = 0; (u4Byte < (UINT4) u1Size) &&\
                     !(u1Diff = (pKey1[u4Byte] ^ pKey2[u4Byte])); u4Byte++);

#define DIFFBIT(u1Bit, u1Diff) for(u1Bit = 0; !(u1Diff >> (7-u1Bit)); u1Bit++)

#define KEYCMP(Key1, Key2, u1Size)  (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key < (Key2).u4Key) ? -1 : 1) :\
         memcmp ((Key1).pKey, (Key2).pKey, (u1Size)))

#define KEYEQUAL(Key1, Key2, u1Size) (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key == (Key2).u4Key) ? 0 : 1) :\
         (memcmp ((Key1).pKey, (Key2).pKey, (u1Size))))

#define GETBYTE(Key, u1ByteNum, u1Size) (UINT1) (((u1Size) == U4_SIZE) ?\
 ((Key).u4Key >> BYTE_LENGTH*(U4_SIZE - 1 - u1ByteNum)) :\
 ((Key).pKey [u1ByteNum]))

#define KEYCOPY(Dst, Src, u1Size) do {             \
    if ((u1Size) > U4_SIZE)                        \
        adap_memcpy((Dst).pKey, (Src).pKey, (u1Size)); \
    else                                           \
       (Dst).u4Key = (Src).u4Key;                  \
    } while (0)


/* declarations of globals */
extern tRadixNodeHead    gaTrieDbInstance[MAX_NUM_TRIE];
extern tTrieDbConfigParams gTrieDbCfg;


/* Function Prototypes */
void
TrieDbRelease(INT4 i4QueueId, UINT1 * pu1BuffAddr);

UINT1 *
TrieDbAllocate (INT4 i4QueueId);

tRadixNodeHead     *
TrieDbGetInstance (UINT4 u4Type, UINT2 u2KeySize, UINT4 *pu4Instance);

tRadixNodeHead     *
TrieDbGetFreeInstance (UINT4 *pu4Instance);

INT4
TrieDbDoTraverse (UINT2 u2KeySize, tRadixNode *pRoot, tKey Key, tLeafNode **ppNode);


INT4
TrieDbInsertLeaf (UINT2   u2KeySize, tInputParams * pInputParams,
             VOID * pAppSpecInfo, tRadixNode * pParentNode,
             VOID *pOutputParams, tLeafNode **ppNewLeaf);

INT4
TrieDbInsertRadix (UINT2   u2KeySize, UINT1 u1BitToTest, UINT1 u1ByteToTest,
              tInputParams * pInputParams, VOID * pAppSpecInfo,
              tRadixNode *pParentNode, VOID * pOutputParams);

VOID
TrieDbDelRadixLeaf (tRadixNodeHead * pRoot, tLeafNode * pLeafNode,
                  UINT4 u4Instance);

VOID
TrieDbSetMask (UINT2   u2KeySize, VOID *pNode);

INT4
TrieDbGetBestMatch (UINT2   u2KeySize, tInputParams * pInputParams, VOID *pNode,
                   VOID *pOutputParams);

INT4
TrieDbGetBestMatch4 (UINT2   u2KeySize,
                                   tInputParams * pInputParams,
                                   VOID *pNode, VOID * pOutputParams);

VOID               *
TrieDbAllocateateNode (UINT2 u2KeySize, UINT1 u1NodeType, UINT4 u4Instance);

VOID
TrieDbError (UINT1 u1NumError);

VOID
TrieDbLibInitVariables (void);

INT4
TrieDbCreateSemName (INT4 u4Type, UINT1 *pu1SemName);

INT4
TrieDbGetSemId (VOID *pRoot, tUtilSemId *pSemId);

INT4
TrieDbGetTrieInstance (VOID *pRoot, UINT4 *pu4Instance);

INT4
TrieDbGetLinkListIndex (INT1 i1AppId,
                           UINT2 u2AppMask, UINT1 * pu1LinkListIndex);

UINT4
TrieDbGetBitDifference (UINT1 u1NumBytes,
                            UINT1 u1MaskFlag,
                            UINT1 * pu1Key1, UINT1 * pu1Key2);

INT4
TrieDbWalkAll (tInputParams * pInputParams,
                           INT4 (*AppSpecScanFunc) (VOID *),
                           VOID * pScanOutParams);

tLeafNode*
TrieDbGetPrevLeafNode (tLeafNode* pLeaf);

INT4
TrieDbOverlapMatch (tKey Key, UINT2 u2KeySize, tLeafNode* pNode, UINT1 *pu1OvlType);

INT4
TrieDbKeyMatches(tKey Key1, tKey Key2, UINT2 u2PrefixLen, UINT2 u2KeySize);

UINT2
TrieDbPrefixLen (tKey Key, UINT2 u2KeySize);

INT4
TrieDbDefaultRoute (tKey Key, UINT2 u2KeySize);

tLeafNode *
TrieDbFindFirstMatch (UINT2 u2KeySize, tInputParams *pInputParams, void *pNode);

tLeafNode *
TrieDbFindFirstMatch4 (UINT2 u2KeySize, tInputParams *pInputParams, void *pNode);

tLeafNode *
TrieDbFirstMatch (tInputParams *pInputParams);

tLeafNode *
TrieDbDoTraverseLessMoreSpecific  (tInputParams *pInputParams);
tRadixNode *
TrieDbDoTraverseLessSpecific (tInputParams *pInputParams);

tLeafNode *
TrieDbLeftmost (tRadixNode *pNode);

tLeafNode *
TrieDbRightmost (tRadixNode *pNode);

VOID *
TrieDbAllocateRadixNode (UINT2 u2KeySize, UINT4 u4Instance);

VOID *
TrieDbAllocateLeafNode (UINT2 u2KeySize, UINT4 u4Instance);

VOID
TrieDbReleaseNode (UINT1 u1NodeType, UINT4 u4Instance, UINT1 * pNode);

VOID
TrieDb2TakeSem (UINT4, tUtilSemId);

VOID
TrieDb2GiveSem (UINT4, tUtilSemId);

VOID
TrieDb2DelSem (UINT4, tUtilSemId);

/* Exported Functions */

INT4 TrieDbLibInit (void);   
INT4 TrieDbLibMemPoolInit (void);   
void TrieDbLibShut (void);   

tTrieDbId
     TrieDbCrt     (tTrieDbCrtParams * pCreateParams);

tTrieDbId
     TrieDbCreateInstance    (tTrieDbCrtParams * pCreateParams);

INT4 TrieDbDel     (tInputParams * pInputParams, void (*AppSpecDelFunc) (void*),
                  void * pDeleteOutParams);

tTrieDbId
     TrieDbCrtInstance     (tTrieDbCrtParams * pCreateParams);

INT4 TrieDbDelInstance     (tInputParams * pInputParams, void (*AppSpecDelFunc) (void*),
                  void * pDeleteOutParams);

INT4 TrieDbAdd     (tInputParams * pInputParams, void *pAppSpecInfo,
                  VOID *pOutputParams);

INT4 TrieDbRemove  (tInputParams  *pInputParams, VOID *pOutputParams,   
                  VOID *pNxtHop);   

INT4
TrieDbSearch (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4
TrieDbLookup (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4 TrieDbWalk    (tInputParams *pInputParams, 
                  INT4         (*AppSpecScanFunc)(VOID *),   
                  VOID         *pScanOutParams);   

INT4
TrieDbGetNext (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4 TrieDbLookupOverlap (tInputParams * pInputParams, VOID * pOutputParams,
                        VOID **);
INT4 TrieDbGetPrevNode  (tInputParams *pInputParams, VOID *pGetCtx,
                       VOID ** pAppSpecPtr, VOID **);
INT4 TrieDbGetFirstNode (tInputParams *pInputParams, VOID ** pAppSpecPtr,
                       VOID **);
INT4 TrieDbGetNextNode  (tInputParams *pInputParams, VOID *pGetCtx,
                       VOID ** pAppSpecPtr, VOID **);
INT4 TrieDbGetLastNode  (tInputParams *pInputParams, VOID ** pAppSpecPtr,
                       VOID **);

INT4  TrieDbRevise       (tInputParams *, void *pAppSpecInfo, UINT4 u4NewMetric,
                        VOID *pOutputParams);
INT4 TrieDbLookupOverlapLessMoreSpecific  (tInputParams * pInputParams, VOID * pOutputParams,
                              VOID **ppNode);


#endif
