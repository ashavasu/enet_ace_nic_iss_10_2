#include "mea_api.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_rbtree.h"
#include "adap_trie.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_acl.h"
#include "adap_cfanp.h"
#include "adap_ipnp.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "mea_drv_common.h"
#include <sys/ioctl.h> /* ioctl() */
#include <net/if.h>
#include "adap_lpm.h"

#define  MAX_PACKET_LEN                                         2000
#define LPM_VRID_LEN_BITS 8 /* allowing 256 different VRFs */
#define LPM_IPV4_PREFIX_MAX_SIZE ((LPM_VRID_LEN_BITS / 8) + 4)   /* VRF + IPv4 addr */
#define LPM_IPV6_PREFIX_MAX_SIZE ((LPM_VRID_LEN_BITS / 8) + 16)  /* VRF + IPv6 addr */

#define UTIL_SWAP16(x) (ADAP_Uint16)(((x & 0xFF00) >> 8 ) | ((x & 0x00FF) << 8))

#ifdef LPM_ADAPTOR_WANTED
extern char *tapif;
#endif

static pthread_t thread_lpm;

static adap_rbtree *gpL3ArpTbl = NULL;
static adap_rbtree *gpL3NDTbl = NULL;
static adap_rbtree *gpFPShadowRouteTbl = NULL;
static adap_trie *gpLpmRouteTbl = NULL;
static adap_trie *gpLpmIp6RouteTbl = NULL;

/* initialization flag */
static ADAP_Bool g_initialized = ADAP_FALSE;

ADAP_Uint32
Enet_Lpm_Arp_Add (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IfIdx,
		ADAP_Uint16 u2VlanId, tEnetHal_MacAddr *pMacAddr)
{
	adap_rbtree_element *rbtree_elem;
    struct arp_entry *arp_entry;
    struct arp_entry  arp_c;
    EnetHal_Status_t  status;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }
    
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
            "Enet_Lpm_Arp_Add DestIp(%x) IfIndex %d \n", u4IpAddr, u4IfIdx);

    arp_c.DstIp = u4IpAddr;
    arp_c.u4VrId = u4VrId;
    status = adap_rbtree_find(gpL3ArpTbl, &arp_c.elem, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
    	arp_entry = container_of(rbtree_elem, struct arp_entry, elem);
    	arp_entry->u2vlanId = u2VlanId;
    	arp_entry->u4IfIndex = u4IfIdx;
        adap_memcpy(&arp_entry->MacAddr, pMacAddr, sizeof(tEnetHal_MacAddr));
    } else if (status == ENETHAL_STATUS_NOT_FOUND) {
    	arp_entry = (struct arp_entry *)adap_malloc(sizeof(struct arp_entry));
        if (arp_entry == NULL) {
        	return LPM_FAILURE;
        }

        arp_entry->DstIp = u4IpAddr;
        arp_entry->u2vlanId = u2VlanId;
        arp_entry->u4VrId = u4VrId;
        arp_entry->u4IfIndex = u4IfIdx;
		adap_memcpy(&arp_entry->MacAddr, pMacAddr, sizeof (tEnetHal_MacAddr));

		status = adap_rbtree_insert(gpL3ArpTbl, &arp_entry->elem);
		if (status != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
				"Failed to add ARP (%x) IfIndex %d \n", u4IpAddr, u4IfIdx);
			return LPM_FAILURE;
		}
    }

    return LPM_SUCCESS;
}

ADAP_Uint32
Enet_Lpm_Arp_Delete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr)
{
	adap_rbtree_element *rbtree_elem;
    struct arp_entry *arp_entry;
    struct arp_entry  arp_c;
    EnetHal_Status_t  status;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
            "Enet_Lpm_Arp_Delete DestIp(%x) \n", u4IpAddr);

    arp_c.DstIp = u4IpAddr;
    arp_c.u4VrId = u4VrId;
    status = adap_rbtree_find(gpL3ArpTbl, &arp_c.elem, &rbtree_elem);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	return LPM_FAILURE;
    }

	/* When ARP entry is removed, corresponding
	 * routes from FPGA also has to be deleted */
	if (Enet_Lpm_Handle_FPGA_Arp_Delete (u4VrId, u4IpAddr) != LPM_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to remove route from FPGA\n");
	}

    status = adap_rbtree_remove(gpL3ArpTbl, &arp_c.elem, &rbtree_elem);
    if (status != ENETHAL_STATUS_SUCCESS) {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, "Failed to remove ARP (%x)  \n", u4IpAddr);
    	return LPM_FAILURE;
    }

    arp_entry = container_of(rbtree_elem, struct arp_entry, elem);
	adap_safe_free(arp_entry);

    return LPM_SUCCESS;
}


struct arp_entry *
Enet_Lpm_Arp_Get(ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr)
{
	adap_rbtree_element *rbtree_elem;
    struct arp_entry *arp_entry;
    struct arp_entry  arp_c;
    EnetHal_Status_t  status;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return NULL;
    }
    
    arp_c.DstIp = u4IpAddr;
    arp_c.u4VrId = u4VrId;

    status = adap_rbtree_find(gpL3ArpTbl, &arp_c.elem, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
    	arp_entry = container_of(rbtree_elem, struct arp_entry, elem);
    	return arp_entry;
    } else {
    	return NULL;
    }
}


ADAP_Uint32
Enet_Lpm_Route_Add(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len,
		ADAP_Uint32 next_hop, ADAP_Uint32 ifindex, ADAP_Uint16 vlan_id)
{
	EnetHal_Status_t status;
	struct ipv4_next_hop_entry *route;
	ADAP_Uint8 trie_prefix[LPM_IPV4_PREFIX_MAX_SIZE];

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
			"Enet_Lpm_Route_Add %x/%d\n", prefix, prefix_len);

	if ((next_hop != 0) &&
			(Enet_Lpm_Arp_Get(vrid, next_hop) == NULL)) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"FAILED to install route Dest %x/%d to LPM database\n", prefix, prefix_len);
		return LPM_FAILURE;
	}

	route = (struct ipv4_next_hop_entry *)adap_malloc(sizeof(struct ipv4_next_hop_entry));
	if (route == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"FAILED to install route Dest %x/%d to LPM database\n", prefix, prefix_len);
		return LPM_FAILURE;
	}

	route->u2VlanId = vlan_id;
	route->u4IfIndex = ifindex;
	route->u4NextHopGt = next_hop;

	trie_prefix[0] = vrid;
	/* TODO - Refactor this [Roee BH] */
	trie_prefix[1] = (prefix & 0xff000000) >> 24;
	trie_prefix[2] = (prefix & 0xff0000) >> 16;
	trie_prefix[3] = (prefix & 0xff00) >> 8;
	trie_prefix[4] = prefix & 0xff;
	status = adap_trie_insert(gpLpmRouteTbl, trie_prefix, LPM_VRID_LEN_BITS + prefix_len, &route->elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"FAILED to install route Dest %x/%d to LPM database\n", prefix, prefix_len);
		return LPM_FAILURE;
	}

	return LPM_SUCCESS;
}


ADAP_Uint32
Enet_Lpm_Route_Delete(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len)
{
	EnetHal_Status_t status;
	adap_trie_element *trie_elem;
	struct ipv4_next_hop_entry *route;
	ADAP_Uint8 trie_prefix[LPM_IPV4_PREFIX_MAX_SIZE];

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
			"Enet_Lpm_Route_Delete %x/%d)\n", prefix, prefix_len);

	trie_prefix[0] = vrid;
	/* TODO - Refactor this [Roee BH] */
	trie_prefix[1] = (prefix & 0xff000000) >> 24;
	trie_prefix[2] = (prefix & 0xff0000) >> 16;
	trie_prefix[3] = (prefix & 0xff00) >> 8;
	trie_prefix[4] = prefix & 0xff;

	status = adap_trie_remove(gpLpmRouteTbl, trie_prefix, LPM_VRID_LEN_BITS + prefix_len, &trie_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
				"FAILED to delete route Dest %x/%d to LPM database\n", prefix, prefix_len);
		return LPM_FAILURE;
	}

	route = container_of(trie_elem, struct ipv4_next_hop_entry, elem);
	adap_safe_free(route);

	if (Enet_Lpm_Delete_FP_Entry(vrid, prefix, prefix_len) != LPM_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete entry in FP table and forwarder\n");
		return LPM_FAILURE;
	}

	return LPM_SUCCESS;
}


ADAP_Uint32
Enet_Lpm_Route_Get(ADAP_Uint8 vrid, ADAP_Uint32 prefix, ADAP_Uint8 prefix_len,
		ADAP_Uint32 *next_hop, ADAP_Uint32 *ifindex, ADAP_Uint16 *vlan_id)
{
	EnetHal_Status_t status;
	adap_trie_element *trie_elem;
	struct ipv4_next_hop_entry *route;
	ADAP_Uint8 trie_prefix[LPM_IPV4_PREFIX_MAX_SIZE];

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	trie_prefix[0] = vrid;
	/* TODO - Refactor this [Roee BH] */
	trie_prefix[1] = (prefix & 0xff000000) >> 24;
	trie_prefix[2] = (prefix & 0xff0000) >> 16;
	trie_prefix[3] = (prefix & 0xff00) >> 8;
	trie_prefix[4] = prefix & 0xff;

	status = adap_trie_find_best(gpLpmRouteTbl, trie_prefix, LPM_VRID_LEN_BITS + prefix_len, &trie_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return LPM_FAILURE;
	}

	route = container_of(trie_elem, struct ipv4_next_hop_entry, elem);
    *next_hop = route->u4NextHopGt;
    *ifindex = route->u4IfIndex;
    *vlan_id = route->u2VlanId;

    return LPM_SUCCESS;
}

ADAP_Uint32
Enet_Lpm_Add_FP_Entry (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr,
		ADAP_Uint32 u4NextHop, ADAP_Uint32 u4IfIndex,
					   ADAP_Uint16 u2VlanId, ADAP_Uint16 u2VpnId, tEnetHal_MacAddr *pDestMacAddr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct route_entry         *pRoute;
	struct route_entry         RouteElem;
    MEA_OutPorts_Entry_dbt tOutPorts;
    ADAP_Uint32         logPort = 0;
    ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    ADAP_Uint32         modeType = 0;
    MEA_Port_t          PhyPort;
    MEA_Action_t        Action_id=0;
    MEA_EHP_Info_dbt tEhp_info;
    struct arp_entry           *pL3ArpEntry;
    ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    ADAP_Uint32 vlanIdTag=0;
    tEnetHal_MacAddr srcMac;
    tEnetHal_MacAddr daMac;
    MEA_Bool action_valid=MEA_FALSE;
    MEA_SE_Entry_dbt                      	entry;
        
    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
    adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
    adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));
    adap_memset(&RouteElem, 0, sizeof(struct route_entry));

    RouteElem.u4DestAddr = u4IpDestAddr;
    RouteElem.u4VrId = u4VrId;
    status = adap_rbtree_find(gpFPShadowRouteTbl, &RouteElem.elem, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," entry already present in LPM FP & forwarder\n");
	    return LPM_FAILURE;
    }
    pRoute = container_of(rbtree_elem, struct route_entry, elem);

    if (u4NextHop != 0) {
    	logPort = adap_VlanGetPortByVlanId (u2VlanId);
    	MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
    	MEA_SET_OUTPORT(&tOutPorts,PhyPort);

    	pL3ArpEntry = Enet_Lpm_Arp_Get(0, u4NextHop);
    	if(pL3ArpEntry != NULL) {
    		adap_memcpy(&daMac, &pL3ArpEntry->MacAddr, sizeof(tEnetHal_MacAddr));
    	}
    	MeaDrvGetFirstGlobalMacAddress (&srcMac);

    	if (adap_VlanCheckTaggedPort(u2VlanId,logPort) == ADAP_TRUE) {
    		if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
    				(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE)) {
    			AdapGetProviderCoreBridgeMode(&modeType,logPort);
    			if (modeType == ADAP_PROVIDER_NETWORK_PORT) {
    				EtherType = 0x88a8;
    			}
    		}
    		vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    		tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
    		vlanIdTag = (EtherType << 16);
    		vlanIdTag |= u2VlanId;
    	} else {
    		vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
    		tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
    	}
    	MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
    	/* create action for the route */
    	MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
    			&daMac, vlanCommand, vlanIdTag, 0);

    	if (FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
    			&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK) {
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
    	}
    	action_valid = MEA_TRUE;
    }

    adap_memset(&entry, 0, sizeof(entry));

    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=u4IpDestAddr;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = START_L3_VPN;
    entry.data.aging = 3; //Default
    entry.data.static_flag  = MEA_FALSE;//MEA_TRUE;

    if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK) {
    	/* Delete the drop entry and add valid entry in forwarder */
    	if (MeaDrvHwDeleteDestIpForwarder(u4IpDestAddr, 1, START_L3_VPN) != MEA_OK) {
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
    	}
    	if(MeaDrvHwCreateDestIpForwarder(Action_id,action_valid,u4IpDestAddr, 1,
    			START_L3_VPN,&tOutPorts) != MEA_OK) {
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to create entry in forwarder\n");
    		return LPM_FAILURE;
    	}
    }

    if (pRoute == NULL) {
    	pRoute = adap_malloc(sizeof(struct route_entry));
    	if (pRoute == NULL) {
    		return LPM_FAILURE;
    	}

		pRoute->u4VrId = u4VrId;
		pRoute->u4DestAddr = u4IpDestAddr;
		pRoute->u4NextHopGt = u4NextHop;
		pRoute->u4IfIndex = u4IfIndex;
		pRoute->u2VlanId = u2VlanId;
		adap_memcpy(&pRoute->DestMacAddr, pDestMacAddr, sizeof (tEnetHal_MacAddr));
		status = adap_rbtree_insert(gpFPShadowRouteTbl, &pRoute->elem);
		if (status != ENETHAL_STATUS_SUCCESS) {
			return LPM_FAILURE;
		}
    }
    return LPM_SUCCESS;
}


ADAP_Uint32
Enet_Lpm_Delete_FP_Entry (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr, ADAP_Uint32 u4IpSubNetMask)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
    struct route_entry *pRoute;
    struct route_entry  RouteElem;
    ADAP_Uint32 arrIpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
    ADAP_Uint32 numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
    ADAP_Uint32 IpClass=0;
    MEA_OutPorts_Entry_dbt tOutPorts;
        
    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

    if(MiFiDivideIpClass(u4IpDestAddr, u4IpSubNetMask, &IpClass,
                        &arrIpAddr[0],&numOfEntries) != ENET_SUCCESS) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to calculate the subnetting\n");
        return LPM_FAILURE;
    }    

    /* delete from forwarder if alrdy exists */
    if( MeaDrvHwDeleteDestIpForwarder(u4IpDestAddr,IpClass,START_L3_VPN) != MEA_OK) {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete entry in forwarder\n");
    	return LPM_FAILURE;
    }

    adap_memset(&RouteElem, 0, sizeof(struct route_entry));
    RouteElem.u4DestAddr = u4IpDestAddr;
    RouteElem.u4VrId = u4VrId;
    status = adap_rbtree_find(gpFPShadowRouteTbl, &RouteElem.elem, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
    	status = adap_rbtree_find(gpFPShadowRouteTbl, &RouteElem.elem, NULL);
    	if (status == ENETHAL_STATUS_SUCCESS) {
    		pRoute = container_of(rbtree_elem, struct route_entry, elem);
    		adap_safe_free(pRoute);
    	}
    }

    return LPM_SUCCESS;
}


ADAP_Int32
Enet_Lpm_Handle_FPGA_Arp_Delete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpAddr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
    struct route_entry *pRoute;
    struct route_entry route;
    ADAP_Uint32 u4IpSubNetMask = 0xffffffff;
    
    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    status = adap_rbtree_iter(gpFPShadowRouteTbl, NULL, &rbtree_elem);
    if (status == ENETHAL_STATUS_SUCCESS) {
    	pRoute = container_of(rbtree_elem, struct route_entry, elem);

    	memcpy(&route, pRoute, sizeof(struct route_entry));

        if (pRoute->u4NextHopGt == u4IpAddr) {
           if (Enet_Lpm_Delete_FP_Entry(u4VrId, pRoute->u4DestAddr,
                u4IpSubNetMask) != LPM_SUCCESS) {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "Failed to delete route from FPGA\r\n");
            }
        }

    	status = adap_rbtree_iter(gpFPShadowRouteTbl, &route.elem, &rbtree_elem);
    }
    return LPM_SUCCESS;
}


static
ADAP_Int32 EnetAdapProcessLpmPacket(ADAP_Uint32 ifindex, void *pBuf, ADAP_Uint32 len)
{
    MEA_OutPorts_Entry_dbt tOutPorts;
    ADAP_Uint32         logPort = 0;
    struct arp_entry *pL3ArpEntry;
    ADAP_Uint32 u4NextHopGt;
    ADAP_Uint32 u4IfIndex;
    ADAP_Uint16 u2VlanId;
    tProtocl_sniff      ProtoSniff;
    ADAP_Uint16               u2VpnId = 0;
    ADAP_Uint16               u2ValToPtr = 0;
    ADAP_Uint32               u4Index = 0;
    ADAP_Uint32               u4DefIP = 0;
    ADAP_Uint8               u1ChkDefRt = MEA_FALSE;
    ADAP_Uint8               aRcvStaticBuf[MAX_PACKET_LEN];    /*Buffer used during packet Rx */

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    adap_memset(&ProtoSniff,0,sizeof(ProtoSniff));
    adap_memset(aRcvStaticBuf, 0, sizeof (aRcvStaticBuf));

    adap_memcpy(aRcvStaticBuf, pBuf, len);
    /* Extract Dst MAC, VLAN, DestIP from the packet */
    if(get_sourceip_from_ipPacket(aRcvStaticBuf, len-4, &ProtoSniff, &u4Index) != ADAP_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"unable to parse the packet\r\n");
        return MEA_FALSE;
    }

    /* replace the source mac with interface mac */
    if( MeaDrvGetFirstGlobalMacAddress (&ProtoSniff.ethernet.ether_shost) != MEA_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvGetFirstGlobalMacAddress\r\n");
        return MEA_FALSE;
    }

    if (adap_memcmp(&ProtoSniff.ethernet.ether_dhost,
                    &ProtoSniff.ethernet.ether_shost,
					sizeof(tEnetHal_MacAddr)) != 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DestMAC lookup failed, Packet not destined to this router\r\n");
        return MEA_FALSE;
    }

    u2VpnId = adap_GetVpnByVlan (ProtoSniff.ethernet.vlanId);

    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	/* create entry in forwarder to drop further packets */
	if(MeaDrvHwCreateDestIpForwarder(0,MEA_TRUE,ProtoSniff.destIp, 1,
			START_L3_VPN,&tOutPorts) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to create drop route entry in forwarder\n");
	}

    pL3ArpEntry = Enet_Lpm_Arp_Get(0, ProtoSniff.destIp);
    if (pL3ArpEntry == NULL) {
    	if (Enet_Lpm_Route_Get(0, ProtoSniff.destIp, 32, &u4NextHopGt, &u4IfIndex, &u2VlanId) == LPM_SUCCESS) {
    		pL3ArpEntry = Enet_Lpm_Arp_Get(0, u4NextHopGt);
    		if (pL3ArpEntry != NULL) {
    			adap_memcpy(&ProtoSniff.ethernet.ether_dhost, &pL3ArpEntry->MacAddr, sizeof(tEnetHal_MacAddr));
    		} else {
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
    					"Next hop resolution failed, unable to route\r\n");
    			u1ChkDefRt = MEA_TRUE;
    		}
    	} else {
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
    				"Route lookup failed, unable to route\r\n");
    		u1ChkDefRt = MEA_TRUE;
    	}
    } else {
    	adap_memcpy(&ProtoSniff.ethernet.ether_dhost, &pL3ArpEntry->MacAddr, sizeof(tEnetHal_MacAddr));
    }

    if (u1ChkDefRt == MEA_TRUE) {
    	if (Enet_Lpm_Route_Get(0, u4DefIP, 0, &u4NextHopGt, &u4IfIndex, &u2VlanId) == LPM_SUCCESS) {
    		pL3ArpEntry = Enet_Lpm_Arp_Get(0, u4NextHopGt);
    		if (pL3ArpEntry != NULL) {
    			adap_memcpy(&ProtoSniff.ethernet.ether_dhost,
    					&pL3ArpEntry->MacAddr, sizeof(tEnetHal_MacAddr));
    		} else {
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
    					"Route lookup failed, drop the packet\r\n");
    			if (MeaDrvHwDeleteDestIpForwarder (ProtoSniff.destIp, 1, START_L3_VPN) != MEA_OK) {
    				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
    			}
    			return MEA_FALSE;
    		}
    	} else {
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
    				"Route lookup failed, unable to route\r\n");
    		if (MeaDrvHwDeleteDestIpForwarder (ProtoSniff.destIp, 1, START_L3_VPN) != MEA_OK) {
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
    		}
    		return MEA_FALSE;
    	}
    }

	/* Update the FP route table for received route */
	if ((u1ChkDefRt == MEA_FALSE) &&
			(Enet_Lpm_Add_FP_Entry (pL3ArpEntry->u4VrId, ProtoSniff.destIp,
					u4NextHopGt, u4IfIndex,
					pL3ArpEntry->u2vlanId, u2VpnId,
					&pL3ArpEntry->MacAddr) != LPM_SUCCESS)) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"Route addition to FPGA shadow table failed\r\n");
	}

    /* Update the packet Dest MAC, Vlan for forwarding */
    adap_mac_to_arr(&ProtoSniff.ethernet.ether_dhost, pBuf);
    adap_mac_to_arr(&ProtoSniff.ethernet.ether_shost, pBuf + ETH_ALEN);

    if ((aRcvStaticBuf[ENET_VLAN_TAG_OFFSET] == 0x81) &&
    		(aRcvStaticBuf[ENET_VLAN_TAG_OFFSET+1] == 0x00)) {
    	u2ValToPtr = UTIL_SWAP16(0x8100);
    	adap_memcpy(&aRcvStaticBuf[ENET_VLAN_TAG_OFFSET], (ADAP_Uint8 *) &u2ValToPtr, sizeof(ADAP_Uint16));
    	u2ValToPtr = UTIL_SWAP16(pL3ArpEntry->u2vlanId);
    	adap_memcpy(&aRcvStaticBuf[ENET_VLAN_TAG_OFFSET+2], (ADAP_Uint8 *) &u2ValToPtr, sizeof(ADAP_Uint16));
    }

    adap_memcpy(pBuf, aRcvStaticBuf, len);
    logPort = adap_VlanGetPortByVlanId (pL3ArpEntry->u2vlanId);

    MeaAdapSendPacket(pBuf, logPort, len-4);
    return MEA_TRUE;
}


/* TODO - re-implement with Trie iterator */
//void
//DumpLPMRouteTable (void)
//{
//    tRtNextHopEntry    *pRtEntry = NULL;
//
//    if (g_initialized == ADAP_FALSE) {
//    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
//        return LPM_FAILURE;
//    }
//
//    printf(" ==================================== \n");
//    printf("          LPM Route Table  \n");
//    printf(" ==================================== \n");
//    printf("  DestIp/Mask     VlanId   NextHop  \n");
//    printf(" ==================================== \n");
//    for (TrieDbGetFirstNode (gpLpmRouteTbl, (void **) &pRtEntry,
//                           (void **) &inParams.pLeafNode); inParams.pLeafNode;)
//    {
//        printf ("%.8x/%4x %4d %12.8x ",
//                   pRtEntry->u4IpDestAddr, pRtEntry->u4SubNetMask,
//                   pRtEntry->u2VlanId, pRtEntry->u4NextHopGt);
//        printf ("\n");
//
//        TrieDbGetNextNode (gpLpmRouteTbl, (void *) inParams.pLeafNode,
//                         (void **) &pRtEntry, (void **) &inParams.pLeafNode);
//    }
//    return;
//}

void
DumpArpTbl (void)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
    struct arp_entry *pArpEntry;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return;
    }

    printf(" ==================================== \n");
    printf("          LPM ARP Table  \n");
    printf(" ==================================== \n");
    printf("\n  DstIp   VlanId   MacAddr\n");
    printf("  ==============================\n");

    status = adap_rbtree_iter(gpL3ArpTbl, NULL, &rbtree_elem);
    while (status == ENETHAL_STATUS_SUCCESS) {
    	pArpEntry = container_of(rbtree_elem, struct arp_entry, elem);
    	printf("%.8x %8hd %s\n",
    			pArpEntry->DstIp, pArpEntry->u2vlanId,
				adap_mac_to_string(&pArpEntry->MacAddr));
    	status = adap_rbtree_iter(gpL3ArpTbl, rbtree_elem, &rbtree_elem);
    }
}


void
DumpFPShadowRouteTbl (void)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
    struct route_entry *pRtEntry;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return;
    }

    printf(" ==================================== \n");
    printf("      LPM FP Shadow Route Table  \n");
    printf(" ==================================== \n");
    printf("\n  DstIp     NextHopIp   VlanId    DestMacAddr\n");
    printf(" ================================================\n");

    status = adap_rbtree_iter(gpFPShadowRouteTbl, NULL, &rbtree_elem);
    while (status == ENETHAL_STATUS_SUCCESS) {
    	pRtEntry = container_of(rbtree_elem, struct route_entry, elem);
    	printf ("%.8x %.8x %8hd %s\n",
    			pRtEntry->u4DestAddr, pRtEntry->u4NextHopGt, pRtEntry->u2VlanId,
				adap_mac_to_string(&pRtEntry->DestMacAddr));
    	status = adap_rbtree_iter(gpFPShadowRouteTbl, rbtree_elem, &rbtree_elem);
    }
}

static
void* LpmThreadHandler(void *arg)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    ADAP_Int32		i4LpmSocketId = 0;
    ADAP_Int32		i4BytesRead = 0;
    unsigned char 	buf[MAX_PACKET_LEN];
    MEA_Port_t 		PhyPort=0;
    
    adap_memset(buf, 0, MAX_PACKET_LEN);

    i4LpmSocketId = socket (PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (i4LpmSocketId < 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
             "%s - socket creation failed (errno=%d '%s')\n",
             __func__, errno, strerror(errno));
	return NULL;
    }

    adap_memset(&ifr, 0, sizeof(ifr));
    sprintf (ifr.ifr_name, "%s", tapif);
    if (ioctl (i4LpmSocketId, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        close (i4LpmSocketId);
        return NULL;
    }

    adap_memset(&Enet, 0, sizeof (Enet));
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    if (bind (i4LpmSocketId, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        close (i4LpmSocketId);
        return NULL;
    }

    while (ADAP_TRUE) {
    	i4BytesRead = recvfrom (i4LpmSocketId, buf, MAX_PACKET_LEN, 0, NULL, NULL);
    	if (i4BytesRead == -1 && errno == EAGAIN) {
    		continue;
    	}

    	if (i4BytesRead > 18) {
    		if(buf[12]==0x91 && buf[13]==0x00 &&
    				buf[16]==0x81 && buf[17]==0x00 &&
					buf[14]==0x0f && buf[15]==0xac) {
    			/* Ethernity-attached VLAN 4012 tag is present for LPM */
    			PhyPort = (buf[19])|((buf[18]&0xf)<<8);
    			memmove(buf+12,buf+20,i4BytesRead-20);
    			EnetAdapProcessLpmPacket(PhyPort,buf,i4BytesRead-4);
    		}
    	}
    }   

    ADAP_UNUSED_PARAM (arg);
    return NULL;
}

ADAP_Uint32
Enet_Lpm_ND_Add (ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr, ADAP_Uint32 u4IfIdx,
		ADAP_Uint16 u2VlanId, tEnetHal_MacAddr *pMacAddr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct nd_entry *pL3NDElem;
	struct nd_entry  L3NDElem;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
			"Enet_Lpm_ND_Add DestIp(%s) IfIndex %d \n", adap_ipv6_to_string(pIp6Addr), u4IfIdx);

	adap_memset(&L3NDElem, 0, sizeof(struct nd_entry));
	adap_memcpy(&L3NDElem.DstIp6, pIp6Addr, sizeof(tEnetHal_IPv6Addr));
	L3NDElem.u4VrId = u4VrId;

	status = adap_rbtree_find(gpL3NDTbl, &L3NDElem.elem, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		pL3NDElem = adap_malloc(sizeof(struct nd_entry));
		if (pL3NDElem == NULL) {
			return LPM_FAILURE;
		}

		adap_memcpy(&(pL3NDElem->DstIp6), pIp6Addr, sizeof(tEnetHal_IPv6Addr));
		pL3NDElem->u2vlanId = u2VlanId;
		pL3NDElem->u4VrId = u4VrId;
		pL3NDElem->u4IfIndex = u4IfIdx;
		adap_memcpy(&pL3NDElem->MacAddr, pMacAddr, sizeof (tEnetHal_MacAddr));
		status = adap_rbtree_insert(gpL3NDTbl, &L3NDElem.elem);
		if (status != ENETHAL_STATUS_SUCCESS) {
			ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
					"Failed to add ND (%s) IfIndex %d \n", adap_ipv6_to_string(pIp6Addr), u4IfIdx);
			adap_safe_free(pL3NDElem);
			return LPM_FAILURE;
		}
	} else {
		pL3NDElem = container_of(rbtree_elem, struct nd_entry, elem);
		pL3NDElem->u2vlanId = u2VlanId;
		pL3NDElem->u4IfIndex = u4IfIdx;
		adap_memcpy(&pL3NDElem->MacAddr, pMacAddr, sizeof(tEnetHal_MacAddr));
	}

	return LPM_SUCCESS;
}

ADAP_Uint32
Enet_Lpm_ND_Delete (ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct nd_entry *pL3NDElem;
	struct nd_entry  L3NDElem;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
			"Enet_Lpm_ND_Delete DestIp(%s) \n", adap_ipv6_to_string(pIp6Addr));

	adap_memset(&L3NDElem, 0, sizeof(struct nd_entry));
	adap_memcpy(&L3NDElem.DstIp6, pIp6Addr, sizeof(tEnetHal_IPv6Addr));
	L3NDElem.u4VrId = u4VrId;

	status = adap_rbtree_find(gpL3NDTbl, &L3NDElem.elem, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
				"Failed to remove ND (%s)  \n", adap_ipv6_to_string(pIp6Addr));
		return LPM_FAILURE;
	}

	status = adap_rbtree_remove(gpL3NDTbl, rbtree_elem, &rbtree_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return LPM_FAILURE;
	}

	pL3NDElem = container_of(rbtree_elem, struct nd_entry, elem);
	adap_safe_free(pL3NDElem);

	return LPM_SUCCESS;
}


struct nd_entry *
Enet_Lpm_ND_Get(ADAP_Uint32 u4VrId, tEnetHal_IPv6Addr *pIp6Addr)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct nd_entry *pL3NDElem;
	struct nd_entry  L3NDElem;

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return NULL;
    }

	adap_memset(&L3NDElem, 0, sizeof(struct nd_entry));
	adap_memcpy(&(L3NDElem.DstIp6), pIp6Addr, sizeof (tEnetHal_IPv6Addr));
	L3NDElem.u4VrId = u4VrId;

	status = adap_rbtree_find(gpL3NDTbl, &L3NDElem.elem, &rbtree_elem);
	if (status == ENETHAL_STATUS_SUCCESS) {
		pL3NDElem = container_of(rbtree_elem, struct nd_entry, elem);
		return pL3NDElem;
	} else {
		return NULL;
	}
}

ADAP_Uint32
Enet_Lpm_Ipv6Route_Add(ADAP_Uint8 vrid, tEnetHal_IPv6Addr *prefix, ADAP_Uint8 prefix_len,
		tEnetHal_IPv6Addr *next_hop, ADAP_Uint32 ifindex, ADAP_Uint16 vlan_id)
{
	EnetHal_Status_t status;
	struct ipv6_next_hop_entry *route;
	ADAP_Uint8 trie_prefix[LPM_IPV6_PREFIX_MAX_SIZE];

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

    if ((adap_is_ipv6_zero(next_hop) == ADAP_TRUE) && (Enet_Lpm_ND_Get(vrid, next_hop) == NULL)) {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
        		"FAILED to install route Dest %s/%d to LPM database\n", adap_ipv6_to_string(prefix), prefix_len);
        return LPM_FAILURE;
    }

	route = (struct ipv6_next_hop_entry *)adap_malloc(sizeof(struct ipv6_next_hop_entry));
	if (route == NULL) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"FAILED to install route Dest %s/%d to LPM database\n", adap_ipv6_to_string(prefix), prefix_len);
		return LPM_FAILURE;
	}

	route->u2VlanId = vlan_id;
	route->u4IfIndex = ifindex;
	adap_memcpy(&route->Ip6NextHop, next_hop, sizeof(tEnetHal_IPv6Addr));

	trie_prefix[0] = vrid;
	memcpy(trie_prefix + 1, prefix, sizeof(tEnetHal_IPv6Addr));
	status = adap_trie_insert(gpLpmIp6RouteTbl, trie_prefix, LPM_VRID_LEN_BITS + prefix_len, &route->elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
				"FAILED to install route Dest %s/%d to LPM database\n", adap_ipv6_to_string(prefix), prefix_len);
		return LPM_FAILURE;
	}
    
    return LPM_SUCCESS;
}


ADAP_Uint32
Enet_Lpm_Ipv6Route_Delete (ADAP_Uint8 vrid, tEnetHal_IPv6Addr *prefix, ADAP_Uint8 prefix_len)
{
	EnetHal_Status_t status;
	adap_trie_element *trie_elem;
	struct ipv6_next_hop_entry *route;
	ADAP_Uint8 trie_prefix[LPM_IPV4_PREFIX_MAX_SIZE];

    if (g_initialized == ADAP_FALSE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module not initialized\n");
        return LPM_FAILURE;
    }

	ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
			"Enet_Lpm_Route_Delete %s/%d\n", adap_ipv6_to_string(prefix), prefix_len);

	trie_prefix[0] = vrid;
	memcpy(trie_prefix + 1, prefix, sizeof(tEnetHal_IPv6Addr));

	status = adap_trie_remove(gpLpmIp6RouteTbl, trie_prefix, LPM_VRID_LEN_BITS + prefix_len, &trie_elem);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
				"FAILED to delete route Dest %s/%d to LPM database\n", adap_ipv6_to_string(prefix), prefix_len);
		return LPM_FAILURE;
	}

	route = container_of(trie_elem, struct ipv6_next_hop_entry, elem);
	adap_safe_free(route);

	/* Need to write the IPv6 version */
//	if (Enet_Lpm_Delete_FP_Entry(vrid, prefix, prefix_len) != LPM_SUCCESS) {
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete entry in FP table and forwarder\n");
//		return LPM_FAILURE;
//	}

	return LPM_SUCCESS;
}


static
int LpmL3ArpTblCmp(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct arp_entry         *arp1 = container_of(e1, struct arp_entry, elem);
    struct arp_entry         *arp2 = container_of(e2, struct arp_entry, elem);

    if (arp1->u4VrId < arp2->u4VrId) {
        return -1;
    } else if (arp1->u4VrId > arp2->u4VrId) {
        return 1;
    } else {
		if (arp1->DstIp < arp2->DstIp) {
			return -1;
		} else if (arp1->DstIp > arp2->DstIp) {
			return 1;
		}
    }

    return 0;
}

static
void LpmL3ArpTblSwap(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct arp_entry         *arp1 = container_of(e1, struct arp_entry, elem);
    struct arp_entry         *arp2 = container_of(e2, struct arp_entry, elem);
    struct arp_entry temp;

    adap_memcpy(&temp, arp1, sizeof(struct arp_entry));

    arp1->DstIp = arp2->DstIp;
    arp1->u4VrId = arp2->u4VrId;
    arp1->u4IfIndex = arp2->u4IfIndex;
    arp1->u2vlanId = arp2->u2vlanId;
    adap_memcpy(&arp1->MacAddr, &arp2->MacAddr, sizeof(tEnetHal_MacAddr));

    arp2->DstIp = temp.DstIp;
    arp2->u4VrId = temp.u4VrId;
    arp2->u4IfIndex = temp.u4IfIndex;
    arp2->u2vlanId = temp.u2vlanId;
    adap_memcpy(&arp2->MacAddr, &temp.MacAddr, sizeof(tEnetHal_MacAddr));
}

static
int LpmL3NDTblCmp(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct nd_entry         *nd1 = container_of(e1, struct nd_entry, elem);
    struct nd_entry         *nd2 = container_of(e2, struct nd_entry, elem);

    if (nd1->u4VrId < nd2->u4VrId) {
        return -1;
    } else if (nd1->u4VrId > nd2->u4VrId) {
        return 1;
    } else {
        return adap_memcmp(&nd1->DstIp6, &nd2->DstIp6, sizeof(tEnetHal_IPv6Addr));
    }
}

static
void LpmL3NDTblSwap(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct nd_entry         *nd1 = container_of(e1, struct nd_entry, elem);
    struct nd_entry         *nd2 = container_of(e2, struct nd_entry, elem);
    struct nd_entry temp;

    adap_memcpy(&temp, nd1, sizeof(struct arp_entry));

    adap_memcpy(&nd1->DstIp6, &nd2->DstIp6, sizeof(tEnetHal_IPv6Addr));
    nd1->u4VrId = nd2->u4VrId;
    nd1->u4IfIndex = nd2->u4IfIndex;
    nd1->u2vlanId = nd2->u2vlanId;
    adap_memcpy(&nd1->MacAddr, &nd2->MacAddr, sizeof(tEnetHal_MacAddr));

    adap_memcpy(&nd2->DstIp6, &temp.DstIp6, sizeof(tEnetHal_IPv6Addr));
    nd2->u4VrId = temp.u4VrId;
    nd2->u4IfIndex = temp.u4IfIndex;
    nd2->u2vlanId = temp.u2vlanId;
    adap_memcpy(&nd2->MacAddr, &temp.MacAddr, sizeof(tEnetHal_MacAddr));
}

static
int LpmFPShadowRouteTblCmp(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct route_entry         *route1 = container_of(e1, struct route_entry, elem);
    struct route_entry         *route2 = container_of(e2, struct route_entry, elem);

    if (route1->u4VrId < route2->u4VrId) {
        return -1;
    } else if (route1->u4VrId > route2->u4VrId) {
        return 1;
    } else {
        if (route1->u4DestAddr < route2->u4DestAddr) {
            return -1;
        } else if (route1->u4DestAddr > route2->u4DestAddr) {
            return 1;
        }
    }

    return 0;
}

static
void LpmFPShadowRouteTblSwap(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
    struct route_entry         *route1 = container_of(e1, struct route_entry, elem);
    struct route_entry         *route2 = container_of(e2, struct route_entry, elem);
    struct route_entry temp;

    adap_memcpy(&temp, route1, sizeof(struct route_entry));

	route1->u4DestAddr = route2->u4DestAddr;
	route1->u4VrId = route2->u4VrId;
	adap_memcpy(&route1->DestMacAddr, &route2->DestMacAddr, sizeof(tEnetHal_MacAddr));
	route1->u4NextHopGt = route2->u4NextHopGt;
	route1->u4IfIndex = route2->u4IfIndex;
	route1->u2VlanId = route2->u2VlanId;
	route1->u2Port = route2->u2Port;

	route2->u4DestAddr = temp.u4DestAddr;
	route2->u4VrId =  temp.u4VrId;
	adap_memcpy(&route2->DestMacAddr, &temp.DestMacAddr, sizeof(tEnetHal_MacAddr));
	route2->u4NextHopGt =  temp.u4NextHopGt;
	route2->u4IfIndex =  temp.u4IfIndex;
	route2->u2VlanId =  temp.u2VlanId;
	route2->u2Port =  temp.u2Port;
}


int LpmInitialize(void)
{
	EnetHal_Status_t status;
    struct adap_rbtree_params rbtree_params;
    struct adap_trie_params trie_params;

    if (g_initialized == ADAP_TRUE) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "LPM module already initialized\n");
        return LPM_FAILURE;
    }

    /* Create L3 ARP table */
    rbtree_params.capacity = 0; /* Unlimited */
    rbtree_params.compare = LpmL3ArpTblCmp;
    rbtree_params.swap = LpmL3ArpTblSwap;
    status = adap_rbtree_init(&gpL3ArpTbl, &rbtree_params);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "L3 Arp Table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create L3 ND table */
    rbtree_params.capacity = 0; /* Unlimited */
    rbtree_params.compare = LpmL3NDTblCmp;
    rbtree_params.swap = LpmL3NDTblSwap;
    status = adap_rbtree_init(&gpL3NDTbl, &rbtree_params);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "L3 ND Table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create FPGA Shadow route table */
    rbtree_params.capacity = 0; /* Unlimited */
    rbtree_params.compare = LpmFPShadowRouteTblCmp;
    rbtree_params.swap = LpmFPShadowRouteTblSwap;
    status = adap_rbtree_init(&gpFPShadowRouteTbl, &rbtree_params);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "FPGA Shadow Route table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create ipv4 route table */
    trie_params.capacity = 0; /* Unlimited */
    status = adap_trie_init(&gpLpmRouteTbl, &trie_params);
    if (status != ENETHAL_STATUS_SUCCESS)
    {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "TrieDb creation for IPv4 LPM route table failed \r\n");
        return LPM_FAILURE;
    }

    /* IPv6 Route table creation */
    trie_params.capacity = 0; /* Unlimited */
    status = adap_trie_init(&gpLpmRouteTbl, &trie_params);
    if (status != ENETHAL_STATUS_SUCCESS)
    {
    	ENET_ADAPTOR_PRINT_LOG(LOG_ERR, "TrieDb creation for IPv6 LPM route table failed \r\n");
        return (LPM_FAILURE);
    }

	pthread_create(&thread_lpm, NULL, LpmThreadHandler, NULL);

    g_initialized = ADAP_TRUE;

    return LPM_SUCCESS;
}
