/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"
#include "adap_types.h"
#include "adap_utils.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"


void ADAP_initLinkList(AdapLinkList *pNode,comp_key_func Compfunc)
{
	pNode->head=NULL;
	pNode->compKey=Compfunc;
}

ADAP_Uint32 ADAP_pushLinkList(AdapLinkList *pNode,void *data)
{
	struct accListNode *allocNode=NULL;
	struct accListNode *tempNode=NULL;

	allocNode = adap_malloc(sizeof(struct accListNode));
	if(allocNode == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
		return ENET_FAILURE;
	}

	allocNode->data=data;
	allocNode->next=NULL;

	if(pNode->head == NULL)
	{
		pNode->head = allocNode;
		pNode->head->next=NULL;
		return ENET_SUCCESS;
	}
	tempNode = pNode->head;
	while(tempNode->next != NULL)
	{
	    tempNode = tempNode->next;
	}

	tempNode->next=allocNode;
	return ENET_SUCCESS;
}

ADAP_Uint32 ADAP_pushLinkListBeforeCompKey(AdapLinkList *pNode, void *data/*data to enter*/,
        comp_key_func compKey/*if return is >0 data1 > data2*/)
{
    struct accListNode *tempNode;
    struct accListNode *currNode;
    struct accListNode *dataNode;
    struct accListNode *lastNode;

    dataNode = adap_malloc(sizeof(struct accListNode));
    if(dataNode == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
        return ENET_FAILURE;
    }

    dataNode->data = data;

    // if it is the first entry
    currNode = pNode->head;
    if ((!currNode) || (compKey(currNode->data, data) > 0))
    {
        pNode->head = dataNode;
        dataNode->next = currNode;
        return ENET_SUCCESS;
    }
    tempNode = pNode->head;
    lastNode = tempNode;
    while(tempNode->next != NULL)
    {
        currNode = tempNode->next;
        if(compKey(currNode->data, data) > 0)
        {
            tempNode->next = dataNode;
            dataNode->next = currNode;
            return ENET_SUCCESS;
        }
        tempNode = tempNode->next;
        if (tempNode)
            lastNode = tempNode;
    }

    //if we didn't find - push to back
    if (!lastNode)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to navigate on linked list\r\n");
        adap_safe_free(dataNode);
        return ENET_FAILURE;
    }

    lastNode->next = dataNode;
    dataNode->next = NULL;
    return ENET_SUCCESS;
}

void ADAP_popLinkList(AdapLinkList *pNode,void *dataKey,void **data)
{
	struct accListNode *tempNode=NULL;
	struct accListNode *currNode=NULL;

	(*data) = NULL;
	if(pNode->head == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"linkList empty\r\n");
		return ;
	}
	// if remove the first entry
	currNode = pNode->head;
	if(pNode->compKey(currNode->data,dataKey) == ENET_SUCCESS)
	{
		(*data) = currNode->data;
		tempNode = pNode->head;
		pNode->head = pNode->head->next;
		adap_safe_free(tempNode);
		return ;
	}
	tempNode = pNode->head;
	while(tempNode->next != NULL)
	{
		currNode = tempNode->next;
		if(pNode->compKey(currNode->data,dataKey) == ENET_SUCCESS)
		{
			(*data) = currNode->data;
			currNode = tempNode->next;
			tempNode->next = tempNode->next->next;
			adap_safe_free(currNode);
			return ;
		}
		tempNode = tempNode->next;
	}
}

void ADAP_getDataLinkList(AdapLinkList *pNode,void *dataKey,void **data)
{
	struct accListNode *tempNode=NULL;
	struct accListNode *currNode=NULL;

	(*data) = NULL;
	if(pNode->head == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"linkList empty\r\n");
		return ;
	}
	// if remove the first entry
	currNode = pNode->head;
	if(pNode->compKey(currNode->data,dataKey) == ENET_SUCCESS)
	{
		(*data) = currNode->data;
		return ;
	}
	tempNode = pNode->head;
	while(tempNode->next != NULL)
	{
		currNode = tempNode->next;
		if(pNode->compKey(currNode->data,dataKey) == ENET_SUCCESS)
		{
			(*data) = currNode->data;
			return ;
		}
		tempNode = tempNode->next;
	}
}

void *ADAP_getFirstDataLinkList(AdapLinkList *pNode)
{
	if(pNode->head == NULL)
		return NULL;
	return pNode->head->data;
}

void *ADAP_getNextDataLinkList(AdapLinkList *pNode,void *data)
{
	struct accListNode *tempNode=NULL;
//	struct accListNode *currNode=NULL;

	tempNode = pNode->head;

	while(tempNode != NULL)
	{
		if(tempNode->data == data)
		{
			if(tempNode->next != NULL)
			{
				return tempNode->next->data;
			}
			return NULL;
		}
		tempNode = tempNode->next;
	}
	return NULL;
}

void ADAP_deleteAllDataLinkList(AdapLinkList *pNode,func_to_delete func)
{
	struct accListNode *tempNode=NULL;
	struct accListNode *currNode=NULL;

	if(pNode->head == NULL)
	{
		return ;
	}
	tempNode = pNode->head;
	while(tempNode != NULL)
	{
		currNode = tempNode;
		tempNode = tempNode->next;
		func(currNode->data);
		adap_safe_free(currNode);
	}
	pNode->head = NULL;
}
