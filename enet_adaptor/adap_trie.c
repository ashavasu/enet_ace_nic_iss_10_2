/*
 * adap_trie.c
 */

#include "adap_trie.h"
#include "adap_utils.h"

struct adap_trie {
	adap_trie_element *root;
    ADAP_Uint32 capacity;
    ADAP_Uint32 size;
};

#define MIN(x, y) \
	((x) < (y) ? (x) : (y))

#define ADAP_TRIE_BIT_TEST(prefix, bit) \
	(((prefix)[(bit) / 8] & (0x80 >> ((bit) % 8))) != 0)


/*
 *  Find the index of the first bit that doesn't match in <prefix1> and <prefix2>.
 *  Max number of bits to examine is <length>.
 *  If all bits match return <length>.
 */
static
ADAP_Uint32 adap_trie_find_unmatched_bit(ADAP_Uint8 *prefix1, ADAP_Uint8 *prefix2, ADAP_Uint32 length)
{
	ADAP_Uint32 i;
	ADAP_Uint8 xor;

    for (i = 0; i * 8 < length; i++) {
    	xor = (prefix1[i] ^ prefix2[i]);
		if (xor != 0) {
			return MIN(length, i * 8 + __builtin_clz(xor) - 24);
		}
    }
    return length;
}

/*
 *  Compare <prefix1> and <prefix2>. Max number of bits to examine is <length>.
 *  If prefixes are identical return 0, otherwise return 1.
 */
static
ADAP_Uint32 adap_trie_prefix_compare(ADAP_Uint8 *prefix1, ADAP_Uint8 *prefix2, ADAP_Uint32 length)
{
	return length != adap_trie_find_unmatched_bit(prefix1, prefix2, length);
}


EnetHal_Status_t adap_trie_init(adap_trie** trie, struct adap_trie_params *params)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	adap_trie * t_trie = NULL;

    if ((params == NULL) || (trie == NULL)) {
        rc = ENETHAL_STATUS_NULL_PARAM;
        goto bail;
    }

    t_trie = (adap_trie *)adap_malloc(sizeof(adap_trie));
    if (t_trie == NULL) {
        rc = ENETHAL_STATUS_MEM_ALLOC;
        goto bail;
    }

    t_trie->root = NULL;

    t_trie->capacity = params->capacity;
    t_trie->size = 0;

    *trie = t_trie;
    t_trie = NULL;

bail:
	adap_safe_free(t_trie);
    return rc;
}


EnetHal_Status_t adap_trie_deinit(adap_trie* trie)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

    if (trie == NULL) {
        rc = ENETHAL_STATUS_NULL_PARAM;
        goto bail;
    }

    if (trie->size != 0) {
        rc = ENETHAL_STATUS_NOT_SUPPORTED;
        goto bail;
    }

    adap_safe_free(trie);

bail:
  	return rc;
}


EnetHal_Status_t adap_trie_insert(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element* element)
{
	adap_trie_element* node = NULL;
	adap_trie_element* glue = NULL;
	ADAP_Uint32 diff_bit;
	ADAP_Uint32 bit;

    if ((trie == NULL) || (prefix == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (prefix_length == 0) {
    	return ENETHAL_STATUS_INVALID_PARAM;
    }

    if ((trie->capacity > 0) && (trie->size == trie->capacity)) {
    	return ENETHAL_STATUS_LIMIT_REACHED;
    }

    /* If trie is empty just make element the root */
    if (trie->root == NULL) {
    	trie->root = element;
    	element->glue_node = ADAP_FALSE;
    	element->prefix = (ADAP_Uint8 *)adap_malloc((prefix_length - 1) / 8 + 1);
		if (element->prefix == NULL) {
			return ENETHAL_STATUS_MEM_ALLOC;
		}
    	adap_memcpy(element->prefix, prefix, (prefix_length - 1) / 8 + 1);
    	element->prefix_length = prefix_length;
    	element->parent = NULL;
    	element->left = NULL;
    	element->right = NULL;

    	trie->size++;

		return ENETHAL_STATUS_SUCCESS;
    }

    /* Here the Trie is not empty */
    /* Traverse the LPC-trie */
    node = trie->root;
    while (node->prefix_length <= prefix_length) {
    	diff_bit = adap_trie_find_unmatched_bit(prefix, node->prefix, node->prefix_length);
    	if (diff_bit < node->prefix_length) {
			glue = (adap_trie_element *)adap_malloc(sizeof(adap_trie_element));
			if (glue == NULL) {
				return ENETHAL_STATUS_MEM_ALLOC;
			}
			glue->glue_node = ADAP_TRUE;
			glue->prefix_length = diff_bit;
			glue->prefix = (ADAP_Uint8 *)adap_malloc((diff_bit - 1) / 8 + 1);
			if (glue->prefix == NULL) {
				adap_safe_free(glue);
				return ENETHAL_STATUS_MEM_ALLOC;
			}
			adap_memcpy(glue->prefix, prefix, (diff_bit - 1) / 8 + 1);
			glue->parent = node->parent;

			if (node->parent == NULL) {
				trie->root = glue;
			} else if (node->parent->left == node) {
				node->parent->left = glue;
			} else {
				node->parent->right = glue;
			}

			bit = ADAP_TRIE_BIT_TEST(node->prefix, diff_bit);
			node->parent = glue;
			if (bit) {
				glue->left = NULL;
				glue->right = node;
			} else {
				glue->right = NULL;
				glue->left = node;
			}

			node = glue;

			adap_assert(prefix_length > node->prefix_length);
    	}

		if (prefix_length == node->prefix_length) {
			/*
			if (node->glue_node == ADAP_FALSE) {
				return ENETHAL_STATUS_ALREADY_FOUND;
			}
			*/

			adap_memcpy(element, node, sizeof(adap_trie_element));

			if (node->parent == NULL) {
				trie->root = element;
			} else if (node->parent->left == node) {
				node->parent->left = element;
			} else {
				node->parent->right = element;
			}

			if (node->left != NULL) {
				node->left->parent = element;
			}
			if (node->right != NULL) {
				node->right->parent = element;
			}

			if (node->glue_node == ADAP_TRUE) {
				element->glue_node = ADAP_FALSE;
				adap_safe_free(node);
			}

			return ENETHAL_STATUS_SUCCESS;
		}

		bit = ADAP_TRIE_BIT_TEST(prefix, diff_bit);
		if (bit) {
			if (node->right != NULL) {
		    	node = node->right;
			} else {
		    	element->prefix_length = prefix_length;
		    	element->prefix = (ADAP_Uint8 *)adap_malloc((prefix_length - 1) / 8 + 1);
				if (element->prefix == NULL) {
					return ENETHAL_STATUS_MEM_ALLOC;
				}
				adap_memcpy(element->prefix, prefix, (prefix_length - 1) / 8 + 1);
		    	element->parent = node;
		    	element->left = NULL;
		    	element->right = NULL;
		    	element->glue_node = ADAP_FALSE;

            	node->right = element;

				return ENETHAL_STATUS_SUCCESS;
			}
		} else {
			if (node->left != NULL) {
		    	node = node->left;
			} else {
		    	element->prefix_length = prefix_length;
		    	element->prefix = (ADAP_Uint8 *)adap_malloc((prefix_length - 1) / 8 + 1);
				if (element->prefix == NULL) {
					return ENETHAL_STATUS_MEM_ALLOC;
				}
				adap_memcpy(element->prefix, prefix, (prefix_length - 1) / 8 + 1);
		    	element->parent = node;
		    	element->left = NULL;
		    	element->right = NULL;
		    	element->glue_node = ADAP_FALSE;

            	node->left = element;

				return ENETHAL_STATUS_SUCCESS;
			}
		}
    }

	diff_bit = adap_trie_find_unmatched_bit(prefix, node->prefix, prefix_length);
	if (diff_bit == prefix_length) {
    	element->prefix = (ADAP_Uint8 *)adap_malloc((prefix_length - 1) / 8 + 1);
		if (element->prefix == NULL) {
			return ENETHAL_STATUS_MEM_ALLOC;
		}
		adap_memcpy(element->prefix, prefix, (prefix_length - 1) / 8 + 1);
		element->prefix_length = prefix_length;
		element->glue_node = ADAP_FALSE;
		element->parent = node->parent;

		if (node->parent == NULL) {
			trie->root = element;
		} else if (node->parent->left == node) {
			node->parent->left = element;
		} else if (node->parent->right == node) {
			node->parent->right = element;
		}

		bit = ADAP_TRIE_BIT_TEST(node->prefix, diff_bit);
		node->parent = element;
		if (bit) {
			element->left = NULL;
			element->right = node;
		} else {
			element->right = NULL;
			element->left = node;
		}
	} else {
		glue = (adap_trie_element *)adap_malloc(sizeof(adap_trie_element));
		if (glue == NULL) {
			return ENETHAL_STATUS_MEM_ALLOC;
		}
		glue->glue_node = ADAP_TRUE;
		glue->prefix_length = diff_bit;
		glue->prefix = (ADAP_Uint8 *)adap_malloc((diff_bit - 1) / 8 + 1);
		if (glue->prefix == NULL) {
			adap_safe_free(glue);
			return ENETHAL_STATUS_MEM_ALLOC;
		}
		adap_memcpy(element->prefix, prefix, (diff_bit - 1) / 8 + 1);

		glue->parent = node->parent;
		if (node->parent == NULL) {
			trie->root = glue;
		} else if (node->parent->left == node) {
			node->parent->left = glue;
		} else {
			node->parent->right = glue;
		}

		bit = ADAP_TRIE_BIT_TEST(node->prefix, diff_bit);
		node->parent = glue;

		element->prefix = (ADAP_Uint8 *)adap_malloc((prefix_length - 1) / 8 + 1);
		if (glue->prefix == NULL) {
			/* TODO - Missing rollback here */
			return ENETHAL_STATUS_MEM_ALLOC;
		}
		adap_memcpy(element->prefix, prefix, (prefix_length - 1) / 8 + 1);

		element->glue_node = ADAP_FALSE;
		element->parent = glue;

		if (bit) {
			glue->left = element;
			glue->right = node;
		} else {
			glue->right = element;
			glue->left = node;
		}
	}

	trie->size++;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_trie_remove(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element** element)
{
	EnetHal_Status_t status;
	adap_trie_element *node;
	adap_trie_element *glue;
	adap_trie_element *parent;
	adap_trie_element *sibling;
	adap_trie_element *child;

    if ((trie == NULL) || (prefix == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (trie->root == NULL) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    status = adap_trie_find_exact(trie, prefix, prefix_length, &node);
    if (status != ENETHAL_STATUS_SUCCESS) {
    	return status;
    }

    adap_assert(node->glue_node == ADAP_FALSE);

	*element = node;


    /* case #1:  The node has 2 childs
     *           =====================
     *           The node is changed to a glue node.
     */
    if ((node->right != NULL) && (node->left != NULL)) {
    	glue = (adap_trie_element *)adap_malloc(sizeof(adap_trie_element));
    	if (glue == NULL) {
    		return ENETHAL_STATUS_MEM_ALLOC;
    	}

    	adap_memcpy(glue, node, sizeof(sizeof(adap_trie_element)));
    	glue->glue_node = ADAP_TRUE;

    	node->left->parent = glue;
    	node->right->parent = glue;

    	if (trie->root == node) {
    		trie->root = glue;
    	} else if (node == node->parent->left) {
    		node->parent->left = glue;
    	} else {
    		node->parent->right = glue;
    	}

    	trie->size--;

		return ENETHAL_STATUS_SUCCESS;
    }

    /* case #2:  The node has 0 childs (leaf)
     *           ============================
     *           a. If node is root (parent is NULL) then the trie becomes empty.
     *           b. If parent is a glue node (prefix is NULL) then we need to remove the parent as well (sibling may be now root).
     *           c. If parent is not a glue node then just remove the node as a child (only sibling stays).
     */
    if ((node->right == NULL) && (node->left == NULL)) {
		/* Check if node is root */
		if (node->parent == NULL) {
			trie->root = NULL;
			trie->size--;
			return ENETHAL_STATUS_SUCCESS;
		}

		/* determine parent & sibling */
		if (node->parent->right == node) {
			parent = node->parent;
			parent->right = NULL;
			sibling = parent->left;
		} else {
			parent = node->parent;
			parent->left = NULL;
			sibling = parent->right;
		}

		/* Check if parent is a glue node */
		if (node->parent->glue_node == ADAP_TRUE) {
			if (parent->parent == NULL) {
				trie->root = sibling;
			} else if (parent->parent->right == parent) {
				parent->parent->right = sibling;
			} else {
				parent->parent->left = sibling;
			}
			sibling->parent = parent->parent;

			adap_safe_free(parent->prefix);
			adap_safe_free(parent);
		}

		trie->size--;

		return ENETHAL_STATUS_SUCCESS;
    }


    /* case #3:  The node has 1 child
     *           ====================
     *           If node is root (parent is NULL) then child is now root.
     *           Otherwise connect child & parent of node.
     */
    if (node->right != NULL) {
    	child = node->right;
    } else {
    	child = node->left;
    }
    child->parent = node->parent;

    if (node->parent == NULL) {
		trie->root = child;
    } else if (node->parent->right == node) {
    	node->parent->right = child;
    } else {
    	node->parent->left = child;
    }

    adap_safe_free(node->prefix);

    trie->size--;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_trie_find_best(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element** element)
{
	EnetHal_Status_t status = ENETHAL_STATUS_NOT_FOUND;
	adap_trie_element *node;

    if ((trie == NULL) || (prefix == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (trie->root == NULL) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    node = trie->root;
    while (node->prefix_length <= prefix_length) {
		if (node->glue_node == ADAP_FALSE) {
			if (adap_trie_prefix_compare(node->prefix, prefix, node->prefix_length) == 0) {
				status = ENETHAL_STATUS_SUCCESS;
				*element = node;
			}
		} else {
			break;
		}

		if (ADAP_TRIE_BIT_TEST(prefix, node->prefix_length)) {
			node = node->right;
		} else {
			node = node->left;
		}

		if (node == NULL) {
			break;
		}
    }

    return status;
}

EnetHal_Status_t adap_trie_find_exact(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element** element)
{
	adap_trie_element *node;

    if ((trie == NULL) || (prefix == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (trie->root == NULL) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    node = trie->root;
    while (node->prefix_length < prefix_length) {
		if (ADAP_TRIE_BIT_TEST(prefix, node->prefix_length)) {
			node = node->right;
		} else {
			node = node->left;
		}

		if (node == NULL) {
			return ENETHAL_STATUS_NOT_FOUND;
		}
    }

    if ((node->prefix_length > prefix_length) || (node->glue_node == ADAP_TRUE)) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    if (adap_trie_prefix_compare(node->prefix, prefix, prefix_length) != 0) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    *element = node;

	return ENETHAL_STATUS_SUCCESS;
}

static
adap_trie_element * adap_trie_get_first(adap_trie_element *element)
{
	if (element == NULL) {
		return NULL;
	}

	if (element->left != NULL) {
		return adap_trie_get_first(element->left);
	}

	if (element->right != NULL) {
		return adap_trie_get_first(element->right);
	}

	return element;
}

EnetHal_Status_t adap_trie_iter(adap_trie* trie, ADAP_Uint8 *c_prefix, ADAP_Uint32 c_prefix_length, ADAP_Uint8 **prefix, ADAP_Uint32 *prefix_length, adap_trie_element **element)
{
	adap_trie_element *node;
	adap_trie_element *parent;

	if ((trie == NULL) || (prefix == NULL) || (element == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

	if (trie->root == NULL) {
		return ENETHAL_STATUS_NOT_FOUND;
	}

	if (c_prefix == NULL) {
		node = adap_trie_get_first(trie->root);
		*prefix = (ADAP_Uint8 *)adap_malloc(node->prefix_length);
		adap_memcpy(*prefix, node->prefix, node->prefix_length);
		*prefix_length = node->prefix_length;
		*element = node;
		return ENETHAL_STATUS_SUCCESS;
	} else {
	    node = trie->root;
	    while (node->prefix_length <= c_prefix_length) {
			if (ADAP_TRIE_BIT_TEST(c_prefix, node->prefix_length)) {
				if (node->right != NULL) {
					node = node->right;
				} else {
					break;
				}
			} else {
				if (node->left != NULL) {
					node = node->left;
				} else {
					break;
				}
			}
	    }

	    if ((c_prefix_length != node->prefix_length) ||
	    	(node->glue_node == ADAP_TRUE) ||
	    	(adap_trie_prefix_compare(node->prefix, c_prefix, c_prefix_length) != 0)) {
	    	/* node not found */
	    	return ENETHAL_STATUS_NOT_SUPPORTED;
	    }

	    while (node != NULL) {
		    if (node == trie->root) {
		    	return ENETHAL_STATUS_NOT_FOUND;
		    }

	    	parent = node->parent;
	    	if (node == parent->left) {
	    		node = adap_trie_get_first(parent->right);
		    	if (node != NULL) {
		    		adap_assert(node->glue_node == ADAP_FALSE);
		    		break;
		    	}
	    	}
	    	node = parent;
	    	if (node->glue_node == ADAP_FALSE) {
	    		break;
	    	}
	    }

		*prefix = (ADAP_Uint8 *)adap_malloc(node->prefix_length);
		adap_memcpy(*prefix, node->prefix, node->prefix_length);
		*prefix_length = node->prefix_length;
		*element = node;
		return ENETHAL_STATUS_SUCCESS;
	}
}

ADAP_Uint32 adap_trie_size(adap_trie* trie)
{
    if (trie == NULL) {
        return 0;
    }

    return trie->size;
}
