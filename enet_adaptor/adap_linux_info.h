/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*
 * adap_linux_info.h
 *
 *  Created on: Feb 15, 2017
 *      Author: root
 */

#ifndef ENET_ADAPTOR_ADAP_LINUX_INFO_H_
#define ENET_ADAPTOR_ADAP_LINUX_INFO_H_



ADAP_Int32 get_source_ip_address_by_host_name(ADAP_Int32 num_of_interfaces,tLinuxIpInterface *pInterfaceArray);
ADAP_Int32 get_source_mac_by_host_name(ADAP_Int32 num_of_interfaces,tLinuxIpInterface *pInterfaceArray);
ADAP_Int32 get_arp_cache_per_interface(char *interface_name,ADAP_Int32 *maxOfEntries,tLinuxIpInterface *pInterfaceArray);
uint32_t init_vxlan_thread(void);
ADAP_Int32 get_linux_interface_name(char *fileName,tLinuxIpInterface *pInterfaceArray);

#endif /* ENET_ADAPTOR_ADAP_LINUX_INFO_H_ */
