/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include  <string.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "adap_pppnp.h"


struct session_entry {
	/* RB-tree element */
	adap_rbtree_element	RbNode;

	/* key */
	ADAP_Uint32 		SessionId;

	/* result */
	tEnetHal_MacAddr UserMACAddr;
	tEnetHal_MacAddr SrcMACAddr;
	ADAP_Uint32      vpn;
	ADAP_Uint32 	 IfIndex;
	ADAP_Uint32 	 UserIpAddr;
	ADAP_Uint32 	 Port;
	ADAP_Uint32 	 NpStatus;
	ADAP_Uint32      VlanId;
};


adap_rbtree     *gpPPPoESessionTbl = NULL;


static
int PPPoESessionTblCmp(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct session_entry *session1 = container_of(e1, struct session_entry, RbNode);
	struct session_entry *session2 = container_of(e2, struct session_entry, RbNode);

	if (session1->SessionId < session2->SessionId) {
		return -1;
	} else if (session1->SessionId > session2->SessionId) {
		return 1;
	} else {
		return 0;
	}
}

static
void PPPoESessionTblSwap(adap_rbtree_element * e1, adap_rbtree_element * e2)
{
	struct session_entry *session1 = container_of(e1, struct session_entry, RbNode);
	struct session_entry *session2 = container_of(e2, struct session_entry, RbNode);
	struct session_entry temp;

	adap_memcpy(&temp, session1, sizeof(struct session_entry));

	session1->SessionId = session2->SessionId;
	adap_memcpy(&session1->UserMACAddr, &session2->UserMACAddr, sizeof(tEnetHal_MacAddr));
	adap_memcpy(&session1->SrcMACAddr, &session2->SrcMACAddr, sizeof(tEnetHal_MacAddr));
	session1->vpn = session2->vpn;
	session1->IfIndex = session2->IfIndex;
	session1->UserIpAddr = session2->UserIpAddr;
	session1->Port = session2->Port;
	session1->NpStatus = session2->NpStatus;
	session1->VlanId = session2->VlanId;

	session2->SessionId = temp.SessionId;
	adap_memcpy(&session2->UserMACAddr, &temp.UserMACAddr, sizeof(tEnetHal_MacAddr));
	adap_memcpy(&session2->SrcMACAddr, &temp.SrcMACAddr, sizeof(tEnetHal_MacAddr));
	session2->vpn = temp.vpn;
	session2->IfIndex = temp.IfIndex;
	session2->UserIpAddr = temp.UserIpAddr;
	session2->Port = temp.Port;
	session2->NpStatus = temp.NpStatus;
	session2->VlanId = temp.VlanId;
}


ADAP_Uint8 EnetHal_FsNpPppInit (void)
{
	EnetHal_Status_t status;
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;
	struct adap_rbtree_params rbtree_params;

	for(idx=1; idx < MeaAdapGetNumOfPhyPorts(); idx++) {
		ret = getLxcpId(idx,&lxcp_Id);
		if(ret == ENET_SUCCESS)	{
			ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_PPPoE_DISCOVERY,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
			ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_PPPoE_ICP_LCP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		}
	}

	/* Database initialise for PPPoE session */
	rbtree_params.capacity = 0; /* Unlimited */
	rbtree_params.compare = PPPoESessionTblCmp;
	rbtree_params.swap = PPPoESessionTblSwap;
	status = adap_rbtree_init(&gpPPPoESessionTbl, &rbtree_params);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,"PPPoE Session table RedBlkTree creation failed\n");
		return ENET_FAILURE;
	}

	return ret;
}

ADAP_Uint8 EnetHal_FsNpPPPoEAddNewSession(ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *SessMACAddr,
					tEnetHal_MacAddr *SrcMACAddr, ADAP_Uint32 u2Port,
					ADAP_Uint32 SessionId, ADAP_Uint32 ACCookieId,tEnetHal_VlanTag *pVlanTag)
{
	EnetHal_Status_t status;
	ADAP_Uint8 		editingtype = 0;
	MEA_Action_t 		action_Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t		PhyPort;
    	ADAP_Uint32         	logPort = 0;
	tEnetHal_MacAddr 	DestMACAddr;
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	tIpV4NextRoute   	*pNextRoute=NULL;
	tIpDestV4Routing 	*pIpv4Routing=NULL;
	adap_rbtree_element *rbtree_elem;
    struct session_entry  *pSession;
    struct session_entry   Session;
	tEnetHal_FsNpL3IfInfo   *pRouterPort=NULL;
        ADAP_Uint32              VpnIndex = 0;

	adap_memset(&DestMACAddr, 0, sizeof(tEnetHal_MacAddr));
	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Session, 0, sizeof(struct session_entry));

	Session.SessionId = SessionId;
	adap_memcpy(&Session.UserMACAddr, SessMACAddr, sizeof(tEnetHal_MacAddr));
	status = adap_rbtree_find(gpPPPoESessionTbl, &Session.RbNode, &rbtree_elem);
	if (status == ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Session with ID %d already exists \r\n",SessionId);
		return ENET_FAILURE;
	}

	pSession = container_of(rbtree_elem, struct session_entry, RbNode);

	if(pVlanTag->OuterVlanTag.u2VlanId != 0) {
		VpnIndex = adap_GetVpnByVlan(pVlanTag->OuterVlanTag.u2VlanId);
	} else {
		pRouterPort = getRouterPortInfo(u2Port);
		if (pRouterPort != NULL) {
			VpnIndex = adap_GetVpnByVlan(pRouterPort->u2PortVlanId);
		}
	}

	/* Add PPPoE session MAC and session ID to Adaptor DB */
	pSession = (struct session_entry*)adap_malloc(sizeof(struct session_entry));
	if (pSession == NULL) {
		return ENET_FAILURE;
	}

	pSession->SessionId = SessionId;
	pSession->IfIndex = u4IfIndex;
	pSession->Port = u2Port;
	pSession->NpStatus = ADAP_FALSE;
	pSession->vpn = VpnIndex;
	pSession->VlanId = pVlanTag->OuterVlanTag.u2VlanId;
	adap_memcpy(&pSession->UserMACAddr, SessMACAddr, sizeof(tEnetHal_MacAddr));
	adap_memcpy(&pSession->SrcMACAddr, SrcMACAddr, sizeof(tEnetHal_MacAddr));
	status = adap_rbtree_insert(gpPPPoESessionTbl, &pSession->RbNode);
	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create PPPoE session in Adaptor DB\n");
		return ENET_FAILURE;
	}

	if (AdapGetDefaultGatewayRouteTable(&pNextRoute) != ENET_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get default route \r\n");
		return ENET_FAILURE;
	}

	if (AdapGetEntryIpTable(&pIpv4Routing,pNextRoute->u4NextHopGt) != ENET_SUCCESS) {
		AdapAddFailedArp (pNextRoute->u4NextHopGt);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get nexthop MAC for default route \r\n");
		return ENET_FAILURE;
	}

	adap_memcpy(&DestMACAddr, &pIpv4Routing->macAddress, sizeof(tEnetHal_MacAddr));
        if(pVlanTag->OuterVlanTag.u2VlanId != 0)
        {
	    editingtype = MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan;
        }
        else
        {
	editingtype = MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA;
        }


	logPort = adap_VlanGetPortByVlanId (pNextRoute->u2VlanId);
	MeaAdapGetPhyPortFromLogPort(logPort, &PhyPort);
	MEA_SET_OUTPORT(&tOutPorts,PhyPort);

	if(MeaDrvCreatePPPoEAction(&action_Id, SrcMACAddr, &DestMACAddr,
			editingtype, pNextRoute->u2VlanId, SessionId, PhyPort) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\n");
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d created\n", action_Id);

	if (MeaDrvHwCreatePPPoEForwarder(action_Id, SessMACAddr, SessionId, VpnIndex,&tOutPorts) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create PPPoE session in forwarder\n");
		return ENET_FAILURE;
	} else if (pSession != NULL) {
		pSession->NpStatus = ADAP_TRUE;
	}

	return ENET_SUCCESS;
}



ADAP_Uint8 
EnetHal_FsNpPppIPCPUp (ADAP_Uint32 IfIndex, ADAP_Uint32 LocalIpAddr, ADAP_Uint32 UserIpAddr)
{
	EnetHal_Status_t status;
	ADAP_Uint8 		editingtype = 0;
	MEA_Action_t 		action_Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t		PhyPort;
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	adap_rbtree_element *rbtree_elem;
    struct session_entry *pSession;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	status = adap_rbtree_iter(gpPPPoESessionTbl, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		pSession = container_of(rbtree_elem, struct session_entry, RbNode);
		if (pSession->IfIndex == IfIndex) {
			pSession->UserIpAddr = UserIpAddr;
			break;
		}
		status = adap_rbtree_iter(gpPPPoESessionTbl, rbtree_elem, &rbtree_elem);
	}

	if (status != ENETHAL_STATUS_SUCCESS) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"No PPPoE session for the ID %d\n",IfIndex);
		return ENET_FAILURE;
	}

        if (pSession->VlanId != 0)
        {
            editingtype = MEA_EDITINGTYPE_PPPoE_DL_Append_header_Swap_vlan;
        }
        else
        {
	editingtype = MEA_EDITINGTYPE_PPPoE_DL_Append_header_Extract_valn_Stamp_DA_SA;
        }
	MeaAdapGetPhyPortFromLogPort (pSession->Port, &PhyPort);
	MEA_SET_OUTPORT(&tOutPorts,PhyPort);

	if(MeaDrvCreatePPPoEAction(&action_Id, &pSession->SrcMACAddr, &pSession->UserMACAddr,
			editingtype, pSession->VlanId, pSession->SessionId, PhyPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\n");
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d created\n", action_Id);

	if(MeaDrvHwCreateDestIpForwarder(action_Id,MEA_TRUE,UserIpAddr, 1,
				START_L3_VPN,&tOutPorts) != MEA_OK) {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to create entry in forwarder\n");
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}


ADAP_Uint8 adap_HandleFailedPPPoESession()
{
	EnetHal_Status_t status;
	ADAP_Uint8 		editingtype = 0;
	MEA_Action_t 		action_Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t		PhyPort;
	ADAP_Uint32         	logPort = 0;
	tEnetHal_MacAddr 	DestMACAddr;
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	tIpV4NextRoute   	*pNextRoute=NULL;
	tIpDestV4Routing 	*pIpv4Routing=NULL;
	adap_rbtree_element *rbtree_elem;
	struct session_entry *pSession;

	adap_memset(&DestMACAddr, 0, sizeof(tEnetHal_MacAddr));
	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	status = adap_rbtree_iter(gpPPPoESessionTbl, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		pSession = container_of(rbtree_elem, struct session_entry, RbNode);

		if (pSession->NpStatus == ADAP_FALSE) {
			if (AdapGetDefaultGatewayRouteTable (&pNextRoute) != ENET_SUCCESS) {
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get default route \r\n");
				return ENET_FAILURE;
			}

			if(AdapGetEntryIpTable(&pIpv4Routing,pNextRoute->u4NextHopGt) != ENET_SUCCESS) {
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get nexthop MAC for default route \r\n");
				return ENET_FAILURE;
			}

			adap_memcpy(&DestMACAddr, &pIpv4Routing->macAddress, sizeof(tEnetHal_MacAddr));
                        if(pSession->VlanId != 0)
                        {
                            editingtype = MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan;
                        }
                        else
                        {
			editingtype = MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA;
                        }

			logPort = adap_VlanGetPortByVlanId (pNextRoute->u2VlanId);
			MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
			MEA_SET_OUTPORT(&tOutPorts,PhyPort);

			if(MeaDrvCreatePPPoEAction(&action_Id, &pSession->SrcMACAddr, &DestMACAddr,
					editingtype, pNextRoute->u2VlanId, pSession->SessionId, PhyPort) != MEA_OK) {
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\n");
				return ENET_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d created\n", action_Id);

			if (MeaDrvHwCreatePPPoEForwarder(action_Id, &pSession->UserMACAddr, pSession->SessionId,pSession->vpn, &tOutPorts) != MEA_OK) {
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create PPPoE session in forwarder\n");
				return ENET_FAILURE;
			} else {
				pSession->NpStatus = ADAP_TRUE;
			}
		}

		status = adap_rbtree_iter(gpPPPoESessionTbl, rbtree_elem, &rbtree_elem);
	}

	return ENET_SUCCESS;
}


ADAP_Uint8 EnetHal_FsNpPPPoEDelSession (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u2Port, ADAP_Uint32 SessionId)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
	struct session_entry *pSession;
    struct session_entry  Session ;

	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u2Port);

	adap_memset(&Session, 0, sizeof(struct session_entry));

	Session.SessionId = SessionId;
	status = adap_rbtree_find(gpPPPoESessionTbl, &Session.RbNode, &rbtree_elem);
	if (status == ENETHAL_STATUS_SUCCESS) {
		pSession = container_of(rbtree_elem, struct session_entry, RbNode);

		status = adap_rbtree_remove(gpPPPoESessionTbl, &Session.RbNode, &rbtree_elem);
		if (status == ENETHAL_STATUS_SUCCESS) {
			MeaDrvHwDeleteDestIpForwarder(pSession->UserIpAddr, D_IP_MASK_32,START_L3_VPN);
			MeaDrvHwDeletePPPoEForwarder(&pSession->UserMACAddr, SessionId, pSession->vpn);
			pSession = container_of(rbtree_elem, struct session_entry, RbNode);
			adap_safe_free(pSession);
		}
	}

	return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_FsNpPPPoEAssociateVlan (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Port, tEnetHal_VlanTag *pVlanTag)
{
	tBridgeDomain *pBridgeDomain=NULL;
	tIpInterfaceTable *pInterfaceTable=NULL;
	ADAP_Int32 ret = ENET_FAILURE;
	ADAP_Uint32 VlanId = 0;
	MEA_LxCp_t lxcp_Id=0;

	VlanId = pVlanTag->OuterVlanTag.u2VlanId;
	if (EnetHal_FsMiVlanHwCreateFdbId (0, VlanId) == ENET_SUCCESS)
	{
		if (EnetHal_FsMiVlanHwAssociateVlanFdb (0, VlanId, VlanId) == ENET_SUCCESS)
		{
			Adap_bridgeDomain_semLock();
			pBridgeDomain = Adap_getBridgeDomainByVlan (VlanId, 0);
			if(pBridgeDomain != NULL)
			{
				adap_memset(&pBridgeDomain->ifIndex,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
				adap_memset(&pBridgeDomain->ifType,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);

				pBridgeDomain->ifIndex[0] = u4Port;
				pBridgeDomain->ifType[0] = VLAN_TAGGED_MEMBER_PORT;
				pBridgeDomain->num_of_ports=1;
				AdapSetAccepFrameType(pBridgeDomain->ifIndex[0],ADAP_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES);
				setPortToPvid (pBridgeDomain->ifIndex[0],VlanId);

				if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
						(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
				{
					AdapSetProviderCoreBridgeMode(ADAP_PROVIDER_NETWORK_PORT,pBridgeDomain->ifIndex[0]);
				}

				if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[0],
							pBridgeDomain,VLAN_TAGGED_MEMBER_PORT) == MEA_OK)
				{
					ret = ENET_SUCCESS;
				}
			}
			Adap_bridgeDomain_semUnlock();
		}
	}

	pInterfaceTable = AdapGetIpInterface (u4IfIndex);
	if ((pInterfaceTable != NULL) && (ret == ENET_SUCCESS))
	{
            pInterfaceTable->u2VlanId = VlanId;
        }

	ret = getLxcpId(u4Port,&lxcp_Id);
	if(ret == ENET_SUCCESS)
	{
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,
				MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,
				MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,
				MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

	return ret;
}

ADAP_Uint16 MeaAdapGetPPPSessionVpn (ADAP_Uint16 SessId)
{
	EnetHal_Status_t status;
	adap_rbtree_element *rbtree_elem;
    struct session_entry *pSession;
    ADAP_Uint16       vpn=0;

	status = adap_rbtree_iter(gpPPPoESessionTbl, NULL, &rbtree_elem);
	while (status == ENETHAL_STATUS_SUCCESS) {
		pSession = container_of(rbtree_elem, struct session_entry, RbNode);
        if (pSession->SessionId == SessId) {
            vpn = pSession->vpn;
            break;
        }
		status = adap_rbtree_iter(gpPPPoESessionTbl, rbtree_elem, &rbtree_elem);
	}

    return vpn;
}

