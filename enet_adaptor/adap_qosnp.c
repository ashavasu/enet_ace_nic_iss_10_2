/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_qosnp.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_acl.h"
#include "adap_linklist.h"
#include "adap_enetConvert.h"
#include "adap_service_config.h"
#include "EnetHal_L2_Api.h"
#include "adap_pppnp.h"

static ADAP_Uint16 IssWeight2EnetWeight(ADAP_Uint16 IssWeight)
{
	ADAP_Uint16 enetWeight=0;
    /* Convert Iss weight (0-1000) values to the ENET weight (3-112) */
	if(IssWeight ==0 || IssWeight <= 40 )
		enetWeight = 3;
	else
	{
		if ((IssWeight * 100/1000) >= 100)
			enetWeight = 100;
		else
			enetWeight = (IssWeight * 100/1000);
	}
	enetWeight = (enetWeight) ? enetWeight : enetWeight + 1;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IssWeight2EnetWeight ISS weight:%d ENET weight :%d\n",IssWeight,enetWeight);

	return enetWeight;
}

//static MEA_WredProfileId_t MeaDrvQosUpdateWredProfile(tEnetHal_QoSREDCfgEntry WredProfileConfigTable[ENET_PLAT_QUEUE_NUM_OF_PRI_Q]);
static ADAP_Uint32 MeaDrvQosDeleteAllWredProfiles(void);

static ADAP_Uint32 MeaDrvUpdateEnetQueues (ADAP_Uint16 issPortId, ENET_QueueId_t clusterId)
{
     MEA_Port_t       enetPort;

    ADAP_Uint16      h1Scheduler;                                    /*Root scheduler' Index */
    ADAP_Uint16              h2Scheduler;                            /*The L2 scheduler we re currently working on  */

    tSchedulerDb * pScheduler1 = NULL;
    tSchedulerDb * pScheduler2 = NULL;
    tSchedulerDb * pScheduler3 = NULL;

    tQueueDb *    pPriQueue = NULL;
    ENET_Queue_dbt   enetQueue;
    ADAP_Int8		 strPrint[100];
	ADAP_Uint32		 idxPrint=0;
    tEnetHal_QoSREDCfgEntry  WredProfileConfigTable[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
    ADAP_Int16         idx;
    ADAP_Uint16         H2ChildIdx;
 	MEA_EgressPort_Entry_dbt Egress_entry;
 	//MEA_WredProfileId_t WredProfile;
    MEA_Globals_Entry_dbt globals_entry;

    /* Delete all WRED profiles - todo*/
    MeaDrvQosDeleteAllWredProfiles();

    /***** Build the Queues,Schedulers,Shapers tree for each Egress interface *****/
	if(MeaAdapGetPhyPortFromLogPort(issPortId,&enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"start enetPort:%d\n",enetPort);

	/* Pass the Hierarchy tree */
	/* Find the H1 level scheduler - it MUST be one for the given I/F*/
	for(h1Scheduler=1; h1Scheduler<MAX_NUM_OF_SCHEDULERS; h1Scheduler++)
	{
		pScheduler1 = adap_getSchedulerEntryByIndex(h1Scheduler);

		if( pScheduler1 != NULL && (pScheduler1->iss_PortNumber == issPortId) && (pScheduler1->level == H1Interface))
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues: Root scheduler found: SchedId: %d IssPort: %d enetPort:%d\r\n",
				pScheduler1->issScheduler.QosSchedulerId, pScheduler1->iss_PortNumber,	enetPort);

			for(H2ChildIdx=0; H2ChildIdx<ENET_PLAT_QUEUE_NUM_OF_PRI_Q; H2ChildIdx++)
			{
				idxPrint += sprintf(&strPrint[idxPrint],"%d ",(ADAP_Uint16)pScheduler1->ChildsSchedIdx[H2ChildIdx]);
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Children:%s\r\n ",strPrint);
			break;
		}
	}
	if(h1Scheduler == MAX_NUM_OF_SCHEDULERS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Root scheduler on ISS Port %d not found !!! \n", issPortId);
		return ENET_FAILURE;
	}

	/* Assign the H1 schedulers to Interface */
	ENET_ADAPTOR_Get_EgressPort_Entry(MEA_UNIT_0, enetPort, &Egress_entry);

    if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) {
        Egress_entry.shaper_enable = MEA_TRUE;
    }

    Egress_entry.shaper_info.CIR = (MEA_EirCir_t)(pScheduler1->issShaper.u4QosCIR * MULTIPLAY_SHAPER_CIR_CBS);
    Egress_entry.shaper_info.CBS = (MEA_EbsCbs_t)(pScheduler1->issShaper.u4QosCBS);

    Egress_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    Egress_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    Egress_entry.shaper_info.overhead = 0;
    Egress_entry.shaper_info.Cell_Overhead = 0;

    if (Egress_entry.shaper_info.CIR == 0)
    {
        Egress_entry.shaper_enable = MEA_FALSE;
    }
    else
    {
        Egress_entry.shaper_enable = MEA_TRUE;
    }

    if (ENET_ADAPTOR_Set_EgressPort_Entry(MEA_UNIT_0, enetPort, &Egress_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ENET_ADAPTOR_Set_EgressPort_Entry for port %d failed\n", enetPort);
        return MEA_ERROR;
    }

	/* Assign the H2 schedulers to Cluster(==Queue)*/
	for(H2ChildIdx=0; H2ChildIdx<NUM_OF_CLUSTERS_PER_PORT; H2ChildIdx++)
	{
		h2Scheduler = pScheduler1->ChildsSchedIdx[H2ChildIdx];
		if(h2Scheduler == ADAP_END_OF_TABLE)
			continue;
		pScheduler2 = adap_getSchedulerEntryByIndex(h2Scheduler);
		if(pScheduler2 == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetQueues ERROR: Child %d for scheduler:%d not found \n", H2ChildIdx, h2Scheduler);
			continue;
		}

		/* Get the ENET queue */
		adap_memset(&enetQueue, 0, sizeof(enetQueue));
		if(ENET_IsValid_Queue(ENET_UNIT_0, pScheduler2->ClusterId, ENET_FALSE)!=ENET_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetQueues ERROR: ENET Queue %d is invalid \n",pScheduler2->ClusterId);
			return ENET_FAILURE;
		}
		if(ENET_Get_Queue (ENET_UNIT_0, pScheduler2->ClusterId, &enetQueue)!=ENET_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetQueues ERROR: Can't get the ENET Queue %d!!\n",pScheduler2->ClusterId);
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues: Enet Queue:%d obtained OK for L2 scheduler:%d\n",
				pScheduler2->ClusterId,
				h2Scheduler);

		/* Allocate the Priority queues to H3 schedulers and update the ENET Queue for all the Priority Queues*/
		for(idx=ENET_PLAT_QUEUE_NUM_OF_PRI_Q-1; idx>=0; idx--)
		{
			pScheduler3 = adap_getSchedulerEntryByIndex(pScheduler2->ChildsSchedIdx[idx]);
			if(pScheduler3 == NULL)
				continue;
			pPriQueue = adap_getQueueEntryByIndex(pScheduler3->QueueIdx);
			if(pPriQueue == NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"priority queue is NULL index:%d\n",pScheduler3->QueueIdx);
				continue;
			}
			pPriQueue->PriQueueIdx = idx;
			pPriQueue->WredProfileIdx = ADAP_END_OF_TABLE;
			if(pScheduler3->issScheduler.QosSchedAlgo == ADAP_QOS_SCHED_ALGO_SP)
			{
				enetQueue.pri_queues[idx].mode.type  = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
				enetQueue.pri_queues[idx].mode.value.strict_priority = pPriQueue->issQueue.u1QosQPriority/2; /* Aricent priority: 0-15, Ethernity: 0-7 */
				enetQueue.pri_queues[idx].mode.value.wfq_weight = 0;
			}
			else
			{
			   enetQueue.pri_queues[idx].mode.type  = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
			   enetQueue.pri_queues[idx].mode.value.wfq_weight = IssWeight2EnetWeight(pPriQueue->issQueue.u2QosQWeight); /* Aricent weight:0-1000, Ethernity: 3-112 */
			}
			/* Calculate max num of packets */
			if(pPriQueue->issQueueType.u4QueueSize < 8192)
				enetQueue.pri_queues[idx].max_q_size_Packets = 8;
			else
				enetQueue.pri_queues[idx].max_q_size_Packets = pPriQueue->issQueueType.u4QueueSize/2048;

			// granularity of 64 bytes
			enetQueue.pri_queues[idx].max_q_size_Byte = ( (pPriQueue->issQueueType.u4QueueSize/64)+1)*64;
			if(pScheduler3->issShaper.u4QosCIR == 0)
			{
				 enetQueue.pri_queues[idx].shaperPri_enable = MEA_FALSE;
			}
			 else
			{
				 enetQueue.pri_queues[idx].shaperPri_enable = MEA_TRUE;
			}
			enetQueue.pri_queues[idx].Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
			enetQueue.pri_queues[idx].shaper_info.mode = MEA_SHAPER_TYPE_MEF;
			/* Aricent SLA in Kbytes/Kbits/s, Ethernity:  SLA in Bytes/Bits/s*/
			enetQueue.pri_queues[idx].shaper_info.CIR = (pScheduler3->issShaper.u4QosCIR * MULTIPLAY_SHAPER_CIR_CBS ) ;
			enetQueue.pri_queues[idx].shaper_info.CBS = (pScheduler3->issShaper.u4QosCBS );
			enetQueue.pri_queues[idx].shaper_info.overhead = 20;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues: ISS Queue: ID:%d Size:%d Shaper: ID:%d CIR:%d CBS:%d EIR:%d EBS:%d @%p\n",
					pPriQueue->issQueue.i4QosQId,
					pPriQueue->issQueueType.u4QueueSize,
					pScheduler3->issShaper.u2ShaperId,
					pScheduler3->issShaper.u4QosCIR,
					pScheduler3->issShaper.u4QosCBS,
					pScheduler3->issShaper.u4QosEIR,
					pScheduler3->issShaper.u4QosEBS,
					&pScheduler3->issShaper);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues: L3 scheduler:%d Enet Queue:%d PriQueue:%d Qtype:%s Qsize(Bytes):%lu CIR:%lluBits/s CBS:%luBytes\n",
					pScheduler3->issScheduler.QosSchedulerId,
					pScheduler2->ClusterId,
					pPriQueue->PriQueueIdx,
					enetQueue.pri_queues[idx].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT ? "strict" : "WFQ  ",
					enetQueue.pri_queues[idx].max_q_size_Byte,
					enetQueue.pri_queues[idx].shaper_info.CIR,
					enetQueue.pri_queues[idx].shaper_info.CBS);
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1QosSchedAlgo=%s\n",pScheduler2->issScheduler.QosSchedAlgo==ADAP_QOS_SCHED_ALGO_SP?"Sp":"WFQ");

		/* Configure the shaping parameters for the Cluster itself from the H2 Scheduler */
		if(pScheduler2->issScheduler.QosSchedAlgo == ADAP_QOS_SCHED_ALGO_SP)
		{
		   enetQueue.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
		   enetQueue.mode.value.strict_priority = pPriQueue->issQueue.u1QosQPriority/2;
		   enetQueue.mode.value.wfq_weight = 0;
		}
		else
		{
		   if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK)
		    {
			printf("MEA_API_Get_Globals_Entry is failed!");
			return ENET_FAILURE;
		    }
		    if(globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY)
			    enetQueue.mode.type             = ENET_QUEUE_MODE_TYPE_RR;
		    else
			    enetQueue.mode.type    = ENET_QUEUE_MODE_TYPE_WFQ;
		   enetQueue.mode.value.wfq_weight = IssWeight2EnetWeight(pScheduler2->weight);
		}
		if(pScheduler2->issShaper.u4QosCIR == 0)
		{
			enetQueue.shaper_enable          = MEA_FALSE;
		}
		else
		{
			enetQueue.shaper_enable          = MEA_TRUE;
		}

		enetQueue.Shaper_compensation    = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
		/* TBD: enetQueue.shaper_Id = */
		enetQueue.shaper_info.mode = MEA_SHAPER_TYPE_MEF;
		enetQueue.shaper_info.CIR = (pScheduler2->issShaper.u4QosCIR *  MULTIPLAY_SHAPER_CIR_CBS ) ;
		enetQueue.shaper_info.CBS = (pScheduler2->issShaper.u4QosCBS );
		enetQueue.shaper_info.overhead = 20;

		for(idx=0; idx<ENET_PLAT_QUEUE_NUM_OF_PRI_Q; idx++)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"final MeaDrvUpdateEnetQueues: idx:%d Qtype:%s\n",
					idx,enetQueue.pri_queues[idx].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT ? "strict" : "WFQ  ");

		}

		if(ENET_Set_Queue (ENET_UNIT_0, pScheduler2->ClusterId, &enetQueue)!=ENET_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Cannot update the Queue %d\n", pScheduler2->ClusterId);
			return ENET_FAILURE;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues: ENET Queue %d is updated\n", pScheduler2->ClusterId);

		/* Allocate and configure WRED profile for this ENET Q */
		adap_memset(WredProfileConfigTable, 0, sizeof(WredProfileConfigTable));
		for(idx=ENET_PLAT_QUEUE_NUM_OF_PRI_Q-1; idx>=0; idx--)
		{
			pScheduler3 = adap_getSchedulerEntryByIndex(pScheduler2->ChildsSchedIdx[idx]);
			if(pScheduler3 == NULL)
				continue;
			pPriQueue = adap_getQueueEntryByIndex(pScheduler3->QueueIdx);
			if(pPriQueue == NULL)
				continue;

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Drop Alg type:%d, %s\n",
					pPriQueue->issQueueType.u1DropAlgo,
					pPriQueue->issQueueType.u1DropAlgoEnableFlag==0 ? "disabled" : "enabled");

			if(pPriQueue->issQueueType.u1DropAlgoEnableFlag &&
			(pPriQueue->issQueueType.u1DropAlgo==ADAP_QOS_Q_TEMP_DROP_TYPE_RED ||
			 pPriQueue->issQueueType.u1DropAlgo==ADAP_QOS_Q_TEMP_DROP_TYPE_WRED))
			{
				/* Update the WredProfileConfigTable entry */

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"PriQqNum:%d  MinThresh:%d Max Thresh:%d Prob:%d ECN Thresh: %d ActionFlag: %d\n",
						idx,
						pPriQueue->issWredEntry[0].u4MinAvgThresh,
						pPriQueue->issWredEntry[0].u4MaxAvgThresh,
						pPriQueue->issWredEntry[0].u1MaxProbability,
						pPriQueue->issWredEntry[0].u4ECNThresh,
						pPriQueue->issWredEntry[0].u1RDActionFlag
						);

				WredProfileConfigTable[idx].u4MinAvgThresh=pPriQueue->issWredEntry[0].u4MinAvgThresh;
				WredProfileConfigTable[idx].u4MaxAvgThresh=pPriQueue->issWredEntry[0].u4MaxAvgThresh;
				WredProfileConfigTable[idx].u1MaxProbability=pPriQueue->issWredEntry[0].u1MaxProbability;
				WredProfileConfigTable[idx].u4ECNThresh=pPriQueue->issWredEntry[0].u4ECNThresh;
				WredProfileConfigTable[idx].u1RDActionFlag=pPriQueue->issWredEntry[0].u1RDActionFlag;
				WredProfileConfigTable[idx].inUse = 1;
			}
		}
		//QoSHwSetIngressColorAwareInterfaceType (issPortId,1);

		/* currently Wred profile creation fails in ENET hence blocking*/
		/* Allocate the WRED profile and sub-profile */
//		WredProfile = MeaDrvQosUpdateWredProfile(WredProfileConfigTable);
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvUpdateEnetQueues - WRED profile updated:%d\n", WredProfile);
//		if(WredProfile==UNDEFINED_VALUE16)
//		{
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetQueues ERROR: MeaDrvQosUpdateWredProfile FAIL\n");
//			return ENET_FAILURE;
//		}
//		/* Attach the WRED profile to all relevant Services */
//		for(idx=ENET_PLAT_QUEUE_NUM_OF_PRI_Q-1; idx>=0; idx--)
//		{
//			pScheduler3 = adap_getSchedulerEntryByIndex(pScheduler2->ChildsSchedIdx[idx]);
//			if(pScheduler3 == NULL)
//				continue;
//			pPriQueue = adap_getQueueEntryByIndex(pScheduler3->QueueIdx);
//			if(pPriQueue == NULL)
//				continue;
//			pPriQueue->WredProfileIdx = WredProfile;
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvUpdateEnetQueues - WRED profile:%d attached to PriQueue:%d\n", WredProfile, pPriQueue->PriQueueIdx);
//			/* The rest is done in the MeaDrvAttachEthernityWredProfiles,
//			 * after the QoSHWUpdateEthernityPolicers links the Services to the ENET Queues */
//		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_QoSHwInit (void)
{
	ADAP_Uint16 issPortId;
    MEA_EditingMappingProfile_Id_t      	mappingId=ENET_PLAT_GENERATE_NEW_ID;
    MEA_EditingMappingProfile_Entry_dbt 	mappingEntry;
    ADAP_Uint16 									Idx;
    ADAP_Uint16                                   markingProfileId, cosMappingId;
    MEA_FlowMarkingMappingProfile_Entry_dbt FlowMarkingMappingProfileEntry;
    ADAP_Uint16                                   ItemTableIdx;
	MEA_FlowCoSMappingProfile_Entry_dbt 	entry_cos;

    for(issPortId=1; issPortId<MeaAdapGetNumOfPhyPorts(); issPortId++)
    {
    	MeaAdapSetIngressInterfaceType (issPortId,MEA_FILTER_KEY_INTERFACE_TYPE_SID);
	}

    //Create default mapping to be used in service
	adap_memset(&mappingEntry  , 0 , sizeof(mappingEntry ));

	mappingId = adap_GetDefaultMappingId(0);
	if(mappingId != MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_QoSHwInit - Default mapping already exists:%d\n",mappingId);
	}
	else
	{
		for(Idx=0;Idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;Idx++)
		{
			mappingEntry.item_table[Idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=Idx;
			mappingEntry.item_table[Idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN;
			mappingEntry.item_table[Idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=Idx;
			mappingEntry.item_table[Idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW;
		}

		if(MEA_API_Create_EditingMappingProfile_Entry(MEA_UNIT_0,&mappingEntry,&mappingId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwInit Error:fail to MEA_API_Create_EditingMappingProfile_Entry \n");
			return ENET_FAILURE;
		}
		adap_SetDefaultMappingId(0,mappingId);
		adap_SetEditMapping(mappingId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," EnetHal_QoSHwInit: Default EditingMapping created! mappingId:%d\n", mappingId);
	}

    //Create default flow marking profile to be used in evc service when tos/dscp priority is defined
	markingProfileId = adap_GetDefaultFlowMarkingId();
	if(markingProfileId != MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_QoSHwInit - Default marking already exists:%d\n",markingProfileId);
	}
	else
	{
        /* Create a default Flow Marking profile in ENET*/
		markingProfileId = 0;
        adap_memset(&FlowMarkingMappingProfileEntry, 0, sizeof(FlowMarkingMappingProfileEntry));
        FlowMarkingMappingProfileEntry.type = MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP;
        for (ItemTableIdx=0; ItemTableIdx < MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS; ItemTableIdx++)
        {
            FlowMarkingMappingProfileEntry.item_table[ItemTableIdx].valid=1;
            FlowMarkingMappingProfileEntry.item_table[ItemTableIdx].L2_PRI = (ItemTableIdx >> 3) & 0x7;
        }
        if(MEA_API_Create_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, &FlowMarkingMappingProfileEntry, &markingProfileId) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwInit ERROR: MEA_API_Create_FlowMarkingMappingProfile_Entry fail\n");
            return ENET_FAILURE;
        }
        adap_SetDefaultFlowMarkingId(markingProfileId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," EnetHal_QoSHwInit: Default FlowMarkingMappingProfile created! markingProfileId:%d\n", markingProfileId);
	}

    //Create default cos mapping profile to be used in evc service when tos/dscp priority is defined
	cosMappingId = adap_GetDefaultCosMappingId();
	if(cosMappingId != MEA_PLAT_GENERATE_NEW_ID)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_QoSHwInit - Default cos mapping already exists:%d\n",cosMappingId);
	}
	else
	{
        /* Create a default Cos mapping profile in ENET*/
		cosMappingId = MEA_PLAT_GENERATE_NEW_ID;
        adap_memset(&entry_cos, 0, sizeof(MEA_FlowCoSMappingProfile_Entry_dbt));

		for(Idx=0;Idx<MEA_FLOW_COS_MAPPING_MAX_NUM_OF_ITEMS;Idx++)
		{
			entry_cos.item_table[Idx].valid = 1;
			entry_cos.item_table[Idx].COS   = (Idx >> 3) & 0x7;
			/* Color input to Policer 0-Green / 1-Yellow */
			entry_cos.item_table[Idx].COLOR = ((Idx & 0x01) == 0 ) ? 0 : 1;
		}
		entry_cos.type = MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP;
		if(MEA_API_Create_FlowCoSMappingProfile_Entry(MEA_UNIT_0,&entry_cos,&cosMappingId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwInit ERROR: failed to call MEA_API_Create_FlowCoSMappingProfile_Entry\n");
			return ENET_FAILURE;
		}

		adap_SetDefaultCosMappingId(cosMappingId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," EnetHal_QoSHwInit: Default CosMappingProfile created! ProfileId:%d\n", cosMappingId);
	}

	return ENET_SUCCESS;
}

static void adap_UpdatePolicerParameters(tMeterDb *pMeter, MEA_Policer_Entry_dbt* PolicerEntry)
{
	// CIR:256000 CBS:64 EIR:500000 EBS:94
	//CIR:256000000 CBS:64 EIR:244000000 EBS:0	//
    /* Check Meter parameters */
    if(pMeter->issMeter.u4QoSCIR == 0)
        PolicerEntry->CBS = 0;
    else //CIR non null
    {
        if(pMeter->issMeter.u4QoSCBS == 0)
        {
            PolicerEntry->CBS = 32*1000;
        }
        else
           PolicerEntry->CBS = pMeter->issMeter.u4QoSCBS;
    }
    /* Aricent specifies CIR in Kbits/s. Convert it to Bits/s as Ethernity expects */
    PolicerEntry->CIR = (pMeter->issMeter.u4QoSCIR) * 1000;

    if(pMeter->issMeter.u4QoSEIR == 0)
	{
        PolicerEntry->EBS = 0;
	}
    else //EIR non null
    {
        if(pMeter->issMeter.u4QoSEBS == 0)
        {
            PolicerEntry->EBS = 32*1000;
        }
        else   /* Both EIR and EBS have non-null values.
                * Aricent specifies EBS in Kbits. Convert it to Bits as Ethernity expects */
           PolicerEntry->EBS = (pMeter->issMeter.u4QoSEBS);
    }

    /*    Per Aricent, EIR is the total of both Green and Yellow traffic.
     *    Per Ethernity, EIR is the Yellow traffic only,
     * so EthernityEIR = AricentEIR-AricentCIR.
     * The invariant: AricentEIR>=AricentCIR is enforced by the Aricent CLI.
     * Aricent specifies EIR in Kbits/s. Convert it to Bits/s as Ethernity expects */
     if ( (pMeter->issMeter.u4QoSEIR!=0) && (pMeter->issMeter.u4QoSEIR >= pMeter->issMeter.u4QoSCIR))
        PolicerEntry->EIR = (pMeter->issMeter.u4QoSEIR-pMeter->issMeter.u4QoSCIR) * 1000;

    if( (pMeter->issMeter.u4QoSEIR!=0) && (pMeter->issMeter.u4QoSEIR == pMeter->issMeter.u4QoSCIR) )
        PolicerEntry->EBS= 0;

    /* Set the Color aware flag as spcified */
    //PolicerEntry->color_aware = pMeter->issMeter.u1QoSMeterColorMode==ADAP_QOS_METER_COLOR_AWARE ? 1 : 0;
    PolicerEntry->color_aware = 1;
    PolicerEntry->comp        = 20;

    return;
}

static ADAP_Uint32 MeaDrvCreateEnetPolicer(ADAP_Uint16 MeterIdx)
{
	tMeterDb            	*meter = NULL;
    MEA_Policer_Entry_dbt   PolicerEntry;
    MEA_Uint16              policer_id;
    MEA_AcmMode_t           acm_mode;

    meter = MeaAdapGetMeterByIndex(MeterIdx);
    if(meter == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get Meter for Index: %d!!!\n", MeterIdx);
        return ENET_FAILURE;
    }

    /* Set the Policer configuration object */
    adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));

    adap_UpdatePolicerParameters(meter, &PolicerEntry);

    acm_mode = 0;
    policer_id = 0/*MEA_PLAT_GENERATE_NEW_ID*/;

    


    if(meter->policer_prof_id == MEA_PLAT_GENERATE_NEW_ID || meter->policer_prof_id == 0  )
    {
    	if(MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
                                          &policer_id,
                                          acm_mode,
                                          MEA_INGRESS_PORT_PROTO_TRANS,
                                          &PolicerEntry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateEnetPolicer: MEA_API_Create_Policer_ACM_Profile FAIL\n");
			return ENET_FAILURE;
		}
    } else {
    	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," set Policer : MEA_API_Set_Policer_ACM_Profile \n");

    	policer_id = meter->policer_prof_id; 

        if( MEA_API_Set_Policer_ACM_Profile(MEA_UNIT_0,
    			                    meter->policer_prof_id,
				            acm_mode,
                                            MEA_INGRESS_PORT_PROTO_TRANS,
		                             &PolicerEntry ) != MEA_OK)
	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvCreateEnetPolicer: MEA_API_Set_Policer_ACM_Profile FAIL\n");
    					return ENET_FAILURE;


    	}

    }

 


    /* Store the ENET Policer ID in the database */
    meter->policer_prof_id = policer_id;
	return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvGetVLANPolicingParams (tEnetHal_QoSClassMapEntry  ClassMap,
											MEA_Uint16*             VlanId,
											MEA_Uint32*             ifIdx,
											MEA_Uint8*             priority,
											MEA_Uint8*             RegenPriority,
											MEA_IngressPort_Ip_pri_type_t* priority_type)
{
    if(ClassMap.pPriorityMapPtr==NULL || ClassMap.pPriorityMapPtr->u1PriorityMapStatus==0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvGetVLANPolicingParams ERROR: Invalid ClassMap!!\n");
        return ENET_FAILURE;
    }

    *VlanId = ClassMap.pPriorityMapPtr->u2VlanId;
    *ifIdx  = ClassMap.pPriorityMapPtr->u4IfIndex;
    *priority = ClassMap.pPriorityMapPtr->u1InPriority;
    *RegenPriority = ClassMap.pPriorityMapPtr->u1RegenPri;

    switch(ClassMap.pPriorityMapPtr->u1InPriType)
    {
        case ADAP_QOS_IN_PRI_TYPE_VLAN_PRI ://0
        	*priority_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP;
        break;
        case ADAP_QOS_IN_PRI_TYPE_IP_TOS   ://1
        	*priority_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS;
        break;
        case ADAP_QOS_IN_PRI_TYPE_IP_DSCP  ://2
        	*priority_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP;
        break;
        case ADAP_QOS_IN_PRI_TYPE_MPLS_EXP ://3
        case ADAP_QOS_IN_PRI_TYPE_VLAN_DEI ://4
        case ADAP_QOS_IN_PRI_TYPE_VLAN_ID  ://5
        case ADAP_QOS_IN_PRI_TYPE_SRC_MAC  ://6
        case ADAP_QOS_IN_PRI_TYPE_DEST_MAC ://7
        default:
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvGetVLANPolicingParams ERROR: Unrecognized u1InPriType:%d\n",
                ClassMap.pPriorityMapPtr->u1InPriType);
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

static ADAP_Int32   MeaDrvSetServicePriorityParams(ADAP_Uint16 Service, tMeterDb *pMeter, ADAP_Uint16 MeterIdx, MEA_Policer_Entry_dbt *pPolicerEntry)
{
    MEA_Service_Entry_Data_dbt             Service_data;
    MEA_OutPorts_Entry_dbt                 Service_outPorts;
    MEA_Policer_Entry_dbt                  Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Service_editing;
    MEA_Service_Entry_Key_dbt      			key;

    if (pMeter == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetServicePriorityParams Error: NULL meter, service:%d\n", Service);
		return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetServicePriorityParams : service:%d MeterId:%d\n", Service, pMeter->issMeter.i4QoSMeterId);

    adap_memset(&key,       0 , sizeof(key));
    adap_memset(&Service_data,      0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service,
							&key,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetServicePriorityParams Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service);
		return ENET_FAILURE;
	}

	Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * (Service_editing.num_of_entries+1));

	if (Service_editing.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
		return ENET_FAILURE;
	}

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service,
							NULL,
							&Service_data,
							&Service_outPorts, /* outPorts */
							&Service_policer, /* policer  */
							&Service_editing)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",Service);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	Service_data.tmId = pMeter->tm_Id;
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL;

	Service_data.policer_prof_id = pMeter->policer_prof_id;
	Service_data.editId = 0;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetServicePriorityParams Meter PolicingProfileId:%d\n", Service_data.policer_prof_id);

    if(pPolicerEntry != NULL)
    {
        adap_memcpy(&Service_policer, pPolicerEntry, sizeof(MEA_Policer_Entry_dbt) );
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetServicePriorityParams service:%d pmId:%d tmId:%d\n",  Service, Service_data.pmId, Service_data.tmId);

	if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
							Service,
							&Service_data,
							&Service_outPorts,
							&Service_policer,
							&Service_editing) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetServicePriorityParams Error: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service);
		MEA_OS_free(Service_editing.ehp_info);
		return ENET_FAILURE;
	}

	if(pMeter != NULL)
	{
		if(pMeter->tm_Id == ENET_PLAT_GENERATE_NEW_ID)
			pMeter->tm_Id = Service_data.tmId;
		if(MeaAdapSetPmIdByMeter(pMeter, Service_data.pmId, Service) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s FAIL: Unable to set Pm %d in database for MeterId:%d\n", Service_data.pmId, pMeter->issMeter.i4QoSMeterId);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetServicePriorityParams set tmId:%d pmId:%d on meterIndex:%d Service_data.policer_prof_id:%d\n",Service_data.tmId,Service_data.pmId,MeterIdx, Service_data.policer_prof_id);

	}

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetServicePriorityParams service:%d pmId:%d tmId:%d\n",  Service, Service_data.pmId, Service_data.tmId);
    MEA_OS_free(Service_editing.ehp_info);

	return ENET_SUCCESS;

}

static MEA_Uint32 MeaDrvSetVLANPolicy (tPolicyDb  *pPolicy, MEA_Uint8 trafficType)
{
	tQueueDb                            	*pQueue;      /* ISS Queue to which forward the classified traffic */
    tClassifierDb	    					*pClassifier;
    tMeterDb                            	*pMeter;
    MEA_Uint16                                   Service = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Service_t                                ServiceClone;

    MEA_Uint16                                   VlanId;     /* Classifier' VlanId*/
    MEA_Uint32                                   ifIdx;       /* Classifier' Ingress interface */

    MEA_Uint8                                   Priority;
    MEA_Uint8                                   RegenPriority;
    MEA_IngressPort_Ip_pri_type_t           priority_type;

    MEA_Uint16                                   EnetPolilcingProfile;
    ENET_QueueId_t                          QueueId = ADAP_END_OF_TABLE;        /* ENET Queue ID to which forward the classified traffic */
    MEA_Uint16                                   EnetPriQueueIdx = ADAP_END_OF_TABLE;/* Priority queue inside the ENET Queue to which forward the classified traffic */
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
    MEA_Bool                                valid;
    MEA_Action_t                            Action_id;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;
    MEA_Service_Entry_Key_dbt               Service_key;
     MEA_Port_t                              EnetPortId;
    MEA_Uint32                                     i;
    MEA_Bool                                QueueId_found=MEA_FALSE;
    MEA_EditingMappingProfile_Entry_dbt 	mappingEntry;
    MEA_EditingMappingProfile_Id_t      	mappingId=ENET_PLAT_GENERATE_NEW_ID;
    MEA_Uint32									idx=0;
    MEA_Uint32									QueueIdx;
	MEA_Uint32								    issMasterPort=0;
	MEA_Uint16                       			TmIdToUpdate = ADAP_END_OF_TABLE;
	MEA_Uint16                       			PmIdToUpdate = ADAP_END_OF_TABLE;
	MEA_Uint16									servicePriority=ADAP_END_OF_TABLE;
	MEA_Bool								exist=MEA_FALSE;
	MEA_Bool								PriorityServiceNotCreated = MEA_FALSE;
	ADAP_Uint32								i4DbDefPriority=0;
	tServiceDb 								*pServiceId=NULL;
	ADAP_Uint32 							modeType=0;
	tServiceDb							    *pServiceAlloc=NULL;

    pClassifier = adap_getClassifierEntryByIndex(pPolicy->ClassifierIdx);
    if(pClassifier==NULL)
    {
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: Invalid Policy->ClassifierIdx:%d\n", pPolicy->ClassifierIdx);
       return ENET_FAILURE;
    }

    if(MeaDrvGetVLANPolicingParams (pClassifier->issClassifier, &VlanId, &ifIdx, &Priority, &RegenPriority, &priority_type)
       != ENET_SUCCESS)
    {
       return ENET_SUCCESS;
    }

    if(ifIdx == 0)
    {
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: for ifIndex %d no need to set classify\n", ifIdx);
       return ENET_SUCCESS;
    }

    if(trafficType == D_UNTAGGED_SERVICE_TAG)
    {
    	if(VlanId == 0)
    	{
    		if(AdapGetUserPriority(ifIdx, &i4DbDefPriority) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy ERROR: AdapGetUserPriority for u4IfIndex %d failed\n", ifIdx);
				return ENET_FAILURE;
			}
    		if(i4DbDefPriority == Priority)
			{
				VlanId = getPortToPvid(ifIdx);
			}
    	}
    	else if(getPortToPvid(ifIdx) != VlanId)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ifIndex %d pvid:%d\n", ifIdx,getPortToPvid(ifIdx));
    		return ENET_SUCCESS;

    	}
    	else
    	{
    		pServiceId =MeaAdapGetServiceIdByPortTrafType(0,ifIdx,1);
    		servicePriority = (pServiceId) ? pServiceId->serviceId : ADAP_END_OF_TABLE;
    		if(servicePriority == UNDEFINED_VALUE16)
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: priority vlan service not exist\r\n");
    		}
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: servicePriority:%d IssInPort:%d\n", servicePriority, ifIdx);
    	}
    }

    /* Find the Service which fits the Class */
    pServiceId = MeaAdapGetServiceIdByPortPriority(VlanId, ifIdx, trafficType, Priority, priority_type);

    // if we are customerEdgePort then need to get VLAN of provider edge
	if(pServiceId != NULL)
	{
		Service = pServiceId->serviceId;
		AdapGetProviderCoreBridgeMode(&modeType,ifIdx);
		if( (Service == MEA_PLAT_GENERATE_NEW_ID) && (modeType == ADAP_CUSTOMER_EDGE_PORT) )
		{
			VlanId = adap_GetPBPortServVlanCustomerVlan(ifIdx);
	        if(VlanId == ADAP_END_OF_TABLE)
	        {
	            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy FAIL: Unable to get provider vlan for port:%d \n", ifIdx);
	            return ENET_FAILURE;
	        }

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy adap_GetPBPortServVlanCustomerVlan vlanId:%d\n", VlanId);
			pServiceId = MeaAdapGetServiceIdByPortPriority(VlanId, ifIdx, D_PROVIDER_SERVICE_TAG, Priority, priority_type);
			Service = pServiceId->serviceId;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"QoSGetVLANPolicy DPLGetSvlanFromCustomerVlan serviceId:%d\n", Service);
		}
    }

     /* Get the target EnetQueue and QueueId */
	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy -  QueueIdx:%d\r\n",pPolicy->QueueIdx);
    if(pPolicy->QueueIdx != ADAP_END_OF_TABLE)
    {
        pQueue = adap_getQueueEntryByIndex(pPolicy->QueueIdx);
        if(pQueue != NULL)
        {
            QueueId = pQueue->ClusterId;
            EnetPriQueueIdx = pQueue->PriQueueIdx;
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: QueueIdx:%d ISS queue ID:%d ENET QueueId:%d, ENET PriQueueId:%d\n",
            		pPolicy->QueueIdx, pQueue->issQueue.i4QosQId, QueueId, EnetPriQueueIdx);
        }
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: No Queue Idx found in the Policy!\n");
        //If no queue found for policy - try to find it according to the classifier
        for(QueueIdx=0; QueueIdx<MAX_NUM_OF_QUEUES; QueueIdx++)
        {
            pQueue = adap_getQueueEntryByIndex(QueueIdx);
            if (pQueue == NULL || pQueue->ClassIdx == ADAP_END_OF_TABLE)
                 continue;

            if(pQueue->ClassIdx == pPolicy->ClassifierIdx)
            {
            	EnetPriQueueIdx = pQueue->PriQueueIdx;
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: ENET PriQueueId:%d\n", EnetPriQueueIdx);
            	break;
            }
        }
    }

    pMeter = MeaAdapGetMeterByIndex(pPolicy->MeterIdx);
	if(pMeter != NULL)
	{
		MeaDrvSetIngressColorAwareInterfaceType (ifIdx,1);
		if(pMeter->issMeter.u1QoSMeterColorMode == ENET_QOS_METER_COLOR_AWARE)
		{
		    issMasterPort = ifIdx;
			if(adap_IsPortChannel(issMasterPort) == ENET_SUCCESS)
			{
				if(adap_NumOfLagPortFromAggrId(issMasterPort) == 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"MeaDrvSetVLANPolicy lag configured without phy ports :%d \n",issMasterPort);
					return ENET_SUCCESS;
				}
				issMasterPort = adap_GetLagMasterPort(issMasterPort);
				MeaDrvSetIngressColorAwareInterfaceType (issMasterPort,1);
			}
		}
		if(pPolicy->issInProActEntryValid == ADAP_TRUE)
		{
			if(pMeter->mappingEditingIdx == MEA_PLAT_GENERATE_NEW_ID)
			{
				for(idx=0;idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;idx++)
				{
					mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=0;
					mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=0;
					mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=0;
					mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=0;
				}

				if(pPolicy->u1CurrFlag != 0)
				{
					// create mapping
					if(MEA_API_Create_EditingMappingProfile_Entry(MEA_UNIT_0,&mappingEntry,&mappingId) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error:failed to MEA_API_Create_EditingMappingProfile_Entry \n");
						return ENET_FAILURE;
					}
					pMeter->mappingEditingIdx = adap_SetEditMapping(mappingId);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetVLANPolicy: editMapIdx:%d",pMeter->mappingEditingIdx);
				}
			}
			else
			{
				mappingId = adap_getEditMappingByIdx(pMeter->mappingEditingIdx);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvSetVLANPolicy: mappingId:%d editMapIdx:%d",mappingId, pMeter->mappingEditingIdx);
				if(MEA_API_Get_EditingMappingProfile_Entry(MEA_UNIT_0,mappingId,&mappingEntry) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error:failed to get MEA_API_Get_EditingMappingProfile_Entry:%d\n",mappingId);
					return ENET_FAILURE;
				}
			}
			switch(pPolicy->u1CurrFlag)
			{
				case ENET_QOS_UPDATE_PLY_ACT_CONF_VLAN_PRI: // green
					for(idx=0;idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;idx++)
					{
						mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=pPolicy->issInProActEntry.QoSInProfileActionVlanPrio;
					}
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileActionVlanPrio:%d",pPolicy->issInProActEntry.QoSInProfileActionVlanPrio);
					break;
				case ENET_QOS_UPDATE_PLY_ACT_CONF_VLAN_DE: // green
					for(idx=0;idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;idx++)
					{
						mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=pPolicy->issInProActEntry.QoSInProfileActionVlanDE;
					}

					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileActionVlanDE:%d",pPolicy->issInProActEntry.QoSInProfileActionVlanDE);
					break;
				case ENET_QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_PRI:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileActionInnerVlanPrio:%d",pPolicy->issInProActEntry.QoSInProfileActionInnerVlanPrio);
					break;
				case ENET_QOS_UPDATE_PLY_ACT_CONF_MPLS_EXP:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileActionDscp:%d",pPolicy->issInProActEntry.QoSInProfileActionDscp);
					break;
				case ENET_QOS_UPDATE_PLY_ACT_EXC_VLAN_DE: //yellow
					for(idx=0;idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;idx++)
					{
						mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=pPolicy->issInProActEntry.QoSInProfileExceedActionVlanDE;
					}

					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileExceedActionVlanDE:%d",pPolicy->issInProActEntry.QoSInProfileExceedActionVlanDE);
					break;
				case ENET_QOS_UPDATE_PLY_ACT_EXC_VLAN_PRI: //yellow
					for(idx=0;idx<MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY;idx++)
					{
						mappingEntry.item_table[idx].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=pPolicy->issInProActEntry.QoSInProfileExceedActionVlanPrio;
					}
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QoSInProfileExceedActionVlanPrio:%d",pPolicy->issInProActEntry.QoSInProfileExceedActionVlanPrio);
					break;
				default:
					break;
			}

			if(pPolicy->u1CurrFlag != 0)
			{
				if(MEA_API_Set_EditingMappingProfile_Entry(MEA_UNIT_0,mappingId,&mappingEntry) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error:failed to get MEA_API_Set_EditingMappingProfile_Entry:%d\n",mappingId);
					return ENET_FAILURE;
				}
			}
		}
    }

    if(Service == MEA_PLAT_GENERATE_NEW_ID)
    { // No service for this specified classifier found - New service should be created with the given priority

    	pServiceId = MeaAdapGetSimpleServiceIdByPort(VlanId, ifIdx, trafficType);
    	if(pServiceId != NULL)
    	{
    		Service = pServiceId->serviceId;
			if(Service == MEA_PLAT_GENERATE_NEW_ID)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: No Tagged Service for ISS Port %d, VLAN:%d\n",
					   ifIdx, VlanId);
				return ENET_FAILURE;
			}
    	}

    	/** Apply the obtained parameters to the specified Service */
        adap_memset(&Service_key,       0 , sizeof(Service_key));
        adap_memset(&Service_data,      0 , sizeof(Service_data ));
        adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
        adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
        adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

        /* check if the service exist */
        if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service,&valid) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service);
            return ENET_FAILURE;
        }
        if(valid == MEA_FALSE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ServiceId %d does not exists\n", Service);
            return ENET_FAILURE;
        }

        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: Cloning the Service:%d (VLAN:%d, ISS Port %d)\n",
                Service,VlanId, ifIdx);
        if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                Service,
                                &Service_key,
                                &Service_data,
                                &Service_outPorts,
                                &Service_policer,
                                &Service_editing) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service);
            return ENET_FAILURE;
        }
        Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

        if (Service_editing.ehp_info == NULL )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy ERROR: FAIL to malloc ehp_info\n");
            return ENET_FAILURE;
        }

        if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                Service,
                                &Service_key,
                                &Service_data,
                                &Service_outPorts, /* outPorts */
                                &Service_policer, /* policer  */
                                &Service_editing) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service);
            MEA_OS_free(Service_editing.ehp_info);
            return ENET_FAILURE;
        }

 		if(MeaDrvGetPolicerMeterParams(pPolicy->MeterIdx, ifIdx, Service_data.pmId, &EnetPolilcingProfile, &TmIdToUpdate, &PmIdToUpdate, &Service_policer)!= ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy ERROR: Unable to get PolicerMeterParams for service:%d\n",  Service);
			return ENET_FAILURE;
		}

        Service_data.policer_prof_id = EnetPolilcingProfile;
        Service_data.tmId = TmIdToUpdate;
		Service_data.pmId = PmIdToUpdate;

        Service_data.DSE_learning_enable=1;
        Service_data.DSE_learning_key_type=MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        Service_data.DSE_learning_actionId_valid=1;

        /* Is the target ISS Queue defined ?*/
        if(EnetPriQueueIdx != ADAP_END_OF_TABLE)
        {
            /* Set the target ISS Queue (== ENET cluster + priority queue) */
            Service_data.DSE_learning_srcPort_force=1;
           /* Service_data.DSE_learning_srcPort=QueueId;   */
            Service_data.DSE_forwarding_enable = 0;
            Service_data.COS_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
            Service_data.COS       = EnetPriQueueIdx;
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: Service data ENET PriQueueId:%d\n", Service_data.COS);

        }
        /* Set the VLAN priority regeneration */
        Service_data.flowMarkingMappingProfile_force = 0;
		Service_data.flowCoSMappingProfile_force = MEA_FALSE;
		if(RegenPriority != ADAP_END_OF_TABLE)
		{
			Service_data.L2_PRI_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.L2_PRI       = RegenPriority;
		}
		if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP)
		{
			MeaDrvSetIngressPortPriRule(3 ,ifIdx);
			MeaDrvSetIngressPortMode(3 , ifIdx);
			MeaDrvSetIngressPortProtocol(1,1 ,ifIdx);
		}
		else if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS)
		{
			MeaDrvSetIngressPortPriRule(3 ,ifIdx);
			MeaDrvSetIngressPortMode(2 , ifIdx);
			MeaDrvSetIngressPortProtocol(1,1 ,ifIdx);
		}
		else
		{
			MeaDrvSetIngressPortPriRule(0 ,ifIdx);
			MeaDrvSetIngressPortMode(0 , ifIdx);
			MeaDrvSetIngressPortProtocol(1,0 ,ifIdx);
		}

        /* Set the upstream service key */
		if(MeaAdapGetPhyPortFromLogPort(ifIdx, &EnetPortId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",ifIdx);
			return ENET_FAILURE;
		}

        Service_key.priType = priority_type;
        Service_key.pri = Priority;

        Service_key.evc_enable = MEA_FALSE;
        Service_key.external_internal = MEA_FALSE;

        /* Set the Target Priority Queue using the Reverse action(learning action) per Service */
        Action_id = Service_data.DSE_learning_actionId;
        adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
        adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
        adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
        Action_editing.num_of_entries = 1;
        Action_editing.ehp_info = &Action_editing_info;

        if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                            Action_id,
                            &Action_data,
                            &Action_outPorts,
                            &Action_policer,
                            &Action_editing)!= MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n",Action_id);
            MEA_OS_free(Service_editing.ehp_info);
            return ENET_FAILURE;
        }

        /* Create the reverse action */
        Action_id = MEA_PLAT_GENERATE_NEW_ID;
        Action_data.pm_id_valid = MEA_FALSE;


        if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
                                   &Action_data,
                                   &Action_outPorts,
                                   &Action_policer,
                                   &Action_editing,
                                   &Action_id) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
            MEA_OS_free(Service_editing.ehp_info);
            return ENET_FAILURE;
        }

        /*
                1) out queue
                2) editing
            MEA_SET_OUTPORT

       */
        if(QueueId!=ADAP_END_OF_TABLE)
		{
			for(i=0; i < Service_editing.num_of_entries;i++){

				if(MEA_IS_SET_OUTPORT(&Service_editing.ehp_info[i].output_info,QueueId)!=MEA_FALSE)
					  break;
			}
			if(i >= Service_editing.num_of_entries){
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: the QueueId not found %d \n", QueueId );
				return ENET_FAILURE;
			}
			MEA_CLEAR_ALL_OUTPORT(&Service_editing.ehp_info[i].output_info);
			MEA_SET_OUTPORT(&Service_editing.ehp_info[i].output_info,QueueId);
			if(i > 0)
			{
				adap_memcpy(&Service_editing.ehp_info[0],&Service_editing.ehp_info[i],sizeof(MEA_EHP_Info_dbt));
			}
			Service_editing.num_of_entries=1;
			MEA_CLEAR_ALL_OUTPORT(&Service_outPorts);
			MEA_SET_OUTPORT(&Service_outPorts,QueueId);
			Service_data.editId =0; /*create new Id*/
        }

		if( (pPolicy->issInProActEntryValid == ADAP_TRUE) && (mappingId != ENET_PLAT_GENERATE_NEW_ID) )
		{
			Service_data.editId =0; /*create new Id*/
			for(i=0; i < Service_editing.num_of_entries;i++)
			{

				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_color = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_priority = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_info.mapping_enable = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_info.mapping_id = mappingId;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
				if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP)
				{
					// dscp regen priority
					Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_enable=MEA_TRUE;
					Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_value=RegenPriority;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy DSCP set regen priority:%d\n",  RegenPriority);
				}
				else if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS)
				{
					// dscp regen priority
					Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_enable=MEA_TRUE;
					Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_value=(RegenPriority << 2);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy TOP set regen priority:%d\n",  RegenPriority);
				}
			}
		}
		else if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy DSCP priority type\n");
			Service_data.editId =0; /*create new Id*/
			for(i=0; i < Service_editing.num_of_entries;i++)
			{
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.valid = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_enable=MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_value=RegenPriority;
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_color = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_priority = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
				Service_editing.ehp_info[i].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
				Service_editing.ehp_info[i].ehp_data.eth_info.val.vlan.vid      = VlanId;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy DSCP set regen priority:%d\n",  RegenPriority);
			}
		}
		else if(priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"QoSGetVLANPolicy TOS priority type\n");
			Service_data.editId =0; /*create new Id*/
			for(i=0; i < Service_editing.num_of_entries;i++)
			{
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.valid = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_enable=MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.LmCounterId_info.Command_dscp_stamp_value=(RegenPriority << 2);
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_color = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_priority = MEA_FALSE;
				Service_editing.ehp_info[i].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
				Service_editing.ehp_info[i].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
				Service_editing.ehp_info[i].ehp_data.eth_info.val.vlan.vid      = VlanId;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy TOS set regen priority:%d\n",  RegenPriority);
			}
		}

		Service_data.CM=1;
		Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_DISABLE;

		// if request to force to green
		if(pClassifier->issClassifier.PreColor == ENET_QOS_CLS_COLOR_GREEN)
		{
			Service_data.CM=1;
			Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.COLOR= MEA_POLICER_COLOR_VAL_GREEN;
		}
		// if request to force to yellow
		else if(pClassifier->issClassifier.PreColor == ENET_QOS_CLS_COLOR_YELLOW)
		{
			Service_data.CM=1;
			Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.COLOR= MEA_POLICER_COLOR_VAL_YELLOW;
		}

        Service_data.DSE_learning_actionId   = Action_id;
		ServiceClone = MEA_PLAT_GENERATE_NEW_ID;

		Service_data.flowMarkingMappingProfile_force=MEA_FALSE;
		Service_data.flowCoSMappingProfile_force = MEA_FALSE;

		if((priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP) || (priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS))
		{
			Service_data.DSE_learning_actionId_valid = MEA_FALSE;

			if(Service_editing.num_of_entries == 1)
			{
				if( (Service_editing.ehp_info->ehp_data.eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP) && (Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid == VlanId) )
				{
					Service_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
				}
			}
		}

		MEA_Service_Entry_Key_dbt mySRV;
		Service_key.priType = priority_type;
		Service_key.pri = Priority;

		adap_memset(&mySRV,0,sizeof(mySRV));

		mySRV.net_tag = Service_key.net_tag;
		mySRV.L2_protocol_type = Service_key.L2_protocol_type;
		mySRV.pri=Service_key.pri;
		mySRV.priType = Service_key.priType;

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Service_key - net_tag %d l2Type %d priType:%d priority:%d\n",mySRV.net_tag,mySRV.L2_protocol_type,mySRV.priType, mySRV.pri);
		if(ENET_ADAPTOR_IsExist_Service_ByKey(MEA_UNIT_0,&mySRV,&exist,&ServiceClone) == MEA_OK)
		{
			if(exist == MEA_TRUE)
			{
				MeaDeleteEnetServiceId(ServiceClone);
			}
		}

		if( (Service_key.L2_protocol_type == MEA_PARSING_L2_KEY_Untagged) && (Priority != -1) )
		{
			PriorityServiceNotCreated = MEA_TRUE;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy Untagged traffic - no need to create priority service\n");
		}
		else
		{
			pServiceAlloc = MeaAdapAllocateServiceId();

			if(pServiceAlloc == NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
				return MEA_ERROR;
			}

			pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

			/* Create new service for the given Vlan */
			if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
									   &Service_key,
									   &Service_data,
									   &Service_outPorts,
									   &Service_policer,
									   &Service_editing,
									   &pServiceAlloc->serviceId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: Unable to create the new service %d \n",  pServiceAlloc->serviceId);
				ENET_ADAPTOR_Delete_Action (MEA_UNIT_0, Action_id);
				MEA_OS_free(Service_editing.ehp_info);
				return ENET_FAILURE;
			}
		// save the policer id
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: New service for Vlan %d is created with ServiceID:%d priority:%d\n", VlanId, pServiceAlloc->serviceId, Service_key.pri);

			//Update PM and TM for service
			if(pMeter != NULL)
			{
				if(pMeter->tm_Id == ENET_PLAT_GENERATE_NEW_ID)
					pMeter->tm_Id = Service_data.tmId;
			}

			// save configuration
			pServiceAlloc->ifIndex = ifIdx;
			pServiceAlloc->inPort = EnetPortId;
			pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
			pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
			pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
			pServiceAlloc->pmId = Service_data.pmId;
			pServiceAlloc->vpnId = Service_data.vpn;

			//Update service priority
			pServiceAlloc->enet_IncPriority = Service_key.pri;
			pServiceAlloc->enet_priority_type = Service_key.priType;
		}
    }
    else //Update an existing service
    {
         /** Apply the obtained parameters to the specified Service */
        adap_memset(&Service_data  , 0 , sizeof(Service_data ));
        adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
        adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
        adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

        /* check if the service exist */
        if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service,&valid) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service);
            return ENET_FAILURE;
        }
        if(valid == MEA_FALSE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ServiceId %d does not exists\n", Service);
            return ENET_FAILURE;
        }

        if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                Service,
                                NULL,
                                &Service_data,
                                &Service_outPorts,
                                &Service_policer,
                                &Service_editing) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service);
            return ENET_FAILURE;
        }


        Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);
        if (Service_editing.ehp_info == NULL )
        {
        	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy ERROR: FAIL to malloc ehp_info\n");
            return ENET_FAILURE;
        }


        if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                        Service,
                                        NULL,
                                        &Service_data,
                                        &Service_outPorts,
                                        &Service_policer,
                                        &Service_editing) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service);
                    return ENET_FAILURE;
                }

        if (Service_editing.ehp_info == NULL )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error:  FAIL to malloc ehp_info\n");
            return ENET_FAILURE;
        }

		if(MeaDrvGetPolicerMeterParams(pPolicy->MeterIdx, ifIdx, Service_data.pmId, &EnetPolilcingProfile, &TmIdToUpdate, &PmIdToUpdate, &Service_policer)!= ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy ERROR: Unable to get PolicerMeterParams for service:%d\n",  Service);
			return ENET_FAILURE;
		}

        Service_data.policer_prof_id = EnetPolilcingProfile;
        Service_data.tmId = TmIdToUpdate;
        Service_data.pmId = PmIdToUpdate;

        Service_data.DSE_learning_enable=1;
        Service_data.DSE_learning_key_type=MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        Service_data.DSE_learning_actionId_valid=1;

        /* Is the target ISS Queue defined ?*/
        if(EnetPriQueueIdx != ADAP_END_OF_TABLE)
        {
            /* Set the target ISS Queue (== ENET cluster + priority queue) */
            Service_data.DSE_learning_srcPort_force=1;
            Service_data.DSE_forwarding_enable = 0;
            Service_data.COS_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
            Service_data.COS       = EnetPriQueueIdx;
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy: Service data ENET PriQueueId:%d\n", Service_data.COS);
            if(Service_data.COS_FORCE == MEA_ACTION_FORCE_COMMAND_VALUE){
            	Service_data.flowCoSMappingProfile_force=MEA_FALSE;
            	Service_data.flowCoSMappingProfile_id=0;
            	Service_data.flowMarkingMappingProfile_force=MEA_FALSE;
				Service_data.flowCoSMappingProfile_force = MEA_FALSE;
            	Service_data.flowMarkingMappingProfile_id=0;

            }

        }
        /* Set the VLAN priority regeneration */
        Service_data.flowMarkingMappingProfile_force = 0;
		Service_data.flowCoSMappingProfile_force = MEA_FALSE;
		if(RegenPriority != ADAP_END_OF_TABLE)
		{
			Service_data.L2_PRI_FORCE = MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.L2_PRI       = RegenPriority ;
		}

        if(QueueId!=ADAP_END_OF_TABLE)
        {
        	QueueId_found=MEA_FALSE;
			for(i=0; i < Service_editing.num_of_entries;i++)
			{
				QueueId_found=MEA_FALSE;

				if(MEA_IS_SET_OUTPORT(&Service_editing.ehp_info[i].output_info,QueueId)!=MEA_FALSE){
					QueueId_found=MEA_TRUE;
					break;
				}
			}

			if(QueueId_found)
			{
				MEA_CLEAR_ALL_OUTPORT(&Service_editing.ehp_info[i].output_info);
				MEA_SET_OUTPORT(&Service_editing.ehp_info[i].output_info,QueueId);
				if(i > 0)
				{
					adap_memcpy(&Service_editing.ehp_info[0],&Service_editing.ehp_info[i],sizeof(MEA_EHP_Info_dbt));
				}
				Service_editing.num_of_entries=1;

				MEA_CLEAR_ALL_OUTPORT(&Service_outPorts);
				MEA_SET_OUTPORT(&Service_outPorts,QueueId);
				Service_data.editId =0; /*create new Id*/
			}
        }

		if( (pPolicy->issInProActEntryValid == ADAP_TRUE) && (mappingId != ENET_PLAT_GENERATE_NEW_ID) )
		{
			for(i=0; i < Service_editing.num_of_entries;i++)
			{
				if(Service_editing.ehp_info[i].ehp_data.eth_info.mapping_enable == MEA_FALSE)
				{
					Service_editing.ehp_info[i].ehp_data.eth_info.mapping_enable = MEA_TRUE;
					Service_editing.ehp_info[i].ehp_data.eth_info.mapping_id = mappingId;
				}
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_color = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_info.stamp_priority = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_valid = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_loc   = 12;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
				Service_editing.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			}
		}
		Service_data.CM=1;
		Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_DISABLE;

		// if request to force to green
		if(pClassifier->issClassifier.PreColor == ENET_QOS_CLS_COLOR_GREEN)
		{
			Service_data.CM=1;
			Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.COLOR= MEA_POLICER_COLOR_VAL_GREEN;
		}
		// if request to force to yellow
		else if(pClassifier->issClassifier.PreColor == ENET_QOS_CLS_COLOR_YELLOW)
		{
			Service_data.CM=1;
			Service_data.COLOR_FORSE=MEA_ACTION_FORCE_COMMAND_VALUE;
			Service_data.COLOR= MEA_POLICER_COLOR_VAL_YELLOW;
		}

		Service_data.flowMarkingMappingProfile_force = MEA_FALSE;
		Service_data.flowCoSMappingProfile_force = MEA_FALSE;
        Service_data.editId = 0; /*create new Id*/

		if((priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP) || (priority_type == MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS))
		{
			Service_data.DSE_learning_actionId_valid = MEA_FALSE;

			if(Service_editing.num_of_entries == 1)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy before command set: vlan:%d ehpVlan:%d command:%d\n", VlanId,Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,Service_editing.ehp_info->ehp_data.eth_info.command);

				if( (Service_editing.ehp_info->ehp_data.eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP) && (Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid == VlanId) )
				{
					Service_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy after command set: vlan:%d ehpVlan:%d command:%d\n", VlanId,Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,Service_editing.ehp_info->ehp_data.eth_info.command);
			}
		}

		if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
								Service,
								&Service_data,
								&Service_outPorts,
								&Service_policer,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVLANPolicy Error: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service);
			MEA_OS_free(Service_editing.ehp_info);
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVLANPolicy ENET_ADAPTOR_Set_Service: serviceId:%d priority:%d\n", Service, Priority);

    	//Update PM and TM for service
    	if(pMeter != NULL)
    	{
    		if(pMeter->tm_Id == ENET_PLAT_GENERATE_NEW_ID)
    			pMeter->tm_Id = Service_data.tmId;
    		if(MeaAdapSetPmIdByMeter(pMeter, Service_data.pmId, Service) != ENET_SUCCESS)
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s FAIL: Unable to set Pm %d for MeterIdx:%d\n", Service_data.pmId, pPolicy->MeterIdx);
    			return ENET_FAILURE;
    		}
    	}

		//Update service priority
		pServiceId->enet_IncPriority = Priority;
		pServiceId->enet_priority_type = priority_type;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Service:%d updated with priority:%d\n", Service, Priority);
    }

    MEA_OS_free(Service_editing.ehp_info);

	//Set Pm and Tm for Vlan 0 services with the same PVID
	if( (servicePriority != ADAP_END_OF_TABLE) && (pMeter != NULL) && (PriorityServiceNotCreated == MEA_FALSE))
	{
		if(MeaDrvSetServicePriorityParams(servicePriority, pMeter, pPolicy->MeterIdx, &Service_policer) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Unable to set parameters for service:%d\n", servicePriority);
			return ENET_FAILURE;
		}
	}
	return ENET_SUCCESS;
}


static ADAP_Uint32 MeaDrvUpdatePPPoESessPolicer(ADAP_Uint16 SessId, tPolicyDb *pPolicy)
{
    tMeterDb         *meterDb = NULL;
    ADAP_Uint16       vpn=0;

    if ((pPolicy != NULL) && (pPolicy->MeterIdx == ADAP_END_OF_TABLE))
        return ENET_SUCCESS;  // No Meter for this specified policy found

    meterDb = MeaAdapGetMeterByIndex(pPolicy->MeterIdx);
    if (meterDb == NULL)
    {
        return ENET_FAILURE;
    }

    if (MeaDrvCreateEnetPolicer(pPolicy->MeterIdx) != ENET_SUCCESS)
    {
        return ENET_FAILURE;
    }

    vpn = MeaAdapGetPPPSessionVpn (SessId);

    if (MeaDrvSetPolicerInPPPoEForwarder (SessId, vpn, meterDb->policer_prof_id) != MEA_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Unable to set on forwarder for policer: %d!!!\n",
               meterDb->policer_prof_id);
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}


static ADAP_Uint32 MeaDrvUpdateFilterPolicer(tClassifierDb *pClassifier, tPolicyDb *pPolicy,
		eAdap_ClassificationType *pClassificationType)
{
    if ((pPolicy != NULL) && (pPolicy->MeterIdx == ADAP_END_OF_TABLE))
        return ENET_SUCCESS;  // No Meter for this specified policy found

    if(pClassifier->issClassifier.pL2FilterPtr != NULL &&
            pClassifier->issClassifier.pL2FilterPtr->L2FilterStatus)
    { /* L2 fileds based classification */
        *pClassificationType = ENET_L2;
	//todo in 6.1 code is neither svlanid, not cvlanid flag was set - apply acl to untagged + vlans (0,2-4096)
	// this means that acl rule are applied only for untagged and for vlans, omitting vlanid 1. This seems to be wrong,
	// but should be noted in case of the specific issues. If change is still required - list of omitted vlanIds should
	// be added
	// Apply the modifications
        if(adap_HwProcessFilterHierarchy(ENETHAL_DELETE_ENET_FILTER, FILTER_TYPE_L2,
                             pClassifier->issClassifier.pL2FilterPtr, NULL, pPolicy) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                     "MeaDrvUpdateEnetPolicers ERROR: adap_HwProcessFilterHierarchy for L2 filter FAIL\n");
        }
        if(adap_HwProcessFilterHierarchy(ENETHAL_CREATE_ENET_FILTER, FILTER_TYPE_L2,
                             pClassifier->issClassifier.pL2FilterPtr, NULL, pPolicy) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                     "MeaDrvUpdateEnetPolicers ERROR: adap_HwProcessFilterHierarchy for L2 filter FAIL\n");
            return ENET_FAILURE;
        }
    }

    if(pClassifier->issClassifier.pL3FilterPtr != NULL &&
            pClassifier->issClassifier.pL3FilterPtr->L3FilterStatus)
    { /* L3 fileds based classification */
        *pClassificationType = ENET_L3;
	//todo in 6.1 code is neither svlanid, not cvlanid flag was set - apply acl to all vlans (2-4096)
	// this means that acl rule are applied only for vlans omitting default vlan 1. This seems to be wrong, but should
	// be noted in case of the specific issues. If change is still required - list of omitted vlanIds should be added.
	// Apply the modifications
	if(adap_HwProcessFilterHierarchy(ENETHAL_DELETE_ENET_FILTER, FILTER_TYPE_L3, NULL,
                         pClassifier->issClassifier.pL3FilterPtr, pPolicy) != ENET_SUCCESS)
	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                     "MeaDrvUpdateEnetPolicers ERROR: adap_HwProcessFilterHierarchy for L3 filter FAIL\n");
	}
	if(adap_HwProcessFilterHierarchy(ENETHAL_CREATE_ENET_FILTER, FILTER_TYPE_L3, NULL,
                         pClassifier->issClassifier.pL3FilterPtr, pPolicy) != ENET_SUCCESS)
	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                     "MeaDrvUpdateEnetPolicers ERROR: adap_HwProcessFilterHierarchy for L3 filter FAIL\n");
            return ENET_FAILURE;
	}

        /* TODO - Workaround for bug: QOS rate limiting stops working for downstream traffic if
        * multiple acl applied on same interface.*/
        MeaDrvUpdatePolicerForIp(pClassifier->issClassifier.pL3FilterPtr->FilterDstIpAddr, 
            pClassifier->issClassifier.pL3FilterPtr->FilterDstIpAddrMask);
    }

    return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvUpdateEnetPolicers(void)
{
	ADAP_Uint16         PolicyIdx;
	tPolicyDb       	*pPolicy;
    tClassifierDb	    *pClassifier;

	eAdap_ClassificationType ClassificationType = ENET_CLASSIFICATION_TYPE_MAX;

    /************ For all the Policy Maps */
 	/* Look for all Policers and update/create priority services according to the parameters */
 	for(PolicyIdx=1;PolicyIdx<MAX_NUM_OF_POLICERS;PolicyIdx++)
    {
 		pPolicy = adap_getPolicerEntryByIndex(PolicyIdx);
        if((pPolicy == NULL) || (pPolicy->valid == ADAP_FALSE))
        {
            continue;
        }
        if(pPolicy->ClassifierIdx == ADAP_END_OF_TABLE)
            continue;  // No Classifier for this specified policy found
        pClassifier=adap_getClassifierEntryByIndex(pPolicy->ClassifierIdx);
        if((pClassifier == NULL) || (pClassifier->valid == ADAP_FALSE))
            continue;

        if(pClassifier->issClassifier.pPriorityMapPtr != NULL &&
           pClassifier->issClassifier.pPriorityMapPtr->u1PriorityMapStatus)
        { /* VLAN Priority based classification */
			if(MeaDrvSetVLANPolicy(pPolicy, D_UNTAGGED_SERVICE_TAG) != ENET_SUCCESS)
			 {
				 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetPolicers ERROR: MeaDrvSetVLANPolicy FAIL for Policy:%d\n", pPolicy->issPolicy.QoSPolicyMapId);
				 return ENET_FAILURE;
			 }
			if(MeaDrvSetVLANPolicy(pPolicy, D_TAGGED_SERVICE_TAG) != ENET_SUCCESS)
			 {
				 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetPolicers ERROR: MeaDrvSetVLANPolicy FAIL for Policy:%d\n", pPolicy->issPolicy.QoSPolicyMapId);
				 return ENET_FAILURE;
			 }
            ClassificationType = ENET_VLAN;
        }

        if (MeaDrvUpdateFilterPolicer(pClassifier, pPolicy, &ClassificationType) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetPolicers ERROR: MeaDrvUpdateFilterPolicer FAIL for Policy:%d\n",
                    pPolicy->issPolicy.QoSPolicyMapId);
            return ENET_FAILURE;
        }

        if(ClassificationType==ENET_CLASSIFICATION_TYPE_MAX)
        { /* No Classification declared!!! */
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"QoSHWUpdateEthernityPolicers: No ClassificationType declared!!!\n");
            return ENET_FAILURE;
        }

        if (pClassifier->issClassifier.u2PPPSessId != 0)
        {
            if (MeaDrvUpdatePPPoESessPolicer(pClassifier->issClassifier.u2PPPSessId, pPolicy) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEnetPolicers ERROR: MeaDrvUpdatePPPoESessPolicer FAIL for Policy:%d\n",
                        pPolicy->issPolicy.QoSPolicyMapId);
                return ENET_FAILURE;
            }
        }
    }

    return ENET_SUCCESS;
}


static ADAP_Uint32 MeaDrvAddEthernityWredProfilesToService(MEA_Service_t ServiceId, ADAP_Uint16 WredProfileIdx)
{
    MEA_Bool                                valid;
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;

    adap_memset(&Service_data  , 0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

    /* Check if the service exist */
    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,ServiceId,&valid) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", ServiceId);
        return ENET_FAILURE;
    }
    if(valid == MEA_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService Error: ServiceId %d does not exists\n", ServiceId);
        return ENET_FAILURE;
    }

    /* Get the service */
    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            ServiceId,
                            NULL,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", ServiceId);
        return ENET_FAILURE;
    }
    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

    if (Service_editing.ehp_info == NULL )
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService: FAIL to malloc ehp_info\n");
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            ServiceId,
                            NULL,
                            &Service_data,
                            &Service_outPorts, /* outPorts */
                            &Service_policer, /* policer  */
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",ServiceId);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }

    /* Attach the WRED profile to the Service */
    Service_data.CM        = MEA_TRUE;
    Service_data.wred_profId = AdapGetWredProfileByIdx(WredProfileIdx); //todo implement
    Service_data.flowMarkingMappingProfile_force = MEA_FALSE;
    Service_data.flowCoSMappingProfile_force = MEA_FALSE;
    /* Update the service back to the HW */
    Service_data.editId = 0; /*create new Id*/
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAddEthernityWredProfilesToService - attach WRED profile%d to the serviceId:%d\n", Service_data.wred_profId, ServiceId);
    if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
                            ServiceId,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToService Error: ENET_ADAPTOR_Set_Service failed for service:%d\n", ServiceId);
        return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAddEthernityWredProfilesToService: Service:%d successfully updated with WRED Profile:%d \n",
            ServiceId, Service_data.wred_profId);
    return ENET_SUCCESS;
}


static ADAP_Uint32 MeaDrvAddEthernityWredProfilesToReverseAction(MEA_Action_t ActionId, ADAP_Uint16 WredProfileIdx)
{
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
    MEA_Action_Entry_Data_dbt               Action_data;

    adap_memset(&Service_data  , 0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAddEthernityWredProfilesToReverseAction: Action:%d\n",ActionId);

    Service_editing.ehp_info = NULL;

    if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                                ActionId,
                                &Action_data,
                                &Service_outPorts,
                                &Service_policer,
                                &Service_editing)!= MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToReverseAction Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n", ActionId);
        return ENET_FAILURE;
    }
    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);
    if (Service_editing.ehp_info == NULL )
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToReverseAction ERROR: FAIL to malloc ehp_info\n");
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                        ActionId,
                        &Action_data,
                        &Service_outPorts,
                        &Service_policer,
                        &Service_editing)!= MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToReverseAction Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n",Service_data.DSE_learning_actionId);
        return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAddEthernityWredProfilesToReverseAction num of entries:%d\n",Service_editing.num_of_entries);

   /* Attach the WRED profile to the Action */
    Action_data.wred_profId = AdapGetWredProfileByIdx(WredProfileIdx); //todo implement

    if(ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
                            ActionId,
                            &Action_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) !=    MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAddEthernityWredProfilesToReverseAction ERROR: FAIL to ENET_ADAPTOR_Set_Action\n");
        return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAddEthernityWredProfilesToReverseAction: Action:%d successfully updated with WRED Profile:%d \n",
            ActionId, Service_data.wred_profId);

    MEA_OS_free(Service_editing.ehp_info);
    return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvAttachWredProfileForService(MEA_Service_t ServiceId, tQueueDb *pQueue)
{
    MEA_Port_t                                    enetPort;
    ENET_QueueId_t                                clusterId;
    MEA_Service_Entry_Data_dbt                    Service_data;
    ADAP_Uint16                                   PortsCount;
    MEA_Bool                                      ClusterFound = MEA_FALSE;
    ADAP_Uint32                                   result;
    //NOTE: We have to get SEriveDb entry to get port.
    {
        tServiceDb *pServiceDb;
        pServiceDb = MeaAdapGetServiceId(ServiceId);

        if(!pServiceDb)
        {
            return ENET_SUCCESS;
        }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvAttachWredProfileForService: ServiceId: %d\n", ServiceId);

        //If source port is the same as a queue num then new wred should assign to the service
        result = MeaAdapGetPhyPortFromLogPort (pServiceDb->ifIndex, &enetPort);
        if (result != ENET_SUCCESS)
        {
            return result;
        }
    }

    result = adapGetFirstCluster(enetPort,&clusterId);
    while (result == ENET_SUCCESS)
    {
        if (clusterId == pQueue->ClusterId)
        {
            ClusterFound = MEA_TRUE;
            break;
        }
        result = adapGetNextCluster(enetPort, clusterId, &clusterId);
    }

    if(ClusterFound)
    {
        if(MEA_API_IsOutPortOf_Service(MEA_UNIT_0, ServiceId, pQueue->ClusterId)==MEA_TRUE)
        {
            // Update Service with new Wred profile (not default) in case of only one outPort (found cluster) exists on this service
            PortsCount = MEA_API_Get_Num_OutPorts(MEA_UNIT_0, ServiceId);
            if(MeaDrvAddEthernityWredProfilesToService(ServiceId, (PortsCount == 1) ? pQueue->WredProfileIdx : 0) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAttachWredProfileForService ERROR: MeaDrvAddEthernityWredProfilesToService FAIL for Service:%d!!!\n",
                        ServiceId);
                return ENET_FAILURE;
            }
        } //ClusterFound
    }
    //update reverse action if needed with new wred
    adap_memset(&Service_data , 0 , sizeof(Service_data ));
    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                                ServiceId,
                                NULL,
                                &Service_data,
                                NULL,
                                NULL,
                            NULL) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAttachWredProfileForService ERROR: failed by getting service id%d\n", ServiceId);
        return ENET_FAILURE;
    }
    if((Service_data.DSE_learning_srcPort_force == MEA_TRUE) &&
        (Service_data.DSE_learning_srcPort == pQueue->ClusterId))
    {
        //Update reverse action
        if(MeaDrvAddEthernityWredProfilesToReverseAction(Service_data.DSE_learning_actionId, pQueue->WredProfileIdx) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvAttachWredProfileForService ERROR: MeaDrvAddEthernityWredProfilesToReverseAction FAIL for Service:%d!!!\n",
                    ServiceId);
            return ENET_FAILURE;
        }
    }
    return ENET_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MeaDrvAttachEthernityWredProfiles                     */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : None - uses the  database as the source              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENET_SUCCESS - On success                             */
/*                      ENET_FAILURE - On failure                             */
/*****************************************************************************/
static ADAP_Uint32 MeaDrvAttachEthernityWredProfiles(void)
{
    unsigned int                            QueueIdx, idx;
    tQueueDb                                *pQueue = NULL;

    /* Scan all the Queues for the active WredProfileIdx*/
     for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
     {
	     for(QueueIdx=0; QueueIdx<MAX_NUM_OF_QUEUES; QueueIdx++)
	     {
		     pQueue = adap_getQueueEntry(idx, QueueIdx);
		     if (pQueue == NULL || pQueue->WredProfileIdx == ADAP_END_OF_TABLE || pQueue->WredProfileIdx == 0)
			     continue;

		     ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,
			"MeaDrvAttachEthernityWredProfiles - found wredIdx:%d for Queue ISS port: %d ENET QueueId: %d\n", 
			pQueue->WredProfileIdx, pQueue->iss_PortNumber, pQueue->ClusterId);
		     /* The (W)RED is defined for this queue. Look for all related services 
			and attached Wred profile if needed */
		     // NOTE: In 6.1 using the iss_PortNumber was commented out. 
		     //	This seems to be wrong and made in terms of some bugfix.
		     // So for now only iss_PortNumber was used. In case of bugs, 
		     //code should be modified to be as in 6.1.
		     tServiceDb * srv = MeaAdapGetFirstServicePort(pQueue->iss_PortNumber);
		     while (srv)
		     {
			     if (srv->L2_protocol_type <= D_PROVIDER_SERVICE_TAG)
			     {
				     if (MeaDrvAttachWredProfileForService(srv->serviceId, pQueue) != ENET_SUCCESS)
				     {
					     return ENET_FAILURE;
				     }
			     }
			     srv = MeaAdapGetNextServicePort(pQueue->iss_PortNumber, srv->serviceId);
		     }
	     }
     }
    return ENET_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :    MeaDrvUpdateEthernityQoS                           */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENET_SUCCESS - On success                             */
/*                      ENET_FAILURE - On failure                             */
/*****************************************************************************/
ADAP_Uint32 MeaDrvUpdateEthernityQoS (void)
{
    tQueueDb *pQueue;
    ADAP_Uint16 queueIdx = 0;

    pQueue = adap_getQueueEntryByIndex(queueIdx);
    while (pQueue)
    {
        if (pQueue->valid == ADAP_TRUE)
        {
            /* Update the Ethernity HW with the new changes - do that only when all 8 queues are assigned per cluster*/
            if(adap_getNumDefinedQueuesForCluster(pQueue->iss_PortNumber, pQueue->ClusterId) == ENET_PLAT_QUEUE_NUM_OF_PRI_Q)
            {
                if(MeaDrvUpdateEnetQueues (pQueue->iss_PortNumber, pQueue->ClusterId) != ENET_SUCCESS)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEthernityQoS ERROR: MeaDrvUpdateEthernityQoS FAIL\n");
                    return ENET_FAILURE;
                }
            }
        }
        queueIdx++;
        pQueue = adap_getQueueEntryByIndex(queueIdx);
    }

    if(MeaDrvUpdateEnetPolicers() != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvUpdateEthernityQoS: Setting up EthernityPolicers Not Update !!!\n");
        return ENET_SUCCESS;
    }

    if(MeaDrvAttachEthernityWredProfiles() != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUpdateEthernityQoS ERROR: Setting up the Ethernity WRED profiles FAIL!!!\n");
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

#if 0
/*****************************************************************************/
/* Function Name      : MeaDrvQosUpdateWredProfile                               */
/*                                                                           */
/* Description        : Update the existing ENET WRED profile                */
/*                       or Create a new one if there is no suitable one.    */
/*                      The HW WRED Profiles are a very limited resource,    */
/*                      so the Adapter SW made efforts to reuse these AMAP   */
/*                                                                           */
/* Input(s)           : WredProfileConfigTable - the 8-entries               */
/*                    (entry per Priority queue) ISS WRED profile descriptor */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENET WRED Profile ID or the UNDEFINED_VALUE16        */
/*                      in case of failure -                                 */
/*                      e.g. no more WRED Profiles available in the HW       */
/*****************************************************************************/
static MEA_WredProfileId_t MeaDrvQosUpdateWredProfile(tEnetHal_QoSREDCfgEntry WredProfileConfigTable[ENET_PLAT_QUEUE_NUM_OF_PRI_Q])
{
    MEA_WRED_Profile_Key_dbt            WredProfileKey;
    MEA_WRED_Profile_Data_dbt           WredProfileData;
    MEA_WRED_Profile_Data_dbt           WredNewProfileData;

    /* Replicas of the u4MinAvgThresh, u4MaxAvgThresh scaled to the 0..MAX_Q_OCCUPANCY range*/
    unsigned int                        MinThreshScaled = 0;
    unsigned int                        MaxThreshScaled = 0;
    /* Replica of the u1MaxProbability, scaled to the 0..MAX_PROBABILITY range*/
    unsigned int                        MiddlewayProbabilityScaled;
    /* Replica of the u4ECNThresh, scaled to the 0..MAX_Q_OCCUPANCY range*/
    unsigned int                        EcnThreshScaled;

    MEA_WredProfileId_t                 WredProfile;

    unsigned int                        FreeWredProfileIdx;
    unsigned int                        WredProfileIdx;
    unsigned int                        WredTableIdx;
    unsigned int                        priorityQueueId;
//    unsigned int                        EcnPriorityQueueId;
    MEA_Bool                            WredProfileCompatible;
    adap_memset(&WredProfileKey,0,sizeof(MEA_WRED_Profile_Key_dbt));
    adap_memset(&WredProfileData,0,sizeof(MEA_WRED_Profile_Data_dbt));
    adap_memset(&WredNewProfileData,0,sizeof(MEA_WRED_Profile_Data_dbt));

#ifdef SHOW_DEBUG_MSG
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Wred Profile Config Table:\nPrQ Min Thresh Max Thresh Middleway Prob ECN ECN Thresh \n");
#endif
    /* Calculate the new Wred Priority Table*/
    adap_memset(&WredNewProfileData, 0, sizeof(WredNewProfileData));
    for(priorityQueueId=0; priorityQueueId<ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priorityQueueId++)
    {
        if(WredProfileConfigTable[priorityQueueId].inUse)
        {
            /* Scale the original parameters from the original scale: 0..100
             * to the target range: 0..MEA_WRED_NORMALIZE_AQS_MAX_VALUE for Queue occupancy
             * and 0..MEA_WRED_DROP_PROBABILITY_MAX_VALUE for probability.
             * Care is taken to keep the precision and avoid rounding errors as much as possible */
            MinThreshScaled = (WredProfileConfigTable[priorityQueueId].u4MinAvgThresh*(1024*MEA_WRED_NORMALIZE_AQS_MAX_VALUE/100)+512)/1024;
            MaxThreshScaled = (WredProfileConfigTable[priorityQueueId].u4MaxAvgThresh*(1024*MEA_WRED_NORMALIZE_AQS_MAX_VALUE/100)+512)/1024;
            MiddlewayProbabilityScaled = (WredProfileConfigTable[priorityQueueId].u1MaxProbability*(1024*MEA_WRED_DROP_PROBABILITY_MAX_VALUE/100)+512)/1024;
            /* Scale the ECN threshold to the target range: 0..MEA_WRED_NORMALIZE_AQS_MAX_VALUE for Queue occupancy;
               Limit it to 0 ..60 Round it to nearest X4 - per ENET limitations */
            EcnThreshScaled = (WredProfileConfigTable[priorityQueueId].u4ECNThresh*(1024*MEA_WRED_NORMALIZE_AQS_MAX_VALUE/100)+512)/1024;
            if(EcnThreshScaled > 60)
                EcnThreshScaled = 60;
            EcnThreshScaled = (EcnThreshScaled+2) & (~0x3);
//            EcnPriorityQueueId = priorityQueueId;

            /* Calculate the wredTable for the priorityQueueId */
            for(WredTableIdx=0; WredTableIdx<=MEA_WRED_NORMALIZE_AQS_MAX_VALUE; WredTableIdx++)
            {
                if(WredTableIdx<MinThreshScaled)
                {
                    WredNewProfileData.priorityTable[priorityQueueId].wredTable[WredTableIdx].drop_probability=0;
                }
                else if(WredTableIdx<MaxThreshScaled)
                {
                    WredNewProfileData.priorityTable[priorityQueueId].wredTable[WredTableIdx].drop_probability=MiddlewayProbabilityScaled;
                }
                else
                {
                    WredNewProfileData.priorityTable[priorityQueueId].wredTable[WredTableIdx].drop_probability=MEA_WRED_DROP_PROBABILITY_MAX_VALUE;
                }
            }
            MaxThreshScaled = (WredProfileConfigTable[priorityQueueId].u4MaxAvgThresh * 100) / 127;
            MinThreshScaled = (WredProfileConfigTable[priorityQueueId].u4MinAvgThresh * 100) / 127;

#ifdef SHOW_DEBUG_MSG
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"  %d    %3d %3d    %3d %3d      %3d %3d %d  %3d %3d \n",
                   priorityQueueId,
                   WredProfileConfigTable[priorityQueueId].u4MinAvgThresh,
                   MinThreshScaled,
                   WredProfileConfigTable[priorityQueueId].u4MaxAvgThresh,
                   MaxThreshScaled,
                   WredProfileConfigTable[priorityQueueId].u1MaxProbability,
                   MiddlewayProbabilityScaled,
                   WredProfileConfigTable[priorityQueueId].u1RDActionFlag,
                   WredProfileConfigTable[priorityQueueId].u4ECNThresh,
                   EcnThreshScaled);
#endif
        }
    }

    if( MinThreshScaled > 60)
    {
        WredNewProfileData.ECN_data.ECN_minTH = 60;
    }
    else
    {
        WredNewProfileData.ECN_data.ECN_minTH = MinThreshScaled;
    }

    if( MaxThreshScaled > 92)
    {
        WredNewProfileData.ECN_data.ECN_maxTH = 92;
    }
    else if(MaxThreshScaled < 64)
    {
        WredNewProfileData.ECN_data.ECN_maxTH = 64;
    }
    else
    {
        WredNewProfileData.ECN_data.ECN_maxTH = MaxThreshScaled;
    }
    /* Wred Priority Table done */
    /* Update the ECN data in the new profile */
    //WredNewProfileData.ECN_data.ECN_enable = WredProfileConfigTable[EcnPriorityQueueId].u1RDActionFlag == 0 ? 0 : 1 ;
    WredNewProfileData.ECN_data.ECN_enable = 1;
    //WredNewProfileData.ECN_data.ECN_minTH = EcnThreshScaled;
    //WredNewProfileData.ECN_data.ECN_maxTH = 92;

    /* Find the existing WRED profile with the same priorityTable */
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Looking for suitable WRED profile. Total %d WRED profiles\n", MEA_WRED_NUM_OF_PROFILES);
    FreeWredProfileIdx=UNDEFINED_VALUE16;
    adap_memset(&WredProfileKey, 0, sizeof(WredProfileKey));
    for(WredProfileIdx=0; WredProfileIdx<MEA_WRED_NUM_OF_PROFILES; WredProfileIdx++)
    {
        if(AdapIsWredProfileInUse(WredProfileIdx) == ADAP_FALSE)
        {   /* This table' entry is free - rememeber it in case we'll need to allocate a new entry */
            if(FreeWredProfileIdx == UNDEFINED_VALUE16)
            {
                FreeWredProfileIdx = WredProfileIdx;
            }
            continue;
        }

        /* A real WRED profile found. May this profile be used? */
        WredProfile = AdapGetWredProfileByIdx(WredProfileIdx);
        WredProfileKey.acm_mode =0;
        WredProfileKey.profile_id = WredProfile;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvQosUpdateWredProfile: Read the existing profile ID:%ld, IDX:%d\n",
                WredProfile, WredProfileIdx);
        if (MEA_API_Get_WRED_Profile(MEA_UNIT_0,
                                     &WredProfileKey,
                                     &WredProfileData)
           != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvQosUpdateWredProfile ERROR: MEA_API_Get_WRED_Profile failed getting WRED profile:%ld\n",
                    WredProfileKey.profile_id);
            return UNDEFINED_VALUE16;
        }
        /* Is the ECN status and the thresholds are the same as requested? */
        if(MEA_OS_memcmp(&(WredNewProfileData.ECN_data), &(WredProfileData.ECN_data), sizeof(WredProfileData.ECN_data))
        == 0)
        {
            /* The ECN config match -
               Compare the drop probabilities for all actual Priority Queues */
        WredProfileCompatible = MEA_TRUE; /* Assume it's OK unless difference fouund */
        for(priorityQueueId=0; priorityQueueId<ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priorityQueueId++)
        {
            if(WredProfileConfigTable[priorityQueueId].inUse)
            {
                if(MEA_OS_memcmp(&(WredNewProfileData.priorityTable[priorityQueueId]),
                                 &(WredProfileData.priorityTable[priorityQueueId]),
                                 sizeof(WredProfileData.priorityTable[priorityQueueId]))
                != 0)
                {
                    /* The WRED profiles are NOT the same !*/
                    WredProfileCompatible = MEA_FALSE;
                    break;
                }
            }
            }
        }
        else
        {
            /* The ECN config does not match - this profile can't be reused */
            WredProfileCompatible = MEA_FALSE;
        }
        if(WredProfileCompatible == MEA_TRUE)
        {
            AdapIncWredProfileUseCount(WredProfileIdx);
            return WredProfileIdx;
        }
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvQosUpdateWredProfile: The suitable WRED profile not found - allocate a new one\n");
    WredProfileIdx = FreeWredProfileIdx;
    adap_memset(&WredProfileKey, 0, sizeof(WredProfileKey));
    WredProfileKey.acm_mode =0;
    WredProfileKey.profile_id = AdapGetWredProfileByIdx(WredProfileIdx);
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvQosUpdateWredProfile: New profile ID:%d, IDX:%d allocated\n",
            WredProfileKey.profile_id, WredProfileIdx);

    if(WredProfileKey.profile_id == 0) //do not change profile 0
    {
        WredProfileKey.acm_mode =0;
        WredProfileKey.profile_id = 1;
        WredProfileIdx = 1;
    }
    if(WredProfileKey.profile_id == 1)
    {
        WredProfileKey.acm_mode =0;

        if(MEA_API_IsValid_WRED_Profile(MEA_UNIT_0, &WredProfileKey, MEA_TRUE) == MEA_FALSE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvQosUpdateWredProfile : profile %d not exist create one\n",
                    WredProfileKey.profile_id);
            WredProfileKey.acm_mode =0;
            if (MEA_API_Create_WRED_Profile(MEA_UNIT_0,
                                         &WredProfileKey,
                                         NULL) != MEA_OK)
            {

                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvQosUpdateWredProfile ERROR: MEA_API_Create_WRED_Profile failed, WRED profile%ld\n",
                        WredProfileKey.profile_id);
                return UNDEFINED_VALUE16;
            }

         }
        WredProfileKey.acm_mode=0;

        if (MEA_API_Set_WRED_Profile(MEA_UNIT_0,
                                     &WredProfileKey,
                                     &WredNewProfileData)
           != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvQosUpdateWredProfile ERROR: MEA_API_Set_WRED_Profile failed, WRED profile%ld\n",
                    WredProfileKey.profile_id);
            return UNDEFINED_VALUE16;
        }
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvQosUpdateWredProfile ERROR: ,WRED profile:%lu out of range\n",
                    WredProfileKey.profile_id);
            return UNDEFINED_VALUE16;
    }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MeaDrvQosUpdateWredProfile: New profile ID:%d, IDX:%d allocated\n",
                WredProfileKey.profile_id, WredProfileIdx);
    AdapIncWredProfileUseCount(WredProfileIdx);
    return WredProfileIdx;
}
#endif

/*****************************************************************************/
/* Function Name      : MeaDrvQosDeleteAllWredProfiles                           */
/*                                                                           */
/* Description        : Delete the existing ENET WRED profile                */
/*                      The HW WRED Profiles are a very limited resource,    */
/*                      so the Adapter SW made efforts to reuse these AMAP   */
/*                                                                           */
/* Input(s)           : WredProfileId - the ENET WRED profile ID             */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
static ADAP_Uint32 MeaDrvQosDeleteAllWredProfiles(void)
{
    AdapClearAllWredProfileUseCount();
    return ENET_SUCCESS;
}

void MeaDrvUpdatePolicerForIp(ADAP_Uint32 ipAddr, ADAP_Uint32 mask)
{
    ADAP_Uint16 MeterIndex = 0;
    tMeterDb *meterDb = NULL;
    ADAP_Uint16 index = 0;
    MEA_Action_Entry_Data_dbt Action_data;

    MeterIndex = MeaAdapGetMeterIdLinkedToIP(ipAddr);
    if (MeterIndex == ADAP_END_OF_TABLE)
    {
	return;
    }
    meterDb = MeaAdapGetMeterByIndex(MeterIndex);
    if (meterDb == NULL)
    {
        return;
    }

    if (MeaDrvCreateEnetPolicer(MeterIndex) != ENET_SUCCESS)
    {
        return;
    }

    if (mask == 0xffffffff)
    {
        adap_memset(&Action_data, 0 , sizeof(Action_data));
        if (MeaDrvSetPolicerInDestIpForwarder (ipAddr, START_L3_VPN, meterDb->policer_prof_id, &Action_data) != MEA_TRUE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Unable to set on forwarder for policer: %d!!!\n",
               meterDb->policer_prof_id);
        }
        if (MeaDrvSetPolicerInDestIpForwarder (ipAddr, START_L3_VPN1, meterDb->policer_prof_id, &Action_data) != MEA_TRUE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Unable to set on forwarder for policer: %d!!!\n",
               meterDb->policer_prof_id);
        }
    }
    if (mask == 0xffffff00)
    {
        for (index = 1; index <= MAX_NUM_OF_IP_CLASS_ENTRIES; index++)
        {
            adap_memset(&Action_data, 0 , sizeof(Action_data));
            if (MeaDrvSetPolicerInDestIpForwarder (ipAddr + index, START_L3_VPN, meterDb->policer_prof_id,&Action_data) != MEA_TRUE)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Unable to set on forwarder for policer: %d!!!\n",
                   meterDb->policer_prof_id);
            }
            if (MeaDrvSetPolicerInDestIpForwarder (ipAddr + index, START_L3_VPN1, meterDb->policer_prof_id,&Action_data) != MEA_TRUE)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Unable to set on forwarder for policer: %d!!!\n",
                    meterDb->policer_prof_id);
            }
        }
    }
}

ADAP_Int32
EnetHal_QoSHwMapClassToPolicy (tEnetHal_QoSClassMapEntry * pClsMapEntry,
						tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
						tEnetHal_QoSInProfileActionEntry * pInProActEntry,
						tEnetHal_QoSOutProfileActionEntry * pOutProActEntry,
						tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag)
{

	ADAP_Uint16 ClassifierIndex = ADAP_END_OF_TABLE;
	ADAP_Uint16 PolicerIndex = ADAP_END_OF_TABLE;
	ADAP_Uint16 MeterIndex = ADAP_END_OF_TABLE;
	tMeterDb *pMeterDb = NULL;

    /* Create Classifier if needed */
    if(pClsMapEntry != NULL)
    {
		ClassifierIndex = adap_createClassifierEntry (pClsMapEntry);
		if(ClassifierIndex == ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: Unable to create classifier for ClassId: %d!!\n", pClsMapEntry->QoSMFClass);
			return ENET_FAILURE;
		}
        else
        {
            /* Update the Classifier parameters in the local DB */
            if(adap_updateClassifierEntry (ClassifierIndex, pClsMapEntry) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to update Classifier parameters for Index: %d!!\n", ClassifierIndex);
                return ENET_FAILURE;
            }
        }
    }

    /* Create Policer if needed */
    if(pPlyMapEntry != NULL)
    {
        PolicerIndex = adap_createPolicerEntry (pPlyMapEntry);
        if(PolicerIndex == ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: Unable to create policer for PolicyId: %d!!\n", pPlyMapEntry->QoSPolicyMapId);
			return ENET_FAILURE;
		}
        else
        {
            /* Update the Policer parameters in the local DB */
            if(adap_updatePolicerEntry (PolicerIndex, pPlyMapEntry, pInProActEntry, pOutProActEntry) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: Unable to update Policer parameters for Index: %d!!\n", PolicerIndex);
                return ENET_FAILURE;
            }
        }
    }

    /* Create Meter if needed */
    if(pMeterEntry != NULL)
	{
		pMeterDb = MeaAdapGetMeterParams(pMeterEntry->i4QoSMeterId);
		if(pMeterDb == NULL)
		{
			pMeterDb = MeaAdapAllocateMeterParams(pMeterEntry->i4QoSMeterId);
		}

		if(pMeterDb != NULL)
        {
			MeterIndex = adap_getMeterIndex(pMeterEntry->i4QoSMeterId);
            if(adap_updateMeterEntry (MeterIndex, pMeterEntry) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: Unable to update Meter parameters for Index: %d!!\n", MeterIndex);
                return ENET_FAILURE;
            }

            /* Create the ENET policer and store it's ID in the DB */
            if(MeaDrvCreateEnetPolicer(MeterIndex) != ENET_SUCCESS)
        	{
        		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Meter: Unable to update the HW!!!\n");
        		return ENET_FAILURE;
        	}
        }
    }

    /* Update database with new Meter, Policer and Classifier*/
    if(adap_UpdateMeterPolicerClassifier (MeterIndex, PolicerIndex, ClassifierIndex, 1) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: Unable to update meter, policer and classifier parameters in DPL for MeterIdx: %d, PolicerIdx: %d, ClassIdx: %d!!\n", MeterIndex, PolicerIndex, ClassifierIndex);
        return ENET_FAILURE;
    }
    /* Update the  HW with the new changes*/
    if(MeaDrvUpdateEthernityQoS () != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: MeaDrvUpdateEthernityQoS FAIL\n");
        return ENET_FAILURE;
    }
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"exit\n");
    return ENET_SUCCESS;
}

static ADAP_Uint32 adap_QoSGetPortPolicerParams(ADAP_Uint16 ifIndex,ADAP_Uint16 MeterIdx)
{
	tMeterDb                            *pMeter;
    ADAP_Uint32							PolicingProfileId;
	MEA_Policer_Entry_dbt 				entry;

	pMeter = MeaAdapGetMeterByIndex(MeterIdx);

	if(pMeter == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_QoSGetPortPolicerParams:failed to get meter from meter index:%d\n",MeterIdx);
		return ENET_FAILURE;
	}

	PolicingProfileId = MeaAdapGetDefaultPolicer(ifIndex);

    if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0, PolicingProfileId, 0, &entry )
        != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_QoSGetPortPolicerParams: failed to get PolicingProfileId from index:%d\n",PolicingProfileId);
		return ENET_FAILURE;
    }

    adap_UpdatePolicerParameters(pMeter, &entry);

    if(MEA_API_Set_Policer_ACM_Profile(MEA_UNIT_0,PolicingProfileId, 0, 0, &entry ) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_QoSGetPortPolicerParams: failed to set PolicingProfileId from index:%d\n",PolicingProfileId);
		return ENET_FAILURE;
    }

    return (ENET_SUCCESS);
}

static ADAP_Int32 adap_UpdateInterfaceServiceVlan(ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 setting1588=0;
	ADAP_Uint32 trafficType=0;
	MEA_Service_t    Service;
	tEnetHal_VlanId VlanId = 0;
	tServiceDb 		*pServiceId=NULL;

	//TODO:setting1588 = isPortSet1588(ifIndex);
	setting1588 = MEA_FALSE;
	for(VlanId =0;VlanId <ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
	{
		for(trafficType=MEA_PARSING_L2_KEY_Untagged; trafficType<MEA_PARSING_L2_KEY_Ctag_Ctag; trafficType++)
		{
			pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,ifIndex,trafficType);
			if(pServiceId == NULL)
				continue;

			Service = pServiceId->serviceId;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_UpdateInterfaceServiceVlan  - update serviceId:%d Vlan:%d\r\n",Service,VlanId);
			if(MeaDrvVlanHwUpdateParameters(Service,ifIndex,MEA_TRUE,setting1588,VlanId) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_UpdateInterfaceServiceVlan ERROR: MeaDrvVlanHwUpdateParameters for Service_id:%d failed\n", Service);
				return ENET_FAILURE;
			}
		}
	}

	return ENET_SUCCESS;
}

static ADAP_Uint32 adap_SetEthernityPolicer(ADAP_Uint16 MeterIdx, tEnetHal_QoSMeterEntry * pMeterEntry)
{
	tMeterDb*               meter;
    MEA_Policer_Entry_dbt   PolicerEntry;
    MEA_Uint16              policer_id;
    MEA_AcmMode_t           acm_mode;

    meter = MeaAdapGetMeterByIndex(MeterIdx);
    if(meter == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get Meter for Index: %d!!!\n", MeterIdx);
        return ENET_FAILURE;
    }

    /* Get the ENET Policer ID from the database */
    policer_id = meter->policer_prof_id;
	/* Set the Meter parameters given by Aricent into the local DB */
	if(adap_updateMeterEntry (MeterIdx, pMeterEntry) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to set Meter parameters in DPL for Index: %d!!\n", MeterIdx);
		return ENET_FAILURE;
	}

    /* Set the Policer configuration object */
    adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
    /* ASSUMPTIONS:
     * CBS, CIR, EBS, EIR are all < 1Gbps. Otherwise, the scaler (gn_type) should be used
     * */

    acm_mode = 0;

    if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0, policer_id, acm_mode, &PolicerEntry) !=MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetEthernityPolicer: MEA_API_Get_Policer_ACM_Profile FAIL\n");
        return ENET_FAILURE;
    }

    adap_UpdatePolicerParameters(meter, &PolicerEntry);

    if(MEA_API_Set_Policer_ACM_Profile(MEA_UNIT_0, policer_id, acm_mode, MEA_INGRESS_PORT_PROTO_TRANS, &PolicerEntry) !=MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetEthernityPolicer: MEA_API_Set_Policer_ACM_Profile FAIL\n");
        return ENET_FAILURE;
    }
	return ENET_SUCCESS;
}

static ADAP_Int32 adap_SetPortPolicerToDefault(ADAP_Uint32 ifIndex)
{
    MEA_Policer_Entry_dbt                 PolicerEntry;
    MEA_Uint16                            policer_id;
	MEA_AcmMode_t           			  acm_mode;

	policer_id = MeaAdapGetDefaultPolicer(ifIndex);
	if(policer_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,policer_id, 0, &PolicerEntry )!= MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetPortPolicerToDefault: MEA_API_Get_Policer_ACM_Profile FAIL\n");
			return ENET_FAILURE;
		}
		adap_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
		PolicerEntry.CIR = 100000000 + ifIndex * 1000000;
		PolicerEntry.CBS = 32000;
		PolicerEntry.EIR = 10000000 + ifIndex * 1000000;
		PolicerEntry.EBS = 32000;
		PolicerEntry.comp = 0;
		PolicerEntry.gn_type= 1;
		acm_mode = 0;
		if(MEA_API_Set_Policer_ACM_Profile(MEA_UNIT_0,policer_id,acm_mode,MEA_INGRESS_PORT_PROTO_TRANS, &PolicerEntry ) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_SetPortPolicerToDefault: MEA_API_Set_Policer_ACM_Profile FAIL\n");
			return ENET_FAILURE;
		}
	}

	return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwUpdatePolicyMapForClass (tEnetHal_QoSClassMapEntry * pClsMapEntry,
								tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
								tEnetHal_QoSInProfileActionEntry * pInProActEntry,
								tEnetHal_QoSOutProfileActionEntry * pOutProActEntry,
								tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag)
{
	ADAP_Uint16     MeterIndex =        ADAP_END_OF_TABLE;
    ADAP_Uint16     PolicerIndex =      ADAP_END_OF_TABLE;
    tPolicyDb   	*pPolicer;
    ADAP_Bool        IsQosUpdateNeeded = ADAP_FALSE;
    ADAP_Uint32		i4QoSMeterId;
	ADAP_Uint32		ClassifierIndex;

    if(pClsMapEntry == NULL)
    {
		// in this case need to configure the metering
		if(pPlyMapEntry != NULL)
		{
			MeterIndex = adap_getMeterIndex(pPlyMapEntry->QoSMeterId);
			if(MeterIndex != ADAP_END_OF_TABLE)
			{
				if(adap_QoSGetPortPolicerParams (pPlyMapEntry->IfIndex,MeterIndex)!= ENET_SUCCESS)
				{
				   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: QoSGetPortPolicerParams FAIL\n");
					return ENET_FAILURE;
				}

				// update the all services with the same default policer
				adap_SetDefaultPolicerIdTmId(MEA_PLAT_GENERATE_NEW_ID,pPlyMapEntry->IfIndex);
				adap_SetDefaultPolicerIdPmId(MEA_PLAT_GENERATE_NEW_ID,pPlyMapEntry->IfIndex);
				if(adap_UpdateInterfaceServiceVlan(pPlyMapEntry->IfIndex) != ENET_SUCCESS)
				{
				   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: adap_UpdateInterfaceServiceVlan FAIL\n");
					return ENET_FAILURE;
				}
				adap_SetMeterIndexPerPort(pPlyMapEntry->IfIndex,MeterIndex);
				return ENET_SUCCESS;
			}
		}
    }

    if((pPlyMapEntry != NULL))
    {
        PolicerIndex = adap_getPolicerIndex(pPlyMapEntry->QoSPolicyMapId);
        MeterIndex = adap_getMeterIndex(pPlyMapEntry->QoSMeterId);
    }
    /* Update the Policy parameters on DPL*/
    switch(u1Flag)
    {
        case  ENET_QOS_UPDATE_PLY_IF                      ://1
        case  ENET_QOS_UPDATE_PLY_PHB_TYPE                ://2
        case  ENET_QOS_UPDATE_PLY_PHB_VAL                 ://3
            return ENET_SUCCESS;
        case  ENET_QOS_UPDATE_PLY_METER_ID                ://4
		    if((pPlyMapEntry != NULL))
            {
                if(MeterIndex != ADAP_END_OF_TABLE && pMeterEntry != NULL)
                {   /* Update the existing ENET policer */
                    if(adap_SetEthernityPolicer(MeterIndex, pMeterEntry) != ENET_SUCCESS)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to update Meter for MeterId: %d Meter Index: %d!!\n",
                                pPlyMapEntry->QoSMeterId, MeterIndex);
                        return ENET_FAILURE;
                    }
                    else
                    {
                        if(adap_UpdateMeterPolicerClassifier (MeterIndex, PolicerIndex, ADAP_END_OF_TABLE, 1) != ENET_SUCCESS)
                        {
                            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to update meter parameters in DPL for MeterIdx: %d, PolicerIdx: %d!!\n", MeterIndex, PolicerIndex);
                            return ENET_FAILURE;
                        }
                    }
                }
				else if(pMeterEntry != NULL)
				{
					//request to create meter and update policy to meter
				   if(EnetHal_QoSHwMeterCreate (pMeterEntry) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to create Meter for MeterId: %d!!\n", pMeterEntry->i4QoSMeterId);
						return ENET_FAILURE;
					}

					if(pClsMapEntry != NULL)
					{
						ClassifierIndex = adap_getClassifierIndex(pClsMapEntry->QoSMFClass);
					}
					else
					{
						ClassifierIndex = ADAP_END_OF_TABLE;
					}

					 MeterIndex = adap_getMeterIndex(pMeterEntry->i4QoSMeterId);

					if(adap_UpdateMeterPolicerClassifier (MeterIndex, PolicerIndex, ClassifierIndex, 1) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to update meter parameters in DPL for MeterIdx: %d, PolicerIdx: %d!!\n", MeterIndex, PolicerIndex);
						return ENET_FAILURE;
					}
				}
                else
                {
					if(pPlyMapEntry->QoSMeterId == 0)
                    {
						//get meter from policer index
						MeterIndex = adap_GetMeterIndexFromPolicer(PolicerIndex);
						if(MeterIndex == ADAP_END_OF_TABLE)
						{
							 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: by getting meter index from policer Index: %d!!! \n",
										PolicerIndex);
								return ENET_FAILURE;
						}
						if( (pPlyMapEntry->IfIndex > 0) && (pPlyMapEntry->IfIndex < MeaAdapGetNumOfPhyPorts()) &&
							(adap_GetMeterIndexPerPort(pPlyMapEntry->IfIndex) == MeterIndex) )
						{
							//unset meter from port - meaning return to default
							// update the all services with the same default policer
							adap_SetDefaultPolicerIdTmId(ADAP_END_OF_TABLE,pPlyMapEntry->IfIndex);
							adap_SetDefaultPolicerIdPmId(ADAP_END_OF_TABLE,pPlyMapEntry->IfIndex);
							adap_SetPortPolicerToDefault(pPlyMapEntry->IfIndex);
							if(adap_UpdateInterfaceServiceVlan(pPlyMapEntry->IfIndex) != ENET_SUCCESS)
							{
							   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: adap_UpdateInterfaceServiceVlan FAIL\n");
								return ENET_FAILURE;
							}
							adap_SetMeterIndexPerPort(pPlyMapEntry->IfIndex,ADAP_END_OF_TABLE);
						}
						else
						{
							// need to unset the meter from flows
							tMeterDb          			*meter = NULL;
						    meter = MeaAdapGetMeterByIndex(MeterIndex);

						    if(meter != NULL)
						    {
								i4QoSMeterId = meter->issMeter.i4QoSMeterId;
								if(EnetHal_QoSHwMeterDelete (i4QoSMeterId) != ENET_SUCCESS)
								{
									ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to delete Meter for MeterId: %d Meter Index: %d!!\n",
											i4QoSMeterId, MeterIndex);
									return ENET_FAILURE;
								}
						    }
						}
                    }
                    else
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Invalid Meter Id:%d Meter Index: %d!!! \n",
                                pPlyMapEntry->QoSMeterId, MeterIndex);
                        return ENET_FAILURE;
                    }

                    if(pMeterEntry != NULL)
                    {
                        if(EnetHal_QoSHwMeterCreate (pMeterEntry) != ENET_SUCCESS)
                        {
                            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to create Meter for MeterId: %d!!\n", pMeterEntry->i4QoSMeterId);
                            return ENET_FAILURE;
                        }

                        if(adap_UpdateMeterPolicerClassifier (MeterIndex, PolicerIndex, ADAP_END_OF_TABLE, 1) != ENET_SUCCESS)
                        {
                            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: Unable to update meter parameters in DPL for MeterIdx: %d, PolicerIdx: %d!!\n", MeterIndex, PolicerIndex);
                            return ENET_FAILURE;
                        }
						else
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"QoSHwUpdatePolicyMapForClass assigned meter parameters in DPL for MeterIdx: %d, PolicerIdx: %d!!\n", MeterIndex, PolicerIndex);
						}
                    }
                }
            }
            IsQosUpdateNeeded = ADAP_FALSE;
        break;
   /* Conform (GREEN) packet actions */
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_PORT           ://6
        {
			if(pPlyMapEntry == NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: pPlyMapEntry = NULL\n");
				return ENET_FAILURE;
			}
			if(pClsMapEntry != NULL)
			{
				ClassifierIndex = adap_getClassifierIndex(pClsMapEntry->QoSMFClass);
				if(ClassifierIndex == ADAP_END_OF_TABLE) /*New Classifier should be created */
				{
					ClassifierIndex = adap_createClassifierEntry (pClsMapEntry);
					if(ClassifierIndex == ADAP_END_OF_TABLE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"QoSHwUpdatePolicyMapForClass ERROR: Unable to create classifier for ClassId: %d!!\n", pClsMapEntry->QoSMFClass);
						return ENET_FAILURE;
					}
				}
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_QoSHwUpdatePolicyMapForClass: pClsMapEntry = NULL\n");
			}
            // Not implemented
			if(adap_QoSGetPortPolicerParams (pPlyMapEntry->IfIndex,MeterIndex)!= ENET_SUCCESS)
			{
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: QoSGetPortPolicerParams FAIL\n");
                return ENET_FAILURE;
			}

			// update the all services with the same default policer
			adap_SetDefaultPolicerIdTmId(ADAP_END_OF_TABLE,pPlyMapEntry->IfIndex);
			adap_SetDefaultPolicerIdPmId(ADAP_END_OF_TABLE,pPlyMapEntry->IfIndex);
			if(adap_UpdateInterfaceServiceVlan(pPlyMapEntry->IfIndex) != ENET_SUCCESS)
			{
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwUpdatePolicyMapForClass ERROR: DPL_UpdateInterfaceServiceVlan FAIL\n");
                return ENET_FAILURE;
			}
            return ENET_SUCCESS;
        }
        break;
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_FLAG           ://5
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_IPTOS          ://7
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_IPDSCP         ://8
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_VLAN_PRI       ://9
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_VLAN_DE        ://10
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_PRI ://11
        case  ENET_QOS_UPDATE_PLY_ACT_CONF_MPLS_EXP       ://12
    /* Exceed (YELLOW) packet actions */
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_FLAG            ://13
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_IPTOS           ://14
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_IPDSCP          ://15
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_INNER_VLAN_PRI  ://16
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_VLAN_PRI        ://17
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_VLAN_DE         ://18
        case  ENET_QOS_UPDATE_PLY_ACT_EXC_MPLS_EXP        ://19
        {
            /* Update the Conform/Exceed action in the DB.
             * Futher processing is done in the QoSHWUpdateEthernityPolicers */
            pPolicer = adap_getPolicerEntryByIndex(PolicerIndex);
            if(pPolicer)
            {
				pPolicer->u1CurrFlag = u1Flag;
                if(pInProActEntry != NULL)
                {
                    /* Copy the Action data to the DB */
                    adap_memcpy(&(pPolicer->issInProActEntry),
                                  pInProActEntry,
                                  sizeof(tEnetHal_QoSInProfileActionEntry));
                    pPolicer->issInProActEntryValid = ADAP_TRUE;
                 }
                else
                	pPolicer->issInProActEntryValid = ADAP_FALSE;
                IsQosUpdateNeeded = ADAP_TRUE;
            }
        }
        break;
    /* Violation (RED) packet actions */
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_ACT_FLAG        ://20
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_IPTOS           ://21
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_IPDSCP          ://22
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_INNER_VLAN_PRI  ://23
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_VLAN_PRI        ://24
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_VLAN_DE         ://25
        case  ENET_QOS_UPDATE_PLY_ACT_VIO_MPLS_EXP        ://26
             /* Update the Violation action in the DB.
             * Futher processing is done in the QoSHWUpdateEthernityPolicers */
        {
            pPolicer = adap_getPolicerEntryByIndex(PolicerIndex);
            if(pPolicer)
            {
                if(pOutProActEntry != NULL)
                {
                    /* Copy the Action data to the DB */
                    adap_memcpy(&(pPolicer->issOutProActEntry),
                                  pOutProActEntry,
                                  sizeof(tEnetHal_QoSOutProfileActionEntry));
                    pPolicer->issOutProActEntryValid = ADAP_TRUE;
                }
                else
                	pPolicer->issOutProActEntryValid = ADAP_FALSE;
                IsQosUpdateNeeded = ADAP_TRUE;
            }//Policer don't exists. What to do ??
        }
        break;
        default:
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_oSHwUpdatePolicyMapForClass - Invalid Policy map flag: %d!!! \n", u1Flag);
            return ENET_FAILURE;
        }
    }
    /* Update the Ethernity HW with the new changes*/
    if(IsQosUpdateNeeded == ADAP_TRUE)
    {
        if(MeaDrvUpdateEnetPolicers () != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToPolicy ERROR: MeaDrvUpdateEnetPolicers FAIL\n");
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwUnmapClassFromPolicy (tEnetHal_QoSClassMapEntry * pClsMapEntry,
                           tEnetHal_QoSPolicyMapEntry * pPlyMapEntry,
                           tEnetHal_QoSMeterEntry * pMeterEntry, ADAP_Uint8 u1Flag)
{
    MEA_SE_Entry_dbt entry;
    MEA_Bool found;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
    MEA_Policer_Entry_dbt                   Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Action_editing;
    MEA_EHP_Info_dbt                        Action_editing_info;
    ADAP_Uint16 MeterIndex =        ADAP_END_OF_TABLE;
    ADAP_Uint16 PolicerIndex =      ADAP_END_OF_TABLE;
    ADAP_Uint32 ClassifierIndex =   ADAP_END_OF_TABLE;

    if ((pClsMapEntry == NULL) || (pPlyMapEntry == NULL) || (pMeterEntry == NULL))
        return ENET_SUCCESS;

    tMeterDb *pMeterDb = MeaAdapGetMeterParams(pMeterEntry->i4QoSMeterId);
    if(pMeterDb == NULL)
    {
	return ENET_SUCCESS;
    }

    adap_memset(&entry, 0, sizeof(entry));
    adap_memset(&Action_data, 0 , sizeof(Action_data    ));
    adap_memset(&Action_editing, 0 , sizeof(Action_editing ));
    adap_memset(&Action_policer, 0 , sizeof(Action_policer ));
    adap_memset(&Action_editing_info, 0, sizeof(Action_editing_info));
    adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));

    Action_editing.num_of_entries = 1;
    Action_editing.ehp_info = &Action_editing_info;

    if( ENET_ADAPTOR_GetFirst_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to get first entry\r\n");
        return ENET_FAILURE;
    }

    while( (found == MEA_TRUE))
    {
        if((entry.data.actionId_valid  == MEA_TRUE) &&
            ((entry.data.actionId != AdapGetRouteActionToCpu()) || (entry.data.actionId != 0)) &&
            (entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN))
        {
            if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                                        entry.data.actionId,
                                        &Action_data,
                                        &Action_outPorts,
                                        &Action_policer,
                                        &Action_editing) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get action_Id:%d\n", entry.data.actionId);
                return ENET_FAILURE;
            }

            if(pMeterDb->policer_prof_id == Action_data.policer_prof_id)
            {
                Action_data.tm_id_valid = MEA_FALSE;
                Action_data.tm_id = 0;
                if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                                        Action_data.policer_prof_id, 0,/* ACM_Mode */
                                                        &Action_policer) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Policer_ACM_Profile failed\n");
                    return ENET_FAILURE;
                }
                if (ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
                                        entry.data.actionId,
                                        &Action_data,
                                        &Action_outPorts,
                                        &Action_policer,
                                        &Action_editing) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set action_Id:%d\n", entry.data.actionId);
                    return ENET_FAILURE;
                }
            }
        }

        if( ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to get first entry\r\n");
            return ENET_FAILURE;
        }
    }

    MeterIndex = adap_getMeterIndex(pPlyMapEntry->QoSMeterId);
    PolicerIndex = adap_getPolicerIndex(pPlyMapEntry->QoSPolicyMapId);
    ClassifierIndex = adap_getClassifierIndex(pClsMapEntry->QoSMFClass);
    if(adap_UpdateMeterPolicerClassifier (MeterIndex, PolicerIndex, ClassifierIndex, 0) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR: Unable to update meter parameters in DPL for MeterIdx: %d, PolicerIdx: %d!!\n", 
            pMeterEntry->i4QoSMeterId, pPlyMapEntry->QoSPolicyMapId);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvUnmapClassifierFromFilters(ADAP_Uint16 ClassifierIndex)
{
    tClassifierDb *pClassifier=NULL;
    eAdap_ClassificationType ClassificationType;


    pClassifier=adap_getClassifierEntryByIndex(ClassifierIndex);
    if((pClassifier == NULL) || (pClassifier->valid == ADAP_FALSE))
    {
        // Doing nothing
        return ENET_SUCCESS;
    }

    if (MeaDrvUpdateFilterPolicer(pClassifier, NULL, &ClassificationType) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvUnmapClassifierFromFilters ERROR: MeaDrvUpdateFilterPolicer FAIL for ClassfierIndex:%d\n",
                ClassifierIndex);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}


ADAP_Int32
EnetHal_QoSHwDeleteClassMapEntry (tEnetHal_QoSClassMapEntry * pClsMapEntry)
{
	ADAP_Uint16 ClassifierIndex;

    /* Unmap the Class from Policy*/
    if(pClsMapEntry == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," Unable to delete Classifier - NULL parameter\n");
        return ENET_FAILURE;
    }

	ClassifierIndex = adap_getClassifierIndex(pClsMapEntry->QoSMFClass);
	if(ClassifierIndex == ADAP_END_OF_TABLE)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," EnetHal_QoSHwDeleteClassMapEntry - Classifier %d already deleted\n", pClsMapEntry->QoSMFClass);
        return ENET_SUCCESS;
    }

    // Drop policer for all services which include this policer_id
    // NOTE: ENET_FAILURE return is still ok, because profile can be with no owners
	if(MeaDrvUnmapClassifierFromFilters (ClassifierIndex) != ENET_SUCCESS)
	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to unmap ClassIdx: %d from associated filters!!\n", ClassifierIndex);
	    return ENET_FAILURE;
	}

    adap_UnmapClassFromPolicer (ClassifierIndex);

    /* Delete Classifier */
    if(adap_freeClassifierEntry (pClsMapEntry->QoSMFClass) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to reset Classifier parameters  for ClassId: %d!!\n", pClsMapEntry->QoSMFClass);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwMeterCreate (tEnetHal_QoSMeterEntry * pMeterEntry)
{
	ADAP_Uint16 MeterIndex = ADAP_END_OF_TABLE;
	tMeterDb *pMeterDb = NULL;

    if(pMeterEntry != NULL)
    {
		pMeterDb = MeaAdapGetMeterParams(pMeterEntry->i4QoSMeterId);
		if(pMeterDb == NULL)
		{
			pMeterDb = MeaAdapAllocateMeterParams(pMeterEntry->i4QoSMeterId);
		}

		if(pMeterDb != NULL)
        {
			MeterIndex = adap_getMeterIndex(pMeterEntry->i4QoSMeterId);
            if(adap_updateMeterEntry (MeterIndex, pMeterEntry) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMeterCreate ERROR: Unable to update Meter parameters for Index: %d!!\n", MeterIndex);
                return ENET_FAILURE;
            }

            /* Create the ENET policer and store it's ID in the DB */
            if(MeaDrvCreateEnetPolicer(MeterIndex) != ENET_SUCCESS)
            {
        		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Meter: Unable to update the HW!!!\n");
        		return ENET_FAILURE;
            }
        }  
    }

    return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvDeleteEthernityPolicer(ADAP_Uint16 MeterIdx)
{
	tMeterDb          			*meter = NULL;
	MEA_EditingMappingProfile_Id_t  id_i;

    meter = MeaAdapGetMeterByIndex(MeterIdx);

    if(meter != NULL)
    {
		//Delete policer and editing mapping profile
		if(meter->mappingEditingIdx != MEA_PLAT_GENERATE_NEW_ID)
		{
			id_i = adap_getEditMappingByIdx(meter->mappingEditingIdx);
			if(id_i != MEA_PLAT_GENERATE_NEW_ID)
			{
				MEA_API_Delete_EditingMappingProfile_Entry(MEA_UNIT_0,id_i);
				adap_freeEditMappingByIdx(meter->mappingEditingIdx);
				meter->mappingEditingIdx=MEA_PLAT_GENERATE_NEW_ID;
			}
		}

                //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Meter: ##################  API_Delete_Policer_ACM_Profile   \n");

		if(MEA_API_Delete_Policer_ACM_Profile(MEA_UNIT_0,
											  meter->policer_prof_id,
											  0)!= MEA_OK)
		{
			return ENET_FAILURE;
		}

		meter->policer_prof_id = MEA_PLAT_GENERATE_NEW_ID;
    }
	return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwMeterDelete (ADAP_Int32 i4MeterId)
{
	ADAP_Uint16 MeterIndex = ADAP_END_OF_TABLE;
	tMeterDb *pMeterDb = NULL;

	pMeterDb = MeaAdapGetMeterParams(i4MeterId);

	if(pMeterDb != NULL)
	{
		MeterIndex = adap_getMeterIndex(i4MeterId);
		if(MeterIndex != ADAP_END_OF_TABLE)
		{
			if(MeaDrvDeleteEthernityPolicer (MeterIndex) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Meter: Unable to update the HW!!!\n");
				return ENET_FAILURE;
			}
		    /* Reset the Meter parameters in the local DB */
			if(MeaAdapFreeMeterParams (i4MeterId) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to reset Meter parameters in DPL for MeterId: %d!!\n", i4MeterId);
				return ENET_FAILURE;
			}
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwSchedulerAdd (tEnetHal_QoSSchedulerEntry * pSchedEntry)
{
  	tSchedulerDb *pSchedulerDbEntry;

    if(pSchedEntry == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSchedulerAdd ERROR: Unable to create Scheduler - pSchedEntry parameter NULL\n");
		return ENET_FAILURE;
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE," EnetHal_QoSHwSchedulerAdd - Port: %d, SchedId: %d\n", pSchedEntry->QosIfIndex, pSchedEntry->QosSchedulerId);
    }

    /* Verify there is no scheduler with this ID already */
    if(adap_getSchedulerEntry(pSchedEntry->QosIfIndex, pSchedEntry->QosSchedulerId) != NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwSchedulerAdd: The scheduler %d for port:%d already exists!\n", pSchedEntry->QosSchedulerId, pSchedEntry->QosIfIndex);
        return ENET_SUCCESS;
    }

    /* Set the Scheduler given by Aricent into the local DB */
    pSchedulerDbEntry = adap_createSchedulerEntry(pSchedEntry);
	if(pSchedulerDbEntry == NULL)
	{

        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSchedulerAdd ERROR: Unable to create Scheduler in DB for SchedId: %d!!\n", pSchedEntry->QosSchedulerId);
        return ENET_FAILURE;
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwSchedulerAdd: Scheduler created in DB for Port: %d, SchedId: %d\n", pSchedEntry->QosIfIndex, pSchedEntry->QosSchedulerId);
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwSchedulerUpdateParams (tEnetHal_QoSSchedulerEntry * pSchedEntry)
{
	tSchedulerDb *pSchedDb = NULL;

	if(pSchedEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSchedulerUpdateParams ERROR: Unable to create Scheduler - NULL parameter\n");
		return ENET_FAILURE;
	}

	/* Get the index of the Scheduler in the Scheduler array according to the given ScheId*/
    if((pSchedDb = adap_getSchedulerEntry(pSchedEntry->QosIfIndex, pSchedEntry->QosSchedulerId)) == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid u4SchedId %d for port:%d not exists in DB!\n", pSchedEntry->QosSchedulerId, pSchedEntry->QosIfIndex);
        return ENET_FAILURE;
    }
    else
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_QoSHwSchedulerUpdateParams - Scheduler found for u4SchedId: %d!!! \n", pSchedEntry->QosSchedulerId);
    }

    if(adap_compareSchedulerEntry(pSchedDb, pSchedEntry) == ADAP_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Nothing to update for schedId:%d ifIndex:%d!\n", pSchedEntry->QosSchedulerId, pSchedEntry->QosIfIndex);
        return ENET_SUCCESS;
    }

	/* Update the Scheduler params given by Aricent into the local DB */
    if(adap_updateSchedulerEntry(pSchedEntry) == ENET_FAILURE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to update Scheduler data for u4SchedId %d!\n", pSchedEntry->QosSchedulerId);
        return ENET_FAILURE;
    }
    else
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwSchedulerUpdateParams - Scheduler data for u4SchedId: %d is updated\n", pSchedEntry->QosSchedulerId);
    }

    /* Executing update on Clusters */
    if (MeaDrvUpdateEthernityQoS () != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error during Qos settings update with u4SchedId %d!\n", pSchedEntry->QosSchedulerId);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwSchedulerDelete (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId)
{
    /* Get the index of the Scheduler in the Scheduler array according to the given ScheId*/
    if(adap_getSchedulerEntry(i4IfIndex, u4SchedId) == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid Scheduler Id %d  for port:%d - not exists!\n", u4SchedId, i4IfIndex);
        return ENET_FAILURE;
    }
    else
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Scheduler found for u4SchedId: %d!!! \n", u4SchedId);
    }

    /* Free the Scheduler parameters in the local DB */
    if(adap_freeSchedulerEntry (i4IfIndex, u4SchedId) != ENET_SUCCESS)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to free Scheduler:%d for port:%d!!! \n", u4SchedId, i4IfIndex);
		return ENET_FAILURE;
    }
    else
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"A Scheduler %d for port:%d is free!!! \n", u4SchedId, i4IfIndex);
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwQueueCreate (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4QId, tEnetHal_QoSQEntry * pQEntry,
                  tEnetHal_QoSQtypeEntry * pQTypeEntry,
                  tEnetHal_QoSREDCfgEntry *papRDCfgEntry[], ADAP_Int16 i2HL)
{
    tQueueDb 		*pQueueDbEntry;

    if( (pQEntry == NULL) || (pQTypeEntry == NULL))
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwQueueCreate ERROR:pQEntry(%p) or pQTypeEntry(%p) parameter is NULL!!\n",
				pQEntry, pQTypeEntry);
		return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwQueueCreate QId: %d Weight:%d\n", pQEntry->i4QosQId, pQEntry->u2QosQWeight);

    /* In Ethernity, the queues may be on the L3 only!!!     */
    if(i2HL != 3)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwQueueCreate ERROR: Queue must be on the L3 level - i2HL=%d\n", i2HL);
		return ENET_FAILURE;
    }

    /* Set the Queue parameters into the local DB */
    pQueueDbEntry = adap_createQueueEntry(i4IfIndex, pQEntry, pQTypeEntry, papRDCfgEntry);
	if(pQueueDbEntry == NULL)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwQueueCreate ERROR: Unable to create Queue in DB for QueueId: %d!!\n", u4QId);
        return ENET_FAILURE;
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwQueueCreate: Queue created in DB for Port: %d, QueueId: %d\n", i4IfIndex, u4QId);
    }

    /* Update the Ethernity HW with the new changes - do that only when all 8 queues are assigned per cluster*/
	if(adap_getNumDefinedQueuesForCluster(i4IfIndex, pQueueDbEntry->ClusterId) == ENET_PLAT_QUEUE_NUM_OF_PRI_Q)
	{
		if(MeaDrvUpdateEnetQueues (i4IfIndex, pQueueDbEntry->ClusterId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwQueueCreate ERROR: MeaDrvUpdateEthernityQoS FAIL\n");
			return ENET_FAILURE;
		}
	}

	return ENET_SUCCESS;

}

ADAP_Int32
EnetHal_QoSHwQueueDelete (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4Id)
{
	/* Get the index of the Queue in the Queue array according to the given QId*/
	if(adap_getQueueEntry (i4IfIndex,u4Id) == NULL)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid Queue Id %d  - not exists!\n", u4Id);
        return ENET_FAILURE;
    }
    else
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Queue found for QId: %d!!! \n", u4Id);
    }
		/* Reset the Queue parameters in the local DB */
    if(adap_freeQueueEntry (i4IfIndex,u4Id) != ENET_SUCCESS)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to free Queue:%d!!! \n", u4Id);
		return ENET_FAILURE;
    }
    else
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"A Queue %d is free!!! \n", u4Id);
    }

	/* Update the Ethernity HW with the new changes*/
	if(MeaDrvUpdateEthernityQoS () != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Queue: Unable to update the HW!!!\n");
		return ENET_FAILURE;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Queue parameters are updated in the HW!!! \n");
	}

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"===Exiting EnetHal_QoSHwQueueDelete for Port: %d, QId: %d\n", i4IfIndex, u4Id);
    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwMapClassToQueueId (tEnetHal_QoSClassMapEntry * pClsMapEntry, ADAP_Int32 i4IfIndex,
								ADAP_Int32 i4ClsOrPriType, ADAP_Uint32 u4ClsOrPri,
								ADAP_Uint32 u4QId, ADAP_Uint8 u1Flag)
{
	ADAP_Uint16 ClassifierIndex;
    tClassifierDb   *pClassifier;

    if(i4IfIndex == 0) //We don't need to map class for ifIndex 0
    	return ENET_SUCCESS;

    if(pClsMapEntry == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_QoSHwMapClassToQueueId: Null classifier \n");
		return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_QoSHwMapClassToQueueId - ClassId:%d PriMap:%p L2Filter:%p L3Filter:%p\n",
    		pClsMapEntry->QoSMFClass, pClsMapEntry->pPriorityMapPtr, pClsMapEntry->pL2FilterPtr, pClsMapEntry->pL3FilterPtr);

    switch (i4ClsOrPriType)
    {
        case  ADAP_QOS_QMAP_PRI_TYPE_NONE:
        case  ADAP_QOS_QMAP_PRI_TYPE_L2_FIL:
        case  ADAP_QOS_QMAP_PRI_TYPE_L3_FIL:
        {
			ClassifierIndex = adap_getClassifierIndex(pClsMapEntry->QoSMFClass);
			if(u1Flag == 1) //map
			{
				if(ClassifierIndex == ADAP_END_OF_TABLE)
				{
					ClassifierIndex = adap_createClassifierEntry (pClsMapEntry);
					if(ClassifierIndex == ADAP_END_OF_TABLE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToQueueId ERROR: Unable to create classifier for ClassId: %d!!\n", pClsMapEntry->QoSMFClass);
						return ENET_FAILURE;
					}
					else
					{
						/* Update the Classifier parameters in the local DB */
						if(adap_updateClassifierEntry (ClassifierIndex, pClsMapEntry) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to update Classifier parameters for Index: %d!!\n", ClassifierIndex);
							return ENET_FAILURE;
						}
					}
				}

				pClassifier = adap_getClassifierEntryByIndex(ClassifierIndex);
				if(pClassifier == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get Classifier for Index: %d!!!\n", ClassifierIndex);
					return ENET_FAILURE;
				}
				pClassifier->iss_PortNumber = i4IfIndex;
			}
			if(ClassifierIndex != ADAP_END_OF_TABLE)
			{
				if(adap_MapClassToQueue (i4IfIndex,ClassifierIndex, u4QId, u1Flag) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to map Class with ClassIdx: %d to Queue with Qid: %d!!\n", ClassifierIndex, u4QId);
					return ENET_FAILURE;
				}
				else
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Class with  ClassIdx: %d is mapped to Queue with Qid: %d!!\n", ClassifierIndex, u4QId);
				}
			}
        }
        break;
        default:
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unsupported type: %d\n", i4ClsOrPriType);
    }
    if(MeaDrvUpdateEthernityQoS () != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwMapClassToQueueId ERROR: MeaDrvUpdateEthernityQoS FAIL\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}


ADAP_Int32
EnetHal_QoSHwSchedulerHierarchyMap (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4SchedId, ADAP_Uint16 u2Sweight,
		ADAP_Uint8 u1Spriority, ADAP_Uint32 u4NextSchedId,
		ADAP_Uint32 u4NextQId, ADAP_Int16 i2HL, ADAP_Uint8 u1Flag)
{
    ADAP_UNUSED_PARAM (i4IfIndex);
    ADAP_UNUSED_PARAM (u4SchedId);
    ADAP_UNUSED_PARAM (u2Sweight);
    ADAP_UNUSED_PARAM (u1Spriority);
    ADAP_UNUSED_PARAM (u4NextSchedId);
    ADAP_UNUSED_PARAM (u4NextQId);
    ADAP_UNUSED_PARAM (i2HL);
    ADAP_UNUSED_PARAM (u1Flag);

	/* Map/Unmap the Hierarchy Scheduler in the local DB */
	if(u1Flag == ADAP_QOS_HIERARCHY_ADD)
	{
		if(adap_SchedHierarchyMap (i4IfIndex, u4SchedId, u4NextSchedId, i2HL, u2Sweight) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to map Hierarchy Scheduler in DPL for SchedId: %d!!\n", u4SchedId);
			return ENET_FAILURE;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Hierarchy Scheduler for SchedId: %d is mapped!!! \n", u4SchedId);
		}
	}
	else //u1Flag == ADAP_QOS_HIERARCHY_DEL
	{
		if(adap_SchedHierarchyUnMap (i4IfIndex, u4SchedId, u4NextSchedId, i2HL) != ENET_SUCCESS)
		{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to unmap Hierarchy Scheduler in DPL for SchedId: %d!!\n", u4SchedId);
				return ENET_FAILURE;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Hierarchy Scheduler for SchedId: %d is unmapped!!! \n", u4SchedId);
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwGetMeterStats (ADAP_Uint32 u4MeterId, ADAP_Uint32 u4StatsType,
		tEnetHal_COUNTER64_TYPE * pu8MeterStatsCounter)
{
    MEA_Counters_PM_dbt         PM_Entry;
    MEA_Counter_t               counter_val;
    MEA_PmId_t                  PmId[MAX_NUMBER_OF_SERVICES];
    ADAP_Uint16						idx, pmIdx = 0;
	tMeterDb 					*pMeter=NULL;

    adap_memset(pu8MeterStatsCounter, 0, sizeof (tAdap_SNMP_COUNTER64_TYPE));
    adap_memset(PmId, 0, sizeof(MEA_PmId_t) * MAX_NUMBER_OF_SERVICES);

	pMeter = MeaAdapGetMeterParams(u4MeterId);
	if(pMeter == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get PMs for MeterId: %d!!! \n", u4MeterId);
		return ENET_FAILURE;
	}

	for(idx=0;idx<MAX_NUMBER_OF_PM_PER_METER;idx++)
	{
		if(pMeter->pmDb[pmIdx].valid == ADAP_TRUE)
		{
			PmId[pmIdx++] = pMeter->pmDb[idx].pmId;
		}
	}

	counter_val.val=0;
	pmIdx = 0;

	do
	{
		adap_memset(&PM_Entry,0,sizeof(PM_Entry));

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"meterId:%d, pmID:%d, pmIdx:%d\n",u4MeterId,PmId[pmIdx], pmIdx);

		if (MEA_API_Get_Counters_PM(MEA_UNIT_0, PmId[pmIdx], &PM_Entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get statistics for PmID: %d!!! \n", PmId[pmIdx]);
			return ENET_FAILURE;
		}

		/* Fill the statistics*/
		switch(u4StatsType)
		{
			case ADAP_QOS_POLICER_STATS_CONF_PKTS:
			{

				counter_val.val += PM_Entry.green_fwd_pkts.val;
				(pu8MeterStatsCounter->lsn) =  counter_val.s.lsw;
				(pu8MeterStatsCounter->msn) =  counter_val.s.msw;


			}
			break;
			case ADAP_QOS_POLICER_STATS_CONF_OCTETS:
			{

				counter_val.val += PM_Entry.green_fwd_bytes.val;
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;
			}
			break;
			case ADAP_QOS_POLICER_STATS_EXC_PKTS:
			{

				counter_val.val  += PM_Entry.yellow_fwd_pkts.val ;
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;

			}
			break;
			case ADAP_QOS_POLICER_STATS_EXC_OCTETS:
			{
				counter_val.val  += PM_Entry.yellow_fwd_bytes.val;
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;

			}
			break;
			case ADAP_QOS_POLICER_STATS_VIO_PKTS:
			{
				counter_val.val  +=  (PM_Entry.green_dis_pkts.val  +
										 PM_Entry.yellow_dis_pkts.val +
										 PM_Entry.dis_mtu_pkts.val    +
										 PM_Entry.other_dis_pkts.val);
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;


			}
			break;
			case ADAP_QOS_POLICER_STATS_VIO_OCTETS:
			{
				counter_val.val +=  (PM_Entry.green_dis_bytes.val  +
										 PM_Entry.yellow_or_red_dis_bytes.val);
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;
			}
			break;
			default:
			{
				counter_val.val  = 0;
				(pu8MeterStatsCounter->msn) = counter_val.s.msw;
				(pu8MeterStatsCounter->lsn) = counter_val.s.lsw;
			}
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"type :%d pmId:%d enet counter %d ISS-msb:%d ISS-lsb:%d\n",
				u4StatsType,
				PmId[pmIdx],
				counter_val.val,
				pu8MeterStatsCounter->msn,
				pu8MeterStatsCounter->lsn);
		pmIdx++;
	}	while(PmId[pmIdx] != 0);

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwGetCoSQStats (ADAP_Int32 i4IfIndex, ADAP_Uint32 u4QId, ADAP_Uint32 u4StatsType,
		tEnetHal_COUNTER64_TYPE * pu8CoSQStatsCounter)
{
 	MEA_Counters_Queue_dbt 	entry;
	MEA_Port_t				enetPort;
	tQueueDb 				*pQueue=NULL;

	if(MeaAdapGetPhyPortFromLogPort(i4IfIndex,&enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",i4IfIndex);
		return ENET_FAILURE;
	}

    if(pu8CoSQStatsCounter != NULL)
    {
    	pu8CoSQStatsCounter->lsn=0;
    	pu8CoSQStatsCounter->msn=0;
    }

    switch(u4StatsType)
    {
    case ADAP_QOS_COSQ_STATS_ENQ_PKTS:
    	break;
    case ADAP_QOS_COSQ_STATS_ENQ_OCTETS:
    	break;
    case ADAP_QOS_COSQ_STATS_DEQ_PKTS:
    	pQueue = adap_getQueueEntry(i4IfIndex, u4QId);
    	if(pQueue != NULL)
    	{
			// update from API
			if(	MEA_API_Get_Counters_Queue(MEA_UNIT_0,(ENET_QueueId_t)enetPort,&entry) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MEA_API_Get_Counters_Queue cluster:%d!!! \n", enetPort);
				return ENET_FAILURE;
			}

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"u4QId = %d Queue cluster: %d  priQue: %d \n", u4QId, enetPort,pQueue->issQueue.u1QosQPriority );

			if(pQueue->issQueue.u1QosQPriority < 8)
			{
				pu8CoSQStatsCounter->lsn = entry.forward_packet[pQueue->issQueue.u1QosQPriority].s.lsw;
				pu8CoSQStatsCounter->msn = entry.forward_packet[pQueue->issQueue.u1QosQPriority].s.msw;
			}
		}
    	break;
    case ADAP_QOS_COSQ_STATS_DEQ_OCTETS:
    	break;
    case ADAP_QOS_COSQ_STATS_DISCARD_PKTS:
    	break;
    case ADAP_QOS_COSQ_STATS_DISCARD_OCTETS:
    	break;
    case ADAP_QOS_COSQ_STATS_OCCUPANCY_BYTES:
    	break;
    case ADAP_QOS_COSQ_STATS_CONG_MGNT_DROP_BYTES:
    	break;
    default:
    	break;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwSetPbitPreferenceOverDscp (ADAP_Int32 i4Port, ADAP_Int32 i4PbitPref)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t				enetPort;

	if(MeaAdapGetPhyPortFromLogPort(i4Port,&enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",i4Port);
		return ENET_FAILURE;
	}

	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSetPbitPreferenceOverDscp: MEA_API_Get_IngressPort_Entry FAIL\n");
		return ENET_FAILURE;
	}
    if(i4PbitPref)
        entry.parser_info.port_ip_aw=0;
    else
        entry.parser_info.port_ip_aw=1;

	if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSetPbitPreferenceOverDscp: MEA_API_Set_IngressPort_Entry FAIL\n");
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}


ADAP_Int32
EnetHal_QoSHwSetCpuRateLimit (ADAP_Int32 i4CpuQueueId, ADAP_Uint32 u4MinRate, ADAP_Uint32 u4MaxRate)
{
    MEA_Policer_Entry_dbt entry;

    if(MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0, 2/*policer_id*/, 0, &entry )
        != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSetCpuRateLimit: MEA_API_Get_Policer_ACM_Profile FAIL\n");
		return ENET_FAILURE;
    }

    entry.CIR = u4MaxRate*1000;

    if(MEA_API_Set_Policer_ACM_Profile(MEA_UNIT_0, 2/*policer_id*/, 0, 0, &entry )
        != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_QoSHwSetCpuRateLimit: MEA_API_Set_Policer_ACM_Profile FAIL\n");
		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_QoSHwMapClasstoPriMap (tEnetHal_QosClassToPriMapEntry * pQosClassToPriMapEntry)
{
	ADAP_Uint32 trafficType=0;
	MEA_Service_t    Service;
	MEA_Bool valid=MEA_FALSE;
	tEnetHal_VlanId VlanId;
	tServiceDb 		*pServiceId=NULL;

	if(pQosClassToPriMapEntry == NULL)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"tEnetHal_QosClassToPriMapEntry: NULL data\n");
		return ENET_FAILURE;
	}

	if((pQosClassToPriMapEntry->u1PriType != ADAP_QOS_IN_PRI_TYPE_VLAN_PRI))
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"tEnetHal_QosClassToPriMapEntry: u1PriType%d is not pri_map\n", pQosClassToPriMapEntry->u1PriType);
		return ENET_FAILURE;
	}

	VlanId = pQosClassToPriMapEntry->u2VlanId;

	for(trafficType=MEA_PARSING_L2_KEY_Untagged; trafficType<MEA_PARSING_L2_KEY_Ctag_Ctag; trafficType++)
	{
		pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,pQosClassToPriMapEntry->i4IfIndex,trafficType);
		if(pServiceId == NULL)
			continue;

		Service = pServiceId->serviceId;
		if(VlanId > 0)
		{
			if(pQosClassToPriMapEntry->u1Flag == ADAP_QOS_NP_ADD)
			{
				if(MeaDrvVlanHwCreateFilterPriorityVlan(Service,
														trafficType,
														pQosClassToPriMapEntry->i4IfIndex,
														pQosClassToPriMapEntry->u1Priority,
														VlanId,
														pQosClassToPriMapEntry->u1RegenPriority) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"tEnetHal_QosClassToPriMapEntry Error: failed to create priority service port:%d VLAN:%d priority:%d\n",
							pQosClassToPriMapEntry->i4IfIndex, VlanId,pQosClassToPriMapEntry->u1Priority);
					return ENET_FAILURE;
				}
			}
			else if(pQosClassToPriMapEntry->u1Flag == ADAP_QOS_NP_DEL)
			{
				pServiceId = MeaAdapGetServiceIdByVlanPortPri(VlanId, pQosClassToPriMapEntry->i4IfIndex, trafficType, pQosClassToPriMapEntry->u1Priority);
				if(pServiceId != NULL)
				{
					/* Check if the service exist */
					Service = pServiceId->serviceId;
					if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service,&valid) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"tEnetHal_QosClassToPriMapEntry Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service);
						return ENET_FAILURE;
					}
					if(valid == MEA_FALSE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"tEnetHal_QosClassToPriMapEntry Error: ServiceId %d does not exists\n", Service);
						return ENET_FAILURE;
					}
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete  service %d\n", Service);
					MeaDeleteEnetServiceId(Service);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service);
				}
			}
		}
	}

    return ENET_SUCCESS;
}
