#include "adap_lpminc.h"
#include "adap_lpmrb.h"
#include "adap_lpmtrie.h"
#include "adap_lpmnp.h"
#include "adap_utils.h"

tRedBlkTree     gpL3ArpTbl = NULL;
tRedBlkTree     gpL3NDTbl = NULL;
tRedBlkTree     gpFPShadowRouteTbl = NULL;
void    *gpLpmRouteTbl;        /* L3 Route Tbl */
void    *gpLpmIp6RouteTbl;        /* L3 Route Tbl */
int	gLpmAdapStatus = 0;

struct TrieDbAppFns  LpmTrieDbLibFuncs = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) LpmTrieDbAddRtEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop, tKey Key)) LpmTrieDbDeleteRtEntry,

    (INT4 (*) (tInputParams *pInputParams, VOID *pOutputParams,
                   VOID *pAppPtr)) NULL,

    (INT4 (*) (tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)) LpmTrieDbLookUpEntry,

    (VOID *(*) (tInputParams *, void (*) (VOID *),VOID *)) NULL,

    (VOID (*)  (VOID *)) NULL,

    (INT4 (*) (VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*) (VOID *, VOID *, tKey Key)) NULL,
    
    (INT4 (*) (tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams *pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) LpmTrieDbDelAllRtEntry,

    (VOID (*)(VOID *)) LpmTrieDbCbDelete,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) LpmTrieDbDelete,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*) (UINT2  u2KeySize, tInputParams * pInputParams,
               VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)) LpmTrieDbBestMatch,

    (INT4 (*)(tInputParams * , VOID *, VOID *, tKey )) NULL,

    (INT4 (*)(tInputParams * , VOID *, VOID *, UINT2 , tKey )) NULL
        
};


struct TrieDbAppFns  LpmTrieDbIp6LibFuncs = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) LpmTrieDbAddIp6RtEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop, tKey Key)) LpmTrieDbDeleteIp6RtEntry,

    (INT4 (*) (tInputParams *pInputParams, VOID *pOutputParams,
                   VOID *pAppPtr)) NULL,

    (INT4 (*) (tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)) LpmTrieDbLookUpIp6Entry,

    (VOID *(*) (tInputParams *, void (*) (VOID *),VOID *)) NULL,

    (VOID (*)  (VOID *)) NULL,

    (INT4 (*) (VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*) (VOID *, VOID *, tKey Key)) NULL,

    (INT4 (*) (tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams *pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) /*NULL,*/LpmTrieDbIp6DelAllRtEntry,

    (VOID (*)(VOID *)) LpmTrieDbIp6CbDelete,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) LpmTrieDbDelete,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*) (UINT2  u2KeySize, tInputParams * pInputParams,
               VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)) LpmTrieDbIp6BestMatch,

    (INT4 (*)(tInputParams * , VOID *, VOID *, tKey )) NULL,

    (INT4 (*)(tInputParams * , VOID *, VOID *, UINT2 , tKey )) NULL

};


/************************************************************************/
/* Routines for managing resources based on names                       */
/************************************************************************/
/************************************************************************/
/*  Function Name   : EnetUtilRscAdd                                    */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (pthread_mutex_lock (&gUtilMutex) != 0)
    {
        return (LPM_FAILURE);
    }

    /* scan global semaphore array to find a free slot */
    for (u4Idx = 1; u4Idx <= 1000; u4Idx++)
    {
        if ((gaUtilSem[u4Idx].u2Free) == TRUE)
        {
            gaUtilSem[u4Idx].u2Free = FALSE;
            gaUtilSem[u4Idx].u2Filler = 0;
            adap_memcpy(gaUtilSem[u4Idx].au1Name, au1Name,8);
            pthread_mutex_unlock (&gUtilMutex);
            return (LPM_SUCCESS);
        }
    }

    pthread_mutex_unlock (&gUtilMutex);
    return (LPM_FAILURE);
}

/************************************************************************/
/*  Function Name   : UtilRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
UtilRscDel (UINT4 u4RscType, VOID *pRscId)
{
    UINT4               u4Idx;

    if (pthread_mutex_lock (&gUtilMutex) != 0)
    {
        return;
    }

    /* scan global semaphore array to find the semaphore */
    for (u4Idx = 1; u4Idx <= 1000; u4Idx++)
    {
        if (&(gaUtilSem[u4Idx].SemId) == (sem_t *) pRscId)
        {
            gaUtilSem[u4Idx].u2Free = TRUE;
            adap_memset(gaUtilSem[u4Idx].au1Name, '\0', 8);
            break;
        }
    }
    pthread_mutex_unlock (&gUtilMutex);
}

/************************************************************************/
/*  Function Name   : UtilRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Util Resource-Id                       */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilRscFind (UINT1 au1Name[], UINT4 u4RscType, VOID **pRscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (LPM_FAILURE);
    }
    if (pthread_mutex_lock (&gUtilMutex) != 0)
    {
        return (LPM_FAILURE);
    }

    /* scan global semaphore array to find the semaphore */
    for (u4Idx = 1; u4Idx <= 1000; u4Idx++)
    {
        if (adap_memcmp(au1Name, gaUtilSem[u4Idx].au1Name, 8) == 0)
        {
            /* pThread version of UtilRscFind returns pointer to semId */
            *pRscId = (void *) &gaUtilSem[u4Idx].SemId;
            pthread_mutex_unlock (&gUtilMutex);
            return (LPM_SUCCESS);
        }
    }
    pthread_mutex_unlock (&gUtilMutex);
    return (LPM_FAILURE);
}


/************************************************************************/
/*  Function Name   : UtilInitialize                                    */
/*  Description     : The  Initialization routine. To be called     */
/*                    before any other  functions are used.         */
/*  Input(s)        : pUtilCfg - Pointer to  config info.           */
/*  Output(s)       : None                                              */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilInitialize ()
{
    UINT4               u4Idx;

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a pThreads */
    /* specific call and must be mapped to relevant call for OS  */

    if (pthread_mutex_init (&gUtilMutex, NULL))
    {
        return (LPM_FAILURE);
    }
    for (u4Idx = 0; u4Idx <= 1000; u4Idx++)
    {
        gaUtilSem[u4Idx].u2Free = TRUE;
        gaUtilSem[u4Idx].u2Filler = 0;
        adap_memset(gaUtilSem[u4Idx].au1Name, '\0', 8);
    }
    return (LPM_SUCCESS);
}


/************************************************************************/
/* Routines for managing semaphores                                     */
/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/

/************************************************************************
 *  Function Name   : UtilCreateSem
 *  Description     : This creates a sema4 of a given name.
 *  Input           : au1SemName  - Name of sema4.
 *                    u4InitialCount - Initial value of sema4.
 *                    u4Flags        - Unused.
 *  Output          : pSemId - Pointer to memory which contains SEM-ID
 *  Returns         : LPM_SUCCESS/LPM_FAILURE.
 ************************************************************************/
UINT4
UtilCreateSem (const UINT1 au1SemName[4], UINT4 u4InitialCount,
               UINT4 u4Flags, tUtilSemId * pSemId)
{
    UINT1               au1Name[8], u1Index;

    adap_memset((UINT1 *) au1Name, '\0', 8);
    for (u1Index = 0;
         ((u1Index < 4) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    ((VOID) u4Flags);

    if (UtilRscFind (au1Name, 0, (VOID **) pSemId) == LPM_SUCCESS)
    {
        /* Semaphore by this name already exists. */
        return (LPM_FAILURE);
    }
    if (UtilSemCrt (au1Name, pSemId) == LPM_FAILURE)
    {
        return (LPM_FAILURE);
    }
    /* Implementation assumes that mutex is created when u4InitialCount */
    /* is 1, otherwise sem is for task sync. UtilSemCrt creates binary  */
    /* sem in blocked state. So, if u4InitialCount is 1, give the sem.  */
    if (u4InitialCount == 1)
    {
        UtilSemGive (*pSemId);
    }
    return (LPM_SUCCESS);
}

/************************************************************************/
/*  Function Name   : UtilSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilSemCrt (UINT1 au1SemName[], tUtilSemId * pSemId)
{
    UINT1               au1Name[8];
    VOID               *pId = NULL;

    adap_memset((UINT1 *) au1Name, '\0', 8);
    adap_memcpy(au1Name, au1SemName, 4);

    /* For sem, the pThreads version of UtilRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0. */

    pId = pSemId;
    if (UtilRscFind (au1Name, 0, pId) == LPM_SUCCESS)
    {
        /* Semaphore by this name already exists. */
        return (LPM_FAILURE);
    }
    if (UtilRscAdd (au1Name, 0, NULL) == LPM_FAILURE)
    {
        return (LPM_FAILURE);
    }

    /* the pThread version of UtilRscFind returns pointer to void pointer,
     * hence assigned the semId pointer to void pointer to remove dereferncing 
     * type-punned warning */
    pId = pSemId;
    UtilRscFind (au1Name, 0, pId);
    if (sem_init (*pSemId, 0, 0))
    {
        UtilRscDel (0, (VOID *) *pSemId);
        return (LPM_FAILURE);
    }
    return (LPM_SUCCESS);
}

/************************************************************************/
/*  Function Name   : UtilSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
UtilSemDel (tUtilSemId SemId)
{
    UtilRscDel (0, (VOID *) SemId);
    sem_destroy (SemId);
}

/************************************************************************/
/*  Function Name   : UtilSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilSemGive (tUtilSemId SemId)
{
    if (sem_post ((sem_t *) SemId) < 0)
    {
        return (LPM_FAILURE);
    }
    return (LPM_SUCCESS);
}

/************************************************************************/
/*  Function Name   : UtilSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : LPM_SUCCESS/LPM_FAILURE                         */
/************************************************************************/
UINT4
UtilSemTake (tUtilSemId SemId)
{
    UINT4               u4RetVal = LPM_SUCCESS;

    while (sem_wait ((sem_t *) SemId) != 0)
    {
        if (errno != EINTR)
        {
            u4RetVal = LPM_FAILURE;
            assert (0);
        }
    }

    return (u4RetVal);
}

int
LpmInitialize (void)
{
    tTrieDbCrtParams      TrieDbCrtParams;
    tTrieDbCrtParams      TrieDbCrtParamsIp6;

    if (UtilInitialize () == LPM_FAILURE)
    {
        log ("Initialization of mutex, semaphore failed\r\n");
        return LPM_FAILURE;
    }

    if (RedBlkTreeLibInit () == LPM_FAILURE)
    {
        log ("Initialization of RedBlkTree failed\r\n");
        return LPM_FAILURE;
    }

    if (TrieDbLibInit () == LPM_FAILURE)
    {
        log ("Initialization of TrieDb failed\r\n");
        return LPM_FAILURE;
    }
    /* Create L3 ARP table */
    gpL3ArpTbl = RedBlkTreeCreateEmbedded 
                (UTIL_OFFSETOF (tArpEntry, RbNode), LpmL3ArpTblCmp);
    if (gpL3ArpTbl == NULL)
    {
        log ("L3 Arp Table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create L3 ND table */
    gpL3NDTbl = RedBlkTreeCreateEmbedded 
                (UTIL_OFFSETOF (tNDEntry, RbNode), LpmL3NDTblCmp);
    if (gpL3NDTbl == NULL)
    {
        log ("L3 ND Table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create FPGA Shadow route table */
    gpFPShadowRouteTbl = RedBlkTreeCreateEmbedded 
                (UTIL_OFFSETOF (tFPRouteEntry, RbNode), LpmFPShadowRouteTblCmp);
    if (gpFPShadowRouteTbl == NULL)
    {
        log ("FPGA Shadow Route table RedBlkTree creation failed\n");
        return LPM_FAILURE;
    }

    /* Create ipv4 route table */
    adap_memset(&TrieDbCrtParams, 0, sizeof (tTrieDbCrtParams));
    TrieDbCrtParams.u2KeySize = 2 * sizeof (UINT4);
    TrieDbCrtParams.u4Type = 0;
    TrieDbCrtParams.AppFns = &(LpmTrieDbLibFuncs);
    TrieDbCrtParams.u1AppId = LPM_TRIE_APP_ID;
    TrieDbCrtParams.u4NumRadixNodes = LPM_MAX_ROUTES + 1;
    TrieDbCrtParams.u4NumLeafNodes = LPM_MAX_ROUTES;
    TrieDbCrtParams.u4NoofRoutes = LPM_MAX_ROUTES;
    TrieDbCrtParams.bPoolPerInst = TRUE;
    TrieDbCrtParams.bSemPerInst = TRUE;
    TrieDbCrtParams.bValidateType = FALSE;

    gpLpmRouteTbl = TrieDbCreateInstance (&TrieDbCrtParams);
    if (gpLpmRouteTbl == NULL)
    {
        log ("TrieDb creation for IPv4 LPM route table failed \r\n");
        return LPM_FAILURE;
    }

    /* IPv6 Route table creation */
    adap_memset(&TrieDbCrtParamsIp6, 0, sizeof (tTrieDbCrtParams));
    TrieDbCrtParamsIp6.u2KeySize = 16 * sizeof (UINT2);
    TrieDbCrtParamsIp6.u4Type = 0;
    TrieDbCrtParamsIp6.AppFns = &(LpmTrieDbIp6LibFuncs);
    TrieDbCrtParamsIp6.u1AppId = LPM_TRIE_APP_IP6_ID;
    TrieDbCrtParamsIp6.u4NumRadixNodes = LPM_MAX_ROUTES;
    TrieDbCrtParamsIp6.u4NumLeafNodes = LPM_MAX_ROUTES;
    TrieDbCrtParamsIp6.u4NoofRoutes = LPM_MAX_ROUTES;
    TrieDbCrtParamsIp6.bPoolPerInst = TRUE;
    TrieDbCrtParamsIp6.bSemPerInst = TRUE;
    TrieDbCrtParamsIp6.bValidateType = FALSE;

    gpLpmIp6RouteTbl = TrieDbCreateInstance (&TrieDbCrtParamsIp6);
    if (gpLpmIp6RouteTbl == NULL)
    {
        log ("TrieDb creation for IPv6 LPM route table failed \r\n");
        return (LPM_FAILURE);
    }

    return LPM_SUCCESS;
}

INT4
LpmL3ArpTblCmp (tRedBlkElem * e1, tRedBlkElem * e2)
{
    tArpEntry         *pArp = NULL;
    tArpEntry         *pArpIn = NULL;

    pArp = (tArpEntry *) e1;
    pArpIn = (tArpEntry *) e2;

    if (pArp->u4VrId < pArpIn->u4VrId)
        return (-1);
    else if (pArp->u4VrId > pArpIn->u4VrId)
        return (1);

    if (pArp->DstIp < pArpIn->DstIp)
        return (-1);
    else if (pArp->DstIp > pArpIn->DstIp)
        return (1);

    return LPM_SUCCESS;
}

INT4
LpmL3NDTblCmp (tRedBlkElem * e1, tRedBlkElem * e2)
{
    tNDEntry      *pE1 = (tNDEntry *) e1;
    tNDEntry      *pE2 = (tNDEntry *) e2;
    UINT1          u1Ip6Addr1[16];
    UINT1          u1Ip6Addr2[16];
    INT4           i4ret = 0;

    adap_memset(&u1Ip6Addr1, 0, 16);
    adap_memset(&u1Ip6Addr2, 0, 16);

    adap_memcpy(u1Ip6Addr1, pE1->DstIp6.u1_addr, 16);
    adap_memcpy(u1Ip6Addr2, pE2->DstIp6.u1_addr, 16);


    if (pE2->u4VrId > pE1->u4VrId)
        return (1);
    else if (pE2->u4VrId < pE1->u4VrId)
        return (-1);
    else
    {
        i4ret = adap_memcmp(u1Ip6Addr1, u1Ip6Addr2, 16);
        return (i4ret);
    }
}

INT4
LpmFPShadowRouteTblCmp (tRedBlkElem * e1, tRedBlkElem * e2)
{
    tFPRouteEntry         *pRoute = NULL;
    tFPRouteEntry         *pRouteIn = NULL;

    pRoute = (tFPRouteEntry *) e1;
    pRouteIn = (tFPRouteEntry *) e2;

    if (pRoute->u4VrId < pRouteIn->u4VrId)
        return (-1);
    else if (pRoute->u4VrId > pRouteIn->u4VrId)
        return (1);

    if (pRoute->u4DestAddr < pRouteIn->u4DestAddr)
        return (-1);
    else if (pRoute->u4DestAddr > pRouteIn->u4DestAddr)
        return (1);

    return LPM_SUCCESS;
}


INT4
LpmTrieDbAddRtEntry (VOID *pInputParams, VOID *pOutputParams,
                     tNextHopInfo ** ppAppSpecInfo,
                     tNextHopInfo * pNewAppSpecInfo)
{
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbDeleteRtEntry (VOID *pInputParams, tNextHopInfo ** ppAppSpecInfo,
                        VOID *pOutputParams, VOID *pNextHop, tKey Key)
{

    (*ppAppSpecInfo) = NULL;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbLookUpEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams,
                      VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                    VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return LPM_SUCCESS;
}

UINT1              *
LpmTrieDbDelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                        tOutputParams * pOutputParams)
{
    UINT1              *pInpRtInfo;

    pInpRtInfo = (UINT1 *) pInputParams;
    return (pInpRtInfo);
}

VOID
LpmTrieDbCbDelete (VOID *pInput)
{
    return;
}

INT4
LpmTrieDbDelete (VOID *pInputParams, VOID **ppAppSpecPtr, tKey Key)
{
    return LPM_SUCCESS;
}


/* Ipv6 TrieDb Functions */
INT4
LpmTrieDbAddIp6RtEntry (VOID *pInputParams, VOID *pOutputParams,
                        tIpv6NextHop ** ppAppSpecInfo,
                        tIpv6NextHop * pNewAppSpecInfo)
{
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbDeleteIp6RtEntry (VOID *pInputParams,
                           tIpv6NextHop ** ppAppSpecInfo,
                           VOID *pOutputParams, VOID *pNextHop, tKey Key)
{
    (*ppAppSpecInfo) = NULL;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbLookUpIp6Entry (tInputParams * pInputParams,
                         tOutputParams * pOutputParams,
                         VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return LPM_SUCCESS;
}

INT4
LpmTrieDbIp6BestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                       VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return LPM_SUCCESS;
}

UINT1              *
LpmTrieDbIp6DelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                           tOutputParams * pOutputParams)
{
    UINT1              *pInpRtInfo;
    pInpRtInfo = (UINT1 *) pInputParams;
    return (pInpRtInfo);
}

VOID
LpmTrieDbIp6CbDelete (VOID *pInput)
{
    return;
}

VOID
UtilIpv6CopyAddrBits (tIp6Address * pPref, tIp6Address * pAddr, INT4 i4NumBits)
{

    INT4                i4Dw = 0, i4_set_bit, i4Bit;
    UINT4               u4B1 = 0, u4Mask = 0;

    adap_memset(pPref, 0, sizeof (tIp6Address));

    for (i4_set_bit = 0; i4_set_bit < i4NumBits; i4_set_bit++)
    {
        i4Dw = u4B1 = u4Mask = 0;
        i4Bit = i4_set_bit;

        i4Dw = i4Bit >> 0x05;

        u4B1 = pAddr->u4_addr[i4Dw];
        i4Bit = ~i4Bit;
        i4Bit &= 0x1f;
        u4Mask = UTIL_HTONL (1 << i4Bit);

        pPref->u4_addr[i4Dw] |= (u4B1 & u4Mask);
    }
}

