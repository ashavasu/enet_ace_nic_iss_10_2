/*
 * adap_rbtree.c
 */

#include "adap_rbtree.h"
#include "adap_utils.h"

struct adap_rbtree {
	adap_rbtree_element *root;
	int (*compare)(adap_rbtree_element* element1, adap_rbtree_element* element2);
	void (*swap)(adap_rbtree_element* element1, adap_rbtree_element* element2);

	ADAP_Uint32 capacity;
	ADAP_Uint32 size;
};

static adap_rbtree_element sentinel_null = {.red = ADAP_FALSE};
#define RBNULL (&sentinel_null)

EnetHal_Status_t adap_rbtree_init(adap_rbtree** tree, struct adap_rbtree_params *params)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	adap_rbtree * rbtree = NULL;

	if ((params == NULL) || (tree == NULL)) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	rbtree = (adap_rbtree *)adap_malloc(sizeof(adap_rbtree));
	if (rbtree == NULL) {
		rc = ENETHAL_STATUS_MEM_ALLOC;
		goto bail;
	}

	rbtree->root = RBNULL;
	rbtree->capacity = params->capacity;
	rbtree->compare = params->compare;
	rbtree->swap = params->swap;
	rbtree->size = 0;

	*tree = rbtree;
	rbtree = NULL;

bail:
	adap_safe_free(rbtree);

	return rc;
}

EnetHal_Status_t adap_rbtree_deinit(adap_rbtree* tree)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;

	if (tree == NULL) {
		rc = ENETHAL_STATUS_NULL_PARAM;
		goto bail;
	}

	if (tree->size != 0) {
		rc = ENETHAL_STATUS_NOT_SUPPORTED;
		goto bail;
	}

	adap_safe_free(tree);

bail:
	return rc;
}


/* =========  RB-tree Utils  ============ */
#if 0 /* TODO - open when debug is needed */
static
void rbtree_print(adap_rbtree_element *node)
{
	if (node == RBNULL) {
		return;
	}

	rbtree_print(node->left);
	printf("Node %p with C=%d, P=%p, L=%p, R=%p\n", node, node->red, node->parent, node->left, node->right);
	rbtree_print(node->right);
}

static
ADAP_Bool rbtree_check_reds(adap_rbtree_element *node)
{
	if (node == RBNULL) {
		return ADAP_TRUE;
	}

	if (node->red == ADAP_TRUE) {
		if ((node->left->red != ADAP_FALSE) || (node->right->red != ADAP_FALSE)) {
			return ADAP_FALSE;
		}
	}

	return rbtree_check_reds(node->left) && rbtree_check_reds(node->right);
}

static
ADAP_Bool rbtree_validate(adap_rbtree* tree)
{
	if (tree == NULL) {
		printf("Tree is NULL!!\n");
		return ADAP_FALSE;
	}

	if (tree->root == RBNULL) {
		/* empty tree */
		return ADAP_TRUE;
	}

	if (RBNULL->red != ADAP_FALSE) {
		printf("Null node is not Black!!\n");
		return ADAP_FALSE;
	}

	if (tree->root->parent != RBNULL) {
		printf("Root parent (%p) is not RBNULL (%p)!!\n", tree->root->parent, RBNULL);
		return ADAP_FALSE;
	}

	if (tree->root->red != ADAP_FALSE) {
		printf("Root is not Black!!\n");
		return ADAP_FALSE;
	}

	if (rbtree_check_reds(tree->root) != ADAP_TRUE) {
		printf("A red node with red child!!\n");
		return ADAP_FALSE;
	}

//	if (rbtree_check_paths(tree->root) != ADAP_TRUE) {
//		printf("Tree is not balanced!!\n");
//		return ADAP_FALSE;
//	}

	return ADAP_TRUE;
}
#endif

static
void rotate_left(adap_rbtree* tree, adap_rbtree_element *node)
{
	adap_rbtree_element *y;

	if (node->right != RBNULL) {
		y = node->right;
		node->right = y->left;
		if (y->left != RBNULL) {
			y->left->parent = node;
		}
		y->parent = node->parent;
		if (node->parent == RBNULL) {
			tree->root = y;
		} else if (node == node->parent->left) {
			node->parent->left = y;
		} else {
			node->parent->right = y;
		}

		y->left = node;
		node->parent = y;
	}
}

static
void rotate_right(adap_rbtree* tree, adap_rbtree_element *node)
{
	adap_rbtree_element *y;

	if (node->left != RBNULL) {
		y = node->left;
		node->left = y->right;
		if (y->right != RBNULL) {
			y->right->parent = node;
		}
		y->parent = node->parent;
		if (node->parent == RBNULL) {
			tree->root = y;
		} else if (node == node->parent->right) {
			node->parent->right = y;
		} else {
			node->parent->left = y;
		}

		y->right = node;
		node->parent = y;
	}
}

static
void deletefix(adap_rbtree* tree, adap_rbtree_element *node)
{
	adap_rbtree_element *w;

    while ((node != tree->root) && (node->red == ADAP_FALSE)) {
        if (node == node->parent->left) {
            w = node->parent->right;
            if (w->red == ADAP_TRUE) {
            	w->red = ADAP_FALSE;
            	node->parent->red = ADAP_TRUE;
                rotate_left(tree, node->parent);
                w = node->parent->right;
            }

            if ((w->left->red == ADAP_FALSE) && (w->right->red == ADAP_FALSE)) {
            	w->red = ADAP_TRUE;
                node = node->parent;
            } else {
                if (w->right->red == ADAP_FALSE) {
                	w->left->red = ADAP_FALSE;
                	w->red = ADAP_TRUE;
                    rotate_right(tree, w);
                    w = node->parent->right;
                }
                w->red = node->parent->red;
                node->parent->red = ADAP_FALSE;
                w->right->red = ADAP_FALSE;
                rotate_left(tree, node->parent);
                node = tree->root;
            }
        } else {
            w = node->parent->left;
            if (w->red == ADAP_TRUE) {
            	w->red = ADAP_FALSE;
            	node->parent->red = ADAP_TRUE;
                rotate_right(tree, node->parent);
                w = node->parent->left;
            }

            if ((w->right->red == ADAP_FALSE) && (w->left->red == ADAP_FALSE)) {
            	w->red = ADAP_TRUE;
                node = node->parent;
            } else {
                if (w->left->red == ADAP_FALSE) {
                	w->right->red = ADAP_FALSE;
                	w->red = ADAP_TRUE;
                    rotate_left(tree, w);
                    w = node->parent->left;
                }
                w->red = node->parent->red;
                node->parent->red = ADAP_FALSE;
                w->left->red = ADAP_FALSE;
                rotate_right(tree, node->parent);
                node = tree->root;
            }
        }
    }
    node->red = ADAP_FALSE;
}
/* =========  RB-tree Utils  ============ */

EnetHal_Status_t adap_rbtree_insert(adap_rbtree* tree, adap_rbtree_element* element)
{
	adap_rbtree_element *node;
	adap_rbtree_element *y;
	int comp;

	if ((tree == NULL) || (element == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

	if ((tree->capacity > 0) && (tree->size == tree->capacity)) {
		return ENETHAL_STATUS_LIMIT_REACHED;
	}

	element->left = RBNULL;
	element->right = RBNULL;
	element->parent = RBNULL;
	element->red = ADAP_TRUE;

	node = tree->root;
	if (node == RBNULL) {
		tree->root = element;
		element->red = ADAP_FALSE;
		tree->size++;
		return ENETHAL_STATUS_SUCCESS;
	}

	while (node != RBNULL) {
		comp = tree->compare(node, element);
		if (comp == 0) {
			return ENETHAL_STATUS_ALREADY_FOUND;
		}
		element->parent = node;
		if (comp < 0) {
			node = node->right;
		} else {
			node = node->left;
		}
	}

	if (comp < 0) {
		element->parent->right = element;
	} else {
		element->parent->left = element;
	}

	while ((element != tree->root) && (element->parent->red == ADAP_TRUE)) {
		if (element->parent == element->parent->parent->left) {
			y = element->parent->parent->right;
			if (y->red == ADAP_TRUE) {
				element->parent->red = ADAP_FALSE;
				y->red = ADAP_FALSE;
				element->parent->parent->red = ADAP_TRUE;
				element = element->parent->parent;
			} else {
				if (element == element->parent->right) {
					element = element->parent;
					rotate_left(tree, element);
				}
				element->parent->red = ADAP_FALSE;
				element->parent->parent->red = ADAP_TRUE;
				rotate_right(tree, element->parent->parent);
			}
		} else {
			y = element->parent->parent->left;
			if (y->red == ADAP_TRUE) {
				element->parent->red = ADAP_FALSE;
				y->red = ADAP_FALSE;
				element->parent->parent->red = ADAP_TRUE;
				element = element->parent->parent;
			} else {
				if (element == element->parent->left) {
					element = element->parent;
					rotate_right(tree, element);
				}
				element->parent->red = ADAP_FALSE;
				element->parent->parent->red = ADAP_TRUE;
				rotate_left(tree, element->parent->parent);
			}
		}
	}

	tree->root->red = ADAP_FALSE;

	tree->size++;

	return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_rbtree_remove(adap_rbtree* tree, adap_rbtree_element *c_element, adap_rbtree_element **element)
{
	adap_rbtree_element *node;
	adap_rbtree_element *y;
	adap_rbtree_element *x;
	int comp;

	if ((tree == NULL) || (c_element == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

	node = tree->root;
	while (node != RBNULL) {
		comp = tree->compare(node, c_element);
		if (comp == 0) {
			break;
		} else if (comp < 0) {
			node = node->right;
		} else {
			node = node->left;
		}
	}

	if (node == RBNULL) {
		return ENETHAL_STATUS_NOT_FOUND;
	}

    if ((node->left != RBNULL) && (node->right != RBNULL)) {
    	y = node->right;
    	while (y->left != RBNULL) {
    		y = y->left;
    	}
    } else {
    	y = node;
    }
    adap_assert((y->left == RBNULL) || (y->right == RBNULL));

    if (y->left != RBNULL) {
    	x = y->left;
    } else {
    	x = y->right;
    }

   	x->parent = y->parent;

    if (y->parent == RBNULL) {
    	tree->root = x;
    	tree->root->parent = RBNULL;
    } else if (y == y->parent->left) {
    	y->parent->left = x;
    } else {
    	y->parent->right = x;
    }

    if (node != y) {
    	tree->swap(node, y);
    	/* TODO - copy more fields? */
    }

    if (y->red == ADAP_FALSE) {
    	deletefix(tree, x);
    }

    if (element != NULL) {
    	*element = y;
    }

	tree->size--;

	return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_rbtree_find(adap_rbtree* tree, adap_rbtree_element *c_element, adap_rbtree_element **element)
{
	int comp = 0;
	adap_rbtree_element *node;

	if ((tree == NULL) || (c_element == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

	node = tree->root;
	while (node != RBNULL) {
		comp = tree->compare(node, c_element);
		if (comp == 0) {
			break;
		} else if (comp < 0) {
			node = node->right;
		} else {
			node = node->left;
		}
	}

	if (node == RBNULL) {
		return ENETHAL_STATUS_NOT_FOUND;
	}

	if (element != NULL) {
		*element = node;
	}

	return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_rbtree_iter(adap_rbtree* tree, adap_rbtree_element *c_element, adap_rbtree_element **element)
{
	adap_rbtree_element *parent;
	int comp;

	if ((tree == NULL) || (element == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

	if (tree->root == RBNULL) {
		return ENETHAL_STATUS_NOT_FOUND;
	}

	*element = tree->root;
	if (c_element == NULL) {
		while ((*element)->left != RBNULL) {
			*element = (*element)->left;
		}
	} else {
		while (*element != RBNULL) {
			parent = *element;
			comp = tree->compare(c_element, *element);
			if (comp == 0) {
				break;
			} else if (comp < 0) {
				*element = (*element)->left;
			} else {
				*element = (*element)->right;
			}
		}

		if (*element == RBNULL) {
			*element = parent;
			comp = tree->compare(c_element, *element);
			if (comp < 0) {
				return ENETHAL_STATUS_SUCCESS;
			}
		}

		if ((*element)->right != RBNULL) {
			*element = (*element)->right;
			while ((*element)->left != RBNULL) {
				*element = (*element)->left;
			}
		} else {
			if (tree->root == *element) {
				return ENETHAL_STATUS_NOT_FOUND;
			}
			while ((*element) == (*element)->parent->right)	{
				*element = (*element)->parent;
				if (tree->root == *element) {
					return ENETHAL_STATUS_NOT_FOUND;
				}
			}
			*element = (*element)->parent;
		}
	}

	return ENETHAL_STATUS_SUCCESS;
}

ADAP_Uint32 adap_rbtree_size(adap_rbtree* tree)
{
	if (tree == NULL) {
		return 0;
	}

	return tree->size;
}

