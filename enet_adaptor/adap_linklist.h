/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_LINKLIST_H
#define ADAP_LINKLIST_H


struct accListNode
{
  void 						*data;
  struct accListNode 		*next;
};

typedef ADAP_Uint32 (*comp_key_func)( void *data1, void *data2);
typedef void (*func_to_delete)( void *data);

typedef struct
{
	struct accListNode				*head;
	comp_key_func					compKey;
}AdapLinkList;

void ADAP_initLinkList(AdapLinkList *pNode,comp_key_func Compfunc);
ADAP_Uint32 ADAP_pushLinkList(AdapLinkList *pNode,void *data);
ADAP_Uint32 ADAP_pushLinkListBeforeCompKey(AdapLinkList *pNode, void *data/*data to enter*/,
        comp_key_func compKey/*if return is >0 data1 > data2*/);
void ADAP_popLinkList(AdapLinkList *pNode,void *dataKey,void **data);
void ADAP_getDataLinkList(AdapLinkList *pNode,void *dataKey,void **data);
void *ADAP_getFirstDataLinkList(AdapLinkList *pNode);
void *ADAP_getNextDataLinkList(AdapLinkList *pNode,void *data);
void ADAP_deleteAllDataLinkList(AdapLinkList *pNode,func_to_delete func);
#endif
