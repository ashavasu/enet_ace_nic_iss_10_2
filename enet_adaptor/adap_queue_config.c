/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "enet_queue_drv.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_queue_config.h"

MEA_Status MeaDeleteCluster(ENET_QueueId_t  queueId)
{
	return ENET_Delete_Queue(MEA_UNIT_0,queueId);
}

MEA_Status MeaCreateCluster(char *name,MEA_Port_t 	enetPort,ENET_QueueId_t  *queueId)
{
	ENET_Queue_dbt queue;
	MEA_Globals_Entry_dbt globals_entry;
	MEA_Uint32 PriQIdx;

	adap_memset(&queue,0,sizeof(queue));
	MEA_OS_strcpy(queue.name,ENET_PLAT_GENERATE_NEW_NAME);
	queue.port.type              = ENET_QUEUE_PORT_TYPE_PORT;
	queue.port.id.port           = enetPort;
	queue.adminOn                = MEA_TRUE;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK)
	{
		printf("MEA_API_Get_Globals_Entry for port: %d Cluster!", enetPort);
		return MEA_ERROR;
	}

	if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY)
	{
		queue.mode.type             = ENET_QUEUE_MODE_TYPE_RR;
		queue.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL;
	}
	else
	{
		queue.mode.type    = ENET_QUEUE_MODE_TYPE_WFQ;
		queue.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
	}

	queue.MTU                    = MEA_QUEUE_CLUSTER_MTU_DEF_VAL;
	queue.shaper_enable          = MEA_FALSE;
	queue.Shaper_compensation    = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	queue.groupId                = 0;
	adap_memset(&queue.shaper_info,0,sizeof(queue.shaper_info));

	for(PriQIdx=0;PriQIdx<MEA_NUM_OF_ELEMENTS(queue.pri_queues);PriQIdx++)
	{
		queue.pri_queues[PriQIdx].max_q_size_Packets			= ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(PriQIdx);
		queue.pri_queues[PriQIdx].max_q_size_Byte				= ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(PriQIdx);
		queue.pri_queues[PriQIdx].mtu							= MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
		if(MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY)
		{
			queue.pri_queues[PriQIdx].mode.type					= ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
			queue.pri_queues[PriQIdx].mode.value.strict_priority	= ENET_PQ_STRICT_DEF_VAL;
		}
		else
		{
			if(MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY){
			queue.pri_queues[PriQIdx].mode.type					= ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
			queue.pri_queues[PriQIdx].mode.value.strict_priority	= ENET_PQ_RR_DEF_VAL;
			}
			else{
				queue.pri_queues[PriQIdx].mode.type					    = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
				queue.pri_queues[PriQIdx].mode.value.strict_priority	= ENET_PQ_RR_DEF_VAL;

			}
		}
		queue.pri_queues[PriQIdx].mc_MQS                      = ENET_QUEUE_MC_MQS_DEF_VAL;
	}
	//(*queueId)= ENET_PLAT_GENERATE_NEW_ID;
	if(ENET_Create_Queue (MEA_UNIT_0, &queue, queueId) != ENET_OK)
	{
		return MEA_ERROR;
	}
	return MEA_OK;
 }
 
 MEA_Status MeaSetCluster_Shaper(ENET_QueueId_t  queueId,MEA_Bool enable,ADAP_Uint32 cir,ADAP_Uint32 cbs)
 {
	 ENET_Queue_dbt         entry;
	 
	if(ENET_Get_Queue(MEA_UNIT_0,queueId,&entry) != ENET_OK)
	{
		return MEA_ERROR;
	}
	
	entry.shaper_enable = enable;
	entry.shaper_info.CIR = cir;
	entry.shaper_info.CBS = cbs;
	entry.shaper_info.resolution_type  = ENET_SHAPER_RESOLUTION_TYPE_BIT;
	entry.shaper_info.overhead = 0;
	entry.shaper_info.Cell_Overhead = 0;	
	 
	if(ENET_Set_Queue(MEA_UNIT_0,queueId,&entry) != ENET_OK)
	{
		return MEA_ERROR;
	}
	return MEA_OK;
 }

 MEA_Status MeaSetCluster_PriQShaper(ENET_QueueId_t  queueId,ADAP_Uint32 PriQIdx,MEA_Bool enable, ADAP_Uint32 cir,ADAP_Uint32 cbs)
 {
	 ENET_Queue_dbt         entry;

	if(PriQIdx > MEA_NUM_OF_ELEMENTS(entry.pri_queues)){
	 printf("PriQIdx is invalid: %d!", PriQIdx);
		return MEA_ERROR;
	}

	if(ENET_Get_Queue(MEA_UNIT_0,queueId,&entry) != ENET_OK)
	{
		printf("PriQIdx is invalid: %d!", PriQIdx);
		return MEA_ERROR;
	}


		entry.pri_queues[PriQIdx].shaperPri_enable = enable;
		entry.pri_queues[PriQIdx].shaper_info.CIR = cir;
		entry.pri_queues[PriQIdx].shaper_info.CBS = cbs;
		entry.pri_queues[PriQIdx].shaper_info.Cell_Overhead = 0;
		entry.pri_queues[PriQIdx].shaper_info.overhead = 0;
		entry.pri_queues[PriQIdx].shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
		entry.pri_queues[PriQIdx].Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;


	if(ENET_Set_Queue(MEA_UNIT_0,queueId,&entry) != ENET_OK)
	{
		return MEA_ERROR;
	}
	return MEA_OK;
 }

