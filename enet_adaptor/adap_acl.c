/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#include "mea_api.h"
#include "unistd.h"
#include "pthread.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_acl.h"
#include "adap_enetConvert.h"
#include "adap_service_config.h"
#include "EnetHal_L2_Api.h"

#include <sys/queue.h>

#define ACL_TABLE_ID 0
#define EMPTY_VLAN_PRIORITY (ADAP_Uint16)(-1)
#define EMPTY_POLICING_PROFILE (ADAP_Uint16)(-1)
#define MAX_NUM_OF_FILTERS          256

struct tAclServiceEntry
{
    MEA_Service_t serviceId;
    ADAP_Uint32 vlanId;
    ADAP_Uint32 trafficType;
    MEA_Bool serviceCreated;
};

struct tVlanSubProtocolEntry
{
    ADAP_Uint32 vlanId;
    ADAP_Uint32 subProtocolType;
    ADAP_Uint32 L2ProtocolType;
    LIST_ENTRY(tVlanSubProtocolEntry) entries;
};

struct slFiltParams{
    tAclFilterParams tAclParams;
    tUpdateServiceParam tServiceParams;
    tAdapAclFilterType tAclType;
    eEnetHal_FilterAction FilterAction;
    tPolicyDb policy;
    ADAP_Uint32 filterId;
    eAclFilterTypes ftype;
    ADAP_Uint32 inPort;
    STAILQ_ENTRY(slFiltParams) entries;
};

typedef STAILQ_HEAD(slFiltParamsHeadStruct, slFiltParams) slFiltParamsHead;

struct slServiceList{
    MEA_Service_t serviceId;
    slFiltParamsHead filtParams;
    STAILQ_ENTRY(slServiceList) entries;
};

typedef STAILQ_HEAD(slServiceListHeadStruct, slServiceList) slServiceListHead;

MEA_Status MeaDrvSetGlobalMacAddress(tEnetHal_MacAddr *macAddr,MEA_Bool addNewMac)
{
	MEA_Globals_Entry_dbt entry;
	ADAP_Uint32 idx;
	MEA_Bool found=MEA_FALSE;

	adap_memset(&entry,0,sizeof(MEA_Globals_Entry_dbt));

	if(MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed!\n");
		return MEA_ERROR;
	}

	if(addNewMac == MEA_TRUE)
	{
		for(idx=0;idx<MEA_MAX_LOCAL_MAC;idx++)
		{
			if(adap_is_mac_equal(macAddr, entry.Local_Mac_Address[idx].b) == ADAP_TRUE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"mac address already in global table!\n");
				return MEA_OK;
			}
		}

		for(idx=0;idx<MEA_MAX_LOCAL_MAC;idx++)
		{
			//if(memcmp(&entry.Local_Mac_Address[idx],&zeroForwardMac[0],6) == 0) // only for NIC one MAC
			{
				adap_mac_to_arr(macAddr, entry.Local_Mac_Address[idx].b);
				found=MEA_TRUE;
				break;
			}
		}
		if(found  == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"not enough place for adding new entry!\n");
			return MEA_ERROR;
		}
	}
	else
	{
		for(idx=0;idx<MEA_MAX_LOCAL_MAC;idx++)
		{
			if(adap_is_mac_equal(macAddr, entry.Local_Mac_Address[idx].b) == ADAP_TRUE)
			{
				adap_memset(&entry.Local_Mac_Address[idx],0x00,ETH_ALEN);  // Clear the
				found=MEA_TRUE;
			}
		}
		if(found  == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip address not found in global entry!\n");
			return MEA_ERROR;
		}
	}
	if(MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Globals_Entry failed!\n");
		return MEA_ERROR;
	}


	return MEA_OK;
}

MEA_Status MeaDrvGetFirstGlobalMacAddress(tEnetHal_MacAddr *macAddr)
{
	MEA_Globals_Entry_dbt entry;

	adap_memset(&entry,0,sizeof(MEA_Globals_Entry_dbt));

	if(MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed!\n");
		return MEA_ERROR;
	}
	adap_mac_from_arr(macAddr, entry.Local_Mac_Address[0].b);

	return MEA_OK;

}

MEA_Status MeaDrvGetFirstGlobalIpAddress(ADAP_Uint32 *ipaddress)
{
	MEA_Globals_Entry_dbt entry;

	adap_memset(&entry,0,sizeof(MEA_Globals_Entry_dbt));

	if(MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed!\n");
		return MEA_ERROR;
	}

	(*ipaddress) =  entry.Local_IP_Address[0];
	return MEA_OK;

}

MEA_Status MeaDrvSetGlobalIpAddress(ADAP_Uint32 ipaddress,MEA_Bool addNewIp)
{
	MEA_Globals_Entry_dbt entry;
	ADAP_Uint32 idx;
	MEA_Bool found=MEA_FALSE;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"request to %s ipaddress:%x\n",addNewIp==MEA_TRUE?"ADD":"REM",ipaddress);

	adap_memset(&entry,0,sizeof(MEA_Globals_Entry_dbt));

	if(MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed!\n");
		return MEA_ERROR;
	}

	if(addNewIp == MEA_TRUE)
	{
		for(idx=0;idx<MEA_MAX_LOCAL_IP;idx++)
		{
			if(entry.Local_IP_Address[idx] == ipaddress)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip address allready in global table!\n");
				return MEA_ERROR;
			}
		}

		for(idx=0;idx<MEA_MAX_LOCAL_IP;idx++)
		{
			if(entry.Local_IP_Address[idx] == 0)
			{
				entry.Local_IP_Address[idx] = ipaddress;
				found=MEA_TRUE;
				break;
			}
		}
		if(found  == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"not enough place for adding new entry!\n");
			return MEA_ERROR;
		}
	}
	else
	{
		for(idx=0;idx<MEA_MAX_LOCAL_IP;idx++)
		{
			if(entry.Local_IP_Address[idx] == ipaddress)
			{
				entry.Local_IP_Address[idx] = 0;
				found=MEA_TRUE;
			}
		}
		if(found  == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip address not found in global entry!\n");
			return MEA_ERROR;
		}
	}
	if(MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Globals_Entry failed!\n");
		return MEA_ERROR;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"saved at global IP :%x\n",ipaddress);

	return MEA_OK;
}




MEA_Status MeaDrvCreateLxcp(MEA_LxCp_t *pNewLxcp)
{
	MEA_LxCP_Entry_dbt  LxCp_entry;

	(*pNewLxcp) = MEA_PLAT_GENERATE_NEW_ID;

	adap_memset(&LxCp_entry, 0, sizeof(LxCp_entry));

	if(MEA_API_Create_LxCP(MEA_UNIT_0, &LxCp_entry, pNewLxcp))
	{
		 return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status MeaDrvSetLxcpParamaters(MEA_LxCP_Protocol_te opcode,MEA_LxCP_Protocol_Action_te action,MEA_LxCp_t lxcp_Id,MEA_Action_t *pAction_id,MEA_OutPorts_Entry_dbt *pAction_outPorts)
{
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;


	adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
	adap_memset(&LxCp_data, 0, sizeof(LxCp_data));

	LxCp_key.protocol = opcode;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, lxcp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id:%d\r\n",lxcp_Id);
		return MEA_ERROR;
	}

	// if allready configured with the same then do nothing
	if(LxCp_data.action_type == action)
	{
		return MEA_OK;
	}

	LxCp_data.action_type = action;
	// if need to do special thing
	if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION)
	{
		if( (pAction_id == NULL) || (pAction_outPorts == NULL) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"output port must be configured\r\n");
			return MEA_ERROR;
		}
		LxCp_data.ActionId_valid = MEA_TRUE;
		LxCp_data.ActionId = *pAction_id;
		LxCp_data.OutPorts_valid = MEA_TRUE;
		adap_memcpy(&LxCp_data.OutPorts,pAction_outPorts,sizeof(LxCp_data.OutPorts));
	}
	else
	{
		LxCp_data.ActionId_valid = MEA_FALSE;
		LxCp_data.OutPorts_valid = MEA_FALSE;
	}

	if(MEA_API_Set_LxCP_Protocol(MEA_UNIT_0, lxcp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set lxcp id:%d opcode\r\n",lxcp_Id,opcode);
		return MEA_ERROR;
	}

	return MEA_OK;

}

MEA_Status meaDrvGetAllHpmPriority(MEA_TFT_t  tft_profile_Id,MEA_TFT_data_dbt  *tft_entry,ADAP_Uint32 *pNumOfEntries)
{
	ADAP_Uint32 index=0;
	ADAP_Uint32 numOfEntries=0;
	MEA_TFT_data_dbt  local_tft_entry;

	numOfEntries = (*pNumOfEntries);
	(*pNumOfEntries) = 0;

	for(index=0;index<32;index++)
	{
		adap_memset(&local_tft_entry,0,sizeof(MEA_TFT_data_dbt));

		if(ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_UNIT_0,tft_profile_Id,index,&local_tft_entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",tft_profile_Id,index);
			return MEA_ERROR;
		}
		if(numOfEntries > (*pNumOfEntries))
		{
			if(tft_entry[index].TFT_info.L4_dest_valid == MEA_TRUE)
			{
				adap_memcpy(&tft_entry[(*pNumOfEntries)++],&local_tft_entry,sizeof(MEA_TFT_data_dbt));
			}
		}
	}

	return MEA_OK;
}

MEA_Status meaDrvSetAllHpmPriority(MEA_TFT_t  tft_profile_Id,MEA_TFT_data_dbt  *tft_entry,ADAP_Uint32 numOfEntries)
{
	ADAP_Uint32 index=0;
	for(index=0;index<numOfEntries;index++)
	{
		if(tft_entry[index].TFT_info.L4_dest_valid == MEA_TRUE)
		{
			if(ENET_ADAPTOR_TFT_Set_ClassificationRule(MEA_UNIT_0,tft_profile_Id,tft_entry[index].TFT_info.QoS_Index,&tft_entry[index]) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",tft_profile_Id,index);
				return MEA_ERROR;
			}
		}
	}
	return MEA_OK;
}

MEA_Status meaDrvSetIpv6HpmPriority(MEA_TFT_t  tft_profile_Id,tEnetHal_IPv6Addr *ipv6Addr,ADAP_Uint32 mask,ADAP_Uint32 entryId)
{
	MEA_TFT_data_dbt  tft_entry;

	adap_memset(&tft_entry,0,sizeof(MEA_TFT_data_dbt));
	if(ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
		return MEA_ERROR;
	}

	tft_entry.TFT_info.Type=MEA_TFT_TYPE_IPV6;
	tft_entry.TFT_info.Ipv6_valid = MEA_TRUE;
	adap_memcpy(tft_entry.TFT_info.Ipv6,ipv6Addr,sizeof(tEnetHal_IPv6Addr));
	tft_entry.TFT_info.Ipv6_mask=mask;

	tft_entry.TFT_info.Type = MEA_TFT_TYPE_IPV6;
	tft_entry.TFT_info.Priority_Rule = entryId;
	tft_entry.TFT_info.QoS_Index = entryId;



	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Type:%d\r\n",tft_entry.TFT_info.Type);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IPv6:%x %x %x %x\r\n",tft_entry.TFT_info.Ipv6[0],
																		tft_entry.TFT_info.Ipv6[1],
																		tft_entry.TFT_info.Ipv6[2],
																		tft_entry.TFT_info.Ipv6[3]);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IPv6_mask:%d\r\n",tft_entry.TFT_info.Ipv6_mask);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IPv6_valid:%d\r\n",tft_entry.TFT_info.Ipv6_valid);


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Priority_Rule:%d\r\n",tft_entry.TFT_info.Priority_Rule);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"QoS_Index:%d\r\n",tft_entry.TFT_info.QoS_Index);


	// need to calculate the priority rule
	if(ENET_ADAPTOR_TFT_Set_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
		return MEA_ERROR;
	}

	return MEA_OK;
}

MEA_Status meaDrvClearHpmPriority(MEA_TFT_t  tft_profile_Id,ADAP_Uint32 entryId)
{
	MEA_TFT_data_dbt  tft_entry;

	adap_memset(&tft_entry,0,sizeof(MEA_TFT_data_dbt));
	if(ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
		return MEA_ERROR;
	}


	if( (tft_entry.TFT_info.Dest_IPv4_valid == MEA_TRUE) || (tft_entry.TFT_info.Ipv6_valid == MEA_TRUE) )
	{
		tft_entry.TFT_info.Dest_IPv4_valid = MEA_FALSE;
		tft_entry.TFT_info.Ipv6_valid = MEA_FALSE;
		if(ENET_ADAPTOR_TFT_Set_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
			return MEA_ERROR;
		}
	}

	return MEA_OK;
}

MEA_Status meaDrvSetHpmPriority(MEA_TFT_t  tft_profile_Id,ADAP_Uint32 ipAddr,ADAP_Uint32 mask,ADAP_Uint32 entryId)
{
	MEA_TFT_data_dbt  tft_entry;

	adap_memset(&tft_entry,0,sizeof(MEA_TFT_data_dbt));
	if(ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
		return MEA_ERROR;
	}

	tft_entry.TFT_info.Dest_IPv4_valid = MEA_TRUE;
	tft_entry.TFT_info.Dest_IPv4 = ipAddr;
	tft_entry.TFT_info.Dest_IPv4_mask = mask;

	tft_entry.TFT_info.Type = MEA_TFT_TYPE_IPV4;
	tft_entry.TFT_info.Priority_Rule = (32-mask);
	tft_entry.TFT_info.QoS_Index = entryId;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"valid:%d\r\n",tft_entry.TFT_info.Dest_IPv4_valid);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Dest_IPv4:%d\r\n",tft_entry.TFT_info.Dest_IPv4);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Dest_IPv4_mask:%d\r\n",tft_entry.TFT_info.Dest_IPv4_mask);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Dest_IPv4_valid:%d\r\n",tft_entry.TFT_info.Dest_IPv4_valid);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Type:%d\r\n",tft_entry.TFT_info.Type);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Priority_Rule:%d\r\n",tft_entry.TFT_info.Priority_Rule);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"QoS_Index:%d\r\n",tft_entry.TFT_info.QoS_Index);


	// need to calculate the priority rule
	if(ENET_ADAPTOR_TFT_Set_ClassificationRule(MEA_UNIT_0,tft_profile_Id,entryId,&tft_entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set TFT rule profile:%d index:%d\r\n",tft_profile_Id,entryId);
		return MEA_ERROR;
	}

	return MEA_OK;
}

MEA_Status adap_IpRolesDbEntryValid(tIpRoleData *pRoleData,ADAP_Uint32 *entrymask)
{
	MEA_TFT_data_dbt  tft_entry;
	ADAP_Uint32 entryId;

	(*entrymask)=0;

	for(entryId=0;entryId<32;entryId++)
	{
		adap_memset(&tft_entry,0,sizeof(MEA_TFT_data_dbt));
		if(ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_UNIT_0,pRoleData->tft_profile_Id,entryId,&tft_entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get TFT rule profile:%d index:%d\r\n",pRoleData->tft_profile_Id,entryId);
			return MEA_ERROR;
		}

		if( (tft_entry.TFT_info.Dest_IPv4_valid = MEA_TRUE) || (tft_entry.TFT_info.Ipv6_valid = MEA_TRUE) )
		{
			(*entrymask) |= tft_entry.TFT_info.Priority_Rule;
		}

	}
	return MEA_OK;
}

MEA_Status adap_SetAclFilterMaskParameters(tAdapAclFilterType *pAclType,MEA_ACL_mask_dbt *pAclMask,MEA_Filter_Key_dbt *pFilterKey,ADAP_Uint32 *pMaxNumOfAcl)
{
	ADAP_Uint32 maxNumOfAcls=(*pMaxNumOfAcl);

	(*pMaxNumOfAcl)=0;
	if(pAclType->bitmask & ADAP_FILTER_DA_MAC_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on DA MAC\r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer2.ethernet.DA_valid = MEA_TRUE;
			adap_apply_mask_to_mac(&pAclType->da_mac, pAclType->DestMacAddrMask, &pAclType->da_mac);
			adap_mac_to_arr(&pAclType->da_mac, pFilterKey[(*pMaxNumOfAcl)].layer2.ethernet.DA.b);
            pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31  |= (pAclType->DestMacAddrMask & 0xFFFFFFFF);
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (pAclType->DestMacAddrMask & 0xFFFF00000000) >> 32;
			pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DA_MAC;


			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type

			
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));
			(*pMaxNumOfAcl)++;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"num of filters are bigger then max\r\n");
			return MEA_ERROR;
		}

	}
	if(pAclType->bitmask & ADAP_FILTER_SA_MAC_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on SA MAC\r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer2.ethernet.SA_valid = MEA_TRUE;
            adap_apply_mask_to_mac(&pAclType->src_mac, pAclType->SourceMacAddrMask, &pAclType->src_mac);
			adap_mac_to_arr(&pAclType->src_mac, pFilterKey[(*pMaxNumOfAcl)].layer2.ethernet.SA.b);
            pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31  |= (pAclType->SourceMacAddrMask & 0xFFFFFFFF);
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (pAclType->SourceMacAddrMask & 0xFFFF00000000) >> 32;
			pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SA_MAC;

			//pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) ((src_sid & (0x00000fff)) << (20));    // ((MEA_Uint32)0x1FF << 20); // alex
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));
			(*pMaxNumOfAcl)++;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"num of filters are bigger then max\r\n");
			return MEA_ERROR;
		}
	}

	if(pAclType->bitmask & ADAP_FILTER_IP_PROTOCOL_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on ip protocol \r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.priority_valid=MEA_TRUE;
			pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.priority=0;
			pFilterKey[(*pMaxNumOfAcl)].layer3.ip_protocol_valid=MEA_TRUE;
			pFilterKey[(*pMaxNumOfAcl)].layer3.ip_protocol=pAclType->ipProto;
			pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid=MEA_TRUE;
			pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=0;
			pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid=MEA_TRUE;
			pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port=0;

			pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT;


			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= 0x000000FF;// alex 0x0000007F


			if(pAclType->bitmask & ADAP_FILTER_SOURCE_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ source port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=pAclType->SourcePort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0xFFFF0000;
			}
			if(pAclType->bitmask & ADAP_FILTER_DEST_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ destination port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port=pAclType->DestPort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
			}

			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));

			(*pMaxNumOfAcl)++;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"num of filters are bigger then max\r\n");
			return MEA_ERROR;
		}
	}
	if(pAclType->bitmask & ADAP_FILTER_SOURCE_IPV6_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on source ipv6 \r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6_valid = MEA_TRUE;

			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[0] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[0] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[1] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[2] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[3] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[1] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[4] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[5] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[6] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[7] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[2] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[8] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[9] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[10] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[11] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[3] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[12] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[13] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[14] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.src_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->srcIpv6)[15] & 0xFF)) ;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31  |= 0xFFFFFFFF;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xFFFF0000) >> 16;

			if(pAclType->bitmask & ADAP_FILTER_SOURCE_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ source port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=pAclType->SourcePort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT;
			}			
			else if(pAclType->bitmask & ADAP_FILTER_DEST_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ destination port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = pAclType->DestPort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT;
			}
			else
			{
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid= MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = 0;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT;
			}
			(*pMaxNumOfAcl)++;			
		}
	}
	
	if(pAclType->bitmask & ADAP_FILTER_DEST_IPV6_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on destination ipv6 \r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6_valid = MEA_TRUE;

			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[0] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[0] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[1] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[2] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[0] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[3] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[1] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[4] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[5] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[6] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[1] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[7] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[2] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[8] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[9] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[10] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[2] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[11] & 0xFF)) ;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[3] = ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[12] & 0xFF)) << 24;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[13] & 0xFF)) << 16;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[14] & 0xFF)) << 8;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv6.dst_ipv6[3] |= ((MEA_Uint32)(((ADAP_Uint8*)&pAclType->dstIpv6)[15] & 0xFF)) ;
			
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"destination ip %s\r\n",
					adap_ipv6_to_string(&pAclType->dstIpv6));
			
			pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31  |= 0xFFFFFFFF;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xFFFF0000) >> 16;

			if(pAclType->bitmask & ADAP_FILTER_DEST_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ destination port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = pAclType->DestPort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT;
			}
			else if(pAclType->bitmask & ADAP_FILTER_SOURCE_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ source port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=pAclType->SourcePort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT;
			}
			else
			{
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid= MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = 0;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT;
			}
			(*pMaxNumOfAcl)++;			
		}
	}
	if(pAclType->bitmask & ADAP_FILTER_DEST_IPV4_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on destination ip \r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.dst_ip_valid = MEA_TRUE;

			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.dst_ip = pAclType->destIpv4;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.dst_ip &= (pAclType->destIpv4Mask);
			pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31  |= (pAclType->destIpv4Mask & 0xFFFF) << 16;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (pAclType->destIpv4Mask & 0xFFFF0000) >> 16;






			if(pAclType->bitmask & ADAP_FILTER_DEST_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ destination port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = pAclType->DestPort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT;
			}
			else if(pAclType->bitmask & ADAP_FILTER_SOURCE_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ source port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=pAclType->SourcePort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT;
			}
			else
			{
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid= MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = 0;
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT;
			}

			//pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (ADAP_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
			//pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) ((src_sid & (0x00000fff)) << (20));    // ((MEA_Uint32)0x1FF << 20); // alex
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));

			(*pMaxNumOfAcl)++;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"num of filters are bigger then max\r\n");
			return MEA_ERROR;
		}
	}
	if(pAclType->bitmask & ADAP_FILTER_SOURCE_IPV4_KEY_TYPE)
	{
		if(maxNumOfAcls > (*pMaxNumOfAcl) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on source ip\r\n");
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.src_ip_valid = MEA_TRUE;

			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.src_ip = pAclType->srcIpv4;
			pFilterKey[(*pMaxNumOfAcl)].layer3.IPv4.src_ip &= (pAclType->SourceIpv4Mask);

			pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 = 0;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (pAclType->SourceIpv4Mask & 0xFFFF0000) >> 16;
			pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= (pAclType->SourceIpv4Mask & 0xFFFF) << 16;

			pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT;

			if(pAclType->bitmask & ADAP_FILTER_SOURCE_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ source port \r\n");
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port=pAclType->SourcePort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
			}
			else if(pAclType->bitmask & ADAP_FILTER_DEST_PORT_KEY_TYPE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"+ destination port\r\n");
				pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = pAclType->DestPort;
				pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x0000FFFF;
			}
			else
			{
				pFilterKey[(*pMaxNumOfAcl)].layer4.src_port_valid = MEA_TRUE;
				pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = 0;
			}
			//pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (ADAP_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
			//pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) ((src_sid & (0x00000fff)) << (20));    // ((MEA_Uint32)0x1FF << 20); // alex
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
			pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));
			(*pMaxNumOfAcl)++;
		}
	}

    if(pAclType->bitmask & ADAP_FILTER_OUTER_ETHTYPE_KEY_TYPE)
    {
        if(maxNumOfAcls > (*pMaxNumOfAcl) )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"OuterEtherType:%d\r\n",pAclType->outerEtherType);
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype = pAclType->outerEtherType;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority = 0;

            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= 0x0000FFFF;
            pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (MEA_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
            (*pMaxNumOfAcl)++;
        }
    }

    if(pAclType->bitmask & ADAP_FILTER_INNER_ETHTYPE_KEY_TYPE)
    {
        if(maxNumOfAcls > (*pMaxNumOfAcl) )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"InnerEtherType:%d\r\n",pAclType->innerEtherType);
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype = pAclType->innerEtherType;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority = 0;

            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= 0x0000FFFF;
            pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (MEA_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
            (*pMaxNumOfAcl)++;
        }
    }

    if(pAclType->bitmask & ADAP_FILTER_CVLAN_PRIORITY_KEY_TYPE)
    {
        if(maxNumOfAcls > (*pMaxNumOfAcl) )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"i1IssL2FilterCVlanPriority:%d\r\n",pAclType->cvlanPriority);
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority = pAclType->cvlanPriority;

            pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x7 << 13;
            pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG;
//            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (MEA_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));
            (*pMaxNumOfAcl)++;
        }
    }

    if(pAclType->bitmask & ADAP_FILTER_SVLAN_PRIORITY_KEY_TYPE)
    {
        if(maxNumOfAcls > (*pMaxNumOfAcl) )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FilterSVlanPriority:%d\r\n", pAclType->svlanPriority);
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.inner_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.ethertype = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.vlan = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.cf = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority = pAclType->svlanPriority;

            pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 |= 0x7 << 13;
            pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG;
//            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= ( ( (MEA_Uint32)pAclMask[(*pMaxNumOfAcl)].filter_key_type) & 0xF) << 16;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (0xF) << 16;  // filter_key_type
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 |= (MEA_Uint32) (((0x00000fff)) << (20));
            (*pMaxNumOfAcl)++;
        }
    }

    if(pAclType->bitmask & ADAP_FILTER_L2_PROTOCOL_KEY_TYPE)
    {
        if(maxNumOfAcls > (*pMaxNumOfAcl) )
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on appl ethtype\r\n");
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"l2ProtocolType:%d\r\n", pAclType->l2ProtocolType);

            pFilterKey[(*pMaxNumOfAcl)].layer3.ip_protocol_valid=MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer3.ip_protocol=0;
            pFilterKey[(*pMaxNumOfAcl)].layer3.ethertype3 = pAclType->l2ProtocolType;
            pFilterKey[(*pMaxNumOfAcl)].layer3.ethertype3_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port_valid = MEA_TRUE;
            pFilterKey[(*pMaxNumOfAcl)].layer4.dst_port = 0;
            pFilterKey[(*pMaxNumOfAcl)].layer2.outer_vlanTag.priority_valid = MEA_TRUE;

            pAclMask[(*pMaxNumOfAcl)].filter_key_type = MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_0_31 = 0xffff << 16;
            pAclMask[(*pMaxNumOfAcl)].mask.mask_32_63 = 0xffff << 16;
            (*pMaxNumOfAcl)++;
        }
        else
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"num of filters are bigger then max\r\n");
            return MEA_ERROR;
        }
    }

	if((*pMaxNumOfAcl) == 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"no filter match configured\r\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}

static
ADAP_Uint32 UpdateFiltersHierByAclProfileFilterType(ADAP_Uint32 acl_profile,
                MEA_Filter_Key_Type_te filterType, ADAP_Uint32 aclHierId)
{
    MEA_Filter_t filterId;
    MEA_Bool found;
    MEA_Filter_Key_dbt FilterKey;
    MEA_Filter_Data_dbt FilterData;
    MEA_Filter_Key_Type_te keyType;
    MEA_OutPorts_Entry_dbt         OutPorts_Entry;
    MEA_Policer_Entry_dbt          Policer_entry;
 //   MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;
    MEA_Filter_t filters[MAX_NUM_OF_FILTERS];
    int count = 0, i;

    if (ENET_ADAPTOR_GetFirst_Filter(MEA_UNIT_0, &filterId, &found) != MEA_OK)
    {
        return ENET_FAILURE;
    }

    while (found == MEA_TRUE)
    {
        if (ENET_ADAPTOR_Get_Filter (MEA_UNIT_0,filterId,
                        &FilterKey,
                        NULL,
                        &FilterData,
                        &OutPorts_Entry,
                        &Policer_entry,
                        NULL) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                        "Error getting filter\n");
                return ENET_FAILURE;
            }

        if (ENET_ADAPTOR_Get_Filter_KeyType(MEA_UNIT_0,
                &FilterKey,
                &keyType) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                    "Error getting filter keytype\n");
            return ENET_FAILURE;

        }
        if ((FilterKey.interface_key.acl_prof == acl_profile) &&
           (keyType == filterType) && (FilterKey.Acl_hierarchical_id != aclHierId))
        {
            filters[count] = filterId;
            count++;
        }

        if (ENET_ADAPTOR_GetNext_Filter(MEA_UNIT_0, &filterId, &found) != MEA_OK)
        {
            return ENET_FAILURE;
        }
    }

    for (i=0; i<count;i++)
    {
        if (ENET_ADAPTOR_Get_Filter (MEA_UNIT_0,filters[i],
                        &FilterKey,
                        NULL,
                        &FilterData,
                        &OutPorts_Entry,
                        &Policer_entry,
                        NULL /*&EHP_Entry*/) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                        "Error getting filter\n");
                return ENET_FAILURE;
            }

        FilterKey.Acl_hierarchical_id = aclHierId;

        if (ENET_ADAPTOR_Delete_Filter(MEA_UNIT_0, filters[i]) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "IssHwUpdateL3Filter Error: ENET_ADAPTOR_Delete_Filter type == action to action\n");
            return MEA_ERROR;
        }

        if (ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_UNIT_0,
                &FilterKey,
                NULL,
            &found,
            &filterId) != MEA_OK)
        {
            return ENET_FAILURE;
        }

        if (found == MEA_TRUE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Filter exists\n");
            continue;
        }

        if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
                                &FilterKey,
                                NULL,
                                &FilterData,
                                &OutPorts_Entry,
                                &Policer_entry,
                                /*(EHP_Entry.ehp_info) ? &EHP_Entry:*/ NULL,
                                &filters[i]) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                                "IssHwUpdateL3Filter Error: ENET_ADAPTOR_Create_Filter type == action to action\n");
                    return MEA_ERROR;
                }
    }
    return ENET_SUCCESS;
}

static
MEA_Status UpdateFilterPolicingInfo(MEA_Filter_Data_dbt *FilterData, tAclFilterParams *pAclParams,
        MEA_Policer_Entry_dbt *filter_policer)
{
    if (pAclParams->isPolicerSet == ADAP_TRUE)
    {
        if(MEA_API_Get_Policer_ACM_Profile (MEA_UNIT_0, pAclParams->enetPolilcingProfile, 0, /*ACM_Mode*/
              filter_policer) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "HwUpdateL3Filter Error: MEA_API_Get_Policer_ACM_Profile FAIL for profile:%d\n",
                    pAclParams->enetPolilcingProfile);
            return MEA_ERROR;
        }
        FilterData->action_params.policer_prof_id_valid = MEA_TRUE;
        FilterData->action_params.policer_prof_id = 0 ;  //pAclParams->enetPolilcingProfile; //Alex
        FilterData->action_params.tm_id_valid = MEA_TRUE;
     } else
     {
         FilterData->action_params.tm_id = 0;
         FilterData->action_params.tm_id_valid = MEA_FALSE;
         FilterData->action_params.policer_prof_id_valid = MEA_FALSE;
         FilterData->action_params.policer_prof_id = 0;
     }
    return MEA_OK;
}

static
ADAP_Uint32 CreateFiltersFromFilterList(ADAP_Uint32 acl_profile, MEA_Filter_Key_Type_te filterType, ADAP_Uint32 aclHierId,
        MEA_Filter_Data_dbt *filterData, MEA_Filter_Key_dbt *filterKey, tAclFilterParams *pAclParams)
{

    MEA_Filter_Key_Type_te keyType;
    int i;
    MEA_Filter_t filterId;
    MEA_Bool exist;
    MEA_Policer_Entry_dbt filter_policer;
    tAdap_FilterEntry *entry = Adap_GetFirstAccessListLinkList();
    MEA_Filter_Key_dbt filterKeyLoc;
    tAdapAclProfile * pAclProf;

    if (!entry)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"No filters to process\n");
        return ENET_SUCCESS;
    }

    while (entry)
    {
        // Skip filters with different action_type
        /*if (entry->tAclParams.action_type != pAclParams->action_type)
            continue;
        */

        for (i = 0; i< entry->tAclParams.num_of_filters; i++)
        {
            //Checking inPort
            pAclProf = AdapGetAclProfilePtr(acl_profile, pAclParams->filterNoId);
            if (!pAclProf)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                        "Error getting acl profile ptr\n");
                return ENET_FAILURE;
            }
            if (entry->fType == FILTER_TYPE_L3)
            {
                if (!entry->x.l3FilterEntry.InPortList[pAclProf->inPort])
                    continue;
            } else
            {
                if (!entry->x.l2FilterEntry.InPortList[pAclProf->inPort])
                    continue;
            }

            if (ENET_ADAPTOR_Get_Filter_KeyType(MEA_UNIT_0,
                      &entry->tAclParams.arrFilterKey[i],
                      &keyType) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                        "Error getting filter keytype\n");
                return ENET_FAILURE;
            }
            // if keyType matches - try to create this filter
            if (keyType == filterType)
            {
                //Build proper filterKey
                memcpy (&filterKeyLoc, &entry->tAclParams.arrFilterKey[i], sizeof(filterKeyLoc));
                filterKeyLoc.interface_key.src_port_valid = filterKey->interface_key.src_port_valid;
                filterKeyLoc.interface_key.src_port = filterKey->interface_key.src_port;
                filterKeyLoc.interface_key.sid_valid = filterKey->interface_key.sid_valid;
                filterKeyLoc.interface_key.acl_prof  = acl_profile;

                filterKeyLoc.Acl_hierarchical_id = aclHierId;

                //Check if filter is already exists:
                if(ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_UNIT_0,
                        &filterKeyLoc,
                        NULL,
                        &exist,
                        &filterId) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                                        "Can't get filter info\n");
                    return MEA_ERROR;
                }

                // Skip if already exist
                if (exist == MEA_TRUE)
                    continue;

                if (filterData->action_type != MEA_FILTER_ACTION_TO_ACTION)
                {
                    if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
                            &filterKeyLoc,
                            NULL,
                            filterData,
                            NULL,
                            NULL,
                            NULL,
                            &filterId) != MEA_OK)
                        {
                            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                               " CreateFiltersFromFilterList Error: ENET_ADAPTOR_Create_Filter FAIL action type != action to action\n");
                            return MEA_ERROR;
                        }
                } else {
                    adap_memset(&filter_policer, 0, sizeof(filter_policer));
                    if (UpdateFilterPolicingInfo(filterData, &entry->tAclParams, &filter_policer) != MEA_OK)
                    {
                        return MEA_ERROR;
                    }

                    if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
                                        &filterKeyLoc,
                                        NULL,
                                        filterData,
                                        &entry->tAclParams.outPorts,
                                        (entry->tAclParams.isPolicerSet == ADAP_TRUE) ? &filter_policer : NULL,
                                        NULL, /* for ACL no editing */
                                        &filterId) != MEA_OK)
                        {
                            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                                        "IssHwUpdateL3Filter Error: ENET_ADAPTOR_Create_Filter type == action to action\n");
                            return MEA_ERROR;
                        }
                }
            }
        }
       entry = Adap_GetNextAccessListLinkList(entry);
    }

    return ENET_SUCCESS;
}

MEA_Status adap_SetAclFilterConfiguration(tAclFilterParams *pAclParams,ADAP_Uint32 acl_index,
					ADAP_Uint32 acl_profile)
{
    MEA_Filter_Data_dbt         FilterData;
    MEA_Filter_Key_dbt 		FilterKey;
    MEA_Filter_Key_Type_te keyType;
    MEA_Policer_Entry_dbt       filter_policer;
    ADAP_Uint32			index=0;
    MEA_Bool    exists;
    for(index=0;index<pAclParams->num_of_filters;index++)
    {
    adap_memset(&FilterData, 0, sizeof(FilterData));
	adap_memset(&FilterKey, 0, sizeof(FilterKey));
	adap_memset(&filter_policer, 0, sizeof(filter_policer));

	adap_memcpy(&FilterKey,&pAclParams->arrFilterKey[index],sizeof(MEA_Filter_Key_dbt));

	FilterData.action_params.ed_id = 0;

	//FilterData.action_params.proto_llc_valid = MEA_TRUE;
	//FilterData.action_params.protocol_llc_force = MEA_TRUE;
	//FilterData.action_params.Protocol = 0x1;
	//FilterData.action_params.Llc = 0;

	FilterKey.interface_key.src_port_valid = MEA_FALSE;
	FilterKey.interface_key.src_port = 0;
	FilterKey.interface_key.sid_valid = MEA_TRUE;
	FilterKey.interface_key.acl_prof  = acl_profile; //pAclParams->serviceId;

	FilterKey.Acl_hierarchical_id = index;  // why  is  index+acl_index;

	FilterData.action_type = pAclParams->action_type;

	pAclParams->FilterId[index]=MEA_PLAT_GENERATE_NEW_ID;
	FilterData.lxcp_win = MEA_TRUE;

	if(FilterData.action_type != MEA_FILTER_ACTION_TO_ACTION)
	{
        FilterData.fwd_Act_en = MEA_FALSE;
	    FilterData.action_params.output_ports_valid=MEA_FALSE;    //aaa

	    if(ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_UNIT_0,
            &FilterKey,
            NULL,
            &exists,
            &pAclParams->FilterId[index]) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                                "Can't get filter info\n");
            return MEA_ERROR;
        }

        if (exists == MEA_TRUE)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                    "IssHwUpdateL3Filter Filter already exists\n");
            goto filter_exists;
        }

	    if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
						&FilterKey,
						NULL,
						&FilterData,
						NULL,
						NULL,
						NULL,
						&pAclParams->FilterId[index]) != MEA_OK)
             {
                 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
			" HwUpdateL3Filter Error: ENET_ADAPTOR_Create_Filter FAIL action type != action to action\n");
                 return MEA_ERROR;
             }
        }
	else
	{
	    if (UpdateFilterPolicingInfo(&FilterData, pAclParams, &filter_policer) != MEA_OK)
	    {
	        return MEA_ERROR;
	    }

		FilterData.fwd_Act_en = MEA_TRUE;
		FilterData.action_params.pm_id_valid = MEA_TRUE;
		FilterData.action_params.pm_id = 0;

		FilterData.action_params.output_ports_valid=MEA_FALSE;
		FilterData.action_params.ed_id_valid = MEA_FALSE;
		FilterData.action_params.ed_id = 0;
		FilterData.action_params.force_color_valid = MEA_ACTION_FORCE_COMMAND_VALUE;
		FilterData.action_params.COLOR = MEA_POLICER_COLOR_VAL_GREEN;
		//FilterData.policer_prof_id = pServiceDb->policer_id;

		if(ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_UNIT_0,
		    &FilterKey,
		    NULL,
		    &exists,
		    &pAclParams->FilterId[index]) != MEA_OK)
		{
	        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
	                            "Can't get filter info\n");
	        return MEA_ERROR;
		}

		if (exists == MEA_TRUE)
		{
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
		            "IssHwUpdateL3Filter Filter already exists\n");
		    goto filter_exists;
		}

		if (ENET_ADAPTOR_Create_Filter (MEA_UNIT_0,
						&FilterKey,
						NULL,
						&FilterData,
						&pAclParams->outPorts,
						(pAclParams->isPolicerSet == ADAP_TRUE) ? &filter_policer : NULL,
						NULL, /* for ACL no editing */
						&pAclParams->FilterId[index]) != MEA_OK)
		{
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                        "IssHwUpdateL3Filter Error: ENET_ADAPTOR_Create_Filter type == action to action\n");
		    return MEA_ERROR;
		}
        }

filter_exists:
        if (ENET_ADAPTOR_Get_Filter_KeyType(MEA_UNIT_0,
                        &FilterKey,
                        &keyType) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                            "Error getting filter keytype\n");
                    return ENET_FAILURE;

                }

        if (CreateFiltersFromFilterList(acl_profile, keyType, index, &FilterData, &FilterKey, pAclParams) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                    "Error creating supp acl hier\n");
            return ENET_FAILURE;

        }
        if (UpdateFiltersHierByAclProfileFilterType(acl_profile, keyType, index) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                    "Error Updating acl hier\n");
            return ENET_FAILURE;

        }
    }
    return MEA_OK;
}


static ADAP_Int32
EnetHal_CreateTosDscpService(ADAP_Uint16 Service,ADAP_Int8 tos,ADAP_Int8 dscp,
        ADAP_Uint32 ifIndex,MEA_Service_Entry_Data_dbt  *pService_data,MEA_Policer_Entry_dbt *pPolicerEntry,
        MEA_Service_t *pNewService,ADAP_Uint16 vlanId,ADAP_Uint32 trafficType,MEA_Bool allow)
{
    MEA_Service_Entry_Data_dbt             Service_data;
    MEA_OutPorts_Entry_dbt                 Service_outPorts;
    MEA_Policer_Entry_dbt                  Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Service_editing;
    MEA_Service_Entry_Key_dbt               key;
    MEA_Service_t                          ServiceClone;
    tServiceDb                             *pServiceAlloc;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_CreateTosDscpService : service:%d dscp:%d tos:%d\n", Service,dscp,tos);

    adap_memset(&key,       0 , sizeof(key));
    adap_memset(&Service_data,      0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service,
                            &key,
                            &Service_data,
                            &Service_outPorts,
                            &Service_policer,
                            &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_CreateTosDscpService Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", Service);
        return ENET_FAILURE;
    }

    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) *(Service_editing.num_of_entries+1));

    if (Service_editing.ehp_info == NULL )
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: Can't allocate memory for Service_editing.ehp_info\n");
        return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                            Service,
                            NULL,
                            &Service_data,
                            &Service_outPorts, /* outPorts */
                            &Service_policer, /* policer  */
                            &Service_editing)
        != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FAIL: ENET_ADAPTOR_Get_Service (2) for service %d failed\n",Service);
        MEA_OS_free(Service_editing.ehp_info);
        return ENET_FAILURE;
    }


    Service_data.editId=0;
    Service_data.pmId=0;
    Service_data.policer_prof_id=0;
    Service_data.tmId=0;

    Service_data.DSE_learning_actionId = 0;
    Service_data.DSE_learning_actionId_valid = MEA_FALSE;

    if(tos != ENETHAL_TOS_INVALID * 2)
    {
        key.priType = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS;
        key.pri = tos;
        MeaDrvSetIngressPortPriRule(4 ,ifIndex);
        MeaDrvSetIngressPortMode(2 , ifIndex);
        MeaDrvSetIngressPortProtocol(1,1 ,ifIndex);

    }
    else if(dscp != -1)
    {
        key.priType = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP;
        key.pri = dscp;
        MeaDrvSetIngressPortPriRule(4 ,ifIndex);
        MeaDrvSetIngressPortMode(3 , ifIndex);
        MeaDrvSetIngressPortProtocol(1,1 ,ifIndex);
    }

    key.evc_enable = MEA_FALSE;
    key.external_internal = MEA_FALSE;

    ServiceClone=MEA_PLAT_GENERATE_NEW_ID;
    Service_data.flowCoSMappingProfile_force = MEA_FALSE;
    Service_data.flowMarkingMappingProfile_force = MEA_FALSE;

//    Service_data.ACL_filter_info.data_info[0].valid = pService_data->ACL_filter_info.data_info[0].valid;
//    Service_data.ACL_filter_info.data_info[0].filter_key_type = pService_data->ACL_filter_info.data_info[0].filter_key_type;
//    Service_data.ACL_filter_info.data_info[0].mask.mask_0_31 = pService_data->ACL_filter_info.data_info[0].mask.mask_0_31;
//    Service_data.ACL_filter_info.data_info[0].mask.mask_32_63 = pService_data->ACL_filter_info.data_info[0].mask.mask_32_63;

    ServiceClone=MEA_PLAT_GENERATE_NEW_ID;

    Service_data.DSE_forwarding_enable   = MEA_FALSE;
    Service_data.DSE_learning_enable     = MEA_FALSE;
    Service_data.DSE_learning_actionId   = 0;
    Service_data.DSE_learning_actionId_valid = MEA_FALSE;
    Service_data.DSE_learning_srcPort    = 0;
    Service_data.DSE_learning_srcPort_force = MEA_FALSE;

    if(pPolicerEntry != NULL)
    {
        adap_memcpy(&Service_policer, pPolicerEntry, sizeof(MEA_Policer_Entry_dbt) );
    }
    else //Filter is enabled and Policer is defined upon Filter create
    {
        Service_data.filter_enable = MEA_TRUE;
    }

    Service_data.DSE_learning_actionId_valid = MEA_FALSE;

    if(Service_editing.num_of_entries == 1)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_CreateTosDscpService before command set: vlan:%d ehpVlan:%d command:%d\n", vlanId,Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,Service_editing.ehp_info->ehp_data.eth_info.command);

        if( (Service_editing.ehp_info->ehp_data.eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP) && (Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid == vlanId) )
        {
            Service_editing.ehp_info->ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_CreateTosDscpService after command set: vlan:%d ehpVlan:%d command:%d\n", vlanId,Service_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,Service_editing.ehp_info->ehp_data.eth_info.command);
    }

    if(allow == MEA_TRUE)
    {
        /* Create new service for the given Vlan */
        if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                                 &key,
                                 &Service_data,
                                 &Service_outPorts,
                                 &Service_policer,
                                 &Service_editing,
                                 &ServiceClone) != MEA_OK)
        {
          ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_CreateTosDscpService Error: Unable to create the new service %d in DPL\n",  ServiceClone);
          MEA_OS_free(Service_editing.ehp_info);
          return ENET_FAILURE;
        }
    }
    else
    {
        /* Create new service for the given Vlan */
        if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                                 &key,
                                 &Service_data,
                                 NULL,
                                 &Service_policer,
                                 NULL,
                                 &ServiceClone) != MEA_OK)
        {
          ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_CreateTosDscpService Error: Unable to create the new service %d in DPL\n",  ServiceClone);
          MEA_OS_free(Service_editing.ehp_info);
          return ENET_FAILURE;
        }

    }

     ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_CreateTosDscpService Old service:%d pmId:%d, New Service:%d pmid:%d\n",  Service, pService_data->pmId, ServiceClone, Service_data.pmId);

    MEA_OS_free(Service_editing.ehp_info);

    pServiceAlloc = MeaAdapAllocateServiceId();
    if(pServiceAlloc == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
        return ENET_FAILURE;

    }

    pServiceAlloc->ifIndex = ifIndex;
    pServiceAlloc->valid = MEA_TRUE;
    pServiceAlloc->enet_priority_type = key.priType;
    pServiceAlloc->enet_IncPriority = key.pri;
    pServiceAlloc->L2_protocol_type = MEA_PARSING_L2_KEY_Ctag; //We set tagged because this functions works only for tagged services.
    pServiceAlloc->outerVlan = vlanId;
    pServiceAlloc->serviceId = ServiceClone;

    (*pNewService) = ServiceClone;

    return ENET_SUCCESS;

}


static
ADAP_Uint32 EnetHal_PrepareTosDscpService( tAclFilterParams *pAclData,ADAP_Uint32 IssInPort,eEnetHal_FilterAction action, MEA_Bool *serviceCreated,
        struct tVlanSubProtocolEntry *subProtocolEntry)
{
    ADAP_Uint16                                   VlanId;
    ADAP_Uint32                                   trafficType;
    MEA_Service_t                           Service_id=0;
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_Service_t                           newService;
    ADAP_Int8                                    enetTos= ENETHAL_TOS_INVALID;
    ADAP_Int8                                    enetDscp= -1;
    tServiceDb                                  *servDb;
    ADAP_Uint16                                 trfType;

    *serviceCreated = MEA_FALSE;
    if( (pAclData->tos < ENETHAL_TOS_INVALID) && (pAclData->tos > ENETHAL_TOS_NONE) )
    {
        enetTos = pAclData->tos;
    }
    enetDscp = (ADAP_Int8)pAclData->dscp;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete tos:%d dscp:%d\n", enetTos,enetDscp);
    trfType=D_TAGGED_SERVICE_TAG;
    // if its untagged
    if (subProtocolEntry->vlanId == 0)
    {
        VlanId=1;
    }
    else
    {
        VlanId = subProtocolEntry->vlanId;
    }
    for(trafficType=trfType;trafficType<D_PROVIDER_SERVICE_TAG;trafficType++)
    {
        servDb = MeaAdapGetServiceIdByPortTrafTypeSubProtocol(VlanId, IssInPort, trafficType, subProtocolEntry->subProtocolType);
        Service_id = (servDb) ? servDb->serviceId : UNDEFINED_VALUE16;

        if(Service_id == UNDEFINED_VALUE16)
        {
            continue;
        }

        if( (enetTos < ENETHAL_TOS_INVALID) && (enetTos > ENETHAL_TOS_NONE) )
        {
             servDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,IssInPort,trafficType,
                    enetTos*2, MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS, subProtocolEntry->subProtocolType);
            newService = (servDb) ? servDb->serviceId : UNDEFINED_VALUE16;

            if(newService != UNDEFINED_VALUE16)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete tos service %d\n", newService);
                MeaDeleteEnetServiceId(newService);

                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",newService);
            }
        }

        if (enetDscp != -1)
        {
            servDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,IssInPort,trafficType,
                    enetDscp, MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP, subProtocolEntry->subProtocolType);
            newService = (servDb) ? servDb->serviceId : UNDEFINED_VALUE16;

            if(newService != UNDEFINED_VALUE16)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete dscp service %d\n", newService);
                MeaDeleteEnetServiceId(newService);
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",newService);
            }
        }

        if(action == ENETHAL_DROP)
        {
            if(EnetHal_CreateTosDscpService(Service_id, enetTos*2, enetDscp, IssInPort, &Service_data, NULL, &newService, VlanId,
                    trafficType, MEA_FALSE) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_CreateTosDscpService ERROR: Unable to reset the service %dL\n", Service_id);
                return ENET_FAILURE;
            }
            *serviceCreated = MEA_TRUE;
        }
        break;
    }
    return ENET_SUCCESS;
}

static
ADAP_Int32 EnetHal_CreateAclService(ADAP_Uint16 VlanId,ADAP_Uint32 IfIndex, MEA_Service_t *pService_id,ADAP_Uint8 serviceType,
        ADAP_Uint32 acl_profile_id, ADAP_Uint32 subProtocolType)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_Service_t                         Service_id;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Port_t                            enetPort;
    MEA_Bool exist=MEA_FALSE;
    tServiceDb                           *pServiceAlloc;
    MEA_Uint16                            policer_id;

    adap_memset(&Service_data     , 0 , sizeof(Service_data));
    adap_memset(&Service_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&Service_policer, 0, sizeof(MEA_Policer_Entry_dbt));
    adap_memset(&Service_editing, 0, sizeof(Service_editing));
    adap_memset(&Service_editing_info, 0, sizeof(MEA_EHP_Info_dbt));
    adap_memset(&Service_key, 0, sizeof(MEA_Service_Entry_Key_dbt));

    MeaAdapGetPhyPortFromLogPort (IfIndex, &enetPort);
    Service_key.src_port         = enetPort;
    Service_key.net_tag          = VlanId;
    Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
    Service_key.sub_protocol_type = subProtocolType;

    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;

    if(serviceType == D_UNTAGGED_SERVICE_TAG)
    {
        Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
        Service_key.net_tag          = 0;
        Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
        Service_key.inner_netTag_from_value          = 0xFFFFFFF;
		Service_key.inner_netTag_from_valid          = MEA_FALSE;
    }


    /* Made the service priority-unaware (default priority) */
    // TODO We currently does not support evc in hardware
//    Service_key.evc_enable       = MEA_TRUE;
    Service_key.pri              = 7;

    Service_data.ACL_Prof = acl_profile_id;
    Service_data.ACL_Prof_valid = MEA_TRUE;
    /* Build the upstream service data attributes for the forwarder  */
    Service_data.DSE_forwarding_enable   = MEA_FALSE;
    Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.DSE_learning_enable     = MEA_FALSE;
    Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    Service_data.DSE_learning_actionId   = 0;
    Service_data.DSE_learning_actionId_valid = MEA_FALSE;
    Service_data.DSE_learning_srcPort    = 0;
    Service_data.DSE_learning_srcPort_force = MEA_FALSE;

    Service_data.vpn = 0;


    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;

    /* Set the Upstream Service policing */
    if(MeaAdapGetDefaultPolicer(IfIndex) == MEA_PLAT_GENERATE_NEW_ID)
    {
        policer_id=MEA_PLAT_GENERATE_NEW_ID;
        if(MeaDrvCreateDefaultPolicer (&policer_id, enetPort) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get MeaDrvCreateDefaultPolicer\r\n");
            return ENET_FAILURE;
        }

        /* Add the default Policer ID to the DB */
        MeaAdapSetDefaultPolicer(policer_id,IfIndex);
    } else
    {
        policer_id = MeaAdapGetDefaultPolicer(IfIndex);
    }
    Service_data.policer_prof_id = policer_id;

    if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
            policer_id,
                                        0,/* ACM_Mode */
                                        &Service_policer) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile failed\n");
        return ENET_FAILURE;
    }

    Service_editing.ehp_info       = &Service_editing_info;


    if (ENET_ADAPTOR_IsExist_Service_ByKey(MEA_UNIT_0, &Service_key, &exist, &Service_id) == MEA_OK)
    {
        if (exist == MEA_TRUE)
        {

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d L2_protocol_type:%d service id:%d vlan:%lu already exist going to delete\n", IfIndex, Service_key.L2_protocol_type, Service_id, (long unsigned int)Service_key.net_tag & 0xFFF);
            MeaDeleteEnetServiceId(Service_id);
        }
    }


    Service_id = MEA_PLAT_GENERATE_NEW_ID;

    /* Create new service for the given Vlan. */
    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &Service_outPorts,
                               &Service_policer,
                               &Service_editing,
                               &Service_id) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to create the new service %d\n",  Service_id);
        return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create the new service %d\n",Service_id);

    pServiceAlloc = MeaAdapAllocateServiceId();
    if(pServiceAlloc == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
        return ENET_FAILURE;

    }

    pServiceAlloc->ifIndex = IfIndex;
    pServiceAlloc->valid = MEA_TRUE;
    pServiceAlloc->enet_priority_type = Service_key.priType;
    pServiceAlloc->enet_IncPriority = Service_key.pri;
    pServiceAlloc->L2_protocol_type = Service_key.L2_protocol_type;
    pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
    pServiceAlloc->outerVlan = VlanId;
    pServiceAlloc->serviceId = Service_id;

    (*pService_id) = Service_id;

    return ENET_SUCCESS;
}

static ADAP_Uint32
MeaGetServiceIdFromPortId(ADAP_Uint32 InPort, tAclFilterParams *pAclData, struct tAclServiceEntry *aclServiceEntry,
        eEnetHal_FilterAction FilterAction, struct tVlanSubProtocolEntry *subProtocolEntry)
{
    MEA_Service_t               Service_id=0, baseService_id;
    ADAP_Uint32                 VlanId;
    tServiceDb 			*pServDb;
    ADAP_Uint32 		trafficType=0;

    aclServiceEntry->serviceId = MEA_PLAT_GENERATE_NEW_ID;
    aclServiceEntry->serviceCreated = MEA_FALSE;

    if(adap_IsPortChannel(InPort) == ENET_SUCCESS)
    {
      if( (adap_NumOfLagPortFromAggrId(InPort)) > 0)
      {
          InPort = adap_GetLagMasterPort(InPort);
      }
    }

    // if its untagged
    if (subProtocolEntry->vlanId == 0)
    {
        VlanId = 0;
    }
    else
    {
        VlanId = subProtocolEntry->vlanId;
    }
    trafficType=subProtocolEntry->L2ProtocolType;

    if( ( (pAclData->tos < ENETHAL_TOS_INVALID) && (pAclData->tos > ENETHAL_TOS_NONE) ) ||
            (pAclData->dscp != -1) )
    {
        EnetHal_PrepareTosDscpService(pAclData,InPort, FilterAction, &aclServiceEntry->serviceCreated, subProtocolEntry);
    }

    Service_id = MEA_PLAT_GENERATE_NEW_ID;

    if( (pAclData->tos < ENETHAL_TOS_INVALID) && (pAclData->tos > ENETHAL_TOS_NONE) )
    {
	    pServDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,InPort,trafficType,pAclData->tos*2,
			    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS, subProtocolEntry->subProtocolType);
	    Service_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;

	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"request TOS serviceId from IfIndex:%d vlanId:%d Service_id:%d\r\n", InPort,VlanId,Service_id);
    }
    else if(pAclData->dscp != -1)
    {
	    pServDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,InPort,trafficType,pAclData->dscp,
			    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP, subProtocolEntry->subProtocolType);
	    Service_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;

	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"request DSCP serviceId from IfIndex:%d vlanId:%d Service_id:%d\r\n", InPort,VlanId,Service_id);
    }
    else if(pAclData->cVlanPriority != EMPTY_VLAN_PRIORITY)
    {
	    pServDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,InPort,trafficType,pAclData->cVlanPriority,
			    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP, subProtocolEntry->subProtocolType);
	    Service_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;

	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"request priority serviceId from IfIndex:%d vlanId:%d Service_id:%d\r\n", InPort,VlanId,Service_id);

	    if(Service_id == MEA_PLAT_GENERATE_NEW_ID)
	    {
		    // Getting basic service id
		    pServDb = MeaAdapGetSimpleServiceIdByPortSubType(VlanId, InPort, trafficType, subProtocolEntry->subProtocolType);
		    baseService_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;

		    if(MeaDrvVlanHwCreateFilterPriorityVlan(baseService_id,trafficType,InPort,pAclData->cVlanPriority,VlanId,15) != ENET_SUCCESS)
		    {
			    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: failed to create priority service port:%d VLAN:%d priority:%d\n",
					    InPort, VlanId,pAclData->cVlanPriority);
			    return ENET_FAILURE;
		    }
		    pServDb = MeaAdapGetServiceIdByTrafTypePortPriSubType(VlanId,InPort,trafficType,pAclData->cVlanPriority,
				    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP, subProtocolEntry->subProtocolType);
		    Service_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;
		    if(Service_id == MEA_PLAT_GENERATE_NEW_ID)
		    {
			    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Warning: No priority service port:%d VLAN:%d priority:%d\n",
					    InPort, VlanId,pAclData->cVlanPriority);
		    } else {
			    aclServiceEntry->serviceCreated = MEA_TRUE;
		    }
	    }
    }
    else
    {
	    tServiceDb *pServDb = MeaAdapGetServiceIdByPortTrafTypeSubProtocol(VlanId, InPort, trafficType, subProtocolEntry->subProtocolType);
	    Service_id = (pServDb) ? pServDb->serviceId : MEA_PLAT_GENERATE_NEW_ID;
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"request serviceId from IfIndex:%d vlanId:%d Service_id:%d trafficType:%d\r\n", InPort,VlanId,Service_id,trafficType);
    }

    if(Service_id == MEA_PLAT_GENERATE_NEW_ID)
    {
	    // in case there is no service on specific VLAn then create one
	    //create service with NULL group
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaSetFilterTypeToPortId vlanId:%d portId:%d\n", VlanId,InPort);

	    if(EnetHal_CreateAclService(VlanId,InPort,&Service_id,trafficType, pAclData->acl_profile_filt.acl_profile,
				    subProtocolEntry->subProtocolType) != ENET_SUCCESS)
	    {
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaSetFilterTypeToPortId service Id %d failed\n", Service_id);
		    return ENET_FAILURE;
	    }
	    aclServiceEntry->serviceCreated = MEA_TRUE;
    }
    aclServiceEntry->vlanId=VlanId;
    aclServiceEntry->trafficType=trafficType;
    aclServiceEntry->serviceId = Service_id;
    return ENET_SUCCESS;
}

static ADAP_Uint32
RemoveFilterFromService(tServiceAclParam *aclParam)
{
    MEA_Service_Entry_Data_dbt             Service_data;
    MEA_OutPorts_Entry_dbt                 Service_outPorts;
    MEA_Policer_Entry_dbt                  Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Service_editing;
    MEA_Bool                               valid;

    adap_memset(&Service_data  , 0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
    /* check if the service exist */
    if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0, aclParam->serviceId,&valid) != MEA_OK)
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"RemoveFilterFromService Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", aclParam->serviceId);
      return ENET_FAILURE;
    }
    if(valid == MEA_FALSE)
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"MeaSetFilterTypeToPortId Warning: ServiceId %d does not exists\n", aclParam->serviceId);
      return ENET_SUCCESS;
    }
    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                aclParam->serviceId,
                NULL,
                &Service_data,
                &Service_outPorts,
                &Service_policer,
                &Service_editing) != MEA_OK)
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaSetFilterTypeToPortId Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", aclParam->serviceId);
      return ENET_FAILURE;
    }

    Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

    if (Service_editing.ehp_info == NULL )
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaSetFilterTypeToPortId Error:  FAIL to malloc ehp_info\n");
      return ENET_FAILURE;
    }

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                aclParam->serviceId,
                NULL,
                &Service_data,
                &Service_outPorts, /* outPorts */
                &Service_policer, /* policer  */
                &Service_editing) != MEA_OK)
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaSetFilterTypeToPortId Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",aclParam->serviceId);
      MEA_OS_free(Service_editing.ehp_info);
      return ENET_FAILURE;
    }

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Removing filter %d\n", aclParam->serviceId);
    adap_memcpy(&Service_data.ACL_filter_info, &aclParam->ACL_filter_info, sizeof(aclParam->ACL_filter_info));
    adap_memcpy(&Service_data.ACL_filter_info.data_info, &aclParam->ACL_filter_info.data_info, sizeof(aclParam->ACL_filter_info.data_info));

    Service_data.filter_enable =aclParam->filter_enable;
    Service_data.ACL_Prof_valid = aclParam->ACL_Prof_valid;
    Service_data.ACL_Prof = aclParam->ACL_Prof;

    if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
            aclParam->serviceId,
                &Service_data,
                &Service_outPorts,
                &Service_policer,
                &Service_editing) != MEA_OK)
    {
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaSetFilterTypeToPortId Error: ENET_ADAPTOR_Set_Service failed for service:%d\n", aclParam->serviceId);
      MEA_OS_free(Service_editing.ehp_info);
      return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static
ADAP_Int8
EnetHal_HwL2AclFilterSelection (tEnetHal_L2FilterEntry * pL2FilterEntry, tAdapAclFilterType *tAclType, ADAP_Uint32 *numOfAclReq)
{
    ADAP_Int8 ret = ENET_SUCCESS;
    /* Selection of filters is based on the incoming field values */
    if (adap_is_mac_zero(&pL2FilterEntry->L2FilterSrcMacAddr) == ADAP_FALSE)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_SA_MAC_KEY_TYPE;
        adap_memcpy(&tAclType->src_mac,&pL2FilterEntry->L2FilterSrcMacAddr, sizeof(tEnetHal_MacAddr));
        if(pL2FilterEntry->FilterSrcMacAddrMask != 0)
            tAclType->SourceMacAddrMask = pL2FilterEntry->FilterSrcMacAddrMask;
        else
            tAclType->SourceMacAddrMask =0xFFFFFFFFFFFF;
    }
    if (adap_is_mac_zero(&pL2FilterEntry->L2FilterDstMacAddr) == ADAP_FALSE)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_DA_MAC_KEY_TYPE;
        adap_memcpy(&tAclType->da_mac,&pL2FilterEntry->L2FilterDstMacAddr, sizeof(tEnetHal_MacAddr));
        if(pL2FilterEntry->FilterDstMacAddrMask != 0)
            tAclType->DestMacAddrMask = pL2FilterEntry->FilterDstMacAddrMask;
        else
            tAclType->DestMacAddrMask =0xFFFFFFFFFFFF;

    }

    if(pL2FilterEntry->u2OuterEtherType != 0)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_OUTER_ETHTYPE_KEY_TYPE;
        tAclType->outerEtherType = pL2FilterEntry->u2OuterEtherType;
    }
    if (pL2FilterEntry->filterCustomerVlanId != 0)
    {
        //*numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_CVLAN_KEY_TYPE;
        tAclType->cVlanId = pL2FilterEntry->filterCustomerVlanId;
    }
    if (pL2FilterEntry->filterServiceVlanId != 0)
    {
        //*numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_SVLAN_KEY_TYPE;
        tAclType->sVlanId = pL2FilterEntry->filterServiceVlanId;
    }
    if(pL2FilterEntry->u2InnerEtherType != 0)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_INNER_ETHTYPE_KEY_TYPE;
        tAclType->innerEtherType = pL2FilterEntry->u2InnerEtherType;
    }
    if(pL2FilterEntry->FilterCVlanPriority != -1)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_CVLAN_PRIORITY_KEY_TYPE;
        tAclType->cvlanPriority = pL2FilterEntry->FilterCVlanPriority;
    }
    if(pL2FilterEntry->FilterSVlanPriority != -1)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_SVLAN_PRIORITY_KEY_TYPE;
        tAclType->svlanPriority = pL2FilterEntry->FilterSVlanPriority;

   }
    if(pL2FilterEntry->filterProtocolType != 0)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_L2_PROTOCOL_KEY_TYPE;
        tAclType->l2ProtocolType = pL2FilterEntry->filterProtocolType;
    }
    return ret;

}

static
ADAP_Uint8
EnetHal_HwL3AclFilterSelection (tEnetHal_L3FilterEntry * pL3FilterEntry, tAdapAclFilterType *tAclType, ADAP_Uint32 *numOfAclReq)
{
    /* Selection of filters is based on the incoming field values */
    if (pL3FilterEntry->FilterDstIpAddr != 0)
    {
        *numOfAclReq = *numOfAclReq + 1;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set dest ip address\r\n");
        tAclType->bitmask |= ADAP_FILTER_DEST_IPV4_KEY_TYPE;
        tAclType->destIpv4= pL3FilterEntry->FilterDstIpAddr;
        if(pL3FilterEntry->FilterDstIpAddrMask != 0)
            tAclType->destIpv4Mask = pL3FilterEntry->FilterDstIpAddrMask;
        else
            tAclType->destIpv4Mask =0xFFFFFFFF;
    }
    if (pL3FilterEntry->FilterSrcIpAddr != 0)
    {
        *numOfAclReq = *numOfAclReq + 1;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set source ip address\r\n");
        tAclType->bitmask |= ADAP_FILTER_SOURCE_IPV4_KEY_TYPE;
        tAclType->srcIpv4= pL3FilterEntry->FilterSrcIpAddr;
        if(pL3FilterEntry->FilterSrcIpAddrMask != 0)
            tAclType->SourceIpv4Mask = pL3FilterEntry->FilterSrcIpAddrMask;
        else
            tAclType->SourceIpv4Mask =0xFFFFFFFF;
    }
    if (pL3FilterEntry->FilterMinSrcProtPort != 0)
 //   if (pL3FilterEntry->FilterMinSrcProtPort != 0 ||
 //       pL3FilterEntry->FilterMaxSrcProtPort != 65535)
    {
        tAclType->bitmask |= ADAP_FILTER_SOURCE_PORT_KEY_TYPE;
        tAclType->SourcePort = pL3FilterEntry->FilterMinSrcProtPort;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set src port :%d\r\n",tAclType->SourcePort);
    }
    if (pL3FilterEntry->FilterMinDstProtPort != 0)
 //   if (pL3FilterEntry->FilterMinDstProtPort != 0 ||
 //       pL3FilterEntry->FilterMaxDstProtPort != 65535)
    {
        tAclType->bitmask |= ADAP_FILTER_DEST_PORT_KEY_TYPE;
        tAclType->DestPort = pL3FilterEntry->FilterMinDstProtPort;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set dest port :%d\r\n",tAclType->DestPort);
    }
    if (adap_is_ipv6_zero(&pL3FilterEntry->ipv6SrcIpAddress) == ADAP_FALSE)
    {
       *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_SOURCE_IPV6_KEY_TYPE;
        adap_memcpy(&tAclType->srcIpv6, &pL3FilterEntry->ipv6SrcIpAddress, sizeof(tEnetHal_IPv6Addr));
    }
    if (adap_is_ipv6_zero(&pL3FilterEntry->ipv6DstIpAddress) == ADAP_FALSE)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_DEST_IPV6_KEY_TYPE;
        adap_memcpy(&tAclType->dstIpv6, &pL3FilterEntry->ipv6DstIpAddress, sizeof(tEnetHal_IPv6Addr));
    }
    if (pL3FilterEntry->L3FilterProtocol != ENETHAL_PROTO_ANY)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set source ip proto\r\n");
        if(*numOfAclReq < 4)
        {
            tAclType->bitmask |= ADAP_FILTER_IP_PROTOCOL_KEY_TYPE;
            tAclType->ipProto = pL3FilterEntry->L3FilterProtocol;
        }
        else
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ip protocol not set bacause there is no ACL room\r\n");
        }
    }
    if (pL3FilterEntry->CVlanId != 0)
    {
        tAclType->bitmask |= ADAP_FILTER_CVLAN_KEY_TYPE;
        tAclType->cVlanId = pL3FilterEntry->CVlanId;
    }
    if (pL3FilterEntry->SVlanId != 0)
    {
        tAclType->bitmask |= ADAP_FILTER_SVLAN_KEY_TYPE;
        tAclType->sVlanId = pL3FilterEntry->SVlanId;
    }
    if (pL3FilterEntry->CVlanPriority != -1)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_CVLAN_PRIORITY_KEY_TYPE;
        tAclType->cvlanPriority = pL3FilterEntry->CVlanPriority;
    }
    if (pL3FilterEntry->SVlanPriority != -1)
    {
        *numOfAclReq = *numOfAclReq + 1;
        tAclType->bitmask |= ADAP_FILTER_SVLAN_PRIORITY_KEY_TYPE;
        tAclType->svlanPriority = pL3FilterEntry->SVlanPriority;
    }
    return ENET_SUCCESS;
}

static ADAP_Uint32 RemoveAclFilters(tAdap_FilterEntry *entry, ADAP_Uint32 acl_profile)
{
    MEA_Bool exist;
    MEA_Filter_t filterId;
    int i, j;
    for (j = 0; j < MEA_ACL_HIERARC_NUM_OF_FLOW; j++)
        for (i = 0; i < entry->tAclParams.num_of_filters; i++)
        {
            entry->tAclParams.arrFilterKey[i].interface_key.src_port_valid = MEA_FALSE;
            entry->tAclParams.arrFilterKey[i].interface_key.src_port = 0;
            entry->tAclParams.arrFilterKey[i].interface_key.sid_valid = MEA_TRUE;
            entry->tAclParams.arrFilterKey[i].interface_key.acl_prof  = acl_profile;
            entry->tAclParams.arrFilterKey[i].Acl_hierarchical_id = j;
            if (ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_UNIT_0,
                    &entry->tAclParams.arrFilterKey[i],
                    0,
                    &exist,
                    &filterId) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                        "RemoveAclFilter Error: ENET_ADAPTOR_IsExist_Filter_ByKey failed\n");
                return ENET_FAILURE;
            }
            if (exist == MEA_TRUE)
            {
                if(ENET_ADAPTOR_Delete_Filter (MEA_UNIT_0, filterId)!= MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                            "RemoveAclFilter Error: ENET_ADAPTOR_Delete_Filter failed for filter %d\n", filterId);
                    return ENET_FAILURE;
                }
            }
        }
    return ENET_SUCCESS;
}

static ADAP_Uint32 ClearAclServiceInfo(ADAP_Uint32 acl_profile, ADAP_Uint32 filterId)
{
        tAdapAclProfile *prof = AdapGetAclProfilePtr(acl_profile, filterId);
        int i;
        if (!prof)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ACL - filternoid %d  Error\r\n",acl_profile);
                           return ENET_FAILURE;
        }

        for (i=0; i< MAX_ACL_PROF_NUM; i++)
        {
            if (prof->servicePrevFilt[i].valid)
            {
                if (prof->servicePrevFilt[i].serviceCreated == MEA_TRUE)
                {
                    MeaDeleteCopiedEnetServiceId(prof->servicePrevFilt[i].serviceId);
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",prof->servicePrevFilt[i].serviceId);
                    prof->servicePrevFilt[i].valid = MEA_FALSE;
                }
                else
                {
                    if(RemoveFilterFromService(&prof->servicePrevFilt[i]) != ENET_SUCCESS)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error restoring params\n");
                        return ENET_FAILURE;
                    }
                }
            }
        }
    return ENET_SUCCESS;
}

static int NotIncRule(tUpdateServiceParam *mainParams, tUpdateServiceParam *addingParams)
{
    int i,j;
    ADAP_Bool inc;
    for (i=0; i<MEA_ACL_HIERARC_NUM_OF_FLOW; i++)
    {
        inc = ADAP_FALSE;
        if (addingParams->x.filterPararms.ACL_filter_info.data_info[i].valid == MEA_FALSE)
            continue;

        for (j=0; j<MEA_ACL_HIERARC_NUM_OF_FLOW; j++)
        {
            if ((mainParams->x.filterPararms.ACL_filter_info.data_info[j].valid == MEA_TRUE) &&
               (addingParams->x.filterPararms.ACL_filter_info.data_info[i].filter_key_type ==
               mainParams->x.filterPararms.ACL_filter_info.data_info[j].filter_key_type))
            {
                inc = ADAP_TRUE;
                break;
            }
        }
        if (inc == ADAP_FALSE)
            return i;
    }
    return -1;
}

static ADAP_Uint32 GetAclProfile( struct slFiltParams *filtParams, ADAP_Uint32 *acl_profile)
{
    ADAP_Uint32 priField=0;
    if (filtParams->tAclType.cvlanPriority != EMPTY_VLAN_PRIORITY)
    {
        priField = filtParams->tAclType.cvlanPriority;
    }
    else if (filtParams->tAclType.tos != ENETHAL_TOS_NONE)
    {
        priField = filtParams->tAclType.tos;
    }
    else if (filtParams->tAclType.dscp != -1)
    {
        priField = filtParams->tAclType.dscp;
    }

    *acl_profile = AdapGetAclProfileFtype(filtParams->filterId, filtParams->ftype, filtParams->inPort);
    if (*acl_profile == 0xFFFFFFFF)
        *acl_profile = AdapAllocAclProfileFilterIdFtype(filtParams->filterId, filtParams->ftype, filtParams->inPort,
                filtParams->tAclType.cVlanId, priField);

    if (*acl_profile == 0xFFFFFFFF)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error getting acl profile\n");
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

static
ADAP_Uint32 SaveMergedServiceInfo(struct slFiltParams *mergedParams, ADAP_Uint32 acl_profile)
{
    tAdapAclProfile *prof;
    tAdap_FilterEntry *entry;
    int i;
    //Saving service information
    prof = AdapGetAclProfilePtr(acl_profile, mergedParams->filterId);
    for (i=0; i<MAX_ACL_PROF_NUM; i++)
    {
        if (((prof->servicePrevFilt[i].valid == MEA_TRUE) &&
            (prof->servicePrevFilt[i].serviceId == mergedParams->tServiceParams.Service)) ||
            (prof->servicePrevFilt[i].valid == MEA_FALSE))
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Saving config for serviceID = %d\n", mergedParams->tServiceParams.Service);
            prof->servicePrevFilt[i].valid = MEA_TRUE;
            prof->servicePrevFilt[i].serviceId = mergedParams->tServiceParams.Service;
            prof->servicePrevFilt[i].serviceCreated =  mergedParams->tServiceParams.serviceCreated;
            prof->servicePrevFilt[i].ACL_Prof = 0;
            prof->servicePrevFilt[i].ACL_Prof_valid = MEA_FALSE;
            prof->servicePrevFilt[i].filter_enable = MEA_FALSE;
            //Saving previous state as 0.
            adap_memset(&prof->servicePrevFilt[i].ACL_filter_info, 0,
                        sizeof(prof->servicePrevFilt[i].ACL_filter_info));
            adap_memset(&prof->servicePrevFilt[i].ACL_filter_info.data_info, 0,
                        sizeof(prof->servicePrevFilt[i].ACL_filter_info.data_info));
            //Updating tAclParams.

            entry = Adap_GetFilterEntryFromLinkList(mergedParams->filterId, mergedParams->ftype);
            if (!entry)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR: Can't file filter entry\n");
                return ENET_FAILURE;
            }

            adap_memcpy(&entry->tAclParams, &mergedParams->tAclParams, sizeof(entry->tAclParams));
            break;
        }
    }
    return ENET_SUCCESS;
}

static ADAP_Uint32 MergeServiceParams(struct slServiceList *serviceEntry, struct slFiltParams *mergedParams)
{
    struct slFiltParams *np;
    int firstSet = 0, i, j;
    eEnetHal_FilterAction basicAction;

    if (STAILQ_EMPTY(&serviceEntry->filtParams))
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FilterParams can not be empty for service %d\n", serviceEntry->serviceId);
        return ENET_FAILURE;
    }
    STAILQ_FOREACH(np, &serviceEntry->filtParams, entries)
    {
        if(!firstSet)
        {
            firstSet = 1;
            basicAction = np->FilterAction;
            adap_memcpy(mergedParams, np, sizeof(*mergedParams));\
            mergedParams->tAclParams.filterNoId = np->filterId;
            continue;
        }

        // Drop all filters that does not fit to basic action
        // Apply filters that are configured as COPYTO_CPU
        if ((np->FilterAction != basicAction)
            && (np->FilterAction != ENETHAL_SWITCH_COPYTOCPU))
            continue;

        for (i=0; i<MEA_ACL_HIERARC_NUM_OF_FLOW; i++)
        {
            if (mergedParams->tServiceParams.x.filterPararms.ACL_filter_info.data_info[i].valid == MEA_FALSE)
            {

                j = NotIncRule(&mergedParams->tServiceParams, &np->tServiceParams);
                if (j == -1)
                    break;

                mergedParams->tServiceParams.x.filterPararms.ACL_filter_info.data_info[i].filter_key_type =
                    np->tServiceParams.x.filterPararms.ACL_filter_info.data_info[j].filter_key_type;
                mergedParams->tServiceParams.x.filterPararms.ACL_filter_info.data_info[i].mask.mask_0_31 =
                    np->tServiceParams.x.filterPararms.ACL_filter_info.data_info[j].mask.mask_0_31;
                mergedParams->tServiceParams.x.filterPararms.ACL_filter_info.data_info[i].mask.mask_32_63 =
                    np->tServiceParams.x.filterPararms.ACL_filter_info.data_info[j].mask.mask_32_63;
                mergedParams->tServiceParams.x.filterPararms.ACL_filter_info.data_info[i].valid = MEA_TRUE;
                if (mergedParams->tAclParams.num_of_filters < i + 1)
                {
                    mergedParams->tAclParams.num_of_filters = i + 1;
                    adap_memcpy(&mergedParams->tAclParams.arrFilterKey[i],&np->tAclParams.arrFilterKey[j],sizeof(MEA_Filter_Key_dbt));
                }

                if ((np->policy.valid == ADAP_TRUE) && (mergedParams->policy.valid == ADAP_FALSE))
                {
                    mergedParams->policy = np->policy;
                }
            }
        }
    }

    return ENET_SUCCESS;
}

static
ADAP_Uint32 AclGetOutPorts (tUpdateServiceParam *tParams, tAclFilterParams *tAclParams)
{
    MEA_Service_Entry_Data_dbt             Service_data;
    MEA_OutPorts_Entry_dbt                 Service_outPorts;
    MEA_Policer_Entry_dbt                  Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Service_editing;

    if (tAclParams->action_type != MEA_FILTER_ACTION_TO_ACTION)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO, "No need to set outports. Action type != actio to action\n");
        return ENET_SUCCESS;
    }

    adap_memset(&Service_data  , 0 , sizeof(Service_data ));
    adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
    adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
    adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
                  tParams->Service,
                  NULL,
                  &Service_data,
                  &Service_outPorts,
                  &Service_policer,
                  &Service_editing) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"AclGetOutPorts Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", tParams->Service);
        return ENET_FAILURE;
    }

    adap_memcpy(&tAclParams->outPorts, &Service_outPorts, sizeof(Service_outPorts));

    return ENET_SUCCESS;
}

ADAP_Uint32 RemoveDuplicatingEntries(struct tVlanSubProtocolEntry *first)
{
    struct tVlanSubProtocolEntry *np, *ns, *tmp;

    if (!first)
        return ENET_FAILURE;

    for (np = first; np != NULL; np = LIST_NEXT(np, entries))
    {
        ns = np->entries.le_next;
        while (ns != NULL)
        {
            if ((np->subProtocolType == ns->subProtocolType) &&
                (np->L2ProtocolType == ns->L2ProtocolType) &&
                (np->vlanId == ns->vlanId))
            {
                tmp = LIST_NEXT(ns, entries);
                LIST_REMOVE(ns, entries);
                free(ns);
                ns = tmp;
            }
            else
            {
                ns = LIST_NEXT(ns, entries);
            }
        }
    }
    return ENET_SUCCESS;
}

static
struct slServiceList *GetListEntryForService(MEA_Service_t serviceId, slServiceListHead *srvHead)
{
    struct slServiceList *np;
    struct slServiceList *listEntry;
    if (!STAILQ_EMPTY(srvHead))
    {
        STAILQ_FOREACH(np, srvHead, entries)
        {
            if (np->serviceId == serviceId)
                return np;
        }
    }

    listEntry = adap_malloc(sizeof(*listEntry));
    adap_memset(listEntry, 0, sizeof(*listEntry));
    listEntry->serviceId = serviceId;

    STAILQ_INIT(&listEntry->filtParams);
    if (STAILQ_EMPTY(srvHead))
        STAILQ_INSERT_HEAD(srvHead, listEntry, entries);
    else
        STAILQ_INSERT_TAIL(srvHead, listEntry, entries);

    return listEntry;
}


static
ADAP_Uint32 SaveServiceInfo(tUpdateServiceParam *tServiceParams, tAclFilterParams *tAclParams, tAdapAclFilterType *tAclType,
        eEnetHal_FilterAction FilterAction, slServiceListHead *srvHead, tPolicyDb *pPolicy, ADAP_Uint32 filterNoId, eAclFilterTypes ftype,
        ADAP_Uint32 inPort)
{
    struct slFiltParams *params;
    tAdap_FilterEntry *entry;
    //Getting item from the service list
    struct slServiceList *listEntry = GetListEntryForService(tServiceParams->Service, srvHead);
    if (!listEntry)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't get entry from service %d", tServiceParams->Service);
        return ENET_FAILURE;
    }

    params = adap_malloc(sizeof(*params));
    if (!params)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error in malloc\n");
        return ENET_FAILURE;
    }
    adap_memcpy(&params->tAclParams, tAclParams, sizeof(*tAclParams));
    adap_memcpy(&params->tServiceParams, tServiceParams, sizeof(*tServiceParams));
    adap_memcpy(&params->tAclType, tAclType, sizeof(params->tAclType));
    adap_memcpy(&params->policy, pPolicy, sizeof(params->policy));
    params->filterId = filterNoId;
    params->ftype = ftype;
    params->inPort = inPort;

    params->FilterAction = FilterAction;

    //Saving filter configuration
    entry = Adap_GetFilterEntryFromLinkList(filterNoId, ftype);
    if (!entry)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error getting filterEntry\n");
        adap_safe_free(params);
        return ENET_FAILURE;
    }
    adap_memcpy(&entry->tAclParams, tAclParams, sizeof(entry->tAclParams));
    if (STAILQ_EMPTY(&listEntry->filtParams))
        STAILQ_INSERT_HEAD(&listEntry->filtParams, params, entries);
    else
        STAILQ_INSERT_TAIL(&listEntry->filtParams, params, entries);

    return ENET_SUCCESS;
}

static
ADAP_Uint32 EnetHal_HwCreateFiltersToService(struct slFiltParams *filtParams, ADAP_Uint32 acl_profile)
{
    MEA_Action_Entry_Data_dbt Action_data;
    ADAP_Uint32 index=0;
    ADAP_Uint32 destIp=0;
    tAclFilterParams *tAclParams = &filtParams->tAclParams;
    tAdapAclFilterType *tAclType = &filtParams->tAclType;
    tPolicyDb *pPolicy = &filtParams->policy;
    tAclParams->EHP_Entry.num_of_entries=0;
    tAclParams->EHP_Entry.ehp_info = NULL;

    /* need to create metering */
    if ((pPolicy != NULL) && (pPolicy->valid == ADAP_TRUE))
    {
        tMeterDb *meter = NULL;
        meter = MeaAdapGetMeterByIndex(pPolicy->MeterIdx);
        if(meter == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get Meter for Index: %d!!!\n", pPolicy->MeterIdx);
            return ENET_FAILURE;
        }
        tAclParams->enetPolilcingProfile =  meter->policer_prof_id;  /// Alex
        tAclParams->isPolicerSet = ADAP_TRUE;

        /* For DestIP ACL update the forwarder with Action - Permit/Drop and set policer */
        if(tAclType->bitmask & ADAP_FILTER_DEST_IPV4_KEY_TYPE)
        {
            if (tAclType->destIpv4Mask == 0xffffffff)
            {
                adap_memset(&Action_data, 0 , sizeof(Action_data));
                if (MeaDrvSetPolicerInDestIpForwarder (tAclType->destIpv4,
                    START_L3_VPN, meter->policer_prof_id, &Action_data) != MEA_TRUE)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to set on forwarder for meter: %d!!!\n",
                        pPolicy->MeterIdx);
                }
                if (MeaDrvSetPolicerInDestIpForwarder (tAclType->destIpv4,
                    START_L3_VPN1, meter->policer_prof_id, &Action_data) != MEA_TRUE)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to set on forwarder for meter: %d!!!\n",
                        pPolicy->MeterIdx);
                }
            }
	    else if (tAclType->destIpv4Mask == 0xffffff00)
            {
                for (index=1; index<=MAX_NUM_OF_IP_CLASS_ENTRIES; index++)
                {
                    adap_memset(&Action_data, 0 , sizeof(Action_data));
                    destIp = tAclType->destIpv4 + index;
		    if (MeaDrvSetPolicerInDestIpForwarder (destIp,START_L3_VPN,
                                 meter->policer_prof_id, &Action_data) == MEA_TRUE)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"policer set on forwarder for meter: %d!!!\n",
                            pPolicy->MeterIdx);
                    }
		    if (MeaDrvSetPolicerInDestIpForwarder (destIp,START_L3_VPN1,
                                 meter->policer_prof_id, &Action_data) == MEA_TRUE)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"policer set on forwarder for meter: %d!!!\n",
                            pPolicy->MeterIdx);
                    }
                }
            }
        }
    } else
    {
        tAclParams->isPolicerSet = ADAP_FALSE;
        tAclParams->enetPolilcingProfile = EMPTY_POLICING_PROFILE;
    }
 //   tAclParams.num_of_filters=MaxNumOfAcl;
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"number of ACLs %d\r\n",tAclParams->num_of_filters);

    if(adap_SetAclFilterConfiguration(tAclParams, 0, acl_profile) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create filters\r\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static
ADAP_Uint32 EnetHal_HwApplyFilterToService(struct slServiceList *serviceEntry)
{
    struct slFiltParams mergedParams;
    ADAP_Uint32 acl_profile;
    adap_memset(&mergedParams, 0, sizeof(mergedParams));
    // Merging service configuration
    if(MergeServiceParams(serviceEntry, &mergedParams) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Failed to merge params\r\n");
        return ENET_FAILURE;
    }
    // Getting ACL profile based on mergedParams
    if(GetAclProfile(&mergedParams, &acl_profile) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Failed to get acl profile\r\n");
        return ENET_FAILURE;
    }
    mergedParams.tServiceParams.x.filterPararms.ACL_Prof_valid=ADAP_TRUE;
    mergedParams.tServiceParams.x.filterPararms.ACL_Prof = acl_profile;
    mergedParams.tAclParams.acl_profile_filt.acl_profile = acl_profile;
    mergedParams.tAclParams.acl_profile_filt.valid = MEA_TRUE;

    if(SaveMergedServiceInfo(&mergedParams, acl_profile) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to save merged parameters\r\n");
        return ENET_FAILURE;
    }
    // Post service changes
    if(MeaDrvUpdateService (&mergedParams.tServiceParams) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to update service\r\n");
        return ENET_FAILURE;
    }
    // create filters
    if(EnetHal_HwCreateFiltersToService(&mergedParams, acl_profile) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Failed to create filters\r\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static
ADAP_Uint32 ConvertFilterAction(eEnetHal_FilterAction filterAction, MEA_Filter_Action_te *action_type)
{
    switch (filterAction)
    {
    case ENETHAL_DROP:
        *action_type = MEA_FILTER_ACTION_DISCARD;
        return ENET_SUCCESS;
    case ENETHAL_ALLOW:
        *action_type = MEA_FILTER_ACTION_TO_ACTION;
        return ENET_SUCCESS;
    case ENETHAL_SWITCH_COPYTOCPU:
        *action_type = MEA_FILTER_ACTION_SEND_TO_CPU;
        return ENET_SUCCESS;
    default:
        return ENET_FAILURE;
  }
}

static
ADAP_Int32 EnetHal_HwUpdateFilter (ADAP_Uint32 filterNoId, tAdapAclFilterType *tAclType, ADAP_Uint32 numOfAclReg,
        slServiceListHead *srvHead, eEnetHal_FilterAction FilterAction, ADAP_Uint8 *InPortList, eAclFilterTypes ftype, tPolicyDb *pPolicy)
{
    ADAP_Uint32 ret = ENET_SUCCESS;
    tAclFilterParams tAclParams;
    MEA_ACL_mask_dbt arrAclMask[MEA_ACL_HIERARC_NUM_OF_FLOW];
    unsigned int MaxNumOfAcl=MEA_ACL_HIERARC_NUM_OF_FLOW;
    tUpdateServiceParam tParams, tServiceParams;
    ADAP_Bool isCommonRule;
    ADAP_Uint8 inPort;
    MEA_EHP_Info_dbt acl_ehp_info;
    ADAP_Bool isDenyAnyAny = ADAP_FALSE;
    ADAP_Bool isPermitAnyAny = ADAP_FALSE;
    int index;
    //Clean structures
    adap_memset(&tAclParams,0,sizeof(tAclFilterParams));
    adap_memset(&acl_ehp_info,0,sizeof(MEA_EHP_Info_dbt));
    adap_memset(&arrAclMask, 0, sizeof(MEA_ACL_mask_dbt) * MEA_ACL_HIERARC_NUM_OF_FLOW);

    isCommonRule = (tAclType->bitmask & ADAP_FILTER_CVLAN_KEY_TYPE) ? ADAP_FALSE : ADAP_TRUE;
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Common Rule %d\n", isCommonRule);
    // Deny any any and Allow any any situation
    if ((tAclType->bitmask == ADAP_FILTER_CVLAN_KEY_TYPE) || (tAclType->bitmask == 0))
    {
        if (FilterAction == ENETHAL_DROP)
            isDenyAnyAny = ADAP_TRUE;
        else
            isPermitAnyAny = ADAP_TRUE;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IsDeny = %d IsPermit = %d\n", isDenyAnyAny, isPermitAnyAny);
    }

    tAclParams.acl_profile_filt.valid = MEA_TRUE;
    tAclParams.filterCustomerVlanId = tAclType->cVlanId;
    tAclParams.filterServiceVlanId = tAclType->sVlanId;
    tAclParams.cVlanPriority = tAclType->cvlanPriority;
    tAclParams.tos = tAclType->tos;
    tAclParams.dscp = tAclType->dscp;
    ret = ConvertFilterAction(FilterAction, &tAclParams.action_type);
    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: Wrong FilterAction %d\n", FilterAction);
        return ENET_FAILURE;
    }
    //TODO remove this line
//    tAclParams.action_type = (FilterAction == ENETHAL_DROP) ? MEA_FILTER_ACTION_DISCARD : MEA_FILTER_ACTION_TO_ACTION;

    if ((isDenyAnyAny == ADAP_TRUE) && (ftype == FILTER_TYPE_L2))
    {
        // Set sa mac
        tAclType->bitmask |= ADAP_FILTER_SA_MAC_KEY_TYPE;

        arrAclMask[0].filter_key_type = ENET_L2FLTR_SRC_MAC_ADD_FIELD;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"use default configuration with SA_MAC\r\n");
        tAclParams.arrFilterKey[0].layer2.ethernet.SA_valid = MEA_TRUE;
        adap_memset(&tAclParams.arrFilterKey[0].layer2.ethernet.SA.b[0],0,6);

        arrAclMask[0].valid = MEA_TRUE;
        arrAclMask[0].filter_key_type =  MEA_FILTER_KEY_TYPE_SA_MAC;
        arrAclMask[0].mask.mask_0_31 = 0x00000000;
        arrAclMask[0].mask.mask_32_63 = 0xF0000000;

        // bits 48 to 51 filter key
        arrAclMask[0].mask.mask_32_63 |=
              ( ( (MEA_Uint32)arrAclMask[0].filter_key_type) & 0xF) << 16;

        arrAclMask[0].mask.mask_32_63 |= ((MEA_Uint32)0x1FF << 20);
        MaxNumOfAcl = 1;
    } else if ((ftype == FILTER_TYPE_L3) &&
            ((isDenyAnyAny == ADAP_TRUE) || (isPermitAnyAny == ADAP_TRUE)))
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"requested filter on appl ethtype\r\n");

        tAclParams.arrFilterKey[0].layer3.ip_protocol_valid=MEA_TRUE;
        tAclParams.arrFilterKey[0].layer3.ip_protocol=0;
        tAclParams.arrFilterKey[0].layer3.ethertype3 = 0x800;
        tAclParams.arrFilterKey[0].layer3.ethertype3_valid = MEA_TRUE;
        tAclParams.arrFilterKey[0].layer4.dst_port_valid = MEA_TRUE;
        tAclParams.arrFilterKey[0].layer4.dst_port = 0;
        tAclParams.arrFilterKey[0].layer2.outer_vlanTag.priority_valid = MEA_TRUE;

        arrAclMask[0].filter_key_type = MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT;
        arrAclMask[0].mask.mask_0_31 = 0xffff << 16;
        arrAclMask[0].mask.mask_32_63 = 0xffff << 16;
        MaxNumOfAcl = 1;
    } else if (isPermitAnyAny == ADAP_TRUE)
    {
        //NOTE No need to create filters for permit any any.
        MaxNumOfAcl = 0;
    } else
    {
        // setting filter mask parameters
        if(adap_SetAclFilterMaskParameters(tAclType,&arrAclMask[0],&tAclParams.arrFilterKey[0],&MaxNumOfAcl) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: ACL - number of ACLs is bigger then MAX\n");
            return ENET_FAILURE;
        }
    }

    // collect the service filter parameter
    adap_memset(&tParams,0,sizeof(tParams));
    // Note: filter_mode_OR_AND_type can set the following parameters:
    // 0 - A || B || C || D (Suitable for 1 rule)
    // 1 - A && B || C || D (Suitable for 2 rules)
    // 2 - (A && B && C && D) || ( A && B && C) || (A && B) || (A) - not used for now
    // 3 - (A && B && C && D) - suitable for 3 or more rules
    tParams.x.filterPararms.ACL_filter_info.filter_mode_OR_AND_type = (MaxNumOfAcl - 1 < 2) ? MaxNumOfAcl - 1: 3;

    if (FilterAction == ENETHAL_DROP)
    {
        tParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable = MEA_FALSE;
    }
    else
    {
        tParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable = MEA_FALSE;
    }

    tParams.mode = E_SET_SERVICE_CONFIGURATION;
    tParams.type = E_UPDATE_FILTER_PARAMETERS;
    tParams.x.filterPararms.filter_enable=ADAP_TRUE;
    // set the mask parameters according to adap_SetAclFilterParameters
    for(index=0;index<MaxNumOfAcl;index++)
    {
        tParams.x.filterPararms.ACL_filter_info.data_info[index].filter_key_type=arrAclMask[index].filter_key_type;
        tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_0_31 = arrAclMask[index].mask.mask_0_31;
        tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_32_63 = arrAclMask[index].mask.mask_32_63;
        tParams.x.filterPararms.ACL_filter_info.data_info[index].valid = MEA_TRUE;
    }

    for(inPort = 1; inPort <= MeaAdapGetNumOfPhyPorts(); inPort++)
    {
        if (InPortList[inPort])
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Processing port %d\n", inPort);
            struct tVlanSubProtocolEntry *np;
            ADAP_Uint8 masterPort = inPort;
            struct tVlanSubProtocolEntry *n;
            ADAP_Bool headPosted = ADAP_FALSE;
            tServiceDb * srv;
            struct tAclServiceEntry serviceEntry;
            ADAP_Bool srvFound = ADAP_FALSE;
            ADAP_Bool pvidIsSet = ADAP_FALSE;
            //Fill the queue of acl services
            LIST_HEAD(listhead, tVlanSubProtocolEntry) head = LIST_HEAD_INITIALIZER(head);
            LIST_INIT(&head);

            if(adap_IsPortChannel(inPort) == ENET_SUCCESS)
            {
                if( (adap_NumOfLagPortFromAggrId(inPort)) > 0)
                {
                    masterPort = adap_GetLagMasterPort(inPort);
                }
            }
            if (tAclParams.filterCustomerVlanId == getPortToPvid(masterPort))
            {
                pvidIsSet = ADAP_TRUE;
            }

            // Getting vlan list
            srv = MeaAdapGetFirstServicePort(masterPort);
            while (srv)
            {
                if ((srv->L2_protocol_type < D_PROVIDER_SERVICE_TAG) &&
                    (srv->valid == ADAP_TRUE))
                {
                    if ((isCommonRule == ADAP_TRUE) ||
                        (tAclParams.filterCustomerVlanId == srv->outerVlan))
                    {
                        srvFound = ADAP_TRUE;
                    }
                    else if ((pvidIsSet == ADAP_TRUE) &&
                       (srv->L2_protocol_type == D_UNTAGGED_SERVICE_TAG))
                    {
                        srvFound = ADAP_TRUE;
                    }
                }

                if (srvFound == ADAP_TRUE)
                {
                    srvFound = ADAP_FALSE;
                    n = adap_malloc(sizeof(*n));
                    n->L2ProtocolType = srv->L2_protocol_type;
                    n->subProtocolType = srv->sub_protocol_type;
                    n->vlanId = srv->outerVlan;
                    if (headPosted == ADAP_FALSE)
                    {
                        LIST_INSERT_HEAD(&head, n, entries);
                        headPosted = ADAP_TRUE;
                    }
                    else
                        LIST_INSERT_AFTER(np, n, entries);
                    np = n;
                }
                srv = MeaAdapGetNextServicePort(masterPort, srv->serviceId);
            }

            // Ignore if we haven't found any suitable services
            if (!head.lh_first)
                continue;

            //Removing duplicates from the list:
            if (RemoveDuplicatingEntries(head.lh_first) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"RemoveDuplicatingEntries call failed\n");
                return ENET_FAILURE;
            }

            LIST_FOREACH(np, &head, entries)
            {
                adap_memset(&serviceEntry, 0, sizeof(serviceEntry));
                // Copy tParams to avoid value overwrite
                adap_memcpy(&tServiceParams, &tParams, sizeof(tParams));
                MeaGetServiceIdFromPortId(masterPort, &tAclParams, &serviceEntry, FilterAction, np);

                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Working with ServiceID %d\n", serviceEntry.serviceId);
                tServiceParams.Service = serviceEntry.serviceId;
                tServiceParams.serviceCreated = serviceEntry.serviceCreated;

                if (tServiceParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable != MEA_TRUE)
                {
                    if(AclGetOutPorts (&tServiceParams, &tAclParams) != ENET_SUCCESS)
                    {
                        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get outports\r\n");
                        return ENET_FAILURE;
                    }
                }
                tAclParams.num_of_filters=MaxNumOfAcl;

                if(SaveServiceInfo(&tServiceParams, &tAclParams, tAclType, FilterAction, srvHead, pPolicy, filterNoId,
                    ftype, inPort) != ENET_SUCCESS)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to save service info\r\n");
                    return ENET_FAILURE;
                }
            }

            // Cleanup the list
            n = LIST_FIRST(&head);
            while (n != NULL) {
                np = LIST_NEXT(n, entries);
                free(n);
                n = np;
            }
        }
    }
    return ret;
}

static ADAP_Uint32 HwProcessSVlanId(tAdapAclFilterType *tAclType)
{
    // Overwrite cvlan tag if svlan tag was set
    if (tAclType->bitmask & ADAP_FILTER_SVLAN_KEY_TYPE)
    {
        tAclType->cVlanId = tAclType->sVlanId;
        tAclType->cvlanPriority = tAclType->svlanPriority;
        tAclType->bitmask |= ADAP_FILTER_CVLAN_KEY_TYPE;
    }
    return ENET_SUCCESS;
}

static ADAP_Int32 EnetHal_HwUpdateFilterEntryStruct(tAdap_FilterEntry *entry, slServiceListHead *srvHead)
{
    ADAP_Uint32 numOfAclReq = 0;
    tAdapAclFilterType tAclType;
    //Clean structures
    adap_memset(&tAclType, 0, sizeof(tAclType));
    tAclType.dscp = -1;
    tAclType.tos = ENETHAL_TOS_NONE;
    tAclType.svlanPriority = EMPTY_VLAN_PRIORITY;
    tAclType.cvlanPriority = EMPTY_VLAN_PRIORITY;
    tPolicyDb *pPolicy = &entry->policy;

    if (entry->fType == FILTER_TYPE_L2)
    {
        // Getting acl params
        if (EnetHal_HwL2AclFilterSelection(&entry->x.l2FilterEntry, &tAclType, &numOfAclReq) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: Unsupported parameter in filter entry\n");
            return ENET_FAILURE;
        }
        HwProcessSVlanId(&tAclType);
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Processing filtnoid = %d\n",entry->x.l2FilterEntry.filterNoId);
        return EnetHal_HwUpdateFilter(entry->x.l2FilterEntry.filterNoId, &tAclType, numOfAclReq, srvHead,
                entry->x.l2FilterEntry.FilterAction, entry->x.l2FilterEntry.InPortList, entry->fType, pPolicy);
    }
    else if (entry->fType == FILTER_TYPE_L3)
    {
        // Getting acl params
        if (EnetHal_HwL3AclFilterSelection(&entry->x.l3FilterEntry, &tAclType, &numOfAclReq) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: Unsupported parameter in filter entry\n");
            return ENET_FAILURE;
        }
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Processing l3 filtnoid = %d\n",entry->x.l3FilterEntry.filterNoId);
        tAclType.tos = entry->x.l3FilterEntry.FilterTos;
        tAclType.dscp = entry->x.l3FilterEntry.FilterDscp;
        HwProcessSVlanId(&tAclType);

        return EnetHal_HwUpdateFilter(entry->x.l3FilterEntry.filterNoId, &tAclType, numOfAclReq, srvHead,
                entry->x.l3FilterEntry.FilterAction, entry->x.l3FilterEntry.InPortList, entry->fType, pPolicy);
    }
    return ENET_FAILURE;
}

static
ADAP_Uint32 ClearAclServiceInfoFilterId(ADAP_Uint32 filterNoId, eAclFilterTypes fType)
{
    int inPort;
    for(inPort = 1; inPort <= MeaAdapGetNumOfPhyPorts(); inPort++)
    {
        ADAP_Uint32 acl_profile = AdapGetAclProfileFtype(filterNoId, fType, inPort);

        if (acl_profile == 0xFFFFFFFF)
        {
            continue;
        }

        if (ClearAclServiceInfo(acl_profile,filterNoId) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error clear service info \n");
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}

static
ADAP_Uint32 RemoveAclFilterEntry(ADAP_Uint32 filterNoId, ADAP_Uint32 ftype)
{
    int inPort;

    tAdap_FilterEntry *entry = Adap_GetFilterEntryFromLinkList(filterNoId, ftype);

    for(inPort = 1; inPort < MeaAdapGetNumOfPhyPorts(); inPort++)
    {
        ADAP_Uint32 acl_profile = AdapGetAclProfileFtype(filterNoId, ftype, inPort);
        if (acl_profile == 0xFFFFFFFF)
        {
            continue;
        }
        //Removing all filters
        if (RemoveAclFilters(entry, acl_profile) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error removing acl filter \n");
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}

static
ADAP_Uint32 RemoveAclProfile(tAdap_FilterEntry *entry)
{
    int inPort;
    ADAP_Uint32 filterNoId;

    if (entry->fType == FILTER_TYPE_L3)
    {
        filterNoId = entry->x.l3FilterEntry.filterNoId;
    } else {
        filterNoId = entry->x.l2FilterEntry.filterNoId;
    }

    for(inPort = 1; inPort < MeaAdapGetNumOfPhyPorts(); inPort++)
    {
        ADAP_Uint32 acl_profile = AdapGetAclProfileFtype(filterNoId, entry->fType, inPort);
        if (acl_profile == 0xFFFFFFFF)
        {
            continue;
        }
        if (ENET_SUCCESS != AdapfreeAclProfile(acl_profile,filterNoId))
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error removing acl profile %d \n", acl_profile);
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}


static
ADAP_Uint32 ClearFiltersFromServices()
{
    //Remove all filters for Hw
    tAdap_FilterEntry *entries[MAX_ACL_PROF_NUM];
    tAdap_FilterEntry *entry = Adap_GetFirstAccessListLinkList();
    int i = 0;
    if (entry)
    {
        while (entry)
        {
            entries[i++] = entry;
            entry = Adap_GetNextAccessListLinkList(entry);
        };
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Got num = %d\n", i);
        //pointing to the last element
        if (i > MAX_ACL_PROF_NUM)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Num of entries is greater then MAX\n");
            return ENET_FAILURE;
        }
        i--;
        for (; i>=0; i--)
        {
            if (entries[i]->fType == FILTER_TYPE_L2)
            {
                if (ClearAclServiceInfoFilterId(entries[i]->x.l2FilterEntry.filterNoId, FILTER_TYPE_L2)
                        != ENET_SUCCESS)
                {
                    return ENET_FAILURE;
                }
            }
            if (entries[i]->fType == FILTER_TYPE_L3)
            {
                if (ClearAclServiceInfoFilterId(entries[i]->x.l3FilterEntry.filterNoId, FILTER_TYPE_L3)
                        != ENET_SUCCESS)
                {
                    return ENET_FAILURE;
                }
            }
        }
    }

    return ENET_SUCCESS;
}

ADAP_Int32 adap_HwProcessFilterHierarchy(ADAP_Int32 i4Value, eAclFilterTypes fType,
        tEnetHal_L2FilterEntry *pL2FilterEntry, tEnetHal_L3FilterEntry *pL3FilterEntry, tPolicyDb *pPolicy)
{
    ADAP_Uint32 res = ENET_SUCCESS;
    ADAP_Uint32 filterNoId;
    tAdap_FilterEntry out;
    tAdap_FilterEntry *entry=NULL;
    tAdap_FilterEntry processingEntry;
  //  ADAP_Uint32 filterNoId;
    slServiceListHead srvHead = STAILQ_HEAD_INITIALIZER(srvHead);
    struct slServiceList *np;
    STAILQ_INIT(&srvHead);

    adap_memset(&out, 0, sizeof(tAdap_FilterEntry));
    adap_memset(&processingEntry, 0, sizeof(tAdap_FilterEntry));


    if (ClearFiltersFromServices() != ENET_SUCCESS)
    {
        return ENET_FAILURE;
    }
    switch (fType)
    {
        case FILTER_TYPE_L3:
            res = Adap_L3AccessListToFilterEntry(pL3FilterEntry, pPolicy, &processingEntry);
            filterNoId = pL3FilterEntry->filterNoId;
            break;
        case FILTER_TYPE_L2:
            res = Adap_L2AccessListToFilterEntry(pL2FilterEntry, pPolicy, &processingEntry);
            filterNoId = pL2FilterEntry->filterNoId;
            break;
        default:
            res = ENET_FAILURE;
    }

    if (res != ENET_SUCCESS)
    {
        return ENET_FAILURE;
    }

    res = RemoveAclFilterEntry(filterNoId, fType);

    if (res != ENET_SUCCESS)
    {
        return ENET_FAILURE;
    }
    //Update Linked list
    switch(i4Value)
    {
    case ENETHAL_CREATE_ENET_FILTER:
        // add if not exists
        res = Adap_UpdateFilterEntryInLinkList(&processingEntry);
        if (res != ENET_SUCCESS)
        {
            res = Adap_pushFilterEntryToLinkListSorted(&processingEntry);
        }
        break;
    case ENETHAL_MODIFY_ENET_FILTER:
        res = Adap_UpdateFilterEntryInLinkList(&processingEntry);
        break;
    case ENETHAL_DELETE_ENET_FILTER:
        res = Adap_popFilterEntryLinkList(&processingEntry, &out);
        if (res == ENET_SUCCESS)
        {
            res = RemoveAclProfile(&out);
        }
        break;
    }

    if (res != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error processing filter\n");
        return res;
    }


    //Apply All filters
    entry = Adap_GetFirstAccessListLinkList();

    if (entry)
    {
        while (entry)
        {
            if (EnetHal_HwUpdateFilterEntryStruct(entry, &srvHead) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error, Can't get filter info\n");
                res = ENET_FAILURE;
                goto clear;
            }
            entry = Adap_GetNextAccessListLinkList(entry);
        }

        STAILQ_FOREACH(np, &srvHead, entries)
        {
            if (EnetHal_HwApplyFilterToService(np) != ENET_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error, Can't apply filter info\n");
                res = ENET_FAILURE;
                goto clear;
            }
        }
    }

clear:
    // Cleanup the list
    STAILQ_FOREACH(np, &srvHead, entries)
    {
        struct slFiltParams *a, *b;
        a = STAILQ_FIRST(&np->filtParams);
        while (a != NULL) {
            b = STAILQ_NEXT(a, entries);
            free(a);
            a = b;
        }
        STAILQ_INIT(&np->filtParams);

    }
    {
        struct slServiceList *a, *b;
        a = STAILQ_FIRST(&srvHead);
        while (a != NULL) {
            b = STAILQ_NEXT(a, entries);
            free(a);
            a = b;
        }
        STAILQ_INIT(&srvHead);
    }

    return res;
}

/*
 * Acl rules work behaviour
 *            | L2 traffic    |   L3 traffic |
 * deny ip    |     pass      |      drop    |
 * permit ip  |     drop      |      pass    |
 * deny mac   |     drop      |      drop    |
 * permit mac |     pass      |      pass    |
 * deny tcp   | pass (arp)    | drop (TCP)   |
 * permit tcp | drop (arp)    | pass (TCP)   |
 **/

ADAP_Int32 EnetHal_HwUpdateL2Filter (tEnetHal_L2FilterEntry *pL2FilterEntry, ADAP_Int32 i4Value)
{
    return adap_HwProcessFilterHierarchy(i4Value, FILTER_TYPE_L2, pL2FilterEntry, NULL, NULL);
}

ADAP_Int32 EnetHal_HwUpdateL3Filter (tEnetHal_L3FilterEntry *pL3FilterEntry, ADAP_Int32 i4Value)
{
    return adap_HwProcessFilterHierarchy(i4Value, FILTER_TYPE_L3, NULL, pL3FilterEntry, NULL);
}

/*
 * Implementation of ACL based ARP processing functionality
 */
static
ADAP_Uint32 GetArpVacantFilterId(ADAP_Int32 *id)
{
    tAdap_FilterEntry *baseEntry, *entry = Adap_GetFirstAccessListLinkList();
    ADAP_Uint32 filterId = 1; // Setting filterId by default
    ADAP_Uint32 tmpId;
    if (!entry)
    {
        *id = filterId;
        return ENET_SUCCESS;
    }
    baseEntry = entry;

    while (entry)
    {
        if (entry->fType == FILTER_TYPE_L2)
        {
            tmpId = entry->x.l2FilterEntry.filterNoId;
        }
        else if (entry->fType == FILTER_TYPE_L3)
        {
            tmpId = entry->x.l3FilterEntry.filterNoId;
        } else {

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: inconsistent filter type\n");
            return ENET_FAILURE;
        }

        if (tmpId == filterId)
        {
            filterId++;
            // Increment filterId and go to start
            if (filterId >= (ADAP_Uint32)-1)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: no free filterId\n");
                return ENET_FAILURE;
            }
            entry = baseEntry;
            continue;
        }
        entry = Adap_GetNextAccessListLinkList(entry);
    }
    *id = filterId;
    return ENET_SUCCESS;
}

static
ADAP_Uint32 GetArpVacantFilterPriority(ADAP_Int32 *filterPriority)
{
    tAdap_FilterEntry *entry = Adap_GetFirstAccessListLinkList();
    ADAP_Int32 filterPri = 1; // Setting filterPri by default
    ADAP_Int32 tmpPri;
    if (!entry)
    {
        *filterPriority = filterPri;
        return ENET_SUCCESS;
    }

    while (entry)
    {
        if (entry->fType == FILTER_TYPE_L2)
        {
            tmpPri = entry->x.l2FilterEntry.filterPriority;
        }
        else if (entry->fType == FILTER_TYPE_L3)
        {
            //We set l2 priority so we don't care about l3 priority
            continue;
        } else {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: inconsistent filter type\n");
            return ENET_FAILURE;
        }

        if (tmpPri > filterPri)
            break;

        if (tmpPri == filterPri)
        {
            filterPri++;
            // Increment filterId and go to start
            if (filterPri >= (ADAP_Uint32)-1)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: no free filterPriority\n");
                return ENET_FAILURE;
            }
        }
       entry = Adap_GetNextAccessListLinkList(entry);
    }
    *filterPriority = filterPri;
    return ENET_SUCCESS;
}

static
ADAP_Uint32 GetArpFilterSettings(ADAP_Uint16 vlanId, tEnetHal_L2FilterEntry *entry)
{
    ADAP_Uint32 ret;
    // try to get existing filter from DB
    ret = AdapGetArpFilterParams(vlanId, &entry->filterNoId, &entry->filterPriority);
    if (ret == ENET_SUCCESS)
    {
        // Got filter parameters
        return ENET_SUCCESS;
    }

    // Getting filterId
    ret = GetArpVacantFilterId(&entry->filterNoId);
    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: No free filterId values\n");
        return ENET_FAILURE;
    }
    // Getting filterPriority
    ret = GetArpVacantFilterPriority(&entry->filterPriority);
    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: No free priority values\n");
        return ENET_FAILURE;
    }

    // Create db entry
    ret = AdapCreateArpFilterParams(vlanId, entry->filterNoId, entry->filterPriority);
    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: Creating new DB entry of ArpFilter\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 ConfigureArpRedirection(ADAP_Uint16 vlanId, ADAP_Uint16 portMap[MAX_NUM_OF_SUPPORTED_PORTS],
      ADAP_Bool to_cpu)
{
    tEnetHal_L2FilterEntry entry;
    int i;
    ADAP_Uint32 ret;
    adap_memset(&entry,0,sizeof(entry));
    entry.FilterSVlanPriority = -1;
    entry.FilterCVlanPriority = -1;
    entry.filterProtocolType = ARP_PROTOCOL;
    entry.filterCustomerVlanId = vlanId;
    entry.FilterAction = ENETHAL_SWITCH_COPYTOCPU;
    for (i=0; i<MAX_NUM_OF_SUPPORTED_PORTS; i++)
        entry.InPortList[i] = portMap[i];
    ret = GetArpFilterSettings(vlanId, &entry);
    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get ARP filterId\n");
        return ENET_FAILURE;
    }
    if (to_cpu == ADAP_TRUE)
    {
        return EnetHal_HwUpdateL2Filter(&entry, ENETHAL_CREATE_ENET_FILTER);
    } else
    {
        // Remove DB entry
        ret = AdapRemoveArpFilterParams(vlanId);
        if (ret != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error removing arp filter db entry\n");
            return ENET_FAILURE;
        }
        return EnetHal_HwUpdateL2Filter(&entry, ENETHAL_DELETE_ENET_FILTER);
    }
}

ADAP_Bool ArpRedirectionRulePresent(ADAP_Uint16 vlanId)
{
    ADAP_Int32 filterNoId, filterPriority;
    ADAP_Uint32 ret;
    // try to get existing filter from DB
    ret = AdapGetArpFilterParams(vlanId, &filterNoId, &filterPriority);
    return (ret == ENET_SUCCESS) ? ADAP_TRUE : ADAP_FALSE;
}
//EOF
