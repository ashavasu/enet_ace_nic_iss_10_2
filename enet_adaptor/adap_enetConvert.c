/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <sys/ioctl.h>
//#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_enetConvert.h"

#define USE_SEM_LOCK 1

/**********************************************/
/* ENET_ADAPTOR MEA_API                       */
/**********************************************/
MEA_Status ENET_ADAPTOR_Create_Service(MEA_Unit_t  	unit,
		 	 MEA_Service_Entry_Key_dbt           	*key,
			 MEA_Service_Entry_Data_dbt          	*data,
			 MEA_OutPorts_Entry_dbt              	*OutPorts_Entry,
			 MEA_Policer_Entry_dbt                 	*Policer_entry,
			 MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
			 MEA_Service_t                       	*o_serviceId)

{
	MEA_Status retStatus = MEA_ERROR;
	tMeterDb *pMeterId=NULL;
	ADAP_Uint32 idx;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif

	retStatus = MEA_API_Create_Service (unit,
			                            key,
			                            data,
			                            OutPorts_Entry,
			                            Policer_entry,
			                            EHP_Entry,
			                            o_serviceId);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif

	if( (retStatus == MEA_OK) && (data != NULL) )
	{
		pMeterId = MeaAdapGetMeterParamsByPolicerId(data->policer_prof_id);
		if(pMeterId != NULL)
		{
			for(idx=0;idx<MAX_NUMBER_OF_PM_PER_METER;idx++)
			{
				if(pMeterId->pmDb[idx].valid == ADAP_FALSE)
				{
					pMeterId->pmDb[idx].pmId = data->pmId;
					pMeterId->pmDb[idx].valid = ADAP_TRUE;
					pMeterId->pmDb[idx].service=(*o_serviceId);
				}
			}
		}
	}
	return retStatus;

}


MEA_Status  ENET_ADAPTOR_Set_Service(MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)

{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Set_Service(unit,serviceId,data,OutPorts_Entry,Policer_entry,EHP_Entry);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	return retStatus;
}



MEA_Status ENET_ADAPTOR_Get_Service (MEA_Unit_t                      unit_i,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Key_dbt      *key,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_Entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)
{

	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Get_Service(unit_i,serviceId, key,data,OutPorts_Entry, Policer_Entry,EHP_Entry);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	return retStatus;
}
MEA_Status ENET_ADAPTOR_Delete_all_services(MEA_Unit_t  unit)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Delete_all_services(unit);
#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif

	return retStatus;
}


MEA_Status ENET_ADAPTOR_Delete_Service (MEA_Unit_t                      unit_i,
                                    MEA_Service_t                   serviceId)
{

	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Delete_Service(unit_i,serviceId);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	// check if need to delete serviceId;

	MeaAdapDeleteServiceFromPmDb(serviceId);
	return retStatus;
}

MEA_Status ENET_ADAPTOR_IsExist_Service_ById(MEA_Unit_t  unit_i,MEA_Service_t  serviceId,MEA_Bool *valid)
{
	MEA_Status retStatus = MEA_ERROR;

#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_IsExist_Service_ById(unit_i,serviceId,valid);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	return retStatus;
}


MEA_Status ENET_ADAPTOR_IsExist_Service_ByKey(MEA_Unit_t                 unit,
    MEA_Service_Entry_Key_dbt *key,
    MEA_Bool                  *exist,
    MEA_Service_t             *serviceId)
{

    MEA_Status retStatus = MEA_ERROR;

#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_IsExist_Service_ByKey(unit, key, exist, serviceId);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;


}

MEA_Status ENET_ADAPTOR_Service_Set_MSTP_State(MEA_Unit_t unit, MEA_Service_t serviceId, MEA_MSTP_State_t mstp_state)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Service_Set_MSTP_State(unit,serviceId, mstp_state);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;


}

/**************************************************************************/

MEA_Status ENET_ADAPTOR_Create_Action (MEA_Unit_t                              unit_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_pi,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi,
                                   MEA_Action_t                          *Action_Id_io)
{

	MEA_Status retStatus = MEA_ERROR;
	tMeterDb *pMeterId=NULL;
	ADAP_Uint32 idx;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Create_Action(	unit_i,
									 	Action_Data_pio,
									   	Action_OutPorts_pi,
										Action_Policer_pi,
										Action_Editing_pi,
										Action_Id_io);


#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	if( (retStatus == MEA_OK) && (Action_Data_pio != NULL) )
	{
		pMeterId = MeaAdapGetMeterParamsByPolicerId(Action_Data_pio->policer_prof_id);
		if(pMeterId != NULL)
		{
			for(idx=0;idx<MAX_NUMBER_OF_PM_PER_METER;idx++)
			{
				if(pMeterId->pmDb[idx].valid == ADAP_FALSE)
				{
					pMeterId->pmDb[idx].pmId = Action_Data_pio->pm_id;
					pMeterId->pmDb[idx].valid = ADAP_TRUE;
					pMeterId->pmDb[idx].action=(*Action_Id_io);
				}
			}
		}
	}
	return retStatus;
}


MEA_Status ENET_ADAPTOR_Set_Action    (MEA_Unit_t                             unit_i,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_pi,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
		MEA_API_Lock();
#endif
		retStatus = MEA_API_Set_Action(unit_i,
	                                   Action_Id_i,
	                                   Action_Data_pio,
	                                   Action_OutPorts_pi,
	                                   Action_Policer_pi,
	                                   Action_Editing_pi);

#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
		return retStatus;

}


MEA_Status  ENET_ADAPTOR_Get_Action    (MEA_Unit_t                             unit_i,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_po,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_po,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_po,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_po)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_Action(unit_i,
                                    Action_Id_i,
                                    Action_Data_po,
                                    Action_OutPorts_po,
                                    Action_Policer_po,
                                    Action_Editing_po);
#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
return retStatus;
}

MEA_Status  ENET_ADAPTOR_Delete_Action (MEA_Unit_t                            unit_i,
                                        MEA_Action_t                          Action_Id_i){
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus = MEA_API_Delete_Action (unit_i, Action_Id_i);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    MeaAdapDeleteActionFromPmDb(Action_Id_i);
	return retStatus;
}


MEA_Status ENET_ADAPTOR_GetFirst_Action (MEA_Unit_t                           unit_i,
                                    MEA_Action_t                        *Action_Id_o ,
                                    MEA_Bool                            *found_o,
                                    mea_action_type_te 					Action_type)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
	MEA_API_Lock();
#endif
	retStatus =     MEA_API_GetFirst_Action (unit_i,Action_Id_o ,found_o,Action_type);
#ifdef USE_SEM_LOCK
	MEA_API_Unlock();
#endif
	return retStatus;

}

MEA_Status ENET_ADAPTOR_GetNext_Action  (MEA_Unit_t                           unit_i,
                                    MEA_Action_t                        *Action_Id_io,
                                    MEA_Bool                            *found_o,
                                    mea_action_type_te 					Action_type)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
		MEA_API_Lock();
#endif
		retStatus =     MEA_API_GetNext_Action (unit_i, Action_Id_io,found_o,Action_type);
#ifdef USE_SEM_LOCK
		MEA_API_Unlock();
#endif
		return retStatus;
}


/************************************************************************/
/*   SE   FWD                                                           */
/************************************************************************/
MEA_Status ENET_ADAPTOR_Create_SE_Entry(MEA_Unit_t               unit,
                                        MEA_SE_Entry_dbt        *entry)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Create_SE_Entry(unit, entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}




MEA_Status ENET_ADAPTOR_Delete_SE_Entry(MEA_Unit_t              unit_i,
    MEA_SE_Entry_key_dbt    *key_pi)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Delete_SE_Entry(unit_i, key_pi);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Set_SE_Entry(MEA_Unit_t                 unit,
    MEA_SE_Entry_dbt          *entry)
{
    MEA_Status retStatus = MEA_ERROR;

#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Set_SE_Entry(unit, entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_SE_Entry(MEA_Unit_t                    unit,
                                     MEA_SE_Entry_dbt             *entry)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_SE_Entry(unit, entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status ENET_ADAPTOR_GetFirst_SE_Entry(MEA_Unit_t              unit,
    MEA_SE_Entry_dbt       *entry,
    MEA_Bool*              found)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_GetFirst_SE_Entry(unit, entry, found);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status ENET_ADAPTOR_GetNext_SE_Entry(MEA_Unit_t               unit,
    MEA_SE_Entry_dbt *entry,
    MEA_Bool*                found)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_GetNext_SE_Entry(unit, entry, found);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

/************************************************************************/
/*     ENET_ADAPTOR_Create_Filter                                       */
/************************************************************************/



MEA_Status ENET_ADAPTOR_Create_Filter(MEA_Unit_t                           unit,
    MEA_Filter_Key_dbt                  *key_from,
    MEA_Filter_Key_dbt                  *key_to,
    MEA_Filter_Data_dbt                 *data,
    MEA_OutPorts_Entry_dbt               *OutPorts_Entry,
    MEA_Policer_Entry_dbt                *Policer_entry,
    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry,
    MEA_Filter_t                         *o_filterId)

{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Create_Filter(unit,
        key_from,
        key_to,
        data,
        OutPorts_Entry,
        Policer_entry,
        EHP_Entry,
        o_filterId);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status  ENET_ADAPTOR_Set_Filter(MEA_Unit_t                      unit,
    MEA_Filter_t                    filterId,
    MEA_Filter_Data_dbt            *data,
    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
    MEA_Policer_Entry_dbt          *Policer_entry,
    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)

{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Set_Filter(unit,
           filterId,
           data,
           OutPorts_Entry,
           Policer_entry,
           EHP_Entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status  ENET_ADAPTOR_Get_Filter(MEA_Unit_t                      unit,
    MEA_Filter_t                    filterId,
    MEA_Filter_Key_dbt             *key_from,
    MEA_Filter_Key_dbt             *key_to,
    MEA_Filter_Data_dbt               *data,
    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
    MEA_Policer_Entry_dbt          *Policer_entry,
    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_Filter(unit,
        filterId,
        key_from,
        key_to,
        data,
        OutPorts_Entry,
        Policer_entry,
        EHP_Entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_Filter_KeyType(MEA_Unit_t unit_i,
    MEA_Filter_Key_dbt *key_i,
    MEA_Filter_Key_Type_te *keyType_o)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_Filter_KeyType(unit_i,
        key_i,
        keyType_o);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status  ENET_ADAPTOR_Delete_Filter(MEA_Unit_t                      unit,
    MEA_Filter_t                    filterId)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Delete_Filter(unit,filterId);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Delete_all_filters(MEA_Unit_t unit)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Delete_all_filters(unit);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_GetFirst_Filter(MEA_Unit_t     unit,
    MEA_Filter_t *o_filterId,
    MEA_Bool     *o_found)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_GetFirst_Filter(unit, o_filterId,  o_found);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_GetNext_Filter(MEA_Unit_t     unit,
    MEA_Filter_t *io_filterId,
    MEA_Bool     *o_found)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_GetNext_Filter(unit, io_filterId, o_found);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status ENET_ADAPTOR_IsExist_Filter_ById(MEA_Unit_t     unit,
    MEA_Filter_t  filterId,
    MEA_Bool      *exist)
{
    MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_IsExist_Filter_ById(unit, filterId, exist);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}





/* Note: only keys and exist are mandatory */
/* Note : key_to parameter MUST be NULL*/
MEA_Status ENET_ADAPTOR_IsExist_Filter_ByKey(MEA_Unit_t                 unit,
    MEA_Filter_Key_dbt       *key_from,
    MEA_Filter_Key_dbt       *key_to,
    MEA_Bool                  *exist,
    MEA_Filter_t             *filterId)
{
    MEA_Status retStatus = MEA_ERROR;


    retStatus = MEA_API_IsExist_Filter_ByKey(unit,
        key_from,
        key_to,
        exist,
        filterId);



    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_Globals_Entry(MEA_Unit_t     unit,MEA_Globals_Entry_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Get_Globals_Entry(unit,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Set_Globals_Entry(MEA_Unit_t     unit,MEA_Globals_Entry_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Set_Globals_Entry(unit,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_Counters_PM(MEA_Unit_t     unit,MEA_PmId_t pmid,MEA_Counters_PM_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Get_Counters_PM(unit,pmid,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_Get_IngressPort_Entry(MEA_Unit_t     unit,MEA_Port_t enetPort,MEA_IngressPort_Entry_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Get_IngressPort_Entry(unit, enetPort, entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Set_IngressPort_Entry(MEA_Unit_t     unit,MEA_Port_t enetPort,MEA_IngressPort_Entry_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Set_IngressPort_Entry(unit,enetPort,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_EgressPort_Entry(MEA_Unit_t     unit,MEA_Port_t enetPort,MEA_EgressPort_Entry_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Get_EgressPort_Entry(unit, enetPort, entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Set_EgressPort_Entry(MEA_Unit_t     unit,MEA_Port_t enetPort,MEA_EgressPort_Entry_dbt  *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif

    retStatus = MEA_API_Set_EgressPort_Entry(unit,enetPort,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Get_LxCP_Protocol(MEA_Unit_t unit,MEA_LxCp_t LxCp_Id,MEA_LxCP_Protocol_key_dbt *LxCp_key,MEA_LxCP_Protocol_data_dbt  *LxCp_data)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_LxCP_Protocol(unit, LxCp_Id,LxCp_key,LxCp_data);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Set_LxCP_Protocol(MEA_Unit_t unit,MEA_LxCp_t LxCp_Id,MEA_LxCP_Protocol_key_dbt *LxCp_key,MEA_LxCP_Protocol_data_dbt  *LxCp_data)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Set_LxCP_Protocol(unit, LxCp_Id,LxCp_key,LxCp_data);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Create_LxCP_Protocol(MEA_Unit_t unit,MEA_LxCP_Entry_dbt  *LxCp_entry,MEA_LxCp_t *pNewLxcp)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Create_LxCP(unit, LxCp_entry,pNewLxcp);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_Unit_t unit,MEA_Uint16 policer_id,MEA_AcmMode_t ACM_Mode,MEA_Policer_Entry_dbt  *Entry_o)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_Policer_ACM_Profile(unit, policer_id,ACM_Mode,Entry_o);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Create_Policer_ACM_Profile(MEA_Unit_t  unit, MEA_Uint16 *policer_id,MEA_AcmMode_t  ACM_Mode,MEA_IngressPort_Proto_t       port_proto_prof_i,MEA_Policer_Entry_dbt         *Entry_i)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Create_Policer_ACM_Profile(unit,policer_id,ACM_Mode,port_proto_prof_i,Entry_i );

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Collect_Counters_PMs_Block(MEA_Unit_t  unit,MEA_Uint32 block)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Collect_Counters_PMs_Block(unit,block);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}
MEA_Status ENET_ADAPTOR_TFT_Prof_Mask_Create(MEA_Unit_t  unit,MEA_Uint16 *MaskId)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Prof_Mask_Create(unit,MaskId);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_TFT_Prof_Mask_Get(MEA_Unit_t  unit,MEA_Uint16 MaskId)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Prof_Mask_Delete(unit,MaskId);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_TFT_Create_QoS_Profile(MEA_Unit_t  unit,MEA_TFT_t  *tft_profile_Id, MEA_TFT_entry_data_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Create_QoS_Profile(unit,tft_profile_Id,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}
MEA_Status ENET_ADAPTOR_TFT_Mask_Set(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Prof_Mask_Set(unit,MaskId,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_TFT_Mask_Get(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Prof_Mask_Get(unit,MaskId,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_TFT_Prof_Mask_IsExist(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_Bool silent, MEA_Bool *exist)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Prof_Mask_IsExist(unit,MaskId,silent,exist);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status ENET_ADAPTOR_TFT_Get_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Get_ClassificationRule(unit,tft_profile_Id,index,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_TFT_Set_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_TFT_Set_ClassificationRule(unit,tft_profile_Id,index,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Create_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Create_UEPDN_TEID_ConfigAction(unit,UEPDN_Id,index,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_Collect_Counters_RMON(MEA_Unit_t unit,MEA_Port_t port,MEA_Counters_RMON_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Collect_Counters_RMON(unit,port,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_Get_Counters_RMON(MEA_Unit_t unit,MEA_Port_t port,MEA_Counters_RMON_dbt *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Get_Counters_RMON(unit,port,entry);

#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}



MEA_Status ENET_ADAPTOR_Set_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry )
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Set_IngressPort_Default_PDUs_Sid(unit,port,PDUsType,entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}


MEA_Status ENET_ADAPTOR_Set_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry )
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_Set_IngressPort_Default_L2Type_Sid(unit,port,L2Type,entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_VPLSi_UE_PDN_DL_Create_Entry(MEA_Unit_t unit,MEA_Uint16 vpls_Ins_Id,MEA_Uint32  UE_pdn_Id,MEA_VPLS_UE_Pdn_dl_dbt  *entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_VPLSi_UE_PDN_DL_Create_Entry(unit,vpls_Ins_Id,UE_pdn_Id,entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Status ENET_ADAPTOR_VPLSi_isExist(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id,MEA_Bool *exist)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_VPLSi_isExist(unit,vpls_Ins_Id,exist);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}
MEA_Status ENET_ADAPTOR_VPLSi_Create_Entry(MEA_Unit_t  unit, MEA_Uint16 *vpls_Ins_Id, MEA_VPLS_dbt* entry)
{
	MEA_Status retStatus = MEA_ERROR;
#ifdef USE_SEM_LOCK
    MEA_API_Lock();
#endif
    retStatus = MEA_API_VPLSi_Create_Entry(unit,vpls_Ins_Id,entry);
#ifdef USE_SEM_LOCK
    MEA_API_Unlock();
#endif
    return retStatus;
}

MEA_Bool ENET_ADAPTOR_IsExist_ActionID (MEA_Unit_t   unit_i,
										MEA_Action_t  Action_Id_i)
{
	MEA_Bool retStatus = MEA_FALSE;
	MEA_Bool exist_o   = MEA_FALSE;

#ifdef USE_SEM_LOCK
		MEA_API_Lock();
#endif

	  if(MEA_API_IsExist_Action_ByType (unit_i, Action_Id_i,MEA_ACTION_TYPE_FWD,&exist_o) !=MEA_OK){
		  retStatus=MEA_FALSE;
	  }
	  retStatus=exist_o;

#ifdef USE_SEM_LOCK
		MEA_API_Unlock();
#endif
		return retStatus;
}

MEA_Bool ENET_ADAPTOR_Create_FWD_VPN_Ipv6(MEA_Unit_t    unit, MEA_FWD_VPN_dbt *entry)
{
	MEA_Bool retStatus = MEA_TRUE;

#ifdef USE_SEM_LOCK
                MEA_API_Lock();
#endif

	if (MEA_API_Create_FWD_VPN_Ipv6(unit, entry) != MEA_OK)
	{
		retStatus = MEA_FALSE;
	}

#ifdef USE_SEM_LOCK
                MEA_API_Unlock();
#endif
                return retStatus;
}
