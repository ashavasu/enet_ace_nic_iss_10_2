#//Alex weis 10/12/2017

export MY_PLATFORM="pcicard"
export ENET_WITH_THIRD_PARTY = "no"


CFLAGS      +=-g -Wall -DMEA_OS_LINUX
PROGNAME:=SmartNic.e
include_dirs := ./ ../include/
link_flags:= -ldl -lrt -lpthread 
source_dirs  = ../src/
OBJ_DIR = ../obj
#LDFLAGS += -static -lRemoteClient
export NIC_PLATFORM="yes"




SUBDIR_APP=./OpenFlow_agent/bin
SUBDIR_CLI=./OpenFlow_agent/openflow_cli/bin
SUBDIR_OF=./OpenFlow_agent/build
SUBDIR_OF_CLI=./OpenFlow_agent/openflow_cli/build
SUBDIR_ENET_ADAP=./enet_adaptor/Make
SUBDIR_ENET_RESTFul= ./ENET_RESTFul_python/Enet_sources
SUBDIR_ENET_DRIVER=./Ethernity
SUBDIR_COMLIB=./comlib/build

export MEA_ETHERNITY="yes"
#Open this when we work withHW
#export HW_BOARD_IS_PCI="yes"

export MEA_EXTRA_SUBSYSTEM=L1/$MY_PLATFORM;
export ENET_OBJ_DIR			= $(SUBDIR_ENET_DRIVER)/lib/$(MEA_EXTRA_SUBSYSTEM)
 

subsystem:
#	cd $(SUBDIR_ENET_DRIVER) && ./zynq70_Build_proj_o.bash
	./zynq70_Build_proj_o.bash
	cd $(SUBDIR_ENET_ADAP) && $(MAKE)
	cd $(SUBDIR_OF) && $(MAKE)
	cd $(SUBDIR_ENET_RESTFul) && $(MAKE)
	
all:	
	./zynq70_Build_proj_o.bash
zync: 
	cp make_zync.h make_ext.h
	./zynq70_Build_proj_o.bash
	cd $(SUBDIR_ENET_ADAP) && $(MAKE)
	cd $(SUBDIR_OF) && $(MAKE)
	cd $(SUBDIR_ENET_RESTFul) && $(MAKE)
	cp $(SUBDIR_APP)/SmartNic.e ./AceNic_output 

nic:
	export MY_PLATFORM="pcicard"
	cp make_nic.h make_ext.h
	cd $(SUBDIR_ENET_DRIVER) && export NIC_PLATFORM=yes  && ./PCI_CARD_Rebuild.bash
	cd $(SUBDIR_COMLIB) && $(MAKE)
	cd $(SUBDIR_ENET_ADAP) && $(MAKE) NIC_PLATFORM=yes
	cd $(SUBDIR_OF) && $(MAKE)
	cd $(SUBDIR_OF_CLI) && $(MAKE)
	cd $(SUBDIR_ENET_RESTFul) && $(MAKE)
	cp $(SUBDIR_APP)/SmartNic.e ./AceNic_output 
	cp $(SUBDIR_CLI)/ofcli.e ./AceNic_output/ofcli

bl:
	export MY_PLATFORM="pcicard"
	cp make_nic.h make_ext.h
	cd $(SUBDIR_ENET_DRIVER) && export BL_PLATFORM=yes && ./PCI_CARD_Rebuild.bash
	cd $(SUBDIR_COMLIB) && $(MAKE)
	cd $(SUBDIR_ENET_ADAP) && $(MAKE) BL_PLATFORM=yes
	cd $(SUBDIR_OF) && $(MAKE)
	cd $(SUBDIR_OF_CLI) && $(MAKE)
	cd $(SUBDIR_ENET_RESTFul) && $(MAKE)
	cp $(SUBDIR_APP)/SmartNic.e ./AceNic_output
	cp $(SUBDIR_CLI)/ofcli.e ./AceNic_output/ofcli



target_copy:


clean: 
	echo text "clean all"
	find . -name '*.o' -delete
	find . -name '*.d' -delete




